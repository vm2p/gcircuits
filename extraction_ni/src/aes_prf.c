/**
   OCaml wrapper for AES-NI implementation
   from justGarble.
 */

#include <stdio.h>
#include <wmmintrin.h>
#include "aes.h"

#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>

CAMLprim value
aes (const unsigned char *in, unsigned char *key) {
	CAMLparam2(in, key);
	CAMLlocal1(cipher);
	
	unsigned char out[16];

	AES_KEY new_key;
	AES_set_encrypt_key(key, 128, &new_key);
	
	AES_encrypt(in, out, &new_key);

	int i;

	cipher = caml_alloc(16,0);

	for (i=0;i<16;i++)
	{ Store_field(cipher, i, out[i]); }	

	CAMLreturn(cipher);
}
