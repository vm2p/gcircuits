%!Tex Root=sfecert.tex

\subsection{Overview of the protocol and its proof}
\label{sec:proofoverview}

Excellent descriptions of Yao's secure function evaluation 
protocol with slightly different security proofs can be found 
in~\cite{LP09,DBLP:conf/ccs/BellareHR12}.

\heading{Yao's protocol in a nutshell}
Yao's idea of garbling the circuit computing $f$ consists informally 
of: \begin{inparaenum}[i.]
\item expressing such a circuit as a set of truth tables (one for each
  gate) and meta information describing the wiring between gates;
\item replacing the actual Boolean values in the truth tables with
  random cryptographic keys, called {\em labels}; and 
\item translating the wiring relations using a system of {\em locks}:
  truth tables are encrypted one label at a time so that, for each
  possible combination of the input wires, the corresponding labels
  are used as encryption keys that lock the label for the correct
  Boolean value at the output of that gate.
  \end{inparaenum}
Then, given a garbled circuit for $f$ and a set of labels representing
(unknown) values for the input wires encoding $x_1$ and $x_2$, one can
obliviously evaluate the circuit by sequentially computing one gate
after another: given the labels of the input wires to a gate, only one
entry in the corresponding truth table will be decryptable, revealing
the label of the output wire. The output of the circuit will comprise
the labels at the output wires of the output gates. 

To build a SFE protocol between two honest-but-curious parties, one can 
use Yao's garbled circuits as follows. Bob (holding $x_2$) garbles the 
circuit and provides this to Alice (holding $x_1$) along with: i. the 
label assignment for the input wires corresponding to $x_2$, and ii. all 
the information required to decode the Boolean values of the output wires.
In order for Alice to be able to evaluate the circuit, she should be 
able to obtain an the correct label assignment for $x_1$. Obviously, 
Alice cannot reveal $x_1$ to Bob, as this would totally destroy the 
goal of SFE. Furthermore, Bob cannot reveal information that would 
allow Alice to encode anything other than $x_1$, since this would 
reveal more than $f(x_1,x_2)$. To solve this problem, Yao proposed the 
use of an {\em oblivious transfer} (OT) protocol. This is a (lower-level) 
SFE protocol for a very simple functionality that allows Alice to obtain 
the labels that encode $x_1$ from Bob, without revealing anything about 
$x_1$ and learning nothing more than the labels she requires.\footnote{Luckily, 
efficient OT protocols exist that can be used for this specific purpose, 
thereby eliminating what could otherwise be a circular dependency.} The 
protocol is completed by Alice evaluating the circuit, recovering the 
output, and providing the output value back to Bob.

\heading{Bellare, Hoang and Rogaway's~\cite{DBLP:conf/ccs/BellareHR12} proof}
Our starting point for producing a formally verified implementation of
Yao's protocol is to transpose to \EasyCrypt the modular security proof that
was given by Bellare, Hoang and Rogaway~\cite{DBLP:conf/ccs/BellareHR12}.
The central component in this proof is a new abstraction called a {\em
garbling scheme} that captures the functionality of security properties
of the circuit garbling technique that is central to Yao's SFE protocol.
This new abstraction was used by the authors to make precise the
different security notions that could apply to this garbling step.
This, in turn, permits separating the design and analysis of efficient 
garbling schemes with different properties from higher level protocols, 
which include secure function evaluation, but extend well beyond.

Figure~\ref{fig:origproof} shows the structure of the proof of security 
for Yao's protocol given in~\cite{DBLP:conf/ccs/BellareHR12}
(we focus only on the result that is relevant for this paper).
We depict constructions as rectangles with grey captions and 
primitives (i.e., cryptographic abstractions with a well-defined
sytax and security model) as rounded rectangles with black captions.
Security proofs are represented by dashed arrows and implications
between notions as solid arrows.
A construction enclosing a primitive in the diagram indicates that
the primitive is used as an abstract building block in its
security proof. 
For example, arrow (1) indicates that the first step in the
proof is the construction of a Dual Key Cipher using a standard
PRF security assumption via a simple Dual Masking construction. 
The same primitive is constructed from an Ideal Cipher via 
the Dual Encryption construction.

\begin{figure}[t]
\centering
\scalebox{.9}{
\begin{tikzpicture}[
  node distance=0.5mm,
  construction/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=2.2cm},
  primitive/.style={rounded rectangle, draw=black!50, font=\scriptsize\ttfamily}
]
  \node (dmasking) [construction] { Double Masking };
  \node (prf) [below=of dmasking.south, primitive] { PRF };
  \node (dkc1) [rectangle, draw=black!50, fit={(dmasking) (prf)}] {};

  \node (dencrypt) [below=of dmasking.south, construction, yshift=-0.7cm] { Double Encryption };
  \node (ic) [below=of dencrypt.south, primitive] { Ideal Cipher };
  \node (dkc2) [rectangle, draw=black!50, fit={(dencrypt) (ic)}] {};

  \node (dkc) [primitive, draw=black!50, right=of dmasking.east,xshift=1cm,yshift=-0.8cm] {Dual Key Cipher};

  \draw [->,dashed] (dkc1.east) -- (dkc.175)  node[midway,above] {\tiny 1};
  \draw [->,dashed] (dkc2.east) -- (dkc.185);

  \node (g1) [construction,above= of dkc.north,yshift=0.4cm] { Garble1 };
  \node (gscheme1) [rectangle, draw=black!50, fit={(g1) (dkc) (dkc1) (dkc2)}] {};

  \node (gscheme) [primitive, draw=black!50, dashed, right=of gscheme1.east,xshift=0.5cm,text width=2.4cm,align=center] {Garbling Scheme\\IND Secure};
  \draw [->,dashed] (gscheme1.east) -- (gscheme)  node[midway,above] {\tiny 2};

  \node (gschemesim) [primitive, draw=black!50, below=of gscheme.south,yshift=-0.2cm,text width=2.4cm,align=center] {Garbling Scheme\\SIM Secure};
  \draw [->] (gscheme.south) -- (gschemesim)  node[midway,right] {\tiny 3};

  \node (ot) [primitive, draw=black!50, above=of gscheme.north,yshift=0.2cm,text width=2.4cm,align=center] {Oblivious Transfer\\2PPP Secure};

  \node (yao) [construction,above= of g1.north,yshift=0.3cm] { Yao's Protocol };
  \node (sfe1) [rectangle, draw=black!50, fit={(gscheme1) (gschemesim) (yao)}] {};

  \node (sfe) [primitive, draw=black!50, right=of sfe1.east,xshift=0.5cm,text width=2cm,align=center] {SFE\\2PPP Secure};
  \draw [->,dashed] (sfe1.east) -- (sfe)  node[midway,above] {\tiny 4};
\end{tikzpicture}
}
\vspace{-6pt}
\caption{Structure of Yao's protocol security proof in~\cite{DBLP:conf/ccs/BellareHR12}.}
\label{fig:origproof}
\vspace{-12pt}
\end{figure}

A {\em dual-key cipher} (DKC) is a tweakable deterministic 
encryption scheme that encrypts secret keys (corresponding 
to output wire labels) and is keyed by two independent keys 
(corresponding to input wire labels).
%
The DKC security model is purposefully designed to be 
just strong enough to allow constructing garbling schemes, 
in order not to exclude interesting instantiations, 
namely dual-encryption.\footnote{This introduces some
intricacies that render proofs unnecessarily
complicated when the goal is to rely only on 
instantiations based on PRFs.}
Intuitively, DKC security is a real-or-random
definition, where there is an unbounded number of keys
to choose from, both for posing as encryption keys and
as encrypted keys.
One of these secret keys is singled out as the challenge
secret key, and it can never be encrypted nor revealed 
to the attacker (who may see other keys).
The model also captures the fact that it is convenient to leak 
the least significant bit of such keys in order to encode the 
topology of a circuit during garbling.

The second step in the proof (2) is to construct a garbling
scheme from a (DKC). 
There are two security definitions for garbling schemes:
indistinguishability-based (IND) and simulation-based (SIM).
The former is used as a stepping stone (hence its dashed
presentation in the diagram) to proving SIM-security.
Indeed, the two notions are proven to be equivalent for
for certain classes of garbling schemes (this is shown 
as step 3 in the digram).
Proving that a concrete construction called \textsf{Garble1}
achieves IND security is the most challenging part of the
proof, as it involves a hybrid argument over those wires
in the circuit that are {\em not} visible to an attacker
(the security model allows the attacker to observe the
opening of the circuit for one concrete input).

The final step (4) in the proof is to show that Yao's
technique of combining an oblivious transfer protocol---
two-party passively (2PPP) secure---with a SIM-secure
garbling scheme yields a 2PPP secure SFE protocol.
This step consists of a game-hopping argument
with two relatively simple transitions.

\heading{Our Proof}
We show in Figure~\ref{fig:ourproof} the structure of our
\EasyCrypt formalization. It is visible in the figure that
the main structure of the proof, steps 1-4 are very close to
the original proof of~\cite{DBLP:conf/ccs/BellareHR12}.
The only deviation here is that we simplify the Dual Key
Cipher security game to a slightly stronger variant that 
is still satisfied by the PRF-based instantiation, but 
which has an internal structure that makes the proof of
security of the garbling scheme significantly easier.
Intuitively, the difference is that one imposes that
the tweak effectively makes encryptions of the same value 
indistinguishable of each-other, which might not happen 
in some valid DKC instantiations. 
To further simplify our proofs, our DKC security definition
is also parametrized by two integer parameters 
$\mathsf{c}$ and $\mathsf{pos}$. The first parameter
provides an upper-bound on the number of keys in the game,
so that they can all be sampled in the beginning of
the security experiment. The second parameter specifies
an index in the range $[1..\mathsf{c}]$ that will
be used in oracle queries as the index for the hidden
secret key.

The Figure also shows three additional proof steps.
These correspond to instantiation (i.e., restricted 
forms of composition) steps that are often implicit in 
hand-written cryptographic proofs.
For example, suppose construction $C_1^{P_2}$ is 
proven to achieve the properties specified by primitive 
$P_1$ assuming the properties specified by abstract primitive 
$P_2$. 
Then, if construction $C_2^{P_3}$ is proven to meet the requirements
imposed by primitive $P_1$, under assumption $P_3$, this implies 
that $C_1^{C_2}$ also meets the $P_1$ requirements under assumption
$P_3$.

These are important steps because we want our final Theorem
to refer to a concrete efficient implementation of Yao's
protocol which is implemented in an \EasyCrypt dialect
that is prone to extraction into OCaml code.
This is shown in blue in the diagram. 
To obtain such a result we need to include explicit
theorems in our formalization that instantiate abstract
security results in order to obtain concrete security
bounds for implementations.
For example, in order to obtain a security theorem 
for our implementation one needs to prove i. that it 
is functionally equivalent to the composition of
concrete oblivious-transfer and garbling schemes
and ii. that this implies that the security
bound for the generic theorem (4) can be instantiated
into a concrete bound by plugging in the security
bounds for the intermediate results all the way 
down to the low level PRF, DDH and entropy 
smoothing assumptions. 

In what follows we give a brief overview of how 
\EasyCrypt enables formalizing both the complex 
abstract security proofs and the instantiation
steps (with very little overhead in these cases).

\heading{A note on randomness} It is
common practice in cryptographic implementations to separate the
randomness sampling operations from the rest of the implementation.
There are two reasons for this: on one hand, general-purpose
programming languages are not probabilistic, and so randomness must
invariably be obtained via some sort of system/library call and
handled explicitly; on the other hand, developers (particularly in
open-source scenarios) often give the end-users the possibility (and
responsibility) to choose their preferred randomness sampling
procedure (see, for example, the NaCl cryptographic
library\footnote{\url{http://nacl.cr.yp.to}}).

We take the same approach, describing various cryptographic algorithms
as deterministic functional operators, so that we immediately get for
free a one-to-one mapping between the \EasyCrypt functional
definitions and ML code.  The randomness generation procedures, which
we see as ideal specifications of the required distributions that must
be available in practical applications, are described separately as
\EasyCrypt modules.
%written imperatively.

\begin{figure}[t]
\centering
\scalebox{.9}{
\begin{tikzpicture}[
  node distance=0.5mm,
  implementation/.style={rectangle, fill=black!20, draw=blue!50, 
  font=\fontsize{6}{6}\color{blue!50}\ttfamily,minimum width=2.2cm},
  construction/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=2.2cm},
  primitive/.style={rounded rectangle, draw=black!50, font=\scriptsize\ttfamily}
]
  \node (dmasking) [construction] { Double Masking };
  \node (prf) [below=of dmasking.south, primitive] { PRF };
  \node (dkc1) [rectangle, draw=black!50, fit={(dmasking) (prf)}] {};

  \node (dkc) [primitive, draw=black!50, right=of dkc1.east,xshift=1cm] {Dual Key Cipher'};

  \draw [->] (dkc1.east) -- (dkc.west)  node[midway,above] {\tiny 1};

  \node (g1) [construction,above= of dkc.north] { SomeGarble };
  \node (gscheme1) [rectangle, draw=black!50, fit={(g1) (dkc) (dkc1) }] {};

  \node (gscheme) [primitive, draw=black!50, dashed, right=of gscheme1.east,xshift=0.5cm,text width=2.4cm,align=center] {Garbling Scheme\\IND Secure};
  \draw [->] (gscheme1.east) -- (gscheme)  node[midway,above] {\tiny 2};

  \node (gschemesim) [primitive, draw=black!50, above=of gscheme.north,yshift=0.2cm,text width=2.4cm,align=center] {Garbling Scheme\\SIM Secure};
  \draw [->] (gscheme.north) -- (gschemesim)  node[midway,right] {\tiny 3};
  \draw [->, draw=blue!50] (gscheme1.8) -- (gschemesim.-175)  node[midway,above] {\tiny 5};

  \node (ddh) [above=of dmasking.north, primitive,yshift=1cm,xshift=-.05cm] { DDH };
  \node (es) [right=of ddh.east, primitive,xshift=1cm] { Entropy Smoothing };
  \node (someot) [construction, above=of es.north,xshift=-1cm] { SomeOT };
  \node (ot1) [rectangle, draw=black!50, fit={(ddh) (es) (someot)},minimum width=6.1cm] {};

  \node (ot) [primitive, draw=black!50, right=of ot1.east,xshift=0.5cm,text width=2.4cm,align=center] {Oblivious Transfer\\2PPP Secure};
  \draw [->, draw=blue!50] (ot1.east) -- (ot)  node[midway,above] {\tiny 6};  

  \node (yao) [construction,above= of ot1.north,xshift=1cm] { Yao's Protocol };
  \node (sfe1) [rectangle, draw=black!50, fit={(gscheme1) (gschemesim) (yao) (ot) (ot1)}] {};

  \node (sfe) [primitive, draw=black!50, right=of sfe1.east,xshift=0.5cm,text width=2cm,align=center] {SFE\\2PPP Secure};
  \draw [->] (sfe1.east) -- (sfe)  node[midway,above] {\tiny 4};

  \node (efficient) [implementation,above= of sfe.north,yshift=.72cm] { Implementation };
  \draw [<->, draw=blue!50] (sfe1.15) -- (efficient.west)  node[midway,above] {\tiny 7};
  \draw [->, draw=blue!50] (efficient.south) -- (sfe.north)  node[midway,right] {\tiny 8};

\end{tikzpicture}
}
\vspace{-6pt}
\caption{Structure of our verified security proof of an implementation of Yao's protocol.}
\label{fig:ourproof}
\vspace{-12pt}
\end{figure}
