%!Tex Root=sfecert.tex
\section{Introduction} \label{sec:intro}
Computer-aided cryptography is an emerging branch of cryptography that
aims to improve the design, security analysis, and implementation of
cryptography. At present, computer-aided cryptography embraces two
loosely connected lines of work: high-assurance cryptography and
cryptographic engineering. Broadly speaking, one main goal of
high-assurance cryptography is to develop rigorous verification
methods for reductionist security proofs of algorithmic descriptions
of cryptographic constructions, in the style that prevails provable
security; this line of work leverages deep connections between the
proofs methods used in reductionist proofs and those used in formal
verification to prove relational properties (specifically different
forms of equivalence) of programs. Another main goal of high-assurance
cryptography is to prove functional correctness of implementations;
this line of work is well aligned with the bulk of work carried in
program verification, and benefits from significant advances of the
field. 

Cryptographic engineering is the niche area of software engineering 
that brings cryptography to practice; this encompasses widely used 
cryptographic implementations and libraries such as 
openSSL,\footnote{\url{http://openssl.org}} 
s2n,\footnote{\url{http://https://github.com/awslabs/s2n}} 
Bouncy Castle\footnote{\url{https://www.bouncycastle.org/}}
and prototyping frameworks such as CHARM~\cite{CHARM} and
SCAPI~\cite{SCAPI}.
More recently, cryptographic engineering has evolved to
a series of ambitious projects that aim to bring to real-world 
applications a new generation of cryptographic protocols that has 
matured in the last two decades, the more prominent of which 
being secure computation over encrypted data. 
These projects include 
FRESCO\footnote{\url{https://github.com/aicis/fresco}}~\cite{DamgardDNNT15}, 
TASTY~\cite{HKSSW10} and Sharemind~\cite{BLW08}.

In contrast to other areas of software engineering for critical systems,
the benefits of formal verification technology for cryptographic engineering 
have been very limited, with some recent and notable
exceptions~\cite{CRYPTOL,Fournet:2011,Affeldt:2012,Bhargavan:2013,Almeida:2013,Barthe:2014}.
The reasons for this are plain: cryptographic software is a harder
challenge for high-assurance software development, due to the 
tension that arises between complex specifications and the need
for very high-efficiency---security is supposed to be invisible,
and current verification technology comes with a performance penalty.

Taken separately, cryptographic engineering and high-assurance
cryptography cannot meet growing expectations for simultaneous 
high-assurance, high-speed an high-security efficiency required
for practical deployment. However, unity
is strength, and a tight integration of high-assurance cryptography
and cryptographic engineering can deliver the combined benefits of 
provable security and best engineering practices. While this claim 
is not new, the combination of the two approaches has only been 
experimented at a small scale, typically on
core cryptographic primitives. In this paper, we combine the two
approaches at a significantly larger scale and deliver a fast and
verified software stack for secure multi-party computation (SMPC).
Our choice is to focus on the specific SMPC instance of 
two-party secure function evaluation (SFE).
This choice is motivated by several factors: first SFE is
among the foremost practical applications of cryptography, and is a
fundamental building block for making cloud computing secure. Second,
it is a tremendous challenge for high-assurance cryptography, as its
security proofs are markedly distinct from prior work in formalizing
reductionist arguments.

\heading{Contributions}
We present a high-assurance and high-speed software stack for secure
multi-party computation. Figure~\ref{fig:iarch} presents the overall
architecture of the stack. The lowest component of our stack is
FRESCO~\cite{DamgardDNNT15}; an
existing, practical, open-source, framework for secure multi-party
computation, which we use for communications (we assume passive adversaries)
and input/output.

\begin{wrapfigure}{R}{.5\textwidth}
\centering
\begin{tikzpicture}[
  node distance=5mm,
  thing/.style={rectangle, dashed, draw=black!50,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=3.5cm},
  unverif/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=3.5cm},
  verif/.style={rounded rectangle, draw=blue!70, font=\fontsize{6}{6}\color{blue!70}\ttfamily,minimum width=3.5cm}
]
  \node (ccode) [thing] { C program };
  \node (ccomp) [below=of ccode, verif] { Certified Compiler };
  \node (bcircuit) [below=of ccomp, thing] { Boolean circuit };
  \node (yao) [below=of bcircuit, verif] { Verified Yao's Protocol };
  \node (comms) [below=of yao, thing] { Communications };
  \node (fresco) [below=of comms, unverif,yshift=3.5mm] { FRESCO };
  \node (frescob) [rectangle, draw=black!50, fit={(fresco) (comms) (yao)},yshift=1mm] {};

  \draw [->,dashed] (ccode.south) -- (ccomp.north) {};
  \draw [->,dashed] (ccomp.south) -- (bcircuit.north) {};
  \draw [->,dashed] (bcircuit.south) -- (yao.north) {};

  \draw [<->] (yao.south) -- (comms.north) {};
  \draw [->] ([xshift=-1.5cm]frescob.175) -- (frescob.175) node[midway,above,font=\fontsize{6}{6}\color{black!50}\ttfamily] {Inputs};
  \draw [<-] ([xshift=-1.5cm]frescob.-175) -- (frescob.-175) node[midway,below,font=\fontsize{6}{6}\color{black!50}\ttfamily] {Outputs};

\end{tikzpicture}
\vspace{-6pt}
\caption{Architecture of our verified cryptographic software stack.}
\label{fig:iarch}
\vspace{-.7cm}
\end{wrapfigure}

The intermediate component of our stack is a verified implementation
of Yao's secure function evaluation protocol based on 
garbled circuits and oblivious transfer.
This protocol allows two parties holding private
inputs $x_1$ and $x_2$, to jointly evaluate any function
$f(x_1,x_2)$ and learn its result, whilst being assured that no
additional information about their respective inputs is
revealed. Two-party secure function evaluation provides a general 
distributed solution to the problem of computing over encrypted data 
in cloud scenarios; and we allow for both scenarios where the 
function is public and both sides provide inputs and scenarios 
where one party provides the
(secret albeit with leaked topology) circuit to be computed and
the other party provides the input to the computation.

Our implementation is mechanically verified in
\EasyCrypt~\cite{Barthe:2011a}, an interactive proof assistant
with dedicated support to perform reductionist proofs in the
computational model, and leverages the foundational framework put
forth by Bellare, Hoang and Rogaway~\cite{DBLP:conf/ccs/BellareHR12,BHR:2012a}, and
an $n$-fold extension of the oblivious transfer protocol by Bellare
and Micali~\cite{BM89}, in the hashed version presented by Naor and
Pinkas~\cite{Naor:2001} (\(n\)~being the size of the selection
string). The implementation is proved secure relative to standard
assumptions: the Decisional Diffie-Hellman problem, the existence of
entropy-smoothing hash functions and pseudorandom functions.

The higher-level component of our stack is a verified optimizing compiler
from C programs to boolean circuits. Our compiler is mechanically
verified using the \Coq proof assistant, and builds on top of the
\CompCert compiler~\cite{Leroy06}, a verified optimizing compiler for
C programs. It reuses the front-end and middle end of \CompCert, but
provides a new verified back-end to boolean circuits.  Our back-end
combines several techniques, such as translation of RTL programs into
guarded form and static single assignment form, some of which have not
been verified formally before.
We also implemented some (yet unverified) further optimizations that
make our circuit generator a competitive alternative for automatically
generating circuit implementations for the domain of cryptography,
when compared to other existing solutions such as CBMC-GC\footnote{\url{http://forsyte.at/software/cbmc-gc/}}~\cite{Franz:CC14}.

Each component is an independent, general-purpose, tool that can be
used outside of our software stack; for instance, the compiler can be
used to produce correct inputs for any cryptographic system that takes
Boolean circuits as an input. Our formalization of secure function
evaluation delivers several generic building blocks (e.g.\, oblivious
transfer) that could be reused by many other verified cryptographic
systems. However, the main strength of our results resides in the
fact that, for the first time, we are able to produce a joining of
high-assurance cryptography and cryptography engineering that covers
all the layers in a (admittedly passively secure) secure multiparty
computation software framework.
%
To summarize our contributions, we provide:
\begin{enumerate}
\item a verified implementation of Yao's two-party secure function
  evaluation (SFE) protocol based on garbled circuits and oblivious
  transfer, with a fully verified security proof down to standard
  low level assumptions on pseudorandom functions, Diffie-Helmann
  groups and hash functions.
  
\item a certified compiler that translates computation specifications
  in C into Boolean circuits; not only is our tool provably correct,
  but it also includes---for users that choose performance over
  verification---a set of unverified optimizations that produce 
  more efficient circuit implementations for the domain of cryptography
  that are competitive with respect to existing tools. 

\item experimental validation of our verified cryptographic system
    via integration into the open-source cryptographic framework 
    FRESCO. This allows us to use the outputs of our certified 
    Boolean circuit compiler with arbitrary cryptographic systems
    also integrated into FRESCO and, similarly, use our verified
    implementation of Yao's SFE protocol to evaluate computations
    generated via other tools. All of our code is publicly 
    available.
\end{enumerate}

\heading{Challenges}
The development of the software stack raised several challenges, which
we describe below.
\paragraph*{Machine-checked proofs of computational security.}
\EasyCrypt~\cite{Barthe:2011a} is an interactive proof assistant with dedicated
support to perform game-based reductionist proofs in the computational
model. In particular, \EasyCrypt supports common reasoning principles
in cryptographic proofs, including equivalence proofs, or proofs of
equivalence up to failure events, reductions, and eager/lazy sampling.
\EasyCrypt has been used for several emblematic examples, including
signatures and encryption schemes.

In spite of these successes, formalizing secure function evaluation is
a challenge, because it uses two cryptographic techniques that are not
supported: hybrid arguments and simulation-based security proofs. In
contrast to other standard techniques, which remain within the realm
of the relational program logic that forms the core of \EasyCrypt,
hybrid arguments and simulation-based proofs lie at the interface
between the relational program logic of \EasyCrypt, and its ambient
logic. Specifically, hybrid arguments combine induction proofs and
proofs in the relational program logic. Similarly, simulation-based
security proofs intrinsically require existential quantification over
adversarial algorithms and the ability to instantiate security models
with concrete algorithms (the simulators) that serve as witnesses as
to the validity of the security claims. These two forms of reasoning
excercise the expressive power of the ambient logic, and are thus
markedly distinct from the reductionistic arguments typically
addressed by \CryptoVerif or \EasyCrypt.

Secure function evaluation is also a challenging test case in terms of
scalability. Indeed, \EasyCrypt has been used primarily for primitives
and to a lesser extent for (components) of protocols. While these
examples can be intricate to verify, there is a difference of scale
with more complex cryptographic systems, such as secure function 
evaluation, which involve several layers of cryptographic constructions.
%
To realize our broader goal, we make several improvements to the
\EasyCrypt tool, in the form of new functionalities and of new
libraries. Highlights of our contributions include:
\begin{itemize} 
\item a formally verified library of generic hybrid arguments. The
  library is based on formalizing security games as parametrisable
  modules, and critically uses quantification over modules. We use
  hybrid arguments in the security proofs for oblivious transfer and
  for garbled circuits. However, the library can be used for other
  purposes, and will contribute to build a broad scopus of formally
  verified cryptographic techniques; 

\item simulation-based proofs where simulators and attackers can be
  seen as interchangeable adversarial algorithms, which can either be
  concrete, existentially quantified, or universally quantified. In
  particular, the generic statements of security for the SFE protocol
  involve two alternations of quantifiers, which would be challenging
  to handle even in a hand-written proof.
 
\item layered security proofs in which general compositional theorems
  can be proven using abstract views of cryptographic primitives, and
  instantiations of these primitives can be proven secure down to one
  or more computational assumptions. This type of modular reasoning,
  which is not necessary when performing reductionistic proofs (even
  complex ones), is essential to permit tackling security proofs of
  high-level protocols.
\end{itemize}


\paragraph*{High-assurance and high-speed implementations.}
Prior work to connect computer-aided reductionist security proofs with
efficient implementations leverage advances in programming languages,
in particular verified compilers, i.e.\, compilers that come equipped
with a proof that they preserve the functional behavior of programs.
Almeida et al.~\cite{Almeida:2012,Almeida:2013,AlmeidaBBD16} have
previously explored this approach using \CompCert~\cite{Leroy06},
the most prominent example of verified optimizing compiler for C.
However, the goal of this line of research is to carry guarantees from
algorithmic descriptions to assembly implementations. 

In contrast, our implementation of Yao's protocol can be thought of as a secure virtual
machine for executing arbitrary computations. The challenge is therefore
dual: in addition to a verified implementation of this so-called
virtual machine, one needs to generate correct and efficient 
computation descriptions in a format
(in this case Boolean circuits) that can be executed in this
virtual computational platform. Generating such circuit representations by hand is
not realistic, and appropriate tool support is critical if widespread
practical adoption is the goal.  The requirement of end-to-end
verification further imposes that the transformation from a high-level
computation specification into circuit form must be itself verified.

The certified compiler that we have developed fills this gap from
both a cryptographic engineering and a high-assurance cryptography
performance. Indeed, when we include all the optimizations we have
implemented (some of which are not verified) the circuits we produce
are more efficient than alternative existing tools. 
Furthermore, our compiler and evaluation engines are integrated into
FRESCO~\cite{DamgardDNNT15}, a pre-existing framework for general 
multiparty computation deployment.
Moreover, and crucially, when we omit all unverified optimizations, one 
obtains a certified output, which is proven to be correct.

Highlights of our technical contributions at this level include:
\begin{itemize} 
\item the addition of a loop unrolling transformation to the \CompCert 
      middle-end, which combined with an improved value analysis
      allows \CompCert's constant propagation and deadcode elimination
      stages to produce, in a certified way, loop-free versions of
      input programs that are prone to circuit translation.

\item the formalization of a new intermediate language in \CompCert
      and a correspoding semantics preservation transformation
      that removes all branching from a (loop-free) input
      program and produces a straight-line program where instructions
      are conditionally evaluated.

\item the implementation of a certified domain-specific conversion to 
      Static Single Assignment (form) that exploits the structural
      properties imposed by the transformations above to obtain a
      streamlined semantics preservation proof relying on a restricted
      view of $\phi$-nodes that is more natural for circuit generation.


\item the formalization of a new target language that captures the semantics
      of Boolean circuits and permits stating and proving a semantics
      preservation theorem relating the input/output behaviour of an input C 
      program to that of a generated Boolean circuit.
 \end{itemize}

\heading{Structure of the paper}
In the remainder of this section we review related work.
Then, in Section~\ref{sec:ecproof} we describe the \EasyCrypt
formalization and the verified implementation of Yao's protocol.
In Section~\ref{sec:compcc} we present our certified Boolean
circuit compiler.
Benchmarking results and performance analysis are given in
Section~\ref{sec:bench} and we conclude the paper in 
Section~\ref{sec:conclusion}.

\begin{comment}
\heading{Perspectives}
Our work proposes a complete workflow of specification, validation,
and prototyping for building verified implementations of cryptographic
systems. What we envision here is a formal counterpart to existing
prototyping systems for cryptography~\cite{CHARM,SCAPI,CAO,CRYPTOL},
in terms of integrated environments in which cryptographers can
specify cryptographic algorithms, perform lightwheight validation of
implementations (e.g., using a type-checking mechanism), formalize
security proofs, and generate reasonably efficient implementations.
The workflow is by no means restricted to secure function evaluation,
and could be applied to other settings in the future.
\end{comment}

\section{Related work}

There have been significant advances towards the development of
computer-aided tools for cryptography. These tools fall into two
loosely related categories. The first category comprises a broad
spectrum of high-assurance tools, which use formal methods to deliver
strong correctness or security guarantees on models or (more seldom)
on implementations. The second category comprises many cryptographic
engineering tools, whose goal is to facilitate development and rapid
deployment of high-speed, high-quality software. We review some main
tools from both families; for sake of focus, we limit our review to 
prior work that either delivers verified security proofs in the
computational model, targets verified implementations, or is directly
relevant to secure multi-party computation. We refer the reader
to~\cite{Blanchet:2012} for a more extensive account of the use of
formal methods in (symbolic and computational) cryptography, and
to~\cite{DBLP:journals/iacr/Halevi05a,DBLP:conf/eurocrypt/BellareR06} 
for motivations on computer-aided
proofs.

% Real-world cryptography is an active field in cryptographic research.
% Its main goal is to address the current gap between provable security
% and cryptographic implementations by refining the security notions and
% algorithmic descriptions used in reductionistic security proofs. In
% spite of the additional complexity induced by more precise models,
% real-world cryptography, retains the elegance of provable security and
% has delivered valuable insights on the (in)security of cryptographic
% protocols.\footnote{develop and add references} However, real-world
% cryptography only makes an (important) first step towards verified
% implementations. It is also our firm belief that computer-aided tools
% will be instrumental in future evolutions of real-world cryptography.

\subsection{High-assurance cryptography}

\heading{General-purpose tools}
\CryptoVerif~\cite{Blanchet08CV} was among the first tools to support
security proofs of cryptographic constructions in the computational
model. \CryptoVerif uses probabilistic process algebra as modelling
language and leverages different notions of equivalence to support
proofs of equivalence, equivalence up to failure events, as well as
simple forms of hybrid arguments. \CryptoVerif has been used for
verifying primitives as well as protocols. More recently, Cad\'e and
Blanchet~\cite{CadeB13} have complemented the work on \CryptoVerif
with a mechanism to generate functional code from \CryptoVerif models,
and use it to generate a verified implementation of SSH.


Swamy et al.~\cite{fstar} build a type-based approach for verifying
Although \fstar\ supports direct reasoning about implementations,
programs written in the typed functional programming language \fstar.  Their
approach uses refinement types, a rich typing discipline which can be
seen as the counterpart for typed functional languages of the
assertions used in imperative languages. Their approach lacks support
for fine-grained modelling of probabilistic operations, but it uses
instead ideal primitives to reason compositionally about the security
of cryptographic protocols. Bhargavan et al.~\cite{mitls} use
\fstar\ to develop high-assurance implementations of TLS.
%
Although \fstar\ supports direct reasoning about implementations,
prior work does not address the problem of generating trustworthy
executables from verified implementations. Recent work~\cite{lowfstar}
addresses the problem, by targetting \CompCert as a back-end.
%
It is also noteworthy in our context that Rastogi, Swamy and
Hicks~\cite{RSH16} use \fstar\ as a host language for embedding {\sc
  Wysteria}, a domain-specific language for multi-party computation
(see below). However, and similarly to prior work on \fstar, their
embedding does not consider the generation of circuits from high-level
programs.

\VST (Verified Software Toolchain)~\cite{vst} is a general purpose
toolchain for reasoning about C programs. \VST harnesses the \CompCert
compiler to carry safety proofs from C to assembly programs.
Moreover, it provides an expressive program logic for reasoning about
C programs. One main application of \VST\ is cryptographic libraries;
in particular, Appel~\cite{Appel15} proves functional correctness of
SHA-256. In a companion effort, Beringer et al~\cite{BeringerPYA15}
connect \VST with \FCF (Foundational Cryptographic Framework) of
Petcher and Morrisett~\cite{PetcherM15}, in order to provide a
machine-checked proof of reductionist security for a realistic
implementation of HMAC.

\heading{High-assurance MPC}
There have been many works that develop or apply formal methods for
secure multi-party computation.

Backes et al.~\cite{BackesMM10} develop computationally sound methods
for protocols that use secure multi-party computation as a primitive
and analyse the SIMAP sugar-beet double auction protocol
However, they do not consider verified implementations.
%
{\sc Wysteria}~\cite{SP:RasHamHic14} is a new programming language
for mixed-mode multiparty computations. Its design is supported by a
rigorous pen-and-paper proof that typable programs do not leak
information in unintended ways. However, their guarantees are cast in
the setting of language-based security, rather than in the usual style
of provable security.
%
Dahl and Damg{\aa}rd~\cite{DD14} consider the symbolic analysis
of specifications extracted from two-party SFE protocol descriptions,
and show that the symbolic proofs of security are computationally
sound in the sense that they imply security in the standard UC model
for the original protocols.  
%
Pettai and Laud~\cite{PettaiL14} have developed a static analysis for
proving that {\sc Sharemind} applications are secure against active
adversaries. They show that programs accepted by their analysis
satisfy a simulation-based notion called black-box
security.%\footnote{further work?}

Fournet, Keller and Laporte~\cite{FournetKL16} propose a certified
compiler from C to quadratic arithmetic circuits (QAP) compatible with
the domain of SNARKs. However, the underlying cryptographic system
does not come with a verified implementation.
%\footnote{mention of
%  gepetto and pinocchio}
Carmer and Rosulek~\cite{CarmerR16} introduce a core language,
called \textsf{LiniCrypt}, for writing programs that perform linear
operations on a finite field and calls to random oracles. They prove
that the equivalence of \textsf{LiniCrypt} programs can be decided
efficiently, and leverage this result to build a tool that performs
SMT-based synthesis of garbled circuits.

\begin{comment}
\paragraph*{Other specialized high-assurance tools}
In addition to the above works, there have been a significant efforts
towards high-assurance cryptography.


Galois??? cryptol and other stuff

zk proofs

synthesis?
\end{comment}

\subsection{Cryptographic engineering of MPC protocols}

FRESCO~\cite{DamgardDNNT15} is a Java framework for efficient secure 
computation. In FRESCO, functions to be securely evaluated are described 
as circuits, and our certified compiler has been equipped with a backend
that permits seamless integration into this framework.
Run-time systems in FRESCO specify how circuits are evaluated, and are thus
highly dependent on the underlying protocol for secure computation that they
support. In addition to our formally verified implementation of Yao's
protocol, run-time systems written for FRESCO include support for 
several protocols for secure computation, including the TinyOT protocol 
by Nielsen \emph{et al.} for actively secure two-party
computation based on boolean circuits \cite{DBLP:conf/crypto/NielsenNOB12};
the Bedoza protocol by Bendlin \emph{et al.} for actively secure
  multi-party computation based on arithmetic circuits \cite{DBLP:conf/eurocrypt/BendlinDOZ11};
  and SPDZ protocol by Damg{\aa}rd \emph{et al.} for actively and covertly
  secure multi-party computation based on arithmetic circuits
  \cite{DBLP:conf/crypto/DamgardPSZ12,DBLP:conf/esorics/DamgardKLPSS13}.

Fairplay is a system originally developed to support two-party computation~\cite{MalkhiNPS04} and then extended to FairplayMP to support multiparty computation \cite{DBLP:conf/ccs/Ben-DavidNP08}: Fairplay implements a two party computation protocol in the manner suggested by Yao; FairplayMP is based on the Beaver-Micali-Rogaway protocol. Sharemind~\cite{ESORICS08:BLW08} is a secure service platform for data collection and analysis, employing a 3-party additive secret sharing scheme and provably secure protocols in the honest-but-curious security model with no more than one passively corrupted party. TASTY (Tool for Automating Secure Two-partY computations) is a tool suite addressing secure two-party computation in the semi-honest model~\cite{DBLP:conf/ccs/HeneckaKSSW10} whose main feature allows to compile and evaluate functions not only using garbled circuits, but also homomorphic encryption schemes, at the same time. 

Holzer \emph{et al.}~\cite{HFKV12} present a compiler that uses the
bounded model-checker CBMC to translate {\sf ANSI C} programs into
boolean circuits. The circuits can be used as inputs to the secure
computation framework of Huang et al.~\cite{HEKM11}. Their compiler
can be used as a front-end to our verified implementation of Yao's
protocol. However, our approach delivers higher assurance and,
if one activates all (even unverified) optimizations the circuits
generated by our compiler can offer better performace in comparison
with the current version of CBMC-GC (v0.9.3) as we show in 
Section~\ref{sec:bench}.

\begin{comment}
\subsection{Leftovers from previous lifes}
We concentrate on closely related work on the verification of
multi-party computation protocols and cryptographic software
implementations.  We refer to Appendix~\ref{app:related} for other
related work in cryptography..



Finally, Küsters \emph{et al.}~\cite{Kuesters+14} develop a framework
to verify cryptographic applications written in Java, and use it to
verify a non-trivial cloud storage system.  Another practical approach
to achieve high-confidence cryptographic implementations is to use
prototyping systems for
cryptography~\cite{CHARM,SCAPI,CAO,CRYPTOL}. In a series of works
starting from~\cite{AkinyeleGHP12}, Akinyele \emph{et al.} advocate the
convergence between such systems and verification tools; in
particular, their latest work~\cite{AkinyeleBGSS14} combines
\textsf{AutoBatch} and \EasyCrypt to generate verified implementations
of batch verifiers.
\end{comment}
