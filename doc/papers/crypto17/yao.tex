% !TeX root=sfecert.tex

\subsection{Final result and code extraction}

The main theorem in our \EasyCrypt formalization states the following,
for $\mathsf{c}$ an upper bound on the total number of wires in 
the circuit and $\mathsf{n}$ an upper bound on the number of input 
wires in the circuit.

\begin{theorem}[Security of the concrete SFE protocol]
\label{thm:main-theorem}
For all $\mathsf{SFE}$ adversary $\A$ against the \ec{Concrete}
SFE protocol, we construct an efficient simulator $\Sim$ and efficient
adversaries $\B_{\mathsf{DDH}}$, $\B_{\mathsf{ES}}$ and 
$\B^i_{\mathsf{PRF}}$ for $i \in [1..\mathsf{c}]$, such 
that the following holds:

\vspace{-6pt}
\small
\[
\mathsf{Adv}^{\mathsf{SFE}^1_{\Phi_{\sf topo}}}_{\mathsf{Concrete},\Sim}(\A) \leq
\mathsf{c} \cdot \varepsilon_{\mathsf{PRF}} + \varepsilon
\qquad \qquad
\mathsf{Adv}^{\mathsf{SFE}^2_{\Phi_{\sf topo}}}_{\mathsf{Concrete},\Sim}(\A) \leq
\varepsilon,
\]
\normalsize

\noindent
where $\varepsilon = \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{DDH}}(\B_{\mathsf{DDH}}) +
   \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{ES}}(\B_{\mathsf{ES}})$ and
   $\varepsilon_{\mathsf{PRF}}$ is an upper bound on the PRF advantage of
   adversaries $\B^i_{\mathsf{PRF}}$.
\end{theorem}

\begin{comment}
\subsection{Constructing a Secure SFE Protocol}\label{sec:yao}
%
We now explain how garbled circuits and oblivious transfer can be
combined to provide a general secure function evaluation protocol, and
formalize a generic security proof, parameterized by a secure garbling
scheme and a secure oblivious transfer protocol. We then instantiate
the abstract argument with our concrete oblivious transfer protocol
(Section~\ref{sec:ot}) and our concrete garbling scheme
(Section~\ref{sec:garbled}) and conclude.

\heading{Yao's SFE Construction} As discussed briefly in
Section~\ref{sec:ot}, we model SFE as a two-party protocol. We
consider the functionality to be evaluated, encoded as a circuit, as
part of Party 2's input and set up the leakage function to let it
become public. We denote with $\mathsf{Adv}^{\mathsf{SFE}^i}$ and
$\Ad^{\mathsf{SFE}}_i$ the instantiations of
$\mathsf{Adv}^{\mathsf{Prot}^i}$ and $\Ad^{\mathsf{Prot}}_i$ to the
SFE construction. We preface the definition of our generic SFE
construction with two named clones of the abstract garbling scheme and
oblivious transfer theories. This allows us to essentially
parameterize the SFE protocol with a garbling scheme and an oblivious
transfer protocol. Instantiating these parameters is simply done by
instantiating the \ec{Gb} and \ec{OT} theories with concrete
definitions and proofs.
%
In Figure~\ref{fig:yao}, we formalize a standard SFE construction
detailed informally in Appendix~\ref{app:garble}.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
clone Garble as Gb.
clone OT as OT.

clone Protocol as SFE with
  type rand1 = OT.rand1,
  type input1 = bool array,
  type output1 = Gb.output,
  type leak1 = int,
  type rand2 = OT.rand2 * Gb.rand,
  type input2 = Gb.func * bool array,
  type output2 = unit,
  type leak2 = Gb.func * int,
  op f i$_1$ i$_2$ = let (c,i$_2$) = i$_2$ in Gb.eval c (i$_1$ || i$_2$),(),
  type conv = (Gb.funcG * token array * Gb.outputK) * OT.conv,
  op validInputs (i$_1$:input1) (i$_2$:input2) =
    0 < length i$_1$ /\ Gb.validInputs (fst i$_2$) (i$_1$ || snd i$_2$),
  op prot (i$_1$:input1) (r$_1$:rand1) (i$_2$:input2) (r$_2$:rand2) =
    let (c,i$_2$) = i$_2$ in
    let fG = Gb.funG c (snd r$_2$) in
    let oK = Gb.outputK c (snd r$_2$) in
    let iK = Gb.inputK c (snd r$_2$) in
    let iK1 = (take (length i$_1$) iK) in
    let (ot_conv, (t$_1$,_)) = OT.prot i$_1$ r$_1$ iK1 (fst r$_2$) in
    let GI$_2$ = Gb.encode (drop (length i$_1$) iK) i$_2$ in
    (((fG,GI$_2$,oK),ot_conv), (Gb.decode oK (Gb.evalG fG (t$_1$ || GI$_2$)),())).
\end{lstlisting}
\vspace{-1em}
\caption{Abstract SFE Construction.\label{fig:yao}}
\end{figure}

\begin{theorem}[Abstract SFE security]
\label{thm:SFE-security}
For any oblivious transfer protocol \ec{OT} and any garbling scheme
\ec{Gb}, let \ec{SFE}$_a$ be the SFE protocol built using Yao's
construction from \ec{OT} and \ec{Gb}.  For all randomness generators
$\mathsf{R}^G$, $\mathsf{R}^O_1$ and $\mathsf{R}^O_2$, we can
construct SFE randomness generators
$\mathsf{R}^{\mathsf{SFE}}_1$ and
$\mathsf{R}^{\mathsf{SFE}}_2$ such that, for all SFE adversary
$\A = (\A_1,\A_2)$ implementing type $\Ad^{\mathsf{SFE}} =
(\Ad^{\mathsf{SFE}}_1,\Ad^{\mathsf{SFE}}_2)$, OT simulator $\Sim^O$
and garbling simulator $\Sim^G$, we can construct efficient
adversaries $\A^O = (\A^O_1,\A^O_2)$ of type $\Ad^{\mathsf{OT}} =
(\Ad^{\mathsf{OT}}_1,\Ad^{\mathsf{SFE}}_2)$ and $\A^G$ of type
$\Ad^{\mathsf{Gb}}$ and an efficient simulator $\Sim$, such that
the following inequalities hold.

\[
\mathsf{Adv}^{\mathsf{SFE}^1_\Phi}_{\mathsf{SFE}_a,\Sim}(\A_1) \leq
\mathsf{Adv}^{\mathsf{OT}^1_\Phi}_{\mathsf{OT},\mathsf{S}^O}(\A^O_1) +
\mathsf{Adv}^{\SIMCPA_\Phi}_{\mathsf{Gb},\mathsf{S}^G}(\A^G)
\]
\[
\mathsf{Adv}^{\mathsf{SFE}^2_\Phi}_{\mathsf{SFE}_a,\mathsf{S}}(\A_2) \leq
\mathsf{Adv}^{\mathsf{OT}^2_\Phi}_{\mathsf{OT},\mathsf{S}^O}(\A^O_2)
\]

\end{theorem}
\begin{proof}[Sketch]
Since no additional randomness is used by the SFE protocol, the SFE
randomness generators are trivially constructed from the randomness
generators for OT and garbling. 
The proof then follows the argument sketched in~\cite{DBLP:conf/ccs/BellareHR12}.
From the adversary $\A$, we can easily
construct $\A^O$ and $\A^G$ by moving some of the code from the SFE
protocol into the adversary. The security assumptions on the oblivious
transfer protocol and garbling scheme can then be used to build two
simulators $\Sim^O$ and $\Sim^G$. In turn, these can be combined into
a valid simulator $\Sim$ for the full SFE protocol.
\end{proof}

\heading{A concrete SFE protocol} Finally, we instantiate \ec{OT} with
the concrete oblivious transfer protocol we proved secure in
Section~\ref{sec:ot} and \ec{Gb} with the concrete garbling scheme we
proved secure in Section~\ref{sec:garbled} in the construction.
%
The security proof for this concrete construction immediately follows
from Theorems~\ref{thm:SFE-security}, \ref{thm:SomeOT-SIMCPA}
and~\ref{thm:SomeGarble-SIMCPA}.
%
However, we take this opportunity to implement some
instantiation-specific optimizations across abstraction boundaries and
translate high-level programming constructs like maps and higher-order
functions into more efficient data structures such as arrays. We also
instantiate the constants \ec{n} and \ec{c} as described further in
Section~\ref{sec:evaluation} and discharge any axioms affecting
them. A separate proof that our efficient implementation is perfectly
equivalent to the one on which we performed the security proof yields
the final security theorem.

\begin{theorem}[Security of the concrete SFE protocol]
\label{thm:main-theorem}
For all $\mathsf{SFE}$ adversary $\A$ against the \ec{Concrete}
SFE protocol, we construct an efficient simulator $\Sim$ and efficient
adversaries $\B_{\mathsf{DKC}}$, $\B_{\mathsf{DDH}}$ and $\B_{\mathsf{ES}}$,
such that the following inequalities hold:
\[
\mathsf{Adv}^{\mathsf{SFE}^1_{\Phi_{\sf topo}}}_{\mathsf{Concrete},\Sim}(\A) \leq
(\mathsf{c} + 1) \cdot \mathsf{Adv}^{\mathsf{DKC}}(\B_{\mathsf{DKC}}) + \varepsilon
\]
\[
\mathsf{Adv}^{\mathsf{SFE}^2_{\Phi_{\sf topo}}}_{\mathsf{Concrete},\Sim}(\A) \leq
\varepsilon,
\]
where $\varepsilon = \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{DDH}}(\B_{\mathsf{DDH}}) +
   \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{ES}}(\B_{\mathsf{ES}})$.

\end{theorem}
\begin{proof}[Sketch]
\label{thm:concrete-security}
By functional equivalence of the \ec{Concrete} protocol and
\ec{SFE(SomeOT,SomeGarble)} (called \ec{SFE}$_c$ below), we prove
$\mathsf{Adv}^{\mathsf{SFE}^1_{\Phi_{\sf
      topo}}}_{\mathsf{Concrete},\Sim}(\A) =
\mathsf{Adv}^{\mathsf{SFE}^1_{\Phi_{\sf
      topo}}}_{\mathsf{SFE}_c,\Sim}(\A)$ and it suffices to bound the
latter. This can be done by applying Theorem~\ref{thm:SFE-security}
and using the adversaries constructed in
Theorems~\ref{thm:SomeOT-SIMCPA} and~\ref{thm:SomeGarble-SIMCPA} along
with the corresponding bounds to conclude.
\end{proof}
\end{comment}
