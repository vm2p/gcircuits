% !TeX root=sfecert.tex

\heading{Extraction and Micro Benchmarks}
Our verified implementation of Yao's protocol has been obtained via the
extraction mechanism included in the more recent version of 
\EasyCrypt.
The only exceptions to this are the low-level operations left 
abstract in the formalisation, namely:
\begin{itemize}
 \item abstract core libraries such as cyclic
   algebraic structures, a PRF relying on AES, or the
   entropy-smoothing hash of \ec{SomeOT}. These are
   implemented using the \texttt{CryptoKit}
   library.\footnote{See \url{http://forge.ocamlcore.org/projects/cryptokit/}}
 \item a front-end that parses circuits and runs the extracted
   SFE code (we also instrumented this front-end to perform the
   time measurements presented in this section).
\end{itemize}

We fix the bound $\mathsf{c}$ on circuit sizes to be the largest
\OCaml integer ($2^{k - 1}\mbox{--}\,1$ on a $k$-bit machine), allowing us to
represent circuits without having to use arbitrary precision
arithmetic whilst remaining large enough to encode all practical
circuits. We use this same value to instantiate \ec{n}.
%
Inputs are generated randomly using \OCaml's \textsf{Rand} module, and
the cryptographic randomness is generated using \texttt{CryptoKit}'s RNG.

Our preliminary results show that, whilst being slower 
than optimized implementations of SFE~\cite{HEKM11,BHKR13},
the performance of the \emph{extracted} program
is compatible with real-world deployment, providing some evidence
that the (unavoidable) overhead implied by our formal verification
and code extraction approach is not prohibitive.
%
We now present our experimental results in details.%
In addition to the overall execution time of the SFE protocol and the
splitting of the processing load between the two involved parties, we
also measure various speed parameters that permit determining the
weight of the underlying components: the time spent in executing the
OT protocol, and the garbling and evaluation speeds for the garbling
scheme.
%
%% In all tests, party {\sf P1} holds input $x_1$, whereas party {\sf P2}
%% holds input $x_2$. Consistently with our description in the previous
%% sections, party {\sf P2} initiates the protocol and performs the
%% garbling operation. Party {\sf P1} obtains the encoding of $x_1$ using
%% the OT protocol and performs the evaluation operation.
%
Our measured execution times do not include serialisation and
communication overheads (which are out of the scope of this work), nor
do they include the time to sample the randomness (which
can be pre-generated).
%
We run our experiments on an x86-64 Intel Core 2 Duo clocked at a
modest 1.86 GHz with a 6MB L2 cache. The extracted code and parser are
compiled with {\sf ocamlopt} version 4.00.1. The tests are run in isolation, using
the \OCaml {\sf Sys.time} operator for time readings.  We run tests in
batches of 100 runs each, noting the median of the times recorded in
the runs.

Our measurements are conducted
over circuits made publicly available by the cryptography group at the
University of Bristol,
\footnote{\url{http://www.cs.bris.ac.uk/Research/CryptographySecurity/MPC/}}
precisely for the purpose of enabling the testing and benchmarking of
multiparty-computation and homomorphic encryption implementations. A
simple conversion of the circuit format is carried out to ensure that
the representation matches the conventions adopted in the
formalisation.

%% Moving this table here to avoid leaking into the references
\begin{table*}[t]
\scriptsize
\caption{Execution times (milliseconds): total ({\sf TTime}), {\sf P2}
  stage $1$ garbling ({\sf P2 S1 GT}), {\sf P2} stage $1$ OT
  ({\sf P2 S1 OT}), {\sf P1} stage $1$ OT ({\sf P1 S1 OT}), {\sf
    P2} stage $2$ OT ({\sf P2 S2 OT}), {\sf P1} stage $2$ OT
  ({\sf P2 S1 OT}) and {\sf P1} stage $2$ evaluation ({\sf P1 S2
    ET}).}
\label{tb:results}
\centering
\vspace{-6pt}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline
{\bf Circuit}	& 	{\bf NGates}	& 	{\bf TTime}	& 	{\bf P2 S1 GT}	& 	{\bf P2 S1 OT}	& 	{\bf P1 S1 OT}	& 	{\bf P2 S2 OT}	& 	{\bf P1 S2 OT}	& 	{\bf P1 S2 ET}\\
\hline
{\sf COMP32}	& 	301	& 	253	& 	2	& 	48	& 	50	& 	102	& 	50	& 	1\\
{\sf ADD32}	& 	408	& 	251	& 	3	& 	48	& 	49	& 	101	& 	49	& 	1\\
{\sf ADD64}	& 	824	& 	494	& 	6	& 	95	& 	97	& 	197	& 	97	& 	3\\
{\sf MUL32}	& 	12438	& 	356	& 	95	& 	47	& 	47	& 	98	& 	48	& 	22\\
{\sf AES}	& 	33744	& 	1301	& 	248	& 	192	& 	199	& 	403	& 	200	& 	60\\
{\sf SHA1}	& 	106761	& 	2825	& 	803	& 	366	& 	367	& 	744	& 	366	& 	180\\			\hline
\end{tabular}
\vspace{-12pt}
\end{table*}

A subset of our results are presented in Table~\ref{tb:results}, for
circuits {\sf COMP32} (32-bit signed number less-than comparison),
{\sf ADD32} (32-bit number addition), {\sf ADD64} (64-bit number
addition), {\sf MUL32} (32-bit number multiplication), {\sf AES} (AES
block cipher), {\sf SHA1} (SHA-1 hash algorithm). The semantics of the
evaluation of the arithmetic circuits is that each party holds one of
the operands. In the {\sf AES} evaluation we have that {\sf P1} holds
the $128$-bit input block, whereas {\sf P2} holds the $128$-bit secret
key. Finally, in the {\sf SHA1} example we model the (perhaps
artificial) scenario where each party holds half of a $512$-bit input
string.

We present the number of gates for each circuit as well as the
execution times in milliseconds.  A rough comparison with results
presented in, for example~\cite{HEKM11}, where an execution of the AES
circuit takes roughly 1.6 seconds (albeit including communications
overhead and randomness generation time) allows us to conclude that
real-world applications are within the reach of the implementations
generated using the approach described in this paper. Furthermore, 
additional optimisation effort can lead to significant
performance gains, e.g., by resorting to hardware support for
low-level cryptographic implementations as in~\cite{BHKR13}, or
implementing garbled-circuit optimisations such as those allowed by
XOR gates~\cite{KS08}.

\todo[inline]{Updade This}
