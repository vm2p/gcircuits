%!TeX Root=sfecert.tex

\heading{Other highlights of the formalization}
The rest of the \EasyCrypt formalization follows in the same style
as what was presented above for oblivious transfer. It includes 
formalization and security proofs for a Dual Key Cipher from a
pseudorandom function; the highly challenging hybrid argument 
that is used to reduce the security of our garbling scheme to 
the security of the underlying Dual Key Cipher; abstract proofs 
that permit going from the oblivious transfer primitive and 
garbling scheme primitive to SFE.
The resulting specification of Yao's protocol for which we construct
this proof of security is given in Figure~\ref{fig:sfe}.
Note that, at this point, the garbling scheme can be instantiated
with any secure DKC, and the protocol can be instantiated with
any secure garbling scheme.

\begin{figure}[t]
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type topo = int * int * int * int array * int array.
type 'a circuit = topo * (int * 'a * 'a,'a) map.

type leak = topo.

type input, output = bool array.
type func = bool circuit.

type funcG = token circuit.
type inputG, outputG = token array.
type rand, inputK = ((int * bool),token) map.




op evalG f i =
  let ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}$) = f in
  let evalGate = lambda g x$_1$ x$_2$,
    let x$_{1,0}$ = lsb x$_1$ and x$_{2,0}$ = lsb x$_2$ in
    D (tweak g x$_{1,0}$ x$_{2,0}$) x$_1$ x$_2$ $\mathbb{G}$[g,x$_{1,0}$,x$_{2,0}$] in
  (* extend the array with q zeroes  *)
  let wires = extend i q in 
  (* decrypt wires *)
  let wires = map (lambda g, evalGate g $\mathbb{A}$[g] $\mathbb{B}$[g]) 
                  wires in 
  sub wires (n + q - m) m.

op encode iK x = init (length x) (lambda k, iK[k,x.[k]]).

op inputK (f:func) (r:((int * bool),token) map) =
  let ((n,_,_,_,_),_) = f in 
      filter (lambda x y, 0 <=  fst x < n) r.

\end{lstlisting}
\end{minipage}
\ 
\begin{minipage}{.52\textwidth}
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
op funcG (f:func) (r:rand) =
  let ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}$) = f in
  for (g,x$_\mathsf{a}$,x$_\mathsf{b}$) $\in$ [0..q] * bool * bool
    let a = $\mathbb{A}$[g] and b = $\mathbb{B}$[g] in
    let t$_\mathsf{a}$ = r[a,x$_\mathsf{a}$] and t$_\mathsf{b}$ = r[b,x$_\mathsf{b}$] in
    $\mathbb{G}'$[g,t$_\mathsf{a}$,t$_\mathsf{b}$] = E (tweak g t$_\mathsf{a}$ t$_\mathsf{b}$) t$_\mathsf{a}$ t$_\mathsf{b}$ r[g,$\mathbb{G}$[g,x$_\mathsf{a}$,x$_\mathsf{b}$]]
                    ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}'$).

clone Protocol as SFE with
  type rand1 = OT.rand1,
  type input1 = bool array,
  type output1 = Gb.output,
  type leak1 = int,
  type rand2 = OT.rand2 * Gb.rand,
  type input2 = Gb.func * bool array,
  type output2 = unit,
  type leak2 = Gb.func * int,
  type conv = (Gb.funcG * token array * Gb.outputK) 
       * OT.conv,
  op f i$_1$ i$_2$ = let (c,i$_2$) = i$_2$ in Gb.eval c (i$_1$ || i$_2$),(),
  op validInputs (i$_1$:input1) (i$_2$:input2) = 0 < length i$_1$ 
        /\ Gb.validInputs (fst i$_2$) (i$_1$ || snd i$_2$),
  op prot (i$_1$:input1) (r$_1$:rand1) (i$_2$:input2) (r$_2$:rand2) =
    let (c,i$_2$) = i$_2$ in
    let fG = Gb.funG c (snd r$_2$) in
    let oK = Gb.outputK c (snd r$_2$) in
    let iK = Gb.inputK c (snd r$_2$) in
    let iK1 = (take (length i$_1$) iK) in
    let (ot_conv, (t$_1$,_)) = OT.prot i$_1$ r$_1$ iK1 (fst r$_2$) in
    let GI$_2$ = Gb.encode (drop (length i$_1$) iK) i$_2$ in
        (((fG,GI$_2$,oK),ot_conv), 
        (Gb.decode oK (Gb.evalG fG (t$_1$ || GI$_2$)),())).
\end{lstlisting}
\end{minipage}
\vspace{-1em}
\caption{Garbling Scheme and Yao's protocol.}
\label{fig:sfe}
\vspace{-18pt}
\end{figure}

The remainder of the \EasyCrypt formalization consists of
instantiating all theorems to derive a security result for
the full specification. 
At this point we also take the opportunity to implement some
instantiation-specific optimizations across abstraction boundaries and
translate high-level programming constructs like maps and higher-order
functions into more efficient data structures such as arrays. 
A separate proof that our efficient implementation is perfectly
equivalent to the one on which we performed the security proof yields
the final security theorem, which we present in the next subsection.
Overall, the $\EasyCrypt$ development comprises
XXX lines of code and, to the best of our knowledge, it is the 
largest project ever to be developed in the formal verification of 
cryptographic security proofs in the standard model. 
In the next section we present the top-level result and describe
the resulting verified implementation of Yao's protocol.

\begin{comment}
\subsection{Garbling schemes}\label{sec:garbled}
Garbling schemes~\cite{DBLP:conf/ccs/BellareHR12}
(Figure~\ref{fig:garble}, top) are operators on \emph{functionalities} of
type \ec{func}.
%
Such functionalities can be evaluated on some input using an \ec{eval}
operator.  In addition, a functionality can be \emph{garbled} using
three operators (all of which may consume randomness). \ec{funG}
produces the garbled functionality, \ec{inputK} produces an
input-encoding key, and \ec{outputK} produces an output-encoding key.
%
The garbled evaluation \ec{evalG} takes a garbled functionality and
some encoded input and produces the corresponding encoded output. The
input-encoding and output-decoding functions are self-explanatory.

\begin{wrapfigure}{R}{.6\textwidth}
\vspace{-0.8cm}
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
type func, input, output.
op eval : func -> input -> output.
op valid: func -> input -> bool.

type rand, funcG, inputK, outputK.
op funcG  : func -> rand -> funcG.
op inputK : func -> rand -> inputK.
op outputK: func -> rand -> outputK.

type inputG, outputG.
op evalG : funcG -> inputG -> outputG.
op encode: inputK -> input -> inputG.
op decode: outputK -> outputG -> output.
\end{lstlisting}

\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type topo = int * int * int * int array * int array.
type 'a circuit = topo * (int * 'a * 'a,'a) map.

type leak = topo.

type input, output = bool array.
type func = bool circuit.

type funcG = token circuit.
type inputG, outputG = token array.
op evalG f i =
  let ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}$) = f in
  let evalGate = lambda g x$_1$ x$_2$,
    let x$_{1,0}$ = lsb x$_1$ and x$_{2,0}$ = lsb x$_2$ in
    D (tweak g x$_{1,0}$ x$_{2,0}$) x$_1$ x$_2$ $\mathbb{G}$[g,x$_{1,0}$,x$_{2,0}$] in
  (* extend the array with q zeroes  *)
  let wires = extend i q in 
  (* decrypt wires *)
  let wires = map (lambda g, evalGate g $\mathbb{A}$[g] $\mathbb{B}$[g]) wires in 
  sub wires (n + q - m) m.

type rand, inputK = ((int * bool),token) map.
op encode iK x = init (length x) (lambda k, iK[k,x.[k]]).

op inputK (f:func) (r:((int * bool),token) map) =
  let ((n,_,_,_,_),_) = f in filter (lambda x y, 0 <=  fst x < n) r.

op funcG (f:func) (r:rand) =
  let ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}$) = f in
  for (g,x$_\mathsf{a}$,x$_\mathsf{b}$) $\in$ [0..q] * bool * bool
    let a = $\mathbb{A}$[g] and b = $\mathbb{B}$[g] in
    let t$_\mathsf{a}$ = r[a,x$_\mathsf{a}$] and t$_\mathsf{b}$ = r[b,x$_\mathsf{b}$] in
    $\widetilde{\mathbb{G}}$[g,t$_\mathsf{a}$,t$_\mathsf{b}$] = E (tweak g t$_\mathsf{a}$ t$_\mathsf{b}$) t$_\mathsf{a}$ t$_\mathsf{b}$ r[g,$\mathbb{G}$[g,x$_\mathsf{a}$,x$_\mathsf{b}$]]
  ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\widetilde{\mathbb{G}}$).
\end{lstlisting}

\vspace{-1em}
\caption{Abstract Garbling Scheme (top) and an Intantiation \ec{SomeGarble} (bottom).}
\label{fig:garble}
\vspace{-.8cm}
\end{wrapfigure}

In practice, we are interested in garbling functionalities encoded as
Boolean circuits and therefore fix the \ec{func} and \ec{input} types
and the \ec{eval} function. Circuits themselves are represented by
their topology and their gates.
%
A topology is a tuple $(n,m,q,\mathbb{A},\mathbb{B})$, where $n$ is
the number of input wires, $m$ is the number of output wires, $q$ is
the number of gates, and $\mathbb{A}$ and $\mathbb{B}$ map to each
gate its first and second input wire respectively.
%
A circuit's gates are modelled as a map $\mathbb{G}$ associating
output values to a triple containing a gate number and the values of
the input wires.  Gates are modelled polymorphically, allowing us to
use the same notion of circuit for Boolean circuits and their garbled
counterparts.
%
We only consider \emph{projective schemes}~\cite{DBLP:conf/ccs/BellareHR12}, 
where Boolean values
on each wire are encoded using a fixed-length random \emph{token}.
% and the values of output wires can be decoded by simply taking the
%token's least significant bit.
This fixes the type \ec{funcG} of garbling schemes, and the
\ec{outputK} and \ec{decode} operators.

Following the \textsf{Garble1} construction of Bellare et
al.~\cite{DBLP:conf/ccs/BellareHR12}, we construct our garbling scheme
using a variant of Yao's garbled circuits based on a pseudorandom
function, via an intermediate Dual-Key Cipher (DKC)
primitive. We denote the DKC encryption with \ec{E}, and DKC
decryption with \ec{D}. Both take four tokens as argument: a tweak
that we generate with an injective function and use as unique IV, two
keys, and a plaintext (or ciphertext).
%
Some intuition about the construction is given in
Appendix~\ref{app:garble}.
%
We give functional specifications to the garbling algorithms in
Figure~\ref{fig:garble} (bottom). For clarity, we denote
functional \ec{fold}s using stateful \ec{for} loops.

\heading{Security of Garbling Schemes} The privacy property of
garbling schemes required by Yao's SFE protocol is more conveniently
captured using a simulation-based definition.  Like the security
notions for protocols, the privacy definition for garbling schemes is
parameterized by a leakage function upper-bounding the information
about the functionality that may be leaked to the adversary. (We
consider only schemes that leak at most the topology of the circuit.)

\begin{figure}[t]
\begin{minipage}{.5\textwidth}
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type leak.
op $\Phi$: func -> leak.

module type Sim = {
  fun sim(x: output, l: leak): funcG * inputG
}.

module type $\Ad^\mathsf{Gb}$ = {
  fun choose(): func * input
  fun distinguish(F: funcG, X: inputG) : bool
}.

module SIM(R: Rand, $\Sim$: Sim, $\A$: $\Ad^\mathsf{Gb}$) = {
  fun main() : bool = {
    var real, adv, f, x, F, X;
    (f,x) = $\A$.gen_query();
    real =$ {0,1};
    if (!valid f x)
      adv =$ {0,1};
    else {
      if (real) {
        r = R.gen($\Phi$ f);
        F = funcG f r;
        X = encode (inputK f r) x;
      } else {
        (F,X) = $\Sim$.sim(f(x),$\Phi$ f);
      }
      adv = $\A$.dist(F,X);
    }
    return (adv = real);
  }
}.
\end{lstlisting}
\end{minipage}
\ \!
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[language=easycrypt,style=easycrypt-pretty,xleftmargin=0pt,xrightmargin=0pt,mathescape]











module type $\Ad^{\mathsf{IND}}$ = {
  fun choose(): ptxt * ptxt
  fun distinguish(c:ctxt): bool
}.

module IND (R:Rand, $\A$:$\Ad^{\mathsf{IND}}$) = {
  fun main(): bool = {
    var p$_0$, p$_1$, p, c, b, b', ret, r;
    (p$_0$,p$_1$) = $\A$.choose();
    if (valid p$_0$ /\ valid p$_1$ /\ $\Phi$ p$_0$ = $\Phi$ p$_1$) {
      b =${0,1};
      p = if b then p$_1$ else p$_0$;
      r = R.gen(|p|);
      c = enc p r;
      b' = $\A$.distinguish(c);
      ret = (b = adv);
    }
    else ret =${0,1};
    return ret;
  }
}.
\end{lstlisting}
\end{minipage}
\vspace{-1em}
\caption{Security for Garbling Schemes. Simulation-based (left)
and Indistinguishability-based (right).}
\label{fig:garblesec}
\vspace{-12pt}
\end{figure}

Consider efficient non-adaptive adversaries that provide two
procedures:
i. \ec{choose} takes no input and outputs a pair \ec{(f,x)}
  composed of a functionality and some input to that functionality;
ii. on input a garbled circuit and garbled input pair \ec{(F,X)},
  \ec{distinguish} outputs a bit $b$ representing the adversary's
  guess as to whether he is interacting with the real or ideal
  functionality.
%
Formally, we define the $\SIMCPA_\Phi$ advantage of an adversary $\A$
of type $\Ad^\mathsf{Gb}$ against garbling scheme $\Gb$ =
\ec{(funcG,inputK,outputK)} and simulator $\Sim$ as

\[
\mathsf{Adv}^{\SIMCPA_\Phi}_{\Gb,\mathsf{R},\mathsf{S}}(\A) = 
\left| 2 \cdot \Pr[\mathsf{SIM}(\mathsf{R},\mathsf{S},\A): \result] - 1\right|.
\]

A garbling scheme $\Gb$ using randomness generator \ec{R} is
$\SIMCPA_\Phi$-secure if, for all adversary $\A$ of type
$\Ad^\mathsf{Gb}$, there exists an efficient simulator $\Sim$ of type
\ec{Sim} such that
$\mathsf{Adv}^{\SIMCPA_\Phi}_{\Gb,\mathsf{R},\mathsf{S}}(\A)$ is
small.

Following~\cite{DBLP:conf/ccs/BellareHR12}, we establish simulation-based
security via a general result that leverages a more convenient 
indistinguishability-based security notion denoted
$\INDCPA_{\Phi_{\mathsf{topo}}}$: we formalize a general theorem
stating that, under certain restrictions on the leakage function $\Phi$,
$\INDCPA_\Phi$-security implies $\SIMCPA_\Phi$ security. This result is
discussed below as Lemma~\ref{lem:ind-implies-sim}.

\heading{A modular proof}
The general lemma stating that $\INDCPA$-security implies
$\SIMCPA$-security is easily proved in a very abstract model, and is then
as easily instantiated to our concrete garbling setting. We describe the
abstract setting to illustrate the proof methodology enabled by \EasyCrypt
modules on this easy example.

The module shown in Figure~\ref{fig:garblesec} is a slight
generalization of the standard $\INDCPA$ security notions for
symmetric encryption, where some abstract leakage operator $\Phi$
replaces the more usual check that the two adversary-provided
plaintexts have the same length.We formally prove an abstract result
that is applicable to any circumstances where
indistinguishability-based and simulation-based notions of security
interact.
%
We define the $\INDCPA$ advantage of an adversary $\A$ of type
$\Ad^{\mathsf{IND}}$ against the encryption operator \ec{enc} using
randomness generator \ec{R} with leakage $\Phi$ as
\[
\mathsf{Adv}^{\INDCPA_\Phi}_{\mathsf{enc},\mathsf{R}}(\A) =
\left|2 \cdot \mbox{\ec[basicstyle=\small\sffamily,literate={A}{{$\A$}}{1}]{Pr[Game_IND(R,A): res]}} - 1\right|
\]
where \ec{R} is the randomness generator used in the concrete theory.

In the rest of this subsection, we use the following notion of
invertibility.  A leakage function $\Phi$ on plaintexts (when we
instantiate this notion on garbling schemes these plaintexts are
circuits and their inputs) is \emph{efficiently invertible} if there
exists an efficient algorithm that, given the leakage corresponding to
a given plaintext, can find a plaintext consistent with that leakage.

\begin{lemma}[$\INDCPA$-security implies $\SIMCPA$-security]%
\label{lem:ind-implies-sim}
If $\Phi$ is efficiently invertible, then for every efficient
$\SIMCPA$ adversary $\A$ of type $\Ad^{\mathsf{Gb}}$, one can build an
efficient $\INDCPA$ adversary $\B$ and an efficient simulator
$\Sim$ such that
$$\mathsf{Adv}^{\SIMCPA_\Phi}_{\mathsf{enc},\Sim}(\A) =
\mathsf{Adv}^{\INDCPA_\Phi}_{\mathsf{enc}}(\B).$$
\end{lemma}
\begin{proof}[Sketch]
Using the inverter for $\Phi$, $\B$ computes a second plaintext from
the leakage of the one provided by $\A$ and uses this as the second
part of her query in the $\INDCPA$ game. Similarly, simulator $\Sim$
generates a simulated view by taking the leakage it receives and
computing a plaintext consistent with it using the $\Phi$-inverter.
The proof consists in establishing that $\A$ is called by $\B$ in a
way that coincides with the $\SIMCPA$ experiment when $\Sim$ is used
in the ideal world, and is performed by code motion.
\end{proof}

%{\color{red} {\bf Francois:} Discuss the application of the lemma... This may require us to anticipate lemma 2 *and* the SFE security lemma, so we can show the interplay between adversaries and how we can easily compose them into towers of functor applications. We should still discuss this here so that we can (informally) discuss the memory restrictions on a simple example before diving into huge stacks of compositions. The restrictions are hugely important to the soundness of the EasyCrypt approach to composition and anyone familiar with UC frameworks will no doubt be confused if we do not mention it.}

\heading{Finishing the proof}
We reduce the $\INDCPA_{\Phi{\sf topo}}$-security of \ec{SomeGarble}
to the $\mathsf{DKC}$-security of the underlying DKC primitive
(see~\cite{DBLP:conf/ccs/BellareHR12}).
% {\color{red} DKC security should make it into an appendix.} (no space)).
In the lemma statement, \ec{c} is an abstract upper bound on the size
of circuits (in number of gates) that are considered valid. The lemma
holds for all values of \ec{c} that can be encoded in a token minus
two bits.

\begin{lemma}[\textsf{SomeGarble} is $\INDCPA_{\Phi_{\sf topo}}$-secure]
\label{lem:SomeGarble-INDCPA}
For every efficient $\INDCPA$ adversary $\A$ of type
$\Ad^{\mathsf{Gb-IND}}$, we can construct a efficient $\mathsf{DKC}$
adversary $\B$ such that
\begin{align*}
\mathsf{Adv}^{\INDCPA_{\Phi_{\sf topo}}}_{\mathsf{SomeGarble}}&(\A) \leq
(\mathsf{c} + 1) \cdot
\mathsf{Adv}^{\mathsf{DKC}}_{\mathsf{SomeGarble}}(\B).
\end{align*}
\end{lemma}
\begin{proof}[Sketch]
The constructed adversary $\B$, to simulate the garbling scheme's oracle,
samples a wire $\ell_0$ which is used as pivot in a hybrid construction where:
\begin{inparaenum}[i.]
\item all tokens that are revealed by the garbled evaluation on the
  adversary-chosen inputs are garbled normally, using the real DKC scheme;
  otherwise
\item all tokens for wires less than $\ell_0$ are garbled using encryptions of
  random tokens (instead of the real tokens representing the gates' outputs);
\item tokens for wire $\ell_0$ uses the real-or-random DKC oracle; and
\item all tokens for wires greater than $\ell_0$ are garbled normally.
\end{inparaenum}

Here again, the generic hybrid argument (Figure~\ref{fig:hybrid}) can
be instantiated and applied without having to be proved again,
yielding a reduction to an adaptive DKC adversary. A further reduction
allows us to then build a non-adaptive DKC adversary, since all DKC
queries made by $\B$ are in fact random and independent.
\end{proof}

From Lemmas~\ref{lem:ind-implies-sim} and~\ref{lem:SomeGarble-INDCPA}, we can
conclude with a security theorem for our garbling scheme.

\begin{theorem}[\textsf{SomeGarble} is $\SIMCPA_{\Phi_{\sf topo}}$-secure]
\label{thm:SomeGarble-SIMCPA}
For every $\SIMCPA$ adversary $\A$ that implements
$\Ad^{\mathsf{Gb}}$, one can construct an efficient simulator $\Sim$
and a DKC adversary $\B$ such that
\begin{align*}
\mathsf{Adv}&^{\SIMCPA_{\Phi_{\sf topo}}}_{\mathsf{SomeGarble},\Sim}(\A) \leq
(\mathsf{c} + 1) \cdot \mathsf{Adv}^{\mathsf{DKC}}_{\mathsf{SomeGarble}}(\B).
\end{align*}
\end{theorem}
\begin{proof}[Sketch]
Lemma~\ref{lem:ind-implies-sim} allows us to construct from $\A$ the
simulator \ec{S} and an \INDCPA adversary $\mathcal{C}$. From
$\mathcal{C}$, Lemma~\ref{lem:SomeGarble-INDCPA} allows us to
construct $\B$ and conclude.
\end{proof}
\end{comment}
