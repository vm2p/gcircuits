%!Tex Root=sfecert.tex

\section{\label{sec:compcc}%
  Certified Boolean circuit compiler}

In this section we describe a new certified compiler that can convert 
(a large subset of) C programs into Boolean circuit descriptions. 
This is a self-contained, standalone tool that can be used in arbitrary 
contexts where computation is specified by Boolean circuits.

By a certified compiler we mean a compiler that is coupled with a
formal proof asserting that the semantics of the source program is
preserved through the compilation process (i.e. whenever the source
program exhibits a well-defined behaviour, the behaviour of the target
program matches it). The tool is based on the \CompCert
Certified Compiler~\cite{Leroy06}, ensuring
the adoption of a widely accepted formal semantics for the C language.

\heading{Relevant \CompCert features}
\CompCert is actually a family of compilers for implemetations of the C
programming language for various architectures (PowerPC, ARM, x86).
It is moderately optimising, sometimes compared to GCC at optimisation level 1
or~2.
It is formally verified:
the semantics of the programming languages involved in the compiler
(in particular C and the assembly languages)
are formally specified;
and correctness theorems are proved.
The correctness of a compiler is stated as a behaviour inclusion property:
each possible behaviour of the target program is a possible behaviour of the
source program.
A behaviour of a program is a (maybe infinite) sequence of events that describe
the interactions of the running program with its environment.
For the current prototype we have adapted the 2.5 distribution of
\CompCert\footnote{\url{http://compcert.inria.fr/}}.
The tool is available for download and use in~\url{http://fix.me}.
\todo[inline]{Fix/write something more.}

\subsection{Outline of the tool and its architecture}

The meaning of a C program is normally specified as a set of traces
that captures the interactions with the \emph{execution environment}
triggered by the execution of the program (e.g. I/O of data; calls to
the operating system; etc.). In order to match it with the behaviour
of evaluating a Boolean circuit, we need to be somewhat more strict on
the semantics of programs and, consequently, on the class of programs
deemed acceptable to be translated by the tool. The overall assumption
of our Boolean circuit compiler is that the input C program is coupled with a
specification of two memory regions (an input region and an output
region), and that we are able to identify the meaning of the C program
with a Boolean function acting on those memory regions. The tool
should then generate a circuit implementing that specific Boolean
function, thus capturing the meaning of the source program.

\smallskip
\begin{figure}[t]
  \centering
  \includegraphics[width=.8\textwidth]{CDGarch.png}
  \vspace{-18pt}
  \caption{Architecture of the certified compiler}
  \label{fig:archcc}
  \vspace{-12pt}
\end{figure}

The compiler architecture is shown in Figure~\ref{fig:archcc}. 
It is split in two components:
i. the frontend, whose task is to convert the source program
  into an intermediate language representation that has
  been modified to already admit a Boolean circuit 
  {\em interpretation}; 
  and
ii. the backend, that formalises the intended Boolean
    circuit interpretation of programs, and carries out 
    the (certified) transformations up to an explicit 
    Boolean circuit.
%
In other words, the frontend will reject programs for which 
it cannot determine that there exists a valid Boolean circuit 
interpretation; whereas the backend will make explicit the 
Boolean circuit interpretation that exists for any program 
accepted by the front-end.

The compiler frontend follows closely the first few compilation
passes of \CompCert, adapting and extending it to meet
the specific requirements imposed by our specific domain. 
The backend has been developed from scratch.

\subsection{C features/usage restrictions}

\begin{wrapfigure}{R}{.4\textwidth}
\vspace{-.7cm}
\begin{minipage}{.4\textwidth}
\begin{lstlisting}[language=easycrypt]
#include "circgen.h"

int millionaires(int x, int y)
{
  if (x < y) return 1;
  else return 0;
}

int main(void)
{
  static int a, b, result;
  AddInput(a);
  AddInput(b);
  BeginCirc();
  result = millionaires(a, b);
  EndCirc();
  AddOutput(result);
  return 0;
}
\end{lstlisting}
\end{minipage}
\vspace{-0.5cm}
\caption{\label{fig:example-C}%
  Example C program}
\vspace{-0.8cm}
\end{wrapfigure}

The driving goal in our design is to let the programmer use most of
the C language constructs (memory, functions, control structures as
loops and conditional branches, etc.)  that are convenient to program
complex, large circuits.  For instance, the circuit that compares its
two inputs to decide which is the largest can de described by the C
program shown in \figref{example-C} (function \textsf{millionaires}).
So as to be correctly handled by the compiler, and to be able to state
the correctness of the compiler, the circuit must be wrapped in a
\textsf{main} function that declares what are the inputs and the
outputs of the circuit.  The dedicated header file provides convenient
macros.

\heading{Premises} 
We start by enumerating natural high-level restrictions imposed
on input programs:
i. the program must consist of a single compilation unit;
ii. input and output memory regions must be properly identified;
iii. any feature that produces observable events
under \CompCert's semantics is disallowed (e.g. volatile memory
accesses; external calls; inline assembly; etc);
and iv. so far, only integral types are allowed.

\heading{Functions}
The source C program can be structured in different functions, but the
tool will force all function calls to be inlined (independently of the
presence of the \textsf{inline} keyword in function headers). As a
consequence, we should exclude any form of recursion in source
programs (either direct or indirect), since it would lead to an
infinite expansion of the program.  In practical terms, we adapt the
function inlining pass of \CompCert, which refuses to
inline any kind of recursive function (each time it inlines a function
\textsf{f}, it removes \textsf{f} from the inlining
context). Therefore, this restriction amounts to enforcing that, after
inlining, the program entry point does not include any function call.

\heading{Control structure and termination}
In order to extract a Boolean function from a C program we need to
enforce termination on all possible inputs. Since recursion has
already been excluded, possible non-terminating behaviour can only be
caused by C loop statements or unstructured use of \textsf{goto}s.
For loops, we consider a specific compiler pass that will attempt to
remove them by a suitable number of unfoldings (to be detailed
below). Regarding \textsf{goto}s, we chose not to support them 
in the tool (in particular, any attempt to build a loop
using \textsf{goto}s will cause a rejection of the program).

\heading{Variables and memory}
During the conversion of C programs into Boolean circuits,
variables need to be converted into wires connecting
gates. Specifically, each live range of a variable gives rise to a set
of wires (with the number of wires matching the number of bits stored in the
variable) --- \emph{writing} to a variable means that the wires corresponding
to that variable will originate in the output ports of a gate that produces
the value to be stored; and \emph{reading} from a variable means that
the associated wires will be connected to the input wires of some gate
that is consuming the value to perform an operation. Memory accesses to/from fixed
locations behave in this respect pretty much like variables: a
\emph{store} and \emph{load} to a fixed location correspond
essentially to a \emph{write} and \emph{read} of a specific
variable.

Memory accesses can, however, be subtler when the location of the access
(pointer) depends on additional data, such as in the case of indexed
memory accesses (e.g., array operations). When reading from such a
composite address, one is led to a selection of specific wires from a
much larger pool of wires, which amounts to some kind of multiplexer
in Boolean circuit jargon. Conversely, storing in an indexed
address would lead to a demultiplexer gate. The problem lies on the
fact that these (meta-)gates are very expensive if built from
elementary logic operations, leading to exponential circuit sizes
on the number of selection bits. This clearly makes unrestricted
(32-bit) indexes out of reach, leading to the necessity adopting some
strategy to bound them to reasonable limits. 
We therefore exclude any form of dynamic memory allocation (both in
the heap and in the stack) and consider only programs that allocate 
memory statically and for which memory usage is determined at
compile-time.

\begin{wrapfigure}{L}{.45\textwidth}
\vspace{-7mm}
{\scriptsize\sffamily
\catcode`\_=12
\catcode`\&=12
main() \{\\
  x18 = volatile load int8u(&__circgen_io)\\
  int8u[a] = x18\\
  x18 = volatile load int8u(&__circgen_io)\\
  int8u[a + 1] = x18\\
  …\\
  int8u[b + 3] = x335\\
  x9 = __circgen_fence()\\
  {\color{red!50!black}
  x7 = int32[a]\\
  x8 = int32[b]\\
  if (x7 <s x8) goto 14 else goto 13\\
14: x332 = 1; goto 12\\
13: x332 = 0\\
12: int32[result] = x332\\
  }
  x6 = __circgen_fence()\\
  x329 = int8u[result]\\
  _ = volatile store int8u(&__circgen_io, x329)\\
    …\\
  x323 = int8u[result + 3]\\
  _ = volatile store int8u(&__circgen_io, x323)\\
  x2 = 0\\
  return x2\\
\}
}
\caption{\label{fig:example-RTL}%
  Front-end RTL output}
\vspace{-.5cm}
\end{wrapfigure}


\subsection{Front-end compiler passes}

The front-end of \CompCert, for the most part unchanged, is used to parse, 
unroll loops, inline functions, and perform general optimisations at the Register
Transfer Language (RTL) level
(constant propagation, CSE, redundancy elimination).
The RTL intermediate representation 
produced by the front-end for the 
example input program of \figref{example-C} is given in
\figref{example-RTL}.
We can observe that,
because of inlining,
only the main function is left.
It starts by by a sequence of volatile loads that take the circuit inputs from
the environment into the designated global variables, one octet at a time.
After the code of the circuit
(in red on the Figure),
comes a final sequence of volatile stores that sends the circuit outputs to the
environment, one octet at a time.
These three sections of the RTL program are delimited by dummy external calls
(to {\sffamily\scriptsize\_\_circgen\_fence});
they prevent any optimization across these boundaries that could prevent the
correct recognition of inputs and outputs in the next compilation pass.

\heading{Loop unroll} The loop-elimination pass is split into two
elementary transformations: i. one that unrolls the loop by an
arbitrary number of iterations, but leaves the loop unchanged at the
end; ii. one that establishes that the loop kept after all the
unrollings is indeed redundant (i.e. is unreachable).  By doing this,
we simplify significantly the semantic preservation proof, since the
first transformation follows directly from the operational semantics
of loops and is always sound, independently of the number of
unrolls. The second transformation can be seen as a specific instance
of dead code elimination.

We implement the first transformation as a new compiler pass in
\CompCert and prove its semantic preservation theorem. This pass is
performed at the \textsf{Cminor} intermediate language since it has a
unified treatment for all C loop constructors, but still retains
enough structure in the loops to support a simple semantic
preservation proof. The number of unfolds for each loop is received by
the tool as external advice.  For the second transformation, we rely
on the pre-existing \CompCert's dead code elimination pass to remove
the remaining loops that are kept after the unfolds. Note that dead
code elimination is performed after loop unrolling, function inlining
and constant propagation passes, which ensures that loops with simple
control structure are successfully eliminated as dead-code (provided
that sufficient large unroll estimates were given at the loop unroll
pass; otherwise compilation will fail).  Note that we had to slightly
improve the abstract domain used in \CompCert's value analysis to
enable this elimination of dead loops.


\heading{Memory access handling} \vlnote{This paragraph must be
  updated} Memory accesses are classified in \emph{fixed locations}
and \emph{indexed accesses}. In order to bound the size of indexes on
the last class, two strategies are conceivable: i. declare memory
regions where indexed accesses are allowed (either implicitly,
intercepting array declarations, or explicit through user
intervention); or ii. carry out an interval analysis on the program
variables and allow indexed accesses when bounds are within reasonable
limits.  We have adopted the first option, since it seems to offer a
good balance between flexibility, development effort, and
expressivity. The second is arguably more general, but would require
the development of a certified interval analysis which, by itself,
would demand a significant development effort. We leave it for future
work.

\subsection{Backend compiler passes}

\heading{RTL Circuits}
The first intermediate language of the backend is a variant of RTL
that we have called RTLC (RTL Circuits). The language is itself very
similar to RTL, but the control-flow has been replaced by
conditional execution. Specifically, each conditional test is
assigned to a propositional variable. These propositional
variables are used in path-condition formulas that are assigned
to each instruction and discriminate which combination of branches
can possibly lead to it. We note, however, that RTLC retains all the
memory accesses from RTL, that is, write/read accesses to/from
global variables and stack data.

The semantics of RTLC is sequential (each and every
instruction is evaluated following the order of appearance in the
program), but the execution of an instruction is guarded by the
corresponding path condition. 
We have adopted Ordered-Reduced Binary Decision
  Trees~\cite{ref} as canonical representatives of path-conditions, where nodes
are tagged with propositional variables (branching points), and leafs
are boolean values. 
\figref{example-RTLC-SSA} (left) shows the test program after the
path-condition computation pass. Path-conditions are the guards shown
at the end of each line (propositional variables are denoted by their
index).

\begin{figure}
{\scriptsize\sffamily
\begin{minipage}{.4\textwidth}
\begin{tabbing}
INPUTS: a[0] a[1] a[2] a[3] b[0] b[1] b[2] b[3] \\
OUTPUTS: result[0] result[1] result[2] result[3] \\
\\
x6 = int32[a]\hspace{12mm}\=| T\\
x7 = int32[b]\>| T\\
test b14 = x6 <s x7\>| T\\
x16 = 1\>| b14\\
x16 = 0\>| ¬b14\\
int32[result] = x16\>| T\\
\end{tabbing}
\end{minipage}
\hfill
\begin{minipage}{.45\textwidth}
\begin{tabbing}
INPUTS: a[0] a[1] a[2] a[3] b[0] b[1] b[2] b[3]\\
OUTPUTS: result[0] result[1] result[2] result[3]\\
\\
   0: int32[a]\hspace{14mm}\=|\\
   1: int32[b]\>| \\
   4: w0 <s w1\>| T\\
   5: 1\>|\\
   6: 0\>|\\
   7: (w4?w5:w6)\>|\\
   8: int32[result] := w7\>| T
\end{tabbing}
\end{minipage}
}
\caption{\label{fig:example-RTLC-SSA}%
  Guarded instructions instead of control-flow (left)
  and corresponding SSA conversion (right).}
\vspace{-.5cm}
\end{figure}

\heading{From RTL to RTLC}
The translation from RTL to RTLC amounts essentially to the computation of
\emph{path-conditions} for every instruction in the program.
This computation is part of the RTL structural validation that occurs
as the final pass of the compiler front-end component.
This validation ensures that a Boolean circuit interpretation can be assigned to
the RTL program,
making it ready to be processed by the compiler back-end.
This is accomplished by a traversal of the control-flow graph in topological order that:
i.~identifies boundaries of the three segments of the program
(sequence of inputs, body, and sequence of outputs);
ii.~checks that the body only includes forward jumps;
iii.~checks that it does not execute any unsupported instruction
(function call, volatile memory access…).
Note that check iii ensures that the control-flow graph is acyclic,
which in particular validates that every loop was discharged by the
redundancy elimination pass.

During this traversal,
path conditions for the instructions of the body are also constructed applying
the following rules:
i.~initially, all instructions have the \textsf{false} path condition
(unreachable instruction) but the first instruction of the body that has the
\textsf{true} path condition (unconditionally executed);
ii.~when a non-branching instruction is visited,
its path condition is propagated to its successor
(joining it with any previously computed path condition for that program point);
iii.~when a branching instruction (condition test) is visited,
the corresponding propositional variable (resp.~its negation)
is added to the path condition which gets propagated to the  \emph{then} successor
(resp.~the \emph{else} successor).

\heading{Static Single Assignment (SSA)} Presenting
RTLC programs in Static Single Assignment form allows for a
neat correspondence between program variables (register variables in
RTLC) and their intended view of wire buses in a boolean
circuit. More importantly, explicit information on the discrimination
conditions for variable aggregation performed at the control-flow join points
($\phi$-nodes) is easily accessible by looking at path-conditions from
the incoming nodes.  Indeed, during the translation into SSA, we add a rich
 variant of $\phi$-nodes describing not only the variables that are
 merged in the node, but also the conditions that discriminate between
 the different incoming paths.

At this stage, we also take the opportunity to perform an optimization with
significant impact on the size of the resulting circuit: certain
operations whose arguments are constants in every possible
execution path are expanded into multiple instances of those
operations with constant arguments (the associated path-conditions
differentiate between the paths).
This pass is (for now) unverified, and hence must be deactivated if the
goal is to take advantage of our current semantics preservation proof.
In our benchmarking section later on we will discuss the impact of
turning on/off unverified optimizations on circuits generated
by the compiler.

\figref{example-RTLC-SSA} (right) illustrates the effect of the SSA
pass on the running example. 
\vlnote{Syntax is ugly}

\heading{High-Level Circuits}
%\label{sec:ccgt-hlcirc}
%
We call \emph{High-level Circuits} Boolean circuits with complex
gates. This is the next intermediate language used by the compiler
backend. Each of these gates have a specified number of input and output
wires, and behaves in accordance with a predefined boolean function
$\mathsf{eval}_G:2^\mathsf{in} \rightarrow 2^\mathsf{out}$. Circuits
are specified by a sequence (array) of \emph{wire-buses} (sets of
wires). Specifically, the circuit descriptions starts with a
(nonempty) set of input wire-buses, which collectively constitute the
input wires of the circuit. Then, it follows the topological
description of the circuit, describing the gates and how they connect
to each other. Concretely, each line specifies a \emph{wire-bus} (a
set of wires), matching the \textsf{out}-arity of the gate. Inputs to
the gate are specified by \emph{connectors} that select which wires
are plugged to each gate's input. Moreover, a topological constraint
is imposed, as the connector for gate computing wire-bus $k$ can only
refer to wires appearing earlier in the circuit. Finally, we have a
description of the outputs of the circuit (again, described by a
connector).% Figure~\ref{} presents a circuit description for a simple
%circuit.

\heading{Handling of RTLC Memory Accesses}
%
The main abstraction gap between SSA-RTLC and HLcirc is the use of
memory. Recall that RTLC retains memory operations to access/update
global variables and data stored on the stack.
%Moreover, the
%RTL validation pass ensures that every access is done on a known block,
%with a known size.
Hence, the translation into circuits should keep
track of which wires store the data of every relevant memory regions in
each program point.

We treat every memory region as a pool of wires. Read and store
operations consist in either accessing or changing (some of the)
wires. Concretely, we consider four distinct gates to handle memory
read/write operations, all parametrized/indexed by the bit-width of the elements
and the memory region size:
\begin{itemize}
\item \textsf{upd($w$,$n$)}: takes as inputs a condition bit; $w\times n$
  data wires and $\log(n)$ index wires. Outputs the updated $w\times n$ data
  wires.
\item \textsf{sel-$w$-$n$}: takes as inputs an enabler bit; $w\times n$
  \vlnote{enabler vs. condition?}
  data wires and $\log(n)$ index wires. Outputs $w$ wires
  corresponding to the selected element.
\item \textsf{updk-$w$-$n$-$k$}: takes as inputs a condition bit and
  $w\times n$ data wires. Outputs the updated data wires.
\item \textsf{selk-$w$-$n$-$k$}: takes as inputs $w\times n$ data wires and
  outputs $w$ wires corresponding to the \(k\)\textsuperscript{th}~element.
\end{itemize}

A first observation is that updates are always guarded by a
condition bit. In fact, for memory writes, we retain the guarded
sequential execution semantics of RTLC. We should also stress the
distinction between arbitrary and constant indexed variants of both
operations -- while, in the former, the index is provided as an input
to the gate, in the latter the index is a (constant) parameter. The
reason for the distinction is the huge difference between the
gate-complexity of those variants, since constant-index operations
amount essentially to a simple rewiring, while the arbitrary indexes impose
heavy decoding and multiplexing operations. 

\gnote{Do we really mention constant-expansion above?}
This is indeed the main motivation for the (unverified)
constant-expansion pass mentioned earlier: memory accesses constitute
the best example where the impact of unfolding constant alternatives
can be significant, depending on the memory access pattern of the
compiled program.  The handling of global variables can be observed in
~\figref{XXX}.

\heading{Register-to-Wire Mapping and $\phi$-node Placement}
To finalize the translation from SSA-RTLC to HLcirc it now suffices
to associate to each RTLC register a variable the associated number of
wires and insert explicit code to handle $\phi$-nodes. We note that the
SSA form already ensures no cyclic dependencies in the wire
definitions, and path-conditions provide explicit information to
transform $\phi$-nodes into conditional expressions. This is clearly
noted in of \figref{XXX}.\todo{We need a HLcirc example}

\heading{Circuit generation} 
From a high-level circuit, the compiler backend generates a 
Boolean circuit by obtaining instantiations of the high-level
complex gates used in the HLcirc language from an external oracle,
and expanding the entire circuit into Boolean gates.
This external oracle is part of the trusted base of the compiler.
If this is constructed using formally verified instantiations---for
example one can have a formally verified library of Boolean circuits 
for all C native operations such as that described in~\cite{YYY}---
then our semantic preservation theorem states that the generated
circuit is correct with respect to the input C program.

\todo[inline]{Write formal theorem statement?}

Our current implementation implements the gate instantiation
oracle with optimized gates, tailored for multiparty computation
applications similar to those used by CBMC-GC, and which we
assume to be correct down to extensive testing. Formally verifying
the implementations of these gates can be done using the approach
in~\cite{} and we leave this for future work.\todo{expand}

\heading{Further optimizations}
We have implemented additional unverified optimization steps
after the circuit generation part. These optimizations render
our compiler more efficient than alternative solutions such
as CBMC-GC~\cite{VVV}. They will be verified in the future,
but for now they are fully functional in order to render
the tool as useful as possible to a wider audience that may
not be concerned with the high-assurance guarantee that comes
from certification.

\begin{comment}
Overview of the architecture:

\begin{itemize}
\item
  Intermediate language RTLC: the control-flow of RTL programs is
  transformed into guarded instructions.
\item
  SSA construction: introduction of φ-nodes, optimistic removal of
  guards (the result of spuriously enabled instruction is discarded by
  further φ-nodes). The program at this step is very similar to a
  circuit, except that it has a global memory, and wires carry values.
\item
  High-level circuit construction: values acquire a bit-level
  representation, memory blocks become wires in the circuit, gates are
  high-level black-boxes. This pass is a kind of type-inference, where
  types are the sizes of the bit-level representation (plus some block,
  if the value is a pointer).
\item
  Low-level circuit construction: each high-level gate is replaced by
  its off-the-shelf implementation.
\end{itemize}

Some interesting facts:

\begin{itemize}
\item
  Various passes rely on a generalization of ORBDT, an awesome
  data-structure.
\item
  Two optimisations have been dropped: constant expansion at the RTLC
  level, redundancy elimination on low-level circuits.
\item
  Did we find bugs? I don't know… Some correctness arguments are
  not completely clear: in RTLC construction, we rely on translation
  validation; we have to keep the guards of the test gates (in SSA
  construction) since we don't know under which condition it is correct
  to remove them. In both cases, we are confident that the correctness
  conditions hold (but we have no argument).
\end{itemize}


Starting from RTL,
the following compilation passes are dedicated to Boolean circuits.
The first step is to convert the control-flow into guards,
as shown on \figref{example-RTLC}.
The conditions are assigned to dedicated variables
(\textsf{b14} in the example);
such variables can then appear in guards,
that are Boolean formulas, displayed on the right of each instruction,
enabling the execution of the corresponding instruction.

In this intermediate representation (RTLC),
a program consists of a sequence of instructions.

The translation from RTL to RTLC is performed in two steps.
First,
we compute for each (RTL) program point its guard,
i.e., a Boolean formula that captures all execution paths that may lead to it.
This set of paths is represented by a binary decision tree.
This is possible only as there are no more loops in the programs at this stage.
The second step emits the RTLC instructions in topological order,
with the guards expressing the path conditions computed in the previous step.

The path conditions must honour a couple of fundamental properties,
that justify that it is valid to execute more instructions in the RTLC program
than in the RTL one (i.e., the instructions of the non-taken branches)
as their guards will not be satisfied.
These properties are checked,
in the spirit of translation validation.



The next step is SSA construction.
The result of this pass is shown on \figref{example-ValCirc}.
For each RTLC register and each program point,
we compute at which program points and under which conditions (guards)
they have been set.
On every use of a register,
if its definition point is ambiguous,
a φ-node is inserted,
e.g., at line 7 in the example.

To syntactically enfore the SSA property,
registers are not named but referred to through their definition points.

Also, since the φ-nodes contain the guards, we can remove them from other instructions,
unconditionally enabling then.

Next pass introduces bit-level representations of the values and removes the
memory.

The final step replace high-level gates by their low-level implemetation in
terms of AND and XOR gates.
Assuming the implementations of each gate is correct,
this is mostly rewriting.
This pass is not verified.
\end{comment}
