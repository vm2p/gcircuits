%!TeX Root=main.tex


\section{Details of \EasyCrypt formalization}\label{sec:ot}
\label{app:walkthrough}

The top-level abstraction in our formalization is a high-level view
of two-party protocols, which is later independently refined to derive 
formalizations of both oblivious transfer and secure function evaluation. 
%
We introduce these concepts by focusing on a classic oblivious transfer
protocol~\cite{BM89,Naor:2001} and discussing its security proof. Its
small size and relative simplicity make it a good introductory example
to \EasyCrypt formalization.
We also introduce our general framework for dealing with hybrid arguments
in \EasyCrypt.

\heading{Two-Party Protocols} In \EasyCrypt,
declarations pertaining to abstract concepts meant to later be refined
can be grouped into named theories such as the one shown in
Figure~\ref{fig:Prot}. Any lemma proved in such a theory is also a
lemma of any implementation (or instantiation) where the theory axioms
hold. 

\begin{figure}[th]
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
theory Protocol.
 type input$_1$, output$_1$. 
 type input$_2$, output$_2$.
 op validInputs: input$_1$ -> input$_2$ -> bool.
 op f: input$_1$ -> input$_2$ -> output$_1$ * output$_2$.

 type rand$_1$, rand$_2$, conv.
 op prot: input$_1$->rand$_1$->input$_2$->rand$_2$-> conv * output$_1$ * output$_2$.
  $\ldots$
end Protocol.
\end{lstlisting}
\caption{Abstract Two-Party Protocol.}
\label{fig:Prot}
\end{figure}

The top level abstraction that represents two-party protocols
is given in Figure~\ref{fig:Prot}.
%
Two parties want to compute a \emph{functionality} \ec{f} on their joint
inputs, each obtaining their share of the output. This may be done interactively
via a \emph{protocol} \ec{prot} that may make use of additional randomness
(passed in explicitly for each of the parties) and produces, in addition to the
result, a \emph{conversation trace} of type \ec{conv} that describes
the messages publicly exchanged by the parties during the protocol execution.
%
In addition, the input space may be restricted by a validity predicate
\ec{validInputs}. This predicate expresses restrictions on the
adversary-provided values, typically used to exclude trivial
attacks not encompassed by the security definition.

\heading{Simulation-based security}
Following the standard approach for secure multi-party computation protocols,
security is defined using simulation-based definitions. In this case we capture
honest-but-curious (or semi-honest, or passive) adversaries. We consider each
party's \emph{view} of the protocol (typically containing its randomness and the
list of messages exchanged during a run), and a notion of \emph{leakage} for
each party, modelling how much of that party's input may be leaked by the
protocol execution (for example, its length). 
%
Informally, we say that such a protocol is secure if each party's view can be
efficiently simulated using only its inputs, its outputs and precisely defined
leakage about the other party's input.

\begin{figure}[th]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type leak$_1$, leak$_2$. 
op $\phi_1$ : input$_1$ -> leak$_1$. 
op $\phi_2$ : input$_2$ -> leak$_2$.
type view$_1$ = rand$_1$ * conv. 
type view$_2$ = rand$_2$ * conv.

module type Sim = {
  proc sim$_1$(i$_1$: input$_1$, o$_1$: output$_1$, l$_2$: leak$_2$) : view$_1$
  proc sim$_2$(i$_2$: input$_2$, o$_2$: output$_2$, l$_1$: leak$_1$) : view$_2$
}.

module type Sim$_i$ = {
  proc sim$_i$(i$_i$: input$_i$, o$_i$: output$_i$, l$_{3-i}$: leak$_{3-i}$) : view$_i$
}.

module type $\Ad^\mathsf{Prot}_i$ = {
  proc choose(): input$_1$ * input$_2$
  proc distinguish(v: view$_i$) : bool
}.

module Sec$_1$(R$_1$: Rand$_1$, R$_2$: Rand$_2$, $\Sim$: Sim$_1$, $\A_1$: $\Ad^\mathsf{Prot}_1$) = {
  proc main() : bool = {
    var real, adv, view$_1$, o$_1$, r$_1$, r$_2$, i$_1$, i$_2$;
    (i$_1$,i$_2$) = $\A_1$.choose();
    real =$ {0,1};
    if (!validInputs i$_1$ i$_2$)
      adv =$ {0,1};
    else {
      if (real) {
        r$_1$ = R$_1$.gen($\phi_1$ i$_1$);
        r$_2$ = R$_2$.gen($\phi_2$ i$_2$);
        (conv,_) = prot i$_1$ r$_1$ i$_2$ r$_2$;
        view$_1$ = (r$_1$, conv);
      } else {
        (o1,_) = f i$_1$ i$_2$;
        view$_1$ = $\Sim$.sim$_1$(i$_1$, o$_1$, $\phi_2$ i$_2$);
      }
      adv = $\A_1$.distinguish(view$_1$);
    }
    return (adv = real);
  }
}.
\end{lstlisting}
\caption{Security of a two-party protocol protocol.}
\label{fig:protsec}
\end{figure}

Formally, we express this security notion using two games (one for
each party).  We display one of them in Figure~\ref{fig:protsec}, in
the form of an \EasyCrypt \emph{module}. Note that modules are used to
model games and experiments, but also schemes, oracles and
adversaries.\footnote{Our formalisation accommodates generic protocols (e.g.,
oblivious transfer of an arbitrary, albeit polynomial, number of
messages) which justifies the technicality of parametrising the
randomness generation procedures with public information 
associated with the protocol inputs.}

Modules are composed of a \emph{memory} (a set of global variables,  here
empty) and a set of \emph{procedures}.
Note that procedures in the same module may share state; it is therefore not
necessary to explicitly add state to the module signature.
In addition, modules can be parameterized by other modules (in which case, 
we often call them \emph{functors}) whose procedures they can query like 
\emph{oracles}.
Which oracles may be accessed by which procedure is specified using
\emph{module types}. A module is said to fulfill a module type if it implements
all the procedures declared in that type. Any procedures implemented in addition
to those appearing in the module type are not accessible as oracles. For
example, even if a module that implements module type \ec{Sim} is used to
instantiate the $\Sim$ parameter of the \ec{Sec}$_1$ module, none of the
procedures in \ec{Sec}$_1$ may call the \ec{sim}$_2$ oracle.

Module type $\Ad^\mathsf{Prot}_i$ ($i \in \{1,2\}$) tells us that an adversary
impersonating Party $i$ is defined by two procedures:
i. \ec{choose} that takes no argument and chooses a full input pair
for the functionality; and
ii. \ec{distinguish}, that uses Party $i$'s view of the protocol execution to
produce a Boolean guess as to whether it was produced by the real system or the
simulator.
Since the module type is not parameterized, the adversary is not given access
to any oracles (modelling a non-adaptive adversary).
We omit module types for the randomness generators \ec{R}$_1$ and
\ec{R}$_2$, as they only provide a single procedure \ec{gen} taking
some leakage and producing some randomness. We also omit the dual
security game for Party 2.

The security game, modelled as module \ec{Sec}$_1$, is explicitly parameterized
by two randomness-producing modules \ec{R}$_1$ and \ec{R}$_2$, a simulator
\ec{S}$_1$ and an adversary $\A_1$. This enables the code of procedures defined
in \ec{Sec}$_1$ to make queries to any procedure that appears in the module
types of its parameters. However, they may not directly access the internal state or
procedures that are implemented by concrete instances of the module parameters, when
these are hidden by the module type.
We omit the indices representing randomness generators whenever they are clear 
from the context.

The game implements, in a single experiment, both the real and ideal worlds. In
the real world, the protocol \ec{prot} is used with adversary-provided inputs to
construct the adversary's view of the protocol execution.
%
In the ideal world, the functionality is used to compute Party 1's output, which
is then passed along with Party 1's input and Party 2's leakage to the
simulator, which produces the adversary's view of the system.
%
We prevent the adversary from trivially winning by denying it any advantage when
it chooses invalid inputs.

A two-party protocol \ec{prot} (parameterized by its
randomness-producing modules) is said to be secure with leakage $\Phi
= (\phi_1,\phi_2)$ whenever, for any adversary $\A_i$ implementing
$\Ad^\mathsf{Prot}_i$ ($i \in \left\{1,2\right\}$), there exists a
simulator $\Sim_i$ implementing \ec{Sim}$_i$ such that

{\small
\[
\mathsf{Adv}^{\mathsf{Prot}_{i,\Phi}}_{\mathsf{prot},\mbox{\ec{S}$_i$},\mbox{\ec{R}$_1$},\mbox{\ec{R}$_2$}}(\A_i) =
 \left| 2 \cdot \Pr[{\mathsf{Sec}_i(\mbox{\ec{R}$_1$},\mbox{\ec{R}$_2$},\mbox{\ec{S}$_i$},\A_i)}: {\result}] - 1\right|
\]}

\noindent
is small, where \ec{res} denotes the Boolean output of procedure \ec{main}.

Intuitively, the existence of such a simulator $\mathsf{S}_i$
implies that the protocol conversation and output cannot reveal any
more information than the information revealed by the simulator's
input.

\heading{Oblivious Transfer Protocols}
We can now define oblivious transfer, restricting our attention to a specific
notion useful for constructing general SFE functionalities. To do so, we
\emph{clone} the \ec{Protocol} theory, which makes a literal copy of it and
allows us to instantiate its abstract declarations with concrete definitions.
%
When cloning a theory, everything it declares or defines is part of the clone,
including axioms and lemmas. Note that lemmas proved in the original theory
are also lemmas in the clone.
%
The partial instantiation is shown in Figure~\ref{fig:ot}.

We restrict the input, output and leakage types for the parties,
as well as the leakage functions and the functionality \ec{f}. The chooser
(Party 1) takes as input a list of Boolean values (i.e., a bit-string) 
she needs to encode, and the sender (Party 2), takes as input a list of 
pairs of messages (which can also be seen as alternative encodings for the 
Boolean values in Party 1's inputs). Together, they compute
the array encoding the chooser's input, revealing only the lengths of each
other's inputs. We declare an abstract constant \ec{n} that bounds the size
of the chooser's input. This introduces an implicit quantification on the bound
\ec{n} in all results we prove.

\begin{figure}[th]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty, mathescape]
clone Protocol as OT with
  type input$_1$ = bool array,
  type output$_1$ = msg array,
  type leak$_1$ = int,
  type input$_2$ = (msg * msg) array,
  type output$_2$ = unit,
  type leak$_2$ = int,
  op $\phi_1$ ($\arrayVar[1]{i}$: bool array) = length $\arrayVar[1]{i}$,
  op $\phi_2$ ($\arrayVar[2]{i}$: (msg * msg) array) = length $\arrayVar[2]{i}$,
  op f ($\arrayVar[1]{i}$: bool array) ($\arrayVar[2]{i}$: (msg * msg) array) = $\arrayVar[1]{i}_{\arrayVar[2]{i}}$.
  op validInputs($\arrayVar[1]{i}$: bool array) ($\arrayVar[2]{i}$: (msg * msg) array) =
      0 < length $\arrayVar[1]{i}$ $\leq$ n$_{max}$ /\ length $\arrayVar[1]{i}$ = length $\arrayVar[2]{i}$,
$\ldots$
\end{lstlisting}
\caption{Instantiating Two-Party Protocols.}
\label{fig:ot}
\end{figure}

Defining OT security is then simply a matter of instantiating the
general notion of security for two-party protocols via cloning. 
Looking ahead, we use
%
$\mathsf{Adv}^{\mathsf{OT}_i}$ to denote the resulting instance of
$\mathsf{Adv}^{\mathsf{Prot}_{i,\Phi}}$, 
where $\Phi=(\mathsf{length},\mathsf{length})$,
and similarly we write $\Ad^\mathsf{OT}_i$ the types for adversaries
against the OT instantiation.

\heading{Garbling schemes}
Garbling schemes~\cite{DBLP:conf/ccs/BellareHR12}
(Figure~\ref{fig:garble}) are operators on \emph{functionalities} of
type \ec{func}.
%
Such functionalities can be evaluated on some input using an \ec{eval}
operator.  In addition, a functionality can be \emph{garbled} using
three operators (all of which may consume randomness). \ec{funG}
produces the garbled functionality, \ec{inputK} produces an
input-encoding key, and \ec{outputK} produces an output-encoding key.
%
The garbled evaluation \ec{evalG} takes a garbled functionality and
some encoded input and produces the corresponding encoded output. The
input-encoding and output-decoding functions are self-explanatory.
\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
type func, input, output.
op eval : func -> input -> output.
op valid: func -> input -> bool.

type rand, funcG, inputK, outputK.
op funcG  : func -> rand -> funcG.
op inputK : func -> rand -> inputK.
op outputK: func -> rand -> outputK.

type inputG, outputG.
op evalG : funcG -> inputG -> outputG.
op encode: inputK -> input -> inputG.
op decode: outputK -> outputG -> output.
\end{lstlisting}
\caption{Abstract Garbling Scheme.\label{fig:garble}}
\end{figure}
In practice, we are interested in garbling functionalities encoded as
Boolean circuits and therefore fix the \ec{func} and \ec{input} types
and the \ec{eval} function. Circuits themselves are represented by
their topology and their gates.
%
A topology is a tuple $(n,m,q,\mathbb{A},\mathbb{B})$, where $n$ is
the number of input wires, $m$ is the number of output wires, $q$ is
the number of gates, and $\mathbb{A}$ and $\mathbb{B}$ map to each
gate its first and second input wire respectively.
%
A circuit's gates are modelled as a map $\mathbb{G}$ associating
output values to a triple containing a gate number and the values of
the input wires.  Gates are modelled polymorphically, allowing us to
use the same notion of circuit for Boolean circuits and their garbled
counterparts.
%
We only consider \emph{projective schemes}~\cite{DBLP:conf/ccs/BellareHR12}, 
where Boolean values
on each wire are encoded using a fixed-length random \emph{token}.
% and the values of output wires can be decoded by simply taking the
%token's least significant bit.
This fixes the type \ec{funcG} of garbling schemes, and the
\ec{outputK} and \ec{decode} operators.

Following the \textsf{Garble1} construction of Bellare et
al.~\cite{DBLP:conf/ccs/BellareHR12}, we construct our garbling scheme
using a variant of Yao's garbled circuits based on a pseudo-random
permutation, via an intermediate Dual-Key Cipher (DKC)
construction. We denote the DKC encryption with \ec{E}, and DKC
decryption with \ec{D}. Both take four tokens as argument: a tweak
that we generate with an injective function and use as unique IV, two
keys, and a plaintext (or ciphertext).
%
We give functional specifications to the garbling algorithms in
Figure~\ref{fig:somegarble}. For clarity, we denote
functional \ec{fold}s using stateful \ec{for} loops.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type topo = int * int * int * int array * int array.
type 'a circuit = topo * (int * 'a * 'a,'a) map.

type leak = topo.

type input, output = bool array.
type func = bool circuit.

type funcG = token circuit.
type inputG, outputG = token array.
op evalG f i =
  let ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}$) = f in
  let evalGate = lambda g x$_1$ x$_2$,
    let x$_{1,0}$ = lsb x$_1$ and x$_{2,0}$ = lsb x$_2$ in
    D (tweak g x$_{1,0}$ x$_{2,0}$) x$_1$ x$_2$ $\mathbb{G}$[g,x$_{1,0}$,x$_{2,0}$] in
  let wires = extend i q in (* extend the array with q zeroes  *)
  let wires = map (lambda g, evalGate g $\mathbb{A}$[g] $\mathbb{B}$[g]) wires in (* decrypt wires *)
  sub wires (n + q - m) m.

type rand, inputK = ((int * bool),token) map.
op encode iK x = init (length x) (lambda k, iK[k,x.[k]]).

op inputK (f:func) (r:((int * bool),token) map) =
  let ((n,_,_,_,_),_) = f in filter (lambda x y, 0 <=  fst x < n) r.

op funcG (f:func) (r:rand) =
  let ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}$) = f in
  for (g,x$_\mathsf{a}$,x$_\mathsf{b}$) $\in$ [0..q] * bool * bool
    let a = $\mathbb{A}$[g] and b = $\mathbb{B}$[g] in
    let t$_\mathsf{a}$ = r[a,x$_\mathsf{a}$] and t$_\mathsf{b}$ = r[b,x$_\mathsf{b}$] in
    $\widetilde{\mathbb{G}}$[g,t$_\mathsf{a}$,t$_\mathsf{b}$] = E (tweak g t$_\mathsf{a}$ t$_\mathsf{b}$) t$_\mathsf{a}$ t$_\mathsf{b}$ r[g,$\mathbb{G}$[g,x$_\mathsf{a}$,x$_\mathsf{b}$]]
  ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\widetilde{\mathbb{G}}$).
\end{lstlisting}
\caption{\ec{SomeGarble}: our Concrete Garbling Scheme.\label{fig:somegarble}}
\end{figure}

\heading{Security of Garbling Schemes} The privacy property of
garbling schemes required by Yao's SFE protocol is more conveniently
captured using a simulation-based definition.  Like the security
notions for protocols, the privacy definition for garbling schemes is
parameterized by a leakage function upper-bounding the information
about the functionality that may be leaked to the adversary. (We
consider only schemes that leak at most the topology of the circuit.)
%
Consider efficient non-adaptive adversaries that provide two
procedures:
\begin{inparaenum}[i.]
\item \ec{choose} takes no input and outputs a pair \ec{(f,x)}
  composed of a functionality and some input to that functionality;
\item on input a garbled circuit and garbled input pair \ec{(F,X)},
  \ec{distinguish} outputs a bit $b$ representing the adversary's
  guess as to whether he is interacting with the real or ideal
  functionality.
\end{inparaenum}
%
Formally, we define the $\SIMCPA_\Phi$ advantage of an adversary $\A$
of type $\Ad^\mathsf{Gb}$ against garbling scheme $\Gb$ =
\ec{(funcG,inputK,outputK)} and simulator $\Sim$ as
%
$$ \mathsf{Adv}^{\SIMCPA_\Phi}_{\Gb,\mbox{\ec{R}},\mbox{\ec{S}}}(\A) = \left| 2 \cdot \Pr[\mbox{\ec{SIM}}(\mbox{\ec{R}},\mbox{\ec{S}},\A): \result] - 1\right|.$$
%
A garbling scheme $\Gb$ using randomness generator \ec{R} is
$\SIMCPA_\Phi$-secure if, for all adversary $\A$ of type
$\Ad^\mathsf{Gb}$, there exists an efficient simulator $\Sim$ of type
\ec{Sim} such that
$\mathsf{Adv}^{\SIMCPA_\Phi}_{\Gb,\mbox{\ec{R}},\mbox{\ec{S}}}(\A)$ is
small.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type leak.
op $\Phi$: func -> leak.

module type Sim = {
  fun sim(x: output, l: leak): funcG * inputG
}.

module type $\Ad^\mathsf{Gb}$ = {
  fun choose(): func * input
  fun distinguish(F: funcG, X: inputG) : bool
}.

module SIM(R: Rand, $\Sim$: Sim, $\A$: $\Ad^\mathsf{Gb}$) = {
  fun main() : bool = {
    var real, adv, f, x, F, X;
    (f,x) = $\A$.gen_query();
    real =$ {0,1};
    if (!valid f x)
      adv =$ {0,1};
    else {
      if (real) {
        r = R.gen($\Phi$ f);
        F = funcG f r;
        X = encode (inputK f r) x;
      } else {
        (F,X) = $\Sim$.sim(f(x),$\Phi$ f);
      }
      adv = $\A$.dist(F,X);
    }
    return (adv = real);
  }
}.
\end{lstlisting}
\caption{Security of garbling schemes.\label{fig:garblesec}}
\end{figure}


Following~\cite{DBLP:conf/ccs/BellareHR12}, we establish simulation-based
security via a general result that leverages a more convenient 
indistinguishability-based security notion denoted
$\INDCPA_{\Phi_{\mathsf{topo}}}$: we formalize a general theorem
stating that, under certain restrictions on the leakage function $\Phi$,
$\INDCPA_\Phi$-security implies $\SIMCPA_\Phi$ security. This result is
discussed below as Lemma~\ref{lem:ind-implies-sim}.

\heading{A modular proof}
The general lemma stating that $\INDCPA$-security implies
$\SIMCPA$-security is easily proved in a very abstract model, and is then
as easily instantiated to our concrete garbling setting. We describe the
abstract setting to illustrate the proof methodology enabled by \EasyCrypt
modules on this easy example.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,style=easycrypt-pretty,xleftmargin=0pt,xrightmargin=0pt,mathescape]
module type $\Ad^{\mathsf{IND}}$ = {
  fun choose(): ptxt * ptxt
  fun distinguish(c:ctxt): bool
}.

module IND (R:Rand, $\A$:$\Ad^{\mathsf{IND}}$) = {
  fun main(): bool = {
    var p$_0$, p$_1$, p, c, b, b', ret, r;
    (p$_0$,p$_1$) = $\A$.choose();
    if (valid p$_0$ /\ valid p$_1$ /\ $\Phi$ p$_0$ = $\Phi$ p$_1$) {
      b =${0,1};
      p = if b then p$_1$ else p$_0$;
      r = R.gen(|p|);
      c = enc p r;
      b' = $\A$.distinguish(c);
      ret = (b = adv);
    }
    else ret =${0,1};
    return ret;
  }
}.
\end{lstlisting}
\caption{Indistinguishability-based Security for Garbling Schemes.\label{fig:INDCPA}}
\end{figure}
\noindent The module shown in Figure~\ref{fig:INDCPA} is a slight
generalization of the standard $\INDCPA$ security notions for
symmetric encryption, where some abstract leakage operator $\Phi$
replaces the more usual check that the two adversary-provided
plaintexts have the same length.We formally prove an abstract result
that is applicable to any circumstances where
indistinguishability-based and simulation-based notions of security
interact.
%
We define the $\INDCPA$ advantage of an adversary $\A$ of type
$\Ad^{\mathsf{IND}}$ against the encryption operator \ec{enc} using
randomness generator \ec{R} with leakage $\Phi$ as
$$
\mathsf{Adv}^{\INDCPA_\Phi}_{\mathsf{enc},\mbox{\ec{R}}}(\A) =
\left|2 \cdot \mbox{\ec[basicstyle=\small\sffamily,literate={A}{{$\A$}}{1}]{Pr[Game_IND(R,A): res]}} - 1\right|
$$
\noindent where \ec{R} is the randomness generator used in the concrete theory.

In the rest of this subsection, we use the following notion of
invertibility.  A leakage function $\Phi$ on plaintexts (when we
instantiate this notion on garbling schemes these plaintexts are
circuits and their inputs) is \emph{efficiently invertible} if there
exists an efficient algorithm that, given the leakage corresponding to
a given plaintext, can find a plaintext consistent with that leakage.

\begin{lemma}[$\INDCPA$-security implies $\SIMCPA$-security]%
\label{lem:ind-implies-sim}
If $\Phi$ is efficiently invertible, then for every efficient
$\SIMCPA$ adversary $\A$ of type $\Ad^{\mathsf{Gb}}$, one can build an
efficient $\INDCPA$ adversary $\B$ and an efficient simulator
$\Sim$ such that
$$\mathsf{Adv}^{\SIMCPA_\Phi}_{\mathsf{enc},\Sim}(\A) =
\mathsf{Adv}^{\INDCPA_\Phi}_{\mathsf{enc}}(\B).$$
\end{lemma}
\begin{proof}[Proof (Sketch)]
Using the inverter for $\Phi$, $\B$ computes a second plaintext from
the leakage of the one provided by $\A$ and uses this as the second
part of her query in the $\INDCPA$ game. Similarly, simulator $\Sim$
generates a simulated view by taking the leakage it receives and
computing a plaintext consistent with it using the $\Phi$-inverter.
%
The proof consists in establishing that $\A$ is called by $\B$ in a
way that coincides with the $\SIMCPA$ experiment when $\Sim$ is used
in the ideal world, and is performed by code motion.
\end{proof}


\heading{Finishing the proof}
We reduce the $\INDCPA_{\Phi{\sf topo}}$-security of \ec{SomeGarble}
to the $\mathsf{DKC}$-security of the underlying DKC primitive
(see~\cite{DBLP:conf/ccs/BellareHR12}).
In the lemma statement, \ec{c} is an abstract upper bound on the size
of circuits (in number of gates) that are considered valid. The lemma
holds for all values of \ec{c} that can be encoded in a token minus
two bits.

\begin{lemma}[\textsf{SomeGarble} is $\INDCPA_{\Phi_{\sf topo}}$-secure]
\label{lem:SomeGarble-INDCPA}
For every efficient $\INDCPA$ adversary $\A$ of type
$\Ad^{\mathsf{Gb-IND}}$, we can construct a efficient $\mathsf{DKC}$
adversary $\B$ such that
\begin{align*}
\mathsf{Adv}^{\INDCPA_{\Phi_{\sf topo}}}_{\mathsf{SomeGarble}}&(\A) \leq
(\mathsf{c} + 1) \cdot
\mathsf{Adv}^{\mathsf{DKC}}_{\mathsf{SomeGarble}}(\B).
\end{align*}
\end{lemma}
\begin{proof}[Proof (Sketch)]
The constructed adversary $\B$, to simulate the garbling scheme's oracle,
samples a wire $\ell_0$ which is used as pivot in a hybrid construction where:
\begin{inparaenum}[i.]
\item all tokens that are revealed by the garbled evaluation on the
  adversary-chosen inputs are garbled normally, using the real DKC scheme;
  otherwise
\item all tokens for wires less than $\ell_0$ are garbled using encryptions of
  random tokens (instead of the real tokens representing the gates' outputs);
\item tokens for wire $\ell_0$ uses the real-or-random DKC oracle; and
\item all tokens for wires greater than $\ell_0$ are garbled normally.
\end{inparaenum}

Here again, the generic hybrid argument (Figure~\ref{fig:hybrid}) can
be instantiated and applied without having to be proved again,
yielding a reduction to an adaptive DKC adversary. A further reduction
allows us to then build a non-adaptive DKC adversary, since all DKC
queries made by $\B$ are in fact random and independent.
\end{proof}

From Lemmas~\ref{lem:ind-implies-sim} and~\ref{lem:SomeGarble-INDCPA}, we can
conclude with a security theorem for our garbling scheme.

\begin{theorem}[\textsf{SomeGarble} is $\SIMCPA_{\Phi_{\sf topo}}$-secure]
\label{thm:SomeGarble-SIMCPA}
For every $\SIMCPA$ adversary $\A$ that implements
$\Ad^{\mathsf{Gb}}$, one can construct an efficient simulator $\Sim$
and a DKC adversary $\B$ such that
\begin{align*}
\mathsf{Adv}&^{\SIMCPA_{\Phi_{\sf topo}}}_{\mathsf{SomeGarble},\Sim}(\A) \leq
(\mathsf{c} + 1) \cdot \mathsf{Adv}^{\mathsf{DKC}}_{\mathsf{SomeGarble}}(\B).
\end{align*}
\end{theorem}
\begin{proof}[Proof (Sketch)]
Lemma~\ref{lem:ind-implies-sim} allows us to construct from $\A$ the
simulator \ec{S} and an \INDCPA adversary $\mathcal{C}$. From
$\mathcal{C}$, Lemma~\ref{lem:SomeGarble-INDCPA} allows us to
construct $\B$ and conclude.
\end{proof}
