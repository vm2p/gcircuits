% !TeX root=main.tex

\section{Related work}
\label{sec:related}

There have been significant advances towards the development of
computer-aided tools for cryptography. These tools fall into two
loosely related categories. The first category covers a broad spectrum
of high-assurance tools, which use formal methods to deliver strong
correctness or security guarantees on models or (more rarely) on
implementations. The second category comprises many cryptographic
engineering tools, whose goal is to facilitate the development and
rapid deployment of high-speed, high-quality software. We review some
of the main tools from both families. For the sake of focus, we limit
our review to prior work that either delivers verified security proofs
in the computational model, targets verified implementations, or is
directly relevant to secure multi-party computation. We refer the
reader to~\cite{Blanchet:2012} for a more extensive account of the use
of formal methods in (symbolic and computational) cryptography, and
to~\cite{DBLP:journals/iacr/Halevi05a,DBLP:conf/eurocrypt/BellareR06}
for motivations on computer-aided cryptographic proofs.

\subsection{High-assurance cryptography}

\heading{General-purpose tools}
\CryptoVerif~\cite{Blanchet08CV} was among the first tools to support
cryptographic security proofs in the computational model and
%\CryptoVerif uses probabilistic process algebra as modelling
%language and leverages different notions of equivalence to support
%proofs of equivalence, equivalence up to failure events, as well as
%simple forms of hybrid arguments.
it has been used for verifying primitives as well as protocols.
More recently, Cad\'e and
Blanchet~\cite{CadeB13} have complemented the work on \CryptoVerif
with a mechanism to generate functional code from \CryptoVerif models
and use it to generate a verified implementation of SSH.

Swamy et al.~\cite{fstar} build a type-based approach for
reasoning about programs written in the typed functional
programming language \fstar.
%Their approach uses refinement types, a rich typing discipline which can be
%seen as the counterpart for typed functional languages of the
%assertions used in imperative languages.
%Their approach lacks support for fine-grained modelling of probabilistic
%operations, but it uses instead ideal primitives to reason compositionally
%about the security of cryptographic protocols.
Bhargavan et al.~\cite{mitls} subsequently use \fstar\ to develop
high-assurance implementations of TLS.
%
%Although \fstar\ supports direct reasoning about implementations,
%prior work does not address the problem of generating trustworthy
%executables from verified implementations.
% Recent work~\cite{lowfstar} addresses the problem of
% generating trustworthy {\em executables} from such
%verified implementations by targetting \CompCert as a back-end.
%
Rastogi, Swamy and Hicks~\cite{RSH16} also use \fstar\ as a host
language for embedding {\sc Wysteria}, a domain-specific language for
multi-party computation.
%However, and similarly to prior work on \fstar, their
%embedding does not consider the generation of circuits from high-level
%programs.

%\VST (Verified Software Toolchain)~\cite{vst} is a general purpose
%toolchain for reasoning about C programs. \VST harnesses the \CompCert
%compiler to carry safety proofs from C to assembly programs.
%Moreover, it provides an expressive program logic for reasoning about
%C programs. One main application of \VST\ is cryptographic libraries;
%in particular,

Appel~\cite{Appel15} uses \VST (Verified Software Toolchain)~\cite{vst}
to prove the functional correctness of a machine-level implementation
of SHA-256. In a companion effort, Beringer et al.~\cite{BeringerPYA15}
connect \VST with \FCF (Foundational Cryptographic Framework) of
Petcher and Morrisett~\cite{PetcherM15}, in order to provide a
machine-checked proof of reductionist security for a realistic
implementation of HMAC.

\heading{High-assurance MPC}
There have been many works that develop or apply formal methods for
secure multi-party computation.

Backes et al.~\cite{BackesMM10} develop computationally sound methods
for protocols that use secure multi-party computation as a primitive.
%and analyse the SIMAP sugar-beet double auction protocol.
However, they do not consider verified implementations.
%
{\sc Wysteria}~\cite{SP:RasHamHic14} is a new programming language
for mixed-mode multiparty computations. Its design is supported by a
rigorous pen-and-paper proof that typable programs do not leak
information in unintended ways.
%However, their guarantees are cast in
%the setting of language-based security, rather than in the usual style
%of provable security.
%
Dahl and Damg{\aa}rd~\cite{DD14} consider the symbolic analysis
of specifications extracted from two-party SFE protocol descriptions,
and show that the symbolic proofs of security are computationally
sound in the sense that they imply security in the standard UC model
for the original protocols.
%
Pettai and Laud~\cite{PettaiL14} develop a static analysis for proving
that {\sc Sharemind} applications are secure against active
adversaries.%They show that programs accepted by their analysis
%satisfy a simulation-based notion called black-box
%security.%\footnote{further work?}

Fournet, Keller and Laporte~\cite{FournetKL16} propose a certified
compiler from C to quadratic arithmetic circuits (QAP) compatible with
the domain of SNARKs. However, the underlying cryptographic system
does not come with a verified implementation.


Carmer and Rosulek~\cite{CarmerR16} introduce \textsf{LiniCrypt}, a core
language for writing programs that perform linear operations on a finite field
and calls to random oracles. They prove that the equivalence of
\textsf{LiniCrypt} programs can be decided efficiently, and leverage this
result to build a tool for SMT-based synthesis of garbled circuits.

\subsection{Engineering of MPC protocols}
FRESCO~\cite{DamgardDNNT15} is a Java framework for efficient secure
computation. In FRESCO, functions to be securely evaluated are
described as circuits; we equip our certified compiler with a back-end
that integrates seamlessly into this framework.
%%
Run-time systems in FRESCO specify how circuits are evaluated, and are
thus highly dependent on the supported protocols for secure
computation. In addition to our formally verified implementation of
Yao's protocol and the Tiny Tables protocol we use as benchmark,
run-time systems in FRESCO include support for several
protocols for secure computation, including the TinyOT
protocol~\cite{DBLP:conf/crypto/NielsenNOB12} for actively secure
two-party computation based on Boolean circuits; the actively secure
multi-party computation protocol based on arithmetic circuits~\cite{DBLP:conf/eurocrypt/BendlinDOZ11}; and the SPDZ
protocol~\cite{DBLP:conf/crypto/DamgardPSZ12,DBLP:conf/esorics/DamgardKLPSS13}
for actively and covertly secure multi-party computation based on
arithmetic circuits.

Fairplay, Sharemind and TASTY are MPC frameworks alternative to FRESCO.
Fairplay is a system originally developed to support two-party
computation~\cite{MalkhiNPS04} and then extended to multiparty computation as
FairplayMP~\cite{DBLP:conf/ccs/Ben-DavidNP08}: Fairplay implements a two party
computation protocol in the manner suggested by Yao; FairplayMP is based on the
Beaver-Micali-Rogaway protocol~\cite{Beaver:1990:RCS:100216.100287}.
%%
Sharemind~\cite{BLW08} is a secure service platform for data
collection and analysis, employing a 3-party additive secret sharing
scheme and provably secure protocols in the honest-but-curious
security model with no more than one passively corrupted party.
%%
TASTY (Tool for Automating Secure Two-partY computations) is a tool
suite addressing secure two-party computation in the semi-honest
model~\cite{HKSSW10} whose main feature is to
allow the compilation and evaluation of functions using both garbled
circuits and homomorphic encryption.

Holzer et al.~\cite{HFKV12} present a compiler that uses the
bounded model-checker CBMC to translate {\sf ANSI C} programs into
Boolean circuits. The circuits can be used as inputs to the secure
computation framework of Huang et al.~\cite{HEKM11}. This compiler,
CBMC-GC, can also be used as a front-end to our verified
implementation of Yao's protocol. However, as we show in
Section~\ref{sec:bench}, not only does our approach deliver
higher assurance but also, if one activates all optimizations, the
circuits generated by our compiler can offer, for some classes of
circuits, better performace in comparison with the current version
of CBMC-GC (v0.9.3).

\textsf{Frigate}~\cite{7467350} is a toolset that comprises both a
circuit compiler and a new \textsf{C}-like input language, developed
towards the production of more efficient Boolean circuits. Despite not
being a verified compiler, \textsf{Frigate} was built using known
design and testing methods from the compiler community, achieving
better performances when comparing to other circuit compilers such as
\textsf{TinyGarble}~\cite{7163039},
\textsf{Obliv-C}~\cite{cryptoeprint:2015:1153}, 
\textsf{ObliVM}~\cite{Liu:2015:OPF:2867539.2867660} or
\textsf{PCF}~\cite{182941}. Additionally, the same methods used in the 
construction of \textsf{Frigate} allowed the authors to identify a
series of flaws on the Boolean circuit compilers mentioned above.

Recently, Amy et al.~\cite{DBLP:conf/cav/2017-2} built a compiler
that renders {\sf Revs}~\cite{DBLP:journals/corr/ParentRS15} programs
into space-efficient reversible circuits. The work focused on the
usage of such circuits in large quantum computations and was fully
developed and verified using \fstar.
