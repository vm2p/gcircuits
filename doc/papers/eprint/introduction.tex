%!Tex Root=main.tex

\section{Introduction} \label{sec:intro}

Cryptographic engineering is the domain-specific area of software
engineering that brings cryptography to practice. It encompasses
projects that maintain widely used cryptographic libraries such as
OpenSSL,\footnote{\url{http://openssl.org}}
s2n\footnote{\url{http://https://github.com/awslabs/s2n}} and Bouncy
Castle,\footnote{\url{https://www.bouncycastle.org/}} as well as
prototyping frameworks such as CHARM~\cite{CHARM} and
SCAPI~\cite{SCAPI}. More recently, a series of groundbreaking
cryptographic engineering projects have emerged, that aim to bring a
new generation of cryptographic protocols to real-world applications.
In this new generation of protocols, which has matured in the last two
decades, secure computation over encrypted data stands out as one of
the technologies with the highest potential to change the landscape of
secure ITC, namely by improving cloud reliability and thus opening the
way for new secure cloud-based
applications. Projects that aim to bring secure computation over
encrypted data to practice include
FRESCO\footnote{\url{https://github.com/aicis/fresco}}~\cite{DamgardDNNT15},
TASTY~\cite{HKSSW10} and Sharemind~\cite{BLW08}.

In contrast to other areas of software engineering for critical
systems, the benefits of formal verification for cryptographic
engineering have been very limited, with some recent and notable
exceptions~\cite{CRYPTOL,Fournet:2011,mitls,Almeida:2013,Almeida:2016,Barthe:2014}.
The reasons for this are well known: cryptographic software is a
challenge for high-assurance software development due to the tension
that arises between complex specifications and the need for very high
efficiency---security is supposed to be invisible, and current
verification technology comes with a performance penalty.
%
The exceptions mentioned above mark the emergence of a new area of
research: high-assurance cryptography. This aims to apply
formal verification to both cryptographic security proofs and
the functional correctness and security of cryptographic
implementations.

In this paper we demonstrate that a tight integration of
high-assurance cryptography and cryptographic engineering can deliver
the combined benefits of provable security and best cryptographic
engineering practices at a scale that significantly exceeds previous
experiments (typically carried out on core cryptographic primitives).
We deliver a fast and verified software stack for secure computation
over encrypted data. This choice is motivated by several factors.
First, as mentioned above, this technology is among the foremost
practical applications of cryptography and is a fundamental building
block for making cloud computing secure. Second, it is a tremendous
challenge for high-assurance cryptography, as its security proofs are
markedly distinct from prior work in formalizing reductionist
arguments.

\heading{Contributions}
We present a high-assurance and high-speed software stack for secure
multi-party computation. Figure~\ref{fig:iarch} presents the overall
architecture of the stack. The lowest-level component is
FRESCO~\cite{DamgardDNNT15}; an existing, practical, open-source,
framework for secure multi-party computation, which we use for
communications and input/output. The correctness of this framework
(but not its security) is part of our trusted computing base, as
verifying the correctness of a Java-based communications
infrastructure is out of the scope of high-assurance cryptography.

\begin{figure}
\advance\leftskip-1cm
\begin{tikzpicture}[
  node distance=5mm,
  thing/.style={rectangle, dashed, draw=black!50,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=3.5cm},
  unverif/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum
    width=3.5cm},
  unveriff/.style={rounded rectangle, draw=black!70,
    font=\fontsize{6}{6}\color{black!70}\ttfamily,minimum width=3.5cm},
  verif/.style={rounded rectangle, draw=blue!70,
    font=\fontsize{6}{6}\color{blue!70}\ttfamily,minimum width=3.5cm},
  quasiverif/.style={rounded rectangle, dashed, draw=blue!70,
    font=\fontsize{6}{6}\color{blue!70}\ttfamily,minimum width=3.5cm}
]
  \node (ccode) [thing] { C program };
  \coordinate[below=of ccode] (c);
  \node (occomp) [left=of c, quasiverif] { Optimized CircGen } ;
  \node (ccomp) [right=of c, verif] { CircGen };
  \node (bcircuit) [below=of c, thing] { Boolean circuit };

  %\coordinate[below=of bcircuit] (bc);

  \node (circreader) [below=of bcircuit, unveriff] { Circuit reader };
  \node (circdesc) [below=of circreader, thing] { Circuit description
  };

  %\coordinate[below=of bc] (f) ;

  \node (frescoapp) [below=of circdesc, unveriff] { FRESCO application
  };
  \node (veryaosuite) [below=of frescoapp, unveriff] { Verified Yao
    Protocol Suite };

  %\coordinate[below=of f] (vy) ;

  \node (veryaoeval) [below=of veryaosuite, verif] { Verified Yao
    Evaluator };
  \node (comms) [below=of veryaoeval, thing] { Communications };
  \node (fresco) [below=of comms, unverif,yshift=3.5mm] { FRESCO };
  \node (frescob) [rectangle, draw=black!50, fit={(circreader)
    (circdesc) (frescoapp) (veryaosuite) (veryaoeval) (comms) (fresco) },yshift=1mm] {};

  % \node (outputs) [left=of bc,
  % style={rectangle,font=\fontsize{6}{6}\color{black}\ttfamily,minimum
  %   width=3.5cm}] { INPUTS } ;

  % \node (outputs) [below=of veryaoeval,
  % style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum
  %   width=3.5cm}] { Outputs } ;

  \draw [->,dashed] (ccode.south) -- (ccomp.north) {};
  \draw [->,dashed] (ccode.south) -- (occomp.north) {};
  \draw [->,dashed] (ccomp.south) -- (bcircuit.north) {};
  \draw [->,dashed] (occomp.south) -- (bcircuit.north) {};

  \draw [->,dashed] (bcircuit.south) -- (circreader.north) {};
  \draw [->,dashed] (circreader.south) -- (circdesc.north) {};
  \draw [->,dashed] (circdesc.south) -- (frescoapp.north) {};
  \draw [->,dashed] (frescoapp.south) -- (veryaosuite.north) {};
  \draw [->,dashed] (veryaosuite.south) -- (veryaoeval.north) {};

  \draw [<->] (veryaoeval.south) -- (comms.north) {};

  % \draw [->] (veryaoeval.south) -- (outputs.north)
  % node[midway,above,font=\fontsize{6}{6}\color{black!50}\ttfamily] {};

  \draw [->] ([xshift=-1.5cm]circreader.west) -- (circreader.west) node[midway,above,font=\fontsize{6}{6}\color{black!50}\ttfamily] {Inputs};
  \draw [<-] ([xshift=-1.5cm]veryaoeval.west) -- (veryaoeval.west) node[midway,above,font=\fontsize{6}{6}\color{black!50}\ttfamily] {Outputs};

\end{tikzpicture}
\caption{Verified cryptographic software stack. Blue rectangles
  identify the verified components of the stack, while black
  rectangles represent part of our trusting computing base. Dashed
  blue rectangles are partially verified elements and in dashed black
  rectangles one can find intermediate input/output items.}
\label{fig:iarch}
\end{figure}

The intermediate component of our stack is a verified implementation
of Yao's secure function evaluation (SFE) protocol~\cite{Yao82} based
on garbled circuits and oblivious transfer. This protocol allows two
parties $P_1$ and $P_2$, holding private inputs $x_1$ and $x_2$, to
jointly evaluate any function $f(x_1,x_2)$ and learn its result,
whilst being assured that no additional information about their
respective inputs is revealed. Two-party SFE provides a general
distributed solution to the problem of computing over encrypted data
in the cloud~\cite{KSS13}; we allow for both scenarios where the
function is public and both sides provide inputs and scenarios where
one party provides the (secret albeit with leaked topology) circuit to
be computed and the other party provides the input to the computation.

Our implementation is machine-checked in
\EasyCrypt\footnote{https://www.easycrypt.info}~\cite{easycrypt,Barthe:2011a},
an interactive proof assistant with dedicated support to perform game-based
cryptographic proofs in the computational model. Our proof leverages the
foundational framework put forth by Bellare, Hoang and
Rogaway~\cite{DBLP:conf/ccs/BellareHR12} for the security of Yao's garbled
circuits. Our construction of SFE relies on an $n$-fold extension (where $n$ is
the size of the selection string--or the circuit's input) of the oblivious
transfer protocol by Bellare and Micali~\cite{BM89}, in the hashed version
presented by Naor and Pinkas~\cite{Naor:2001}. The implementation is proved
secure relative to standard assumptions: the Decisional Diffie-Hellman problem,
and the existence of entropy-smoothing hash functions and pseudorandom
functions.

The higher-level component of our stack is a verified optimizing compiler from
C programs to Boolean circuits that we call \CircGen.  Our compiler is
mechanically verified using the \Coq proof assistant, and builds on top of
\CompCert~\cite{Leroy06}, a verified optimizing compiler for C programs. It
reuses the front- and middle-end of \CompCert (introducing an extra
loop-unrolling optimization) and it provides a new verified back-end producing
Boolean circuits. The back-end includes correctness proofs for several program
transformations that have not previously been formally verified, including the
translation of RTL programs into guarded form and a memory-agnostic static
single assignment (SSA) form. Our proof of semantic preservation is conditioned
on the existence of an external oracle that provides functionally correct
Boolean circuits for basic operations in the C language, such as 32-bit addition
and multiplication. The low-level circuits used in our current implementation
for these operations have {\em not} been formally verified and are hence part
of our trusted computing base. Verifying Boolean circuits for native C
operations can be done either in \Coq or using other verification
techniques and it is orthogonal to the reported verification effort. 

The Boolean circuits generated by \CircGen compare well with alternative
unverified solutions, namely
CBMC-GC\footnote{\url{http://forsyte.at/software/cbmc-gc/}}~\cite{Franz:CC14},
although they are slightly less efficient (as would be expected). To widen the
applicability of \CircGen to scenarios where speed is more important than
assurance, we also implement some (yet unverified) global post-processing
optimizations that make \CircGen a good alternative to CBMC-GC for high-speed
applications.

Our work delivers several generic building blocks (the Boolean circuit
compiler, a verified implementation of oblivious transfer, \ldots)  that can be
reused by many other verified cryptographic systems.  However, the main
strength of our results resides in the fact that, for the first time, we are
able to produce a joining of high-assurance cryptography and cryptography
engineering that covers all the layers in a (passively) secure multiparty
computation software framework.

\heading{Challenges}
The development of the software stack raised several challenges, which
we now highlight.

\paragraph*{Machine-checked proofs of computational security.}
\EasyCrypt~\cite{easycrypt,Barthe:2011a} is an interactive proof assistant with
dedicated support to perform game-based cryptographic proofs. It has been used
for several emblematic examples, including signatures and encryption schemes.
%
Formalizing the proof of security for our SFE protocol in \EasyCrypt involved
formalizing two generic proof techniques that had not previously been
considered: hybrid arguments and simulation-based security proofs.

In contrast to other standard techniques, which remain within the realm of the
relational program logic that forms the core of \EasyCrypt (i.e., it is used to
verify transitions between successive games), hybrid arguments and
simulation-based proofs lie at the interface between this relational program
logic and the higher-order logic of \EasyCrypt in which security statements are
expressed and proved. Specifically, hybrid arguments combine induction proofs
and proofs in the relational program logic. Similarly, simulation-based
security proofs intrinsically require existential quantification over
adversarial algorithms and the ability to instantiate security models with
concrete algorithms (the simulators) that serve as witnesses as to the validity
of the security claims. These two forms of reasoning excercise the expressive
power of \EasyCrypt's ambient logic, and are thus markedly distinct from the
simple security arguments typically addressed by other similar tools like
\CryptoVerif~\cite{Blanchet08CV}.
%
Secure function evaluation is also a challenging test case in terms of its
scale. Indeed, \EasyCrypt had so far been used primarily for primitives and to
a lesser extent for (components) of protocols. While these examples can be
intricate to verify, there is a difference of scale with more complex
cryptographic systems, such as SFE, which involve several layers of
cryptographic constructions.

Realizing our broader goal required several improvements to the \EasyCrypt
tool. In particular, the complexity and scale of the proof developed here
guided several aspects of \EasyCrypt's development to support compositional
simulation-based proofs, and the aim of producing executable code from
machine-checked specifications served as initial motivation for \EasyCrypt's
code extraction mechanism.
%%
We contribute a generic formalization of hybrid arguments that has since been
included in \EasyCrypt's library of game transformations.

\paragraph*{High-assurance and high-speed implementations.}
Our implementation of Yao's protocol can be thought of as a secure
virtual machine for securely executing arbitrary computations. The
challenge is therefore dual: in addition to a verified implementation
of this virtual machine of sorts, one needs to generate correct and
efficient computation descriptions in a format that can be executed in
this virtual computational platform (in this case Boolean
circuits). Generating such circuit representations by hand is not
realistic, and appropriate tool support is critical if widespread
practical adoption is the goal. The requirement of end-to-end
verification further imposes that compilation into circuits must
itself be verified.
%%
\CircGen fills this gap from both a high-assurance
cryptography perspective---verified outputs incur a small
performance penalty---and a cryptographic engineering perspective---it
supports unverified optimizations for speed-critical applications.

Highlights of our technical contributions at this level include:
\begin{inparaenum}
\item the addition of a loop unrolling transformation to the \CompCert
middle-end that permits converting those programs that can be
expressed as circuits into a loop-free form;
\item new intermediate languages in \CompCert with corresponding
transformations semantics preservation theorems that permit converting
loop-free programs gradually into a circuit representation---this
includes a new domain-specific transformation into Static Single
Assignment (SSA) form; and
\item the formalization of a new target language that captures
the semantics of Boolean circuits and permits stating and proving a
semantics preservation theorem relating the I/O behavior of
an input C program to that of a generated circuit.
\end{inparaenum}

\heading{Access to the development} The \EasyCrypt formalisation of Yao's
protocol, as well as its extracted code, can be found at
\url{https://ci.easycrypt.info/easycrypt-projects/yao}. The code for \CircGen
can be found at \url{https://github.com/haslab/circgen}.
%%Archives containing all proofs, tools and benchmarking material can be
%%retrieved from the following URL: \url{https://fdupress.net/files/conf/ccs17/}.

\heading{Structure of the paper}
In Section~\ref{sec:ecproof} we describe the \EasyCrypt formalization
and the verified implementation of Yao's protocol.
%%
In Section~\ref{sec:compcc} we present \CircGen, our certified Boolean
circuit compiler.
%%
In each of these sections, we give micro-benchmarks for the related software
component. We then present an overall performance evaluation of the software
stack in Section~\ref{sec:bench}.
%%
In Section~\ref{sec:related} we discuss related work, before making some
concluding remarks in Section~\ref{sec:conclusion}.

\heading{Limitations}\label{sec:limitations}
Our approach covers a comfortable subset of C, but some features are excluded
(see Table~\ref{tab:featrest}); some of these features will be added in future
work, while others are traditionally out of reach for SFE. Moreover, some
low-level optimizations have not yet been verified; however, our experiments
show that the verified version of the compiler is already surprisingly close to
the optimized version for most examples. Finally, our Trusted Computing Base
includes the FRESCO platform, \Cryptokit (used to instantiate the hash
function) and \justGarble (used to instantiate the PRF); the formal
verification of these components is out of scope of this work.

