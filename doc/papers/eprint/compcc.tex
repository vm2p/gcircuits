%!Tex Root=main.tex

\section{Certified Boolean circuit compiler}
\label{sec:compcc}

In this section we describe a new certified compiler called \CircGen
that can convert (a large subset of) C programs into Boolean circuit
descriptions.  This is a self-contained, standalone tool that can be
used in arbitrary contexts where computation needs to be specified as
Boolean circuits.
%
By a certified compiler we mean a compiler that is coupled with a formal proof
asserting that the semantics of the source program is preserved through the
compilation process. In other words, whenever the source program exhibits a
well-defined behavior on some input, the behavior of the target program will
match it. The tool is based on the \CompCert certified compiler~\cite{Leroy06},
ensuring the adoption of a widely accepted formal semantics for the C language.

\heading{Relevant CompCert features}
\CompCert is in fact a family of compilers for implementations of the C
programming language for various architectures (PowerPC, ARM, x86).
It is moderately optimizing, sometimes compared to GCC at optimization
level 1 or~2. It is formally verified: the semantics of the
programming languages involved in the compiler (in particular C and
the assembly languages) are formally specified; and correctness
theorems are proved. The correctness of a compiler is stated as a
behavior inclusion property: each possible behavior of the target
program is a possible behavior of the source program. A behavior of
a program is a (maybe infinite) sequence of events that describe the
interactions of the program with its environment.  For the current
prototype we have adapted the 2.5 distribution of
\CompCert.\footnote{\url{http://compcert.inria.fr/}}

\subsection{\CircGen architecture}
The meaning of a C program is normally specified as a set of traces
that captures the interactions with the \emph{execution environment}
triggered by the execution of the program (I/O of data, calls to
the operating system, \ldots). In order to match it with the behavior
of evaluating a Boolean circuit, we need to be somewhat more strict on
the semantics of programs and, consequently, on the class of programs
deemed acceptable to be translated by the tool. The overarching assumption
underlying \CircGen is that the input C program is coupled with a
specification of two memory regions (an input region and an output
region) and that we are able to identify the meaning of the C program
with a Boolean function acting on those memory regions. The tool
should then generate a circuit implementing that specific Boolean
function, thus capturing the meaning of the source program.

\smallskip
\begin{figure}[t]
  \centering
  \includegraphics[width=.5\textwidth]{circgen-arch.png}
  \caption{Architecture of the certified compiler \CircGen}
  \label{fig:archcc}
\end{figure}

The \CircGen architecture is shown in Figure~\ref{fig:archcc}. 
It is split in two components:
\begin{inparaenum}[i.]
\item the front-end, whose task is to convert the source program
  into an intermediate language that has
  been tailored to already admit a Boolean circuit 
  {\em interpretation}; 
  and
\item the back-end, that formalises the intended Boolean
    circuit interpretation of programs, and carries out 
    the (certified) transformations up to an explicit 
    Boolean circuit.
\end{inparaenum}
%
In other words, the front-end will reject programs for which 
it cannot determine that there exists a valid Boolean circuit 
interpretation; whereas the back-end will make explicit the 
Boolean circuit interpretation.

The front-end follows closely the first few compilation
passes of \CompCert, adapting and extending it to meet
the specific requirements imposed by our domain. 
We develop and verify the back-end from scratch.

\subsection{C features/usage restrictions}
The driving goal in our design is to let the programmer use most of
the C language constructs (memory, functions, control structures such as
loops and conditional branches, \ldots) that are convenient to program
complex, large circuits. However, in our presentation we will use a
very simple running example. The circuit that compares its two inputs
to decide which is the largest can be described by the C program shown
in \figref{example-C} (function \textsf{millionaires}).  In order to
be correctly handled by the compiler, the program specifying the
circuit must be wrapped in a \textsf{main} function that declares what
are the inputs and the outputs of the circuit.  The declaration of
inputs and outputs also allows us to state the correctness of the
compiler; intuitively, the trace of this program will include the
incoming inputs and the outgoing outputs of the produced circuit.  The
dedicated header file provides convenient macros. 
%
Note that boolean circuits produced by our compiler are “party-agnostic”. In
the workflow, one specifies which input bits correspond to each party only when
providing the circuit to underlying frameworks. 

\begin{figure}[th]
\centering
{\scriptsize\sffamily
\framebox{
\begin{minipage}{\textwidth}
\begin{tabbing}
\#include "circgen.h"\hspace{3cm} \ \\
\\
int \=millionaires(int x, int y) \{\\
  \>if (x < y) return 1;\\
  \>else return 0;\\
\}\\
int \=main(void) \{\\
  \>static int a, b, result;\\
  \>AddInput(a);\\
  \>AddInput(b);\\
  \>BeginCirc();\\
  \>result = millionaires(a, b);\\
  \>EndCirc();\\
  \>AddOutput(result);\\
  \>return 0;\\
\}
\end{tabbing}
\end{minipage}
}}
\caption{\label{fig:example-C}%
  Example C program}
\end{figure}

\begin{table}[]
\centering
\caption{CircGen features/restrictions}
\label{tab:featrest}
{\scriptsize\sffamily
\begin{tabular}{|ll|}
\hline
Features                &  \\ \hline
Recursion               & $\times$   \\
GOTO's                  & $\times$  \\
Dynamic memory          & $\times$ \\
Static memory           & \checkmark    \\
Loop unfolding          & \checkmark  \\
Integer computations    & \checkmark   \\
Non-integer computation & $\times$  \\ \hline
\end{tabular}
}
\end{table}

\heading{Assumptions on Input Programs} 
We start by enumerating natural high-level restrictions imposed
on input programs:
\begin{inparaenum}[i.]
\item the program must consist of a single compilation unit;
\item input and output memory regions must be properly identified;
\item any feature that produces observable events
under \CompCert's semantics is disallowed (e.g. volatile memory
accesses; external calls; inline assembly; etc);
and
\item so far, only integral types are allowed.
\end{inparaenum} A summary of the most relevant features and
restrictions of \CircGen can be found in Table~\ref{tab:featrest}.
%
The fragment of C that we support is aligned with similar tools. Most
of the limitations at this level are inherent to the
problem of describing programs as (relatively small) Boolean
circuits. 

\heading{Functions}
The source C program can be structured in different functions, but the
tool will force all function calls to be inlined (independently of the
presence of the \textsf{inline} keyword in function headers). As a
consequence, we exclude any form of recursion in source
programs (either direct or indirect).  In practical terms, we adapt the
function inlining pass of \CompCert, which refuses to
inline any kind of recursive function (each time it inlines a function
\textsf{f}, it removes \textsf{f} from the context). Therefore, 
this restriction amounts to enforcing that, after
inlining, the program entry point does not include function calls.

\heading{Control structure and termination}
In order to extract a Boolean function from a C program we need to
enforce termination on all possible inputs. Since recursion has
already been excluded, possible non-terminating behavior can only be
caused by C loop statements or unstructured use of \textsf{goto}s.
For loops, we consider a specific compiler pass that attempts to
remove them by a suitable number of unfoldings (detailed below). We
choose not to support \textsf{goto}s in the tool; in particular, any
attempt to build a loop using \textsf{goto}s will cause the program to
be rejected.

\heading{Variables and memory} During the conversion of C programs
into Boolean circuits, variables need to be converted into wires
connecting gates. Specifically, each live range of a variable gives
rise to a set of wires (with the number of wires matching the number
of bits stored in the variable)---\emph{writing} to a variable means
that the wires corresponding to that variable originate in the output
ports of some gate that produces the value to be stored; and
\emph{reading} from a variable means that the associated wires are
connected to the input wires of some gate that is consuming the
variable value to perform an operation. Memory accesses to fixed
locations behave (in this respect) similarly to variables:
a \emph{store} and \emph{load} to a fixed location correspond to a
\emph{write} and \emph{read} of a specific variable, respectively.

Memory accesses can, however, be subtler when the location of the
access (address) depends on additional data, as in the case of indexed
memory accesses (e.g., array operations). When reading from such a
composite address, one is led to a selection of specific wires from a
much larger pool of wires, which amounts to a multiplexing operation
in Boolean circuit jargon.  Conversely, storing to an indexed address
is akin to a demultiplexer gate. The problem lies in the fact that
these (meta-)gates are very expensive if built from elementary logic
operations, leading to exponential circuit sizes on the number of
selection bits. This clearly makes unrestricted (32-bit) indices out
of reach, leading to the necessity of adopting a strategy to bound
them to reasonable limits.  We therefore exclude any form of dynamic
memory allocation (both in the heap and in the stack) and consider
only programs that
\begin{inparaenum}[i.]
\item allocate memory statically; and
\item for which memory usage is determined at compile-time.
\end{inparaenum}

\begin{figure}[t]
\centering
{\scriptsize\sffamily
\framebox{
\begin{minipage}{\textwidth}
\begin{tabbing}
main\=() \{\\
    \>x18 = volatile load int8u(\&\_\_circgen\_io)\\
    \>int8u[a] = x18\\
    \>x18 = volatile load int8u(\&\_\_circgen\_io)\\
    \>int8u[a + 1] = x18\\
    \>$\dots$\\
    \>int8u[b + 3] = x335\\
    \>x9 = \_\_circgen\_fence()\\
--: \>\color{red!50!black}x7 = int32[a]\\
    \>\color{red!50!black}x8 = int32[b]\\
    \>\color{red!50!black}if (x7 <s x8) goto 14 else goto 13\\
14: \>\color{red!50!black}x332 = 1; goto 12\\
% \end{tabbing}
% \end{minipage}%
% \rule{1em}{0pt}
% \begin{minipage}{.45\textwidth}
% \begin{tabbing}
13: \= \color{red!50!black}x332 = 0\\
12: \>\color{red!50!black}int32[result] = x332\\
    \>x6 = \_\_circgen\_fence()\\
    \>x329 = int8u[result]\\
    \>\_ = volatile store int8u(\&\_\_circgen\_io, x329)\\
    \>$\dots$\\
    \>x323 = int8u[result + 3]\\
    \>\_ = volatile store int8u(\&\_\_circgen\_io, x323)\\
    \>x2 = 0\\
    \>return x2\\
\}
\end{tabbing}
\end{minipage}
}}
\caption{Front-end RTL output}
\label{fig:example-RTL}
\end{figure}

\subsection{Front-end compiler passes}
The front-end of \CompCert, for the most part unchanged, is used to parse,
unroll loops, inline functions, and perform general optimizations at the
Register Transfer Language (RTL) level (constant propagation, common
subexpression elimination, and redundancy elimination).  The RTL intermediate
representation produced by the front-end for the example input program of
\figref{example-C} is given in \figref{example-RTL}. We can observe that,
because of inlining, only the main function is left. It starts with a sequence
of volatile loads that take the circuit inputs from the environment into the
designated global variables, one octet at a time.  Then, following the code of
the circuit (between the lines marked `--' and `12', in red on the Figure),
comes a final sequence of volatile stores that sends the circuit outputs to the
environment, one octet at a time.  These three sections of the RTL program are
delimited by dummy external calls (to
{\sffamily\scriptsize\_\_circgen\_fence}); they block any optimization across
these boundaries that could prevent the correct recognition of inputs and
outputs in the next compilation pass.

\heading{Loop unroll} The loop-elimination pass is split into two
elementary transformations:
\begin{inparaenum}[i.]
\item one that unrolls the loop by an
arbitrary number of iterations, but leaves the loop unchanged at the
end; and
\item one that establishes that the loop kept after all the
unrollings is indeed redundant (i.e., that it is unreachable).
\end{inparaenum}
By doing this, we simplify significantly the semantic preservation
proof, since the first transformation follows directly from the
operational semantics of loops and is always sound, independently of
the number of unrolls. The second transformation can be seen as a
specific instance of dead-code elimination.

We implement the first transformation as a new compiler pass in
\CompCert and prove its semantic preservation theorem. This pass is
performed at the \textsf{Cminor} intermediate language since it has a
unified treatment for all C loop constructors, but still retains
enough structure in the loops to support a simple semantic
preservation proof. The number of unfolds for each loop is received by
the tool as external advice. For the second transformation we rely on
the pre-existing dead code elimination pass of \CompCert to remove the
remaining loops that are kept after the unfolds. Note that dead code
elimination is performed after loop unrolling, function inlining and
constant propagation passes, which ensures that loops with simple
control structure are successfully eliminated as dead-code (provided
that sufficient large unroll estimates are given at the loop unroll
pass; otherwise compilation will fail). To make this transformation
more effective, we had to slightly improve the abstract domain used in
\CompCert's value analysis to improve the accuracy of the
constant-propagation pass.

\subsection{Back-End compiler passes}
\heading{RTL Circuits}
The first intermediate language of the back-end is a variant of RTL
that we have called RTLC (RTL Circuits). The language is itself very
similar to RTL, with the exception that the control-flow is enforced
by conditional execution. Specifically, each conditional test is
assigned to a propositional variable. These propositional variables
are then used to build path-condition formulas that are assigned to
each instruction; the execution of each instruction is conditioned on
the validity of a path condition that encodes the combination of
branches that can possibly lead to it. Note that RTLC retains all the
memory accesses from RTL, that is, writes and reads to and from global
variables and stack data.

The semantics of RTLC is sequential (each and every instruction is
evaluated following the order of appearance in the program), but the
execution of an instruction is guarded by the corresponding path
condition.  We have adopted Ordered-Reduced Binary Decision
Trees~\cite{DBLP:journals/csur/Bryant92} as canonical representatives
of path-conditions, where nodes are tagged with propositional
variables (branching points) and leaves are Boolean values.
\figref{example-RTLC-SSA} (left) shows the test program after the
path-condition computation pass. Path-conditions are the guards shown
at the end of each line (propositional variables are denoted by their
index).

\begin{figure}
{\scriptsize\sffamily
\hfil
\framebox{
\begin{minipage}{.45\textwidth}
\begin{tabbing}
%INPUTS: a[0] a[1] a[2] a[3] b[0] b[1] b[2] b[3] \\
INPUTS: a[0..3] b[0..3] \\
%OUTPUTS: result[0] result[1] result[2] result[3] \\
OUTPUTS: result[0..3] \\
\\
%x6 = int32[a]\hspace{12mm}\=| T\\
x6 = int32[a]\hspace{9mm}\=| T\\
x7 = int32[b]\>| T\\
test b14 = x6 <s x7\>| T\\
x16 = 1\>| b14\\
x16 = 0\>| ¬b14\\
int32[result] = x16\>| T\\

\end{tabbing}
\end{minipage}
}
\hfil
\framebox{
\begin{minipage}{.45\textwidth}
\begin{tabbing}
%INPUTS: a[0] a[1] a[2] a[3] b[0] b[1] b[2] b[3]\\
%OUTPUTS: result[0] result[1] result[2] result[3]\\
INPUTS: a[0..3] b[0..3]\\
OUTPUTS: result[0..3]\\
\\
   0: int32[a]\hspace{14mm}\=|\\
 %  0: int32[a]\hspace{13mm}\=|\\
   1: int32[b]\>| \\
   2: w0 <s w1\>| T\\
   3: 1\>|\\
   4: 0\>|\\
   5: (w2?w3:w4)\>|\\
   6: int32[result] := w5\>| T
\end{tabbing}
\end{minipage}
\hfil
}}

\caption{\label{fig:example-RTLC-SSA}%
  Guarded instructions instead of control-flow (left) %(top)
  and corresponding SSA conversion (right). %(bottom).
}
\end{figure}

\heading{From RTL to RTLC}
The translation from RTL to RTLC amounts essentially to the computation of
\emph{path-conditions} for every instruction in the program.
This computation is part of the RTL structural validation that occurs
as the final pass of the compiler front-end component.
This validation ensures that a Boolean circuit interpretation can be assigned to
the RTL program,
making it ready to be processed by the \CircGen back-end.
This is accomplished by a traversal of the control-flow graph in topological order that:
\begin{inparaenum}[i.]
\item identifies boundaries of the three segments of the program
(sequence of inputs, body, and sequence of outputs);
\item checks that the body only includes forward jumps; and
\item checks that it does not execute any unsupported instruction
(function call, volatile memory access, etc.).
\end{inparaenum}
Note that check ii. ensures that the control-flow graph is acyclic,
which in particular validates that every loop was discharged by the
redundancy elimination pass.

Path conditions for the instructions of the body are also constructed 
during this traversal by applying the following rules:
\begin{inparaenum}[i.]
\item initially, all instructions have the \(\bot\) path condition
(unreachable instruction), except for the first instruction of 
the body that is assigned the \(\top\) path condition 
(unconditionally executed);
\item when a non-branching instruction is visited,
its path condition is propagated to its successor
(joining it with any previously computed path condition for that program point); and
\item when a branching instruction (condition test) is visited,
the corresponding propositional variable (resp.~its negation) is added
to the path condition which gets propagated to the \emph{then}
successor (resp.~the \emph{else} successor).
\end{inparaenum}

\heading{Constant Expansion} The guarded execution model of RTLC is
particularly well-suited to perform an optimization with significant
impact on the size of the resulting circuit for certain classes of C
programs: for some operations it is possible to determine that their
arguments will be constant once the execution path is fixed.  For
those operations we expand the associated instruction into multiple
instances with constant arguments, and use the associated
path-conditions to differentiate between the paths.  In our
implementation we have instrumented this optimization exclusively for
memory operations; the impact for algorithms that rely on array
indexing (e.g., sorting) is dramatic, as we show in the
micro-benchmarking that we present at the end of the section.

\heading{Static Single Assignment (SSA)} Presenting RTLC programs in
Static Single Assignment form allows for a neat correspondence between
program variables (register variables in RTLC) and their intended view
as wire buses in a Boolean circuit. More importantly, explicit
information on the discrimination conditions for variable aggregation
performed at the control-flow join points ($\phi$-nodes) is easily
accessible by looking at path-conditions from the incoming nodes.
Indeed, during the translation into SSA, we add a rich variant of
$\phi$-nodes describing not only the variables that are merged in the
node, but also the conditions that discriminate between the different
incoming paths.

At this stage, we also take the opportunity to remove most of the
path-condition guards on instructions, replacing them with an
implicit $\top$ path-condition, but keeping those whose presence is
required by the semantic preservation result (namely, the guards 
on tests and memory writes). This simplification is justified by:
\begin{inparaenum}[i.]
\item the fact that SSA-form ensures enabled instructions never
destroy previously computed data; and
\item the fact that $\phi$-nodes already have explicit information on incoming
condition guards.
\end{inparaenum}
\figref{example-RTLC-SSA} (right) illustrates the effect of the SSA
pass on the running example.  The SSA property is enforced by the
program syntax: registers are named according to the line at which
they are defined (e.g., \textsf{w2} holds the value resulting from the
evaluation of line~2).

\heading{High-Level Circuits}
%\label{sec:ccgt-hlcirc}
%
\begin{figure}
\centering
{\scriptsize\sffamily
\framebox{
\begin{minipage}{\textwidth}
\begin{tabbing}
  1\ \  \=: 64-wire INPUT\\
  2 \>: 0-bit GLOBAL = []\\
  3 \>: 32-bit GLOBAL = [0,0,0,0]\\
  4 \>: 32-bit GLOBAL = [0,0,0,0]\\
  5 \>: 32-bit GLOBAL = [0,0,0,0]\\
  6 \>: selk(32,32,0)[32] ← [(1,0..31)]\\
  7 \>: id(32)[32] ← [(6,0..31)]\\
  8 \>: selk(32,32,0)[32] ← [(1,32..63)]\\
  9 \>: id(32)[32] ← [(8,0..31)]\\
 10 \>: id(32)[32] ← [(9,0..31)]\\
 11 \>: id(32)[32] ← [(7,0..31)]\\
% \end{tabbing}
% \end{minipage}%
% \rule{2em}{0em}
% \begin{minipage}{.45\textwidth}
% \begin{tabbing}
 12\ \  \=: slt32[1] ← [(11,0..31);(10,0..31)]\\
 13 \>: const32(0)[32] ← []\\
 14 \>: const32(1)[32] ← []\\
 15 \>: guard(¬12)[1] ← [(12,0)]\\
 16 \>: barrierN(32)[32] ← [(15,0);(13,0..31)]\\
 17 \>: guard(12)[1] ← [(12,0)]\\
 18 \>: barrierN(32)[32] ← [(17,0);(14,0..31)]\\
 19 \>: xorN(32)[32] ← [(16,0..31);(18,0..31)]\\
 20 \>: updk(32,32,0)[32] ← [TT;(19,0..31);(5,0..31)]\\
OUTPUT = [(20,0..31)]
\end{tabbing}
\end{minipage}
}}
\caption{\label{fig:example-HLC}%
  High-level circuit for the example program.
}
\end{figure}
We call HL-Circ a language describing Boolean circuits with complex
gates. This is the next intermediate language used by the \CircGen
back-end. Each of these gates has a specified number of input and
output wires, and behaves in accordance with a predefined Boolean
function $\mathsf{eval}_G:2^\mathsf{in} \rightarrow 2^\mathsf{out}$.
Circuits are specified by a sequence (array) of \emph{wire-buses}
(sets of wires) that are fed into and collected from these complex
gates.  Specifically, the circuit description starts with a (nonempty)
set of input wire-buses that collectively constitute the input wires
of the circuit. This is followed by a topological description of the
circuit, describing the gates and how they connect to each other: each
line in the program specifies a \emph{wire-bus} matching
the \textsf{out}-arity of the gate. Inputs to the gate are specified
by \emph{connectors} that select which wires from the incoming bus are
plugged to the gate's input. An obvious topological constraint is
imposed: the connector for a gate can only refer to wires appearing
earlier in the circuit. Finally, we have a description of the outputs
of the circuit (again, described by a connector). \figref{example-HLC}
presents a circuit description for the example program.

\heading{Handling of RTLC Memory Accesses}
%
The main abstraction gap between SSA-RTLC and HLcirc is the use of
memory. Recall that RTLC retains memory operations to access/update
global variables and data stored on the stack.
%Moreover, the
%RTL validation pass ensures that every access is done on a known block,
%with a known size.
Hence, the translation into high-level circuits must keep
track, at each program point, of which wires store the data for 
the relevant memory regions.
To this end, we treat every memory region as a pool of wires, initialized in
accordance with the original C program (lines 2-5 in \figref{example-HLC}, 
which hold the initial data of $\mathsf{stack, a, b}$ and $\mathsf{result}$
respectively). These initial pools are possibly updated by input
declarations (e.g. declaring $\textsf{a}$ as an input redirects its
wires to some of the wires in entry 1 -- the input wires of the
circuit). Read and store operations consist in either reading from or
replacing (some of the) wires in the bus. Concretely, we consider four distinct
gates to handle memory read/write operations, all parameterized
by the bit-width of the elements and the memory region size:
\begin{itemize}[leftmargin=*]
\item \textsf{selk-$w$-$n$-$k$}: takes $n$ data
  wires and outputs the wires for a $w$-bit word 
  corresponding to the $k$-th element ($k$ is a
  constant).
\item \textsf{sel-$w$-$n$}: takes $n$
  data wires and $\log(n/w)$ index wires and outputs 
  the wires for a $w$-bit word 
  corresponding to the indexed element.
\item \textsf{updk-$w$-$n$-$k$}: takes a condition guard (1~bit), $n$
  data wires and $w$ wires holding the value to store; it outputs the
  resulting $n$ data wires (updated at position $k$).
\item \textsf{upd-$w$-$n$}: takes a condition guard, $n$
  data wires, $w$ value wires, and $\log(n/w)$ index wires;
  it outputs the updated $n$ data wires.
\end{itemize}

Note that updates are always guarded by a guard condition. In fact,
for memory writes, we retain the guarded sequential execution
semantics of RTLC. By lazily keeping guards at the update gates we are able to later
remove them with very small overhead (due to constant propagation) and
hence obtain much better generated circuits (in terms of gate
counts). Moreover, observe the distinction between arbitrary
and constant indexed variants of both operations---while, in the
former, the index is provided as an input to the gate, in the latter
the index is a (constant) parameter. The reason for the distinction is
the huge difference between the gate-complexity of those variants,
since constant-index operations amount essentially to a simple
rewiring, while the arbitrary indices impose heavy decoding and
multiplexing operations.
%%
This is indeed the main motivation for the constant-expansion pass
mentioned earlier: memory accesses constitute the best example where
the impact of unfolding constant alternatives can be significant.
%, depending on the memory access pattern of the
%compiled program. % The handling of global variables can be observed in
%~\figref{XXX}.

\heading{Reg-to-Wire Mapping and $\phi$-node Placement}
To finalize the translation from SSA-RTLC to HLcirc it now suffices
to associate to each RTLC variable the correct number of
wires and to insert explicit code to resolve $\phi$-nodes. 
This transformation is justified via the facts that
\begin{inparaenum}[i.]
\item the SSA form ensures no cyclic dependencies in the wire
definitions; and
\item that the explicit guards provided with 
$\phi$-nodes naturally lead to a $w$-bit multiplexer ($w$ being the
bit-width of joined variables).
\end{inparaenum}
This is clearly noted in lines 15--19 of \figref{example-HLC}.

\heading{Circuit generation} 
From a high-level circuit, the \CircGen back-end generates a 
Boolean circuit by obtaining instantiations of the high-level
complex gates used in the HLcirc language from an external oracle,
and expanding the entire circuit into Boolean gates.
This external oracle is part of the trusted base of \CircGen.
If this is constructed using formally verified instantiations---for
example one can have a formally verified library of Boolean circuits 
for all C native operations---
then our semantic preservation theorem states that the generated
circuit is correct with respect to the input C program.
%
In our implementation, the high-level gate instantiation oracle
produces optimized gates, tailored for multiparty computation
applications similar to those used by CBMC-GC, and which we assume to
be correct down to extensive testing. Formally verifying the
implementations of these gates can be done, e.g., by using the
approach in~\cite{verifgates}. We leave this for future work.

\heading{Unverified optimizations} During the gate expansion, we have
implemented some straightforward circuit minimization techniques, such
as memoization to reuse previously computed gates and the removal of
entries not contributing to the output. These global optimizations are
(for now) unverified, and so we report benchmarking results both when
they are turned on and off. When we refer to \emph{optimized} \CircGen
we mean that these optimizations are turned on, and so the semantics
preservation proof does not cover the results. When we refer simply
to \CircGen we mean the semantics-preserving certified tool that
excludes these post-processing optimizations.

\subsection{Micro Benchmarks}
In this section we give a detailed three-way comparison, in terms of gate count
in the output circuit, of both our optimized (partly unverified) and verified
\CircGen and the latest version of
CBMC-GC\footnote{\url{http://forsyte.at/software/cbmc-gc/}} (v0.9.3).
%%
The gate counts for various micro-benchmarks are given in
Table~\ref{tab:compcbmc}. An important caveat should be highlighted at this
point. In collecting results for CBMC-GC, we have truncated its execution time
to be comparable (or at least not too much higher than that of
\CircGen).\footnote{All data was collected with a timeout of $600$.} It is
possible that, by allowing the tool to run for more time, it would have
produced better results. Therefore, our claim here is not that we have a better
tool overall, but that the optimized version of \CircGen is a competitive
alternative to CBMC-GC.  The exception are applications where the computation
heavily relies on array accesses. As can be seen in the table, the constant
expansion optimization that we introduced for static array access optimization
allows us to obtain very significant reductions in gate counts, even in the
verified version of \CircGen, which we do not believe could be resolved via
automatic optimization by CBMC-GC: this is because these optimizations heavily
depend on the high-level semantics of the program.

\begin{table}
\scriptsize
\caption{CBMC-GC/CircGen/Optim. CircGen: Gate Counts}
\label{tab:compcbmc}
\centering

\sffamily
\begin{tabular}{|l|r|r|r|r|r|r|}
\hline
& \multicolumn{2}{|c|}{CBMC-GC} & \multicolumn{2}{|c|}{CircGen} & \multicolumn{2}{|c|}{CircGen Opt.} \\
\hline
Computation & Non XOR & Total\ \ \ \  & Non XOR & Total\ \ \ \  & Non XOR & Total\ \ \ \  \\
\hline
arith100  & 16'143 &  46'215 & 16'952 &  63'005 & 12'657 &  43'361\\
arith1000 & 160'269  &  465'470  & 166'080  &  616'678  & 126'709  &  431'413\\
arith2000 & 319'584  &  936'754  & 332'257  &  1'231'845 & 253'996  &  863'103\\
arith3000 & 479'463  &  1'442'320 & 497'014  &  1'842'778 & 381'382  &  1'294'251\\
\hline
hamming160  & 386 &  1'610  & 2'616  &  10'311 & 650 &  3'086\\
hamming320  & 784 &  3'260  & 5'261  &  20'801 & 1'355  &  6'316\\
hamming800 & 1'997  &  8'248  & 13'196 &  52'271 & 3'470  &  16'006\\
hamming1600  & 5'494  &  22'796 & 26'421 &  104'721  & 6'995  &  32'156\\
\hline
median11 & 10'560 &  17'850 & 3'309  &  14'052 & 2'880  &  12'674\\
median21 & 40'320 &  67'050 & 11'429 &  49'852 & 10'560 &  46'914\\
median31 & 89'280 &  147'600  & 24'424 &  107'627  & 23'040 &  102'754\\
median41 & 902'923  &  1'100'674 & 42'294 &  187'377  & 40'320 &  180'194\\
median51 & 3'520'577 &  3'871'968 & 65'039 &  289'102  & 62'400 &  279'234\\
median61 & 7'410'852 &  7'994'102 & 92'659 &  412'802  & 89'280 &  399'874\\
\hline
matrix3x3  & 32'868 &  85'986 & 28'494 &  86'898 & 27'369 &  79'310\\
matrix5x5  & 148'650  &  398'750  & 131'900  &  402'778  & 127'225  &  369'202\\
matrix8x8  & 3'641'472 &  7'286'912 & 540'224  &  1'650'883 & 522'304  &  1'516'930\\
\hline
aes128-opt  & 6'400  &  30'828 & 90'834 &  387'042  & 6'400  &  31'338\\
aes128-sbox & 504'000  &  719'050  & 164'179  &  664'871  & 50'800 &  310'554\\
aes128-tab  & 865'152  &  1'261'780 & 168'069  &  1'033'945 & 50'800 &  490'586\\
sha256  & 28'571 &  114'169  & 42'677 &  201'626  & 25'667 &  116'181\\
\hline
\end{tabular}
\end{table}

We give counts for both the total number of gates and the total number
of non-XOR gates (AND or OR gates). The latter can be significantly
more costly to evaluate in some protocols than XOR gates, for which 
very effective optimizations exist.
%%
The chosen benchmarks include examples provided in the CBMC-GC distribution,
namely those for arithmetic computation of different complexities, Hamming distance
for strings of different lengths, median computation via sorting and matrix multiplication
(we believe that these examples were used to collect the results in~\cite{Franz:CC14}),
Additionally we include an implementation of the SHA-256 compression function,
taken from the NaCl library,\footnote{\url{https://nacl.cr.yp.to}} and three
different implementations of AES128:
%%
\begin{inparadesc}
\item[\textsf{aes128-tab32}] corresponds to the public-domain optimized
	table-based implementation put forth by Rijmen, Bosselaers and
	Barreto.\footnote{Google ``rijndael-alg-fst.c''}
%%
\item[\textsf{aes128-sbox}] corresponds to the tabled implementation of AES
	included in the \emph{Tiny AES in C}
	library,\footnote{\url{https://github.com/kokke/tiny-AES128-C}} which,
	unlike the previous implementation, stores tables using 8-bit rather
	than 32-bit words; this greatly reduces the book-keeping required to
	extract values from tables.
%%
\item[\textsf{aes128-opt}] corresponds to an optimized version of
	\textsf{aes128-sbox} which we developed by modifying
	\textsf{aes128-box} to make table accesses more
	Boolean-circuit-friendly, taking advantage of our knowledge of the
	Boolean circuits used to instantiate native C operators by the
	back-end, as well as the global cleanup optimizations.  This allows us
	to obtain a relatively efficient circuit implementation
	from both CBMC-GC and optimized \CircGen.
\end{inparadesc}

The verified version of \CircGen is surprisingly close to the optimized version
in all circuits except those corresponding to the Hamming distance and the
optimized AES implementation we described above (\textsf{aes128-opt}), for
which global, circuit-wide, optimizations give the greatest benefit.  A
comparison of optimized \CircGen with CBMC-GC shows that the two are relatively
close for arithmetic operations, CMBC-GC is better in Hamming distance
computations, and our tool is better in all programs that use arrays heavily,
including the vanilla versions of tabled AES implementations.
%
The global optimization passes are the subject of ongoing work. We do not
envision any conceptual difficulty in verifying them, but they do imply
reasonable effort to express cross-gate optimizations such as memoization and
simplification. Indeed, early experiments reveal that these passes do
exacerbate the memory usage of the compiler. The means that we likely will not
be able to rely on the data structures made available by \CompCert's
infrastructure as we do in other passes (specifically, for Maps).

