% !TeX root=main.tex

\section{Verified SFE implementation}
\label{sec:ecproof}

We first give an overview of what we prove in \EasyCrypt, relating this to
established results in the field of cryptography.  We do not go into the
details of the (publicly available) formalization but include in
Appendix~\ref{app:walkthrough} an example-driven presentation of its
highlights. The formalization is available online and the various files that
compose it can be easily matched to the building blocks in the high-level
description we give here. At the end of the section we describe how we obtain
our verified implementation from the \EasyCrypt formalization.

\heading{Yao's protocol in a nutshell}
Yao's protocol is based on the concept of garbled circuits.
Informally, the idea of garbling a circuit computing $f$ consists  
of:
\begin{inparaenum}[i.]
\item expressing the circuit as a set of truth tables (one for each
  gate) and meta information describing the wiring between gates;
\item replacing the actual Boolean values in the truth tables with
  random cryptographic keys, called {\em labels}; and
\item translating the wiring relations using a system of {\em locks}:
  truth tables are encrypted one label at a time so that, for each
  possible combination of the input wires, the corresponding labels
  are used as encryption keys that lock the label for the correct
  Boolean value at the output of that gate.
\end{inparaenum}
Then, given a garbled circuit for $f$ and a set of labels representing
(unknown) values for the input wires encoding $x_1$ and $x_2$, one can
obliviously evaluate the circuit by sequentially computing one gate
after another: given the labels of the input wires to a gate, only one
entry in the corresponding truth table will be decryptable, revealing
the label of the output wire. The output of the circuit will comprise
the labels at the output wires of the output gates.

To build a SFE protocol between two honest-but-curious parties, one can 
use Yao's garbled circuits as follows. Bob (holding $x_2$) garbles the 
circuit and provides this to Alice (holding $x_1$) along with:
\begin{inparaenum}[i.]
\item the label assignment for the input wires corresponding to $x_2$, and
\item all the information required to decode the Boolean values of the output
wires.
\end{inparaenum}
In order for Alice to be able to evaluate the circuit, she should be
able to obtain the correct label assignment for $x_1$. Obviously,
Alice cannot reveal $x_1$ to Bob, as this would violate the
input-privacy goals of SFE. Also, Bob cannot reveal information that
would allow Alice to encode anything other than $x_1$, since this
would reveal more than $f(x_1,x_2)$. To solve this problem, Yao
proposed the use of an {\em oblivious transfer} (OT) protocol. This is
a (lower-level) SFE protocol for a very simple functionality that
allows Alice to obtain the labels that encode $x_1$ from Bob, without
revealing anything about $x_1$ and learning nothing more than the
labels she requires.\footnote{Luckily, efficient protocols for the OT
functionality exist, thereby eliminating what could otherwise be a
circular dependency.} The protocol is completed by Alice evaluating
the circuit, recovering the output, and providing the output value
back to Bob.
%
Excellent descriptions of Yao's SFE protocol with slightly different
security proofs can be found in~\cite{LP09,DBLP:conf/ccs/BellareHR12}.

\heading{A modular proof of security}
Our starting point for producing a formally verified implementation of
Yao's protocol is to transpose to \EasyCrypt the modular security
proof by Bellare, Hoang and Rogaway~\cite{DBLP:conf/ccs/BellareHR12}
(BHR). The central component in this proof is a new abstraction called
a {\em garbling scheme} that captures the functionality and security
properties of the circuit garbling technique that is central to Yao's
SFE protocol. This new abstraction was used by BHR to make precise the
different security notions that could apply to this garbling
step. This permits separating the design and analysis of efficient
garbling schemes from higher level protocols, which may rely on
different security properties of the garbling
component.\footnote{Garbled circuits are used in Yao's SFE protocol,
but have found many other applications in cryptography.}

Figure~\ref{fig:origproof} shows the structure of the proof of
security for Yao's protocol given in~\cite{DBLP:conf/ccs/BellareHR12}
(we focus only on the result that is relevant for this paper). We
depict constructions as rectangles with grey captions and primitives
(i.e., cryptographic abstractions with a well-defined syntax and
security model) as rounded rectangles with black captions. Security
proofs are represented by dashed arrows and implications between
notions as solid arrows. A construction enclosing a primitive in the
diagram indicates that the primitive is used as an abstract building
block in its security proof.  For example, arrow (1) indicates that
the first step in the proof is the construction of a \emph{dual key
cipher} (DKC) using a standard PRF security assumption via a
construction that we call \emph{dual masking}. The same primitive is
also constructed from an ideal cipher via the double encryption
construction.

\begin{figure}[t]
\centering
\scalebox{.63}{
\begin{tikzpicture}[
  node distance=0.5mm,
  construction/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=2.2cm},
  primitive/.style={rounded rectangle, draw=black!50, font=\scriptsize\ttfamily}
]
  \node (dmasking) [construction] { Dual Masking };
  \node (prf) [below=of dmasking.south, primitive] { PRF };
  \node (dkc1) [rectangle, draw=black!50, fit={(dmasking) (prf)}] {};

  \node (dencrypt) [below=of dmasking.south, construction, yshift=-0.7cm] { Double Encryption };
  \node (ic) [below=of dencrypt.south, primitive] { Ideal Cipher };
  \node (dkc2) [rectangle, draw=black!50, fit={(dencrypt) (ic)}] {};

  \node (dkc) [primitive, draw=black!50, right=of dmasking.east,xshift=1cm,yshift=-0.8cm] {Dual Key Cipher};

  \draw [->,dashed] (dkc1.east) -- (dkc.175)  node[midway,above] {\tiny 1};
  \draw [->,dashed] (dkc2.east) -- (dkc.185);

  \node (g1) [construction,above= of dkc.north,yshift=0.4cm] { Garble1 };
  \node (gscheme1) [rectangle, draw=black!50, fit={(g1) (dkc) (dkc1) (dkc2)}] {};

  \node (gscheme) [primitive, draw=black!50, dashed, right=of gscheme1.east,xshift=0.5cm,text width=2.4cm,align=center] {Garbling Scheme\\IND Secure};
  \draw [->,dashed] (gscheme1.east) -- (gscheme)  node[midway,above] {\tiny 2};

  \node (gschemesim) [primitive, draw=black!50, below=of gscheme.south,yshift=-0.2cm,text width=2.4cm,align=center] {Garbling Scheme\\SIM Secure};
  \draw [->] (gscheme.south) -- (gschemesim)  node[midway,right] {\tiny 3};

  \node (ot) [primitive, draw=black!50, above=of gscheme.north,yshift=0.2cm,text width=2.4cm,align=center] {Oblivious Transfer\\2PPP Secure};

  \node (yao) [construction,above= of g1.north,yshift=0.3cm] { Yao's Protocol };
  \node (sfe1) [rectangle, draw=black!50, fit={(gscheme1) (gschemesim) (yao)}] {};

  \node (sfe) [primitive, draw=black!50, right=of sfe1.east,xshift=0.5cm,text width=2cm,align=center] {SFE\\2PPP Secure};
  \draw [->,dashed] (sfe1.east) -- (sfe)  node[midway,above] {\tiny 4};
\end{tikzpicture}
}

\caption{Yao's protocol security proof by BHR~\cite{DBLP:conf/ccs/BellareHR12}.}
\label{fig:origproof}
\end{figure}

A DKC is a tweakable deterministic encryption scheme that can be used
to lock secret keys (corresponding to gate output wire labels) and is
keyed by two other independent keys (corresponding to gate input wire
labels). Informally, the dual masking construction applies two masks
to the encrypted key, computed as $\mathsf{PRF}_{K_i}(T)$ for $i=1,2$,
where $T$ is the tweak.
%
The DKC security model is designed in an ad hoc way to be just strong
enough for constructing garbling schemes from a wide range of
assumptions, including interesting instantiations such as
double encryption. DKC security is a real-or-random notion, where the
attacker has an unbounded number of keys to choose from, both for
posing as encryption keys and as encrypted keys.  One of these secret
keys is singled out as the challenge secret key, and it can never be
encrypted nor revealed to the attacker (who may see all the other
keys). The model also captures the fact that it is convenient to leak
the least significant bit of such keys in order to encode the topology
of a circuit.

The second step in the proof (2) is to construct a garbling scheme
from a (DKC). There are two security definitions for garbling schemes:
indistinguishability-based (IND) and simulation-based (SIM).  The
former is used as a stepping stone (hence its dashed presentation in
the diagram) to proving SIM-security.  Indeed, the two notions are
proven to be equivalent for certain classes of garbling schemes (this
is shown as step 3 in the diagram). Proving that a concrete
construction called \textsf{Garble1} achieves IND security is the most
challenging part of the proof: it involves a hybrid argument over
those wires in the circuit that are {\em not} visible to an attacker
(the security model allows the attacker to observe the opening of the
circuit for one concrete input).

The final step (4) in the proof is to show that Yao's
technique of combining an oblivious transfer protocol---
two-party passively (2PPP) secure---with a SIM-secure
garbling scheme yields a 2PPP secure SFE protocol.
This step consists of a game-based argument
with two relatively simple transitions, but involving
simulation-based definitions and combined universal
and existential quantifications over adversarial algorithms.

\heading{Our Proof}
We show in Figure~\ref{fig:ourproof} the structure of our
\EasyCrypt formalization. It is visible in the figure that
the main structure of the proof, steps 1-4 are very close to
the original proof of~\cite{DBLP:conf/ccs/BellareHR12}.
The only deviation here is that we simplify the Dual Key
Cipher security game to a slightly stronger variant that 
is still satisfied by the dual masking instantiation, but 
which has an internal structure that makes the proof of
security of the garbling scheme significantly easier.
Intuitively, the difference is that one imposes that
the tweak effectively makes encryptions of the same value 
indistinguishable from each other. This excludes 
some secure DKC instantiations that we do not consider
in this paper.
%%
To further simplify our proofs, our DKC security definition
is also parametrized by two integer parameters 
$\mathsf{c}$ and $\mathsf{pos}$. The first parameter
provides an upper-bound on the number of keys in the game,
so that they can all be sampled at the beginning of
the security experiment. The second parameter specifies
an index in the range $[1..\mathsf{c}]$ that will
be used in oracle queries as the index for the hidden
secret key.

Figure~\ref{fig:ourproof} also shows three additional proof steps (5, 7 and 8,
shown in blue).  These correspond to instantiation (i.e., restricted forms of
composition) steps that are often implicit in hand-written cryptographic
proofs.  For example, suppose construction $C_1^{P_2}$ is proven to be a valid
instantiation for primitive $P_1$ under the assumption that instantiations for
abstract primitive $P_2$ exist.  Suppose also that construction $C_2^{P_3}$ is
proven to be a valid instantiation of primitive $P_2$, assuming the existence
of a valid instantiation for (lower level) primitive $P_3$.  Then, this implies
that $C_1^{C_2}$ is also a valid instantiation of $P_1$ under assumption $P_3$.

Such steps are critical in making our main Theorem
(Theorem~\ref{thm:main-theorem} below) apply to a concrete and efficient
implementation of Yao's protocol that can readily be extracted in to OCaml code
from its \EasyCrypt description.
%%
To obtain such a result our formalization needs to explicitly include theorems
that instantiate abstract security results into concrete security bounds for
the implementation.
%%
More precisely,  one needs to prove
\begin{inparaenum}[i.]
\item that the implementation is functionally equivalent to the composition of
	a concrete oblivious transfer and garbling schemes; and
\item that this implies that the security bound for the generic SFE security
	theorem (4 in the figure) can be instantiated into a concrete overall
	bound by plugging in security bounds obtained by instantiating all
	intermediate results all the way down to the PRF, DDH and entropy
	smoothing assumptions.
\end{inparaenum}

\EasyCrypt enables formalizing both the complex abstract security
proofs and the instantiation steps (with very little overhead in the
case of the latter). The main theorem in our formalization states the
following, for any upper bound $\mathsf{c}$ on the total number of
wires in the circuit and any upper bound $\mathsf{n}$ on the number of
input wires in the circuit.

\begin{theorem}
\label{thm:main-theorem}
For all $\mathsf{SFE}$ adversaries $\A$ against the \EasyCrypt
implementation $\mathsf{Impl}$ of Yao's protocol, there exist
efficient simulator~\(\Sim\) and adversaries $\B_{\mathsf{DDH}}$,
$\B_{\mathsf{ES}}$ and $\B^i_{\mathsf{PRF}}$ for $i \in
[1..\mathsf{c}]$, such that:
%
\[
\mathsf{Adv}^{\mathsf{SFE}}_{\mathsf{Impl},\Sim}(\A) \leq%\\
\mathsf{c} \cdot \varepsilon_{\mathsf{PRF}} + \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{DDH}}(\B_{\mathsf{DDH}}) +
   \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{ES}}(\B_{\mathsf{ES}})
\]
%
where $\varepsilon_{\mathsf{PRF}} = \mathsf{max}_{1 \leq i \leq
c}(\mathsf{Adv}(\B^i_{\mathsf{PRF}}))$, and
$\mathsf{Adv}^{\mathsf{PRF}}$, $\mathsf{Adv}^{\mathsf{DDH}}$ and
$\mathsf{Adv}^{\mathsf{ES}}$ represent the advantages against the PRF,
the Diffie-Hellman group and entropy smoothing hash function used as
primitives.
\end{theorem}



\begin{figure}[t]
\centering
\scalebox{.63}{
\begin{tikzpicture}[
  node distance=0.5mm,
  implementation/.style={rectangle, fill=black!20, draw=blue!50, 
  font=\fontsize{6}{6}\color{blue!50}\ttfamily,minimum width=2.2cm},
  construction/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=2.2cm},
  primitive/.style={rounded rectangle, draw=black!50, font=\scriptsize\ttfamily}
]
  \node (dmasking) [construction] { Dual Masking };
  \node (prf) [below=of dmasking.south, primitive] { PRF };
  \node (dkc1) [rectangle, draw=black!50, fit={(dmasking) (prf)}] {};

  \node (dkc) [primitive, draw=black!50, right=of dkc1.east,xshift=1cm] {Dual Key Cipher'};

  \draw [->] (dkc1.east) -- (dkc.west)  node[midway,above] {\tiny 1};

  \node (g1) [construction,above= of dkc.north] { SomeGarble };
  \node (gscheme1) [rectangle, draw=black!50, fit={(g1) (dkc) (dkc1) }] {};

  \node (gscheme) [primitive, draw=black!50, dashed, right=of gscheme1.east,xshift=0.5cm,text width=2.4cm,align=center] {Garbling Scheme\\IND Secure};
  \draw [->] (gscheme1.east) -- (gscheme)  node[midway,above] {\tiny 2};

  \node (gschemesim) [primitive, draw=black!50, above=of gscheme.north,yshift=0.2cm,text width=2.4cm,align=center] {Garbling Scheme\\SIM Secure};
  \draw [->] (gscheme.north) -- (gschemesim)  node[midway,right] {\tiny 3};
  \draw [->, draw=blue!50] (gscheme1.8) -- (gschemesim.-175)  node[midway,above] {\tiny 5};

  \node (ddh) [above=of dmasking.north, primitive,yshift=1cm,xshift=-.05cm] { DDH };
  \node (es) [right=of ddh.east, primitive,xshift=1cm] { Entropy Smoothing };
  \node (someot) [construction, above=of es.north,xshift=-1cm] { SomeOT };
  \node (ot1) [rectangle, draw=black!50, fit={(ddh) (es) (someot)},minimum width=6.1cm] {};

  \node (ot) [primitive, draw=black!50, right=of ot1.east,xshift=0.5cm,text width=2.4cm,align=center] {Oblivious Transfer\\2PPP Secure};
  \draw [->, draw=blue!50] (ot1.east) -- (ot)  node[midway,above] {\tiny 6};  

  \node (yao) [construction,above= of ot1.north,xshift=1cm] { Yao's Protocol };
  \node (sfe1) [rectangle, draw=black!50, fit={(gscheme1) (gschemesim) (yao) (ot) (ot1)}] {};

  \node (sfe) [primitive, draw=black!50, right=of sfe1.east,xshift=0.5cm,text width=2cm,align=center] {SFE\\2PPP Secure};
  \draw [->] (sfe1.east) -- (sfe)  node[midway,above] {\tiny 4};

  \node (efficient) [implementation,above= of sfe.north,yshift=.72cm] { Implementation };
  \draw [<->, draw=blue!50] (sfe1.15) -- (efficient.west)  node[midway,above] {\tiny 7};
  \draw [->, draw=blue!50] (efficient.south) -- (sfe.north)  node[midway,right] {\tiny 8};

\end{tikzpicture}
}
\caption{Structure of our verified security proof of an implementation of Yao's protocol.}
\label{fig:ourproof}

\end{figure}

\heading{Using Generic Lemmas}
In Cryptography, it is common to repeat proof techniques in different proofs or
even inside the same proof. As a side contribution of our work, we formalize a
generic hybrid argument that is included as part of \EasyCrypt's library of
verified transformations. The objective of this library is to formalize
often-used proof techniques once and for all, enabling the user to perform
proofs ``by a hybrid argument'', or ``by eager sampling'', whilst formally
checking that all side conditions are fulfilled at the time the lemma is
applied.

We now describe the generic hybrid argument.

\begin{figure}[th]
%\advance\leftskip-1.15cm
\begin{minipage}[t]{.18\textwidth}
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=-30pt,xrightmargin=0pt,style=easycrypt-pretty]
type input, output, inleaks, outleaks. 
module type Orcl = { proc o(_:input) : output }.
module type Orcl$_b$ = {
  proc leaks(_:inleaks): outleaks
  proc o$_L$(_:input) : output
  proc o$_R$(_:input) : output
}.
module type $\Ad^{\mathsf{Hy}}$ (O$_b$:Orcl$_b$, O:Orcl) = { 
  proc main () : bool 
}.
module L$_n$ (O$_b$:Orcl$_b$, $\A$:$\Ad^{\mathsf{Hy}}$) = {
  module O: Orcl = { 
    (* increment C.c and call O$_b$.o$_L$ *)
  }
  module $\A$' = $\A$(O$_b$, O);
  proc main () : bool = { 
    C.c = 0; return $\A$'.main(); 
  }
}.
module R$_n$ (O$_b$:Orcl$_b$, $\A$:Adv) = { 
  (* Same as L$_n$ but use O$_b$.o$_R$ *) 
}.
op q : int.
\end{lstlisting}
\end{minipage}%
\rule[-6cm]{.1pt}{5cm}\,
\begin{minipage}[t]{.15\textwidth}
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
module $\B$($\A$:$\Ad^{\mathsf{Hy}}$, O$_b$:Orclb, O:Orcl) = {
  module LR = {
    var l, l$_0$ : int  
    proc orcl(m:input):output = {
      var r : output;
      if (l$_0$ < l) r = O$_b$.o$_L$(m);
      else if (l$_0$ = l) r = O.orcl(m);
      else r = O$_b$.o$_R$(m);
      l = l + 1; return r;
    }    
  }
  module $\A$' = $\A$(O$_b$,LR)
  proc main():outputA = {
    var r:outputA;
    LRB.l$_0$ =$ [0..q-1]; LRB.l  = 0; 
    return $\A$'.main();
  }
}.
\end{lstlisting}
\end{minipage}
\rule{5cm}{.1pt}
\begin{minipage}{.4\textwidth}
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
lemma Hybrid: forall (O$_b$:Orcl$_b${C,$\B$}) ($\A$:$\Ad^{\mathsf{Hy}}$ {C,$\B$,O$_b$}), 
  Pr[Ln(O$_b$,$\A$): result /\ C.c $\leq$ n] - Pr[Rn(O$_b$,$\A$): result /\ C.c $\leq$ n] = 
       q * (Pr[Ln(O$_b$,$\B$($\A$)): result /\ $\B$.l $\leq$ n /\ C.c $\leq$ 1] -  
                    Pr[Rn(O$_b$,$\B$($\A$)): result /\ $\B$.l $\leq$ n /\ C.c $\leq$ 1]).
\end{lstlisting}
\end{minipage}
\caption{Hybrid argument lemma.}
\label{fig:hybrid}
\end{figure}

As described in Figure~\ref{fig:hybrid}, consider an adversary
parameterized by two modules. The first parameter \ec{O}$_b$,
implementing the module type \ec{Orcl}$_b$, provides a leakage oracle,
a left oracle \ec{o}$_L$ and right \ec{o}$_R$. The second parameter
\ec{O}, implementing module type \ec{Orcl}, provides a single oracle
\ec{o}.  The goal of an adversary implementing type
$\Ad^{\mathsf{Hy}}$ is to guess, in at most \ec{n} queries to \ec{O.o},
if it is implementing the left oracle \ec{O}$_b$\ec{.o}$_L$ or the right oracle
\ec{O}$_b$\ec{.o}$_R$. To express the advantage of such an adversary,
we write two modules: the first one, \ec{L}$_n$, defines a game where
the adversary is called with \ec{O.o} equal to \ec{O}$_b$\ec{.o}$_L$,
the second one, \ec{R}$_n$, uses \ec{O}$_b$\ec{.o}$_R$ instead. Both
\ec{L}$_n$ and \ec{R}$_n$ use a variable \ec{C.c} to count the number
of queries made to their oracle by the adversary. We define the
advantage of an adversary $\A$ in distinguishing \ec{O}$_b$\ec{.o}$_L$
from \ec{O}$_b$\ec{.o}$_R$ as the difference of the probability of
games \ec{L}$_n$\ec{(O}$_b$\ec{,}$\A$\ec{)} and
\ec{R}$_n$\ec{(O}$_b$\ec{,}$\A$\ec{)} returning 0. Given any
distinguishing adversary $\A$, we construct a distinguishing adversary
$\B$ that may use $\A$ but always makes at most one query to oracle
\ec{O.o}. 

The \ec{Hybrid} lemma relates the advantages of any
adversary $\A$ with the advantage of its constructed adversary $\B$
when $\A$ is known to make at most \ec{q} queries to \ec{O.o}. Note
that the validity of the \ec{Hybrid} lemma is restricted to
adversaries that do not have a direct access to the counter \ec{C.c},
or to the memories of \ec{B} and \ec{O}$_b$; this is denoted by the
notation $\Ad^{\mathsf{Hy}}$\ec{\{C,B,O}$_b$\ec{\}} in the \EasyCrypt
code. Other lemmas shown in this paper also have such restrictions in
their formalizations, but they are as expected (that is, they simply
enforce a strict separation of the various protocols', simulators' and
adversaries' memory spaces) and we omit them for clarity.
%
The construction of $\B$ is generic in the underlying
adversary $\A$, which can remain completely abstract. We underline
that, for all $\A$ implementing module type $\Ad^{\mathsf{Hy}}$, the
partially-applied module $\B(\A)$ implements $\Ad^{\mathsf{Hy}}$ as
well and can therefore be plugged in anywhere a module of type
$\Ad^{\mathsf{Hy}}$ is expected. This ability to generically construct
over abstract schemes or adversaries is central to handling modularity
in \EasyCrypt.

Finally, we observe that the \ec{Hybrid} lemma applies even to an adversary that
may place queries to the individual \ec{O}$_b$\ec{.o}$_L$ and
\ec{O}$_b$\ec{.o}$_R$ oracles. It is of course applicable (and is in
fact often applied) to adversaries that do not place such queries.

An application example of the generic hybrid argument is our
proof of security of the oblivious transfer protocol. 
In Figure~\ref{fig:someot}, we describe the
concrete two-party OT protocol in a purely  
functional manner, making explicit local state shared between the various
stages of each party. For example, \ec{step}$_1$ outputs
the sender's local state \ec{st}$_s$, later used by \ec{step}$_3$.

\begin{figure}[th]
%\advance\leftskip-1.15cm
\begin{minipage}[t]{.19\textwidth}
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=-30pt,xrightmargin=0pt,style=easycrypt-pretty]
op step$_1$ ($\arrayVar{m}$:(msg * msg) array) (r:int array * $\mathcal{G}$) =
  let ($\arrayVar{c}$,hkey) = r in
  let st$_s$ = ($\arrayVar{m}$,$g^{\arrayVar{c}}$,hkey) in
  let m$_1$ = (hkey,$g^{\arrayVar{c}}$) in
  (st$_s$,m$_1$).

op step$_2$ ($\arrayVar{b}$:bool array) ($\arrayVar{r}$:$\mathcal{G}$ array) m$_1$ =
  let (hkey,$\arrayVar{gc}$) = m$_1$ in
  let st$_c$ = ($\arrayVar{b}$,hkey,$\arrayVar{r}$) in
  let $\arrayVar[2]{m}$ = if $\arrayVar{b}$ then $\arrayVar{gc}$ / $g^{\arrayVar{r}}$ else $g^{\arrayVar{r}}$ in
  (st$_c$,$\arrayVar[2]{m}$).
\end{lstlisting}
\end{minipage}%
\rule[-3cm]{.1pt}{2.5cm}\,
\begin{minipage}[t]{.15\textwidth}
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
op step3 st$_s$ (r:$\mathcal{G}$) $\arrayVar[2]{m}$ =
  let ($\arrayVar{m}$,$\arrayVar{gc}$,hkey) = st$_s$ in
  let $\arrayVar{e}$ = (H(hkey,$\arrayVar[2]{m}^\mathsf{r}$) $\oplus$ $\arrayVar{m}_0$,
            H(hkey,($\arrayVar{gc} / \arrayVar[2]{m}$)$^\mathsf{r}$) $\oplus$ $\arrayVar{m}_1$) in
  let m$_3$ = ($g^\mathsf{r}$,$\arrayVar{e}$) in
  m$_3$.

op finalize st$_c$ m$_3$ =
  let ($\arrayVar{b}$,hkey,$\arrayVar{x}$) = st$_c$ in
  let ($\arrayVar{gr}$,$\arrayVar{e}$) = m$_3$ in
  let res = H(hkey,$\arrayVar{gr}^{\arrayVar{x}}$) $\oplus$ $\arrayVar{e}_{\arrayVar{b}}$ in
  res.
\end{lstlisting}
\end{minipage}
\rule{5cm}{.1pt}
\begin{minipage}{.4\textwidth}
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
clone OTProt as SomeOT with
  type rand$_1$ = $\mathcal{G}$ array,
  type rand$_2$ = ($\mathcal{G}$ array * $\mathcal{G}$) * $\mathcal{G}$,
  op prot ($\arrayVar{b}$:input$_1$) ($\arrayVar[c]{r}$:rand$_1$) ($\arrayVar{m}$:input$_2$) (r$_s$:rand$_2$) =
    let (st$_s$,m$_1$) = step1 $\arrayVar{m}$ (fst r$_s$) in
    let (st$_c$,$\arrayVar[2]{m}$) = step2 $\arrayVar{b}$ $\arrayVar[c]{r}$ m$_1$ in
    let m$_3$ = step3 st$_s$ (snd r$_s$) $\arrayVar[2]{m}$ in
    let res = finalize st$_c$ m$_3$ in
    let conv = (m$_1$,$\arrayVar[2]{m}$,m$_3$) in
    (conv,(res,())).
\end{lstlisting}
\end{minipage}
\caption{A Concrete Oblivious Transfer Protocol.}
\label{fig:someot}
\end{figure}

We prove this protocol secure in the standard model via a
reduction to the decisional Diffie-Hellman assumption and an
entropy-smoothing assumption on the hash function.
%
We let $\mathsf{Adv}^{\mathsf{DDH}}(\A)$ and
$\mathsf{Adv}^{\mathsf{ES}}(\A)$ be the advantage of an adversary
$\mathcal{A}$ breaking the DDH and the Entropy Smoothing assumptions,
respectively.

\begin{theorem}[$\mathsf{OT}$-security of \textsf{SomeOT}]
\label{thm:SomeOT-SIMCPA}
For all $i \in \{1,2\}$ and $\mathsf{OT}_i$ adversary $\A_i$ of type
$\Ad^\mathsf{OT}_i$ against the \textsf{SomeOT} protocol, we can
construct two efficient adversaries $\mathcal{D}^{\mathsf{DDH}}$ and
$\mathcal{D}^{\mathsf{ES}}$, and a efficient simulator $\Sim$ such
that
$$
\mathsf{Adv}^{\mathsf{OT}_i}_{\mathsf{SomeOT},\Sim}(\A_i) \leq \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{DDH}}(\mathcal{D}^{\mathsf{DDH}}) + \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{ES}}(\mathcal{D}^{\mathsf{ES}}).
$$
\end{theorem}

In the proof of Theorem~\ref{thm:SomeOT-SIMCPA}, both reductions first go to
$n$-ary versions of the DDH and Entropy-Smoothing hypotheses before reducing
these further to standard assumptions using the generic hybrid argument 
lemma.

\heading{Extraction and Micro Benchmarks}
Our verified implementation of Yao's protocol is obtained via the
extraction mechanism included in recent versions of
\EasyCrypt.
The only exceptions to this are the low-level operations left 
abstract in the formalisation, namely:
\begin{inparaenum}[i.]
\item abstract core libraries for randomness generation,
   the cyclic group algebraic structure, a PRF relying on 
   AES and the entropy-smoothing hash of \ec{SomeOT}. These are
   implemented using the \textsf{CryptoKit}
   library;\footnote{See \url{http://forge.ocamlcore.org/projects/cryptokit/}} and
\item a wrapper that handles parameter passing (circuits, messages and 
   input/output) and calls the extracted SFE code.
\end{inparaenum}
%
We fix the bound $\mathsf{c}$ on circuit sizes to be the largest
\OCaml integer ($2^{k\mbox{-}1}\mbox{-}\,1$ on a $k$-bit machine) 
allowing us to represent circuits without having to use arbitrary
precision arithmetic whilst remaining large enough to encode all
practical circuits. We use this same value to instantiate~$n$.

We conclude this section with microbenchmarking results focusing only on the
extracted OCaml implementation.  Our results show that, whilst being slower
than (unverified) optimized implementations of SFE that use similar
cryptographic techniques~\cite{HEKM11,BHKR13,Wang2017,cryptoeprint:2016:458},
the performance of the extracted program is compatible with real-world
deployment, providing evidence that the (unavoidable) overhead implied by our
code extraction approach is not prohibitive.
%
The overhead of our solution is not intrinsic to the verification 
and extraction methodology.
Indeed, the more modern (unverified) implementations showing significant
improvements rely on either cryptographic
optimizations~\cite{cryptoeprint:2016:458} or on new SFE
protocols~\cite{Wang2017}.
Moreover, although these changes have implications on the security
proofs, these can be addressed using the same techniques presented here to
obtain a verified implementation that benefits from these recent cryptographic
advances.

In addition to the overall execution time of the SFE protocol and the
splitting of the processing load between the two involved parties, we
also measure various speed parameters that permit determining the
weight of the underlying components: the time spent in executing the
OT protocol, and the garbling and evaluation speeds for the garbling
scheme.
%
Our measured execution times do not include serialization and
communication overheads nor do they include the time to sample the
randomness, all of which we account for in Section~\ref{sec:bench}.

Our measurements are conducted over circuits made publicly available
by the cryptography group at the University of Bristol,%
\footnote{\url{http://www.cs.bris.ac.uk/Research/CryptographySecurity/MPC/}}
precisely for the purpose of enabling the testing and benchmarking of
multiparty-computation and homomorphic encryption implementations. A
simple conversion of the circuit format is carried out to ensure that
the representation matches the conventions adopted in the
formalization.
%%
We run our experiments on an x86-64 Intel Core i5 clocked at 
2.4 GHz with 256KB L2 cache per core. The extracted code and parser are
compiled with {\sf ocamlopt} version 4.02.3. The tests are run in isolation, using
the \OCaml {\sf Sys.time} operator for time readings.  We run tests in
batches of 100 runs each, noting the median of the times recorded in
the runs.

\begin{table}
\scriptsize
\caption{Timings (ms): {\sf P1} and 
{\sf P2} denote the parties, {\sf S1} and {\sf S2} the SFE 
protocol stage; {\sf TTime} denotes total time, {\sf OT} 
the time for OT computation, {\sf GT} the garbling time and 
{\sf ET} the evaluation time.}
\label{tb:results}
\centering

\sffamily
\begin{tabular}{|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|}
\hline
{Circuit}	& 	{NGates}	& 	{TTime}	& 	{P2 S1 GT}	& 	{P2 S1 OT}	& 	{P1 S1 OT}	& 	{P2 S2 OT}	& 	{P1 S2 OT}	& 	{P1 S2 ET}\\
\hline
{\sf COMP32}	& 	301	& 	272	& 	1	& 	54	& 	53	& 	109	& 	53	& 	0.3\\
{\sf ADD32}	& 	408	& 	275	& 	1	& 	55	& 	54	& 	112	& 	53	& 	0.5\\
{\sf ADD64}	& 	824	& 	545	& 	3	& 	109	& 	107	& 	217	& 	106	& 	1\\
{\sf MUL32}	& 	12438	& 	329	& 	44	& 	98	& 	54	& 	111	& 	54	& 	10\\
{\sf AES}	& 	33744	& 	1233	& 	118	& 	345	& 	216	& 	435	& 	215	& 	24\\
{\sf SHA1}	& 	106761	& 	2638	& 	349	&  780 & 	431	& 	868	& 	430	& 	77\\			\hline
\end{tabular}
\end{table}

A subset of our results is presented in Table~\ref{tb:results}, for
circuits {\sf COMP32} (32-bit signed number less-than comparison),
{\sf ADD32} (32-bit number addition), {\sf ADD64} (64-bit number
addition), {\sf MUL32} (32-bit number multiplication), {\sf AES} (AES
block cipher), {\sf SHA1} (SHA-1 hash algorithm). The semantics of the
evaluation of the arithmetic circuits is that each party holds one of
the operands. In the {\sf AES} evaluation we have that {\sf P1} holds
the $128$-bit input block, whereas {\sf P2} holds the $128$-bit secret
key. Finally, in the {\sf SHA1} example we model the (perhaps
artificial) scenario where each party holds half of a $512$-bit input
string. We present the number of gates for each circuit as well as the
execution times in milliseconds.
%%
A rough comparison with results for unverified implementations of the same
protocol such as, say, that in~\cite{HEKM11} where an execution of the AES
circuit takes roughly 1.6 seconds (albeit including communications overhead and
randomness generation time), allows us to conclude that real-world applications
are within the reach of the implementations our approach generates.
Furthermore, additional optimization effort can lead to significant performance
gains, e.g., by resorting to hardware support for low-level cryptographic
implementations as in~\cite{BHKR13, Wang2017}, or implementing garbled-circuit
optimizations such as those allowed by XOR gates or component based
garbled-circuits~\cite{KS08, cryptoeprint:2016:458}.
%%
Indeed, we do not aim or claim to produce the fastest implementation of Yao's
protocol, but simply to demonstrate that the new formal verification techniques
that we introduce open the way to verifying a whole new class of provable
security arguments, where modularity, abstraction, and composition (e.g.,
hybrid arguments) mechanisms are essential to dealing with scale and
complexity. 
%

