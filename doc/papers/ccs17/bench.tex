% !TeX root=main.tex

\section{SFE software stack evaluation}
\label{sec:bench}

In this section we present a performance evaluation of the entire SFE
software stack based on FRESCO.  The FRESCO framework is able to read
circuit descriptions in the format produced by \CircGen. We thus use
the Boolean circuits generated in the micro-benchmarks reported in the
previous section to feed two protocol suites supported by this
framework.%
\footnote{For CBMC-GC outputs we implement a circuit translator
that preserves gate counts modulo the introduction of a small number
of output gates, which are required by the FRESCO input format.}  The
results are given in Table~\ref{tab:compcbmct}.

The first protocol we test is the verified implementation of Yao's
protocol described in Section~\ref{sec:ecproof}, which has been
integrated into FRESCO as a new protocol suite (shown in the table
as \textsf{Yao}). The second is the Tiny Tables protocol
of~\cite{DBLP:journals/iacr/DamgardNNR16}, which is provided in the
vanilla distribution of FRESCO; this protocol operates in the
preprocessing model, and includes XOR-specific optimizations. An
interesting feature of FRESCO is the ability to run the same
circuit transparently in either protocol, 
simply by changing the configured suite. 
The times shown are the longest execution time for a party
participating in the protocol, using the host-local communications
infrastructure that is used for testing the FRESCO framework.
%%
The linear evaluation time of our verified implementation of Yao's
garbled circuit protocol verified implementation is visible in the
data.  The amortized execution time per gate is just under 100
$\mu$s (this ratio is not shown in the table; it is essentially
a constant for all circuits).
%%
For the Tiny Tables protocol we present the online computation time
(\textsf{TT onl}) and the amortized execution time per gate
(\textsf{AT pg}). Here variations are caused by the optimizations that
make the evaluation on non-XOR gates less costly.  To make this
evident, we also include in the table the ratio between the number of
non-XOR gates and the total number of gates (\textsf{$\neg$XOR}).
Indeed, in addition to faster overall execution times due to the
preprocessing trade-offs allowed by this protocol, one can see that
for circuits with a lower percentage of non-XOR gates the amortized
execution time per gate drops to as little as 40 $\mu$s per
gate.

We stress that the goal here is {\em not} to compare the speed 
of Yao's protocol with Tiny Tables: this would be meaningless
not only because these protocols offer incomparable security
guarantees, but also because the two implementations have
significantly different characteristics.
%%
Indeed, the fact that FRESCO operates over Java has obvious 
performance costs. These are somewhat mitigated for our verified 
implementation of Yao's protocol, which is running natively.
%%
However, this is not the case for the pre-existing Tiny Tables
implementation, and so it is most likely that even faster execution
times could be achieved for the same circuits in other MPC frameworks.
%%
Our true goal by presenting these results is to demonstrate integration of the
software artifacts that we have developed into a pre-existing
open-source framework, and to illustrate the relative benefits of the
verified and optimized Boolean circuits produced by our compiler.

\begin{table*}[htbp]
\scriptsize
\caption{CBMC-GC vs CircGen vs Optimized CircGen: Timings (ms) for two FRESCO suites.}
\label{tab:compcbmct}
\centering
\sffamily

\begin{comment}
\begin{tabular}{|l|r|r|r|r|r|r|r|r|}
\hline
& \multicolumn{4}{|c|}{Gate Counts} & \multicolumn{2}{|c|}{Verified Yao Suite}	&	\multicolumn{2}{|c|}{Tiny Tables Suite}	\\
& \multicolumn{2}{|c}{CBMC-GC}	&	\multicolumn{2}{c|}{CircGen Opt.}	&	\multicolumn{1}{|c}{CBMC-GC}	&	\multicolumn{1}{c|}{CircGen Opt.} 	&	\multicolumn{1}{|c}{CBMC-GC}	&	\multicolumn{1}{c|}{CircGen Opt.}  \\
\hline
Computation & Non XOR & Total & Non XOR & Total	& Total & Total & Online & Online \\
\hline
arith100  & 16'143 &  46'215  & 12'657 &  43'361 & 5'196	&	5'047 &  3'224&  2'549\\
hamming1600  & 5'494  &  22'796   & 6'995  &  32'156 & 6'252	& 6'412 & 1'449 &  1'649\\
median21 & 40'320 &  67'050  & 10'560 &  46'914 & 6'205	&	4'540 &  6'658 &   2'057\\
matrix3x3  & 32'868 &  85'986  & 27'369 &  79'310 & 7'685	&	7'067 &  5'568 &  4'882\\
aes128-opt  & 6'400  &  30'828 & 6'400  &  31'338 &	2'836 	&	2'901	&  1'412	&	 1'182 \\
sha256  & 28'571 &  114'169  & 25'667 &  116'181 & 	9'943 &	9'879 &  5'157  &	 4'772\\
\hline
\end{tabular}
\end{comment}

\begin{tabular}{lrrrrrrrrrrrr}
\toprule
& \multicolumn{4}{c}{CBMC-GC} & \multicolumn{4}{c}{CircGen}	&	\multicolumn{4}{c}{CircGen Opt.}	\\
  \cmidrule(lr){2-5}
  \cmidrule(lr){6-9}
  \cmidrule(lr){10-13}
Computation	& 	Yao	& $\neg$XOR	& 	TT onl	& 	AT pg	& 	Yao	& $\neg$XOR	& 	TT onl	& 	AT pg	& 	Yao	& 	$\neg$XOR	& 	TT onl	& 	AT pg\\
\midrule
arith100	& 	5590	& 	35\%	& 	3260	& 	0,071	& 	6710	& 	27\%	& 	3390	& 	0,054	& 	5196	& 	29\%	& 	2549	& 	0,059\\
hamming1600	& 	5533	& 	24\%	& 	1411	& 	0,062	& 	12038	& 	25\%	& 	4997	& 	0,048	& 	6252	& 	22\%	& 	1649	& 	0,051\\
median21	& 	6204	& 	60\%	& 	6756	& 	0,101	& 	4801	& 	23\%	& 	2367	& 	0,047	& 	4540	& 	23\%	& 	2057	& 	0,044\\
matrix3x3	& 	7689	& 	38\%	& 	5712	& 	0,066	& 	7700	& 	33\%	& 	5297	& 	0,061	& 	7067	& 	35\%	& 	4882	& 	0,062\\
aes-opt		& 	2836	& 	21\%	& 	1543	& 	0,050	& 	32935	& 	23\%	& 	15997	& 	0,041	& 	2901	& 	20\%	& 	1182	& 	0,038\\
sha256		& 	9943	& 	25\%	& 	5157	& 	0,045	& 	17309	& 	21\%	& 	7642	& 	0,038	& 	9879	& 	22\%	& 	4772	& 	0,041\\
\bottomrule
\end{tabular}
\end{table*}
