* Review #1
** Insists on memory limitations 
   *** Even after rebuttal, he insists that these limitations make
       paper less exciting
   *** We can state at future work that the development of an
       auxiliary mechanisms (like a tool or a new step in the pipeline)
       could allow one to pre-process code without those restrictions into
       code readable by our CircGen.
** C limitations
   *** State that our C limitations are aligned with state-of-art
   limitations
** CBMC comparison
   *** Justify the comparison with CBMC instead of other tools
** AddInputs question
   *** Add a simple line just saying that the circuits generated are
   party agnostic. 

* Review #2
** Reference to "Verified Compilation of space-efficient reversible circuits"
   *** How to compare circuits outputted by the two tools?
   *** The types of circuits are different
   *** They present their benchmarks by means of bits, gates and
       Toffolis gates while we benchmark by means of non XOR gates and
       total gates.
   *** However, their paper focus more on the development of the
       compiler, verified in F*. A possible comparison between our
       verification effort and theirs would not be interesting?
** Memory regions
   *** Add the rebuttal explanation to the paper
** Verification of the optimisations
   *** Enforce the ongoing verification of the optimisations that will
       then make the tool practical.

* Review #3
** Benchmarks
   *** Interesting to add a comparison between our results and the
       ones included in the papers cited by the reviewer?
   *** Then we can include our rebuttal claim, where we define our
       priority as not being to provide the fastest Yao
       implementation
   *** Enforce that the two Yao implementations rely on hardware
       optimisations and that such optimisations would approximate our
       results to theirs
   *** Yet, refer that it would have great impact in the security
       proofs

* Review #4
** Construction around C
   *** The reviewer state that building the stack around C induces a
       series of limitations that reduce the scope of the C language
       readable by CircGen
   *** Also, he/she states that the CircGen part seems a tutorial.
   *** I think that enforcing that our limitations are aligned with
       state-of-art tools is enough to cover this matter.
** Design a nice figure that sums up our stack

* General TODOS
** We are now above the 12 page limit
** Either we write under EN-GB or EN-US
   *** For example, in the paper appears both "formalisation" and
       "formalization", as well as "optimisation" and "optimization"
