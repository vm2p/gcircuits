\documentclass[letterpaper,11pt]{llncs} 
\usepackage{fullpage}
\usepackage[hmargin=0.8in,vmargin=1in]{geometry}

\pagestyle{plain}

\usepackage[pdftex,pdfpagelabels,colorlinks=true,linkcolor=darkblue,urlcolor=darkblue,citecolor=darkgreen]{hyperref}
\usepackage{varwidth} 
\usepackage{amsmath} 
\usepackage{amssymb}
\usepackage{amsfonts} 
%\usepackage{calrsfs}
%\usepackage{mathrsfs} 
\usepackage{soul} 
\usepackage[section]{placeins}
\usepackage[oldenum,olditem]{paralist} 
\usepackage{graphicx}
\usepackage{todonotes}
\usepackage{xspace}
\usepackage{wrapfig}
\usepackage{color} 
\usepackage{comment}

\definecolor{darkblue}{rgb}{0,0,0.5} 
\definecolor{darkred}{rgb}{0.4,0,0} 
\definecolor{darkgreen}{rgb}{0,0.5,0} 

\newcommand{\mbb}[1]{{\bf{\color{orange}{(mbb)~#1}}}} 

\newcommand{\heading}[1]{{\vspace{6pt}\noindent\sc{#1.}}} 
 \DeclareMathOperator{\sample}{\mathrel{\leftarrow\mkern-14mu\leftarrow}} 

\title{Secure Function Evaluation Stack}
\author{Manuel~Barbosa\inst{1}}
\institute{HASLab - INESC TEC, DCC FC University of Porto, Portugal\\[4pt]
\email{mbb$@$dcc.fc.up.pt}}


\begin{document}

\maketitle

\section{Secure Function Evaluation}

A Secure Function Evaluation protocol allows two parties $P_1$ and $P_2$ to compute the value
of a function on their private inputs, whilst learning only their intended outputs. 
When the function itself is a private input from one of the parties, then one usually refers 
to such a protocol as a Private Function Evaluation (PFE) protocol. Here we will not make
such a distinction, since the two can be easily shown to be equivalent via generic techniques.
Our formalization is, however closer to PFE, since the function to be computed is only given
explicitly to one of the parties as input, whereas the other party may or may not get it
depending on a parameterizable leakage function.

We will look at SFE in the passive honest-but-curious setting, where one of the parties may
be malicious, but nevertheless follows the rule of the protocol. For this reason, we can also
consider the simpler case where only one of the parties receives an output (the one not 
getting the function as an input). In the PFE honest-but-curious setting, such a protocol 
can easily be converted into one that provides (secret) outputs to both parties as follows:
i. the party obtaining the function as input modifies it so that it returns its part of the
output hidden by freshly sampled a one-time pad; ii. the parties then execute the protocol;
iii. the party recovering the full output sends back the one-time-padded part of the output
to the party that created the modified function; and iv. this party recovers the output.

\heading{Syntax}
We view a two-party protocol as specified by a pair $\Pi = (\Pi_1, \Pi_2)$ of polynomial-time
algorithms. Party $P_i$ for $i \in \{1, 2\}$ will run $\Pi_1$ on its current state and the 
incoming message from the other party to produce an outgoing message, a local output, and a 
decision to halt or continue. The initial state of party $P_i$ consists of the unary 
encoding $1^\lambda$ of the security parameter $\lambda \in \mathbb{N}$ and the (private) 
input $I_i$ of this party, and the interaction continues until both parties halt. 

Since we are dealing with passive adversaries, we will simply assume that all communications
are reliable and make the {\em correctness} of the communications layer part of our assumptions.
We define a polynomial-time algorithm $\mathsf{View}_\Pi^i$ that on input $(1^\lambda,I_1,I_2)$
returns the view of party $P_i$ in an execution of $\Pi$ with security parameter $\lambda$
and inputs $I_1$, $I_2$ for the two parties, respectively. This algorithm picks at random
coins $\omega_1$, $\omega_2$, executes the interaction between the parties as determined 
by $\Pi$ with the initial state and coins of party $P_i$, for $i \in \{1, 2\}$ being $(1^\lambda, 
I_i )$ and $\omega_i$ respectively, and returns $(conv, \omega_i)$ where the conversation
$conv$ is the sequence of messages exchanged. We let 
$\mathsf{Out}^i_\Pi(1^\lambda, I_1,I_2)$ return the local output of party $P_i$ at
the end of the protocol. This is a deterministic function of $\mathsf{View}_\Pi^i(1^\lambda, I_1,I_2)$.

\heading{Inputs and Functions} 
In our formalization of security we provide party $P_1$ with input $I_1=x_1$, whereas party
$P_2$ obtains $I_2=(x_2,f)$, where $f$ is the (possibly private) function to compute and $x_2$
is a (possibly empty) additional input to $f$.
The outcome of the protocol should be that party $P_1$ learns $f(x_1,x_2)$. 
Security requires that party $P_2$ learns nothing about $x_1$ (beyond its length) and party $P_1$
learns nothing about $(f,x_1)$ (beyond side information we are willing to leak, 
which may include the entire description of $f$, or partial information such as the number of gates in
a circuit that implements $f$ and the length of $x_1$).

So far we have been purposefully vague about what a function $f$ is and what an input $(x_1,x_2)$ is.
We will consider two ways of defining such values:
\begin{description}
	\item[Boolean Circuits] Here both $f$, $x_1$, and $x_2$ will be seen as bit strings following the standard
	convention in cryptography. $f$ will describe a circuit $C \in \mathsf{BC}$ taking as input bit strings $x_1x_2)$
	of length $n = n_1 + n_2$,  where $n_1 = |x_1|$ and $n_2=|x_2|$, and producing an output bit string
	$y$ $\mathsf{sem}$of length $m = |y|$. When representing functions as circuits, we will refer to a polynomial-time 
	deterministic map $\mathsf{BC.eval}$ that associates to any string $f$ a function 
	$\mathsf{BC.eval}(f,\cdot,\cdot) : \{0, 1\}^{n_1} \times \{0, 1\}^{n_2} \times \{0, 1\}^m$
	that for any input $(x_1,x_2)$ of the correct type returns $C(x_1x_2)$, where $C$ is the circuit
	described by $f$.

    \item[C programs] Here $f$ is a program $P \in \mathsf{CP}$ written in a well-defined subset of the C language and
    $x_1$ and $x_2$ are arrays  of lengths $l_1$ and $l_2$, respectively, containing $32$-bit strings
    which we will associate with the type $\mathsf{uint}32$.
    Such programs return an array $y$ of length $k$ containing values of type $\mathsf{uint}32$ as well.
    When representing functions as C programs, we will refer to a 
	deterministic semantics $\mathsf{CP.eval}$ that associates to any program $P$ an efficiently computable
	function $\mathsf{CP.eval}(P,\cdot,\cdot) : \mathsf{uint}32^{l_1} \times \mathsf{uint}32^{l_2} \times 
	\mathsf{uint}32^k$ that for any input $(x_1,x_2)$ of the correct type returns $P(x_1,x_2)$, the
	final state of program $P$ when this is run with initial state $(x_1,x_2)$ according to the semantics
	$\mathsf{sem}$.
\end{description}

The reason for this distinction is that we will be looking at a secure function evaluation software
stack, rather than the classical view of secure function evaluation protocols. 
This better reflects what happens in the real world, where functions to be computed by SFE protocols
are first described in high-level languages, and then converted into circuit form.
To this end, we will formalize a folklore result that precisely defines what properties should be
satisfied by the components in such a stack so that the overall system satisfies the intuitively
expected (but now made formal) notion of security.

\heading{Correctness}
Formally a secure function evaluation protocol is a tuple $\mathcal{F} = (\Pi, \mathsf{eval})$ where $\Pi$ 
is a $2$-party protocol as above and $\mathsf{eval}$ is either $\mathsf{BC.eval}$ or $\mathsf{CP.eval}$.
The correctness requirement is that for all $f$, $x_1$  and $x_2$ of the appropriate types
we have
\[
\Pr \left[ \, \mathsf{Out}^1_\Pi(1^\lambda,x_1,(x_2,f)) = \mathsf{eval}(f,x_1,x_2) \, \right] = 1~.
\]
We assume that $\mathsf{eval}$ returns $\perp$ whenever its inputs are not well typed.

\heading{Security}
The security notion we consider is privacy in the honest-but-curious setting, meaning the parties follow
the protocol and the intent is that their views do not allow the computation of any undesired information.
We present this notion in game-based form following~\cite{CCS:BelHoaRog12}.

An adversary $\mathcal{A}$ is allowed a single $\mathsf{GetView}$ query in game $\mathsf{SFE}_{\mathcal{F},i,\Phi,\mathcal{S}}$ of Figure~\ref{fig:getview}, and its advantage is
\[
\mathsf{Adv}^{\mathsf{sfe},\Phi,\mathcal{S}}_{\mathcal{F},i}(\mathcal{A},\lambda) = 2 \cdot \Pr \left[ \, \mathsf{SFE}_{\mathcal{F},i,\Phi,\mathcal{S}}(\lambda) \Rightarrow \mathsf{true} \, \right] -1~.
\]
We say that $\mathcal{F}$ is $\mathsf{sfe}$-secure relative to $\Phi$ if for each $i \in {0, 1}$ and each polynomial-time adversary $\mathcal{A}$ there is a polynomial-time simulator $\mathcal{S}$ such that the advantage function above is negligible in $\lambda$.

\begin{figure}[t]
\centering
\framebox{
\begin{minipage}{.7\textwidth}
\underline{{\bf proc.} $\mathsf{GetView}(x_1,x_2,f)$}\\
\begin{tabular}{l}
$b \sample \{0, 1\}$\\
if $\mathsf{eval}(x_1,(x_2,f))=\perp$ then return $\perp$\\
if $b=1$\\
then $view \sample \mathsf{View}_\Pi^i(1^\lambda, x_1,(x_2,f))$\\
else if $i = 1$\\
\phantom{else} then $view \sample \mathcal{S}(1^\lambda, x_1, \mathsf{eval}(f,x_1,x_2), (|x_2|,\Phi(f)))$\\
\phantom{else} else $view \sample \mathcal{S}(1^\lambda, (x_2,f),|x_1|)$\\
return $view$\\
\end{tabular}
\end{minipage}
}
\caption{Game for defining the $\mathsf{sfe}$ security of an SFE scheme $\mathcal{F} = (\Pi, \mathsf{eval})$. Procedure $\mathsf{Finalize}(b
')$ returns $(b = b')$. The game depends on a security parameter $\lambda\in \mathbb{N}$.}
\label{fig:getview}
\end{figure}

\section{Certified Boolean Circuit generation from C programs}

Our secure function evaluation stack will be defined with respect to function specifications written in C, but they will rely on SFE protocols that evaluate Boolean circuits. The bridging component between the two
is a certified compiler that generates Boolean circuit descriptions from
C programs.
More precisely, we will show that the following functional correctness property suffices to obtain a secure and correct SFE stack evaluating C programs from a secure and correct SFE protocol evaluating Boolean Circuits.

\begin{definition}[Semantics Preservation]
A semantics preserving C-to-Boolean-Circuit compiler $\mathsf{Comp}$ is an efficiently computable map from $\mathsf{CP}$ to $\mathsf{BC}$ such that, for all $I_1 = x_1 \in \mathsf{w}32^{l_1}$, $I_2 = (x_2, P) \in \mathsf{w}32^{l_2} \times \mathsf{CP}$ we have 
\[
y = \mathsf{CP.eval}(P,x_1,x_2) \, \wedge \, 
C = \mathsf{Comp}(P) \, \Rightarrow \,  
\langle y \rangle = \mathsf{BC.eval}(C,\langle x_1x_2\rangle)~,
\]
where $\langle \cdot \rangle$ is the natural bijection between arrays of $32$-bit
words and bit strings.
\end{definition}

With this definition in hand we can define an SFE protocol for C programs $\Pi$ using an SFE protocol $\Pi'$ for Boolean circuits as follows. If $\Pi' = (\Pi'_1,\Pi'_2)$ then define $\Pi=(\Pi_1,\Pi_2)$ as follows:
\begin{itemize}
	\item Algorithm $\Pi_1$: On input $(1^\lambda,x_1)$, $\Pi_1$ computes $x_1' = \langle x_1\rangle$ and runs $\Pi'_1(1^\lambda,x_1')$. When the protocol terminates outputting $y'$, $\Pi_1$ will output $y$ such that $\langle y \rangle = y'$.
	\item Algorithm $\Pi_2$: On input $(1^\lambda,(x_2,P))$, $\Pi_2$ computes $x_2' = \langle x_2\rangle$, it compiles $C = \mathsf{Comp}(P)$, and then runs $\Pi'_2(1^\lambda,x_2',C)$. 
\end{itemize}
It is easy to see that correctness of this protocol is implied by the semantics preservation of $\mathsf{C}$ and the correctness of $\Pi'$.
The following theorem shows that $\Pi$ is also secure.

\begin{theorem}
If $\Pi'$ is $\mathsf{sfe}$-secure for $\Phi'$, then $\Pi$ will be $\mathsf{sfe}$-secure for
$\Phi$ such that $\Phi(P)=\Phi'(\mathsf{Comp}(P))$.
\end{theorem}
\begin{proof}
The proof follows from the observation that the view of an attacker against $\Pi$, which
is given by its random coins and the messages its exchanges are identical as those for $\Pi'$.
This means that the output of a simulator that works for $\Pi'$ will work equally well for $\Pi$.
It remains to show that one can compute the inputs for a $\Pi'$ simulator from the inputs to a
$\Pi$ simulator. For the case $i=1$, one can compute such inputs as 
$(1^\lambda,\langle x_1 \rangle,(|\langle x_2 \rangle|,\Phi(P)))$, by the definition of $\Phi(P)$.
For the case $i=2$, one can compute such inputs as $(1^\lambda,(x_2,\mathsf{Comp}(P)),|\langle x_1 \rangle|)$. 
\qed
\end{proof}

We note that the above theorem implies a translation of leakage.
For example, if we have a protocol $\Pi'$
that leaks the entire circuit $C$, then we obtain a protocol for C programs that leaks the
circuit produced by $\mathsf{C}$ for the input program $P$.
This clearly contains no more information than $P$ itself and hence implies a protocol that
leaks $P$ as well (the simulator for this new protocol can simply use $P$ to compute 
$C$ and use the original simulator).
On the other hand, if the protocol $\Pi'$ hides everything about circuit $C$
then the protocol for C programs leaks nothing as well.
The case which is less clear is that in which $\Pi'$ leaks partial information about $C$,
namely its topology. In this case, it is not straightforward to give meaning to the
partial information that is revealed about $P$ when one executes the protocol beyond
the fact that one can easily compute it by using $\mathsf{Comp}$.

\bibliographystyle{alpha} 
\bibliography{abbrev3,crypto}

\end{document}
