\begin{frame}
  \frametitle{MPC/SFE overview}
  
  Multi-party computation:
  
  \begin{itemize}
  \item[] Let $f$ be a function accepting $n$ inputs
  \item[] Let $x_1, x_2, ..., x_n$ be the inputs of $n$ parties 
  \end{itemize}

  \alert{Goal}: compute $f(x_1, x_2, ..., x_n)$

  \vspace{10pt}

  Security/correctness properties:
  
  \begin{itemize}
  \item Input privacy: no information about inputs can be disclosed
  \item Correctness: honest parties must not output an incorrect result
  \end{itemize}

  Interesting feature: \alert{elimination of trusted-third party}!

  \vspace{20pt}

  Secure function evaluation: \alert{instance} of MPC (2-PC)

\end{frame}

\begin{frame}
  \frametitle{MPC/SFE as a stack}
  
  \begin{columns}
    
    \begin{column}{.6\textwidth}
      
      \begin{itemize}
      \item Computation over encrypted data requires \alert{specifying a
          computation}
      \item Secure protocols for \alert{very simple tasks}: add/mul,
        and/or, ...
      \item Simple tasks are \alert{composable}!
      \item Complex computations expressed as sequence of simple tasks
      \item Evaluation done by \alert{distributed virtual machine}
        \begin{itemize}
        \item Execute low-level protocols in sequence or in parallel
        \end{itemize}
      \end{itemize}

    \end{column}

    \begin{column}{.4\textwidth}
      
      \begin{center}
        
        \begin{tcolorbox}
          \begin{center}
          High-level language
          \end{center}
        \end{tcolorbox}

        \vspace{-20pt}

        \[
          \begin{CD}
            @VV{Compilation}V\\
          \end{CD}
        \]

        \vspace{-10pt}

        \begin{tcolorbox}
          \begin{center}
          Computation description
          \end{center}
        \end{tcolorbox}

        \vspace{-20pt}

        \[
          \begin{CD}
            @VV{Evaluation}V\\
          \end{CD}
        \]

        \vspace{-10pt}

        \begin{tcolorbox}
          \begin{center}
          Distributed virtual machine
          \end{center}
        \end{tcolorbox}
      \end{center}

    \end{column}

  \end{columns}

\end{frame}

\begin{frame}
  \frametitle{MPC/SFE as a stack: two examples}
  
  \alert{FRESCO} (\url{https://github.com/aicis/fresco}):

  \begin{itemize}
  \item Java-based secure computation framework
  \item Complex computations described as Java structures
  \item Java structures compiled into low-level gate streams
  \item Engine evaluates computations by gradually executing protocols
    for low-level gates
  \end{itemize}

  \alert{SHAREMIND} (\url{sharemind.cyber.ee}):

  \begin{itemize}
  \item Secure computation application-server
  \item Complex computations described as security aware C-like DSL (SecreC)
  \item SecreC compiled into D-S bytecode
  \item Engine executes bytecode: each language instruction mapped
    into a distributed protocol
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{MPC/SFE nowadays}
  
  Nowadays:

  \begin{itemize}
  \item \alert{Mature} technologies
  \item Some systems already being offered to end-users
  \item Companies exploring MPC/SFE
  \end{itemize}

  Challenges:

  \begin{itemize}
  \item Formal verification
    \begin{itemize}
    \item Tools for \alert{program verification} (CompCert, Coq, ...)
    \item Tools for \alert{crypto reasoning} (EasyCrypt, CertiCrypt, ...)
    \item \alert{Combine these tools with MPC stack}?
      \begin{itemize}
        \item Existing technology enough to reason about MPC/SFE?
        \item Theoretical guarantees apply to software instantiation?
      \end{itemize}
    \end{itemize}
  \item Functionality
  \item Correctness
  \end{itemize}

\end{frame}

\begin{frame} \frametitle{Our work: A Fast and Verified Software Stack
    for SFE}
  
  \begin{columns}
    
    \begin{column}{.5\textwidth}
      
      \resizebox{\textwidth}{!}{%
        \begin{tikzpicture}[
          node distance=5mm,
          thing/.style={rectangle, dashed, draw=black!50,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=3.5cm},
          unverif/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum
            width=3.5cm},
          unveriff/.style={rounded rectangle, draw=black!70,
            font=\fontsize{6}{6}\color{black!70}\ttfamily,minimum width=3.5cm},
          verif/.style={rounded rectangle, draw=blue!70,
            font=\fontsize{6}{6}\color{blue!70}\ttfamily,minimum width=3.5cm},
          quasiverif/.style={rounded rectangle, dashed, draw=blue!70,
            font=\fontsize{6}{6}\color{blue!70}\ttfamily,minimum width=3.5cm}
          ]
          \node (ccode) [thing] { C program };
          \coordinate[below=of ccode] (c);
          \node (occomp) [left=of c, quasiverif] { Optimized CircGen } ;
          \node (ccomp) [right=of c, verif] { CircGen };
          \node (bcircuit) [below=of c, thing] { Boolean circuit };
                    
          \node (circreader) [below=of bcircuit, unveriff] { Circuit reader };
          \node (circdesc) [below=of circreader, thing] { Circuit description
          };
                    
          \node (frescoapp) [below=of circdesc, unveriff] { FRESCO application
          };
          \node (veryaosuite) [below=of frescoapp, unveriff] { Verified Yao
            Protocol Suite };
                    
          \node (veryaoeval) [below=of veryaosuite, verif] { Verified Yao
            Evaluator };
          \node (comms) [below=of veryaoeval, thing] { Communications };
          \node (fresco) [below=of comms, unverif,yshift=3.5mm] { FRESCO };
          \node (frescob) [rectangle, draw=black!50, fit={(circreader)
            (circdesc) (frescoapp) (veryaosuite) (veryaoeval) (comms) (fresco) },yshift=1mm] {};
          
          \draw [->,dashed] (ccode.south) -- (ccomp.north) {};
          \draw [->,dashed] (ccode.south) -- (occomp.north) {};
          \draw [->,dashed] (ccomp.south) -- (bcircuit.north) {};
          \draw [->,dashed] (occomp.south) -- (bcircuit.north) {};
          
          \draw [->,dashed] (bcircuit.south) -- (circreader.north) {};
          \draw [->,dashed] (circreader.south) -- (circdesc.north) {};
          \draw [->,dashed] (circdesc.south) -- (frescoapp.north) {};
          \draw [->,dashed] (frescoapp.south) -- (veryaosuite.north) {};
          \draw [->,dashed] (veryaosuite.south) -- (veryaoeval.north) {};
          
          \draw [<->] (veryaoeval.south) -- (comms.north) {};
          
          \draw [->] ([xshift=-1.5cm]circreader.west) -- (circreader.west) node[midway,above,font=\fontsize{6}{6}\color{black!50}\ttfamily] {Inputs};
          \draw [<-] ([xshift=-1.5cm]veryaoeval.west) -- (veryaoeval.west) node[midway,above,font=\fontsize{6}{6}\color{black!50}\ttfamily] {Outputs};
          
        \end{tikzpicture}
      }
    \end{column}
    
    \begin{column}{.5\textwidth}
      
      \begin{center}
        
        C language (subset)

        \vspace{-10pt}

        \[
          \begin{CD}
            @VV{Compilation}V\\
          \end{CD}
        \]

        Boolean circuits

        \vspace{-10pt}

        \[
          \begin{CD}
            @VV{Evaluation}V\\
          \end{CD}
        \]

        Yao SFE protocol
      \end{center}

    \end{column}

  \end{columns}

\end{frame}