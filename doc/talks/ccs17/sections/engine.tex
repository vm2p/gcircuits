\begin{frame} \frametitle{Our approach: Yao SFE protocol verified implementation}
  
  Goal $\Rightarrow$ Yao SFE protocol \alert{verified implementation}
  \begin{itemize}
  \item Formalize Yao SFE protocol (EasyCrypt)
  \item Prove its security (EasyCrypt)
  \item Extract correct-by-construction code (EasyCrypt)
  \end{itemize}

  \vspace{20pt}

  Interesting challenge: \alert{explore verification overhead}
  \begin{itemize}
  \item Cost
    \begin{itemize}
    \item Human resources
    \item Development time
    \end{itemize}
  \item Efficiency
  \end{itemize}

\end{frame}

\begin{frame}[fragile] \frametitle{Yao SFE protocol in a nutshell
    {\footnotesize \alert{(secure against passive adversaries)}}}
  
  \vspace{-10pt}

  \begin{columns}
    
    \begin{column}{.4\textwidth}
      
      \begin{center}
        Alice (holding $x_1$)
      \end{center}

    \end{column}

    \begin{column}{.2\textwidth}
      
    \end{column}

    \begin{column}{.4\textwidth}
      
      \begin{center}
        Bob (holding $x_2$)
      \end{center}

    \end{column}

  \end{columns}

  \begin{columns}
    
    \begin{column}{.4\textwidth}

    \end{column}

    \begin{column}{.2\textwidth}
      
    \end{column}

    \begin{column}{.4\textwidth}
      
      \begin{center}
        1 - Garbles boolean circuit
        \begin{tiny}
          \begin{itemize}
          \item Gate $\mapsto$ Truth table
          \item Input/output wire $\mapsto$ \alert{one
              key per Boolean value}
          \item Encrypt truth table with DKC\\
            G(0,0) = b $\Rightarrow$ \alert{DKC($K_{inL}[0]$, $K_{inR}[0]$,
            $K_{out}[0]$)}
          \item For all gates $\rightarrow$ reuse keys to connect wires
          \end{itemize}
        \end{tiny}

        \vspace{5pt}

        $\xleftarrow{\text{Sends to Alice}}$
      \end{center}

    \end{column}

  \end{columns}

  \vspace{-10pt}

  \begin{columns}
    
    \begin{column}{.3\textwidth}

    \end{column}

    \begin{column}{.4\textwidth}

      \begin{center}
        2 - OT protocol
      \end{center}

      \vspace{-10pt}

      \begin{tiny}
        \begin{itemize}
          \item Alice \alert{only} learns $x_2$ encoding
          \item Bob learns \alert{nothing} about $x_1$ 
        \end{itemize}
      \end{tiny}
      
    \end{column}

    \begin{column}{.3\textwidth}

    \end{column}

  \end{columns}

  \begin{columns}
    
    \begin{column}{.4\textwidth}

      \begin{center}
        3 - Oblivious evaluation
      \end{center}

    \end{column}

    \begin{column}{.2\textwidth}

    \end{column}

    \begin{column}{.4\textwidth}

    \end{column}

  \end{columns}

\end{frame}

\begin{frame} \frametitle{A modular proof of security [BRH'12] (in a
    picture)}
  \begin{figure}
    \centering
  \scalebox{.85}{
    \begin{tikzpicture}[
      node distance=0.5mm,
      construction/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=2.2cm},
      primitive/.style={rounded rectangle, draw=black!50, font=\scriptsize\ttfamily}
      ]
      \node (dmasking) [construction] { Dual Masking };
      \node (prf) [below=of dmasking.south, primitive] { PRF };
      \node (dkc1) [rectangle, draw=black!50, fit={(dmasking) (prf)}] {};
      
      \node (dencrypt) [below=of dmasking.south, construction, yshift=-0.7cm] { Double Encryption };
      \node (ic) [below=of dencrypt.south, primitive] { Ideal Cipher };
      \node (dkc2) [rectangle, draw=black!50, fit={(dencrypt) (ic)}] {};
      
      \node (dkc) [primitive, draw=black!50, right=of dmasking.east,xshift=1cm,yshift=-0.8cm] {Dual Key Cipher};
      
      \draw [->,dashed] (dkc1.east) -- (dkc.175)  node[midway,above] {\tiny 1};
      \draw [->,dashed] (dkc2.east) -- (dkc.185);
      
      \node (g1) [construction,above= of dkc.north,yshift=0.4cm] { Garble1 };
      \node (gscheme1) [rectangle, draw=black!50, fit={(g1) (dkc) (dkc1) (dkc2)}] {};
      
      \node (gscheme) [primitive, draw=black!50, dashed, right=of gscheme1.east,xshift=0.5cm,text width=2.4cm,align=center] {Garbling Scheme\\IND Secure};
      \draw [->,dashed] (gscheme1.east) -- (gscheme)  node[midway,above] {\tiny 2};
      
      \node (gschemesim) [primitive, draw=black!50, below=of gscheme.south,yshift=-0.2cm,text width=2.4cm,align=center] {Garbling Scheme\\SIM Secure};
      \draw [->] (gscheme.south) -- (gschemesim)  node[midway,right] {\tiny 3};
      
      \node (ot) [primitive, draw=black!50, above=of gscheme.north,yshift=0.2cm,text width=2.4cm,align=center] {Oblivious Transfer\\2PPP Secure};
      
      \node (yao) [construction,above= of g1.north,yshift=0.3cm] { Yao's Protocol };
      \node (sfe1) [rectangle, draw=black!50, fit={(gscheme1) (gschemesim) (yao)}] {};
      
      \node (sfe) [primitive, draw=black!50, right=of sfe1.east,xshift=0.5cm,text width=2cm,align=center] {SFE\\2PPP Secure};
      \draw [->,dashed] (sfe1.east) -- (sfe)  node[midway,above] {\tiny 4};
    \end{tikzpicture}
  }
\caption{Yao's protocol security proof by [BHR'12].}
\end{figure}
\end{frame}

\begin{frame} \frametitle{A modular proof of security [BRH'12] (in text)}
  
  Central role $\rightarrow$ new primitive: \alert{garbling scheme}

  \vspace{10pt}

  \begin{itemize}
  \item[1.] Construction of a garbling scheme
    \begin{itemize}
    \item Via DKC scheme
    \end{itemize}
  \item[2.] Construction of DKC
    \begin{itemize}
    \item PRF security assumption
    \item Via \textit{dual-masking} construction
    \end{itemize} 
  \item[3.] IND-SEC $\sim$ SIM-SEC
    \begin{itemize}
    \item For certain classes of garbling schemes
    \end{itemize}
  \item[4.] Secure SFE protocol
    \begin{itemize}
    \item SIM-secure garbling scheme
    \item Secure OT protocol
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame} \frametitle{Our proof (in a picture)}
  
  \begin{figure}
    \centering
  \scalebox{.85}{
    \begin{tikzpicture}[
  node distance=0.5mm,
  implementation/.style={rectangle, fill=black!20, draw=blue!50, 
  font=\fontsize{6}{6}\color{blue!50}\ttfamily,minimum width=2.2cm},
  construction/.style={rectangle,font=\fontsize{6}{6}\color{black!50}\ttfamily,minimum width=2.2cm},
  primitive/.style={rounded rectangle, draw=black!50, font=\scriptsize\ttfamily}
]
  \node (dmasking) [construction] { Dual Masking };
  \node (prf) [below=of dmasking.south, primitive] { PRF };
  \node (dkc1) [rectangle, draw=black!50, fit={(dmasking) (prf)}] {};

  \node (dkc) [primitive, draw=black!50, right=of dkc1.east,xshift=1cm] {Dual Key Cipher'};

  \draw [->] (dkc1.east) -- (dkc.west)  node[midway,above] {\tiny 1};

  \node (g1) [construction,above= of dkc.north] { SomeGarble };
  \node (gscheme1) [rectangle, draw=black!50, fit={(g1) (dkc) (dkc1) }] {};

  \node (gscheme) [primitive, draw=black!50, dashed, right=of gscheme1.east,xshift=0.5cm,text width=2.4cm,align=center] {Garbling Scheme\\IND Secure};
  \draw [->] (gscheme1.east) -- (gscheme)  node[midway,above] {\tiny 2};

  \node (gschemesim) [primitive, draw=black!50, above=of gscheme.north,yshift=0.2cm,text width=2.4cm,align=center] {Garbling Scheme\\SIM Secure};
  \draw [->] (gscheme.north) -- (gschemesim)  node[midway,right] {\tiny 3};
  \draw [->, draw=blue!50] (gscheme1.8) -- (gschemesim.-175)  node[midway,above] {\tiny 5};

  \node (ddh) [above=of dmasking.north, primitive,yshift=1cm,xshift=-.05cm] { DDH };
  \node (es) [right=of ddh.east, primitive,xshift=1cm] { Entropy Smoothing };
  \node (someot) [construction, above=of es.north,xshift=-1cm] { SomeOT };
  \node (ot1) [rectangle, draw=black!50, fit={(ddh) (es) (someot)},minimum width=6.1cm] {};

  \node (ot) [primitive, draw=black!50, right=of ot1.east,xshift=0.5cm,text width=2.4cm,align=center] {Oblivious Transfer\\2PPP Secure};
  \draw [->, draw=blue!50] (ot1.east) -- (ot)  node[midway,above] {\tiny 6};  

  \node (yao) [construction,above= of ot1.north,xshift=1cm] { Yao's Protocol };
  \node (sfe1) [rectangle, draw=black!50, fit={(gscheme1) (gschemesim) (yao) (ot) (ot1)}] {};

  \node (sfe) [primitive, draw=black!50, right=of sfe1.east,xshift=0.5cm,text width=2cm,align=center] {SFE\\2PPP Secure};
  \draw [->] (sfe1.east) -- (sfe)  node[midway,above] {\tiny 4};

  \node (efficient) [implementation,above= of sfe.north,yshift=.72cm] { Implementation };
  \draw [<->, draw=blue!50] (sfe1.15) -- (efficient.west)  node[midway,above] {\tiny 7};
  \draw [->, draw=blue!50] (efficient.south) -- (sfe.north)  node[midway,right] {\tiny 8};

\end{tikzpicture}
  }
\caption{Structure of our verified security proof of an implementation of Yao's protocol.}
\end{figure}

\end{frame}

% \begin{frame}[fragile] \frametitle{Our proof (in text)}
  
%   \begin{itemize}
%   \item[1..4.] Very close to original proof
%     \begin{itemize}
%     \item Deviation: slight simplification of DKC
%     \item Tweak makes encryptions of the same value indistinguishable
%     \item Still satisfies the dual masking instantiation
%     \end{itemize}
%   \item [5.] \alert{Instantiation} of garbling scheme
%     \begin{itemize}
%     \item Existance of PRF instantiation
%     \end{itemize}
%   \item [6.] \alert{Instantiation} of OT protocol
%     \begin{itemize}
%     \item DDH assumption
%     \item Existance of entropy smoothing hash function
%     \end{itemize}
%   \item [7,8.] \alert{Instantiation} of SFE protocol
%     \begin{itemize}
%     \item Existance of garbling scheme
%     \item Existance of an OT protocol
%     \end{itemize}
%   \end{itemize}

%   \begin{theorem}
%     \label{thm:main-theorem}
%     \vspace{-15pt}
%     \[
%       \mathsf{Adv}^{\mathsf{SFE}}_{\mathsf{Impl},\Sim}(\A) \leq%\\
%       \mathsf{c} \cdot \varepsilon_{\mathsf{PRF}} + \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{DDH}}(\B_{\mathsf{DDH}}) +
%       \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{ES}}(\B_{\mathsf{ES}})
%     \]
%   \end{theorem}
  
% \end{frame}

\begin{frame} \frametitle{Proof highlights/challenges}
  
  \begin{itemize}
  \item Compositional reasoning about proofs:
    \begin{itemize}
    \item \alert{Prove Yao's protocol} secure under \alert{abstract} OT and garbling scheme
    \item \alert{Prove garbling scheme construction} secure under \alert{abstract} DKC
    \item \alert{Prove DKC construction} secure under \alert{abstract} PRF
    \item \alert{Prove OT construction} secure under \alert{DDH assumption} and
      \alert{abstract} entropy smoothing hash function
    \item \alert{Derive security for instantiated protocol}
    \end{itemize}
  \item Compositional reasoning in the crypto sense:
    \begin{itemize}
    \item \alert{Hybrid argument over distributions}
    \item \alert{Hybrid argument over wires} in the circuit
    \end{itemize}
  \item Getting a reasonably efficient solution
    \begin{itemize}
    \item \alert{Proof carried out over inefficient specification} of the
      protocol
    \item Specify \alert{efficient protocol to be used in extraction}
    \item Prove the two versions \alert{equivalent}
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame} \frametitle{Yao's SFE protocol verified implementation}
  
  EasyCrypt extraction $\Rightarrow$ \alert{Verified implementation}

  \vspace{10pt}

  Instantiation of abstract components:
  \begin{itemize}
  \item Randomness generation: CryptoKit
  \item Cyclic group algebraic strucutre: CryptoKit
  \item PRF: AES-NI (justGarble)
  \item Entropy smoothing hash function: SHA256 (CryptoKit)
  \end{itemize}

  \begin{center}
    \begin{scriptsize}
      \begin{tabular}{|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|}
        \hline
        {Circuit}	& 	{NGates}	& 	{TTime}	& 	{P2 S1 GT}	& 	{P2 S1 OT}	& 	{P1 S1 OT}	& 	{P2 S2 OT}	& 	{P1 S2 OT}	& 	{P1 S2 ET}\\
        \hline
        {\sf COMP32}	& 	301	& 	65	& 	0.7	& 	13	& 	13	& 	26	& 	13	& 	0.2\\
        {\sf ADD32}	& 	408	& 	68	& 	1	& 	14	& 	13	& 	27	& 	13	& 	0.2\\
        {\sf ADD64}	& 	824	& 	129	& 	2	& 	27	& 	25	& 	51	& 	25	& 	0.5\\
      {\sf MUL32}	& 	12438	& 	105	& 	34	& 	47	& 	13	& 	26	& 	12	& 	7\\
        {\sf AES}	& 	33744	& 	371	& 	96	& 	146	& 	51	& 	104	& 	51	& 	19\\
        {\sf SHA1}	& 	106761	& 	883	& 	315	&  414 & 	102	& 	205	& 	101	& 	61\\			\hline
      \end{tabular}
    \end{scriptsize}
  \end{center}
\end{frame}