require import Bool.
require import Int.
require import Real.
require import Distr.
require import Pair.
require import FMap.
require import Array.
require import Distr.

import ForLoop.
require Monoid.
import Monoid.Mrplus.
require FSet.
import FSet.Interval.

(* Some nice axioms to discharge sometime in the future *) 

axiom range_ext (i j:int) (st:'a) f1 f2:
 (forall k s, i<= k < j => f1 k s = f2 k s) =>
 range i j st f1 = range i j st f2.

axiom sum_mul_distr: forall r (f:'a->real) s,
 r * sum f s = sum (fun k, r*(f k)) s.

lemma sum_ij_mul_distr: forall r (f:int->real) i j,
 r * sum_ij i j f = sum_ij i j (fun k, r*(f k)).
proof. by intros r f i j; rewrite /sum_ij sum_mul_distr. qed.

lemma nosmt neg_def: forall (b:bool), (!b) = (b => false).
proof. by intros b; case b. qed.
lemma nosmt rw_impl_def: forall a b, (a=>b) = (!a \/ b) by [].
lemma nosmt rw_neg_impl: forall a b, (!(a=>b)) = (a /\ !b) by [].

(* TO FMap *)
lemma rm_set_eq (m:('a,'b) map) i x:
 rm i m.[i <- x] = rm i m.
proof.
apply map_ext => k; case (k=i) => Hk; rewrite !get_rm; smt.
qed.

require  Sch.
require  DKCSch.

require import GarbleTools.
import ForLoop.

theory DKCScheme2.

  require ExtWord.
  clone import ExtWord as W.

  op m_bnd : int.
  axiom m_bnd_pos : m_bnd > 0.

  op bound : int.
  axiom boundInf : bound > 1.

  clone DKCSec.DKCSecurity as DKCSec with theory W = W.
  clone import Tweak with theory W = W.

clone DKCSch.DKCScheme as Garble1 with
  theory W = W.

import W.
import Tweak.
import Garble1.Local.

(** Garble2 Scheme definition.
    ========================== **)
clone Sch.Scheme as Garble2 with
  theory Input = Garble1.C2.Input,
  type fun_t = Garble1.C2.fun_t,
  type funG_t = Garble1.C2.funG_t,
  type output_t = Garble1.C2.output_t,
  type outputK_t = (word*word) array,
  type outputG_t = Garble1.C2.outputG_t,
  type leak_t = Garble1.C2.leak_t,
  type rand_t = Garble1.C2.rand_t,
  op validInputs fn x = 
    (Garble1.C2.validInputs fn x) /\ (let (n,m,q,aa,bb) = (fst fn) in m) <= m_bnd,
  pred validRand = Garble1.C2.validRand,
  op phi = Garble1.C2.phi, 
  op eval = Garble1.C2.eval,
  op funG = Garble1.C2.funG, 
  op inputK = Garble1.C2.inputK,
  op outputK (fn:fun_t) (r:rand_t) =
      let (n, m, q, aa, bb) = fst fn in
      init m (fun i, (oget r.[(n+q-m+i,false)], oget r.[(n+q-m+i,true)])),
  op valid_outG (oK:outputK_t) (oG:outputG_t) =
    alli (fun i x, oG.[i] = fst x \/ oG.[i] = snd x) oK,
  op decode (oK:outputK_t) (oG:outputG_t) =
    mapi (fun i x, oG.[i]=snd x) oK,
  op evalG = Garble1.C2.evalG.

  require import SchAuthSec.
  clone import SchAuth with
     theory Scheme = Garble2.
  import Scheme.

  pred Correct (x : unit) = forall r fn i,
    validInputs fn i =>
    validRand fn r =>
    let fG = funG fn r in
    let iK = inputK fn r in
    let oK = outputK fn r in
    let iG = encode iK i in
    let oG = evalG fG iG in
    valid_outG oK oG /\ eval fn i = decode oK oG.


lemma sch_correct : Garble1.DKCSecurity.D.Correct () => Correct ().
proof.
(*Some simplification before proving the main inductive formula *)
simplify Garble1.DKCSecurity.D.Correct Correct.
simplify    validInputs    validRand    eval    decode    outputK    evalG    funG          encode    inputK.
simplify Garble2.validInputs Garble2.validRand Garble2.eval Garble2.decode Garble2.outputK Garble2.evalG Garble2.funG.
simplify Garble1.C2.validInputs Garble1.C2.validRand Garble1.C2.eval Garble1.C2.decode Garble1.C2.outputK Garble1.C2.evalG Garble1.C2.funG.

move=> DKCHyp r fn x.
elim/tuple2_ind fn=> fn topo ff h_fn/=.
elim/tuple5_ind topo h_fn=> topo n m q aa bb h_topo h_fn /=.
simplify fst snd.
rewrite valid_wireinput.
simplify validCircuitP.
simplify fst snd.
move => [[Hmax [[Hn [Hm [Hq [Hmq [Hbound [Haa [Hbb [Hdom Htt]]]]]]]] Hxlen]]]
        Hm_bnd Hcirc.
pose xG := Garble2.Input.encode (Garble2.inputK ((n, m, q, aa, bb), ff) r) x.
cut HxGlen: length xG = n.
  by rewrite /xG /Garble2.inputK /Garble2.Input.encode /Garble1.C2.Input.encode /Garble1.C2.inputK; smt.
pose eval :=(fun (g : int) (x1 x2 : word),
     Garble1.DKCSecurity.D.D (Garble1.Tweak.tweak g (Garble1.Tweak.W.getlsb x1) (Garble1.Tweak.W.getlsb x2)) x1 x2
                 (oget
                    (initGates n q (enc r ((n, m, q, aa, bb), ff))).[(
                    g, Garble1.Tweak.W.getlsb x1, Garble1.Tweak.W.getlsb x2)])).

(* This is the inductive formula that we want to prove *)
cut main : forall (j:int), 0 <= j < n+q =>
   oget r.[(j, (evalComplete q x (extract (fun g x1 x2, oget ff.[(g, x1, x2)]) aa bb)).[j])]
   = (evalComplete q xG (extract eval aa bb)).[j];first last.

 (* End case : As soon as we have this formula for all j we apply it to the output wire and get the final result with the decode and outputK definition *)
 pose validOut := valid_outG _ _; cut HvalidOut: validOut.
  rewrite /validOut /valid_outG /Garble2.valid_outG; rewrite alli_def => k oGi /=.
  rewrite get_init /= ?fst_pair ?snd_pair ; first smt.
  rewrite get_sub; first 4 by smt.
  rewrite -(main (k + (n + q - m)) _); first smt.
  pose xx:= (extract (fun (g : int) (x1 x2 : bool), oget ff.[(g, x1, x2)]) aa bb).
  by case ((evalComplete q x xx).[k + (n + q - m)])=> _; smt.
 split => //.
 apply array_ext; split; first by rewrite Array.length_sub; smt.
 move => j; rewrite Array.length_sub; first 2 smt. 
  rewrite /evalComplete appendInit_length; smt.
 move => Hj /=; rewrite get_mapi; first by rewrite length_init;smt.
 rewrite /= !get_sub;first 8 smt.
 rewrite /= get_init /=; first by smt.
 rewrite -(main (j + (n + q - m)) _); first smt.
 pose v := (evalComplete _ _ _).[_].
 cut := Hcirc (j + (n + q - m)) _; first by smt.
 move=> [Ha [Hb [Hc Hd]]] /=; smt.

(* back to the proof of the main property *)
intros j boundJ.
cut : j < n + q by smt.
cut : 0 <= j by smt; clear boundJ.
elim/ Induction.strongInduction j.
simplify evalComplete.
pose ar1 := (GarbleTools.appendInit x q (GarbleTools.extract (fun (g : int) (x1 x2 : bool), oget ff.[(g, x1, x2)]) aa bb)).
pose ar2 := (GarbleTools.appendInit xG q (GarbleTools.extract eval aa bb)).
intros k kPos hypRec kbound.
case (n <= k)=> hk.
 (* Induction case : use the correction of one gate and the induction hypothesis*)
 rewrite /ar1 /ar2 ! appendInit_getFinal. 
  smt.
  smt. 
  by simplify extract; smt. 
  smt.
  smt.
  by simplify extract; rewrite !get_sub //; expect 4 smt.
 rewrite -/ar1 -/ar2.
 simplify extract.
 cut -> : (k - 1 + 1 = k) by smt.
 rewrite - ! hypRec;first 4 smt.
 simplify eval.
 rewrite get_initGates;first smt.
 simplify enc fst snd.
 move: eval ar2 hypRec; rewrite /Garble1.Tweak.W.getlsb /Garble1.DKCSecurity.W.getlsb => eval ar2 hypRec.
 cut -> : ( Garble1.W.getlsb (oget r.[(aa.[k], ar1.[aa.[k]])]) ^^
            Garble1.W.getlsb (oget r.[(aa.[k], false)])) = ar1.[aa.[k]].
  case ar1.[aa.[k]]=> h;last smt.
  cut := Hcirc aa.[k] _;first clear h;smt.
  generalize (Garble1.W.getlsb (oget r.[(aa.[k], false)]))=> a.
  generalize (Garble1.W.getlsb (oget r.[(aa.[k], true)]))=> b.
  intros [_ [_ [HH _ ]]].
  cut -> : b = ! a by smt;smt.
 cut -> : ( Garble1.W.getlsb (oget r.[(bb.[k], ar1.[bb.[k]])]) ^^
            Garble1.W.getlsb (oget r.[(bb.[k], false)])) = ar1.[bb.[k]].
  case ar1.[bb.[k]]=> h;last smt.
  cut := Hcirc bb.[k] _;first clear h;smt.
  generalize (Garble1.W.getlsb (oget r.[(bb.[k], false)]))=> a.
  generalize (Garble1.W.getlsb (oget r.[(bb.[k], true)]))=> b.
  intros [_ [_ [HH _ ]]].
  cut -> : b = ! a by smt;smt.
 case (n <= k < n + q).
  by progress; rewrite oget_some DKCHyp.
 smt.

(* Base case : prove main for the inputs by using encode and inputK definitions *)
rewrite /ar1 /ar2 ! appendInit_get1;first 4 smt.
rewrite -/ar1 -/ar2.
simplify xG Garble2.Input.encode Garble1.C2.Input.encode Garble2.inputK Garble1.C2.inputK fst.
rewrite get_init /=;first smt.
rewrite FMap.get_filter /=.
by (cut -> : 0 <= k < n = true by smt).
qed.

op fn_m (l:topo_t) : int = let (n, m, q, aa, bb) = l in m.
op fn_n (l:topo_t) : int = let (n, m, q, aa, bb) = l in n.
op fn_q (l:topo_t) : int = let (n, m, q, aa, bb) = l in q.
op fn_aa (l:topo_t) = let (n, m, q, aa, bb) = l in aa.
op fn_bb (l:topo_t) = let (n, m, q, aa, bb) = l in bb.

op outIdx (l:topo_t) (i:int) : int =
 let (n, m, q, aa, bb) = l in n+q-m+i.

lemma fn_topo_rw: forall l,
 l = (fn_n l, fn_m l, fn_q l, fn_aa l, fn_bb l) by smt.

lemma outputK_len (fn: fun_t) (r: rand_t):
 0 <= fn_m (fst fn) =>
 length (outputK fn r) = fn_m (fst fn).
proof.
move => Hm.
by rewrite /outputK /Garble2.outputK (fn_topo_rw (fst fn)) /= length_init.
qed.

lemma encode_len (iK: inputK_t) (x:input_t):
 length (encode iK x) = length x.
proof. by delta=> /=; rewrite length_init; first apply length_pos. qed.

lemma decode_len (oK: outputK_t) (oG:outputG_t):
 length (decode oK oG) = length oK.
proof. by delta=> /=; rewrite length_mapi. qed.

lemma eval_len (fn: fun_t) (x:input_t):
 0 <= fn_m (fst fn) <= fn_n (fst fn) + fn_q (fst fn) =>
 0 <= fn_q (fst fn) =>
 length x >= fn_n (fst fn) =>
 length (eval fn x) = fn_m (fst fn).
proof.
move => H1 H2 H3.
rewrite /eval /Garble2.eval /Garble1.C2.eval {1}(fn_topo_rw (fst fn)) /=.
rewrite Array.length_sub //; first 2 smt.
rewrite /evalComplete appendInit_length //; smt.
qed.


(** Randomness generation
    =====================
*)

(* The reference randomness generation algorithm *)
  module Rand : Rand_t = {
    proc gen(l:topo_t): rand_t = {
      var r : rand_t;
      var n, m, q:int;
      var aa, bb: int array;
      var i: int;
      var trnd: bool;
      var temp1, temp2: word;

      (n, m, q, aa, bb) = l;
      i = 0;
      r = FMap.empty;
      while (i < n+q-m) {
        trnd = $Dbool.dbool;
        temp1 = $Dword.dwordLsb ( trnd);
        temp2 = $Dword.dwordLsb (!trnd);
        r.[(i, false)] = temp1;
        r.[(i, true)] = temp2;
        
        i = i + 1;
      }
      (* i = n+q-m; just to make proofs slightly simpler *)
      while (i < n+q) {
        temp1 = $Dword.dword;
        temp2 = $Dword.dword;
        r.[(i, false)] = temp1;
        r.[(i, true)] = temp2;        
        
        i = i + 1;
      }
      return r;
    }
  }.

lemma Rand_ll: islossless Rand.gen.
proof.
proc.
while (true) (n+q - i).
 by move => z; wp; rnd; rnd; skip; progress; smt.
wp; while (true) (n+q-m - i).
 move=> z; auto;progress. smt. smt. smt. smt.
by wp; skip; progress; smt.
qed.

(** Auxiliary functions: 
    ===================
*)

(* compute the positions of valid forgeries *)
op chkForge (out:bool array) (oK:outputK_t) (oG:outputG_t): bool array =
 init m_bnd (fun i, if i<length oK then oG.[i] = if out.[i]
                                                 then fst oK.[i]
                                                else snd oK.[i]
                                   else false).

op or_array (x: bool array) : bool =
 Monoid.Mbor.sum_ij 0 (m_bnd-1) (fun i, x.[i]).

(** SECURITY PROOF
    ==============

    Proof strategy:
      1) relate the original Authenticity game with a slightly weakened
        form (game G1) which doesn't check the validity of "all" garbled
        output positions (i.e. succeeds if there exist positions with
        valid forgeries).
      2) guess the position/bit-value of the forgery (game G2) --- the
        adversary only wins when it is able to produce a valid forgery in
        that position/bit;
      3) change the garbling of the given function so that the truth table
        for the (guessed) output gate is the constant function (game F).
        The jump is reduced to the "aPriv" computational assumption.
      4) in Fake game, all entries of the forged output gate are equal
        to the bit adversary knows. Adversary advantage is then 
        "2^-tokLen" (information-theoretically).
      5) combine intermediate results to establish the bound
                  Adv(AUT) <= 1/(2*m) * ( Adv(aPriv) + 2^-tokLen )
*)

(*
   Game 1 - Easier authenticity:  same as Game_Auth, but the adversary
     succeeds as soon as some positions of the forged oG has
     valid forgeries 
*)
  module G1(R: Rand_t, A : Adv_Auth_t) = {
    
    proc main() : bool = {
      var fn : fun_t;
      var x : Input.input_t;
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var forgeChk : bool array;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      forgeChk = init m_bnd (fun i, false);
      if (validInputs fn x)
        forgeChk = chkForge (eval fn x) oK oG;

      return or_array forgeChk;
    }
  }.

(* The probability of A winning this game cannot be smaller *)

lemma G1_Auth_equiv:
  forall (A<: Adv_Auth_t{Game_Auth, G1}) (R<: Rand_t{Game_Auth, G1, A}) ,
  equiv [ Game_Auth(R,A).main ~ G1(R,A).main : true  ==> res{1} => res{2}].
proof.
intros A R; proc.
wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
wp; call (_: ={glob A, fG} ==> ={res, glob A}); first by proc true.
wp; call (_: ={l} ==> ={res}); first by proc true.
wp; call (_: true ==> ={res, glob A}); first by proc true.
skip => &1 &2 /= fnL fnR gAL gAR [-> ->] /= rL rR ->.
split => //= H {H} xL xR gAL' gAR' [-> ->] /=.
split => //= H {H} xGL xGR -> /=.
case (validInputs fnR xR) => /=; last by trivial.
rewrite /validInputs /Garble2.validInputs /Garble1.C2.validInputs /validCircuit.
move: (fn_topo_rw (fst fnR)) => -> /=
 [[H1 [[H3 [H4 [H5 [H6 [H7 [H8 [H9 [H10 H11]]]]]]]] H2]] Hm_bnd].
rewrite /valid_outG /Garble2.valid_outG alli_def => [Hval_oG Hforge].
rewrite /or_array /Monoid.Mbor.sum_ij Monoid.or_exists.
cut Hneq: exists k, 0 <= k < fn_m (fst fnR) /\
 (eval fnR xR).[k]<>(decode (outputK fnR rR) xGR).[k].
  move: Hforge; rewrite -Array.rw_array_ext /Array.(==) -nand => [HH|HH].
   cut _:false; last by smt.
   move: HH; rewrite -not_def => HH; apply HH.
   rewrite eval_len; first 3 smt.
   rewrite decode_len outputK_len; smt.
  change (exists (k : int),
  (fun k, 0 <= k < fn_m (fst fnR)  /\
          ! (eval fnR xR).[k]
          = (decode (outputK fnR rR) xGR).[k]) k); apply ex_for.
  move: HH; apply absurd; rewrite !nnot /= => HH i Hi.
  move: (HH i); rewrite - nand nnot => {HH} [HH|HH] //.
  cut _:false; last by smt.
  move: HH; rewrite -not_def => HH; apply HH.
  move: Hi; rewrite eval_len; smt.
elim Hneq => k [Hk Hkforge]; exists k; split; first smt.
rewrite /= /chkForge get_init /=; first smt.
split; first by rewrite outputK_len; smt.
move=> Hk2; move: (Hval_oG k _); first by rewrite outputK_len; smt.
move=> /=[HH|HH]. 
 case ((eval fnR xR).[k]) => Heval //.
 cut: (decode (outputK fnR rR) xGR).[k] by smt.
 rewrite /decode /Garble2.decode get_mapi //.
 rewrite outputK_len; smt.
case ((eval fnR xR).[k]) => Heval //.
cut: !(decode (outputK fnR rR) xGR).[k] by smt.
rewrite /decode /Garble2.decode get_mapi //=.
 rewrite outputK_len; smt.
smt.
qed.

(** Game 2 - guess position/bit of forgery: G2 is like G1 but adversary
    must win on guessed values
*)

  module G2(R: Rand_t, A : Adv_Auth_t) = {
    (* guesses *)
    var pguess: int
    var bguess: bool
    (* positions where A forges the result *)
    var forgeChk: bool array
    var out: bool array
    
    proc game(pguess: int) : bool = {
      var fn : fun_t;
      var x : Input.input_t;
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      out = eval fn x;
      forgeChk = init m_bnd (fun i, false);
      if (validInputs fn x /\ pguess < m_bnd)
        forgeChk = chkForge out oK oG;

      return forgeChk.[pguess];
    }
    proc main() : bool = {
      var r: bool;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      r = game(pguess);
      return r /\ out.[pguess] = bguess;
    }
  }.

(* The event corresponding to success in G1 is also present in G2 *)

lemma G1G2_equiv: forall (A<: Adv_Auth_t) (R<: Rand_t {A}),
   equiv [ G1(R,A).main ~ G2(R,A).main : 
           true  ==> res{1} = (or_array G2.forgeChk){2} ].
proof.
intros A R.
proc; inline G2(R,A).game.
wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
wp; call (_: ={glob A, fG} ==> ={res, glob A}); first by proc true.
wp; call (_: ={l} ==> ={res}); first by proc true.
wp; call (_: true ==> ={res, glob A}); first by proc true.
wp; rnd{2}; rnd{2}; skip; progress; smt.
qed.

(* Splitting the probability of success in G1 (as seen in G2) among flips 
   in the different output bits *)
lemma PrG2_xpnd1 &m: 
forall (A<: Adv_Auth_t) (R<: Rand_t {A}) n,
  n = m_bnd-1 =>
  Pr[G2(R, A).main() @ &m : or_array G2.forgeChk]
  <= sum_ij 0 n (fun k, Pr[G2(R, A).main() @ &m : G2.forgeChk.[k] ]).
proof.
intros A R n Hn.
cut ->: Pr[G2(R, A).main() @ &m : or_array G2.forgeChk]
  = Pr[G2(R, A).main() @ &m : Monoid.Mbor.sum_ij 0 n (fun i, G2.forgeChk.[i])].
 rewrite Pr [mu_eq] => // &1.
 by rewrite /or_array /Monoid.Mbor.sum_ij -eq_iff Hn; congr.
cut HH: 0<=n by smt. clear Hn.
move: HH; elim/Induction.induction n.
 apply eq_le.
 rewrite sum_ij_eq /=.
 rewrite Pr [mu_eq]; trivial => &1.
 by rewrite Monoid.Mbor.sum_ij_eq; smt.
intros k Hk IH.
apply (Trans _ (Pr[G2(R, A).main() @ &m :
                    Monoid.Mbor.sum_ij 0 k (fun i, G2.forgeChk.[i])]
                + Pr[G2(R, A).main() @ &m : G2.forgeChk.[k+1]])).
 cut ->: Pr[G2(R, A).main()@ &m: Monoid.Mbor.sum_ij 0 (k+1) (fun i, G2.forgeChk.[i])]
       = Pr[G2(R, A).main()@ &m: 
             Monoid.Mbor.sum_ij 0 k (fun i, G2.forgeChk.[i])
             \/ Monoid.Mbor.sum_ij (k+1) (k+1) (fun i, G2.forgeChk.[i]) ].
  rewrite Pr [mu_eq] => // &1.
  rewrite (Monoid.Mbor.sum_ij_split (k+1)); first smt.
  cut ->: k+1-1 = k by smt.
  by trivial.
 rewrite Pr [mu_or].
 cut ->: Pr[G2(R, A).main(tt) @ &m :
            (Monoid.Mbor.sum_ij (k + 1) (k + 1) (fun i, G2.forgeChk.[i]))]
         = Pr[G2(R, A).main(tt) @ &m : G2.forgeChk.[k + 1]].
  by rewrite Pr [mu_eq] => // &1; rewrite Monoid.Mbor.sum_ij_eq.
 cut HH: forall (a b:real), 0%r <= a => b - a <= b by smt.
 apply HH; smt.
rewrite (sum_ij_split (k+1)); first smt.
cut ->: k+1-1 = k by smt.
rewrite sum_ij_eq /=.
apply addleM; first by apply IH.
by apply eq_le.
qed.

(* Translation between notations *)

lemma G2game_trf &m k i p:
  forall (A<: Adv_Auth_t) (R<: Rand_t {A}),
    p = Pr[G2(R, A).game(i) @ &m : G2.forgeChk.[k]] =>
    phoare [ G2(R,A).game : pguess=i ==> G2.forgeChk.[k]] = p.
proof.
intros A R H.
bypr; progress.
rewrite H.
byequiv (_: ={pguess} ==> ={G2.forgeChk}) => //.
proc.
wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by proc true.
wp; call (_: ={l} ==> ={res, glob R}); first by proc true.
call (_: true ==> ={res, glob A}); first by proc true.
skip; progress; smt. 
qed.

(* Conditional probability lemma *)

lemma PrG2_eq &m i: 
  forall (A<: Adv_Auth_t {G2}) (R<: Rand_t {A,G2}),
   0 <= i < m_bnd =>
   Pr[G2(R,A).main() @ &m : G2.forgeChk.[i] /\ G2.pguess = i
                            /\ G2.out.[i] = G2.bguess ]
   = Pr[G2(R, A).main() @ &m : G2.forgeChk.[i]] / (2*m_bnd)%r.
proof.
intros A R Hi.
pose p:= Pr[G2(R, A).main() @ &m : G2.forgeChk.[i]].
rewrite -/p.
byphoare (_ : true ==> _) => //.
proc; wp. swap 2 1.
seq 1: (G2.pguess=i) (1%r/m_bnd%r) (p/2%r) (1%r/m_bnd%r) 0%r; trivial.
 rnd; skip; progress.
 cut ->:(fun x, x = i) = ((=) i).
  apply fun_ext => x /=; smt.
 by change (mu_x [0..m_bnd - 1] i = 1%r / m_bnd%r); smt.
 seq 1 : (G2.pguess = i /\ G2.forgeChk.[i]) p (1%r/2%r) (1%r-p) 0%r.
  inline G2(R,A).game.
  do (wp; call (_:true)); wp; skip; smt.
  call (G2game_trf &m i i p A R _ ) => //.
  rewrite /p.
  byequiv (_: pguess{2}=i ==> ={G2.forgeChk}).
    proc; inline G2(R,A).game.
    wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
    wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by proc true.
    wp; call (_: ={l} ==> ={res, glob R}); first by proc true.
    (* ec-bug: adding (glob A) to pre as post conditions of the previous tactic does not work *)
    call (_: true ==> ={res, glob A}); first by proc true.
    by wp; rnd{1}; rnd{1}; skip; progress; smt.
   reflexivity.     
  by trivial.
  rnd ((=) G2.out.[i]); skip; progress.
  by change (mu_x {0,1} G2.out{hr}.[G2.pguess{hr}] = 1%r / 2%r); smt.
  hoare.
  rnd; skip; progress; smt.
  by progress; trivial.
  hoare; inline G2(R,A).game; rnd.
  wp; call (_:true).
  wp; call (_:true).
  wp; call (_:true).
  wp; call (_:true).
  wp; skip; progress; smt.
  progress.
  cut ->: (m_bnd%r * 2%r) = (2 * m_bnd)%r by smt.
  cut ->: 0%r / m_bnd%r = 0%r by smt.
  smt.
qed.

(* Split the probability of success in game 2 by the different
   possible guessed positions *)

lemma PrG2_xpnd2 &m: 
 forall (A<: Adv_Auth_t) (R<: Rand_t),
    Pr[G2(R,A).main() @ &m : G2.forgeChk.[G2.pguess] /\ G2.out.[G2.pguess]=G2.bguess /\ 0 <= G2.pguess < m_bnd ] =
    sum_ij 0 (m_bnd-1) (fun k, Pr [G2(R,A).main() @ &m : G2.forgeChk.[k] /\ G2.out.[k] = G2.bguess /\ G2.pguess = k ]).
proof.
intros A R.
pose n := m_bnd-1.
cut ->: Pr[G2(R, A).main() @ &m :
            G2.forgeChk.[G2.pguess] /\
            G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < m_bnd]
      = Pr[G2(R, A).main() @ &m :
            G2.forgeChk.[G2.pguess] /\
            G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < n+1].
 rewrite Pr [mu_eq]; smt.
cut: 0<=n by smt.
elim/Induction.induction n.
 rewrite sum_ij_eq /=.
 rewrite Pr [mu_eq]; smt.
intros k H1 IH.
rewrite (sum_ij_split (k+1)); first by smt.
rewrite sum_ij_eq /=.
cut ->: Pr[G2(R, A).main() @ &m :
           G2.forgeChk.[G2.pguess] /\
           G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < k + 1 + 1]
      = Pr[G2(R, A).main() @ &m :
           G2.forgeChk.[G2.pguess] /\
           G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < k + 1
           \/ G2.forgeChk.[k+1] /\ G2.out.[k+1] = G2.bguess /\ G2.pguess = k+1 ].
 rewrite Pr [mu_eq]; progress; smt.
rewrite Pr [mu_or].
rewrite IH.
cut ->: k+1-1=k by smt.
cut ->: Pr[G2(R, A).main() @ &m :
           (G2.forgeChk.[G2.pguess] /\
            G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < k + 1) /\
            G2.forgeChk.[k+1] /\
            G2.out.[k+1] = G2.bguess /\ G2.pguess = k + 1]
      = Pr[G2(R, A).main() @ &m : false].
 rewrite Pr [mu_eq]; smt.
rewrite Pr [mu_false]; smt.
qed.

(* Rewriting the success event in G2 to match previous lemma  *)

equiv G2G2_equiv (A<: Adv_Auth_t {G2}) (R<: Rand_t {G2,A}):
  G2(R,A).main ~ G2(R,A).main : 
  true ==> ={glob G2} /\ res{1} = (G2.forgeChk.[G2.pguess]
           /\ G2.out.[G2.pguess]=G2.bguess /\ 0 <= G2.pguess < m_bnd){2}.
proof.
proc; wp.
call (_: ={pguess, G2.pguess, G2.bguess} /\ (pguess = G2.pguess){1}
      ==> ={glob G2} /\ res{1}=(G2.forgeChk.[G2.pguess]){2}).
 proc.
 wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
 wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by proc true.
 wp; call (_: ={l} ==> ={res, glob R}); first by proc true.
 wp; call (_: true ==> ={res, glob A}); first by proc true.
 skip; progress; smt.
rnd; rnd; skip; progress; smt.
qed.

(** A version of G2 in which [chkForge] is replaced by a local check *)

  module G2'(R: Rand_t, A : Adv_Auth_t) = {

    proc game(pguess: int, bguess: bool) : bool = {
      var rg : rand_t;
      var fn : fun_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var x : Input.input_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r: bool;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      r = false;
      if (validInputs fn x /\ pguess < fn_m (fst fn) /\ 
          (eval fn x).[pguess]=bguess)
        r = (oG.[pguess] = if bguess
                             then fst oK.[pguess]
                             else snd oK.[pguess]);

      return r;
    }
    proc main() : bool = {
      var pguess: int;
      var bguess: bool;
      var r: bool;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      r = game(pguess, bguess);
      return r;
    }
  }.

(** G2 and G2' are exactly the same *)

equiv G2G2_equiv' (A<: Adv_Auth_t {G2}) (R<: Rand_t {G2,A}):
   G2(R,A).main ~ G2'(R,A).main : true ==>  ={res}.
proof.
proc; wp.
call (_: ={pguess} /\ pguess{1}=G2.pguess{1} /\ 0 <= pguess{1} < m_bnd
         /\ bguess{2}=G2.bguess{1}
      ==> (res=G2.forgeChk.[G2.pguess]){1} /\ res{2}=(G2.forgeChk{1}.[G2.pguess{1}]
          /\ G2.out{1}.[G2.pguess{1}] = G2.bguess{1})).
 proc.
 wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
 wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by proc true.
 wp; call (_: ={l} ==> ={res, glob R}); first by proc true.
 wp; call (_: true ==> ={res, glob A}); first by proc true.
 skip => &1 &2 /= [-> [-> [Hpguess ->]]] fn1 fn2 gAL gAR [-> ->]
         rL rR gRL gRR [-> _] xL xR gAL' gAR' [-> ->] oL oR ->.
 case (validInputs fn2 xR) => Hval /=; last by rewrite get_init.
 case (G2.pguess{1} < fn_m (fst fn2)) => Hpg //=; last first.
  cut ->: (G2.pguess{1} < m_bnd)=true by smt.
  rewrite get_init //=.
  rewrite /chkForge get_init //=.
  cut ->: length (outputK fn2 rR) = fn_m (fst fn2).
   rewrite outputK_len.
    move: Hval; rewrite /validInputs /Garble2.validInputs /Garble1.C2.validInputs
     /validCircuit.
    move: (fn_topo_rw (fst fn2)) => -> /=; smt.
   reflexivity.
  case ((eval fn2 xR).[G2.pguess{1}] = G2.bguess{1}) => HH; smt.
 cut ->: (G2.pguess{1} < m_bnd)=true by smt.
 case ((eval fn2 xR).[G2.pguess{1}] = G2.bguess{1}) => HH //=.
 rewrite /chkForge get_init //=.
 cut ->: length (outputK fn2 rR) = fn_m (fst fn2).
  rewrite outputK_len.
   move: Hval; rewrite /validInputs /Garble2.validInputs /Garble1.C2.validInputs
    /validCircuit.
   move: (fn_topo_rw (fst fn2)) => -> /=; smt.
  reflexivity.
 smt. 
rnd; rnd; skip; progress; smt.
qed.

(** This puts everything together and completes the multiplicative hop *)

lemma PrG2_res &m:
forall (A<: Adv_Auth_t{Game_Auth,G2}) (R<: Rand_t{Game_Auth,G2,A}),
   islossless A.gen_fun => islossless A.gen_input =>  islossless A.forge =>
   islossless R.gen =>
  Pr[Game_Auth(R,A).main() @ &m: res] <=
  (2*m_bnd)%r * Pr[G2'(R, A).main() @ &m : res].
proof.
intros A R A1L A2L A3L RL.
apply (Trans _ Pr[G1(R, A).main() @ &m : res]).
 by byequiv (G1_Auth_equiv A R).
cut ->: Pr[G1(R, A).main() @ &m : res]
        = Pr[G2(R, A).main() @ &m : or_array G2.forgeChk].
 by byequiv (G1G2_equiv A R).
apply (Trans _ (sum_ij 0 (m_bnd-1)
                 (fun k, Pr[G2(R, A).main() @ &m : G2.forgeChk.[k] ]))).
 by apply (PrG2_xpnd1 &m A R (m_bnd-1) _).
cut ->: Pr[G2'(R, A).main() @ &m : res] = Pr[G2(R, A).main() @ &m : res].
 apply eq_sym.
 by byequiv (G2G2_equiv' A R). 
cut ->: Pr[G2(R, A).main() @ &m : res]
        = Pr[G2(R, A).main() @ &m : G2.forgeChk.[G2.pguess] /\ 
               G2.out.[G2.pguess]=G2.bguess /\ 0 <= G2.pguess < m_bnd].
 by byequiv (G2G2_equiv A R).
cut ->: sum_ij 0 (m_bnd - 1)
           (fun (k : int), Pr[G2(R, A).main() @ &m : G2.forgeChk.[k]])
      = sum_ij 0 (m_bnd - 1)
           (fun (k : int), (2*m_bnd)%r * Pr[G2(R, A).main() @ &m :
             G2.forgeChk.[k] /\ G2.pguess = k /\ G2.out.[k] = G2.bguess ]).
 apply sum_eq => k Hk /=.
 rewrite (PrG2_eq &m k A R _); first smt.
 cut _: (2*m_bnd)%r <> 0%r by smt.
 smt.
cut ->: sum_ij 0 (m_bnd - 1)
  (fun (k : int),
     (2 * m_bnd)%r *
     Pr[G2(R, A).main() @ &m :
        G2.forgeChk.[k] /\ G2.pguess = k /\ G2.out.[k] = G2.bguess]) 
 = (2 * m_bnd)%r * sum_ij 0 (m_bnd - 1)
      (fun (k : int),
        Pr[G2(R, A).main() @ &m :
            G2.forgeChk.[k] /\ G2.pguess = k /\ G2.out.[k] = G2.bguess]) .
 by rewrite sum_ij_mul_distr.
rewrite (PrG2_xpnd2 &m A R) //.
apply eq_le.
congr => //; congr => //; apply fun_ext => k //=.
rewrite Pr [mu_eq]; smt.
qed.

(******************************************************************************)
(*** Let's now formalize aPriv                                              ***)
(******************************************************************************)

  module type Adv_APriv_t = { 
    proc * gen_fun() : fun_t*int*bool
    proc gen_input(fG : funG_t) : Input.input_t
    proc guess(xG : Input.inputG_t, oKi: word*word) : bool
  }.

op fixOutput(fn : fun_t, pguess : int, bguess : bool) = 
   let (n, m, q, aa, bb) = fst fn in
   let gates = snd fn in
   let i = n + q - m + pguess in
   let modgates = gates.[(i,false,false) <- bguess]
                       .[(i,false,true) <- bguess]
                       .[(i,true,false) <- bguess]
                       .[(i,true,true) <- bguess]
   in (fst fn,modgates).

  module APriv(R: Rand_t, A : Adv_APriv_t) = {
    proc main(b:bool) : bool = {
      var fn, fn': fun_t;
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var oKi: word*word;
      var x : Input.input_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var pos: int;
      var posval: bool;
      var b' : bool;

      (fn, pos, posval) = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG (if b then fn else fixOutput fn pos posval) rg;
      x = A.gen_input(fG);
      iK = inputK fn rg; 
      oK = outputK fn rg;
      oKi = oK.[pos]; 
      xG = Input.encode iK x;
      b' = A.guess(xG, oKi); 

      if (!(validInputs fn x /\ (eval fn x).[pos] = posval)) {
             b' = false;
      }

      return b=b';
    }
  }.

(** The algorithm that interpolates between two games down to APriv *)

module Red(R:Rand_t, A:Adv_Auth_t) : Adv_APriv_t = {
  var pguess:int 
  var bguess:bool
  var fn: fun_t
  var x: input_t
  
  proc gen_fun() : fun_t*int*bool = {
    pguess = $[0..(m_bnd-1)];
    bguess = ${0,1};
    fn = A.gen_fun();
    return (fn,pguess,bguess);
  }
    
  proc gen_input(fG: funG_t) : input_t = {
    x = A.gen_input(fG);
    return x;
  }

  proc guess(xG: inputG_t, oKi: word*word) : bool = {
    var r: bool;
    var oG: outputG_t;

    oG = A.forge(xG);

    if (validInputs fn x /\ pguess < fn_m (fst fn) /\ 
       (eval fn x).[pguess]=bguess) {
       r = (oG.[pguess] = if bguess
                          then fst oKi
                          else snd oKi);
      }
      else {
       r = false;
      }
      return r;
    }

}.

  (** Left-hand side of interpolation *)

equiv G2_APriv (A<:Adv_Auth_t {Red}) (R<:Rand_t {Red, A}):
  G2'(R,A).main ~ APriv(R,Red(R,A)).main : b{2} ==> ={res}.
proof.
proc; inline G2'(R, A).game Red(R, A).gen_fun Red(R, A).gen_input Red(R, A).guess.
seq 12 16: (b{2} /\ Red.pguess{2}=pguess{1} /\ pos{2}=pguess{1} /\ pguess0{1}=pguess{1}
           /\ Red.bguess{2}=bguess0{1} /\ posval{2}=bguess0{1}
           /\ x{1}=Red.x{2}
           /\ fn{1}=Red.fn{2}
           /\ oK{1}.[pguess{1}] = oKi0{2}
           /\ oG{1}=oG0{2} /\ ={rg,fn,fG,x}).
 call (_: ={X,glob A} ==> ={res}); first by proc true.
 wp; call (_: ={fG,glob A} ==> ={res, glob A}); first by proc true.
 wp; call (_: ={l} ==> ={res}); first by proc true.
 wp; call (_: true ==> ={res, glob A}); first by proc true.
 by wp; rnd; rnd; skip; progress; smt.
case (validInputs Red.fn{2} Red.x{2} /\  Red.pguess{2} < fn_m (fst Red.fn{2})
      /\ (eval Red.fn{2} Red.x{2}).[Red.pguess{2}] = Red.bguess{2}).
 rcondt {2} 1.
  by move=> &m; skip; progress.
 rcondt {1} 2.
  by move=> &m; wp; skip; progress.
 rcondf {2} 3. 
  by move => &m; wp; skip; progress.
 by wp; skip; progress; smt.
rcondf {2} 1.
  by move=> &m; skip; progress.
rcondf {1} 2.
  by move=> &m; wp; skip; progress.
case (validInputs fn{2} x{2} /\ (eval fn{2} x{2}).[pos{2}] = posval{2}).
 rcondf {2} 3.
  by move=> &m; wp; skip; progress.
 by wp; skip; progress; smt.
rcondt {2} 3.
 by move=> &m; wp; skip; progress.
by wp; skip; progress; smt.
wp; rnd{2}; skip; progress; smt.
qed.

  (*******************)
  (** The final Game *)
  (*******************)

  module G3(R: Rand_t, A : Adv_Auth_t) = {

    proc genRnd(fn: fun_t, pguess: int, bguess:bool) : rand_t = {
      var r: rand_t;
      r = R.gen(phi fn);
      return r;
    }
    proc game(pguess: int, bguess: bool) : bool = {
      var rg : rand_t;
      var fn: fun_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oKi : word;
      var x : Input.input_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r: bool;

      fn = A.gen_fun();
      rg = genRnd(fn, pguess, bguess);
      oKi = oget rg.[(outIdx (phi fn) pguess, !bguess)];
      fG = (funG (fixOutput fn pguess bguess) rg);
      iK = inputK fn rg;
      x = A.gen_input(fG);
      xG = Input.encode iK x;
      oG = A.forge(xG);

      r = false;
      if (validInputs fn x /\ pguess < fn_m (fst fn) /\ 
          (eval fn x).[pguess]=bguess)
        r = (oG.[pguess] = oKi);

      return r;
    }
    proc main() : bool = {
      var pguess: int;
      var bguess: bool;
      var r: bool;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      r = game(pguess, bguess);
      return r;
    }
  }.

  (** right-hand side of interpolation *)

equiv G3_APriv (A<:Adv_Auth_t {Red}) (R<:Rand_t {Red, A}):
  G3(R,A).main ~ APriv(R,Red(R,A)).main : !b{2} ==> res{1}=!res{2}.
proof.
proc; inline G3(R, A).game Red(R, A).gen_fun Red(R, A).gen_input Red(R, A).guess.
seq 12 16: (!b{2} /\ Red.pguess{2}=pguess{1} /\ pos{2}=pguess{1} /\ pguess0{1}=pguess{1}
            /\ Red.bguess{2}=bguess0{1} /\ posval{2}=bguess0{1}
            /\ x{1}=Red.x{2}
            /\ fn{1}=Red.fn{2}
            /\ (pguess{1} < fn_m (fst fn{1}) =>
                oKi{1} = if Red.bguess{2} then fst oKi0{2} else snd oKi0{2})
            /\ oG{1}=oG0{2} /\ fG{1}=fG0{2} /\ ={rg,fn,fG,x}).
 inline G3(R,A).genRnd.
 call (_: ={X,glob A} ==> ={res}); first by proc true.
 wp; call (_: ={fG,glob A} ==> ={res, glob A}); first by proc true.
 wp; call (_: ={l} ==> ={res}); first by proc true.
 wp; call (_: true ==> ={res, glob A}); first by proc true.
 wp; rnd; rnd; skip; progress; first smt.
 rewrite /outIdx /outputK /Garble2.outputK (fn_topo_rw (phi result_R)) /=.
 by move: (fn_topo_rw (fst result_R))=> -> /=; delta=> /=; smt.
case (validInputs Red.fn{2} Red.x{2} /\  Red.pguess{2} < fn_m (fst Red.fn{2})
      /\ (eval Red.fn{2} Red.x{2}).[Red.pguess{2}] = Red.bguess{2}).
 rcondt {2} 1.
  by move=> &m; skip; progress.
 rcondt {1} 2.
  by move=> &m; wp; skip; progress.
 rcondf {2} 3. 
  by move => &m; wp; skip; progress.
 by wp; skip; progress; smt.
rcondf {2} 1.
  by move=> &m; skip; progress.
rcondf {1} 2.
  by move=> &m; wp; skip; progress.
case (validInputs fn{2} x{2} /\ (eval fn{2} x{2}).[pos{2}] = posval{2}).
 rcondf {2} 3.
  by move=> &m; wp; skip; progress.
 by wp; skip; progress; smt.
rcondt {2} 3.
 by move=> &m; wp; skip; progress.
by wp; skip; progress; smt.
wp; rnd{2}; skip; progress; smt.
qed.

lemma Game2_3_pr: forall &m (A<:Adv_Auth_t {Red}) (R<:Rand_t {Red, A}),
  Pr[G2'(R,A).main()@ &m:res] - Pr[G3(R,A).main()@ &m:res]
  =  Pr[ APriv(R,Red(R,A)).main(true)@ &m:res]
     - Pr[ APriv(R,Red(R,A)).main(false)@ &m:!res].
proof.
move => &m A R.
cut ->: Pr[G2'(R,A).main()@ &m:res] = Pr[APriv(R,Red(R,A)).main(true)@ &m:res].
 by byequiv (G2_APriv A R).
cut ->: Pr[G3(R,A).main()@ &m:res] = Pr[APriv(R,Red(R,A)).main(false)@ &m:!res].
 by byequiv (G3_APriv A R).
reflexivity.
qed.

(* And the final hop where we show that the adversary has no information
  on the output key that shall guess.
  The proof strategy is the following:
   1 - we replace the rand. generation procedure so that the generation of the
     relevant item is dettached;
   2 - after been used, the item is removed from the randomness;
   3 - all scheme operations used after the removal are shown to be insensive
     to it;
   4 - we can then perform the lazy sampling, and establish the desired bound.
*)

  module Fake(R: Rand_t, A : Adv_Auth_t) = {
    proc genRnd(fn: fun_t, pguess: int, bguess: bool) : rand_t = {
      var rgate: word;
      var r: rand_t;
      r = R.gen(phi fn);
      rgate = $Dword.dword;
      if (0 <= pguess < fn_m (phi fn))
        r.[(outIdx (phi fn) pguess, !bguess)] = rgate;
      return r;
    }
    proc game(pguess: int, bguess: bool) : bool = {
      var rg : rand_t;
      var fn: fun_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oKi : word;
      var x : Input.input_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r: bool;

      fn = A.gen_fun();
      rg = genRnd(fn, pguess, bguess);
      oKi = oget rg.[(outIdx (phi fn) pguess, !bguess)];
      rg = rm (outIdx (phi fn) pguess, !bguess) rg;

      fG = (funG (fixOutput fn pguess bguess) rg);
      iK = inputK fn rg;
      x = A.gen_input(fG);
      xG = Input.encode iK x;
      oG = A.forge(xG);

      r = false;
      if (validInputs fn x /\ pguess < fn_m (fst fn) /\ 
          (eval fn x).[pguess]=bguess)
        r = (oG.[pguess] = oKi);

      return r;
    }
    proc main() : bool = {
      var pguess: int;
      var bguess: bool;
      var r: bool;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      r = game(pguess, bguess);
      return r;
    }
  }.

lemma genRnd_Rgen (A<:Adv_Auth_t) :
 forall bound,
 equiv [G3(Rand,A).genRnd ~ Fake(Rand,A).genRnd
       : ={fn,pguess,bguess}
         /\ validCircuit bound fn{2} 
       ==> ={res}].
proof.
move => maxG; proc; inline Rand.gen.
case (0<= pguess{2} <fn_m (phi fn{2})); last first.
 rcondf {2} 9.
  move => &m.
  rnd; wp; while true; first by wp; rnd; rnd; skip; trivial.
  wp; while true; first by wp; rnd; rnd; rnd; skip; trivial.  
  by wp; skip; trivial.
 rnd {2}; wp.
 conseq (_: ={fn,pguess,bguess} ==> ={r0}); first 2 smt.
 by inline Rand.gen; sim.
rcondt {2} 9.
 move => &m.
 rnd; wp; while true; first by wp; rnd; rnd; skip; trivial.
 wp; while true; first by wp; rnd; rnd; rnd; skip; trivial.  
 by wp; skip; trivial.
splitwhile{1} 6: (i < n+q-m+pguess).
splitwhile{2} 6: (i < n+q-m+pguess).
cut _: forall maxg fn,
        validCircuit maxg fn => 0 <= (phi fn).`1 + (phi fn).`3 - (phi fn).`2.
 by rewrite /validCircuit => maxg fn; rewrite (fn_topo_rw (fst fn)) /=; smt.
seq 6 6: (={i,fn,r0,n,m,q,pguess,bguess} /\ i{2} = n{2} + q{2} - m{2} + pguess{2} /\
  0 <= pguess{2} < m{2} /\ (*validTopo n{2} q{2} m{2}*) validCircuit maxG fn{2}
  /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2).
 while (={i,l,r0,n,m,q,pguess,bguess} /\
        n{2} + q{2} - m{2} <= i{2} <= n{2} + q{2} - m{2} + pguess{2}
        /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2).
  by wp; rnd; rnd; skip; smt.
 wp; while (={i,l,r0,n,m,q,pguess,bguess} /\ 0 <= i{2} <= n{2} + q{2} - m{2}
          /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2).
  by wp; rnd; rnd; rnd; skip; smt.
 by wp; skip; progress; smt.
unroll {1} 1; unroll {2} 1.
rcondt {1} 1; first by move=> &m; skip; progress; smt.
rcondt {2} 1; first by move=> &m; skip; progress; smt.
transitivity {2}
 { i = n + q - m + pguess + 1;
   temp1 = $Dword.dword;
   temp2 = $Dword.dword;
   r0.[(n+q-m+pguess, false)] = temp1;
   r0.[(n+q-m+pguess, true)] = temp2;
   rgate = $Dword.dword;
   r0.[(n+q-m+pguess, !bguess)] = rgate;
   while (n+q-m+pguess+1 <= i < n+q) {
     temp1 = $Dword.dword;
     temp2 = $Dword.dword;
     r0.[(i, false)] = temp1;
     r0.[(i, true)] = temp2;
     i = i+1;
   }
   r = r0;
 }
 ( ={i,fn,r0,n,q,m,pguess,bguess} /\  i{1}=n{1}+q{1}-m{1}+pguess{1} 
   /\ validCircuit maxG fn{2}
   /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2 
 ==> ={r} /\ validCircuit maxG fn{2} 
     /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn){2}.`2)
 ( ={i,fn,r0,n,q,m,pguess,bguess} /\  i{1}=n{1}+q{1}-m{1}+pguess{1}
   /\ validCircuit maxG fn{2}
   /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2
 ==> ={r})=> //.
  move => &1 &2 H; exists fn{2}, pguess{2}, bguess{2}, r0{2}, n{2}, m{2}, q{2}, i{2}.
  by move: H; progress; trivial.
 wp; while (={r0,i,n,q,m,pguess} /\ n{2}+q{2}-m{2}+pguess{2}<i{2}
           /\ validCircuit maxG fn{2}
           /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2).
  by wp; rnd; rnd; skip; progress; smt.
 case (bguess{2}).
  swap {2} 6 -3.
  by wp; rnd; rnd; rnd {2}; wp; skip; progress; smt.
 swap {2} 3 -1.
 by wp; rnd; wp; rnd; rnd {2}; wp; skip; progress; smt.
transitivity {2}
 { i = n+q-m+pguess+1;
   temp1 = $Dword.dword;
   temp2 = $Dword.dword;
   r0.[(n+q-m+pguess, false)] = temp1;
   r0.[(n+q-m+pguess, true)] = temp2;
   while (n+q-m+pguess+1 <= i < n+q) {
     temp1 = $Dword.dword;
     temp2 = $Dword.dword;
     r0.[(i, false)] = temp1;
     r0.[(i, true)] = temp2;
     i = i+1;
   }
   rgate = $Dword.dword;
   r0.[(n+q-m+pguess, !bguess)] = rgate;
   r = r0;
 }
 ( ={i,fn,r0,n,q,m,pguess,bguess} /\  i{1}=n{1}+q{1}-m{1}+pguess{1} 
   /\ validCircuit maxG fn{2}
   /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2 
 ==> ={r} )
 ( ={i,fn,r0,n,q,m,pguess,bguess} /\  i{1}=n{1}+q{1}-m{1}+pguess{1}
   /\ validCircuit maxG fn{2}
   /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2
 ==> ={r})=> //.
  move => &1 &2 H; exists fn{2}, pguess{2}, bguess{2}, r0{2}, n{2}, m{2}, q{2}, i{2}.
  by move: H; progress; trivial.
 wp 8 8.
 seq 5 5 : (={i, fn, r0, n, q, m, pguess, bguess}
           /\ i{1} = n{1} + q{1} - m{1} + pguess{1}+1).
  by wp; rnd; rnd; wp; skip; progress.
 conseq (_: ={r0, i, fn, n, q, m, pguess, bguess}
           ==> ={r0, i, fn, n, q, m, pguess, bguess}); first 2 smt.
 eager while
  (h: rgate = $Dword.dword;
      r0.[(n+q-m+pguess, !bguess)] = rgate;
    ~ rgate = $Dword.dword;
      r0.[(n+q-m+pguess, !bguess)] = rgate;
    : ={r0, n, q, m, pguess, bguess} ==> ={r0}).
    by wp; rnd; skip; progress.
   by progress.
  swap {2} 6 -5; wp; rnd; wp; rnd; wp; rnd; skip; progress; smt.
 by sim.
wp; rnd; wp.
while (={r0,i,n,q,m,pguess} /\ n{1}+q{1}-m{1}+pguess{1}<i{1}
       /\ validCircuit maxG fn{2} 
       /\ n{2} = (phi fn{2}).`1 /\ q{2} = (phi fn{2}).`3 /\ m{2} = (phi fn{2}).`2).
 by wp; rnd; rnd; skip; progress; smt.
by wp; rnd; rnd; wp; skip; progress; smt.
qed.

lemma outIdx_ge: forall (fn:fun_t) pguess,
 0 <= pguess =>
 outIdx (fst fn) pguess >= fn_n (fst fn) + fn_q (fst fn) - fn_m (fst fn).
proof.
rewrite /outIdx => fn pguess Hpguess.
rewrite {1}(fn_topo_rw (fst fn)) /=; smt.
qed.

lemma fixOutput_oget: forall (fn:fun_t) pguess (bguess:bool) (r:rand_t) k b,
 0 <= pguess =>
 k < fn_n (fst fn) + fn_q (fst fn) - fn_m (fst fn) =>
 oget (rm (outIdx (phi fn) pguess, !bguess) r).[(k, b)] 
 = oget r.[(k,b)].
proof.
move => fn pguess bguess r k b Hpguess Hk.
rewrite /phi /Garble2.phi /Garble1.C2.phi get_rm_neq; last by trivial.
move: (outIdx_ge fn pguess _) => // HoutIdx.
cut: outIdx (fst fn) pguess <> k by smt.
 by apply absurd; rewrite !nnot => [-> _].
qed.

lemma fixOutputP: forall (fn:fun_t) pguess bguess b1 b2,
 oget (snd (fixOutput fn pguess bguess)).[(outIdx (phi fn) pguess, b1, b2)] = bguess.
proof.
rewrite /outIdx /fixOutput /= => fn pguess bguess b1 b2.
rewrite /phi /Garble2.phi /Garble1.C2.phi.
rewrite (fn_topo_rw (fst fn)) /= snd_pair.
case b1; case b2 => Hb1 Hb2.
   rewrite get_set_eq => //.
  rewrite get_set_neq; first by smt.
  rewrite get_set_eq => //.
 do 2! (rewrite get_set_neq; first by smt).
 rewrite get_set_eq => //.
do 3! (rewrite get_set_neq; first by smt).
by rewrite get_set_eq.
qed.

lemma enc_rm: forall fn pguess (bguess:bool) r k b1 b2,
 0 <= pguess =>
 (fn_aa (fst fn)).[k] < fn_n (fst fn) + fn_q (fst fn) - fn_m (fst fn) =>
 (fn_bb (fst fn)).[k] < fn_n (fst fn) + fn_q (fst fn) - fn_m (fst fn) =>
 enc (rm (outIdx (phi fn) pguess, ! bguess) r)
     (fixOutput fn pguess bguess)
     k b1 b2
 = enc r (fixOutput fn pguess bguess) k b1 b2.
proof.
rewrite /enc /= => fn pguess bguess r k b1 b2 Hpguess Haa Hbb.
cut ->: fst (fixOutput fn pguess bguess) = fst fn by smt.
rewrite (fn_topo_rw (fst fn)) /=; congr.
   do 4! (rewrite (fixOutput_oget fn pguess bguess r _ _ _ _); first 2 assumption).
   reflexivity.
  do 2! (rewrite (fixOutput_oget fn pguess bguess r _ _ _ _); first 2 assumption).
  reflexivity.
 do 2! (rewrite (fixOutput_oget fn pguess bguess r _ _ _ _); first 2 assumption).
 reflexivity.
rewrite get_rm_neq.
cut HH: forall (k1 k2:int) (b1 b2:bool),
          (k1 = k2 => b1 = b2) => ! (k1,!b1) = (k2,b2) by smt.
apply HH => <-. 
rewrite fixOutputP //.
do 2! (rewrite (fixOutput_oget fn pguess bguess r _ _ _ _); first 2 assumption).
reflexivity.
qed.

lemma funG_rm: forall bound fn pguess bguess r,
 0 <= pguess =>
 validCircuit bound fn =>
 funG (fixOutput fn pguess bguess) (rm (outIdx (phi fn) pguess, !bguess) r)
 = funG (fixOutput fn pguess bguess) r.
proof.
rewrite /validCircuit => bound fn pguess bguess r Hpguess.
rewrite (fn_topo_rw (fst fn)) /=.
move => [H1 [H2 [H3 [H4 [H5 [H6 [H7 [H8]]]]]]]].
pose p := fun i, 0 <= (fn_aa (fst fn)).[i] /\
         (fn_bb (fst fn)).[i] < i /\
         (fn_bb (fst fn)).[i] < fn_n (fst fn) + fn_q (fst fn) - fn_m (fst fn) /\
         (fn_aa (fst fn)).[i] < (fn_bb (fst fn)).[i].
pose rangeExp := range _ _ _ _.
cut ->: rangeExp = (range (fn_n (fst fn)) (fn_n (fst fn) + fn_q (fst fn)) true
                          (fun i b, b /\ p i)) by trivial.
rewrite rangeb_forall /p /= => {p rangeExp} H9.
rewrite /funG /Garble2.funG /Garble1.C2.funG /=.
cut ->: fst (fixOutput fn pguess bguess) = fst fn by smt.
rewrite (fn_topo_rw (fst fn)) /=.
rewrite /initGates /=.
apply range_ext => k st Hk /=.
move: (H9 k _) => // {H9} [H9 [H10 [H11 H12]]].
cut _: (fn_aa (fst fn)).[k] < fn_n (fst fn) + fn_q (fst fn) - fn_m (fst fn) by smt.
by do 4!rewrite enc_rm //.
qed.

lemma inputK_rm: forall bound fn pguess (bguess:bool) r,
 0 <= pguess =>
 validCircuit bound fn =>
 inputK fn (rm (outIdx (phi fn) pguess, !bguess) r)
 = inputK fn r.
proof.
rewrite /validCircuit => bound fn pguess bguess r Hpguess.
rewrite (fn_topo_rw (fst fn)) /=.
move => [H1 [H2 [H3 [H4 [H5 [H6 [H7 [H8]]]]]]]].
pose p := fun i, 0 <= (fn_aa (fst fn)).[i] /\
         (fn_bb (fst fn)).[i] < i /\
         (fn_bb (fst fn)).[i] < fn_n (fst fn) + fn_q (fst fn) - fn_m (fst fn) /\
         (fn_aa (fst fn)).[i] < (fn_bb (fst fn)).[i].
pose rangeExp := range _ _ _ _.
cut ->: rangeExp = (range (fn_n (fst fn)) (fn_n (fst fn) + fn_q (fst fn)) true
                          (fun i b, b /\ p i)) by trivial.
rewrite rangeb_forall /p /= => {p rangeExp} H9.
rewrite /inputK /Garble2.inputK /Garble1.C2.inputK.
rewrite (fn_topo_rw (fst fn)) /=.
apply map_ext => x; rewrite !get_filter /=.
case (0 <= fst x < fn_n (fst fn)); last by trivial.
move => Hx; rewrite get_rm_neq; last by trivial.
cut HH: outIdx (phi fn) pguess >= fn_n (fst fn) by smt.
smt.
qed.

lemma G3_Fake (A<:Adv_Auth_t):
  islossless A.gen_fun => islossless A.gen_input => islossless A.forge =>
  equiv [G3(Rand,A).main ~ Fake(Rand,A).main : true ==> ={res}].
proof.
move => Agenfun_ll Ageninput_ll Aforge_ll; proc.
inline G3(Rand,A).game Fake(Rand,A).game.
seq 5 5: (={pguess0, bguess0, fn, glob A} /\ 0 <= pguess0{1} < m_bnd).
 call (_: true ==> ={res, glob A}); first by proc true.
 by wp; rnd; rnd; skip; progress; smt.
case (validCircuit Garble1.bound fn{2}).
 wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
 wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by proc true.
 wp; call (_: ={fn,pguess,bguess} /\ validCircuit Garble1.bound fn{2} ==> ={res}).
  by apply (genRnd_Rgen A Garble1.bound).
(* EC BUG:  progress seems to block!!! *)
 skip => &1 &2 /=.
 move => [[[-> [-> [-> ->]]] [H1 H2]] H3].
 split; first by trivial.
 move => _1 rL rR ->.
 split.
  split; first by trivial.
  by rewrite (funG_rm Garble1.bound fn{2}) //.
 move => [_2 HfunG] xL xR gAL' gAR' [-> ->].
 split.
  split; first by trivial.
  by rewrite (inputK_rm Garble1.bound fn{2}) //.
 move => [_5 Hencode] oL oR ->.
 smt.
cut _: forall fn x, !validCircuit Garble1.bound fn => !validInputs fn x.
 move => fn x; apply absurd; rewrite !nnot.
 rewrite /validInputs /Garble2.validInputs /Garble1.C2.validInputs.
 by rewrite (fn_topo_rw (fst fn)) /=; progress. 
rcondf {1} 9.
 move => &m.
 wp; call (_: true); wp; call (_:true); wp; call (_:true) => //.
 by skip; progress; smt.
rcondf {2} 10.
 move => &m.
 wp; call (_: true); wp; call (_:true); wp; call (_:true) => //.
 by skip; progress; smt.
inline G3(Rand,A).genRnd Fake(Rand,A).genRnd.
wp; call {1} (_: true ==> true); first proc true; trivial. 
wp; call {1} (_: true ==> true); first proc true; trivial. 
wp; call {1} (_: true ==> true); first by apply Rand_ll.
wp; call {2} (_: true ==> true); first proc true; trivial. 
wp; call {2} (_: true ==> true); first proc true; trivial. 
wp; rnd {2}; call {2} (_: true ==> true); first by apply Rand_ll.
by wp; skip; progress; smt.
qed.


  module Fake'(R: Rand_t, A : Adv_Auth_t) = {
    proc genRnd(fn: fun_t, pguess: int, bguess: bool) : rand_t = {
      var rgate: word;
      var r: rand_t;
      r = R.gen(phi fn);
      rgate = $Dword.dword;
      if (0 <= pguess < fn_m (fst fn))
        r.[(outIdx (phi fn) pguess, !bguess)] = rgate;
      return r;
    }
    proc game(pguess: int, bguess: bool) : bool = {
      var rgate: word;
      var rg : rand_t;
      var fn: fun_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oKi : word;
      var x : Input.input_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r: bool;

      fn = A.gen_fun();

      rg = R.gen(phi fn);
      rg = rm (outIdx (phi fn) pguess, !bguess) rg;

      rgate = $Dword.dword;
      oKi = rgate;

      fG = (funG (fixOutput fn pguess bguess) rg);
      iK = inputK fn rg;
      x = A.gen_input(fG);
      xG = Input.encode iK x;
      oG = A.forge(xG);

      r = false;
      if (pguess < fn_m (fst fn))
        r = (oG.[pguess] = oKi);

      return r;
    }
    proc main() : bool = {
      var pguess: int;
      var bguess: bool;
      var r: bool;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      r = game(pguess, bguess);
      return r;
    }
  }.

lemma Fake_pr' (A<:Adv_Auth_t)  (R<:Rand_t {A})&m:
  Pr[Fake'(R, A).main() @ &m : res] <= 1%r / (2 ^ (W.length))%r.
proof.
byphoare (_:true ==> _) => //.
proc; inline Fake'(R,A).game.
swap [8..9] 6; wp; rnd ((=) oG.[pguess0]).
do 4! (wp; call (_:true)).
wp; rnd; rnd; skip; progress.
delta; simplify.
pose bound := (_ / _).
pose distr := DKCScheme2.W.Dword.dword.
change (mu_x distr result0.[pguess] <= bound).
rewrite /distr DKCScheme2.W.Dword.mu_x_def.
smt.
qed.

equiv Fake_Fake' (A<:Adv_Auth_t):
  Fake(Rand,A).main ~ Fake'(Rand,A).main : true ==> res{1} => res{2}.
proof.
proc; inline Fake(Rand,A).game Fake(Rand,A).genRnd Fake'(Rand,A).game Fake'(Rand,A).genRnd.
wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
wp; call (_: ={glob A, fG} ==> ={res, glob A}); first by proc true.
wp; rnd.
wp; call (_: ={l} ==> ={res}); first by proc; sim.
wp; call (_: true ==> ={res, glob A}); first by proc true.
wp; rnd; rnd; skip  (* EC-BUG: progress halts!!!*).
move => &1 &2 /= pguessL HpguessL; split; first by trivial.
move => _1 bguess Hbguess; split; first by trivial.
move => _2 fnL fnR gAL gAR [-> ->]; split; first by trivial.
move => _3 rL rR -> rgateL HrgateL; split; first by trivial.
move => _4.
case (0 <= pguessL < fn_m (phi fnR)); last by progress; smt.
move => Hpguess_bnd; split.
 split; first by trivial.
 by congr; rewrite rm_set_eq.
move => [_5 _6] /= xL xR gAL' gAR' [-> ->]; split.
 split; first by trivial.
 by congr; rewrite rm_set_eq.
move=> [_7 _8] oGL ogR ->.
case (pguessL < fn_m (fst fnR)); last smt.
by move=> //=; rewrite FMap.get_set.
qed.

(** Putting it all together to get adaptive authenticity for Garble 2
    assuming APriv holds. *)
lemma realFact1: forall (r r1 r2:real), 0%r <= r => r1 <= r2 => r*r1 <= r*r2.
proof. move=> r r1 r2 Hr H; smt. qed.

lemma Scheme2_is_auth &m:
 forall (A<:Adv_Auth_t {Game_Auth, G2, Red}) epsAPriv,
 islossless A.gen_fun => islossless A.gen_input =>  islossless A.forge =>
 epsAPriv = `| Pr[APriv(Rand,Red(Rand,A)).main(true)@ &m:res]
               - Pr[APriv(Rand,Red(Rand,A)).main(false)@ &m:!res] |
 => Pr[Game_Auth(Rand,A).main() @ &m : res] <= (2*m_bnd)%r * (1%r / (2^W.length)%r + epsAPriv).
proof.
move: Rand_ll => Rgen A epsAPriv Agenfun Agenin Aforge.
pose eAPriv := Pr[ APriv(Rand,Red(Rand,A)).main(true)@ &m: res ]
               - Pr[ APriv(Rand,Red(Rand,A)).main(false)@ &m: !res ].
move => Heps.
apply (Trans _ ((2*m_bnd)%r * Pr[G2'(Rand, A).main() @ &m : res])).
 by apply (PrG2_res &m A Rand _ _ _ _).
apply realFact1; first by smt.
cut ->: Pr[G2'(Rand, A).main(tt) @ &m : res]
        = Pr[G2'(Rand, A).main(tt) @ &m : res]-Pr[G3(Rand, A).main(tt) @ &m : res]
          + Pr[G3(Rand, A).main(tt) @ &m : res]
 by smt.
rewrite (Game2_3_pr &m A Rand).
cut _: Pr[G3(Rand, A).main(tt) @ &m : res] <= 1%r / (2 ^ (W.length)%W)%r.
 cut ->: Pr[G3(Rand, A).main(tt) @ &m : res] = Pr[Fake(Rand, A).main(tt) @ &m : res].
  by byequiv (G3_Fake A _ _ _) => //.
 apply (Trans _ (Pr[Fake'(Rand, A).main(tt) @ &m : res])).
  by byequiv (Fake_Fake' A).
 by apply (Fake_pr' A Rand &m).
smt.
qed.

end DKCScheme2.
