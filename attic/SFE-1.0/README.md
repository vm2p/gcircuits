This archive contains:

 - In the root directory, the proof presented in the CSS'14 submission,
 - In easycrypt/, a copy of the EasyCrypt's source code for compilation,
 - In extraction/, the ML code extracted from the EasyCrypt formalization

To check the proof, you first have to compile EasyCrypt. A detailed README
is provided in the easycrypt/ directory. Under Linux/Mac, the short story is:
    
    $ cd easycrypt
    $ make toolchain && make provers # NOTE: this command get archives
    $ $(./scripts/activate-toolchain.sh)   # from the EasyCrypt webserver.
    $ make clean && make

This builds the EasyCrypt toolchain, some SMT provers and the EasyCrypt
tool. EasyCrypt (and its toolchain) compilation can take a while depending
on your machine.

You can then check the development by simply typing `make check` or
`make weakcheck` if you don't want to replay the SMT proofs.

The extracted code can be compiled after having installed `mlgmp` and
`cryptokit`. Both can be installed with the EasyCrypt toolchain.

   $ $(./scripts/activate-toolchain.sh) # if not yet done
   $ opam install -v -y mlgmp
   $ opam install -v -y cryptokit
   $ cd extraction && make
