require import Array.
require import Int.
require import Pair.
require import Real.
require import Distr.

(** This is an extension to the Array theory with several facts relating
 array distributions and loops of samplings.
*)


import Darray.

axiom mu_x_cons (x:'a) (xs:'a array) (d:'a distr): forall len,
 len = length xs =>
 mu_x (Darray.darray (1+len) d) (x::xs)
 = (mu_x d x) * (mu_x (darray len d) xs).

import Dprod.

axiom darray_cons (d:'a distr) (len:int):
 0 <= len =>
 darray (1+len) d = Dapply.dapply (fun x,fst x::snd x) (d*(darray len d)).

axiom mu_x_app (x:'a) (xs ys:'a array) (d:'a distr): forall lxs lys,
 lxs = length xs =>
 lys = length ys =>
 mu_x (Darray.darray (lxs+lys) d) (xs || ys) 
 = (mu_x (darray lxs d) xs) * (mu_x (darray lys d) ys).

theory DArrayTake.
 type t.
 module M = {
  proc gen1(len:int, d: t distr) : t array = {
    var xs: t array;
    xs = $darray len d;
    return xs;
  }
  proc gen2(len' len:int, d: t distr) : t array = {
    var xs: t array;
    xs = $darray len' d;
    xs = take len xs;
    return xs;
  }
 }.

 axiom darray_take_equiv:
  equiv [ M.gen1 ~ M.gen2 : 
         ={len, d} /\ 0 <= len{2} <= len'{2} ==> ={res} ].

end DArrayTake.

theory DArrayWhile.
 type t.
 module M = {
  proc gen1(len:int, d: t distr, dflt: t) : t array = {
    var i: int;
    var x: t;
    var xs: t array;
    i = 0;
    xs = Array.make len dflt;
    while (i < len) {
      x = $d;
      xs.[i] = x;
      i = i+1;
    }
    return xs;
  }
  proc gen2(len:int, d: t distr) : t array = {
    var xs: t array;
    xs = $darray len d;
    return xs;
  }
 }.

 axiom darray_loop_equiv:
  equiv [ M.gen1 ~ M.gen2 : 
         ={len, d} /\ 0 <= len{1} ==> ={res} ].

end DArrayWhile.

theory DArrayWhile2.
 type t1, t2, t.
 module M = {
  proc gen1(len:int,d1:t1 distr,d2:t2 distr,f:t1->t2->t,dflt:t) : t array = {
    var i: int;
    var x: t1;
    var y: t2;
    var z: t array;
    i = 0;
    z = Array.make len dflt;
    while (i < len) {
      x = $d1;
      y = $d2;
      z.[i] = f x y;
      i = i+1;
    }
    return z;
  }
  proc gen2(len:int,d1:t1 distr,d2:t2 distr,f:t1->t2->t) : t array = {
    var x: t1 array;
    var y: t2 array;
    var z: t array;
    x = $darray len d1;
    y = $darray len d2;
    z = init len (fun k, f x.[k] y.[k]);
    return z;
  }
 }.

 axiom darray2_loop_equiv:
  equiv [ M.gen1 ~ M.gen2 : 
         ={len, d1, d2, f} /\ 0 <= len{1} ==> ={res} ].

end DArrayWhile2.

