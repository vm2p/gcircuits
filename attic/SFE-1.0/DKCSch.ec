require import Real.
require import Int.
require import FMap.
require import FSet.
require import Array.
require import Distr.
require import Bool.
require import Pair.
require import Monoid.
require import Option.
require import Means.

require import GarbleTools.

require        Sch.
require        SchSec.
require        DKCSec.

import ForLoop.

theory DKCScheme.  
(* BEGIN ABSTRACT DEFINITION *)
  require ExtWord.
  clone import ExtWord as W.

  op bound : int.
  axiom boundInf : bound > 1.
(* END ABSTRACT DEFINITION *)

(* BEGIN MEANS CLONE *)
   clone Means as MeanInt with
     type input <- int,
     type output <- bool,
     op d <- Dinter.dinter 0 (bound-1).

   clone Means as MeanBool with
     type input <- bool,
     type output <- bool,
     op d <- Dbool.dbool.
(* END MEANS CLONE *)

(* BEGIN INTRO *)

  (* We need this to prove that the tweak is injectively computed from the gate id *)
  op maxGates: int = 2^62 - 1.

  clone import DKCSec.DKCSecurity with theory W = W, op bound <- (2*maxGates).

  clone import Tweak with theory W = W.

  theory Local.
    (* some types reused for garbling scheme definition  *)
    type tokens_t = (int*bool, word) map.
    type 'a gates_t = (int*bool*bool, 'a) map.
    type 'a funct_t = topo_t * 'a gates_t.

    (* Some small function that facilitate the definition of scheme operatots *)
    op validGG (n q:int) (gg:bool gates_t) =
      range n (n + q) true (fun g b,
                              b /\
                              in_dom (g, false, false) gg /\
                              in_dom (g, false, true) gg /\
                              in_dom (g, true, false) gg /\
                              in_dom (g, true, true) gg).

    op validCircuit (bound:int) (f:bool funct_t) =
      let (n, m, q, aa, bb) = fst f in
      1 < n /\ 0 < m /\ 0 < q /\ m <= q /\
      n + q - m = bound /\
      length aa = n + q /\ length bb = n + q /\ validGG n q (snd f) /\
      (range n (n + q) true (fun i b,
                               b /\
                               0 <= aa.[i] /\
                               bb.[i] < i /\
                               bb.[i] < n+q-m /\
                               aa.[i] < bb.[i])).

    pred validCircuitP (bound:int) (f:bool funct_t) =
      let (n, m, q, aa, bb) = fst f in
      1 < n /\ 0 < m /\ 0 < q /\ m <= q /\
      n + q - m = bound /\
      Array.length aa = n + q /\ Array.length bb = n + q /\
      (forall g i1 i2, n <= g < n + q => in_dom (g, i1, i2) (snd f)) /\
      (forall i, n <= i < n+q =>
         0 <= aa.[i] /\
         bb.[i] < i /\
         bb.[i] < n+q-m /\
         aa.[i] < bb.[i]).

    lemma valid_wireinput (bound:int) (f:bool funct_t):
      validCircuit bound f <=>  validCircuitP bound f.
    proof.
      rewrite /validCircuitP /validCircuit /validGG.
      elim/tuple5_ind (fst f)=> topo n m q aa bb valF /=.
      (* simplifying the calls to range with boolean accumulators to forall *)
      cut ->: (fun (g0:int) (b:bool), b /\
                 in_dom (g0,false,false) (snd f) /\
                 in_dom (g0,false,true ) (snd f) /\
                 in_dom (g0,true ,false) (snd f) /\
                 in_dom (g0,true ,true ) (snd f)) =
              (fun (g0:int) (b:bool), b /\
                 (fun k,
                    in_dom (k,false,false) (snd f) /\
                    in_dom (k,false,true ) (snd f) /\
                    in_dom (k,true, false) (snd f) /\
                    in_dom (k,true ,true ) (snd f)) g0) by smt.
      cut ->: (fun i b, b /\ 0 <= aa.[i] /\ bb.[i] < i /\ bb.[i] < n + q - m /\ aa.[i] < bb.[i]) =
              (fun i b, b /\
                 (fun k, 0 <= aa.[k] /\ bb.[k] < k /\  bb.[k] < n + q - m /\ aa.[k] < bb.[k]) i) by smt.
      rewrite !rangeb_forall //=.
      progress; last 4 by apply H5.
      by cut:= H5 g _=> //=; case i1; case i2; smt.
    qed.

    op initGates (base:int) size (f:int->bool->bool->'a) : 'a gates_t =
      let fill = fun g a b gg, gg.[(g, a, b) <- f g a b] in
      range base (base + size) FMap.empty
            (fun (g:int) gg, fill g false false (fill g false true (fill g true false (fill g true true gg)))).

    lemma get_initGates (base:int) size (f:int->bool->bool->'a) g a b :
      0 <= size =>
      (initGates base size f).[(g, a, b)] = if base <= g < base + size then Some (f g a b) else None.
    proof.
      simplify initGates.
      simplify FMap."_.[_]".
      elim/Induction.strongInduction size=> //.
      progress; case (j > 0)=> hj; last smt.
      rewrite range_ind_lazy /=; first smt.
      rewrite !FMap.get_set.
      case (g = base + j - 1)=> hg; first by case a; case b; smt.
      cut neq: forall x y, ((base + j - 1, x, y) = (g, a, b)) = false by smt.
      rewrite !neq /=.
      cut -> : base + j - 1 = base + (j - 1) by smt.
      rewrite H0; smt.
    qed.

    lemma in_dom_init_Gates base size (f:int->bool->bool->'a) (g:int) (a b:bool) :
      0 <= size =>  base <= g < base + size => in_dom (g, a, b) (initGates base size f).
    proof.
      simplify initGates.
      simplify in_dom.    
      elim/Induction.strongInduction size=> //.
      progress.
      case (j > 0)=> hj; last smt.
      rewrite range_ind_lazy /=; first smt.
      rewrite ! dom_set ! mem_add.
      case (g = base + j - 1)=> hg; first by case a;case b;smt.
      cut -> : base + j - 1 = base + (j - 1) by smt.
      rewrite H0; smt.
    qed.

    op enc (x:tokens_t) (fn:bool funct_t) (g:int) (x1:bool) (x2:bool) : word =
      let (n, m, q, aa, bb) = fst fn in
      let t1 = (getlsb (oget x.[(aa.[g], false)])) in
      let t2 = (getlsb (oget x.[(bb.[g], false)])) in
      let aa = (oget x.[(aa.[g], x1^^t1)]) in
      let bb = (oget x.[(bb.[g], x2^^t2)]) in
      E (tweak g (getlsb aa) (getlsb bb)) aa bb (oget x.[(g, oget (snd fn).[(g, x1^^t1, x2^^t2)])]).
  end Local.
  import Local.
  (* END INTRO *)

  (* BEGIN SCHEME DEFINITION *)
  clone Sch.Scheme as C1 with
    type Input.input_t = bool array,
    type Input.inputK_t = tokens_t,
    type Input.inputG_t = word array,
    op Input.encode (iK:inputK_t) (x:input_t) =
      init (length x) (fun k, oget iK.[(k, x.[k])]),
    op Input.inputG_len(x : word array) = Array.length x.

  clone C1 as C2 with
    type fun_t = bool funct_t,
    type funG_t = word funct_t,
    type output_t = bool array,
    type outputK_t = unit,
    type outputG_t = word array,
    type leak_t = topo_t,
    type rand_t = tokens_t,

    op validInputs (fn:fun_t) (i:Input.input_t) =
      let (n, m, q, aa, bb) = fst fn in
      n + q <= maxGates /\
      validCircuit bound fn /\ length i = n,

    pred validRand (fn:fun_t) (x:rand_t) =
      let (n, m, q, aa, bb) = fst fn in
      forall (i:int), 0 <= i < (n + q)%Int =>
        option_rect false (fun (x:word), true) x.[(i,false)] /\
        option_rect false (fun (x:word), true) x.[(i,true )] /\
        !getlsb (oget x.[(i,false)]) = getlsb (oget x.[(i,true )]) /\
        (n + q - m <= i => !(getlsb (oget x.[(i,false)]))),

    op phi (fn:fun_t) = fst fn,

    op eval (fn:fun_t) (i:Input.input_t) =
      let (n, m, q, aa, bb) = fst fn in
      Array.sub (GarbleTools.evalComplete q i (GarbleTools.extract (fun g x1 x2, (oget (snd fn).[(g, x1, x2)])) aa bb)) (n+q-m) m,

    op evalG (fn:funG_t) (i:Input.inputG_t) =
      let (n, m, q, aa, bb) = fst fn in
      let evalGate =
        fun g x1 x2,
          D (tweak g (W.getlsb x1) (W.getlsb x2)) x1 x2 (oget (snd fn).[(g, W.getlsb x1, W.getlsb x2)]) in
      Array.sub (GarbleTools.evalComplete q i (GarbleTools.extract evalGate aa bb)) (n+q-m) m,

    op funG (fn:fun_t) (r:rand_t) = 
      let (n, m, q, aa, bb) = fst fn in
      (fst fn, initGates n q (enc r fn)),

    op inputK (fn:fun_t) (r:tokens_t) =
      let (n, m, q, aa, bb) = fst fn in
      filter (fun x y, 0 <= fst x < n) r,

    op outputK (fn:fun_t) (r:rand_t) = tt,

    op decode(k:outputK_t, o: outputG_t) =
      map W.getlsb o,

    op pi_sampler(im : (topo_t * Input.input_t)) =
      let (n,m,q,aa,bb) = fst im in
      let gg = initGates n q (fun g (a b:bool), if g  < n + q - m then false else (snd im).[g-(n+q-m)]) in
      let x = init n (fun k, false) in
      ((fst im, gg), x).

  clone import SchSec.SchSecurity with theory Sch.Scheme = C2.

  import SchSecurity.Sch.Scheme.
  (* END SCHEME DEFINITION *)

  (* BEGIN THE CORRECTNESS PROOF *)
  (* This is not cleaned up! *)
  lemma sch_correct : D.Correct () => Sch.Correct ().
  proof.
    (*Some simplification before proving the main inductive formula *)
    simplify D.Correct Sch.Correct.
    simplify    validInputs    validRand    eval    decode    outputK    evalG    funG          encode    inputK.
    simplify C2.validInputs C2.validRand C2.eval C2.decode C2.outputK C2.evalG C2.funG.
    intros DKCHyp x fn input.
    elim/tuple2_ind fn=> fn topo ff h_fn/=.
    elim/tuple5_ind topo h_fn=> topo n m q aa bb h_topo h_fn /=.
    simplify fst snd.
    rewrite valid_wireinput.
    simplify validCircuitP.
    simplify fst snd.
    progress.

    pose n := length input.

    pose inputEnc := C2.Input.encode (C2.inputK ((n, m, q, aa, bb), ff) x) input.

    cut ->: W.getlsb = getlsb by done.
    pose eval:= (fun (g : int) (x1 x2 : word),
                   D (tweak g (getlsb x1) (getlsb x2)) x1 x2
                     (oget
                       (initGates n q (enc x ((n,m,q,aa,bb),ff))).[(g,getlsb x1,getlsb x2)])).

    (* This is the inductive formula that we want to prove *)
    cut main: forall (j:int), 0 <= j < n+q =>
                oget x.[(j, (GarbleTools.evalComplete q input (GarbleTools.extract (fun g x1 x2, oget ff.[(g, x1, x2)]) aa bb)).[j])]
                = (GarbleTools.evalComplete q inputEnc (GarbleTools.extract eval aa bb)).[j];first last.
    (* End case : As soon as we have this formula for all j we apply
        it to the output wire and get the final result with the decode
        and outputK definition *)
      (apply array_ext;split;first smt)=> j hj.
      rewrite get_map;first (rewrite Array.length_sub;smt).
      rewrite !get_sub;first 8 smt.
      rewrite -(main (j + (n + q - m)) _); first smt.
      pose v := (GarbleTools.evalComplete _ _ _).[_].
      by cut:= H9 (j + (n + q - m)) _; smt.
  
    intros j boundJ.
    cut : j < n + q by smt.
    cut : 0 <= j by smt; clear boundJ.
    elim/ Induction.strongInduction j.
    simplify evalComplete.
    pose ar1 := (GarbleTools.appendInit input q (GarbleTools.extract (fun (g : int) (x1 x2 : bool), oget ff.[(g, x1, x2)]) aa bb)).
    pose ar2 := (GarbleTools.appendInit inputEnc q (GarbleTools.extract eval aa bb)).
    intros k kPos hypRec kbound.
    case (n <= k)=> hk.
      (* Induction case : use the correction of one gate and the induction hypothesis*)
      rewrite /ar1 /ar2 ! appendInit_getFinal. smt. smt. simplify extract;smt. smt. smt. simplify extract;rewrite ! get_sub;smt.
      rewrite -/ar1 -/ar2.
      simplify extract.
      cut -> : (k - 1 + 1 = k) by smt.
      rewrite - ! hypRec;first 4 smt.
      simplify eval.
      rewrite get_initGates;first smt.
      simplify enc fst snd.
      cut ->: (getlsb (oget x.[(aa.[k], ar1.[aa.[k]])]) ^^
               getlsb (oget x.[(aa.[k], false)])) = ar1.[aa.[k]].
        case ar1.[aa.[k]]=> h;last smt.
        cut := H9 aa.[k] _;first clear h;smt.
        generalize (getlsb (oget x.[(aa.[k], false)]))=> a.
        generalize (getlsb (oget x.[(aa.[k], true)]))=> b.
        intros [_ [_ [HH _ ]]].
        by cut -> : b = ! a by smt;smt.
      cut ->: (getlsb (oget x.[(bb.[k], ar1.[bb.[k]])]) ^^
               getlsb (oget x.[(bb.[k], false)])) = ar1.[bb.[k]].
        case ar1.[bb.[k]]=> h;last smt.
        cut := H9 bb.[k] _;first clear h;smt.
        generalize (getlsb (oget x.[(bb.[k], false)]))=> a.
        generalize (getlsb (oget x.[(bb.[k], true)]))=> b.
        intros [_ [_ [HH _ ]]].
        by cut -> : b = ! a by smt;smt.
      case (n <= k < n + q); last smt (* absurd *).
      by rewrite /oget /=; do !rewrite -/(oget _); rewrite DKCHyp.

    (* Base case : prove main for the inputs by using encode and inputK definitions *)
    rewrite /ar1 /ar2 ! appendInit_get1;first 4 smt.
    rewrite -/ar1 -/ar2.
    simplify inputEnc C2.Input.encode C2.inputK fst.
    rewrite get_init /=;first smt.
    rewrite FMap.get_filter /=.
    by (cut -> : 0 <= k < n = true by smt).
  qed.
(* END THE CORRECTNESS PROOF *)

  (* BEGIN THE SECURITY PROOF *)
  op evali(fn:fun_t, i:Input.input_t, k:int) =
    let (n, m, q, aa, bb) = fst fn in
    (GarbleTools.evalComplete q i (GarbleTools.extract (fun g x1 x2, (oget (snd fn).[(g, x1, x2)])) aa bb)).[k].

  pred validInputsP (plain:fun_t*input_t) =
    let (n, m, q, aa, bb) = fst (fst plain) in
    (n + q)%Int <= maxGates /\ validCircuitP bound (fst plain) /\ Array.length (snd plain) = n.

  (*Contains the random used for a normal garbling *)
  module R = {
    var t:bool array
    var xx:tokens_t
  }.

  (* This is the circuit to garble (fixed after the adversary runs and the challenge is chosen) *)
  module C = {
    var f:fun_t
    var x:input_t
    var n:int
    var m:int
    var q:int
    var aa:int array
    var bb:int array
    var gg:bool gates_t
    var v:bool array
  }.

  (* handle low level part of garbling *)
  module G = {
    var pp:word gates_t
    var yy:word array
    var randG: word gates_t
    var a:int
    var b:int
    var g:int
  }.

  module Rand:EncSecurity.Rand_t = {
    proc gen(l:topo_t): rand_t = {
      var i:int;
      var tok1,tok2:word;
      var trnd:bool;
    
      (C.n, C.m, C.q, C.aa, C.bb) = l;
      R.xx = FMap.empty;
      i = 0;
      while (i < C.n + C.q) {
        trnd = $Dbool.dbool;
        trnd = if (i >= C.n + C.q - C.m) then false else trnd;
        tok1 = $Dword.dwordLsb ( trnd);
        tok2 = $Dword.dwordLsb (!trnd);

        R.xx.[(i, false)] = tok1;
        R.xx.[(i,  true)] = tok2;

        i = i + 1;
      }
      return R.xx;
    }
  }.

  section.
    (* This modules contains values set at the beginning and not modified after initialization *)
    module CV = {
      var l : int
    }.

    module Cf = {
      proc init(p:fun_t*input_t) : unit = {
        var i : int;
        C.f = fst p;
        C.x = snd p;
        (C.n, C.m, C.q, C.aa, C.bb) = fst (fst p);
        C.gg = snd (fst p);
        C.v = Array.init (C.n + C.q) (fun (i:int), false);
        i = 0;
        while (i < C.n + C.q) {
          C.v.[i] = evali (fst p) C.x i;
          i = i + 1;
        }
      }
    }.

    local lemma CinitL: phoare[Cf.init: true ==> true] = 1%r.
    proof. by proc; while true (C.n + C.q - i); auto; smt. qed.

    local lemma CinitH (plain:fun_t*input_t):
      validInputsP plain =>
      hoare[Cf.init: p = plain ==>
                     C.f = fst plain /\
                     C.x = snd plain /\
                     (C.n, C.m, C.q, C.aa, C.bb) = fst (fst plain) /\
                     C.gg = snd (fst plain) /\
                     Array.length C.v = (C.n + C.q) /\
                     validInputsP (C.f, C.x)].
    proof.
      move=> vIn; proc.
      while (Array.length C.v = C.n + C.q).
        by auto; smt.
      auto=> //= &m p_plain; subst.
      (* This is all just a matter of careful rewriting in which smt gets lost. We do it by hand. *)
      move: vIn; rewrite /validInputsP /validCircuitP /fst /fst /fst /snd /snd.
      elim plain=> [fn] ii /=.
      elim/tuple2_ind fn=> fn tt gg fn_def /=; subst.
      elim/tuple5_ind tt=> tt n m q aa bb tt_def /=; subst.
      progress.
      by rewrite length_init //;smt.
    qed.

    local equiv CinitE: Cf.init ~ Cf.init:
      ={p} /\
      (validInputsP p){1} ==>
      ={glob C} /\
      Array.length C.v{1} = (C.n + C.q){1} /\
      C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
      validInputsP (C.f, C.x){1} /\
      (forall i, 0 <= i < C.n{2} => C.v{2}.[i] = C.x{2}.[i]) /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = oget C.gg.[(i, C.v{2}.[C.aa{2}.[i]], C.v{2}.[C.bb.[i]])]){2}.
    proof.
      proc.
      while ((Array.length C.v = C.n + C.q){1} /\
             ={glob C, i, p} /\
             fst p{2} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){2} /\
             0 <= C.n{2} /\
             (forall j, C.n <= j < C.n + C.q => 0 <= C.aa.[j] < j){2} /\
             (forall j, C.n <= j < C.n + C.q => 0 <= C.bb.[j] < j){2} /\
             (forall j, C.n <= j < i => C.v{2}.[j] = oget C.gg.[(j, C.v{2}.[C.aa{2}.[j]], C.v{2}.[C.bb.[j]])]){2} /\
             (forall j,   0 <= j < i => C.v{2}.[j] = evali (fst p) C.x j){2} /\
             0 < C.q{2} /\
             length C.x{2} = C.n{2}).
        auto=> //=.
        move=> &1 &2 [[lenCv]] [[[eqCv [eqCb [eqCq [eqCn [eqCf [eqCx [eqCm [eqCa eqCg]]]]]]]]]] [eqi eqp]; subst.
        rewrite /fst; elim (p{2})=> fn ii /=.
        elim/tuple2_ind fn=> fn tt gg fn_def [[]]; subst.
        elim/tuple5_ind tt=> tt n m q aa bb tt_def []; subst.
        move=> n_Cn m_Cm q_Cq aa_Caa bb_Cbb gg_Cgg; subst.
        move=> [leq0_Cn] [vA] [vB] [vGates] [vWires] [lt0_q] lenCx [i_lt_CnCq] H {H}.
        split=> //=; first smt.
        split; first smt.
        split=> //.
        split=> //.
        split.
          move=> j j_bnd; case (i{2} = j).
            move=> i_j; subst; rewrite !get_set /=; first 3 smt.
            cut -> /=: (j = C.aa{2}.[j]) = false by smt.
            cut -> /=: (j = C.bb{2}.[j]) = false by smt.
            rewrite /evali /fst /snd /=.
            rewrite /evalComplete appendInit_getFinal //; first smt.
              by rewrite /extract //= !get_sub //; smt.
            rewrite /extract (_: j - 1 + 1 = j) //=; first smt.
              congr; congr; split=> //.
                by rewrite vWires; first smt.
                by rewrite vWires; first smt.
            rewrite -neqF=> j_neq_i; cut j_lt_i: j < i{2} by smt.
            rewrite get_set 2:j_neq_i /=; first smt.
            rewrite vGates; first smt.
            cut ->: forall x, C.v{2}.[i{2} <- x].[C.aa{2}.[j]] = C.v{2}.[C.aa{2}.[j]] by smt.
            cut ->: forall x, C.v{2}.[i{2} <- x].[C.bb{2}.[j]] = C.v{2}.[C.bb{2}.[j]] by smt.
            done.
        split=> //=.
          move=> j j_bnd; case (i{2} = j).
            by move=> i_j; subst; rewrite !get_set //=; first smt.
            rewrite -neqF=> j_neq_i; cut j_lt_i: j < i{2} by smt.
            rewrite get_set 2:j_neq_i /=; first smt.
            by rewrite vWires; first smt.
      auto; move=> &1 &2 [eqP vIn] //=; subst.
      move: vIn; rewrite /validInputsP /validCircuitP /fst /fst /fst /snd /snd //=.
      elim (p{2})=> fn ii /=; subst.
      elim/tuple2_ind fn=> fn tt gg fn_def /=; subst.
      elim/tuple5_ind tt=> tt n m q aa bb tt_def /=; subst.
      by progress; expect 10 smt.
    qed.

    local equiv CinitE_rnd: Cf.init ~ Cf.init:
      EncSecurity.queryValid_IND (p{1}, p{2}) ==>
      ={C.n, C.m, C.q, C.aa, C.bb} /\
      (forall i, C.n{1} + C.q{1} - C.m{1} <= i < C.n{1} + C.q{1} => C.v{1}.[i] = C.v{2}.[i]) /\
      (eval C.f C.x){1} = (eval C.f C.x){2} /\
      (((C.n, C.m, C.q, C.aa, C.bb), C.gg) = C.f /\
       Array.length C.v = (C.n + C.q) /\
       validInputsP (C.f, C.x)){1} /\
      (forall i, 0 <= i < C.n{1} => C.v{1}.[i] = C.x{1}.[i]) /\
      (((C.n, C.m, C.q, C.aa, C.bb), C.gg) = C.f /\
       Array.length C.v = (C.n + C.q) /\
       validInputsP (C.f, C.x)){2} /\
      (forall i, 0 <= i < C.n{1} => C.v{2}.[i] = C.x{2}.[i]).
    proof.
      proc.
      seq 6 6: (={C.v,C.n,C.m,C.q,C.aa,C.bb,i} /\
                i{1} = 0 /\
                Array.length C.v{1} = C.n{1} + C.q{1} /\
                eval C.f{1} C.x{1} = eval C.f{2} C.x{2} /\
                ((C.n,C.m,C.q,C.aa,C.bb),C.gg){1} = C.f{1} /\
                ((C.n,C.m,C.q,C.aa,C.bb),C.gg){2} = C.f{2} /\
                C.f{1} = p{1}.`1 /\
                C.f{2} = p{2}.`1 /\
                length C.x{1} = C.n{1} /\
                length C.x{2} = C.n{2} /\
                length C.x{1} + C.q{1} <= maxGates /\
                1 < length C.x{1} /\
                0 < C.m{1} /\
                0 < C.q{1} /\
                C.m{1} <= C.q{1} /\
                C.n{1} + C.q{1} - C.m{1} = bound /\
                length C.aa{1} = C.n{1} + C.q{1} /\
                length C.bb{1} = C.n{1} + C.q{1} /\
                (forall g b1 b2,
                   C.n{1} <= g < C.n{1} + C.q{1} => in_dom (g,b1,b2) C.gg{1}) /\
                (forall g b1 b2,
                   C.n{1} <= g < C.n{1} + C.q{1} => in_dom (g,b1,b2) C.gg{2}) /\
                (forall i,
                   C.n{1} <= i < C.n{1} + C.q{1} =>
                   0 <= C.aa{1}.[i] /\ C.bb{1}.[i] < i /\
                   C.bb{1}.[i] < C.n{1} + C.q{1} - C.m{1} /\ C.aa{1}.[i] < C.bb{1}.[i])).
        auto; rewrite /EncSecurity.queryValid_IND /EncSecurity.Encryption.valid_plain /EncSecurity.Encryption.leak.
        rewrite /validInputs /C2.validInputs /validCircuit /validGG /phi /C2.phi /fst /fst /fst /snd /snd /snd //=.
        move=> &1 &2.
        elim (p{1})=> fn1 ii1 /=.
        elim/tuple2_ind fn1=> fn1 tt1 gg1 fn1_def /=; subst.
        elim/tuple5_ind tt1=> tt1 n1 m1 q1 aa1 bb1 tt1_def /=; subst.
        elim (p{2})=> fn2 ii2 /=.
        elim/tuple2_ind fn2=> fn2 tt2 gg2 fn2_def /=; subst.
        elim/tuple5_ind tt2=> tt2 n2 m2 q2 aa2 bb2 tt2_def /=; subst.
        cut ->: (fun i b, b /\ 0 <= aa1.[i] /\ bb1.[i] < i /\ bb1.[i] < n1 + q1 - m1 /\ aa1.[i] < bb1.[i])
                = (fun i b,
                    b /\ (fun j, 0 <= aa1.[j] /\ bb1.[j] < j /\ bb1.[j] < n1 + q1 - m1 /\ aa1.[j] < bb1.[j]) i)
          by done.
        cut ->: (fun g b,
                   b /\
                   in_dom (g,false,false) gg1 /\
                   in_dom (g,false,true ) gg1 /\
                   in_dom (g,true ,false) gg1 /\
                   in_dom (g,true ,true ) gg1) =
                 (fun g b,
                   b /\
                   (fun j,
                     in_dom (j,false,false) gg1 /\
                     in_dom (j,false,true ) gg1 /\
                     in_dom (j,true ,false) gg1 /\
                     in_dom (j,true ,true ) gg1) g)
          by done.
        cut ->: (fun i b, b /\ 0 <= aa2.[i] /\ bb2.[i] < i /\ bb2.[i] < n2 + q2 - m2 /\ aa2.[i] < bb2.[i])
                = (fun i b,
                    b /\ (fun j, 0 <= aa2.[j] /\ bb2.[j] < j /\ bb2.[j] < n2 + q2 - m2 /\ aa2.[j] < bb2.[j]) i)
          by done.
        cut ->: (fun g b,
                   b /\
                   in_dom (g,false,false) gg2 /\
                   in_dom (g,false,true ) gg2 /\
                   in_dom (g,true ,false) gg2 /\
                   in_dom (g,true ,true ) gg2) =
                 (fun g b,
                   b /\
                   (fun j,
                     in_dom (j,false,false) gg2 /\
                     in_dom (j,false,true ) gg2 /\
                     in_dom (j,true ,false) gg2 /\
                     in_dom (j,true ,true ) gg2) g)
          by done.
        rewrite !rangeb_forall //=.
        progress [-split].
        do 11!(split; first smt).
        split; first by move=> g b1 b2 g_bnd; cut:= H7 g _=> //; case b1; case b2.
        split; first by move=> g b1 b2 g_bnd; cut:= H17 g _; [smt|]; case b1; case b2.
        smt.
      conseq* (_: _ ==>
                  (forall j,
                     C.n{2} + C.q{2} - C.m{2} <= j < C.n{2} + C.q{2} =>
                     C.v{1}.[j] = C.v{2}.[j]) /\
                  length C.v{1} = C.n{2} + C.q{2} /\
                  length C.v{2} = C.n{2} + C.q{2} /\
                  (forall j, 0 <= j < C.n{2} => C.v{1}.[j] = C.x{1}.[j]) /\
                  (forall j, 0 <= j < C.n{2} => C.v{2}.[j] = C.x{2}.[j])).
        by progress; expect 3 smt.
      while (={C.n,C.m,C.q,C.aa,C.bb,i} /\
             0 <= C.q{2} /\
             0 <= C.n{2} + C.q{2} - C.m{2} /\
             (Array.length C.v = C.n + C.q){1} /\
             (Array.length C.v = C.n + C.q){2} /\
             (eval C.f C.x){1} = (eval C.f C.x){2} /\
             (forall j, C.n{1} + C.q{1} - C.m{1} <= j < i{1} => C.v{1}.[j] = C.v{2}.[j]) /\
             (forall j, 0 <= j < i{1} /\ j < C.n{1}=> C.v{2}.[j] = C.x{2}.[j]) /\
             C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
             (forall j, 0 <= j < i{1} /\ j < C.n{1} => C.v{1}.[j] = C.x{1}.[j]) /\
             C.f{2} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){2} /\
             p{1}.`1 = C.f{1} /\
             p{2}.`1 = C.f{2} /\
             (C.n = length C.x){1} /\ (C.n = length C.x){2}).
        auto.
        progress; first 2 smt.
          rewrite !Array.get_set; first 2 smt.
          move: H3; rewrite /eval /C2.eval /evali /fst /fst /snd H7 H8 /=.
          pose w := length C.x{1} + C.q{2} - C.m{2}.
          move=> H3.
          cut: (sub (evalComplete C.q{2} C.x{1}
                      (extract (fun g x1 x2,
                                  oget C.gg{1}.[(g, x1, x2)]) C.aa{2} C.bb{2}))
                    w C.m{2}).[i{2}-w] =
               (sub (evalComplete C.q{2} C.x{2}
                      (extract (fun g x1 x2,
                                  oget C.gg{2}.[(g, x1, x2)]) C.aa{2} C.bb{2}))
                    w C.m{2}).[i{2}-w]
            by rewrite H3.
          by case ((i{2} = j){2}) => _; rewrite !Array.get_sub; smt.
          rewrite Array.get_set; first smt.
          by case ((i = j){2}) => _;
            rewrite /evali /evalComplete ?appendInit_get1; smt.
          rewrite Array.get_set;first smt.
          by case ((i = j){2}) => _;
            rewrite /evali /evalComplete ?appendInit_get1; smt.
      skip; progress; expect 10 smt.
    qed.

    local module Rf = {
      proc init(useVisible:bool): unit = {
        var i, tok1, tok2, v, trnd;

        R.t = Array.init (C.n + C.q) (fun x, false);
        R.xx = FMap.empty;
        i = 0;
        while (i < C.n + C.q) {
          trnd = $Dbool.dbool;
          v = if useVisible then C.v.[i] else false;
          trnd = if (i >= C.n + C.q - C.m) then v else trnd;
          tok1 = $Dword.dwordLsb ( trnd);
          tok2 = $Dword.dwordLsb (!trnd);

          R.t.[i] = trnd;

          R.xx.[(i, v)] = tok1;
          R.xx.[(i,!v)] = tok2;

          i = i + 1;
        }
      } 
    }.

    local lemma RgenInitL: phoare[Rf.init: true ==> true] = 1%r.
    proof.
      proc.
      while true (C.n + C.q - i).
        auto; progress; [smt| | |smt];
        expect 2 by cut:= Dword.dwordLsb_lossless; rewrite /weight /True=> ->.
      by auto; smt.
    qed.

    local lemma RinitH:
      hoare[Rf.init: 0 <= C.m <= C.n + C.q /\
                     fst C.f = (C.n, C.m, C.q, C.aa, C.bb) ==>
                     validRand C.f R.xx /\
                     Array.length R.t = (C.n+C.q)].
    proof.
      proc.
      while (0 <= C.m <= C.n + C.q /\
             Array.length R.t = C.n + C.q /\
             (forall (j:int), 0 <= j => j < i =>
                option_rect false (fun (x : word), true) R.xx.[(j, false)] /\
                option_rect false (fun (x : word), true) R.xx.[(j, true)] /\
                getlsb (oget R.xx.[(j, false)]) <> getlsb (oget R.xx.[(j, true)]) /\
             (C.n + C.q - C.m <= j => !(getlsb (oget R.xx.[(j, false)]))))); first last.
        auto; progress [-split].
        split; first smt.
        progress [-split].
        rewrite /validRand /C2.validRand; elim (fst C.f{hr}) H1=> n m q aa bb [-> -> -> -> ->] /=.
        by split=> //= i [i_lb i_ub]; move: (H6 i _ _); first 2 smt.
      auto; progress.
        smt.
        case (j < i{hr})=> h.
          by rewrite !FMap.get_set_neq; expect 3 smt.
        cut ->: i{hr} = j by smt.
        rewrite !FMap.get_set /=.
        by case (useVisible{hr} && C.v{hr}.[j]).
        case (j < i{hr})=> h.
          by rewrite !FMap.get_set_neq; expect 3 smt.
        cut ->: i{hr} = j by smt.
        rewrite !FMap.get_set /=.
        by case (useVisible{hr} && C.v{hr}.[j])=> //=.
        case (j < i{hr})=> h.
          by rewrite !FMap.get_set_neq; expect 5 smt.
        cut ->: i{hr} = j by smt.
        rewrite !FMap.get_set /=.
        by case (useVisible{hr} && C.v{hr}.[j]); rewrite //= oget_some //=; smt.
        case (j < i{hr})=> h.
          by rewrite !FMap.get_set_neq; expect 3 smt.
        cut ->: i{hr} = j by smt.
        rewrite !FMap.get_set /=.
        by case (useVisible{hr} && C.v{hr}.[j]); rewrite //= oget_some //=; smt.
    qed.

    local equiv RinitE:
      Rf.init ~ Rf.init:
        ={useVisible, glob C} /\
        (0 <= C.m <= C.n + C.q){1} /\
        (fst C.f = (C.n, C.m, C.q, C.aa, C.bb)){1} ==>
        ={glob R} /\
        (validRand C.f R.xx){1} /\
        (Array.length R.t = (C.n+C.q)){1}.
    proof. by conseq* (_: ={useVisible,glob C} ==> ={glob R}) RinitH; sim. qed.

    local equiv RinitE_rnd: Rf.init ~ Rf.init:
      ={useVisible, C.n, C.m, C.q, C.aa, C.bb} /\
      (forall i, C.n{1} + C.q{1} - C.m{1} <= i < C.n{1} + C.q{1} =>
         C.v{1}.[i] = C.v{2}.[i]) /\
      (0 <= C.m <= C.n + C.q){1} /\
      useVisible{1} /\
      (fst C.f = (C.n, C.m, C.q, C.aa, C.bb)){1} ==>
      ={R.t} /\
      (forall g, 0 <= g < (C.n + C.q){1} => R.xx.[(g, C.v.[g])]{1} = R.xx.[(g, C.v.[g])]{2}) /\
      (forall g, 0 <= g < (C.n + C.q){1} => R.xx.[(g, !C.v.[g])]{1} = R.xx.[(g, !C.v.[g])]{2}) /\
      (0 <= C.m <= C.n + C.q /\
       validRand C.f R.xx /\
       Array.length R.t = (C.n+C.q)){1}.
    proof.
      conseq* (_: _ ==>
                  ={R.t} /\
                  (forall g, 0 <= g < C.n{1} + C.q{1} => R.xx.[(g,C.v.[g])]{1} = R.xx.[(g,C.v.[g])]{2}) /\
                  (forall g, 0 <= g < C.n{1} + C.q{1} => R.xx.[(g,!C.v.[g])]{1} = R.xx.[(g,!C.v.[g])]{2}))
               RinitH=> //=.
      proc.
      while (={useVisible, C.n, C.m, C.q, C.aa, C.bb, i, R.t} /\
             (forall g, 0 <= g < i{1} =>
                R.xx.[(g, C.v.[g])]{1} = R.xx.[(g, C.v.[g])]{2}) /\
             (forall g, 0 <= g < i{1} =>
                R.xx.[(g, !C.v.[g])]{1} = R.xx.[(g, !C.v.[g])]{2}) /\
             (0 <= C.m <= C.n + C.q){1} /\
             (Array.length R.t = C.n + C.q){1} /\
             useVisible{1} /\
             (forall i, C.n{1} + C.q{1} - C.m{1} <= i < C.n{1} + C.q{1} => C.v{1}.[i] = C.v{2}.[i]){1});
        first last.
        auto; progress;
          first 2 by rewrite !get_empty.
          smt.
          by rewrite H6; first smt.
          by rewrite H7; first smt.
      auto; progress; rewrite H4 //=; first 5 smt.
      by rewrite !FMap.get_set; smt.
      by rewrite !FMap.get_set; smt.
      smt.
    qed.

    pred t_xor (sup:int) (t1 t2 v:bool array) = forall i,
      0 <= i < sup =>
      t1.[i] = t2.[i] ^^ v.[i].

    (** TODO: Rework with conseq* *)
    local equiv RgenClassicVisibleE:
      Rf.init ~ Rf.init:
        ={glob C} /\ fst C.f{2} = (C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}) /\
        0 <= C.n{2} + C.q{2} /\
        useVisible{1} = true /\
        useVisible{2} = false ==>
        ={R.xx} /\
        (validRand C.f{1} R.xx{1}) /\
        (forall i v, 0 <= i < C.n + C.q => getlsb (oget R.xx.[(i, v)]) = v^^R.t.[i]){2} /\
        Array.length R.t{1} = (C.n{1}+C.q{1}) /\
        t_xor (C.n{1} + C.q{1}) R.t{1} R.t{2} C.v{1}.
    proof.
    proc; while (={i, R.xx, glob C} /\
                useVisible{1} = true /\
                useVisible{2} = false /\
                t_xor i{1} R.t{1} R.t{2} C.v{1} /\
                0 <= i{2} /\
                length R.t{1} = C.n{2} + C.q{2} /\
                length R.t{2} = C.n{2} + C.q{2} /\
                (forall j v, 0 <= j < i => getlsb (oget R.xx.[(j, v)]) = v^^R.t.[j]){2} /\ 
                (forall (j:int), 0 <= j < i =>
                  option_rect false (fun (x : word), true) R.xx.[(j, false)] /\
                  option_rect false (fun (x : word), true) R.xx.[(j, true)] /\
                  getlsb (oget R.xx.[(j, false)]) <> getlsb (oget R.xx.[(j, true)]) /\
                  (C.n + C.q - C.m <= j => !(getlsb (oget R.xx.[(j, false)])))){2} /\
                (fst C.f = (C.n, C.m, C.q, C.aa, C.bb)){2}).
      case (C.v{1}.[i{1}] = false).
        do !(wp; rnd); skip; progress=> //;
          first 9 (rewrite /t_xor //=; progress=> //; try (cut := H i0); smt).
        rewrite ! FMap.get_set get_set;first smt.
        case (i{2} = j)=> h;[cut := H16;cut := H12;case v0=> hv0 /= |cut := H3 j v0 _].
          by rewrite h //=; smt.
          by rewrite h (_: ((j,true) = (j,false)) = false) //=; smt.
          smt.
          smt.
        by rewrite !FMap.get_set; case (i{2} = j)=> h;[ |cut := H4 j _]; smt.
        by rewrite !FMap.get_set; case (i{2} = j)=> h;[ |cut := H4 j _]; smt.
        by rewrite !FMap.get_set; case (i{2} = j)=> h;[ |cut := H4 j _]; smt.
        by rewrite !FMap.get_set; case (i{2} = j)=> h;[ |cut := H4 j _]; smt.
      swap{1} 4 1; do 2!(wp; rnd); wp; rnd (fun (x:bool), !x); skip;(progress=> //;first 6 smt).
        by rewrite FMap.set_set; smt.
        by simplify t_xor; progress; cut:= H i0; smt.
        smt.
        smt.
        smt.
        rewrite !FMap.get_set get_set; first smt.
        case (i{2} = j)=> h.
          subst; case v0=> h /=.
            by rewrite oget_some; smt.
            by rewrite (_: ((j,true) = (j,false)) = false); smt.
          by cut:= H3 j v0; smt.
        by rewrite !FMap.get_set; case (i{2} = j)=> h;[ |cut := H4 j]; smt.
        by rewrite !FMap.get_set; case (i{2} = j)=> h;[ |cut := H4 j]; smt.
        by rewrite !FMap.get_set; case (i{2} = j)=> h;[subst=> /= |cut := H4 j]; smt.
        by rewrite !FMap.get_set; case (i{2} = j)=> h;[ |cut := H4 j]; smt.
      wp; skip; progress=> //;simplify validRand C2.validRand;smt.
    qed.

    module type G_t = {
      proc getInfo() : int*int*bool
    }.

    module Gf : G_t = {
      proc garb(yy:word, alpha:bool, bet:bool): unit = {
        G.pp.[(G.g, (R.t.[G.a]^^alpha), (R.t.[G.b]^^bet))] =
                     (E
                       (tweak G.g (R.t.[G.a]^^alpha) (R.t.[G.b]^^bet))
                       (oget R.xx.[(G.a, (alpha^^C.v.[G.a]))])
                       (oget R.xx.[(G.b, (  bet^^C.v.[G.b]))])
                       yy);
      }

      proc garbD1(rand:bool, alpha:bool, bet:bool): unit = {
        var tok:word;

        tok = $Dword.dword;
        if (rand) G.randG.[(G.g,alpha,bet)] = tok;
      }

      proc garbD2(rand:bool, alpha:bool, bet:bool) : word = {
        var yy:word;

        yy = if rand
             then oget G.randG.[(G.g,alpha,bet)]
             else oget R.xx.[(G.g, (oget C.gg.[(G.g, C.v.[G.a]^^alpha,C.v.[G.b]^^bet)]))];
        garb(yy, alpha, bet);
        return yy;
      }

      proc garbD(rand:bool, alpha:bool, bet:bool): word = {
        var yy : word;

        garbD1(rand, alpha, bet);
        yy = garbD2(rand, alpha, bet);
        return yy;
      }

      proc getInfo(): int*int*bool = {
        return (G.a, G.b, C.gg.[(G.g,(!C.v.[G.a]), false)] = C.gg.[(G.g,(!C.v.[G.a]), true)]);
      }
    }.

    local lemma GgarbL    : islossless Gf.garb    by (proc; wp).

    local lemma GgarbD1L  : islossless Gf.garbD1  by (proc; wp; rnd; skip; smt).

    local lemma GgarbD2L  : islossless Gf.garbD2  by (proc; call GgarbL; wp).

    local lemma GgarbDL   : islossless Gf.garbD   by (proc; call GgarbD2L; call GgarbD1L).

    local lemma GgetInfoL : islossless Gf.getInfo by (proc; wp).

    local equiv GgarbE  : Gf.garb   ~ Gf.garb  :
      ={glob G, C.n, C.m, C.q, C.aa, C.bb, R.t,   yy, alpha, bet} /\
      (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){1} = (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){2} /\
      (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){1} = (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){2} ==>
      ={glob G, res}.
    proof. by proc; auto; progress; rewrite H H0. qed.

    local equiv GgarbD1E: Gf.garbD1 ~ Gf.garbD1:
      ={glob G, C.n, C.m, C.q, C.aa, C.bb, rand, alpha, bet} ==>
      ={glob G, res}
    by (proc; wp; rnd; skip; smt).

    local equiv GgarbD2E: Gf.garbD2 ~ Gf.garbD2:
      ={glob G, glob C, glob R, rand, alpha, bet} ==>
      ={glob G, res}
    by (proc;call GgarbE;wp).

    local equiv GgarbD2E_rnd: Gf.garbD2 ~ Gf.garbD2:
      ={glob G, C.n, C.m, C.q, C.aa, C.bb, R.t, rand, alpha, bet} /\
      (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){1} = (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){2} /\
      (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){1} = (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){2} /\
      rand{1} = true ==>
      ={glob G, res}
    by (proc; call GgarbE; wp).

    local equiv GgarbDE: Gf.garbD  ~ Gf.garbD:
      ={glob G, glob C, glob R, rand, alpha, bet} ==>
      ={glob G, res}
    by (proc;call GgarbD2E;call GgarbD1E).

    local equiv GgarbDE_rnd: Gf.garbD  ~ Gf.garbD:
      ={glob G, C.n, C.m, C.q, C.aa, C.bb, R.t, rand, alpha, bet} /\
      (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){1} = (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){2} /\
      (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){1} = (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){2} /\
      rand{1} = true ==>
      ={glob G, res}
    by (proc;call GgarbD2E_rnd;call GgarbD1E).

    (* This is a alternative definition to Rf that will help to do the reduction
        ideally we should use only this definition *)
    module Rf2 = {
      proc initT(useVisible:bool): unit = {
        var i:int;
        var v,trnd:bool;

        R.t = Array.init (C.n + C.q) (fun x, false);
        i = 0;
        while (i < C.n + C.q) {
          trnd = $Dbool.dbool;
          v = if useVisible then C.v.[i] else false;
          trnd = if (i >= C.n + C.q - C.m) then v else trnd;
          R.t.[i] = trnd;
          i = i + 1;
        }
      } 
      proc initR(useVisible:bool): unit = {
        var i:int;
        var tok1,tok2:word;
        var v,trnd:bool;

        R.xx = FMap.empty;
        i = 0;
        while (i < C.n + C.q) {
          v = if useVisible then C.v.[i] else false;
          trnd = R.t.[i];
          tok1 = $Dword.dwordLsb ( trnd);
          tok2 = $Dword.dwordLsb (!trnd);

          R.xx.[(i, v)] = tok1;
          R.xx.[(i,!v)] = tok2;

          i = i + 1;
        }
      } 
      proc init(useVisible:bool): unit = {
        initT(useVisible);
        initR(useVisible);
      }
    }.

    local lemma Rf2InitTL: islossless Rf2.initT.
    proof. by proc; while true (C.n + C.q - i); auto; smt. qed.

    local lemma Rf2InitRL: islossless Rf2.initR.
    proof. by proc; while true (C.n + C.q - i); auto; smt. qed.

    local lemma Rf2InitL: islossless Rf2.init.
    proof. by proc; call Rf2InitRL; call Rf2InitTL. qed.

    module RedComon(A:EncSecurity.Adv_IND_t) = {
      var query : (fun_t * input_t) * (fun_t*input_t)
      var c : bool

      proc preInit(): unit = {
        query= witness;
        c    = witness;
        CV.l = $Dinter.dinter 0 (bound-1);
        C.f  = witness;
        C.x  = witness;
        C.n  = witness;
        C.m  = witness;
        C.q  = witness;
        C.aa = witness;
        C.bb = witness;
        C.gg = witness;
        C.v  = witness;
        R.t  = witness;
        R.xx = witness;
        G.randG = witness;
        G.b  = witness;
        G.pp = witness;
        G.yy = witness;
        G.a  = witness;
        G.g  = witness;
      }

      proc genQuery(rand:bool, alpha:bool, bet:bool): query = {
        var tw : word;
        var gamma, pos : bool;
        var i, j : int*bool;
        var b: bool;
        tw = tweak G.g (R.t.[G.a] ^^ alpha) (R.t.[G.b] ^^ bet);
        gamma = C.v.[G.g] ^^ (oget C.gg.[(G.g, C.v.[G.a] ^^ alpha, C.v.[G.b] ^^ bet)]);
        if (G.a = CV.l)
        {
          pos = true;
          i = (G.b, R.t.[G.b]^^bet);
         }
         else
         {
           pos = false;
           i = (G.a, R.t.[G.a]^^alpha);
         }
         b = $Dbool.dbool;
         if (rand)
         {
           j = ((G.g+C.n+C.q), b);
         }
         else
         {
           j = (G.g, R.t.[G.g]^^gamma);
         }
         return (i, j, pos, tw);
      }
      proc doAnswer(rand:bool, alpha:bool, bet:bool, ans:answer): unit = {
        var kki, kkj, zz : word;
        var gamma : bool;
        gamma = C.v.[G.g] ^^ (oget C.gg.[(G.g, C.v.[G.a]^^alpha, C.v.[G.b]^^bet)]);
        (kki, kkj, zz) = ans;
        G.pp.[(G.g, R.t.[G.a]^^alpha, R.t.[G.b]^^bet)] = zz;
        if (G.a = CV.l)
          R.xx.[(G.b, C.v.[G.b]^^bet)] = kki;
        else
          R.xx.[(G.a, C.v.[G.a]^^alpha)] = kki;
        if (rand)
          G.randG.[(G.g, true, true)] = kkj;
        else
          R.xx.[(G.g, C.v.[G.g]^^gamma)] = kkj;
      }
      proc pre(info:bool): unit = {
        var p : fun_t*input_t;
        query = A.gen_query();
        c = $Dbool.dbool;
        p = (if c then snd query else fst query);
        R.xx = FMap.empty;
        Cf.init(p);
        Rf2.initT(true);
        R.t.[CV.l] = ! info;
      }
      proc coreNAda_pre(): query array = {
        var q1, q2 = witness;
        var qs = Array.empty;
        G.a = 0;
        G.b = 0;
        G.yy = Array.empty;
        G.randG = FMap.empty;
        G.pp = FMap.empty;
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          if (G.a = CV.l)
          {
            q1 = genQuery(false, true, false);
            q2 = genQuery(false, true, true);
            qs = qs || (q1::q2::Array.empty);
          }
          if (G.b = CV.l)
          {
            q1 = genQuery(false, false, true);
            q2 = genQuery(true, true, true);
            qs = qs || (q1::q2::Array.empty);
          }
          G.g = G.g + 1;
        }
        return qs;
      }
      proc coreNAda_post(answers:answer array): unit = {
        var ans = 0;
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          if (G.a = CV.l)
          {
            doAnswer(false, true, false, answers.[ans]);
            ans=ans+1;
            doAnswer(false, true, true, answers.[ans]);
            ans=ans+1;
          }
          if (G.b = CV.l)
          {
            doAnswer(false, false, true, answers.[ans]);
            ans=ans+1;
            doAnswer(true, true, true, answers.[ans]);
            ans=ans+1;
          }
          G.g = G.g + 1;
        }
      }
      proc sampleTokens() : unit = {
        var i : int;
        var tok : word;
        i = 0;
        while (i < C.n + C.q) {
          if (! in_dom (i,  C.v.[i]) R.xx)
          {
            tok = $Dword.dwordLsb ( R.t.[i]);
            R.xx.[(i,  C.v.[i])] = tok;
          }
          if (! in_dom (i,  !C.v.[i]) R.xx /\ i <> CV.l)
          {
            tok = $Dword.dwordLsb ( !R.t.[i]);
            R.xx.[(i,  !C.v.[i])] = tok;
          }
          i = i + 1;
        }
      }
      
      proc garble() : unit = {
        var tok1, tok2 : word;
        G.g = C.n;
        G.yy = Array.init (C.n + C.q) (fun x, (W.zeros));
        G.pp = FMap.empty;
        G.randG = FMap.empty;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          Gf.garb(oget R.xx.[(G.g, C.v.[G.g])], false, false);
          if (G.a <> CV.l /\ G.b <> CV.l)
          {
            tok2 = Gf.garbD(G.a<=CV.l,  true, false);
            tok2 = Gf.garbD(G.b<=CV.l, false,  true);
            tok1 = Gf.garbD(G.a<=CV.l,  true,  true);
            if ((G.a<=CV.l<=G.b) /\ (C.gg.[(G.g, !C.v.[G.a], false)] = C.gg.[(G.g, !C.v.[G.a], true)]))
              Gf.garb(tok1, true, false);
          }
          else
          {
            if (G.a=CV.l)
            {
              tok2 = Gf.garbD(false, false,  true);
            }
            else
            {
              tok2 = Gf.garbD( true,  true, false);
              if (C.gg.[(G.g, !C.v.[G.a], false)] = C.gg.[(G.g, !C.v.[G.a], true)])
                tok2 = Gf.garbD2(true, true, false);
              
            }
          }
          G.g = G.g + 1;
        }
      }
      proc post() : bool = {
        var r : bool;
        sampleTokens();
        garble();
        if ((EncSecurity.queryValid_IND query)%EncSecurity)
        {
          r = A.get_challenge((((C.n, C.m, C.q, C.aa, C.bb), G.pp), encode (inputK C.f R.xx) C.x, tt));
(* init C.n (lambda i, proj R.xx.[(i, C.v.[i])])*) 
          r = (r = c);
        }
        else
          r = $Dbool.dbool;
        return r;
      }
   }.

    declare module A : EncSecurity.Adv_IND_t {Rand, DKCm, RedComon}.
    axiom AgenL: islossless A.gen_query.
    axiom AgetL: islossless A.get_challenge.

    (** We use the following eager argument
        to use it in place of Rf.           **)
  module type OIB = {
    proc sample (_:int * bool * bool) : word
    proc sampleG (_:int * bool * bool) : word
  }.

  module type AIB(O:OIB) = {
    proc init (_: int) : (int * bool * bool) set { }
    proc main () : bool { O.sample O.sampleG }
  }.
    
  local module IB = {
    var xx  : (int * bool * bool, word) map
    var xxG : (int * bool * bool, word) map
  
    var gwork : (int * bool * bool) set
 
    proc init (work:(int * bool * bool) set) : unit = {
      gwork = work;
      xx  = FMap.empty;
      xxG = FMap.empty;
    }

    proc sample (ivb:int * bool * bool) : word = {
      var tok;
      tok = $Dword.dwordLsb(ivb.`3);
      xx = if (!in_dom ivb xx) then xx.[ivb <- tok] else xx;
      return oget xx.[ivb];
    }

    proc sampleG (ivb:int * bool * bool) : word = {
      var tok;
      tok = $Dword.dword;
      xxG = if (!in_dom ivb xxG) then xxG.[ivb <- tok] else xxG;
      return oget xxG.[ivb];
    }

    proc resample1 () : unit = {
      var work, f, y;
      work = gwork;
      while (work <> FSet.empty) {
        f = pick work;
        y = sample(f);
        work = rm f work;
      }
    }

    proc resampleG () : unit = {
      var work, f, y;
      work = gwork;
      while (work <> FSet.empty) {
        f = pick work;
        y = sampleG(f);
        work = rm f work;
      }
    }
    
    proc resample () : unit = {
      resample1();
      resampleG();
    }
  }.

  local module MIB (A:AIB) = {
    module A = A(IB)

    proc mainL (x:int) : bool = {
      var b, work0;
      work0 = A.init(x);
      IB.init(work0);
      b = A.main();
      return b;
    }

    proc mainL' (x:int) : bool = {
      var b, work0;
      work0 = A.init(x);
      IB.init(work0);
      b = A.main();
      IB.resample();
      return b;
    }

    proc mainE(x:int) : bool = {
      var b, work0;
      work0 = A.init(x);
      IB.init(work0);
      IB.resample();
      b = A.main();
      return b;
    } 
  }.

  local lemma losslessIBsample : phoare [IB.sample : true ==> true] = 1%r.
  proof.
    proc;auto;progress;smt.
  qed.

  local lemma losslessIBsampleG : phoare [IB.sampleG : true ==> true] = 1%r.
  proof.
    proc;auto;progress;smt.
  qed.

  local equiv MIB_mainL_mainL' (A0 <:AIB {IB}) : 
      MIB(A0).mainL ~ MIB(A0).mainL' : ={glob A0, x} ==> ={glob A0, res}.
  proof.
    proc.
    inline {2} IB.resample IB.resample1 IB.resampleG.
    while{2} (true) (card work1{2}).
      intros &m z; inline *;auto;smt.
    wp;while{2} (true) (card work{2}).
      intros &m z; inline *;auto;smt.
    wp;conseq (_: _ ==> ={glob A0, b}) => //.
      progress;smt.
    sim.
  qed.

  local equiv IBsample_in ivb1 (w1:word):
    IB.sample ~ IB.sample : 
       ={ivb, IB.xx} /\ IB.xx{1}.[ivb1] = Some w1 ==>
       ={res, IB.xx} /\ IB.xx{1}.[ivb1] = Some w1.
  proof.
    proc;wp;rnd;skip;progress [-split];smt.
  qed.

  local equiv IBsampleG_in ivb1 (w1:word):
    IB.sampleG ~ IB.sampleG : 
       ={ivb, IB.xxG} /\ IB.xxG{1}.[ivb1] = Some w1 ==>
       ={res, IB.xxG} /\ IB.xxG{1}.[ivb1] = Some w1.
  proof.
    proc;wp;rnd;skip;progress [-split];smt.
  qed.

  local equiv IBsample_diff ivb1 tok1 :
    IB.sample ~ IB.sample : 
       ={ivb} /\ !in_dom ivb1 IB.xx{1} /\
         IB.xx{2} = IB.xx{1}.[ivb1 <- tok1] /\ ivb1 <> ivb{1} ==>
       ={res} /\ !in_dom ivb1 IB.xx{1} /\ IB.xx{2} = IB.xx{1}.[ivb1 <- tok1].
  proof.
    proc;wp;rnd;skip;progress [-split];smt.
  qed.

  local equiv IBsampleG_diff ivb1 tok1 :
    IB.sampleG ~ IB.sampleG : 
       ={ivb} /\ !in_dom ivb1 IB.xxG{1} /\
         IB.xxG{2} = IB.xxG{1}.[ivb1 <- tok1] /\ ivb1 <> ivb{1} ==>
       ={res} /\ !in_dom ivb1 IB.xxG{1} /\ IB.xxG{2} = IB.xxG{1}.[ivb1 <- tok1].
  proof.
    proc;wp;rnd;skip;progress [-split];smt.
  qed.

  local lemma eager_query1:
      eager [IB.resample(); , IB.sample ~ IB.sample, IB.resample(); :
        ={ivb, glob IB} ==>       ={res, glob IB}].
  proof.
    eager proc.
    inline IB.resample. swap{1} 2 3.
    call (_: ={IB.xxG, IB.gwork}); first by sim.
    wp => /=;inline IB.resample1; swap{2} 4 -3.
    sp 1 1.
    exists * ivb{1}, IB.xx{1}; elim* => ivb1 xx1.
    case (in_dom ivb{1} IB.xx{1}).
       pose w1 := oget (xx1.[ivb1]).
       rnd{1}; while (={work, IB.xx} /\ IB.xx{1}.[ivb1] = Some w1).
         by wp;call (IBsample_in ivb1 w1);auto.
       auto;progress[-split];smt. 
    case (!mem ivb{1} work{1}).
        swap{1} 2 -1.
        while (={work} /\ !in_dom ivb1 IB.xx{1} /\ !mem ivb1 work{1} /\
                 IB.xx{2} = IB.xx{1}.[ivb1 <- tok{1}]).
         exists* tok{1};elim* => w1.
         by wp;call (IBsample_diff ivb1 w1);auto;progress;smt. 
        auto;progress [-split];smt. 
    conseq* (_ : _ ==> ={IB.gwork, IB.xx} /\ in_dom ivb{1} IB.xx{1} /\ 
                        result{2} = oget (IB.xx{2}.[ivb{2}])); first by progress;smt.
    transitivity{1} {tok = $Dword.dwordLsb ivb.`3;
                     while (! work = (FSet.empty)%FSet) {
                       f = pick work;                    
                       y = $Dword.dwordLsb f.`3;
                       IB.xx = 
                        if !in_dom f IB.xx then IB.xx.[f <- if f = ivb then tok else y]
                        else IB.xx;
                       work = rm f work;
                    }}
       (={ivb,glob IB, work} /\ ! in_dom ivb1 IB.xx{1} /\ mem ivb1 work{1}
             ==> ={ivb, glob IB}) 
        ( ivb1 = ivb{1} /\ xx1 = IB.xx{1} /\
           work{2} = IB.gwork{2} /\ work{1} = IB.gwork{1} /\ ={ivb, IB.gwork, IB.xx} /\
           ! in_dom ivb{1} IB.xx{1} /\ mem ivb{1} work{1} ==>
           ={IB.gwork, IB.xx} /\
             in_dom ivb{1} IB.xx{1} /\ result{2} = oget IB.xx{2}.[ivb{2}]) => //.
      progress. exists IB.gwork{2}, IB.xx{2}, IB.xxG{2}, ivb{2}, IB.gwork{2} => //.
      symmetry.
      conseq (_:  ={glob IB, work, ivb} ==> _) => //.
      eager while (H:tok = $Dword.dwordLsb ivb.`3; ~ tok = $Dword.dwordLsb ivb.`3; :
                ={ivb} ==> ={tok})=> //;[by sim | | by sim].
      inline IB.sample. swap{2} 7 -6; wp.
      case ((ivb = pick work){1}).
        by rnd{1};wp;rnd;wp;rnd{2};skip;progress;smt.
      auto;progress;smt.
    while (={work} /\ (mem ivb work = !in_dom ivb IB.xx){1} /\
           IB.xx{2}.[ivb{1}] = Some tok{1} /\
           IB.xx{2} = (if !in_dom ivb IB.xx then IB.xx.[ivb <- tok] else IB.xx){1}).
      inline IB.sample;auto;progress;smt. 
    auto;progress;smt. 
  qed.

  local lemma eager_queryG:
      eager [IB.resample(); , IB.sampleG ~ IB.sampleG, IB.resample(); :
        ={ivb, glob IB} ==>       ={res, glob IB}].
  proof.
    eager proc.
    inline IB.resample. swap{1} 1 4; swap{2} 4 1.
    call (_: ={IB.xx, IB.gwork}); first by sim.
    wp => /=;inline IB.resampleG; swap{2} 4 -3.
    sp 1 1.
    exists * ivb{1}, IB.xxG{1}; elim* => ivb1 xx1.
    case (in_dom ivb{1} IB.xxG{1}).
       pose w1 := oget (xx1.[ivb1]).
       rnd{1}; while (={work, IB.xxG} /\ IB.xxG{1}.[ivb1] = Some w1).
         by wp;call (IBsampleG_in ivb1 w1);auto.
timeout 10.
       auto;progress[-split];smt. 
timeout 3.
    case (!mem ivb{1} work{1}).
        swap{1} 2 -1.
        while (={work} /\ !in_dom ivb1 IB.xxG{1} /\ !mem ivb1 work{1} /\
                 IB.xxG{2} = IB.xxG{1}.[ivb1 <- tok{1}]).
         exists* tok{1};elim* => w1.
         by wp;call (IBsampleG_diff ivb1 w1);auto;progress;smt. 
        auto;progress [-split];smt. 
    conseq* (_ : _ ==> ={IB.gwork, IB.xxG} /\ in_dom ivb{1} IB.xxG{1} /\ 
                        result{2} = oget (IB.xxG{2}.[ivb{2}])); first by progress;smt.
    transitivity{1} {tok = $Dword.dword;
                     while (! work = (FSet.empty)%FSet) {
                       f = pick work;                    
                       y = $Dword.dword;
                       IB.xxG = 
                        if !in_dom f IB.xxG then IB.xxG.[f <- if f = ivb then tok else y]
                        else IB.xxG;
                       work = rm f work;
                    }}
       (={ivb,glob IB, work} /\ ! in_dom ivb1 IB.xxG{1} /\ mem ivb1 work{1}
             ==> ={ivb, glob IB}) 
        ( ivb1 = ivb{1} /\ xx1 = IB.xxG{1} /\
           work{2} = IB.gwork{2} /\ work{1} = IB.gwork{1} /\ ={ivb, IB.gwork, IB.xxG} /\
           ! in_dom ivb{1} IB.xxG{1} /\ mem ivb{1} work{1} ==>
           ={IB.gwork, IB.xxG} /\
             in_dom ivb{1} IB.xxG{1} /\ result{2} = oget IB.xxG{2}.[ivb{2}]) => //.
      progress. exists IB.gwork{2}, IB.xx{2}, IB.xxG{2}, ivb{2}, IB.gwork{2} => //.
      symmetry.
      conseq (_:  ={glob IB, work, ivb} ==> _) => //.
      eager while (H:tok = $Dword.dword; ~ tok = $Dword.dword; :
                ={ivb} ==> ={tok})=> //;[by sim | | by sim].
      inline IB.sampleG. swap{2} 7 -6; wp.
      case ((ivb = pick work){1}).
        by rnd{1};wp;rnd;wp;rnd{2};skip;progress;smt.
      auto;progress;smt.
    while (={work} /\ (mem ivb work = !in_dom ivb IB.xxG){1} /\
           IB.xxG{2}.[ivb{1}] = Some tok{1} /\
           IB.xxG{2} = (if !in_dom ivb IB.xxG then IB.xxG.[ivb <- tok] else IB.xxG){1}).
      inline IB.sampleG;auto;progress;smt. 
    auto;progress;smt. 
  qed.

  local lemma eager_aux (A0 <:AIB {IB}) : 
      equiv [MIB(A0).mainL ~ MIB(A0).mainE : ={glob A0, x} ==> ={res, glob A0}].
   proof.
     transitivity MIB(A0).mainL' (={glob A0,x} ==> ={res, glob A0})
         (={glob A0,x} ==> ={res, glob A0}) => //.
     progress; exists (glob A0){2}, x{2} => //.
     conseq (MIB_mainL_mainL' A0) => //.
     proc.
     seq 2 2 : (={glob A0, glob IB}). 
      inline *;wp; call (_: true); auto.
      symmetry.
      eager (H: IB.resample(); ~ IB.resample();:
                  ={glob IB} ==> ={IB.xx, IB.xxG}): 
            (={glob A0, glob IB}) => //; first by sim.
      eager proc H (={glob IB})=> //.
        by apply eager_query1.
        by sim.
        by apply eager_queryG.
        by sim.
    qed.

   (*******************************)

   op all_tuple_int_aux (js:int * (int*bool*bool) set) = 
      let (j,s) = js in
      let s = add (j, true, true) s in
      let s = add (j, true, false) s in
      let s = add (j, false, true) s in
      (j+1, add (j, false, false) s).
      
   op all_tuple_int (n:int) = 
     snd (Int.fold all_tuple_int_aux (0, FSet.empty) n).

   lemma all_tuple_int_spec (n:int) :
      forall i v b, 0 <= i < n => mem (i,v,b) (all_tuple_int n).
   proof.
     intros i v b Hi.
     rewrite /all_tuple_int.
     cut Hn : 0 <= n by smt.
     cut H : fst (fold all_tuple_int_aux (0, FSet.empty) n) = n /\
         (forall i, 
          0 <= i < n => 
          mem (i, v, b) (snd (fold all_tuple_int_aux (0, FSet.empty) n))).
     apply (Int.Induction.induction (fun x,
          fst (fold all_tuple_int_aux (0, FSet.empty) x) = x /\
          forall i, 0 <= i < x => 
          mem (i, v, b) (snd (fold all_tuple_int_aux (0, FSet.empty) x))) _ _ n _) => //=.
       smt.
       move => {Hi} {i} k Hk. 
       rewrite foldS //; case (fold _ _ k) => j s; rewrite /all_tuple_int_aux /fst /snd /= 
         => [-> Hrec2] /= i Hi; rewrite !mem_add.
       case (i < k) => Hix. smt.
       cut ->: (i = k) by smt.
       by case v;case b.          
     by elim H => _ H;apply H.
    qed.

   local module Rf'(O:OIB) = {
      proc init(useVisible:bool): unit = {
        var i, v, trnd;

        R.t = Array.init (C.n + C.q) (fun x, false);
        R.xx = FMap.empty;
        i = 0;
        while (i < C.n + C.q) {
          trnd = $Dbool.dbool;
          v = if useVisible then C.v.[i] else false;
          trnd = if (i >= C.n + C.q - C.m) then v else trnd;
          R.t.[i] = trnd;
          R.xx.[(i, v)] = O.sample(i,v,trnd);
          R.xx.[(i,!v)] = O.sample(i,!v,!trnd);
          i = i + 1;
        }
      } 
    }.

    local equiv Rf_Rf' : Rf.init ~ Rf'(IB).init : 
      ={glob C, useVisible} /\ IB.xx{2} = FMap.empty ==> 
      ={R.t, R.xx}.
    proof.
      proc. 
      while (={i, useVisible, R.t, R.xx, glob C} /\ 
             forall j v b, i{1} <= j => !in_dom (j,v,b) IB.xx{2}).
        swap{1} 5 2. swap{1} 4 1. 
        seq 4 4 : (={i, useVisible, R.t, R.xx, glob C, trnd, v} /\ 
                   forall j v b, i{1} <= j => !in_dom (j,v,b) IB.xx{2});first by auto.
        seq 2 1 : (={i, useVisible, R.t, R.xx, glob C, trnd, v} /\ 
                   !in_dom (i{1},!v{1},!trnd{1}) IB.xx{2} /\
                   forall j v b, i{1} < j => !in_dom (j,v,b) IB.xx{2});
          inline *;auto;progress. 
          rewrite H //=. smt. smt. smt. rewrite H /=. 
        rewrite get_set_eq;smt. smt.
     auto;progress. smt.
   qed.

    local module Rf2' (O:OIB) = {
      proc init(useVisible:bool): unit = {
        var i, v, trnd;

        R.t = Array.init (C.n + C.q) (fun x, false);
        R.xx = FMap.empty;
        i = 0;
        while (i < C.n + C.q) {
          trnd = $Dbool.dbool;
          v = if useVisible then C.v.[i] else false;
          R.t.[i] = if i >= C.n + C.q - C.m then v else trnd;
          i = i + 1;
        }
        i = 0;
        while (i < C.n + C.q) {
          v = if useVisible then C.v.[i] else false;
          trnd = R.t.[i];
          R.xx.[(i, v)] = O.sample(i,v,trnd);
          R.xx.[(i,!v)] = O.sample(i,!v,!trnd);
          i = i + 1;
        }
      } 
    }.

    local equiv Rf2_Rf2' : Rf2.init ~ Rf2'(IB).init : 
       ={glob C, useVisible} /\ IB.xx{2} = FMap.empty ==> 
       ={R.t, R.xx}.
    proof.
      proc; inline *.
      while (={R.t, R.xx, glob C} /\ i0{1} = i{2} /\ useVisible1{1} = useVisible{2} /\
             forall j v b, i0{1} <= j => !in_dom (j,v,b) IB.xx{2}).
        auto; progress.
          rewrite !H //= in_dom_set.
          cut -> /=: forall (v b:bool), ((i{2},!v,!b) = (i{2},v,b)) = false by smt.
          by rewrite !H //= !FMap.get_set.
          rewrite !H //= in_dom_set.
          cut -> /=: forall (v b:bool), ((i{2},!v,!b) = (i{2},v,b)) = false by smt.
          by rewrite !H //= !in_dom_set; smt.
      wp; while (={i, R.t, glob C} /\ useVisible0{1} = useVisible{2}).
        by auto.
      by auto; progress; rewrite in_dom_empty.
    qed.

(*
    (** The following now becomes much easier **)
    local equiv eqRf : Rf.init ~ Rf2.init : ={useVisible} ==> ={glob R}. 
    proof.
      proc.
      inline *.
      swap{2} 5 -3; sp 0 2.
      swap{2} 4 -2.
       (* loop fusion *).
    qed.
*)
    (* Contains the flag variables used by GInit, their value depends
       of the type of the garbling : Fake, Real, Hybrid *)
    local module F = {
      var flag_ft : bool
      var flag_tf : bool
      var flag_tt : bool
      var flag_sp : bool
    }.

    (* Type of a module that fill the F modules *)
    module type Flag_t(G:G_t) = { proc gen() : unit{ G.getInfo } }.

    (*handle high level part of garbling *)
    local module GInit(Flag:Flag_t) = {
      module Flag = Flag(Gf)

      proc init() : unit = {
        var tok : word;

        G.yy = Array.init (C.n + C.q) (fun x, (W.zeros));
        G.pp = FMap.empty;
        G.randG = FMap.empty;
        G.a = 0;
        G.b = 0;

        G.g = C.n;
        while (G.g < C.n + C.q)
        {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          Flag.gen();
          Gf.garb(oget R.xx.[(G.g, C.v.[G.g])], false, false);
          tok = Gf.garbD(F.flag_ft, false,  true);
          tok = Gf.garbD(F.flag_tf,  true, false);
          G.yy.[G.g] = Gf.garbD(F.flag_tt,  true,  true);
          if (F.flag_sp) Gf.garb(G.yy.[G.g], true, false);
          G.g = G.g + 1;
        }
      }
    }.

    local lemma GinitL (F <: Flag_t{G,C}): islossless F(Gf).gen => islossless GInit(F).init.
    proof.
    intros GgenL; proc; while true (C.n + C.q - G.g); last by wp; skip; smt.
    intros z; seq 7: (C.n + C.q - G.g = z) 1%r 1%r 0%r _=> //.
      by do 3!call GgarbDL; call GgarbL; call GgenL; wp.
      by if; wp; [call GgarbL | ]; skip; smt.
      by hoare; do !(call (_: true ==> true)=> //); wp.
    qed.

    op todo (a b g bound:int) = 0 <= a /\ b < g /\ b < bound + 1 /\ a < b.

    (* Local definition of PrvInd ... BUT WHY? *)
    module type Scheme_t = {
      proc enc(p:fun_t*input_t) : funG_t*inputG_t*outputK_t { }
    }.

    module Game(S:Scheme_t, ADV:EncSecurity.Adv_IND_t) = {
      proc main() : bool = {
        var query,p,c,b,adv,ret;

        query = ADV.gen_query();
        if (EncSecurity.queryValid_IND query)
        {
          b = ${0,1};
          p = if b then snd query else fst query;
          c = S.enc(p);
          adv = ADV.get_challenge(c);
          ret = (b = adv);
        }
        else
          ret = ${0,1};
        return ret;
      }
    }.

    (* This is Gb from figure 7 *)
    local module Garble1 : Scheme_t = {
      proc enc(p:fun_t*input_t) : funG_t*inputG_t*outputK_t = {
        Cf.init(p);
        Rf.init(false);
        return
          (funG C.f R.xx, encode (inputK C.f R.xx) C.x, outputK C.f R.xx);
      }
    }.

    local lemma RedComon_garble_ll (A <: EncSecurity.Adv_IND_t):
      islossless RedComon(A).garble.
    proof.
      proc.
      while true (C.n + C.q - G.g); last by auto; smt.
      move=> z; wp; conseq* (_: _ ==> true); first smt.
      seq 3: true 1%r 1%r 0%r _ => //=.
        by call GgarbL; auto.
        if.
          seq 3: true 1%r 1%r 0%r _ => //=.
            by do 3!call GgarbDL.
            by if=> //=; call GgarbL.
          if.
            by call GgarbDL.
            seq 1: true 1%r 1%r 0%r _ => //=.
              by call GgarbDL.
              by if=> //=; call GgarbD2L.
    qed.

    local lemma RedComon_sampleTokens_ll (A <: EncSecurity.Adv_IND_t):
      islossless RedComon(A).sampleTokens.
    proof.
      proc.
      while true (C.n + C.q - i).
        move=> z; wp.
        conseq* (_ : _ ==> true). smt.
        if.
          case ( (! in_dom (i, ! C.v.[i]) R.xx /\ ! i = CV.l)).
            rcondt 3; auto;smt.
            rcondf 3; auto;smt.
          by if; auto; smt.
      by auto; smt.
    qed.

    local lemma RedComon_doAnswer_ll (A <: EncSecurity.Adv_IND_t):
      islossless RedComon(A).doAnswer.
    proof. by proc; wp. qed.

    local lemma RedComon_coreNAda_post_ll (A <: EncSecurity.Adv_IND_t):
      islossless RedComon(A).coreNAda_post.
    proof.
      proc; while true (C.n + C.q - G.g); last by auto; smt.
      move=> z; sp.
      case (G.a = CV.l).
        rcondt 1=> //.
        case (G.b = CV.l).
          rcondt 5; first by inline *; auto.
          do !(wp; call (RedComon_doAnswer_ll A)).
          by skip; smt.
          rcondf 5; first by inline *; auto.
          do !(wp; call (RedComon_doAnswer_ll A)).
          by skip; smt.
        rcondf 1=> //.
        case (G.b = CV.l).
          rcondt 1=> //.
          do !(wp; call (RedComon_doAnswer_ll A)).
          by skip; smt.
          by rcondf 1=> //; wp; skip; smt.
    qed.

    local lemma RedComon_genQuery_ll (A <: EncSecurity.Adv_IND_t):
      islossless RedComon(A).genQuery.
    proof. by proc; auto; smt. qed.

    local lemma RedComon_coreNAda_pre_ll (A <: EncSecurity.Adv_IND_t):
      islossless RedComon(A).coreNAda_pre.
    proof.
      proc; while true (C.n + C.q - G.g); last by auto; smt.
      move=> z; sp.
      if.
        case (G.b = CV.l).
          rcondt 4; first by inline *; auto.
          do !(wp; call (RedComon_genQuery_ll A)).
          by skip; smt.
        rcondf 4; first by inline *; auto.
        do !(wp; call (RedComon_genQuery_ll A)).
        by skip; smt.
      if.
        do !(wp; call (RedComon_genQuery_ll A)).
        by skip; smt.
      by wp; skip; smt.
    qed.

    (* This lemma makes the correspondance between B&R prvind def used in this file and 
    the one presented in EncSec.ec *)
    local lemma eqDefs:
      equiv[Game(Garble1,A).main ~ EncSecurity.Game_IND(Rand,A).main:
        ={glob A} ==> ={res}].
    proof.
      proc; inline Game(Garble1,A).main.
      swap{2} 1 1; seq 1 1: (={query, glob A}); first by call (_: true).
      case (EncSecurity.queryValid_IND query{1}); last by rcondf{1} 1=> //; rcondf{2} 2; auto; smt.
      rcondt{1} 1=> //; rcondt{2} 2; first by auto.
      inline *; wp; call (_: true).
      wp; while (useVisible{1}= false /\ i0{1} = i{2} /\ ={glob A, C.n, C.m, C.q, R.xx});
        first by auto.
      wp; while{1} (true) ((C.n + C.q - i){1});
        first by auto; smt.
timeout 10.
      by auto; smt.
timeout 3.
    qed.

    (* This is the garble from Hy_l, figure 10 *)
    local module Garble2(F:Flag_t) : Scheme_t = {
      module GI = GInit(F)

      proc enc(p:fun_t*input_t) : funG_t*inputG_t*outputK_t = {
        var tok1, tok2 : word;

        Cf.init(p);
        Rf.init(true);
        GI.init();
        return (
          ((C.n, C.m, C.q, C.aa, C.bb), G.pp),
          encode (inputK C.f R.xx) C.x,
          tt);
      }
    }.

    local module FR(G:G_t) : Flag_t(G) = {
      proc gen() : unit = {
        F.flag_ft = false;
        F.flag_tf = false;
        F.flag_tt = false;
        F.flag_sp = false;
      }
    }.

    local lemma FReal_genL : islossless FR(Gf).gen.
    proof. by proc; wp. qed.

    local module FF(G:G_t) : Flag_t(G) = {
      proc gen() : unit = {
        F.flag_ft = true;
        F.flag_tf = true;
        F.flag_tt = true;
        F.flag_sp = false;
      }
    }.

    local lemma FFake_genL : islossless FF(Gf).gen.
    proof. by proc; wp. qed.

    local module FH(G:G_t) : Flag_t(G) = {
      proc gen() : unit = {
        var a, b : int;
        var val : bool;
        (a, b, val) = G.getInfo();

        F.flag_ft = a <= CV.l;
        F.flag_tf = b <= CV.l;
        F.flag_tt = a <= CV.l;
        F.flag_sp = a <= CV.l < b /\ val;
      }
    }.

    local lemma FHybrid_genL : islossless FH(Gf).gen.
    proof. by proc; wp; call GgetInfoL. qed.

    local equiv equiv_FH_FR: FH(Gf).gen ~ FR(Gf).gen:
      ={glob C, glob R, glob G} /\
      CV.l{1} = -1 /\
      (todo G.a G.b G.g bound){1} ==>
      ={glob C, glob R, glob G, glob F}.
    proof. by proc; inline Gf.getInfo; auto; smt. qed.

    local equiv equiv_FH_FF: FH(Gf).gen ~ FF(Gf).gen:
      ={glob C, glob R, glob G} /\
      (CV.l = bound /\
      todo G.a G.b G.g bound){1} ==>
      ={glob C, glob R, glob G, glob F}.
    proof. by proc; inline Gf.getInfo; auto; smt. qed.

    lemma get_initGates2 (base:int) size (f:int->bool->bool->'a) g a b :
      0 <= size =>
      (initGates base size f).[(g, a, b)] = if base <= g < base + size then Some (f g a b) else None
    by smt.

    local equiv equivGReal: Garble2(FH).enc ~ Garble2(FR).enc:
      ={p} /\ CV.l{1} = -1 /\ validInputsP p{1} ==> ={res}.
    proof.
      proc.
      inline Garble2(FH).GI.init Garble2(FR).GI.init.
      while (={p, glob C, glob G, glob R} /\
             C.n{1} <= G.g{1} <= C.n{1} + C.q{1} /\
             CV.l{1} = -1 /\
             length C.v{1} = C.n{1} + C.q{1} /\
             C.f{1} = ((C.n,C.m,C.q,C.aa,C.bb),C.gg){1} /\
             validInputsP (C.f,C.x){1} /\
             validRand C.f{1} R.xx{1} /\
             length R.t{1} = C.n{1} + C.q{1} /\
             (forall i, C.n{1} <= i < C.n{1} + C.q{1} =>
                C.v{1}.[i] = oget C.gg{1}.[(i,C.v{1}.[C.aa{1}.[i]],C.v{1}.[C.bb{1}.[i]])])); first last.
        wp; call RinitE; call CinitE; skip.
        by rewrite {3}/validInputsP /validCircuitP /fst /fst /snd; progress; expect 4 smt.
      seq 3 3: (={p, glob C, glob G, glob R, glob F} /\
                C.n{1} <= G.g{1} < C.n{1} + C.q{1} /\
                G.a{1} = C.aa.[G.g]{1} /\
                G.b{1} = C.bb.[G.g]{1} /\
                CV.l{1} = -1 /\
                length C.v{1} = C.n{1} + C.q{1} /\
                C.f{1} = ((C.n,C.m,C.q,C.aa,C.bb),C.gg){1} /\
                validInputsP (C.f,C.x){1} /\
                validRand C.f{1} R.xx{1} /\
                length R.t{1} = C.n{1} + C.q{1} /\
                (forall i, C.n{1} <= i < C.n{1} + C.q{1} =>
                   C.v{1}.[i] = oget C.gg{1}.[(i,C.v{1}.[C.aa{1}.[i]],C.v{1}.[C.bb{1}.[i]])])).
        by inline *; auto; rewrite /validInputsP /validCircuitP /fst /fst /snd; progress; expect 3 smt.
      case (F.flag_sp{1}).
        rcondt{1} 5; first by auto; do !call (_: true).
        rcondt{2} 5; first by auto; do !call (_: true).
        by wp; call GgarbE; do 3!(call GgarbDE); call GgarbE; auto; progress; expect 2 smt.
        rcondf{1} 5; first by auto; do !call (_: true).
        rcondf{2} 5; first by auto; do !call (_: true).
        by wp; do 3!(call GgarbDE); call GgarbE; auto; progress; expect 2 smt.
    qed.

    local equiv equivGarble1: Garble1.enc ~ Garble2(FR).enc:
      ={p} /\ validInputsP p{1} ==> ={res}.
    proof.
      proc; symmetry; inline Garble2(FR).GI.init.
      while{1} (t_xor (C.n{1} + C.q{1}) R.t{1} R.t{2} C.v{1} /\
                0 <= C.q{2} /\
                C.n{2} <= G.g{1} /\
                C.f{2} = ((C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}), C.gg{2}) /\ validCircuitP bound C.f{2} /\
                let (topo, gg) = funG C.f{2} R.xx{2} in
                  ={glob C} /\
                  (forall i v, 0 <= i < C.n + C.q => getlsb (oget R.xx.[(i, v)]) = v ^^ R.t.[i]){2} /\
                  (forall g u, 0 <= g < (C.n + C.q){1} => R.xx.[(g, u)]{1} = R.xx.[(g, u)]{2}) /\
                  (forall i, C.n <= i < C.n + C.q =>
                     C.v{2}.[i] = oget C.gg.[(i, false ^^ C.v{2}.[C.aa{2}.[i]], false ^^ C.v{2}.[C.bb.[i]])]){2} /\
                  topo = (C.n{1}, C.m{1}, C.q{1}, C.aa{1}, C.bb{1}) /\
                  G.g{1} <= C.n{1} + C.q{1} /\
                  (forall i a b, !(G.g{1} <= i < C.n{1} + C.q{1}) => gg.[(i, a, b)] = G.pp{1}.[(i, a, b)]))
               ((C.n + C.q - G.g){1}).
      intros &m z.
      inline Gf.garbD.
      inline Gf.garbD1.
      inline Gf.garbD2.
      inline Gf.garb.
      inline GInit(FR).Flag.gen.
      wp.
      swap 17 -16.
      swap 35 -34.
      swap 53 -52.
      wp.
      simplify.
      do 3 ! rnd.
      skip.
      simplify funG C2.funG fst snd t_xor.
      (progress;first 2 smt);last 4 smt.
      case (G.g{hr} = i)=> hi.
        rewrite hi !FMap.get_set get_initGates2; first smt.
        cut -> /=: C.n{m} <= i < C.n{m} + C.q{m} by smt.
        rewrite !xor_true !xor_false /=.
        cut hneq : forall (x:bool), ((! x) = x) = false by smt.
        cut lem : forall u v, Some (enc R.xx{m} ((C.n{m}, C.m{m}, C.q{m}, C.aa{m}, C.bb{m}), C.gg{m}) i
          (u ^^ R.t{hr}.[C.aa{m}.[i]]) (v ^^ R.t{hr}.[C.bb{m}.[i]])) =
            Some (E (tweak i (R.t{hr}.[C.aa{m}.[i]]^^u) (R.t{hr}.[C.bb{m}.[i]]^^v))
              (oget R.xx{hr}.[(C.aa{m}.[i], u ^^ C.v{m}.[C.aa{m}.[i]])]) (oget R.xx{hr}.[(C.bb{m}.[i], v ^^ C.v{m}.[C.bb{m}.[i]])])
              (oget R.xx{hr}.[(i, (oget C.gg{m}.[(i, u^^C.v{m}.[C.aa{m}.[i]], v^^C.v{m}.[C.bb{m}.[i]])]))])).
          intros u v.
          simplify enc fst snd.
          rewrite !H3;first 4 by elim H2;smt.
          rewrite !H ;first 2 by elim H2;smt.
          rewrite !H4 ;first 3 by elim H2;smt.
          rewrite !(Bool.xorC false) !xor_false.
          cut xor_simpl : forall x y z, x ^^ (y ^^ z) ^^ y = x ^^ z
            by (intros x y z;case x;case y;case z;do rewrite /= ?(xor_true, xor_false) //).
          rewrite !xor_simpl.
          by do 3 !congr; rewrite Bool.xorC; [rewrite (Bool.xorC u) | rewrite (Bool.xorC v)]; rewrite Bool.xorA.
          (case (a = R.t{hr}.[C.aa{m}.[i]])=> ha;[rewrite ? ha|cut -> : a = !R.t{hr}.[C.aa{m}.[i]] by smt]);
          (case (b = R.t{hr}.[C.bb{m}.[i]])=> hb;[rewrite hb|cut -> : b = !R.t{hr}.[C.bb{m}.[i]] by smt]);rewrite ?hneq /=.
          by cut := lem false false;rewrite (H5 i) ?(fst_pair, snd_pair, (Bool.xorC false), xor_false, (Bool.xorC true), xor_true) //;smt.
          by cut := lem false true;rewrite /enc !(fst_pair, snd_pair, (Bool.xorC false), xor_false, (Bool.xorC true), xor_true) //.
          by cut := lem true false;rewrite /enc !(fst_pair, snd_pair, (Bool.xorC false), xor_false, (Bool.xorC true), xor_true) //.
          by cut := lem true true;rewrite /enc !(fst_pair, snd_pair, (Bool.xorC false), xor_false, (Bool.xorC true), xor_true) //.
      cut h : forall aa bb, ((G.g{hr}, R.t{hr}.[C.aa{m}.[G.g{hr}]] ^^ aa, R.t{hr}.[C.bb{m}.[G.g{hr}]] ^^ bb) = (i, a, b)) = false by smt.
      by rewrite !FMap.get_set !h /=;apply H7;smt.
    wp.
    call RgenClassicVisibleE.
    call CinitE.
    skip.
    simplify funG C2.funG fst.
    intros &1 &2 [? valid].
    subst p{1}.
    generalize valid.
    elim (p{2})=> x1 x2.
    elim x1=> topo gg.
    elim topo=> n m q aa bb.
    simplify validInputsP validCircuitP fst snd.
    progress.
      smt.
      smt.
      by rewrite ?(Bool.xorC false) ?xor_false H30;smt.
      smt.
      smt.
      smt.
      apply map_ext=> y.
      elim y=> i a b.
      by apply H53; smt.
    qed.

    local equiv equivGFake: Garble2(FH).enc ~ Garble2(FF).enc:
      ={p} /\ CV.l{1} = bound - 1 /\ validInputsP p{1} ==> ={res}.
    proof.
      proc.
      inline Garble2(FH).GI.init Garble2(FF).GI.init.
      while (={p, glob C, glob G, glob R} /\
             C.n{1} <= G.g{1} <= C.n{1} + C.q{1} /\
             CV.l{1} = bound - 1 /\
             length C.v{1} = C.n{1} + C.q{1} /\
             C.f{1} = ((C.n,C.m,C.q,C.aa,C.bb),C.gg){1} /\
             validInputsP (C.f,C.x){1} /\
             validRand C.f{1} R.xx{1} /\
             length R.t{1} = C.n{1} + C.q{1} /\
             (forall i, C.n{1} <= i < C.n{1} + C.q{1} =>
                C.v{1}.[i] = oget C.gg{1}.[(i,C.v{1}.[C.aa{1}.[i]],C.v{1}.[C.bb{1}.[i]])])); first last.
        wp; call RinitE; call CinitE; skip.
        by rewrite {3}/validInputsP /validCircuitP /fst /fst /snd; progress; expect 4 smt.
      seq 3 3: (={p, glob C, glob G, glob R, glob F} /\
                C.n{1} <= G.g{1} < C.n{1} + C.q{1} /\
                G.a{1} = C.aa.[G.g]{1} /\
                G.b{1} = C.bb.[G.g]{1} /\
                CV.l{1} = bound - 1 /\
                length C.v{1} = C.n{1} + C.q{1} /\
                C.f{1} = ((C.n,C.m,C.q,C.aa,C.bb),C.gg){1} /\
                validInputsP (C.f,C.x){1} /\
                validRand C.f{1} R.xx{1} /\
                length R.t{1} = C.n{1} + C.q{1} /\
                (forall i, C.n{1} <= i < C.n{1} + C.q{1} =>
                   C.v{1}.[i] = oget C.gg{1}.[(i,C.v{1}.[C.aa{1}.[i]],C.v{1}.[C.bb{1}.[i]])])).
        by inline *; auto; rewrite /validInputsP /validCircuitP /fst /fst /snd; progress; expect 3 smt.
      case (F.flag_sp{1}).
        rcondt{1} 5; first by auto; do !call (_: true).
        rcondt{2} 5; first by auto; do !call (_: true).
        by wp; call GgarbE; do 3!(call GgarbDE); call GgarbE; auto; progress; expect 2 smt.
        rcondf{1} 5; first by auto; do !call (_: true).
        rcondf{2} 5; first by auto; do !call (_: true).
        by wp; do 3!(call GgarbDE); call GgarbE; auto; progress; expect 2 smt.
    qed.

    local lemma equivPrvInd (S1 <: Scheme_t {A}) (S2 <: Scheme_t {A}) gCV1:
      equiv[S1.enc ~ S2.enc: ={p} /\ (glob CV){1} = gCV1 /\ validInputsP p{1} ==> ={res}] =>
      equiv[Game(S1,A).main ~ Game(S2,A).main: ={glob A} /\ (glob CV){1} = gCV1 ==> ={res}].
    proof.
      intros=> SencE; proc.
      seq 1 1: (={glob A, query} /\ (glob CV){1} = gCV1);
        first by call (_: true).
      if=> //; last by rnd.
      wp; call (_: true).
      call SencE; wp; rnd; skip.
      progress=> //; elim H;
      simplify EncSecurity.Encryption.valid_plain EncSecurity.Encryption.leak validInputs C2.validInputs EncSecurity.Encryption.leak; smt.
    qed.

    local module Hybrid(A:EncSecurity.Adv_IND_t) = {
      module PrvInd = Game(Garble2(FH), A)

      proc work(x:int) : bool = {
        var b : bool;
        CV.l = x;
        b = PrvInd.main();
        return b;
      }
    }.

      module Gf' (O:OIB) = {
        proc garb = Gf.garb
        proc getInfo = Gf.getInfo
        proc garbD1(rand:bool, alpha:bool, bet:bool): unit = {
          var tok:word;

          if (rand) G.randG.[(G.g,alpha,bet)] = O.sampleG(G.g, alpha, bet);
        }

      proc garbD2(rand:bool, alpha:bool, bet:bool) : word = {
        var yy:word;

        yy = if rand
             then oget G.randG.[(G.g,alpha,bet)]
             else oget R.xx.[(G.g, (oget C.gg.[(G.g, C.v.[G.a]^^alpha,C.v.[G.b]^^bet)]))];
        garb(yy, alpha, bet);
        return yy;
      }

      proc garbD(rand:bool, alpha:bool, bet:bool): word = {
        var yy : word;

        garbD1(rand, alpha, bet);
        yy = garbD2(rand, alpha, bet);
        return yy;
      }
      }.

    local module Hybrid2 (O:OIB) = {
      var query :  (fun_t * input_t) * (fun_t*input_t)
      var b0 : bool

      module Flag = FH(Gf)
      module Gf = Gf'(O)

      proc initG() : unit = {
        var tok : word;

        G.yy = Array.init (C.n + C.q) (fun x, (W.zeros));
        G.pp = FMap.empty;
        G.randG = FMap.empty;
        G.a = 0;
        G.b = 0;

        G.g = C.n;
        while (G.g < C.n + C.q)
        {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          Flag.gen();
          Gf.garb(oget R.xx.[(G.g, C.v.[G.g])], false, false);
          tok = Gf.garbD(F.flag_ft, false,  true);
          tok = Gf.garbD(F.flag_tf,  true, false);
          G.yy.[G.g] = Gf.garbD(F.flag_tt,  true,  true);
          if (F.flag_sp) Gf.garb(G.yy.[G.g], true, false);
          G.g = G.g + 1;
        }
      }

      proc init (x:int) : (int * bool * bool) set = {
        var p;
        CV.l = x;
        query = A.gen_query();
        b0     = $Dbool.dbool;
        p     = b0 ? snd query : fst query;
        Cf.init(p);
        return all_tuple_int maxGates;
      }

      module Rf = Rf'(O)

      proc main () : bool = {
        var adv;  
        var b; 
        var c;
        if ((EncSecurity.queryValid_IND query)%EncSecurity) {
          Rf.init(true);                                      
          initG();
          c = (((C.n, C.m, C.q, C.aa, C.bb), G.pp),            
                  encode (inputK C.f R.xx) C.x, tt);
          adv = A.get_challenge(c);                           
          b = b0 = adv;
        } else {                                             
          b = $Dbool.dbool;
        }                                                    
        return b;
      }
    }.

    local equiv Hybrid2_Hybrid : MIB(Hybrid2).mainE ~ Hybrid(A).work : ={glob Hybrid2,x} ==> ={res}.
    proof.
      transitivity MIB(Hybrid2).mainL (={glob Hybrid2, x} ==> ={res})
        (={glob Hybrid2, x} ==> ={res}) => //.
      progress. 
       exists (glob A){2}, R.t{2}, R.xx{2}, C.gg{2}, C.aa{2}, C.m{2}, C.x{2},
          C.f{2}, C.n{2}, C.q{2}, C.bb{2}, C.v{2}, G.b{2}, G.randG{2}, G.pp{2}, G.yy{2},
            G.a{2}, G.g{2}, CV.l{2}, F.flag_tt{2}, F.flag_ft{2}, F.flag_tf{2}, F.flag_sp{2},
            Hybrid2.query{2}, Hybrid2.b0{2}, x{2} => //.
      symmetry; conseq (eager_aux Hybrid2) => //.
      proc. inline MIB(Hybrid2).A.init Hybrid(A).PrvInd.main IB.init MIB(Hybrid2).A.main.
      inline{2} Garble2(FH).enc.
      seq 3 2 : (={glob A, CV.l} /\ Hybrid2.query{1} = query{2});first by sim.
      if{2}.
        rcondt{1} 9;first by intros &m;conseq* (_: _ ==> true).
        wp;call (_: true);wp;inline Garble2(FH).GI.init Hybrid2(IB).initG;wp.

        while (={CV.l, glob G, glob C, glob R} /\
                  forall j v b, G.g{1} <= j => !in_dom (j,v,b) IB.xxG{1}).
          seq 7 7 : ( (={CV.l, glob F, glob G, glob C, glob R} /\
                  forall j v b, G.g{1} + 1 <= j => !in_dom (j,v,b) IB.xxG{1})); first last.
             wp;conseq*(_: _ ==> ={glob F,CV.l,glob G,glob C,glob R})=> //;sim.
        cut e_garbD: forall (p:int -> bool -> bool -> bool) v0 b0,
           equiv [Hybrid2(IB).Gf.garbD ~ Gf.garbD : 
                    ={arg, CV.l, glob F, glob G, glob C, glob R} /\
                     v0 = arg{1}.`2 /\ b0 = arg{1}.`3 /\
                     !in_dom (G.g{1}, v0, b0) IB.xxG{1}  /\
                     forall j v b, p j v b => !in_dom (j,v,b) IB.xxG{1} ==>
                    ={res, CV.l, glob F, glob G, glob C, glob R} /\
                    forall j v b, (j,v,b) <> (G.g{1},v0,b0) /\ p j v b => 
                       !in_dom (j,v,b) IB.xxG{1}].
            intros p v0 b0;proc.
              seq 1 1 : ( ={rand,alpha, bet, CV.l, glob F, glob G, glob C, glob R} /\
                    forall j v b, (j,v,b) <> (G.g{1},v0,b0) /\ p j v b => 
                       !in_dom (j,v,b) IB.xxG{1} );first last.
                conseq*(_:_ ==>  ={yy,CV.l, glob F, glob G, glob C, glob R}) => //;sim.
              inline *;sp;wp. if{1};auto;progress [-split].
              rewrite H H1 H2 /= get_set_eq /oget /= /=; smt.
              move:H1;rewrite -neqF => -> /=. smt. 
        seq 4 4:  (={CV.l, glob F, glob G, glob C, glob R} /\
            forall (j : int) (v b1 : bool),
               G.g{1} <= j => ! in_dom (j, v, b1) IB.xxG{1}).
           conseq* (_: _ ==> ={CV.l, glob F, glob G, glob C, glob R}) => //;sim.
        exists* G.g{1}; elim* => Gg.
        pose p0 := fun j (v b:bool), Gg <= j.
        pose p1 := fun j (v b:bool), (j,v,b) <> (Gg,false,true) /\ p0 j v b.
        pose p2 := fun j (v b:bool), (j,v,b) <> (Gg,true,false) /\ p1 j v b.
        call (e_garbD p2 true true).
        call (e_garbD p1 true false). 
        call (e_garbD p0 false true);skip;simplify p0 p1 p2;progress [-split].   
        rewrite H //=. progress. 
        apply H1 => //. apply H4 => //. apply H7 => //. smt.
     swap{1} 8 1.  wp;symmetry;call Rf_Rf';wp.
     conseq* (_ : _ ==> ={glob A, glob C} /\ b0{1} = Hybrid2.b0{2}).
      progress. smt. sim.

      rcondf{1} 9;first by intros &m;conseq* (_: _ ==> true).
      auto; call{1} CinitL;auto;progress;smt.
    qed.

    local module PrvInd(A:EncSecurity.Adv_IND_t) = {
      module PrvInd = Game(Garble1, A)
      proc main() : bool = {
        var b : bool;
        b = PrvInd.main();
        return b;
      }
    }.

    local module GarbleFake : Scheme_t = {
      module GI = GInit(FF)

      proc enc(p:fun_t*input_t) : funG_t*inputG_t*outputK_t = {
        var tok1, tok2 : word;

        Cf.init(p);
        Rf.init(true);
        GI.init();
        return (
          ((C.n, C.m, C.q, C.aa, C.bb), G.pp),
          encode (inputK C.f R.xx) C.x,
          tt);
      }
    }.

    local module Fake(A:EncSecurity.Adv_IND_t) = {
      proc main() : bool = {
        var b, b', ret : bool;
        var c : funG_t*inputG_t*outputK_t;
        var query : (fun_t*input_t)*(fun_t*input_t);

        query = A.gen_query();
        if (EncSecurity.queryValid_IND query)
        {
          b = $ {0,1};
          c = GarbleFake.enc(fst query);
          b' = A.get_challenge(c);
          ret = b = b';
        }
        else
          ret = $ {0,1};
        return ret;
      }
    }.

    local lemma prFakeI &m:
      Pr[Fake(A).main() @ &m:res] = 1%r / 2%r.
    proof.
    byphoare (_: true ==> res)=> //.
    proc; seq 1: true (1%r) (1%r/2%r) 0%r 1%r=> //.
      by call AgenL.
      case (EncSecurity.queryValid_IND query).
        (* VALID *)
        rcondt 1; first by intros.
        inline GarbleFake.enc;
        wp; swap 1 6.
        rnd ((=) b').
        call AgetL.
        wp; call (GinitL FF _);
          first by apply FFake_genL.
        call RgenInitL; call CinitL.
        wp; skip; progress;cut := Dbool.mu_x_def;rewrite /mu_x=> -> //.
        (* INVALID *)
        rcondf 1; first by intros.
        rnd (fun b, b); skip; progress=> //.
        by rewrite Dbool.mu_def.
    qed.

    local lemma prFake &m:
      Pr[Game(Garble2(FF),A).main() @ &m:res] = 1%r / 2%r.
    proof.
      rewrite -(prFakeI &m)=> //.
      byequiv (_: CV.l{1} = CV.l{m} /\ ={glob A}  ==> ={res}) => //; proc.
      inline Game(Garble2(FF),A).main.
      seq 1 1: (={glob A, query} /\ CV.l{1} = CV.l{m}); first by call (_: true).
      wp; if=> //; last by rnd.
      wp; call (_: true).
      inline Garble2(FF).enc GarbleFake.enc; wp.
      inline Garble2(FF).GI.init GarbleFake.GI.init.
      wp; while (={glob G, C.n, C.m, C.q, C.aa, C.bb, R.t} /\
                 C.n{1} <= G.g{1} /\
                 validInputsP (((C.n, C.m, C.q, C.aa, C.bb), C.gg), C.x){1} /\
                 (forall g, 0 <= g < (C.n + C.q){1} =>
                    R.xx.[(g,C.v.[g])]{1} = R.xx.[(g,C.v.[g])]{2}) /\
                 (forall g, 0 <= g < (C.n + C.q){1} =>
                    R.xx.[(g,!C.v.[g])]{1} = R.xx.[(g,!C.v.[g])]{2})).
        wp; seq 7 7: (={glob G, glob F, C.n, C.m, C.q, C.aa, C.bb, R.t} /\
                      C.n{1} <= G.g{1} /\
                      validInputsP (((C.n, C.m, C.q, C.aa, C.bb), C.gg), C.x){1} /\
                      (forall g, 0 <= g < (C.n + C.q){1} =>
                         R.xx.[(g,C.v.[g])]{1} = R.xx.[(g,C.v.[g])]{2}) /\
                      (forall g, 0 <= g < (C.n + C.q){1} =>
                         R.xx.[(g,!C.v.[g])]{1} = R.xx.[(g,!C.v.[g])]{2}) /\
                      0 <= G.a{2} < C.n{2} + C.q{2} /\
                      0 <= G.b{2} < C.n{2} + C.q{2} /\
                      !F.flag_sp{1}).
          do 3!call GgarbDE_rnd; wp; call GgarbE.
          inline GInit(FF).Flag.gen;
          wp; skip; simplify validInputsP validCircuitP fst snd; progress=> //.
            smt.            
            by rewrite !(Bool.xorC false) !xor_false; cut:= H10 C.aa{2}.[G.g{2}] _;smt.
            by rewrite !(Bool.xorC false) !xor_false; cut:= H10 C.bb{2}.[G.g{2}] _;smt.
            by rewrite !(Bool.xorC true) !xor_true; cut:= H11 C.bb{2}.[G.g{2}] _; smt.
            by rewrite !(Bool.xorC true) !xor_true; cut:= H11 C.bb{2}.[G.g{2}] _; smt.
            smt.
            smt.
            smt.
            smt.
        by rcondf{1} 1=> //; rcondf{2} 1=> //; skip; progress; expect 1 smt. (* Wut? SMT needs to see a shrink. *)
    wp; call RinitE_rnd.
    call CinitE_rnd.
    wp; rnd; skip; simplify validInputsP C2.validInputs validInputs validCircuitP fst snd;
    progress=> //.
     by case bL; smt.
     by case bL; smt.
     by case bL; smt.
     by case bL; smt.
     smt.
     smt.
     rewrite /encode /C2.Input.encode;
     apply array_ext; split; first smt.
     intros=> i; rewrite length_init; first smt.
     intros=> i_bnd; rewrite !get_init=> //=; first smt.
     cut ->: x_L.[i] = v_L.[i] by smt.
     cut ->: x_R.[i] = v_R.[i] by smt.
     rewrite /inputK /C2.inputK.
     rewrite !fst_pair.
     pose x := (length x_L, m_R, q_R, aa_R, bb_R).
     cut RW: forall (xx:tokens_t),
               (let (n,m,q,aa,bb) = x in filter (fun x0 y, 0 <= fst x0 < n) xx) =
               (filter (fun x0 y, 0 <= fst x0 < length x_L) xx)
      by done.
     cut ->:= RW xx_L.
     cut ->:= RW xx_R.
     rewrite !get_filter /fst //=.
     case (0 <= i < length x_L)=> //.
     by rewrite H52; smt.
    qed.

    local lemma prH0 &m:
      Pr[Hybrid(A).work((-1)) @ &m :res] = Pr[EncSecurity.Game_IND(Rand,A).main()@ &m:res].
    proof.
      cut <-: Pr[Game(Garble1,A).main()@ &m :res] = Pr[EncSecurity.Game_IND(Rand,A).main()@ &m:res]
        by byequiv eqDefs.
      byequiv (_: ={glob A} /\ x{1} = -1 ==> ={res})=> //.
      proc*; inline Hybrid(A).work.
      wp; call (equivPrvInd (Garble2(FH)) Garble1 (-1) _)=> //; last by wp.
      bypr (res{1}) (res{2})=> //;progress.
      apply (eq_trans _ Pr[Garble2(FR).enc(p{1}) @ &m :a = res] _).
        by byequiv equivGReal.
      by apply eq_sym; byequiv equivGarble1; rewrite - ?H.
    qed.

    local lemma prHB &m:
      2%r * Pr[Hybrid(A).work(bound - 1)@ &m:res] = 1%r.
    proof.
      cut: Pr[Hybrid(A).work(bound - 1)@ &m:res] = 1%r / 2%r; last smt.
      rewrite -(prFake &m)=> //.
      byequiv (_: ={glob A} /\ x{1} = bound - 1 ==> ={res})=> //.
      proc*. inline Hybrid(A).work.
      wp; call (equivPrvInd (Garble2(FH)) (Garble2(FF)) (bound - 1) _)=> //; last by wp.
      bypr (res{1}) (res{2})=> //; progress.
      by byequiv equivGFake; rewrite - ?H.
    qed.

    module type RedAda_t(A:EncSecurity.Adv_IND_t, Dkc:Dkc_t) = {
      proc preInit(): unit {}
      proc work(info:bool): bool {A.gen_query Dkc.encrypt A.get_challenge}
    }.

    local module RedI_Ada(A:EncSecurity.Adv_IND_t, D:Dkc_t) : RedAda_t(A D) = {
      module Com = RedComon(A)

      proc preInit = Com.preInit

      proc query(rand:bool, alpha:bool, bet:bool) : unit =
      {
        var q : query;
        var ans : answer;
        q = Com.genQuery(rand, alpha, bet);
        ans = D.encrypt(q);
        Com.doAnswer(rand, alpha, bet, ans);
      }

      proc coreAda(): unit = {
        var trash : word;
        G.a = 0;
        G.b = 0;
        G.yy = Array.empty;
        G.randG = FMap.empty;
        G.pp = FMap.empty;
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          if (G.a = CV.l)
          {
            query(false,  true, false);
            query(false,  true,  true);
          }
          if (G.b = CV.l)
          {
            query(false, false,  true);
            query(true,  true,  true);
          }
          G.g = G.g + 1;
        }
      }

      proc work(info:bool): bool = {
        var q : int;
        var qs : query array;
        var ans : answer;
        var answers : answer array;
        var r : bool;
        Com.pre(info);
        coreAda();
        r = Com.post();
        return r;
      }
   }.

  local lemma RedI_Ada_doAnswerL (A <: EncSecurity.Adv_IND_t) (Dkc <: Dkc_t):
    islossless RedI_Ada(A,Dkc).Com.doAnswer.
  proof. by proc; auto. qed.

  local lemma RedI_Ada_genQueryL (A <: EncSecurity.Adv_IND_t) (Dkc <: Dkc_t):
    islossless RedI_Ada(A,Dkc).Com.genQuery.
  proof. by proc; auto; smt. qed.

  local lemma RedI_Ada_queryL (A <: EncSecurity.Adv_IND_t) (Dkc <: Dkc_t):
    islossless Dkc.encrypt =>
    islossless RedI_Ada(A,Dkc).query.
  proof.
    move=> Dkc_ll; proc.
    call (RedI_Ada_doAnswerL A Dkc).
    call Dkc_ll.
    by call (RedI_Ada_genQueryL A Dkc).
  qed.

  local lemma RedI_Ada_coreAdaL (A <: EncSecurity.Adv_IND_t) (Dkc <: Dkc_t {CV,G,C}):
    islossless Dkc.encrypt =>
    islossless RedI_Ada(A,Dkc).coreAda.
  proof.
    move=> Dkc_ll; proc.
    while true (C.n + C.q - G.g).
      move=> z.
      case (C.aa.[G.g] = CV.l).
        rcondt 3; first by auto.
        case (C.bb.[G.g] = CV.l).
          rcondt 5; first by do !call (_: true); auto.
          wp; do !(call (RedI_Ada_queryL A Dkc _)=> //).
          by auto; smt.
          rcondf 5; first by do !call (_: true); auto.
          wp; do !(call (RedI_Ada_queryL A Dkc _)=> //).
          by auto; smt.
        rcondf 3; first by auto.
        case (C.bb.[G.g] = CV.l).
          rcondt 3; first by auto.
          wp; do !(call (RedI_Ada_queryL A Dkc _)=> //).
          by auto; smt.
          rcondf 3; first by auto.
          by auto; smt.
    by auto; smt.
  qed.

  local module Hybrid1 : MeanInt.Worker = {
    module H = Hybrid(A)
    proc work(x:int) : bool = {
      var r : bool;
      r = H.work(x-1);
      return r;
    }
  }.

  local equiv eqW (W<:MeanInt.Worker) : W.work ~ W.work : ={x, glob W} ==> ={res}
    by proc (true).

  local lemma eqH1 &m (l:int) : Pr[Hybrid(A).work(l-1)@ &m : res] = Pr[Hybrid1.work(l)@ &m : res].
    byequiv (_: ={glob Hybrid(A)} /\ x{2} - 1 = x{1} ==> ={res})=> //.
    proc*.
    inline Hybrid1.work.
    by wp;call (eqW (Hybrid(A)));wp.
  qed.

  lemma query_valid : forall query c,
    EncSecurity.queryValid_IND query =>
    validInputsP (if c then snd query else fst query).
  proof.
    move=> query c; rewrite /fst /snd.
    elim query=> p1 p2.
    elim p1=> f1 i1.
    elim p2=> f2 i2.
    elim f1=> tt1 gg1.
    elim tt1=> n1 m1 q1 aa1 bb1.
    elim f2=> tt2 gg2.
    elim tt2=> n2 m2 q2 aa2 bb2 /=.
    rewrite /EncSecurity.queryValid_IND /fst /snd /=.
    rewrite /EncSecurity.Encryption.valid_plain /fst /snd /=.
    rewrite /validInputs /validInputsP /C2.validInputs /fst /snd /=.
    rewrite !valid_wireinput /fst /=.
    by case c.
  qed.

  module type Rf_t = { proc initT(useVisible:bool) : unit }.

  equiv eqinitT : Rf2.initT ~ Rf2.initT : ={glob C, useVisible} ==> ={R.t}.
  proof. by proc; while (={R.t, i, glob C, useVisible}); auto. qed.

  module MyDkc(O:OIB) = {
    proc preInit = Dkc.preInit
    proc initialize = Dkc.initialize
    proc get_challenge = Dkc.get_challenge

    proc get_k(i:int * bool, v:bool): word = {
      var r:word;
      if (!in_dom i DKCm.kpub)
      {
        r = O.sample(fst i, v, snd i);
        DKCm.kpub.[i] = r;
      }
      return oget DKCm.kpub.[i];
    }

    proc get_k2(i:int * bool): word = {
      var r:word;
      if (!in_dom i DKCm.kpub)
      {
        r = O.sampleG(fst i, true, true);
        DKCm.kpub.[i] = r;
      }
      return oget DKCm.kpub.[i];
    }

    proc get_r(i:int * bool, a:bool, b:bool): word = {
      var r:word;

      if (!in_dom i DKCm.r)
      {
        r = O.sampleG(fst i, a, b);
        DKCm.r.[i] = r;
      }
      return oget DKCm.r.[i];
    }

    proc encrypt(q:query): answer = {
      var ka, kb, ki, kj, rj, msg, t:word;
      var i, j:int * bool;
      var pos:bool;
      var out:answer = bad;

      (i,j,pos,t) = q;
      if (!(mem t DKCm.used) /\ fst i < fst j)
      {
        DKCm.used = add t DKCm.used;
        ki = get_k(i, false);
        kj = get_k(j, false);
        rj = get_r(j, false, false);

        (ka,kb) = if pos then (DKCm.ksec,ki) else (ki,DKCm.ksec);
        msg = if (DKCm.b) then kj else rj;
        out = (ki,kj,E t ka kb msg);
      }
      return out;
    }

    proc encrypt2(q:query, v1:bool, v2:bool): answer = {
      var ka, kb, ki, kj, rj, msg, t:word;
      var i, j:int * bool;
      var pos:bool;
      var out:answer = bad;

      (i,j,pos,t) = q;
      if (!(mem t DKCm.used) /\ fst i < fst j)
      {
        DKCm.used = add t DKCm.used;
        ki = get_k(i, v1);
        kj = get_k(j, v2);
        rj = get_r(j, false, false);

        (ka,kb) = if pos then (DKCm.ksec,ki) else (ki,DKCm.ksec);
        msg = if (DKCm.b) then kj else rj;
        out = (ki,kj,E t ka kb msg);
      }
      return out;
    }
  }.

  local module RedI_AdaRO(O:OIB, Dkc:Dkc_t) = {
    module Red = RedI_Ada(A, Dkc)
    module DkcO = MyDkc(O)
    proc preInit = Red.preInit
    module Gf = Gf'(O)

      proc query(rand:bool, alpha:bool, bet:bool) : unit =
      {
        var q : query;
        var ans : answer;
        var gamma : bool;
      var ka, kb, ki, kj, rj, msg, t:word;
      var i, j:int * bool;
      var pos:bool;
      var out:answer = bad;
        q = Red.Com.genQuery(rand, alpha, bet);
        gamma = C.v.[G.g] ^^ (oget C.gg.[(G.g, C.v.[G.a]^^alpha, C.v.[G.b]^^bet)]);

      (i,j,pos,t) = q;
        DKCm.used = add t DKCm.used;
        ki = DkcO.get_k(i, (if (G.a = CV.l) then (C.v.[G.b]^^bet) else (C.v.[G.a]^^alpha)));
        if (rand)
          kj = DkcO.get_k(j, C.v.[G.g]^^gamma);
        else
          kj = DkcO.get_k2(j);
        rj = DkcO.get_r(j, alpha, bet);

        (ka,kb) = if pos then (DKCm.ksec,ki) else (ki,DKCm.ksec);
        msg = if (DKCm.b) then kj else rj;
        out = (ki,kj,E t ka kb msg);
        ans = out;
        Red.Com.doAnswer(rand, alpha, bet, ans);
      }

      proc coreAda(): unit = {
        var trash : word;
        G.a = 0;
        G.b = 0;
        G.yy = Array.empty;
        G.randG = FMap.empty;
        G.pp = FMap.empty;
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          if (G.a = CV.l)
          {
            query(false,  true, false);
            query(false,  true,  true);
          }
          if (G.b = CV.l)
          {
            query(false, false,  true);
            query(true,  true,  true);
          }
          G.g = G.g + 1;
        }
      }
      
      proc garble() : unit = {
        var tok1, tok2 : word;
        G.g = C.n;
        G.yy = Array.init (C.n + C.q) (fun x, (W.zeros));
        G.pp = FMap.empty;
        G.randG = FMap.empty;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          Gf.garb(oget R.xx.[(G.g, C.v.[G.g])], false, false);
          if (G.a <> CV.l /\ G.b <> CV.l)
          {
            tok2 = Gf.garbD(G.a<=CV.l,  true, false);
            tok2 = Gf.garbD(G.b<=CV.l, false,  true);
            tok1 = Gf.garbD(G.a<=CV.l,  true,  true);
            if ((G.a<=CV.l<=G.b) /\ (C.gg.[(G.g, !C.v.[G.a], false)] = C.gg.[(G.g, !C.v.[G.a], true)]))
              Gf.garb(tok1, true, false);
          }
          else
          {
            if (G.a=CV.l)
            {
              tok2 = Gf.garbD(false, false,  true);
            }
            else
            {
              tok2 = Gf.garbD( true,  true, false);
              if (C.gg.[(G.g, !C.v.[G.a], false)] = C.gg.[(G.g, !C.v.[G.a], true)])
                tok2 = Gf.garbD2(true, true, false);
              
            }
          }
          G.g = G.g + 1;
        }
      }

      proc sampleTokens() : unit = {
        var i : int;
        var tok1, tok2 : word;
        i = 0;
        while (i < C.n + C.q) {
          if (! in_dom (i,  C.v.[i]) R.xx)
            R.xx.[(i,  C.v.[i])] = O.sample(i,  C.v.[i],  R.t.[i]);
          if (! in_dom (i,  !C.v.[i]) R.xx /\ CV.l <> i)
            R.xx.[(i,  !C.v.[i])] = O.sample(i,  !C.v.[i],  !R.t.[i]);
          i = i + 1;
        }
      }
      proc pre(info:bool): unit = {
        var p : fun_t*input_t;
        var t : bool;
        Red.Com.query = A.gen_query();
        Red.Com.c = $Dbool.dbool;
        p = (if Red.Com.c then snd Red.Com.query else fst Red.Com.query);
        R.xx = FMap.empty;
        Cf.init(p);
        t = $Dbool.dbool;
        Rf2.initT(true);
        R.t.[CV.l] = ! t;
        DKCm.ksec = O.sample(CV.l, !C.v.[CV.l], t);
        R.xx.[(CV.l, !C.v.[CV.l])] = DKCm.ksec;
      }
      proc post() : bool = {
        var r : bool;
        sampleTokens();
        garble();
        if ((EncSecurity.queryValid_IND Red.Com.query)%EncSecurity)
        {
          r = A.get_challenge((((C.n, C.m, C.q, C.aa, C.bb), G.pp), encode (inputK C.f R.xx) C.x, tt));
(* init C.n (lambda i, proj R.xx.[(i, C.v.[i])])*) 
          r = (r = Red.Com.c);
        }
        else
          r = $Dbool.dbool;
        return r;
      }
    proc work(info:bool): bool = {
      var q : int;
      var qs : query array;
      var ans : answer;
      var answers : answer array;
      var r : bool;
      pre(info);
      coreAda();
      r = post();
      return r;
    }
  }.


  local module GameAdaRO(O:OIB) : AIB(O) = {

      module M = DKCSecurity.GameAda(MyDkc(O), RedI_AdaRO(O))
      proc init (x:int) : (int * bool * bool) set = {
        return all_tuple_int maxGates;
      }
      proc main = M.work
    }.

local equiv queryRO : RedI_Ada(A, Dkc).query ~ RedI_AdaRO(IB, MyDkc(IB)).query :
  ={rand, alpha, bet, glob DKCm, glob RedI_Ada} /\
  (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
  (0 < G.g /\ G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} 
==>
  ={res, glob DKCm, glob RedI_Ada} /\
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (forall i v,i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2}.
proof.
proc.
inline *.
swap{1} 7 -6; swap{2} 8 -7.
seq  1  1: (={rand,alpha,bet,glob DKCm,glob RedI_Ada} /\
            (forall i v,
              i < C.n + C.q =>
              !(i = CV.l && v) =>
              in_dom (i, C.v.[i] ^^ v, R.t.[i] ^^ v) IB.xx
              = in_dom (i, R.t.[i] ^^ v) DKCm.kpub){2} /\
            (0 < G.g){2} /\
            (G.a < C.n + C.q){2} /\
            (G.b < C.n + C.q){2} /\
            (G.g < C.n + C.q){2} /\
            (G.g <> G.b){2} /\
            (G.g <> G.a){2} /\
            (G.a <> G.b){2} /\
            (G.g <> CV.l){2} /\
            (forall i v,
              !(i = CV.l && v = !C.v.[CV.l]) =>
              in_dom (i, v, R.t.[i] ^^ C.v.[i] ^^ v) IB.xx
              = in_dom (i, v) R.xx){2});
    (* CHECK why simplify doesn't do this. *)
    first by auto; progress; apply H=> //=; cut ->: (v1 = true) <=> v1 by (split=> ->).  
case (rand{2})=> //=.
  rcondt{1} 7; first by auto.
  rcondt{2} 8; first by auto.
  rcondt{2} 17.
    by progress; (seq 7: (rand); last sp; if); auto.
  rcondt{1} 22.
    by progress; (seq 7: (rand); last sp; if); auto.
  rcondt{2} 38.
    progress; (seq 7: (rand); first by auto).
    seq  8: (rand); first by sp; if; auto.
    seq  4: (rand); first by sp; if; auto.
    seq  5: (rand); first by sp; if; auto.
    by seq 13: (rand1)=> //=; sp 12; if; auto.
  sp 11 11.
  case ((!mem t DKCm.used){1}).
    rcondt{1} 1.
      progress; skip; move=> &hr [[]]; progress.
      elim H; progress.
        by elim H ; progress; rewrite /fst //=; expect 2 smt.
        by elim H1; progress; rewrite /fst //=; expect 2 smt.
    admit.
    rcondf{1} 1.
      progress; skip; move=> &hr [[]]; progress.
      elim H; progress.
        by elim H ; progress; rewrite /fst //=; expect 2 smt.
        by elim H1; progress; rewrite /fst //=; expect 2 smt.
    admit.
  rcondf{1} 7; first by auto.
  rcondf{2} 8; first by auto.
  rcondf{2} 17.
    by progress; (seq 7: (!rand); last sp; if); auto.
  rcondf{1} 22.
    by progress; (seq 7: (!rand); last sp; if); auto.
  rcondf{2} 37.
    progress; (seq 7: (!rand); first by auto).
    seq  8: (!rand); first by sp; if; auto.
    seq  4: (!rand); first by sp; if; auto.
    seq  5: (!rand); first by sp; if; auto.
    by seq 12: (!rand1)=> //=; sp 11; if; auto.
  admit.
(*
inline *.
seq 12 15 : (
  ={t, pos0, q0, rand, alpha, bet, gamma, i0, j0, glob DKCm, glob RedI_Ada, out} /\ (gamma = gamma0){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
  (gamma0 = C.v.[G.g] ^^ oget C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]){2} /\
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (forall i v,i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
  (v1 = (if G.a = CV.l then C.v.[G.b] ^^ bet else C.v.[G.a] ^^ alpha)){2} /\
  (v2 = (C.v.[G.g] ^^ gamma)){2} /\
  (i0 = (if G.a = CV.l then (G.b, R.t.[G.b] ^^ bet) else (G.a, R.t.[G.a] ^^ alpha)) /\
  (j0 = (G.g, R.t.[G.g] ^^ gamma0)){2} /\
  rand{2} = false){2}
); first by auto; progress; expect 2 smt.
seq 1 1 : (
  ={t, q0, pos0, rand, out, alpha, bet, gamma, i0, j0, glob DKCm, glob RedI_Ada, out} /\ (gamma = gamma0){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
  (in_dom (if G.a = CV.l then (G.b, R.t.[G.b] ^^ bet) else (G.a, R.t.[G.a] ^^ alpha)) DKCm.kpub /\ in_dom (G.g, R.t.[G.g] ^^ gamma0) DKCm.kpub){2} /\
  (gamma0= C.v.[G.g] ^^ oget C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]){2} /\
    (forall i v,
(i, v) <> (if CV.l = G.a then (G.b, C.v.[G.b] ^^ bet) else (G.a, C.v.[G.a] ^^ alpha)) => (i, v) <> (G.g, C.v.[G.g] ^^ gamma0) =>  (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
  (rand{2} = false){2}
).

(* This two admit need an invariant for the tweak, the idea is that we perform
request with G.g increasing and two different G.g implies two tweaks *)
rcondt{1} 1;first by admit.
rcondt{2} 1;first by admit.

seq 4 4 : (={i1, pos0, t, q0, rand, alpha, bet, gamma, i0, j0, glob DKCm, glob RedI_Ada, out} /\ (gamma = gamma0){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
  (in_dom (if G.a = CV.l then (G.b, R.t.[G.b] ^^ bet) else (G.a, R.t.[G.a] ^^ alpha)) DKCm.kpub){2} /\
  (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
    (forall i v,
(i, v) <> (if CV.l = G.a then (G.b, C.v.[G.b] ^^ bet) else (G.a, C.v.[G.a] ^^ alpha)) => (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (j0 = (G.g, R.t.[G.g] ^^ gamma0)){2} /\
  (v2 = (C.v.[G.g] ^^ gamma)){2} /\
  (gamma0= C.v.[G.g] ^^ oget C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]){2} /\
  rand{2} = false
).

case ((in_dom i0 DKCm.kpub){1}).
rcondf{1} 4;first by auto.
rcondf{2} 4;first by auto.
auto;progress;smt.

rcondt{1} 4;first by auto.
rcondt{2} 4;first by auto.

case ( G.a{2} = CV.l{2} )=> /=.
auto;progress [-split].
split;first done.
auto;progress [-split].
generalize H8.
simplify.
rewrite !fst_pair !snd_pair.
rewrite H7 //;first smt => -> /=.
progress.
smt.
by rewrite H8 /= !FMap.get_set /= oget_some.
by rewrite H8 /= !FMap.get_set /= oget_some in_dom_set.
by rewrite H8 /= !in_dom_set H7;case v3;case (bet{2});case (i4 = G.b{2});rewrite ?(xor_true, xor_false) //=;progress;smt. 
by rewrite H8 /= !in_dom_set H6;smt.

auto;progress [-split].
cut h : (G.a{2} = CV.l{2}) = false by smt.
rewrite !h /=.
generalize H8.
simplify.
rewrite !fst_pair !snd_pair.
rewrite !h /=.
rewrite H7 //;first smt => -> /=.
progress.
smt.
by rewrite H8 /= !FMap.get_set /= oget_some.
by rewrite H8 /= !FMap.get_set /= oget_some in_dom_set.
by rewrite H8 /= !in_dom_set H7;case v3;case (bet{2});case (i4 = G.a{2});rewrite ?(xor_true, xor_false) //=;progress;smt. 
by rewrite H8 /= !in_dom_set H6;smt.

seq 4 4 : (={i2, pos0, j0, ki, t, q0, rand, alpha, bet, gamma, i0, j0, glob DKCm, glob RedI_Ada, out} /\ (gamma = gamma0){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
  (in_dom (if G.a = CV.l then (G.b, R.t.[G.b] ^^ bet) else (G.a, R.t.[G.a] ^^ alpha)) DKCm.kpub /\ in_dom (G.g, R.t.[G.g] ^^ gamma0) DKCm.kpub){2} /\
      (forall i v,
(i, v) <> (if CV.l = G.a then (G.b, C.v.[G.b] ^^ bet) else (G.a, C.v.[G.a] ^^ alpha)) => (i, v) <> (G.g, C.v.[G.g] ^^ gamma0) => (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (gamma0= C.v.[G.g] ^^ oget C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]){2} /\
  (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
  rand{2} = false
).

case ((in_dom j0 DKCm.kpub){1}).
rcondf{1} 4;first by auto.
rcondf{2} 4;first by auto.
auto;progress;smt.

rcondt{1} 4;first by auto.
rcondt{2} 4;first by auto.

auto;progress [-split].
split.
smt.
auto;progress [-split].
generalize H9.
simplify.
rewrite !fst_pair !snd_pair.
rewrite H7 //;first smt => -> /=.
progress.
smt.
by rewrite H9 /= !FMap.get_set /= oget_some.
by rewrite H9 /= !FMap.get_set /= !oget_some !in_dom_set;left.
by rewrite H9 /= !FMap.get_set /= !oget_some !in_dom_set;right.
by rewrite H9 /= !in_dom_set H8;smt.

rewrite H9 /= !FMap.get_set /= !oget_some !in_dom_set H7 //.
progress.
case (in_dom (i4, R.t{2}.[i4] ^^ v3) DKCm.kpub{2})=> //=.
progress.
case (i4 = G.g{2} && v3 = (C.v{2}.[G.g{2}] ^^ oget C.gg{2}.[(G.g{2}, C.v{2}.[G.a{2}], C.v{2}.[G.b{2}])]))=> //.
progress.
case (i4 = G.g{2})=> //=.
smt.

by auto.

(* BEGIN R.xx *)
auto.
progress.
rewrite !FMap.in_dom_set.
pose v3' := C.v{2}.[i4] ^^ v3.
cut -> : R.t{2}.[i4] ^^ C.v{2}.[i4] ^^ v3 = R.t{2}.[i4] ^^ v3' by smt.
cut -> : v3 = C.v{2}.[i4] ^^ v3' by smt.

case ((i4, v3') = (G.g{2}, C.v{2}.[G.g{2}] ^^
  oget C.gg{2}.[(G.g{2}, C.v{2}.[CV.l{2}], C.v{2}.[G.b{2}])]));progress.
smt.

case (((i4, v3') = (G.b, bet)){2});progress.
smt.
generalize H7=> /= H7.
rewrite -H8;progress.
smt.
generalize H11;smt.
generalize H10;smt.
generalize H11.
generalize H10.
cut -> : forall a b c,  a ^^ b ^^ (b ^^ c) = a ^^ c by smt.
cut lem : forall a b c,  (a ^^ b = a ^^ c) = (b = c) by smt.
case (i4 = G.g{2}).
progress; smt.
case (i4 = G.b{2}).
progress.
rewrite lem.
smt.
smt.

rewrite !FMap.in_dom_set.
pose v3' := C.v{2}.[i4] ^^ v3.
cut -> : R.t{2}.[i4] ^^ C.v{2}.[i4] ^^ v3 = R.t{2}.[i4] ^^ v3' by smt.
cut -> : v3 = C.v{2}.[i4] ^^ v3' by smt.

case ((i4, v3') = (G.g{2}, C.v{2}.[G.g{2}] ^^
  oget C.gg{2}.[(G.g{2}, C.v{2}.[G.a{2}], C.v{2}.[G.b{2}])]));progress.
smt.

generalize H10;rewrite -neqF=> H10.
case (((i4, v3') = (G.a, alpha)){2});progress.
generalize H6;rewrite H10 /= => H6.
smt.
rewrite -H8;progress.
smt.
generalize H10;smt.
generalize H9;smt.
generalize H10.
generalize H9.
cut -> : forall a b c,  a ^^ b ^^ (b ^^ c) = a ^^ c by smt.
cut lem : forall a b c,  (a ^^ b = a ^^ c) = (b = c) by smt.
case (i4 = G.g{2}).
progress;smt.
case (i4 = G.a{2}).
progress.
rewrite lem.
smt.
smt.

(* END R.xx *)
*)
qed.

lemma tuple3 :  forall (a a':'a) (b b':'b) (c c':'c), (a, b, c) = (a', b', c') <=> a = a' /\ b = b' /\ c = c' by smt.
lemma tuple2 :  forall (a a':'a) (b b':'b), (a, b) = (a', b') <=> a = a' /\ b = b' by smt.
lemma neq :  forall (a b:bool), (! a) = b <=> !(a = b) by smt.

local equiv sampleTokensRO :  RedComon(A).sampleTokens ~ RedI_AdaRO(IB, MyDkc(IB)).sampleTokens :
  ={glob DKCm, glob C, glob R, RedComon.c, glob A, CV.l} /\
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} ==>
  ={glob DKCm, glob C, glob R, RedComon.c, glob A, CV.l} /\
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2}.
proof.
  proc.
  inline *.
    while (={glob DKCm, glob C, glob R, RedComon.c, i, glob A, CV.l} /\
      (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2}).
    case ((in_dom (i, C.v.[i]) R.xx){1}).
      rcondf{1} 1;first by auto.
      rcondf{2} 1;first by auto.
      case (((in_dom (i, !C.v.[i]) R.xx) \/ (CV.l = i)){1}).
        rcondf{1} 1;first by auto;smt.
        rcondf{2} 1;first by auto;smt.
        by auto.
        
       rcondt{1} 1;first by auto;smt.
       rcondt{2} 1;first by auto;smt.
       auto;progress [-split].
       generalize H3.
       case (i{2} = CV.l{2});first done.
       intros h.
       cut -> : (CV.l{2} = i{2}) = false by smt.
       progress [-split].
       cut h1 : in_dom (i{2},   C.v{2}.[i{2}],   R.t{2}.[i{2}]) IB.xx{2}
         by (generalize H2;(rewrite -H;first by smt);case (C.v{2}.[i{2}]);case (R.t{2}.[i{2}]);rewrite /= !(xor_true, xor_false) /=).
      cut h2 : ! in_dom (i{2}, ! C.v{2}.[i{2}], ! R.t{2}.[i{2}]) IB.xx{2}
        by (generalize H3;(rewrite -H;first smt);case (C.v{2}.[i{2}]);case (R.t{2}.[i{2}]);rewrite /= !(xor_true, xor_false) /=).
timeout 10.
      progress;do rewrite !(h1, h2, FMap.get_set, oget_some, in_dom_set, tuple3, tuple2) //=;smt.
timeout 3.

      rcondt{1} 1;first by auto.
      rcondt{2} 1;first by auto.
      case (((in_dom (i, !C.v.[i]) R.xx) \/ (CV.l = i)){1}).
        rcondf{1} 3;first by auto;smt.
timeout 10.
        rcondf{2} 5;first by auto;smt.
timeout 3.
       auto;progress [-split].
       cut h1 : !in_dom (i{2},   C.v{2}.[i{2}],   R.t{2}.[i{2}]) IB.xx{2}
        by (generalize H2;(rewrite -H;first smt);case (C.v{2}.[i{2}]);case (R.t{2}.[i{2}]);rewrite /= !(xor_true, xor_false) /=).
       generalize H3.
timeout 10.
       case (i{2} = CV.l{2});first progress;do rewrite !(h1, FMap.get_set, oget_some, in_dom_set) //=;smt.
       intros h.
       progress [-split].
      cut h2 : in_dom (i{2}, ! C.v{2}.[i{2}], ! R.t{2}.[i{2}]) IB.xx{2}
        by (generalize H3;(rewrite -H;first smt);case (C.v{2}.[i{2}]);case (R.t{2}.[i{2}]);rewrite /= !(xor_true, xor_false) /=;smt).
      progress;do rewrite !(h1, h2, FMap.get_set, oget_some, in_dom_set, tuple3, tuple2) //=;smt.

        rcondt{1} 3;first by auto;smt.
        rcondt{2} 5;first by auto;smt.
       auto;progress [-split].
       cut h1 : !in_dom (i{2},   C.v{2}.[i{2}],   R.t{2}.[i{2}]) IB.xx{2}
        by (generalize H2;(rewrite -H;first smt);case (C.v{2}.[i{2}]);case (R.t{2}.[i{2}]);rewrite /= !(xor_true, xor_false) /=).
       generalize H3.
       case (i{2} = CV.l{2});first progress;do rewrite !(h1, FMap.get_set, oget_some, in_dom_set) //=;smt.
       intros h.
       progress [-split].
      cut h2 : !in_dom (i{2}, ! C.v{2}.[i{2}], ! R.t{2}.[i{2}]) IB.xx{2}
        by (generalize H3;(rewrite -H;first smt);case (C.v{2}.[i{2}]);case (R.t{2}.[i{2}]);rewrite /= !(xor_true, xor_false) /=;smt).
      progress;do rewrite !(h1, h2, FMap.get_set, oget_some, in_dom_set, tuple3, tuple2, -nor, -nand, -neq) //=;smt.
    by auto.
timeout 3.
qed.

local equiv coreAdaRO : RedI_Ada(A, Dkc).coreAda ~RedI_AdaRO(IB, MyDkc(IB)).coreAda :
={glob DKCm, glob C, glob R, RedComon.query, RedComon.c, glob A, CV.l} /\
(validCircuitP bound ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\ 
      (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2}
==>
={glob DKCm, glob C, glob R, RedComon.query, RedComon.c, glob A, CV.l, glob G} /\
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\ 
      (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2}.
proof.
proc.
    while (
={glob DKCm, glob C, glob R, RedComon.query, RedComon.c, glob A, CV.l, glob G} /\
      (C.n <= G.g){2} /\ (validCircuitP bound ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\ 
      (forall i v, i < C.n + C.q => (i, v) <> (CV.l,true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2}).
   wp.
   seq 2 2 : (
={glob DKCm, glob C, glob R, RedComon.query, RedComon.c, glob A, CV.l, glob G} /\
  (C.n <= G.g <C.n + C.q){2} /\ (validCircuitP bound ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b){2} /\ 
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\ 
      (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2}).
auto;progress [-split].
rewrite H0.
generalize H0;rewrite /validCircuitP fst_pair snd_pair /=.
progress;expect 5 smt.

   seq 1 1 : (
={glob DKCm, glob C, glob R, RedComon.query, RedComon.c,glob A, CV.l, glob G} /\
  (C.n <= G.g <C.n + C.q){2} /\ (validCircuitP bound ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b){2} /\ 
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\ 
      (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2}).
  (if;first done);last by auto.
  call queryRO.
  call queryRO.
(*  by auto.

  (if;first done);last (auto;progress;expect 1 smt).

  call queryRO.
  call queryRO.
  by auto;progress;expect 1 smt.

by auto.*)
admit.
admit.
admit.
qed.

  local equiv GameAda_GameAdaRO :  DKCSecurity.GameAda(Dkc,RedI_Ada(A)).work ~ MIB(GameAdaRO).mainE : 
          ={glob A, DKCm.b, CV.l} ==> 
         ={DKCm.b, res}.
  proof.
admit.
   qed.

lemma in_dom_some : forall (x:'a) (m:('a, 'b) map),
in_dom x m => Some (oget m.[x]) = m.[x].
proof.
smt.
qed.

pred inv_IB(
  IBxx:(int*bool*bool, word) map, IBxxG:(int*bool*bool, word) map,
  Cn:int, Cm:int, Cq:int, Cv:bool array, Caa:int array, Cbb:int array, Cgg:bool gates_t,
  Rxx:tokens_t, Rt:bool array,
  Gg:int, GrandG:(int*bool*bool, word) map, Gpp:word gates_t,
  kpub:(int*bool, word) map, r:(int*bool, word) map, l:int
) =
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IBxx) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IBxxG) /\
  (forall (j : int) (v : bool),
    0 <= j < Cn + Cq => in_dom (j, v) Rxx => Rxx.[(j, v)] =
    IBxx.[(j, v, v ^^ Cv.[j] ^^ Rt.[j])]) /\
  (forall (j : int) (v : bool),
    0 <= j < Cn + Cq => in_dom (j, v) kpub => kpub.[(j, v)] =
    IBxx.[(j, v, v ^^ Cv.[j] ^^ Rt.[j])]) /\
  (forall (j : int) (v : bool) (a:bool) (b:bool),
    0 <= j < Cn + Cq => in_dom (j, v) r => r.[(j, v)] =
    IBxxG.[(j, if (l = Caa.[j]) then true else false, if (l = Caa.[j]) then false else true)]) /\
  (forall (j : int) (a b : bool),
    0 <= j < Cn + Cq => in_dom (j, a, b) GrandG => GrandG.[(j, a, b)] =
    IBxxG.[(j, a, b)]).

pred inv_xxG(
  IBxxG:(int*bool*bool, word) map,
  Cn:int, Cq:int,
  GrandG:(int*bool*bool, word) map
) =
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IBxxG) /\
  (forall (j : int) (a b : bool),
    0 <= j < Cn + Cq => in_dom (j, a, b) GrandG => GrandG.[(j, a, b)] =
    IBxxG.[(j, a, b)]).

pred inv_Gpp_dom1(
  Cn:int, Caa:int array, Cbb:int array,
  Rt:bool array,
  Gpp:word gates_t,
  l:int,
  x
) =
  (forall (g:int), Cn <= g < x =>
    let a = Caa.[g] in
    let b = Cbb.[g] in
    (a = l) <=> in_dom (g, !Rt.[a],  Rt.[b]) Gpp) /\
  (forall (g:int), Cn <= g < x =>
    let a = Caa.[g] in
    let b = Cbb.[g] in
    (a = l) <=> in_dom (g, !Rt.[a], !Rt.[b]) Gpp) /\
  (forall (g:int), Cn <= g < x =>
    let a = Caa.[g] in
    let b = Cbb.[g] in
    (b = l) <=> in_dom (g,  Rt.[a], !Rt.[b]) Gpp) /\
  (forall (g:int), Cn <= g < x =>
    let a = Caa.[g] in
    let b = Cbb.[g] in
    (b = l) <=> in_dom (g, !Rt.[a], !Rt.[b]) Gpp).

pred inv_Gpp_dom2(
  Cn:int, Caa:int array, Cbb:int array,
  Rt:bool array,
  Gpp:word gates_t,
  x
) =
  forall (g:int, a:bool, b:bool), Cn <= g < x <=>
    in_dom (g, a, b) Gpp.

pred inv_Gpp(
  Gpp:word gates_t,
  Cv:bool array, Caa:int array, Cbb:int array, Cgg:bool gates_t,
  Rxx:tokens_t, Rt:bool array,
  IBxxG:(int*bool*bool, word) map,
  l:int
) =
  (forall (g:int),
    let a = Caa.[g] in
    let b = Cbb.[g] in
    in_dom (g, Rt.[a], Rt.[b]) Gpp => 
      oget Gpp.[(g,  Rt.[a],  Rt.[b])] =
        (E
          (tweak g ( Rt.[a]) ( Rt.[b]))
          (oget Rxx.[(a,  Cv.[a])])
          (oget Rxx.[(b,  Cv.[b])])
          (oget Rxx.[(g, (oget Cgg.[(g, Cv.[a],Cv.[b])]))])
        )
  ) /\
  (forall (g:int),
    let a = Caa.[g] in
    let b = Cbb.[g] in
    in_dom (g, !Rt.[a], Rt.[b]) Gpp =>
      oget Gpp.[(g, !Rt.[a], Rt.[b])] =
        (E
          (tweak g (!Rt.[a]) ( Rt.[b]))
          (oget Rxx.[(a, !Cv.[a])])
          (oget Rxx.[(b,  Cv.[b])])
          (if a <= l
           then (if l < b /\ ((oget Cgg.[(g, !Cv.[a],false)]) = (oget Cgg.[(g, !Cv.[a],true)]))
                then oget Gpp.[(g, !Rt.[a], !Rt.[b])]
                else oget IBxxG.[(g, true, false)])
           else oget Rxx.[(g, (oget Cgg.[(g, !Cv.[a],Cv.[b])]))])
        )
  ) /\
  (forall (g:int),
    let a = Caa.[g] in
    let b = Cbb.[g] in
    in_dom (g, Rt.[a], !Rt.[b]) Gpp =>
      oget Gpp.[(g, Rt.[a], !Rt.[b])] =
        (E
          (tweak g ( Rt.[a]) (!Rt.[b]))
          (oget Rxx.[(a,  Cv.[a])])
          (oget Rxx.[(b, !Cv.[b])])
          (if b <= l
           then oget IBxxG.[(g, false, true)]
           else oget Rxx.[(g, (oget Cgg.[(g, Cv.[a],!Cv.[b])]))])
        )
  ) /\
  (forall (g:int),
    let a = Caa.[g] in
    let b = Cbb.[g] in
    in_dom (g, !Rt.[a], !Rt.[b]) Gpp =>
      oget Gpp.[(g, !Rt.[a], !Rt.[b])] =
        (E
          (tweak g (!Rt.[a]) (!Rt.[b]))
          (oget Rxx.[(a, !Cv.[a])])
          (oget Rxx.[(b, !Cv.[b])])
          (if b <= l
           then oget IBxxG.[(g, true, true)]
           else oget Rxx.[(g, (oget Cgg.[(g, !Cv.[a],!Cv.[b])]))])
        )
  ).

pred inv_G(Gg:int, Ga:int, Gb:int, Caa:int array, Cbb:int array) =
  Ga = Caa.[Gg] /\
  Gb = Cbb.[Gg].

print theory FMap.

lemma in_dom_emptyR : forall x, in_dom x FMap.empty<:'a, 'b> = false by smt.

print theory Logic.

lemma ifT : forall (x:bool) (y z:'a), x => (if x then y else z) = y by smt.
lemma ifF : forall (x:bool) (y z:'a), !x => (if x then y else z) = z by smt.

local lemma coreAda : forall xx xxG, phoare[RedI_AdaRO(IB, MyDkc(IB)).coreAda:
  IB.xx = xx /\
  IB.xxG = xxG /\
  0 <= CV.l < C.n + C.q - C.m /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xx) /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xxG) /\
  validCircuitP bound ((C.n, C.m, C.q, C.aa, C.bb), C.gg) /\
  ((C.n + C.q) <= maxGates) /\
  (R.xx.[(CV.l, !C.v.[CV.l])] = Some DKCm.ksec) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q =>
    in_dom (j, v) R.xx =>
      R.xx.[(j, v)] = IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])])
==>
  IB.xx = xx /\
  IB.xxG = xxG /\
  (R.xx.[(CV.l, !C.v.[CV.l])] = Some DKCm.ksec) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q =>
    in_dom (j, v) R.xx =>
      R.xx.[(j, v)] = IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t IB.xxG CV.l) /\
  (inv_Gpp_dom1 C.n C.aa C.bb R.t G.pp CV.l (C.n + C.q))
] = 1%r.
proof.
admit.
qed.

local lemma garble : forall xx xxG, phoare[RedI_AdaRO(IB, MyDkc(IB)).garble :
  IB.xx = xx /\
  IB.xxG = xxG /\
  C.n + C.q <= maxGates /\
  0 <= CV.l < C.n + C.q - C.m /\
  C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg) /\
  length C.x = C.n /\
  validCircuitP bound C.f /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xx) /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xxG) /\
  (inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t IB.xxG CV.l) /\
  (inv_Gpp_dom1 C.n C.aa C.bb R.t G.pp CV.l (C.n + C.q))
==>
  IB.xx = xx /\
  IB.xxG = xxG /\
  (inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t IB.xxG CV.l) /\
  (inv_Gpp_dom2 C.n C.aa C.bb R.t G.pp (C.n + C.q))
] = 1%r.
proof.
admit.
qed.

local lemma initG : forall xx xxG, phoare[Hybrid2(IB).initG :
  IB.xx = xx /\
  IB.xxG = xxG /\
  C.n + C.q <= maxGates /\
  C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg) /\
  length C.x = C.n /\
  validCircuitP bound C.f /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xx) /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xxG)
==>
  IB.xx = xx /\
  IB.xxG = xxG /\
  (inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t IB.xxG CV.l) /\
  (inv_Gpp_dom2 C.n C.aa C.bb R.t G.pp (C.n + C.q))
] = 1%r.
proof.
admit.
qed.

lemma oget_eq :
  forall (x y:'a option),
    x <> None => y <> None =>
    oget x = oget y =>
      x = y.
proof.
intros x y.
case x;case y=> //.
smt.
qed.

lemma not : forall (a b:bool), (! a = b) <=> a = ! b by (intros a b;case a;case b).

lemma reconcilation :
forall pp1 pp2 (v:bool array) aa bb gg xx1 xx2 b ksec t xxG (l n q:int),
  (forall (i : int) (vv : bool),
    0 <= i < n + q =>
    xx2.[(i, vv)] =
    if (i, vv) = (l, ! v.[l]) then Some ksec
    else xx1.[(i, vv)]) =>
  (inv_Gpp_dom2 n aa bb t pp1 (n + q)) =>
  (inv_Gpp_dom2 n aa bb t pp2 (n + q)) =>
  (inv_Gpp pp1 v aa bb gg xx1 t xxG l) =>
  (inv_Gpp pp2 v aa bb gg xx2 t xxG (if b then l - 1 else l)) =>
pp1 = pp2.
proof.
admit.
(*
simplify inv_Gpp inv_Gpp_dom2.
progress.
cut in_dom: forall g a b, in_dom (g, a, b) pp1 <=> in_dom (g, a, b) pp2
  by (intros g a b;rewrite -H0 -H).
apply map_ext;rewrite /FMap.(==) => [g a b].
case (in_dom (g, a, b) pp1)=> h1;cut := h1;rewrite in_dom => h2;
  last by move: h1 h2;rewrite !/in_dom !mem_dom !nnot=> -> ->.
cut hg : n <= g < n + q by rewrite (H g a b).
apply oget_eq.
by rewrite -mem_dom;apply h1.
by rewrite -mem_dom;apply h2.
(case (a = t.[aa.[g]]);[|rewrite not]);(case (b = t.[bb.[g]]);[|rewrite not]);progress.
  by rewrite H1 // H5.
  by rewrite H3 // H7.
  by rewrite H2 // H6 // H4 1:-H // H8 1:-H0.
  by rewrite H4 // H8.
*)
qed.

(* TODO :
 -- Construire l'invariant sur randG
 -- Faire un invariant donnant le domaine de R.xx après les queries
*)

lemma Rxx_increase : forall xx1 (n q:int) pp (v:bool array) aa bb gg xx2 t xxG l,
  (inv_Gpp_dom1 n aa bb t pp l (n + q)) =>
  (inv_Gpp pp v aa bb gg xx1 t xxG l) =>
  (forall i v, in_dom (i, v) xx1 => xx1.[(i, v)] = xx2.[(i, v)]) =>
  (inv_Gpp pp v aa bb gg xx2 t xxG l).
proof.
admit.
qed.

local lemma sampleTokens : forall xx, phoare[RedI_AdaRO(IB, MyDkc(IB)).sampleTokens :
  IB.xx = xx /\
  (inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t IB.xxG CV.l) /\
  (inv_Gpp_dom1 C.n C.aa C.bb R.t G.pp CV.l (C.n + C.q)) /\
  (((C.n + C.q) <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) R.xx => R.xx.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])])) ==>
  IB.xx = xx /\
  (inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t IB.xxG CV.l) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx) /\
  forall (j : int) (v : bool), 0 <= j < C.n + C.q => (j, v) <> (CV.l, !C.v.[CV.l]) =>
    R.xx.[(j, v)] = IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]
] = 1%r.
proof.
intros xx.
proc.
auto.
conseq (_:_==>true) (_:_==>
  IB.xx = xx /\
  (inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t IB.xxG CV.l) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx) /\
  forall (j : int) (v : bool),
    0 <= j < C.n + C.q => (j, v) <> (CV.l, !C.v.[CV.l]) =>
      R.xx.[(j, v)] =  IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]
)=> //;last by admit. (* ll *)
exists* R.xx;elim* => Rxx.
while (
  (0 <= i) /\
  ((C.n + C.q) <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) R.xx => R.xx.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool),
    0 <= j < i =>  (j, v) <> (CV.l, !C.v.[CV.l]) =>
      R.xx.[(j, v)] = IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  IB.xx = xx /\
  (forall i v, in_dom (i, v) Rxx => Rxx.[(i, v)] = R.xx.[(i, v)])
).
wp.
inline *.
seq 1 : (
  (0 <= i{hr}) /\
  (i{hr} < C.n{hr} + C.q{hr}) /\
  ((C.n + C.q){hr} <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){hr} /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) R.xx => R.xx.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool),
    0 <= j < i => (j, v) <> (CV.l, !C.v.[CV.l]) =>
      R.xx{hr}.[(j, v)] = IB.xx{hr}.[(j, v, v ^^ C.v{hr}.[j] ^^ R.t{hr}.[j])]) /\
  R.xx{hr}.[(i, C.v.[i])] = IB.xx{hr}.[(i, C.v.[i], C.v.[i] ^^ C.v{hr}.[i] ^^ R.t{hr}.[i])] /\
  IB.xx = xx /\
  (forall i v, in_dom (i, v) Rxx => Rxx.[(i, v)] = R.xx.[(i, v)])
).
if;[auto|skip;progress;smt].
progress [-split];split;first smt.
progress [-split];split;first smt.
progress [-split];split;first smt.
cut h : in_dom (i{hr}, C.v{hr}.[i{hr}], R.t{hr}.[i{hr}]) IB.xx{hr}
  by (rewrite -H1 all_tuple_int_spec //;smt).
progress [-split];split;first smt.
split.
intros j v hj.
rewrite !h /= !in_dom_set !FMap.get_set.
case ((i{hr}, C.v{hr}.[i{hr}]) = (j, v));progress;
  [rewrite xorK (Bool.xorC false) xor_false|];smt.
split.
progress;rewrite !h /= !FMap.get_set;smt.
split.
progress;rewrite !h /= !FMap.get_set;smt.
split.
smt.
progress;smt.

if;auto;[|progress;[smt|case (j = i{hr});smt]].

progress [-split];split;first smt.
progress [-split];split;first smt.
cut h : in_dom (i{hr}, !C.v{hr}.[i{hr}], !R.t{hr}.[i{hr}]) IB.xx{hr}
  by (rewrite -H2 all_tuple_int_spec //;smt).
progress [-split];split;first smt.

split.
intros j v hj.
rewrite !h /= !in_dom_set !FMap.get_set.
case ((i{hr}, ! C.v{hr}.[i{hr}]) = (j, v));progress;smt.

split.
intros j v hj1 hj2.
rewrite !FMap.get_set.
case ((i{hr}, ! C.v{hr}.[i{hr}]) = (j, v));
  case (in_dom (i{hr}, ! C.v{hr}.[i{hr}], ! R.t{hr}.[i{hr}]) IB.xx{hr});
  progress;smt.
split.
smt.
progress.
rewrite FMap.get_set.
case (i0 = i{hr});last smt.
case ((! C.v{hr}.[i{hr}]) = v);progress;[cut : false|cut -> : ((! C.v{hr}.[i{hr}]) = v) =false];
simplify;smt.

auto;progress[-delta];smt.
qed.

module LeftRT = {
  proc work() : bool = {
    var t:bool;
    t = ${0,1};
    Rf2.initT(true);
    R.t.[CV.l] = ! t;
    return t;
  }
}.

local equiv Rt_Rxx2 : LeftRT.work ~ Hybrid2(IB).Rf.init :
  useVisible{2} = true /\
  R.xx{1} = FMap.empty /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xx{2}) /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xxG{2}) /\
  ={glob C, glob IB} /\
  (0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2}) /\
  (C.n{1} + C.q{1} <= maxGates) /\
  (CV.l{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) /\
  (validCircuitP bound ((C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}), C.gg{2}))
==>
  (forall (i : int) (v : bool),
     0 <= i < C.n{2} + C.q{2} =>
     R.xx{2}.[(i, v)] = IB.xx{2}.[(i, v, v ^^ C.v{2}.[i] ^^ R.t{2}.[i])]) /\
  ={R.t, glob IB} /\ (! res{1} = R.t{1}.[CV.l{1}]).
proof.
proc.
inline *.
swap{1} 3.
seq 3 3 : (
  i{1} = 0 /\
  ={i, R.t} /\
  (useVisible{1} = true) /\
  (useVisible{2} = true) /\
  (R.xx{2} = FMap.empty) /\
  (length R.t = C.n + C.q){2} /\

  ={glob C, glob IB} /\
  0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2} /\
  0 <= C.m{2} /\
  (0 <= C.n + C.q <= maxGates){2} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2}
).
conseq * (_:_==>
  i{1} = 0 /\
  ={i, R.t} /\
  (useVisible{1} = true) /\
  (useVisible{2} = true) /\
  (R.xx{2} = FMap.empty) /\
  (length R.t = C.n + C.q){2}
);first by progress;smt.
by wp;skip;progress;expect 1 rewrite length_init;smt.

transitivity{1} {
  t = $Dbool.dbool;
  while (i < C.n + C.q) {
    trnd = $Dbool.dbool;
    v = C.v.[i];
    trnd = if i >= C.n + C.q - C.m then v else trnd;
    R.t.[i] = if i = CV.l then t else trnd;
    i = i + 1;
  }
}

(={glob C, glob CV, glob IB, R.t, i} /\
  (length R.t = C.n + C.q){1} /\
  (useVisible{1} = true) /\
  (0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2}) /\
  (0 <= C.m{2}) /\
  i{1} = 0
==>
  ={R.t, glob IB} /\
  ={R.t} /\
  (!t = R.t.[CV.l]){1}
)
(

  ={glob C, glob IB, R.t, i} /\
  (0 <= C.n + C.q <= maxGates){2} /\
  (useVisible{2} = true) /\
  0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2} /\
  (i{2} = 0) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (R.xx{2} = FMap.empty) /\
  (length R.t = C.n + C.q){2}
==>
  ={R.t, glob IB} /\
  (forall (i0 : int) (v0 : bool), 0 <= i0 < C.n{2} + C.q{2} =>
    R.xx{2}.[(i0, v0)] = IB.xx{2}.[(i0, v0, v0 ^^ C.v{2}.[i0] ^^ R.t{2}.[i0])])
);[progress;smt|done| |].
conseq* (_:_==>
  ={R.t} /\
  (!t = R.t.[CV.l]){1}
)=> //.
wp;while (={glob C, glob CV} /\
  (useVisible{1} = true) /\
  (0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2}) /\
  (C.m{2} >= 0) /\
  
  (length R.t = C.n + C.q){1} /\
  (length R.t = C.n + C.q){2} /\
  ={i} /\
  (forall j, 0 <= j < length R.t => j <> CV.l => R.t{1}.[j] = R.t{2}.[j]){1} /\
  (i > CV.l => R.t.[CV.l] = t){2}
).
conseq * (_:_==>
  (length R.t = C.n + C.q){1} /\
  (length R.t = C.n + C.q){2} /\
  ={i} /\
  (forall j, 0 <= j < length R.t => j <> CV.l => R.t{1}.[j] = R.t{2}.[j]){1} /\
  (i > CV.l => R.t.[CV.l] = t){2}
)=> //.
auto;progress [-split].
progress.
by rewrite length_set.
by rewrite length_set.
by rewrite !get_set;smt.
by rewrite !get_set;smt.
rnd (fun (x:bool), !x).
skip;progress;(try (apply array_ext;split));smt.

transitivity{1} {
  while (i < C.n + C.q) {
    t = $Dbool.dbool;
    trnd = $Dbool.dbool;
    v = C.v.[i];
    trnd = if i >= C.n + C.q - C.m then v else trnd;
    R.t.[i] = if i = CV.l then t else trnd;
    i = i + 1;
  }
}

(
  ={glob C, glob CV, glob R, glob IB, i}
==>
  ={glob C, glob CV, glob R, glob IB, i}
)
(
  ={glob C} /\
  (0 <= C.n + C.q <= maxGates){2} /\
  (useVisible{2} = true) /\
  0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2} /\
  ={i, R.t, glob IB} /\
  (i{2} = 0) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (R.xx{2} = FMap.empty) /\
  (length R.t = C.n + C.q){2}
==>
  ={R.t, glob IB} /\
  (forall (i0 : int) (v0 : bool), 0 <= i0 < C.n{2} + C.q{2} =>
    R.xx{2}.[(i0, v0)] = IB.xx{2}.[(i0, v0, v0 ^^ C.v{2}.[i0] ^^ R.t{2}.[i0])])
);[progress;smt|done| |].

eager while (h: t = ${0,1} ;~  : true ==> true)=> //.
rnd{1};skip;progress;smt.
sim.
sim.

while (
  ={glob C} /\
  (0 <= C.n + C.q <= maxGates){2} /\
  (useVisible{2} = true) /\
  0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2} /\

  ={i, R.t, glob IB} /\
  (0 <= i{2}) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (forall (i0 : int) (v0 : bool),
    0 <= i0 < i{2} =>
    R.xx{2}.[(i0, v0)] =
    IB.xx{2}.[(i0, v0, v0 ^^ C.v{2}.[i0] ^^ R.t{2}.[i0])]) /\
  (length R.t = C.n + C.q){2}
).
conseq* (_:_==>
  ={i, R.t, glob IB} /\
  (0 <= i{2}) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (forall (i0 : int) (v0 : bool),
    0 <= i0 < i{2} =>
    R.xx{2}.[(i0, v0)] =
    IB.xx{2}.[(i0, v0, v0 ^^ C.v{2}.[i0] ^^ R.t{2}.[i0])]) /\
  (length R.t = C.n + C.q){2}
)=> //.
seq 2 1 : (
  ={glob C} /\
  (0 <= C.n + C.q <= maxGates){2} /\
  (useVisible{2} = true) /\ 
  0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2} /\
  
  i{1} < C.n{1} + C.q{1} /\
  ={i, R.t, glob IB} /\
  (0 <= i{2}) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (forall (i0 : int) (v0 : bool),
    0 <= i0 < i{2} =>
    R.xx{2}.[(i0, v0)] =
    IB.xx{2}.[(i0, v0, v0 ^^ C.v{2}.[i0] ^^ R.t{2}.[i0])]) /\
  (length R.t = C.n + C.q){2} /\
  trnd{2} = if (i = CV.l){1} then t{1} else trnd{1}
).
conseq* (_:_==>
  trnd{2} = if (i = CV.l){1} then t{1} else trnd{1}
)=> //.
case ((i = CV.l){1});[swap{1} 1 1|];rnd;rnd{1};skip;progress;smt.

wp;rnd{2};wp;rnd{2};wp.

skip.

progress [-split];split;first smt.
progress [-split];split;first smt.

cut h : (forall x y,in_dom (i{2}, x, y) IB.xx{2})
  by (intros x y;rewrite -H5;apply all_tuple_int_spec;smt).
do rewrite !h /=.
progress [-split];split.
apply array_ext;split;smt.

progress [-split];split;first smt.
progress [-split];split;first smt.

progress [-split];split.

progress [-split].

case (i{2} = i0).
progress;rewrite !FMap.get_set /= !Array.get_set /=. smt.
cut lem : forall (x y:bool), (!x) ^^ x ^^ y = ! y by smt.
case (v0 = C.v{2}.[i{2}]);[|move=> temp;cut : v0 =(! C.v{2}.[i{2}]) by smt];
progress;rewrite ?neq /=;
rewrite ?(lem, xorK, (Bool.xorC false), xor_false);
pose x := IB.xx{2}.[_];
cut hh : ! x = None by smt;
by case{-1} (x) (Logic.eq_refl x).


progress;rewrite !FMap.get_set /= !Array.get_set /=. smt.
cut -> : (i{2} = i0) = false by smt.
simplify.
apply H6.
smt.
smt.

skip;progress;smt.
qed.

  local lemma reductionAdaCore :
    equiv [DKCSecurity.GameAda(Dkc,RedI_Ada(A)).work ~ Hybrid(A).work:
      ={glob A} /\ (x{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1})
      /\ (0 <= CV.l{1} < bound) ==>
      res{1} = if DKCm.b{1} then res{2} else !res{2} ].
  proof.
timeout 10.
(*----------------------------------------------------------------------*)
(*------------------ EAGER ---------------------------------------------*)
(*----------------------------------------------------------------------*)
transitivity MIB(GameAdaRO).mainE
  (={glob A, DKCm.b, CV.l} ==>  ={DKCm.b, res})
  (={glob A} /\ (x{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1})
      /\ (0 <= CV.l{1} < bound) ==> res{1} = if DKCm.b{1} then res{2} else ! res{2});
  [progress;smt|done|apply GameAda_GameAdaRO| ].
transitivity MIB(Hybrid2).mainE
  (={glob A} /\ (x{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1})
      /\ (0 <= CV.l{1} < bound) ==> res{1} = if DKCm.b{1} then res{2} else ! res{2})
  ( ={glob Hybrid2, x} ==> ={res});
  [ |done| |apply Hybrid2_Hybrid].
progress;exists (glob A){2}, R.t{2}, R.xx{2}, C.gg{2}, C.aa{2}, C.m{2}, C.x{2}, C.f{2},
        C.n{2}, C.q{2}, C.bb{2}, C.v{2}, G.b{2}, G.randG{2}, G.pp{2}, G.yy{2}, G.a{2}, G.g{2},
        CV.l{2}, F.flag_tt{2},  F.flag_ft{2},  F.flag_tf{2}, F.flag_sp{2},
        Hybrid2.query{2}, Hybrid2.b0{2}, (if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) => //.
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
    proc.
    inline MIB(GameAdaRO).A.init.
    inline MIB(Hybrid2).A.init.
    inline MIB(GameAdaRO).A.main.
    inline MIB(Hybrid2).A.main.
    inline GameAda(MyDkc(IB), RedI_AdaRO(IB)).A.work.
    inline Dkc.get_challenge.
    inline RedI_AdaRO(IB, MyDkc(IB)).pre.
    inline RedI_AdaRO(IB, MyDkc(IB)).post.
    inline MyDkc(IB).initialize.
    inline IB.init.

(*----------------------------------------------------------------------*)
(*------------------ get_challenge adversary call ----------------------*)
(*----------------------------------------------------------------------*)
   swap{2} [3..6] 6.
    wp.
    swap{1} 16 -15.
    swap{2} 9 - 8.
    
    seq 1 1 : (={glob A} /\ (x{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) /\ (0 <= CV.l{1} < bound) /\ (RedComon.query{1} = Hybrid2.query{2})).
    call (_: ={glob A} ==> ={glob A, res})=> //;proc true=> //.
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)

(*----------------------------------------------------------------------*)
(*----------------------- Query Valid Test -----------------------------*)
(*----------------------------------------------------------------------*)
    case (EncSecurity.queryValid_IND RedComon.query{1});first last.

    (* Query invalid Case *)
    seq 27 11 : ((!EncSecurity.queryValid_IND Hybrid2.query{2}) /\ (!EncSecurity.queryValid_IND RedComon.query{1})).
      kill{1} 1!27;first by admit. (* ll *)
(*
        call (RedComon_garble_ll A).
        call (RedComon_sampleTokens_ll A).
        call (RedI_Ada_coreAdaL A Dkc _).
          by apply encryptL.
        inline *.
        wp; while true (C.n + C.q - i0); first by auto; smt.
        wp; while true (C.n + C.q - i); first by auto; smt.
        by auto; progress; expect 4 smt.*)
      kill{2} 1!11;first by admit. (* ll *)
     done.
    rcondf{1} 1=> //.
    rcondf{2} 1=> //.
    by auto; smt.
     
    (* Query valid Case *)
    rcondt{1} 28. 
      move=> &m; kill 1!27;first by admit. (* ll *)
(*        call (RedComon_garble_ll A).
        call (RedComon_sampleTokens_ll A).
        call (RedI_Ada_coreAdaL A Dkc _).
          by apply encryptL.
        inline *.
        wp; while true (C.n + C.q - i0); first by auto; smt.
        wp; while true (C.n + C.q - i); first by auto; smt.
        by auto; progress; expect 4 smt.
*)
    done.
    rcondt{2} 12=> //.
      move=> &m; kill 1!11;first by admit. (* ll *)
    done.
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)

(*----------------------------------------------------------------------*)
(*------------------- get_challenge Adversary call ---------------------*)
(*----------------------------------------------------------------------*)
    wp.
    call (_: true).
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)

seq 19 11 : (
  0 <= C.n{2} /\
  0 <= C.m{2} /\
  (R.xx = FMap.empty){1} /\
  (0 <= C.n + C.q <= maxGates){2} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG){2} /\
  ={glob C, glob IB, glob A} /\
  (0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2}) /\
  (CV.l{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) /\
  RedComon.c{1} = Hybrid2.b0{2} /\
  (forall i, 0 <= i < C.n{2} => C.v{2}.[i] = C.x{2}.[i]) /\
  (C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
  (length C.x = C.n){2} /\
  validCircuitP bound C.f{2}
).
(*----------------------------------------------------------------------*)
(*-------------------------------- INIT --------------------------------*)
(*----------------------------------------------------------------------*)
call CinitE.
wp.
rnd.
wp.
rnd{1}.
rnd{1}.
inline *.
while (
={work2, IB.xxG} /\
(work2 <= work0){2} /\
(forall x, (mem x work0 /\ !mem x work2) <=> in_dom x IB.xxG){2}
);first by auto;progress;smt.
wp.
while (
={work1, IB.xx} /\
(work1 <= work0){2} /\
(forall x, (mem x work0 /\ !mem x work1) <=> in_dom x IB.xx){2}
);first by auto;progress;smt.
wp.
skip.
progress [-split];split;first smt.
progress [-split];split;first smt.
progress [-split];split;first smt.
progress [-split];split;first smt.
progress [-split];split;first done.
progress [-split];split.
generalize H1.
rewrite /EncSecurity.queryValid_IND /EncSecurity.Encryption.valid_plain /EncSecurity.Encryption.leak /validInputs /validInputsP /C2.validInputs.
smt.
progress [-split];split;first smt.
generalize H14.
rewrite /validInputsP /validCircuitP ?(fst_pair, snd_pair);progress.
smt.
smt.
smt.
smt.
smt.
smt.
smt.
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)

seq 3 1 : (
(**** unmodified ****)
  (R.xx = FMap.empty){1} /\
  (C.n + C.q <= maxGates){2} /\
  ={glob C, glob A} /\
  (CV.l{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) /\
  (0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2}) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG){2} /\
  RedComon.c{1} = Hybrid2.b0{2} /\
  (forall i, 0 <= i < C.n{2} => C.v{2}.[i] = C.x{2}.[i]) /\
  (C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
  (length C.x = C.n){2} /\
  validCircuitP bound C.f{2} /\
(********************)

  (forall i v,  0 <= i < C.n{2} + C.q{2} => R.xx.[(i, v)] = IB.xx.[(i, v, v ^^ C.v{2}.[i]^^R.t.[i])]){2} /\
  ={R.t, glob IB} /\
  (!t = R.t.[CV.l]){1}
).
conseq * (_:_==> (
  (forall i v,  0 <= i < C.n{2} + C.q{2} => R.xx.[(i, v)] = IB.xx.[(i, v, v ^^ C.v{2}.[i]^^R.t.[i])]){2} /\
  ={R.t, glob IB} /\
  (!t = R.t.[CV.l]){1}
));first done.
(*----------------------------------------------------------------------*)
(*------------------------- R.t + R.xx{2} ------------------------------*)
(*----------------------------------------------------------------------*)
transitivity{1} {t = LeftRT.work();} (={glob CV, glob LeftRT, glob C, glob DKCm, glob IB, glob R, t} ==> ={glob CV, glob LeftRT, glob C, glob DKCm, glob IB, glob R, t}) (
  (R.xx = FMap.empty){1} /\
  (C.n + C.q <= maxGates){2} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG){2} /\
  ={glob C, glob IB, glob A} /\
  (0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2}) /\
  (CV.l{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) /\
  (forall i, 0 <= i < C.n{2} => C.v{2}.[i] = C.x{2}.[i]) /\
  (C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
  (length C.x = C.n){2} /\
  validCircuitP bound C.f{2}
==>
  (forall i v,  0 <= i < C.n{2} + C.q{2} => R.xx.[(i, v)] = IB.xx.[(i, v, v ^^ C.v{2}.[i]^^R.t.[i])]){2} /\
  ={R.t, glob IB} /\
  (!t = R.t.[CV.l]){1}
);[progress;smt|done|by inline LeftRT.work;sim|call Rt_Rxx2;skip;done].
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)

seq 3 0 : (
(**** unmodified ****)
  (!t = R.t.[CV.l]){1} /\
  (C.n + C.q <= maxGates){2} /\
  ={R.t, glob C, glob A} /\
  (CV.l{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) /\
  (0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2}) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG){2} /\
  (forall i v,  0 <= i < C.n{2} + C.q{2} => R.xx.[(i, v)] = IB.xx.[(i, v, v ^^ C.v{2}.[i]^^R.t.[i])]){2} /\
  RedComon.c{1} = Hybrid2.b0{2} /\
  (forall i, 0 <= i < C.n{2} => C.v{2}.[i] = C.x{2}.[i]) /\
  (C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
  (length C.x = C.n){2} /\
  validCircuitP bound C.f{2} /\
(********************)

  ={glob IB} /\
  (R.xx{2}.[(CV.l{1}, !C.v{1}.[CV.l{1}])] = Some DKCm.ksec{1}) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx{1}) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n{1} + C.q{1} => in_dom (j, v) R.xx{1} => R.xx{1}.[(j, v)] =
    IB.xx{1}.[(j, v, v ^^ C.v{1}.[j] ^^ R.t{1}.[j])]) /\
  (inv_Gpp G.pp{1} C.v{1} C.aa{1} C.bb{1} C.gg{1} R.xx{1} R.t{1} IB.xxG{1} CV.l{1}) /\
  (inv_Gpp_dom1 C.n{1} C.aa{1} C.bb{1} R.t{1} G.pp{1} CV.l{1} (C.n{1} + C.q{1}))
).
conseq* (_:_==>
  ={glob IB} /\
  (R.xx{2}.[(CV.l{1}, !C.v{1}.[CV.l{1}])] = Some DKCm.ksec{1}) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx{1}) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n{1} + C.q{1} => in_dom (j, v) R.xx{1} => R.xx{1}.[(j, v)] =
    IB.xx{1}.[(j, v, v ^^ C.v{1}.[j] ^^ R.t{1}.[j])]) /\
  (inv_Gpp G.pp{1} C.v{1} C.aa{1} C.bb{1} C.gg{1} R.xx{1} R.t{1} IB.xxG{1} CV.l{1}) /\
  (inv_Gpp_dom1 C.n{1} C.aa{1} C.bb{1} R.t{1} G.pp{1} CV.l{1} (C.n{1} + C.q{1}))
);first done.

(*----------------------------------------------------------------------*)
(*----------------------- coreAda(R.xx{1} + G.randG{1}) ----------------*)
(*----------------------------------------------------------------------*)
exists* IB.xx{1};elim* => xx.
exists* IB.xxG{1};elim* => xxG.

call{1} (coreAda xx xxG).

wp.
inline *.
wp.
rnd{1}.
wp.
skip.
progress [-split].
generalize H5.
rewrite /validCircuitP fst_pair.
progress [-split].
cut h : in_dom (CV.l{1}, ! C.v{2}.[CV.l{1}], t{1}) IB.xx{2}
  by (rewrite -H2 all_tuple_int_spec //;smt).
rewrite h.
progress.
smt.
by rewrite FMap.get_set.
(*TODO: WHY IS THIS SO COMPLICATED *)
rewrite FMap.get_set.
case ((CV.l{1}, ! C.v{2}.[CV.l{1}]) = (j, v));progress.
cut -> : (! C.v{2}.[CV.l{1}]) ^^ C.v{2}.[CV.l{1}] ^^ R.t{2}.[CV.l{1}] = !R.t{2}.[CV.l{1}] by smt.
cut -> : (! R.t{2}.[CV.l{1}]) = t{1} by smt.
generalize h.
rewrite /in_dom mem_dom.
case (IB.xx{2}.[(CV.l{1}, ! C.v{2}.[CV.l{1}], t{1})]);smt.
generalize H20.
rewrite in_dom_set.
cut -> : ((j, v) = (CV.l{1}, ! C.v{2}.[CV.l{1}])) = false by smt.
progress.
smt.
(************************************)
(*TODO: WHY IS THIS SO COMPLICATED *)
rewrite H6;first smt.
cut -> : (! C.v{2}.[CV.l{1}]) ^^ C.v{2}.[CV.l{1}] ^^ R.t{2}.[CV.l{1}] = !R.t{2}.[CV.l{1}] by smt.
cut -> : (! R.t{2}.[CV.l{1}]) = t{1} by smt.
generalize h.
rewrite /in_dom mem_dom.
case (IB.xx{2}.[(CV.l{1}, ! C.v{2}.[CV.l{1}], t{1})]);smt.
(************************************)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)

seq 1 0 : (
(**** unmodified ****)
  (!t = R.t.[CV.l]){1} /\
  (C.n + C.q <= maxGates){2} /\
  ={R.t, glob C, glob A} /\
  (CV.l{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) /\
  (0 <= CV.l{1} < C.n{2} + C.q{2} - C.m{2}) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG){2} /\
  RedComon.c{1} = Hybrid2.b0{2} /\
  (forall i, 0 <= i < C.n{2} => C.v{2}.[i] = C.x{2}.[i]) /\
  (C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg)){2} /\
  (length C.x = C.n){2} /\
  validCircuitP bound C.f{2} /\
(********************)

  ={glob IB} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){2} /\
  (inv_Gpp G.pp{1} C.v{1} C.aa{1} C.bb{1} C.gg{1} R.xx{1} R.t{1} IB.xxG{1} CV.l{1}) /\
  (inv_Gpp_dom1 C.n{1} C.aa{1} C.bb{1} R.t{1} G.pp{1} CV.l{1} (C.n{1} + C.q{1})) /\
  forall (i : int) (v : bool),
    0 <= i < C.n{2} + C.q{2} =>
    R.xx{2}.[(i, v)] = if (i, v) = (CV.l{1}, !C.v{1}.[CV.l{1}]) then Some DKCm.ksec{1} else R.xx{1}.[(i, v)]
).

conseq* (_:_==> ={glob IB} /\
  (inv_Gpp G.pp{1} C.v{1} C.aa{1} C.bb{1} C.gg{1} R.xx{1} R.t{1} IB.xxG{1} CV.l{1}) /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xx{2}) /\
  forall (i : int) (v : bool),
    0 <= i < C.n{2} + C.q{2} =>
    R.xx{2}.[(i, v)] = if (i, v) = (CV.l{1}, !C.v{1}.[CV.l{1}]) then Some DKCm.ksec{1} else R.xx{1}.[(i, v)]
);first done.

(*----------------------------------------------------------------------*)
(*----------------------- sampleTokens(R.xx{1}) ------------------------*)
(*----------------------------------------------------------------------*)
exists* (IB.xx{1});elim* => xx.
conseq* (_: _ ==>  xx = IB.xx{1} /\
  (inv_Gpp G.pp{1} C.v{1} C.aa{1} C.bb{1} C.gg{1} R.xx{1} R.t{1} IB.xxG{1} CV.l{1}) /\
  (forall (x1 : int * bool * bool),
     mem x1 (all_tuple_int maxGates) <=> in_dom x1 IB.xx{2}) /\
  (forall (i : int) (v : bool),
    0 <= i < C.n{2} + C.q{2} =>
    R.xx{2}.[(i, v)] = if (i, v) = (CV.l{1}, !C.v{1}.[CV.l{1}]) then Some DKCm.ksec{1} else R.xx{1}.[(i, v)])
)=> //.
call{1} (sampleTokens xx);skip;progress.
  case (i = CV.l{1} && v = ! C.v{2}.[CV.l{1}]).
    by intros [-> ->];rewrite H8 //. 
    by intros h;rewrite H5 // H20 //.
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)

wp.
conseq* (_:_ ==> ={G.pp}).
progress.

(*** INPUTS ARE OK ***)
generalize H5;rewrite /validCircuitP /= !fst_pair /=;progress.
rewrite /encode /C2.Input.encode /inputK /C2.inputK fst_pair /=.
cut h : 0 <= length C.x{2} by apply length_pos.
apply array_ext;split;[by rewrite !length_init|].
intros i.
rewrite length_init // => hi.
rewrite !get_init //=.
rewrite !get_filter /= !fst_pair !hi /= -H4 // H9 //;first smt.
by case (i = CV.l{1});[move=> -> /=;case (C.v{2}.[CV.l{1}])|].
(*********************)

(*** challenge bits are OK ****)
by case (DKCm.b{1});case result_R;case (Hybrid2.b0{2}).
(*********************)

(*----------------------------------------------------------------------*)
(*-------------------------------- Garble ------------------------------*)
(*----------------------------------------------------------------------*)
exists* (IB.xx{1});elim* => xx.
exists* (IB.xxG{1});elim* => xxG.
call{1} (garble xx xxG).
call{2} (initG xx xxG).
skip.
progress.
apply (reconcilation pp_L pp_R C.v{2} C.aa{2} C.bb{2} C.gg{2} R.xx{1} R.xx{2} DKCm.b{1} DKCm.ksec{1} R.t{2} IB.xxG{2} CV.l{1} (length C.x{2}) C.q{2}) => //.
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
(*----------------------------------------------------------------------*)
   qed.

  local module WB : MeanBool.Worker = {
    module M = DKCSecurity.GameAda(Dkc, RedI_Ada(A))

    proc work(x:bool) : bool = {
      var r : bool;
      M.preInit();
      DKCm.b = x;
      r = M.work();
      return r;
    }
  }.

  equiv eqExp (M <: Exp) : M.work ~ M.work : ={glob M} ==> ={res} by proc true.

  local lemma eqGameAda &m :
    Pr[MeanBool.Rand(WB).main()@ &m: snd res] = Pr[DKCSecurity.GameAda(Dkc, RedI_Ada(A)).main()@ &m:res].
  proof.
    byequiv (_: ={glob A} ==> (snd res){1} = res{2}) => //.
    proc.
    inline GameAda(Dkc, RedI_Ada(A)).preInit.
    inline Dkc.preInit.
    inline GameAda(Dkc, RedI_Ada(A)).A.preInit.
    inline RedI_Ada(A, Dkc).Com.preInit.
    inline WB.work.
    wp; call (eqExp WB.M).
    inline *; swap{1} 7 -6.
    by auto; smt.
  qed.

  lemma Mean_uniInt (A<:MeanInt.Worker) &m :
     (bound)%r * Pr[MeanInt.Rand(A).main()@ &m: snd res] =
       Mrplus.sum (fun (v:int), Pr[A.work(v)@ &m:res]) (Interval.interval 0 (bound-1)).
  proof.
    pose ev := fun (x:int) (gA:glob A) (b:bool), b.
    cut Hcr: forall x, 
               ISet.mem x (ISet.create (support (Dinter.dinter 0 (bound-1)))) <=>
               mem x (Interval.interval 0 (bound-1)).
      by smt.
    cut Hf : ISet.Finite.finite (ISet.create (support (Dinter.dinter 0 (bound-1)))).
      by exists (Interval.interval 0 (bound-1)) => x;apply Hcr.
    cut := MeanInt.Mean A &m ev _=> //=;simplify ev=> -> //.
    cut -> : 
      ISet.Finite.toFSet (ISet.create (support (Dinter.dinter 0 (bound-1)))) = (Interval.interval 0 (bound-1)).
      by apply FSet.set_ext => x; rewrite ISet.Finite.mem_toFSet //;apply Hcr.
    rewrite Mrplus.sum_in /=.
    pose f := (fun (v : int), if mem v ((Interval.interval 0 (bound-1)))%Interval then Pr[A.work(v) @ &m : res] else 0%r).
    pose g :=  (fun (v : real), (v / (bound)%r)).
    cut -> :  (fun (v : int), if mem v ((Interval.interval 0 (bound-1)))%Interval then mu_x [0..(bound-1)] v * Pr[A.work(v) @ &m : res] else 0%r) = fun v, g (f v).
      apply fun_ext=> v /=.
      simplify g f.
      case (mem v ((Interval.interval 0 (bound-1)))%Interval)=> h.
      rewrite Dinter.mu_x_def_in //;smt.
      smt.
    rewrite Mrplus.sum_comp.
    smt.
    smt.
    simplify f g.
    cut -> : (fun (v : int),
      if mem v ((Interval.interval 0 (bound-1)))%Interval then
        Pr[A.work(v) @ &m : res]
      else 0%r) = (fun (v : int),
      if mem v ((Interval.interval 0 (bound-1)))%Interval then
        (fun v, Pr[A.work(v) @ &m : res]) v
      else 0%r) by smt.
    rewrite -Mrplus.sum_in /=.
    cut: 0%r <> (bound)%r by smt.
    generalize (Mrplus.sum (fun (v : int), Pr[A.work(v) @ &m : res]) ((Interval.interval 0 (bound-1)))%Interval)%Mrplus.
    generalize (bound)%r.
    by move=> x x' neq0x; algebra.
  qed.

  local lemma Mean_uniBool (A<:MeanBool.Worker) &m :
     2%r * Pr[MeanBool.Rand(A).main()@ &m: snd res] =
       Pr[A.work(false)@ &m:res] + Pr[A.work(true)@ &m:res].
  proof.
    pose ev := fun (b:bool) (gA:glob A) (b':bool), b'.
    cut Hcr: forall x, 
               ISet.mem x (ISet.create (support {0,1})) <=>
               mem x (add true (add false (FSet.empty)%FSet)).
      by intros=> x; rewrite !FSet.mem_add; case x=> //=; smt.
    cut Hf : ISet.Finite.finite (ISet.create (support {0,1})).
      by exists (FSet.add true (FSet.add false FSet.empty)) => x;apply Hcr.
    cut := MeanBool.Mean A &m ev _=> //=;simplify ev=> -> //.
    cut -> : 
      ISet.Finite.toFSet (ISet.create (support {0,1})) = (FSet.add true (FSet.add false FSet.empty)).
      by apply FSet.set_ext => x; rewrite ISet.Finite.mem_toFSet //;apply Hcr.
    rewrite Mrplus.sum_add;first smt.
    rewrite Mrplus.sum_add;first smt.
    rewrite Mrplus.sum_empty /= !Bool.Dbool.mu_x_def;simplify ev.
    smt.
  qed.

  local lemma eqHybrid1 &m : Pr[WB.work(true)@ &m:res] = Pr[MeanInt.Rand(Hybrid1).main() @ &m : snd res].
  proof.
    byequiv (_: ={glob A, glob WB} /\ x{1} = true ==> res{1} = snd res{2})=> //.
    proc.
    inline Hybrid1.work.
    wp.
    call reductionAdaCore.
    inline *.
    auto; progress; expect 3 smt.
  qed.

  local lemma eqHybridA &m : Pr[WB.work(false)@ &m:res] = Pr[MeanInt.Rand(Hybrid(A)).main() @ &m : !snd res].
  proof.
    byequiv (_: ={glob WB} /\ x{1} = false ==> res{1} = !snd res{2})=> //.
    proc.
    wp.
    call reductionAdaCore.
    inline *.
    by auto; progress; expect 3 smt.
  qed.

  local lemma lossless_raW &m :
    Pr[MeanInt.Rand(Hybrid(A)).main() @ &m : true] = 1%r.
  proof.
    byphoare (_: true ==> true)=> //.
    proc.
    call (_: true ==> true); last by auto; smt.
    proc; call (_: true ==> true); last by wp.
    proc; seq 1: true 1%r 1%r 0%r _ => //=.
      by call AgenL.
      if; last by auto; smt.
      wp; call AgetL.
      call (_: true ==> true); last by auto; smt.
      proc; inline *.
      while true (C.n + C.q - G.g).
        by auto; progress; expect 2 smt.
      wp; while true (C.n + C.q - i0).
        by auto; progress; expect 4 smt.
      wp; while true (C.n + C.q - i).
        by auto; progress; expect 1 smt.
      by auto; smt.
  qed.

   local lemma reductionAda &m:
      Mrplus.sum (fun l, let l = l - 1 in Pr[Hybrid(A).work(l) @ &m : res]) (Interval.interval 0 (bound-1)) -
        Mrplus.sum (fun l, Pr[Hybrid(A).work(l) @ &m : res]) (Interval.interval 0 (bound-1)) =
        (bound)%r * (2%r * Pr[DKCSecurity.GameAda(Dkc, RedI_Ada(A)).main()@ &m:res] - 1%r).
    proof.
      simplify.
      cut -> : Mrplus.sum (fun (l : int), Pr[Hybrid(A).work(l - 1) @ &m : res]) (Interval.interval 0 (bound-1)) =
        Mrplus.sum (fun (l : int), Pr[Hybrid1.work(l) @ &m : res]) (Interval.interval 0 (bound-1)).
        congr=> //.
        apply fun_ext=> x /=.
        by apply (eqH1 &m x).
      rewrite -(Mean_uniInt Hybrid1 &m).
      rewrite -(Mean_uniInt (Hybrid(A)) &m).
      rewrite -(eqGameAda &m).
      rewrite (Mean_uniBool WB &m).
      rewrite (eqHybrid1 &m).
      rewrite (eqHybridA &m).
      rewrite Pr [mu_not].
      rewrite (lossless_raW &m).
      smt.
    qed.

   equiv DkcE_resample : DkcE.resample ~ DkcE.resample : ={glob DKCm} ==>
        ={glob DKCm} /\ 
        (forall k v, 0 <= k < 2*maxGates => in_dom (k,v) DKCm.r{1}) /\
        (forall k v, 0 <= k < 2*maxGates => in_dom (k,v) DKCm.kpub{1}).
   proof.
     proc.
     inline DkcE.resample_k.
     while (={glob DKCm, work} /\ 
              (forall kv, mem kv (all_pair_int (2*maxGates)) => !mem kv work{1} =>
                   in_dom kv DKCm.kpub{1})).
       by inline *;auto;progress;smt. 
     wp;conseq (_ : _ ==>   ={glob DKCm} /\ 
        (forall k v, 0 <= k < 2*maxGates => in_dom (k,v) DKCm.r{1})) => //.
       by progress;smt.
     inline DkcE.resample_r.
     while (={glob DKCm, work0} /\ 
              (forall kv, mem kv (all_pair_int (2*maxGates)) => !mem kv work0{1} =>
                   in_dom kv DKCm.r{1})).
       by inline *;auto;progress;smt.
     auto;progress;smt.
   qed.

   op update_Rxx (R_xx:tokens_t) (b:bool) (k1 k2:int*bool) (w: DKCSecurity.W.word) = 
     if b then R_xx.[k1 <- w] else  R_xx.[k2 <- w].

   op op_doAnswer (CV_l:int) (C_v:bool array) (C_gg:bool gates_t) (R_t : bool array)
          (G_g G_a G_b : int) (G_pp: word gates_t) (R_xx:tokens_t) (randG: word gates_t)
          (rand:bool) (alpha:bool) (bet:bool) (ans:answer) = 
     let gamma = C_v.[G_g] ^^ (oget C_gg.[(G_g, C_v.[G_a]^^alpha, C_v.[G_b]^^bet)]) in
     let (kki, kkj, zz) = ans in
     let G_pp = G_pp.[(G_g, R_t.[G_a]^^alpha, R_t.[G_b]^^bet) <- zz] in
     let R_xx = 
        update_Rxx R_xx (G_a = CV_l)  (G_b, C_v.[G_b]^^bet) (G_a, C_v.[G_a]^^alpha) kki in
     if rand then (G_pp, R_xx, randG.[(G_g, true, true) <- kkj] )
     else (G_pp, R_xx.[(G_g, C_v.[G_g]^^gamma) <- kkj], randG).
 
   op op_coreNAda_post_body 
        (C_aa C_bb: int array)
        (CV_l:int) (C_v:bool array) (C_gg:bool gates_t) (R_t:bool array)
        (answers: answer array)
        (G_g:int) (st : word gates_t * tokens_t * word gates_t * int) =
          let (G_pp, R_xx, randG, ans) = st in
          let G_a = C_aa.[G_g] in
          let G_b = C_bb.[G_g] in
          let (G_pp, R_xx, randG, ans) = 
            if (G_a = CV_l) then
              let (G_pp, R_xx, randG) = 
                op_doAnswer CV_l C_v C_gg R_t G_g G_a G_b G_pp R_xx randG
                     false true false answers.[ans] in
              let ans = ans+1 in
              let (G_pp, R_xx, randG) = 
                 op_doAnswer CV_l C_v C_gg R_t G_g G_a G_b G_pp R_xx randG
                     false true true answers.[ans] in
              (G_pp, R_xx, randG, ans+1) 
            else (G_pp, R_xx, randG, ans) in
          if (G_b = CV_l) then
            let (G_pp, R_xx, randG) = 
              op_doAnswer CV_l C_v C_gg R_t G_g G_a G_b G_pp R_xx randG
                   false false true answers.[ans] in
            let ans = ans+1 in
            let (G_pp, R_xx, randG) = 
               op_doAnswer CV_l C_v C_gg R_t G_g G_a G_b G_pp R_xx randG
                   true true true answers.[ans] in
            (G_pp, R_xx, randG, ans+1) 
          else (G_pp, R_xx, randG, ans).

    op op_encrypt_body (Dkc_r : (int * bool, DKCSecurity.W.word) map)
          (Dkc_kpub : (int * bool, DKCSecurity.W.word) map)
          (Dkc_ksec : DKCSecurity.W.word) (Dkc_b : bool)
          (qs: query array)           
          (i:int) (st:answer array * DKCSecurity.W.word set) = 
      let (answers, Dkc_used) = st in
      let (Dkc_used, e) = Dkc_encrypt Dkc_r Dkc_kpub Dkc_ksec Dkc_b Dkc_used qs.[i] in
      (answers.[i <- e], Dkc_used).

   lemma range_op_encrypt dkc_r dkc_k dkc_ksec dkc_b qs accu :
         length (range 0 (length qs) accu 
                   (op_encrypt_body dkc_r dkc_k dkc_ksec dkc_b qs)).`1 = 
         length accu.`1.
   proof.
      pose len := length qs.
      cut : forall i, 0 <= i =>
        forall accu, i <= len =>
         length (range (len - i) len accu (op_encrypt_body dkc_r dkc_k dkc_ksec dkc_b qs)).`1 = 
         length accu.`1.
       move=>{accu};elim /Induction.induction. 
       move=> accu; rewrite range_base //.
       move=> i Hi Hrec accu Hlen; rewrite range_ind. smt.
       cut -> : len - (i + 1) + 1 = len - i by smt.
       rewrite (Hrec (op_encrypt_body dkc_r dkc_k dkc_ksec dkc_b qs (len - (i + 1)) accu) _).
        smt.
       rewrite /op_encrypt_body; elim accu => x1 x2 /=.
       by elim (Dkc_encrypt _ _ _ _ _ _) => /= x3 x4;rewrite length_set.
     move=> H; rewrite -(H len _ accu _)=> //. smt.
   qed.

   lemma op_coreNAda_post_body_app (n g:int) pp2 xx2 yy2 aa bb a v gg t aws aws':
     n <= g =>
     let tu = 
       range n g (pp2, xx2, yy2, 0) (op_coreNAda_post_body aa bb a v gg t aws) in
     tu.`4 = length aws =>
     tu = range n g (pp2, xx2, yy2, 0) (op_coreNAda_post_body aa bb a v gg t (aws || aws')).
   proof.
     cut : 
      forall i, 0 <= i =>
        let tu = 
           range n (n+i) (pp2, xx2, yy2, 0) (op_coreNAda_post_body aa bb a v gg t aws) in
        tu.`4 <= length aws =>
        0 <= tu.`4 /\
        tu = range n (n+i) (pp2,xx2,yy2,0) (op_coreNAda_post_body aa bb a v gg t (aws||aws')).
       elim /Induction.induction => /=.
       by rewrite !range_base.
       move=> i Hi Hrec.
       cut Hlt : n < n + (i + 1) by smt.
       cut Heq : (n + (i + 1)) - 1 = n + i by smt.
       rewrite !(range_ind_lazy n (n + (i + 1)))// Heq.
       rewrite {1 2 3 4}/op_coreNAda_post_body.
       move: Hrec; elim (range _ _ _ _) => /= x1 x2 x3 x4.
       elim (range _ _ _ _) => /= x1' x2' x3' x4' Hrec.
       pose do1 := op_doAnswer _ _ _ _ _ _ _ _ _ _ _ _ _ _.    
       case {-1}do1 (Logic.eq_refl do1);rewrite /do1 => {do1} u1 u2 u3 Heq1 /=.
       pose do2 := op_doAnswer _ _ _ _ _ _ _ _ _ _ _ _ _ _.
       case {-1}do2 (Logic.eq_refl do2);rewrite /do2 => {do2} v1 v2 v3 Heq2 /=.
       case (aa.[n + i] = a) => _.
       pose do3 := op_doAnswer _ _ _ _ _ _ _ _ _ _ _ _ _ _.
       case {-1}do3 (Logic.eq_refl do3);rewrite /do3 => {do3} t1 t2 t3 Heq3 /=.
       pose do4 := op_doAnswer _ _ _ _ _ _ _ _ _ _ _ _ _ _.
       case {-1}do4 (Logic.eq_refl do4);rewrite /do4 => {do4} w1 w2 w3 Heq4 /=.
       case (bb.[n + i] = a) => _ Hle; (elim (Hrec _);first by smt);(progress;first by smt).
         rewrite get_append_left;first by smt.
         rewrite Heq1 /= get_append_left;first by smt.
         rewrite Heq2 /= get_append_left;first by smt.
         rewrite Heq3 /= get_append_left;first by smt.
         by rewrite Heq4 //.
       rewrite get_append_left;first by smt.
       rewrite Heq1 /= get_append_left;first by smt.
       by rewrite Heq2.
       pose do3 := op_doAnswer _ _ _ _ _ _ _ _ _ _ _ _ _ _.
       case {-1}do3 (Logic.eq_refl do3);rewrite /do3 => {do3} t1 t2 t3 Heq3 /=.
       pose do4 := op_doAnswer _ _ _ _ _ _ _ _ _ _ _ _ _ _.
       case {-1}do4 (Logic.eq_refl do4);rewrite /do4 => {do4} w1 w2 w3 Heq4 /=.
       case (bb.[n + i] = a) => _ Hle.
         (elim (Hrec _);first by smt);(progress;first by smt).
         rewrite get_append_left;first by smt.
         rewrite Heq3 /= get_append_left;first by smt.
         by rewrite Heq4 //.
       elim (Hrec Hle) => //.
     move=> /= Hg Hle Heq. timeout 10. move: (Hg (g - n) _);smt.
timeout 3.
   qed.

    lemma op_encrypt_body_app dkc_r dkc_k dkc_ksec dkc_b (used:DKCSecurity.W.word set) qs qs'
          (aws aws': _ array):
       length aws = length qs =>
       let r = 
          range 0 (length qs) (aws, used) (op_encrypt_body dkc_r dkc_k dkc_ksec dkc_b qs) in
       range 0 (length qs) (aws || aws', used) 
         (op_encrypt_body dkc_r dkc_k dkc_ksec dkc_b (qs || qs')) = 
             (r.`1 || aws', r.`2).
    proof.
      intros Hlen.
      pose len := length qs.
      cut : forall i, 0 <= i =>
        forall aws used, length aws = len => i <= len =>
        let r =
          range (len - i) len (aws, used)
          (op_encrypt_body dkc_r dkc_k dkc_ksec dkc_b qs) in
        range (len - i) len (aws || aws', used)
          (op_encrypt_body dkc_r dkc_k dkc_ksec dkc_b (qs || qs')) =
        (r.`1 || aws', r.`2).
        move=> i;elim /Induction.induction i.
        move=> _ _; cut -> :len - 0 = len by smt. 
        by rewrite range_base //= range_base.
        move=> i Hi /= Hrec aws0 u0 Haws0 Hil. rewrite range_ind. smt.
        rewrite (range_ind  (len - (i + 1))). smt.
        cut -> : len - (i + 1) + 1 = len - i by smt.         
        rewrite {1 3 5}/op_encrypt_body /=.
        rewrite get_append_left. smt.
        elim (Dkc_encrypt _ _ _ _ _ _) => x1 x2 /=. 
        rewrite set_append_left.
        timeout 10.
          smt.
        timeout 3.
          (* bug de apply ? *)
        cut -> := Hrec (aws0.[len - (i + 1) <- x2]) x1 _ _. smt. smt.
        move => //.
      move => /= H. apply (H len _ aws used _ _); smt.
    qed.

    local lemma Dck_encrypt_spec (q0:query) used0 Dkc_r Dkc_kpub : 
        hoare [ DkcE.encrypt : 
                q = q0 /\ DKCm.used = used0 /\ DKCm.r = Dkc_r /\ DKCm.kpub = Dkc_kpub /\
                in_dom q.`1 DKCm.r /\ in_dom q.`2 DKCm.r /\
                in_dom q.`1 DKCm.kpub /\ in_dom q.`2 DKCm.kpub ==>
                (DKCm.used, res) =  
                   Dkc_encrypt Dkc_r Dkc_kpub DKCm.ksec DKCm.b used0 q0 /\
                DKCm.r = Dkc_r /\ DKCm.kpub = Dkc_kpub ].
     proof.  
       proc. sp.
       if. 
        inline Dkc.get_k Dkc.get_r.
        rcondf 4; first by auto.
        rcondf 7; first by auto.
        rcondf 10; first by auto.
        auto;progress.
        rewrite /Dkc_encrypt /= H3 H4 /=; elim (if _ then _ else _) => //.
      auto;progress.
      by move: H3; rewrite /Dkc_encrypt /= -neqF => ->.
    qed.

    local lemma Dck_encrypt_specL (q0:query) used0 Dkc_r Dkc_kpub : 
        phoare [ DkcE.encrypt : 
                q = q0 /\ DKCm.used = used0 /\ DKCm.r = Dkc_r /\ DKCm.kpub = Dkc_kpub /\
                in_dom q.`1 DKCm.r /\ in_dom q.`2 DKCm.r /\
                in_dom q.`1 DKCm.kpub /\ in_dom q.`2 DKCm.kpub ==>
                (DKCm.used, res) =  
                   Dkc_encrypt Dkc_r Dkc_kpub DKCm.ksec DKCm.b used0 q0 /\
                DKCm.r = Dkc_r /\ DKCm.kpub = Dkc_kpub ] = 1%r.
    proof.    
      conseq (_: true ==> true) (Dck_encrypt_spec q0 used0 Dkc_r Dkc_kpub) => //.
      proc. 
      seq 2: true 1%r 1%r 0%r _ (q = (i,j,pos,t))=> //; first 2 by auto=> &hr; elim (q{hr}).
      by if=> //; wp; call get_rL=> //; do !(call get_kL=> //); wp.
    qed.

    local lemma op_doAnswer_spec
           (A<:EncSecurity.Adv_IND_t) G_pp R_xx randG rand0 alpha0 bet0 ans0 :
       hoare [RedComon(A).doAnswer : 
                 G.pp = G_pp /\ R.xx = R_xx /\ G.randG = randG /\ 
                 rand = rand0 /\ alpha = alpha0 /\ bet = bet0 /\ ans = ans0 ==>
                 (G.pp, R.xx, G.randG)  =
                    op_doAnswer CV.l C.v C.gg R.t G.g G.a G.b G_pp R_xx randG
                     rand0 alpha0 bet0 ans0].
    proof. 
      proc;auto; rewrite /op_doAnswer;progress; elim (ans{hr}) => /=;progress.
    qed.

    local lemma op_doAnswer_specL
       (A<:EncSecurity.Adv_IND_t) G_pp R_xx randG rand0 alpha0 bet0 ans0 :
       phoare [RedComon(A).doAnswer : 
                 G.pp = G_pp /\ R.xx = R_xx /\ G.randG = randG /\
                 rand = rand0 /\ alpha = alpha0 /\ bet = bet0 /\ ans = ans0 ==>
                 (G.pp, R.xx, G.randG) =
                    op_doAnswer CV.l C.v C.gg R.t G.g G.a G.b G_pp R_xx randG
                     rand0 alpha0 bet0 ans0] = 1%r.
    proof. 
      conseq (RedI_Ada_doAnswerL A Dkc) 
        (op_doAnswer_spec A G_pp R_xx randG rand0 alpha0 bet0 ans0).
    qed.

    module type Red_t (A:EncSecurity.Adv_IND_t) = {
      proc preInit(): unit {}
      proc gen_queries(info:bool): query array {A.gen_query}
      proc get_challenge(answers:answer array): bool {A.get_challenge}
    }.

    module RedI(A:EncSecurity.Adv_IND_t) : Red_t(A) = {
      module Com = RedComon(A)
      proc preInit(): unit = {Com.preInit();}
      proc gen_queries(info:bool): query array = {
        var qs;
        Com.pre(info);
        qs = Com.coreNAda_pre();
        return qs;
      }
      proc get_challenge(answers:answer array): bool = {
        var r : bool;
        Com.coreNAda_post(answers);
        r = Com.post();
        return r;
      }
   }.

   local equiv genQuery_spec (A <:EncSecurity.Adv_IND_t {G, R, C, RedComon, Dkc}) 
        dkc_r dkc_k:
      RedComon(A).genQuery ~ RedComon(A).genQuery : 
        ={alpha, bet, rand, DKCm.r, DKCm.b, DKCm.ksec, DKCm.kpub,
              G.g, G.a, G.yy, G.b,
              CV.l, C.v, C.bb, C.q, C.n, C.f, C.x, C.m, C.aa, C.gg, 
              R.t, glob A, RedComon.c, RedComon.query} /\
              dkc_r = DKCm.r{2} /\ dkc_k = DKCm.kpub{2} /\
              (G.a = C.aa.[G.g] /\ G.b = C.bb.[G.g]){1} /\
              (C.n <= G.g < C.n + C.q){1} /\
              (C.n + C.q){1} <= maxGates /\
              (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) dkc_r) /\
              (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) dkc_k) /\
              validCircuitP bound ((C.n{1}, C.m{1}, C.q{1}, C.aa{1}, C.bb{1}), C.gg{1}) ==>
        ={res} /\
         in_dom res{2}.`1 dkc_r /\ in_dom res{2}.`2 dkc_r /\
         in_dom res{2}.`1 dkc_k /\ in_dom res{2}.`2 dkc_k.
   proof.
     proc. wp;rnd;wp;skip;progress [-split].
     move: H4;rewrite /validCircuitP /fst /snd /=;progress [-split].
     elim (H12 G.g{2} _) => //.
     progress [-split].
timeout 10.
     cut : 0 <= C.bb{2}.[G.g{2}] < 2*maxGates by smt.
     cut : 0 <= G.g{2} + C.n{2} + C.q{2} < 2*maxGates by smt.
     cut : 0 <= G.g{2} < 2*maxGates by smt.
     cut : 0 <= C.aa{2}.[G.g{2}] < 2*maxGates by smt.
     progress;(apply H2 || apply H3) => //.
timeout 3.
   qed.

   local equiv Int1 (A<:EncSecurity.Adv_IND_t {G, R, C, RedComon, Dkc}) : 
        DKCSecurity.GameAda(Dkc,RedI_Ada(A)).main ~ 
        DKCSecurity.Game(Dkc,RedI(A)).main :
           ={glob G, glob C, glob R, glob A, glob RedComon}  ==> ={res}.
   proof.
     transitivity DKCSecurity.GameAda(DkcE,RedI_Ada(A)).main 
         (={glob RedI_Ada(A)}  ==> ={res})
         (={glob G, glob C, glob R, glob A, glob RedComon}  ==> ={res}) => //.
       progress. exists (glob A){2}, R.t{2}, R.xx{2}, C.gg{2}, C.aa{2}, C.m{2},
        C.x{2}, C.f{2}, C.n{2}, C.q{2}, C.bb{2}, C.v{2}, G.b{2}, G.randG{2}, G.pp{2},
        G.yy{2}, G.a{2}, G.g{2}, CV.l{2}, RedComon.query{2}, RedComon.c{2} => //.
     conseq (GameAdaEager (RedI_Ada(A))) => //.
     transitivity DKCSecurity.Game(DkcE,RedI(A)).main 
         (={glob G, glob C, glob R, glob A, glob RedComon}  ==> ={res})
         (={glob G, glob C, glob R, glob A, glob RedComon}  ==> ={res}) => //;
       last by symmetry;conseq (GameEager (RedI(A))).
       progress. exists (glob A){2}, R.t{2}, R.xx{2}, C.gg{2}, C.aa{2}, C.m{2},
        C.x{2}, C.f{2}, C.n{2}, C.q{2}, C.bb{2}, C.v{2}, G.b{2}, G.randG{2}, G.pp{2},
        G.yy{2}, G.a{2}, G.g{2}, CV.l{2}, RedComon.query{2}, RedComon.c{2} => //.
     proc.
     inline GameAda(DkcE, RedI_Ada(A)).preInit.
     inline DKCSecurity.Game(DkcE, RedI(A)).preInit.
     inline GameAda(DkcE, RedI_Ada(A)).work DKCSecurity.Game(DkcE, RedI(A)).work.
     inline DkcE.initialize.
     inline DkcE.preInit.
     seq 9 9 : (={info, glob DKCm, glob G, glob C, glob R, glob A, glob RedComon} /\
         (forall k v, 0 <= k < 2*maxGates => in_dom (k,v) DKCm.r{1}) /\
         (forall k v, 0 <= k < 2*maxGates => in_dom (k,v) DKCm.kpub{1})).
       by wp;call DkcE_resample;inline *;auto.
     inline DkcE.get_challenge
       GameAda(DkcE, RedI_Ada(A)).A.work  RedI(A).gen_queries;wp.

     seq 2 2 : (={info0, glob DKCm, glob G, glob C, glob R, glob A, glob RedComon} /\
         (forall k v, 0 <= k < 2*maxGates => in_dom (k,v) DKCm.r{1}) /\
         (forall k v, 0 <= k < 2*maxGates => in_dom (k,v) DKCm.kpub{1}) /\
         (EncSecurity.queryValid_IND RedComon.query{1} =>
           (C.n{1}+C.q{1} <= maxGates /\
           validCircuitP bound ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1}))).
       inline RedI_Ada(A, DkcE).Com.pre Cf.init Rf2.initT;wp.
       while (={C.n,C.q,useVisible, C.v, C.m, R.t} /\ i0{1} = i1{2});first by auto.
       wp; while (={C.n, C.q,C.v, p0, C.x} /\ i{1} = i0{2});auto.
       call (_:true);auto;progress [-split] => /=.
         split => //=. intros _;split => //=.
         progress [-split] => /=. split => //=.
         progress [-split] => /=. split => //=. split => //=. 
         move=> {H3} {H4} {H2} {H5}; rewrite -valid_wireinput.
         move: result_R=> [ [ [[x1 x2 x3 x4 x5 ] f0] f1] 
                          [ [[x1' x2' x3' x4' x5' ] f0'] f1']].
         rewrite /EncSecurity.queryValid_IND /EncSecurity.Encryption.valid_plain.
         rewrite /validInputs /C2.validInputs;simplify fst snd.
         case cL;progress.

     inline RedI(A).get_challenge RedI_Ada(A, DkcE).Com.post.
     inline DKCSecurity.Game(DkcE, RedI(A)).B.encrypt.
     case (EncSecurity.queryValid_IND RedComon.query{1});first last.
       rcondf{1} 4; first by intros &m; conseq *(_:_ ==> true).
       rcondf{2} 13; first by intros &m; conseq *(_:_ ==> true). 
       wp;rnd.
       conseq * (_: _ ==> true) => //.
       call{1} (RedComon_garble_ll A). call{2} (RedComon_garble_ll A).
       call{1} (RedComon_sampleTokens_ll A). call{2} (RedComon_sampleTokens_ll A).
timeout 10.
       call{1} (RedI_Ada_coreAdaL A DkcE _); first by proc; inline *; sp; if; auto; smt.
timeout 3.
       call{2} (RedComon_coreNAda_post_ll A);wp.
       while{2} true ((n - i0){2}).
         move=> &m z;wp. 
         call (_:true ==> true).
          proc.
        seq 2: true 1%r 1%r 0%r _ (q = (i,j,pos,t))=> //; 
             first 2 by auto=> &hr; elim (q{hr}).
        by if=> //; wp; call get_rL=> //; do !(call get_kL=> //); wp.
        auto;progress;smt.
       wp; call{2} (RedComon_coreNAda_pre_ll A).
       by auto; smt.

     rcondt{1} 4; first by intros &m; conseq *(_:_ ==> true).
     rcondt{2} 13; first by intros &m; conseq *(_:_ ==> true).    
     seq 1 10 : (={ glob DKCm, glob G, glob C, glob R, glob A, glob RedComon});first last.
      sim true (={ glob DKCm, glob G, glob C, glob R, glob A, glob RedComon}) : 
          (={DKCm.b} /\ r0{1} = advChallenge{2}) => //;sim.
     inline RedI_Ada(A, DkcE).coreAda RedI(A).Com.coreNAda_pre.
     conseq ( _ :  
              ={glob DKCm, glob G, glob C, glob R, glob A, glob RedComon} /\
              (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) DKCm.r{1}) /\
              (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) DKCm.kpub{1}) /\
        (C.n{1}+C.q{1} <= maxGates /\ 
          validCircuitP bound ((C.n{1},C.m{1},C.q{1},C.aa{1},C.bb{1}),C.gg{1})) ==> _) => /=.
       progress [-split]. rewrite H1 //.
     inline  RedI(A).Com.coreNAda_post.
     exists* DKCm.used{2}, DKCm.r{2}, DKCm.kpub{2}; elim* =>dkc_used dkc_r dkc_k.
     exists* R.xx{2}; elim * => xx2.

     (* doAnswers *)
     while{2} (* Virer les =, ajouter C.aa[g] < C.bb[g] *)
        ( (C.n <= G.g <= C.n + C.q){2} /\
          (G.g = C.n + C.q => (G.a = C.aa.[G.g-1] /\ G.b = C.bb.[G.g-1])){2} /\
          (forall k, C.n <= k < C.n + C.q => C.aa.[k] < C.bb.[k]){2} /\
          answers2{2} = fst (
             let len = Array.length qs1 in
             ForLoop.range 0 len (Array.make len bad, dkc_used)
                (op_encrypt_body dkc_r dkc_k DKCm.ksec DKCm.b qs1)){2} /\
          (G.pp,R.xx,G.randG, ans){2} = 
             (ForLoop.range C.n G.g (FMap.empty, xx2, FMap.empty, 0)
                (op_coreNAda_post_body C.aa C.bb CV.l C.v C.gg R.t answers2)){2})
       (C.n + C.q - G.g){2}.
        intros &m z;wp;sp. elim * => _b _a. 
        exists* C.aa, C.bb, CV.l, C.v, C.gg, R.t, G.g, G.pp, R.xx, G.randG, answers2, ans; 
           elim * => aa0 bb0 l0 v0 gg0 t0 g0 pp0 xx0 rG0 aw0 ans0.
        if. 
          rcondf 5. timeout 10.
conseq * (_: _ ==> true) => //;progress;smt.
timeout 3.
          pose st1 := 
            op_doAnswer l0 v0 gg0 t0 g0 aa0.[g0] bb0.[g0] pp0 xx0 rG0
              false true false aw0.[ans0].
          wp;call (op_doAnswer_specL A st1.`1 st1.`2 st1.`3 false true true aw0.[ans0 + 1]).
          wp;call (op_doAnswer_specL A pp0 xx0 rG0 false true false aw0.[ans0]);skip.
           progress [-split]. 
           rewrite /st1 -H5 /=. 
           timeout 10.
           (progress;first 4 smt);last smt.
           timeout 3.
           rewrite range_ind_lazy. smt.
           move: H3. cut -> : (G.g{hr} + 1 - 1) = G.g{hr} by smt.
           elim (range _ _ _ _) => //. 
           intros x1 x2 x3 x4 <-; rewrite /op_coreNAda_post_body /= -H5.
           timeout 10.
           cut -> : (C.bb{hr}.[G.g{hr}] = C.aa{hr}.[G.g{hr}]) = false by smt.
           timeout 3.
           by rewrite /= -H6.
         if.
          pose st1 := 
            op_doAnswer l0 v0 gg0 t0 g0 aa0.[g0] bb0.[g0] pp0 xx0 rG0
              false false true aw0.[ans0].
          wp;call (op_doAnswer_specL A st1.`1 st1.`2 st1.`3 true true true aw0.[ans0 + 1]).
          wp;call (op_doAnswer_specL A pp0 xx0 rG0 false false true aw0.[ans0]);skip.
           progress [-split]. 
           rewrite /st1 -H6 /=. 
           timeout 10.
           (progress;first 4 smt);last smt.
           timeout 3.
           rewrite range_ind_lazy. smt.
           move: H3. cut -> : (G.g{hr} + 1 - 1) = G.g{hr} by smt.
           elim (range _ _ _ _) => //. 
           intros x1 x2 x3 x4 <-; rewrite /op_coreNAda_post_body /=. 
           cut -> : (C.aa{hr}.[G.g{hr}] = C.bb{hr}.[G.g{hr}]) = false by smt.
           by rewrite /= -H6 /= -H7.
           timeout 10.
         (skip;progress;first 4 smt);last smt.
         timeout 3.
         rewrite range_ind_lazy. smt.
         move: H3. cut -> : (G.g{hr} + 1 - 1) = G.g{hr} by smt.
           elim (range _ _ _ _) => //. 
timeout 20.
           intros x1 x2 x3 x4 <-; rewrite /op_coreNAda_post_body /=; smt.
timeout 3.

     (* doEncrypt *)
     wp. while{2} 
           ( DKCm.r{2} = dkc_r /\ DKCm.kpub{2} = dkc_k /\ 
             n{2} = Array.length qs0{2} /\
             0 <= i0{2} <= n{2} /\
            (answers1, DKCm.used){2} = 
               (ForLoop.range 0 i0 (Array.make n{2} bad, dkc_used)
                   (op_encrypt_body dkc_r dkc_k DKCm.ksec DKCm.b qs0)){2} /\
            (forall k, 0 <= k < n{2} =>  
               in_dom qs0.[k].`1 dkc_r /\ in_dom qs0.[k].`2 dkc_r /\
               in_dom qs0.[k].`1 dkc_k /\ in_dom qs0.[k].`2 dkc_k){2})
          (n - i0){2}.
      intros &m z.
      exists * qs0, DKCm.used, i0; elim * => qs00 used0 i00. 
      wp;call (Dck_encrypt_specL qs00.[i00] used0 dkc_r dkc_k);skip.
      rewrite /op_encrypt_body; progress[-split].
      split. apply (H2 i0{hr}) => //.
      timeout 10.
      (progress; first 2 smt); last smt. 
      timeout 3.
      rewrite range_ind_lazy. smt.
      cut -> :  (i0{hr} + 1 - 1 = i0{hr}). smt.
      by rewrite -H1 /= -H8.
    
     (* doGenQueries *)
     wp;while 
      ( (C.n <= G.g <= C.n + C.q){2} /\
        ={DKCm.r, DKCm.b, DKCm.ksec, DKCm.kpub,
          G.g, G.a, G.yy, G.b,
          CV.l, C.v, C.bb, C.q, C.n, C.f, C.x, C.m, C.aa, C.gg, 
          R.t, glob A, RedComon.c, RedComon.query} /\
          dkc_r = DKCm.r{2} /\ dkc_k = DKCm.kpub{2} /\
          (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) dkc_r) /\
          (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) dkc_k) /\
          C.n{1} + C.q{1} <= maxGates /\
          validCircuitP bound ((C.n{1}, C.m{1}, C.q{1}, C.aa{1}, C.bb{1}), C.gg{1}) /\
          (G.g = C.n + C.q => (G.a = C.aa.[G.g-1] /\ G.b = C.bb.[G.g-1])){1} /\
          (forall k, C.n <= k < C.n + C.q => C.aa.[k] < C.bb.[k]){2} /\
       let len = Array.length qs1{2} in
       (forall k, 0 <= k < len =>  
               in_dom qs1.[k].`1 dkc_r /\ in_dom qs1.[k].`2 dkc_r /\
               in_dom qs1.[k].`1 dkc_k /\ in_dom qs1.[k].`2 dkc_k){2} /\
       let aw =  
          ForLoop.range 0 len (Array.make len bad, DKCm.used{2})
             (op_encrypt_body dkc_r dkc_k DKCm.ksec DKCm.b qs1){2} in
       DKCm.used{1} = aw.`2 /\
       (G.pp,R.xx,G.randG, len){1} = 
           (ForLoop.range C.n G.g (FMap.empty, xx2, FMap.empty, 0)
             (op_coreNAda_post_body C.aa C.bb CV.l C.v C.gg R.t aw.`1)){2}).

        wp;sp. elim * => _bR _aR _bL _aL. 
        if => //. 
          timeout 10.
          rcondf{1} 3. intros &m;conseq * (_: _ ==> true) => //;progress;smt.
          rcondf{2} 4. intros &m;conseq * (_: _ ==> true) => //;progress;smt.
          timeout 3.
          inline RedI_Ada(A, DkcE).query.
          swap{1} [5..6] 4.
          seq 8 2 :
          ( (C.n <= G.g < C.n + C.q){2} /\
            rand{1} = false /\ alpha{1} = true /\ bet{1} = false /\
            rand0{1} = false /\ alpha0{1} = true /\ bet0{1} = true /\
            q0{1} = q1{2} /\ q1{1} = q2{2} /\
            ={DKCm.r, DKCm.b, DKCm.ksec, DKCm.kpub,
              G.g, G.a, G.yy, G.b,
              CV.l, C.v, C.bb, C.q, C.n, C.f, C.x, C.m, C.aa, C.gg, 
              R.t, glob A, RedComon.c, RedComon.query} /\
              dkc_r = DKCm.r{2} /\ dkc_k = DKCm.kpub{2} /\
              (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) dkc_r) /\
              (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) dkc_k) /\
              C.n{1} + C.q{1} <= maxGates /\
              validCircuitP bound ((C.n{1}, C.m{1}, C.q{1}, C.aa{1}, C.bb{1}), C.gg{1}) /\
              (forall k, C.n <= k < C.n + C.q => C.aa.[k] < C.bb.[k]){2} /\
              (G.a = C.aa.[G.g]){1} /\
              (G.b = C.bb.[G.g]){1} /\
              (G.g < C.n + C.q){1} /\ 
              G.a{1} = CV.l{1} /\
              (let len = Array.length qs1{2} in
               (forall k, 0 <= k < len =>  
               in_dom qs1.[k].`1 dkc_r /\ in_dom qs1.[k].`2 dkc_r /\
               in_dom qs1.[k].`1 dkc_k /\ in_dom qs1.[k].`2 dkc_k){2} /\
               let aw =  
                 ForLoop.range 0 len (Array.make len bad, DKCm.used{2})
                     (op_encrypt_body dkc_r dkc_k DKCm.ksec DKCm.b qs1){2} in
               DKCm.used{1} = aw.`2 /\
               (G.pp,R.xx,G.randG, len){1} = 
               (ForLoop.range C.n G.g (FMap.empty, xx2, FMap.empty, 0)
                (op_coreNAda_post_body C.aa C.bb CV.l C.v C.gg R.t aw.`1)){2}) /\
           (in_dom q1.`1 dkc_r /\ in_dom q1.`2 dkc_r /\
            in_dom q1.`1 dkc_k /\ in_dom q1.`2 dkc_k){2} /\
           (in_dom q2.`1 dkc_r /\ in_dom q2.`2 dkc_r /\
            in_dom q2.`1 dkc_k /\ in_dom q2.`2 dkc_k){2}).
       call (genQuery_spec A dkc_r dkc_k); auto;progress. 
       call (genQuery_spec A dkc_r dkc_k); auto;progress. 
       wp.
       exists* C.aa{1}, C.bb{1}, CV.l{1}, C.v{1}, C.gg{1}, R.t{1}, G.g{1}, G.a{1}, G.b{1}.
         elim * => aa1 bb1 l1 v1 gg1 t1 g1 a1 b1.
       exists* G.pp{1}, R.xx{1}, G.randG{1}, DKCm.used{1}, q0{1}, q1{1}, DKCm.ksec{1},
           DKCm.b{1};
          elim* => pp1 xx1 yy1 used1 q01 q11 ks1 mb1.
       pose st1 := Dkc_encrypt dkc_r dkc_k ks1 mb1 used1 q01.
       pose st2 := op_doAnswer l1 v1 gg1 t1 g1 a1 b1 pp1 xx1 yy1 false true false st1.`2. 
       pose st3 := Dkc_encrypt dkc_r dkc_k ks1 mb1 st1.`1 q11.
       call{1} (op_doAnswer_specL A st2.`1 st2.`2 st2.`3 false true true st3.`2).
       call{1} (Dck_encrypt_specL q11 st1.`1 dkc_r dkc_k).
       wp;call{1} (op_doAnswer_specL A pp1 xx1 yy1 false true false st1.`2).
       call{1} (Dck_encrypt_specL q01 used1 dkc_r dkc_k);skip.
       progress [-split]. 
       rewrite H9 H10 H11 H12 /=;progress [-split].
       rewrite /st1 -H17;progress [-split].
       rewrite H13 H14 H15 H16;progress [-split].
       rewrite H4 /st3 /st2 /st1 -H17 /= -H18 /= -H19;progress [-split].
       timeout 10.
       split;first smt. split => //. split=> //. split => //. split. smt.
       timeout 3.
       split => //. split.
       intros k Hk; rewrite get_append //. 
       move: Hk; rewrite length_append length_cons length_cons length_empty => Hk.
       case (0 <= k < length qs1{2}) => //=. apply (H7 k).
       timeout 10.
       move=>_; rewrite get_cons. smt.
       case (0 = k - length qs1{2}) => //.
       move=>_; rewrite get_cons. smt.
       case (0 = k - length qs1{2} - 1) => //. smt.
       rewrite length_append range_add. smt. smt. smt.
       rewrite make_append.
       move: op_encrypt_body_app => /= ->. smt. 
       rewrite (range_ind (length qs1{2})). smt.
       rewrite (range_ind (length qs1{2} + 1)). smt.
       rewrite (range_base (length qs1{2} + 1 + 1)). smt.
       rewrite /op_encrypt_body /op_encrypt_body.
       rewrite (range_ind_lazy C.n{2}). smt.
       rewrite get_append_right. smt.
       rewrite get_append_right. smt.
       rewrite get_cons /=. smt.
       cut -> : length qs1{2} + 1 - length qs1{2} = 1 by smt.
       rewrite get_cons /=. smt.
       rewrite get_cons /=. smt.
       rewrite -H17 /= -H19 /=.
       cut := range_op_encrypt DKCm.r{2} DKCm.kpub{2} DKCm.ksec{2} DKCm.b{2} qs1{2}
                      (make (length qs1{2}) bad, DKCm.used{2}) => /=.
       rewrite length_make 1:smt => Hlen.
       rewrite set_append_right Hlen. smt.
       rewrite set_append_right Hlen. smt. 
       cut -> : length qs1{2} - length qs1{2} = 0 by smt.
       cut -> : length qs1{2} + 1 - length qs1{2} = 1 by smt.
       cut -> : G.g{2} + 1 - 1 = G.g{2} by smt.
       cut := op_coreNAda_post_body_app C.n{2} G.g{2} FMap.empty xx2 FMap.empty  
                C.aa{2} C.bb{2} C.aa{2}.[G.g{2}] C.v{2} C.gg{2} R.t{2}
               ((range 0 (length qs1{2}) (make (length qs1{2}) bad, DKCm.used{2})
                 (op_encrypt_body DKCm.r{2} DKCm.kpub{2} DKCm.ksec{2} DKCm.b{2}
                    qs1{2})).`1)
               (make (length (q1{2} :: (q2{2} :: (Array.empty)%Array))) bad).[0 <-
                 result].[1 <- result0] _ _ => //.
        by rewrite Hlen -H8.
       move=> <-; rewrite -H8 /op_coreNAda_post_body /=.
       rewrite get_append_right. smt.      
       rewrite get_append_right. smt. 
       cut -> : (C.bb{2}.[G.g{2}] = C.aa{2}.[G.g{2}]) = false. smt.
       rewrite Hlen /=.
       rewrite get_set. smt.
       rewrite /= get_set. smt.
       cut -> : length qs1{2} + 1 - length qs1{2} = 1 by smt.
       rewrite /= get_set /=. smt.
       rewrite -H18 /= -H20 /=. smt.
timeout 3.
      if => //.
      inline RedI_Ada(A, DkcE).query.
          swap{1} [5..6] 4.
          seq 8 2 :
          ( (C.n <= G.g < C.n + C.q){2} /\
            rand{1} = false /\ alpha{1} = false /\ bet{1} = true /\
            rand0{1} = true /\ alpha0{1} = true /\ bet0{1} = true /\
            q0{1} = q1{2} /\ q1{1} = q2{2} /\
            ={DKCm.r, DKCm.b, DKCm.ksec, DKCm.kpub,
              G.g, G.a, G.yy, G.b,
              CV.l, C.v, C.bb, C.q, C.n, C.f, C.x, C.m, C.aa, C.gg, 
              R.t, glob A, RedComon.c, RedComon.query} /\
              dkc_r = DKCm.r{2} /\ dkc_k = DKCm.kpub{2} /\
              (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) dkc_r) /\
              (forall (k:int) (v:bool), 0 <= k < 2*maxGates => in_dom (k, v) dkc_k) /\
              C.n{1} + C.q{1} <= maxGates /\
              validCircuitP bound ((C.n{1}, C.m{1}, C.q{1}, C.aa{1}, C.bb{1}), C.gg{1}) /\
              (forall k, C.n <= k < C.n + C.q => C.aa.[k] < C.bb.[k]){2} /\
              (G.a = C.aa.[G.g]){1} /\
              (G.b = C.bb.[G.g]){1} /\
              (G.g < C.n + C.q){1} /\ 
              G.b{1} = CV.l{1} /\
              (let len = Array.length qs1{2} in
               (forall k, 0 <= k < len =>  
               in_dom qs1.[k].`1 dkc_r /\ in_dom qs1.[k].`2 dkc_r /\
               in_dom qs1.[k].`1 dkc_k /\ in_dom qs1.[k].`2 dkc_k){2} /\
               let aw =  
                 ForLoop.range 0 len (Array.make len bad, DKCm.used{2})
                     (op_encrypt_body dkc_r dkc_k DKCm.ksec DKCm.b qs1){2} in
               DKCm.used{1} = aw.`2 /\
               (G.pp,R.xx,G.randG, len){1} = 
               (ForLoop.range C.n G.g (FMap.empty, xx2, FMap.empty, 0)
                (op_coreNAda_post_body C.aa C.bb CV.l C.v C.gg R.t aw.`1)){2}) /\
           (in_dom q1.`1 dkc_r /\ in_dom q1.`2 dkc_r /\
            in_dom q1.`1 dkc_k /\ in_dom q1.`2 dkc_k){2} /\
           (in_dom q2.`1 dkc_r /\ in_dom q2.`2 dkc_r /\
            in_dom q2.`1 dkc_k /\ in_dom q2.`2 dkc_k){2}).
       call (genQuery_spec A dkc_r dkc_k); auto;progress. 
       call (genQuery_spec A dkc_r dkc_k); auto;progress. 
       wp.
       exists* C.aa{1}, C.bb{1}, CV.l{1}, C.v{1}, C.gg{1}, R.t{1}, G.g{1}, G.a{1}, G.b{1}.
         elim * => aa1 bb1 l1 v1 gg1 t1 g1 a1 b1.
       exists* G.pp{1}, R.xx{1}, G.randG{1}, DKCm.used{1}, q0{1}, q1{1}, DKCm.ksec{1},
           DKCm.b{1};
          elim* => pp1 xx1 yy1 used1 q01 q11 ks1 mb1.
       pose st1 := Dkc_encrypt dkc_r dkc_k ks1 mb1 used1 q01.
       pose st2 := op_doAnswer l1 v1 gg1 t1 g1 a1 b1 pp1 xx1 yy1 false false true st1.`2. 
       pose st3 := Dkc_encrypt dkc_r dkc_k ks1 mb1 st1.`1 q11.
       call{1} (op_doAnswer_specL A st2.`1 st2.`2 st2.`3 true true true st3.`2).
       call{1} (Dck_encrypt_specL q11 st1.`1 dkc_r dkc_k).
       wp;call{1} (op_doAnswer_specL A pp1 xx1 yy1 false false true st1.`2).
       call{1} (Dck_encrypt_specL q01 used1 dkc_r dkc_k);skip.
       progress [-split]. 
       rewrite H9 H10 H11 H12 /=;progress [-split].
       rewrite /st1 -H17;progress [-split].
       rewrite H13 H14 H15 H16;progress [-split].
       rewrite H4 /st3 /st2 /st1 -H17 /= -H18 /= -H19;progress [-split].
       timeout 10.
       split;first smt. split => //. split => //. split => //. split. smt.
       split => //. split.
       intros k Hk; rewrite get_append //. 
       move: Hk; rewrite length_append length_cons length_cons length_empty => Hk.
       case (0 <= k < length qs1{2}) => //=. apply (H7 k).
       move=>_; rewrite get_cons. smt.
       case (0 = k - length qs1{2}) => //.
       move=>_; rewrite get_cons. smt.
       case (0 = k - length qs1{2} - 1) => //. smt.
       rewrite length_append range_add. smt. smt. smt.
       rewrite make_append.
       move: op_encrypt_body_app => /= ->. smt. 
       rewrite (range_ind (length qs1{2})). smt.
       rewrite (range_ind (length qs1{2} + 1)). smt.
       rewrite (range_base (length qs1{2} + 1 + 1)). smt.
       rewrite /op_encrypt_body /op_encrypt_body.
       rewrite (range_ind_lazy C.n{2}). smt.
       rewrite get_append_right. smt.
       rewrite get_append_right. smt.
       rewrite get_cons /=. smt.
       cut -> : length qs1{2} + 1 - length qs1{2} = 1 by smt.
       rewrite get_cons /=. smt.
       rewrite get_cons /=. smt.
       rewrite -H17 /= -H19 /=.
       cut := range_op_encrypt DKCm.r{2} DKCm.kpub{2} DKCm.ksec{2} DKCm.b{2} qs1{2}
                      (make (length qs1{2}) bad, DKCm.used{2}) => /=.
       rewrite length_make 1:smt => Hlen.
       rewrite set_append_right Hlen. smt.
       rewrite set_append_right Hlen. smt. 
       cut -> : length qs1{2} - length qs1{2} = 0 by smt.
       cut -> : length qs1{2} + 1 - length qs1{2} = 1 by smt.
       cut -> : G.g{2} + 1 - 1 = G.g{2} by smt.
       cut := op_coreNAda_post_body_app C.n{2} G.g{2} FMap.empty xx2 FMap.empty 
                C.aa{2} C.bb{2} C.bb{2}.[G.g{2}] C.v{2} C.gg{2} R.t{2}
               ((range 0 (length qs1{2}) (make (length qs1{2}) bad, DKCm.used{2})
                 (op_encrypt_body DKCm.r{2} DKCm.kpub{2} DKCm.ksec{2} DKCm.b{2}
                    qs1{2})).`1)
               (make (length (q1{2} :: (q2{2} :: (Array.empty)%Array))) bad).[0 <-
                 result].[1 <- result0] _ _ => //.
        by rewrite Hlen -H8.
       move=> <-; rewrite -H8 /op_coreNAda_post_body /=.
       rewrite get_append_right. smt.      
       rewrite get_append_right. smt. 
       cut -> : (C.aa{2}.[G.g{2}] = C.bb{2}.[G.g{2}]) = false. smt.
       rewrite Hlen /=.
       rewrite get_append_right. smt.
       rewrite get_append_right. smt.
       rewrite Hlen /=.
       rewrite get_set. smt.
       rewrite /= get_set. smt.
       cut -> : length qs1{2} + 1 - length qs1{2} = 1 by smt.
       rewrite /= get_set /=. smt.
       rewrite -H18 /= -H20 /=. smt.

    wp;skip;progress. smt. smt. smt. smt. admit. (* ??????? *)
    (* Pre *)
    wp;skip;progress [-split].
    move:H2; rewrite /validCircuitP /fst /snd /=; progress.
    smt. smt. smt. smt. smt. smt. smt. smt. smt. smt. smt.
    rewrite range_base // make_init //. 
    smt. smt. smt. smt. smt. smt. smt. timeout 20. smt.
    smt. smt. smt. smt. smt. smt. smt. smt. smt. smt. smt. smt.
timeout 3.
  qed.

    local lemma reduction &m:
      Mrplus.sum (fun l, let l = l - 1 in Pr[Hybrid(A).work(l) @ &m : res]) (Interval.interval 0 (bound-1)) -
        Mrplus.sum (fun l, Pr[Hybrid(A).work(l) @ &m : res]) (Interval.interval 0 (bound-1)) =
        (bound)%r * (2%r * Pr[DKCSecurity.Game(Dkc,RedI(A)).main()@ &m:res] - 1%r).
    proof.
      rewrite (reductionAda &m).
      apply (_:forall x y, x=y => (bound)%r * (2%r * x - 1%r) = (bound)%r * (2%r * y - 1%r)).
        smt.
      byequiv (Int1 A)=> //.
    qed.

    local lemma reductionSimplified &m:
      Pr[Hybrid(A).work((-1)) @ &m : res] - Pr[Hybrid(A).work(bound-1) @ &m : res] =
        (bound)%r * (2%r * Pr[DKCSecurity.Game(Dkc,RedI(A)).main()@ &m:res] - 1%r).
    proof.
      rewrite -(reduction &m).
      timeout 10.
      rewrite (Mrplus.sum_rm _ _ 0) /=; first smt.
      rewrite (Mrplus.sum_chind _ (fun (x:int), x - 1) (fun (x:int), x + 1)) /=; first smt.
      rewrite (Interval.dec_interval 0 (bound-1) _); first smt.
      rewrite (Mrplus.sum_rm _ (Interval.interval 0 (bound-1)) (bound-1)) /=; first smt.
      pose s1 := Mrplus.sum _ _.
      pose s2 := Mrplus.sum _ _.
      cut -> : s1 = s2. 
        rewrite /s1 /s2; congr; apply fun_ext => x /=. smt.
      ringeq.      
      timeout 3.
    qed.

    lemma sch_is_ind &m:
      `|2%r * Pr[EncSecurity.Game_IND(Rand,A).main()@ &m:res] - 1%r| =
        2%r * (bound)%r * `|2%r * Pr[DKCSecurity.Game(Dkc,RedI(A)).main()@ &m:res] - 1%r|.
    proof.
      rewrite -(prH0 &m) // -{1}(prHB &m) //=.
      cut -> : forall a b, 2%r * a - 2%r * b = 2%r * (a - b) by smt.
      rewrite (reductionSimplified &m).
      timeout 10.
      cut H: forall (a b:real), 0%r <= a => `| a * b | = a * `| b | by smt.
      rewrite !H=> //; first smt.
      smt.
      timeout 3.
    qed.
  end section.

lemma sch_is_pi: EncSecurity.pi_sampler_works ().
 proof.
 rewrite /EncSecurity.pi_sampler_works=> plain.
 simplify validCircuit C2.validInputs validInputs EncSecurity.Encryption.valid_plain EncSecurity.Encryption.leak EncSecurity.Encryption.pi_sampler pi_sampler C2.pi_sampler phi C2.phi eval C2.eval.
 elim/tuple2_ind plain=> plain p1 p2 hplain.
 elim/tuple2_ind p1 hplain => p1 topo1 x1 hp1.
 elim/tuple5_ind topo1 hp1 => topo1 n m q aa bb htopo hp1 hplain.
 simplify fst snd.
 progress.
     rewrite {1} /GarbleTools.evalComplete /=.
     pose ev := GarbleTools.evalComplete _ _ _.
     timeout 10.
     (apply array_ext;split;first by smt)=> i.
     rewrite Array.length_sub; first 3 smt.
     move=> hi; rewrite !get_sub; first 8 smt.
     rewrite appendInit_get2;first 2 smt.
     rewrite {1} /GarbleTools.extract /=.
     rewrite get_initGates;first smt.
     cut -> : (length p2 <= i + (length p2 + q - m) - 1 + 1 < length p2 + q) = true by smt.
     simplify.
     rewrite oget_some.
     by rewrite get_sub;smt.
     simplify validGG.
     pose {1} k := q.
     cut: k <= q by smt.
     cut: 0 <= k by smt.
     (elim/Induction.induction k;first smt).
     progress.
     rewrite range_ind_lazy /=;first smt.
     cut -> : length p2 + (i + 1) - 1 = length p2 + i by smt.
     rewrite H10 /=; first smt.
     cut ltiq: i < q by smt.
     split; first by apply in_dom_init_Gates; expect 2 smt.
     split; first by apply in_dom_init_Gates; expect 2 smt.
     by split; apply in_dom_init_Gates; expect 4 smt.
     by rewrite length_init; smt.
     timeout 3.
 qed.

module RedS(A : EncSecurity.Adv_SIM_t) = {
  module M = RedI(EncSecurity.RedSI(A))
  proc preInit(): unit = { M.preInit(); }
  proc gen_queries(info:bool): query array = {
    var r: query array;
    r = M.gen_queries(info);
    return r;
  }
  proc get_challenge(answers:answer array): bool = {
    var r: bool;
    r = M.get_challenge(answers);
    return r;
  }
}.

lemma AinitEq : forall (A<:DKCSecurity.Adv_t),
  equiv[A.preInit ~ A.preInit: ={glob A} ==> ={res, glob A}].
proof.
by intros A;proc true.
qed.

lemma Agen_queriesEq : forall (A<:DKCSecurity.Adv_t),
    equiv[A.gen_queries ~ A.gen_queries: ={info, glob A} ==> ={res, glob A}].
proof.
by intros A;proc true.
qed.

lemma Aget_challengeEq : forall (A<:DKCSecurity.Adv_t),
    equiv[A.get_challenge ~ A.get_challenge: ={answers, glob A} ==> ={res, glob A}].
proof.
by intros A;proc true.
qed.

lemma DinitEq : forall (D<:DKCSecurity.Dkc_t),
    equiv[D.preInit ~ D.preInit: true ==> ={res, glob D}].
proof.
by intros D;proc true.
qed.

lemma DinitializeEq : forall (D<:DKCSecurity.Dkc_t),
    equiv[D.initialize ~ D.initialize: ={glob D} ==> ={res, glob D}].
proof.
by intros D;proc true.
qed.

lemma DencryptEq : forall (D<:DKCSecurity.Dkc_t),
    equiv[D.encrypt ~ D.encrypt: ={q, glob D} ==> ={res, glob D}].
proof.
by intros D;proc true.
qed.

lemma Dget_challengeEq : forall (D<:DKCSecurity.Dkc_t),
    equiv[D.get_challenge ~ D.get_challenge: ={glob D} ==> ={res, glob D}].
proof.
by intros D;proc true.
qed.

lemma sch_is_sim (A <: EncSecurity.Adv_SIM_t {Rand, DKCm, RedComon}) &m:
 islossless A.gen_query =>
 islossless A.get_challenge =>
  `|2%r * Pr[EncSecurity.Game_SIM(Rand,EncSecurity.SIM(Rand),A).main()@ &m:res] - 1%r| <=
    2%r * (bound)%r * `|2%r * Pr[DKCSecurity.Game(Dkc,RedI(EncSecurity.RedSI(A))).main()@ &m:res] - 1%r|.
proof.
  move=> ll_ADVp1 ll_ADVp2.
  rewrite -(sch_is_ind (EncSecurity.RedSI(A)) _ _ &m).
    by apply (EncSecurity.RedSIgenL A).
    by apply (EncSecurity.RedSIgetL A).
  apply (EncSecurity.ind_implies_sim Rand A _ _ &m _)=> //.
  by apply sch_is_pi.
qed.
(* END THE SECURITY PROOF *)

end DKCScheme.
