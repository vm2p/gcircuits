require import Option.

require Sch.
require SchSec.

theory SchAuth.

  clone import Sch.Scheme as Scheme.

  module type Rand_t = {
     proc * gen(l : leak_t): rand_t
  }.

  module type Adv_Auth_t = { 
    proc * gen_fun() : fun_t
    proc gen_input(fG : funG_t) : Input.input_t
    proc forge(X : Input.inputG_t) : outputG_t
  }.

  (* We need an adaptive version of Bellare's Authenticity property *)
  module Game_Auth(R: Rand_t, A : Adv_Auth_t) = {
    var fn : fun_t
    var x : Input.input_t
    
    proc main() : bool = {
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r : bool;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      r = validInputs fn x /\ valid_outG oK oG /\ eval fn x <> decode oK oG;

      return r;
    }
  }.


 (* And we need it written in oracle form *)

  (* A module encapsulating the scheme*)
  module type O_AUT'_t = { 
    proc * init(_iK: Input.inputK_t): unit
    proc enc(x : Input.input_t) : Input.inputG_t option
    proc getX() : Input.input_t
  }.

  module O : O_AUT'_t = {
     var queryDone : bool
     var iK : Input.inputK_t
     var _x : Input.input_t

     proc init(_iK : Input.inputK_t) : unit = {
        iK = _iK;
        queryDone = false;
     }
     
     proc enc(x : Input.input_t) : Input.inputG_t option = {
        var rep: Input.inputG_t option;
        var xG : Input.inputG_t;

        _x = x;

        if (queryDone) { rep = None; } else { 
           queryDone = true; 
           xG = Input.encode iK x;
           rep = Some xG; 
        }
        return rep;  
     }

     proc getX() : Input.input_t = {
        return _x;
     }
  }.

  module type Adv_AUT'_t(O : O_AUT'_t) = { 
    proc * gen_fun() : fun_t
    proc forge(fG : funG_t) : outputG_t { O.enc }
  }.


 module Game_AUT'(R: Rand_t, O:O_AUT'_t, ADV : Adv_AUT'_t) = {
    module A = ADV(O)

    var fn : fun_t
    var x : Input.input_t
    
    proc main() : bool = {
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var oG : outputG_t;
      var r : bool;
      var x : Input.input_t;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      iK = inputK fn rg;
      oK = outputK fn rg;
      O.init(iK);
      oG = A.forge(fG);
      x = O.getX();
      r = validInputs fn x /\ valid_outG oK oG /\ eval fn x <> decode oK oG;
      return r;
  }
}.

section.
 module ADV_A(A:Adv_Auth_t,O: O_AUT'_t ) (*: Adv_AUT'_t(O)*) = {
  proc gen_fun() : fun_t = {
    var fn: fun_t;
    fn = A.gen_fun();
    return fn;
  }
  proc forge(fG : funG_t) : outputG_t = {
    var x: input_t;
    var xG: inputG_t option;
    var oG: outputG_t;

    x = A.gen_input(fG);
    xG = O.enc(x);
    oG = A.forge(oget xG);
    return oG;
  }
}.

equiv auth_auth' (A<:Adv_Auth_t {O,Game_Auth,Game_AUT'}) (R<:Rand_t {A,Game_Auth,Game_AUT'}):
 Game_AUT'(R, O, ADV_A(A)).main ~ Game_Auth(R, A).main
 : true ==> ={res}.
proof.
proc; inline O.getX O.enc O.init Game_AUT'(R, O, ADV_A(A)).A.forge Game_AUT'(R, O, ADV_A(A)).A.gen_fun.
wp; call (_: ={glob A, X} ==> ={res}); first by proc true.
wp; call (_: ={glob A, fG} ==> ={res, glob A}); first by proc true.
wp; call (_: ={l} ==> ={res}); first by proc true.
wp; call (_: true ==> ={res, glob A}); first by proc true.
by skip; progress.
qed.

end section.

end SchAuth.

