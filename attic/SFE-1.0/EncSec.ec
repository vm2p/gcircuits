require import Pair.
require import Bool.
require import Real.
require import Distr.

require import Enc.

theory EncSecurity.
  clone import Encryption.

  module type Rand_t = {
     proc gen(l:randgenin): rand
  }.

  (* Definitions needed for IND model *)
  type query_IND = plain * plain.

  op queryValid_IND(query : query_IND) =
     valid_plain (fst query) /\ 
     valid_plain (snd query) /\
     leak (fst query) = leak (snd query).

  module type Adv_IND_t = {
    proc gen_query(): query_IND
    proc get_challenge(cipher:cipher): bool
  }.

  module Game_IND(R:Rand_t, ADV:Adv_IND_t) = {
    proc main(): bool = {
      var query:query_IND;
      var p:plain;
      var c:cipher;
      var b,adv,ret:bool;
      var r:rand;

      b = ${0,1};
      query = ADV.gen_query();
      if (queryValid_IND query)
      {
        p = if b then snd query else fst query;
        r = R.gen(randfeed p);
        c = enc p r;
        adv = ADV.get_challenge(c);
        ret = (b = adv);
      }
      else
        ret = ${0,1};
      return ret;
    }
  }.

  (* Definitions needed for SIM model *)
  type query_SIM = plain.
  op queryValid_SIM(query:query_SIM) =
     valid_plain query.

  pred pi_sampler_works (x:unit) =
    forall (plain : plain),
      valid_plain plain =>
      let leakage = leak plain in
      let pi = pi_sampler leakage in
        leak pi = leakage /\ valid_plain pi.

  module type Adv_SIM_t = {
    proc gen_query():query_SIM
    proc get_challenge(cipher:cipher): bool
  }.

  module type Sim_SIM_t = {
    proc simm(leakage:leakage): cipher
  }.

  module Game_SIM(R:Rand_t, SIM:Sim_SIM_t, ADV:Adv_SIM_t) = {
    proc game(b: bool): bool = {
      var query:query_SIM;
      var c:cipher;
      var b':bool;
      var r:rand;

      query = ADV.gen_query();

      if (!queryValid_SIM query)
         b' = $Dbool.dbool;
      else
      {
        if (b)
        {
          r = R.gen(randfeed query); 
          c = enc query r; 
        }
        else c = SIM.simm(leak query);
        b' = ADV.get_challenge(c);
      }
      return (b = b');
    }
    proc main(): bool = {
      var real, adv: bool;
      real = ${0,1};
      adv = game(real);
      return adv;
    }
  }.

  module SIM(R:Rand_t): Sim_SIM_t = {
    proc simm(leakage:leakage): cipher = {
      var pi:plain;
      var c:cipher;
      var r:rand;

      pi = pi_sampler leakage;
      r = R.gen(randfeed pi);
      c = enc pi r;
      return c;
    }
  }.

 (* Building an IND adversary from a SIM adversary *)
 module RedSI(A:Adv_SIM_t) = {
   proc gen_query():query_IND = {
     var plain0:plain;
     var plain1:plain;
     var leakage0:leakage;

     plain0 = A.gen_query();
     leakage0 = leak plain0;
     plain1 = pi_sampler leakage0;
     return (plain1,plain0);
   }

   proc get_challenge(cipher : cipher) : bool = {
     var answer:bool;

     answer = A.get_challenge(cipher);
     return answer;
   }
 }.

 (* Losslessness properties *)
 lemma RedSIgenL (A <: Adv_SIM_t): islossless A.gen_query => islossless RedSI(A).gen_query.
 proof. by move=> AgenL; proc; wp; call AgenL. qed.

 lemma RedSIgetL (A <: Adv_SIM_t): islossless A.get_challenge => islossless RedSI(A).get_challenge.
 proof. by move=> AgetL; proc; wp; call AgetL. qed.

 section.
 (*******************************************)
 (* IND => SIM proof                        *)
 (*******************************************)
 declare module R:Rand_t.

 declare module A:Adv_SIM_t {R}.
 axiom AgenL: islossless A.gen_query.
 axiom AgetL: islossless A.get_challenge.

 (* The RedSI(A) runs the A in exactly the SIM model environment *)
 local lemma indadv_equiv (A <: Adv_SIM_t{R}):
  pi_sampler_works () =>
  equiv [ Game_IND(R,RedSI(A)).main ~ Game_SIM(R,SIM(R),A).main:
            ={glob R,glob A} ==> ={res} ].
 proof.
 intros=> pi_samplable; proc.
 inline RedSI(A).gen_query Game_SIM(R, SIM(R), A).game RedSI(A).get_challenge
        SIM(R).simm.
 seq 5 3: (={b, glob R, glob A} /\ plain0{1} = query{2} /\ 
           leakage0{1} = leak plain0{1} /\ plain1{1} = pi_sampler leakage0{1} /\ 
           query{1} = (plain1{1}, plain0{1})).
  wp; call (_: true).
  by auto.
 case (queryValid_SIM query{2}); last first.
  rcondf{1} 1; first by move => &m; skip; progress; smt.
  rcondt{2} 1; first by move => &m; skip; smt.
  by wp; rnd ((=)b{2}); skip; progress; smt.
 rcondt{1} 1; first by move => &m; skip; progress; smt.
 rcondf{2} 1; first by move => &m; skip; smt.
 case (b{1}).
  rcondt{2} 1=> //.
  wp; call (_: true).
  wp; call (_: true).
  by auto; progress; smt.
 rcondf{2} 1=> //.
 wp; call (_: true).
 wp; call (_: true).
 by auto; progress; smt.
 qed.

 lemma simadv_indadv &m:
  pi_sampler_works () =>
  Pr[Game_IND(R,RedSI(A)).main()@ &m:res] = Pr[Game_SIM(R,SIM(R),A).main()@ &m:res].
 proof. by move=> pi_samplable; byequiv (indadv_equiv A _). qed.

 lemma ind_implies_sim &m:
  pi_sampler_works () =>
   `|2%r * Pr[Game_SIM(R,SIM(R),A).main()@ &m:res] - 1%r| <=
    `|2%r * Pr[Game_IND(R,RedSI(A)).main()@ &m:res] - 1%r|.
 proof. by move=> pi_samplable; rewrite -(simadv_indadv &m). qed.
end section.

(**********************************)
(** GENERIC ADVANTAGE DEFINITIONS *)
(**********************************)
(** Advantage definition *)
lemma SGame_adv &m (A <: Adv_SIM_t {Game_SIM}) (S <: Sim_SIM_t {A}) (R <: Rand_t):
  islossless A.gen_query =>
  islossless A.get_challenge =>
  islossless R.gen =>
  islossless S.simm =>
  2%r * Pr[Game_SIM(R,S,A).main()  @ &m: res] - 1%r
  = Pr[Game_SIM(R,S,A).game(true)  @ &m: res] 
    - Pr[Game_SIM(R,S,A).game(false)  @ &m: !res].
proof.
  move=> Agen_ll Aget_ll Rgen_ll Ssimm_ll.
  rewrite Pr [mu_not].
  pose p1:= Pr[Game_SIM(R,S,A).game(true) @ &m: res].
  pose p2:= Pr[Game_SIM(R,S,A).game(false) @ &m: res].
  (* lossless condition *)
  cut ->: Pr[Game_SIM(R,S,A).game(false) @ &m : true] = 1%r.
    byphoare (_: true); trivial; proc.
    seq 1: true => //; first by call (_:true).
    case (queryValid_SIM query).
      by rcondf 1=> //; if; do !(wp; call (_: true)); auto.
      by rcondt 1 => //; auto; smt.
  cut Hp1: phoare [Game_SIM(R,S,A).game:
                     (glob Game_SIM(R,S,A)) = (glob Game_SIM(R,S,A)){m} /\ b ==> res] = p1.
    bypr=> &m' [eqG] b'; rewrite /p1 b'.
    by byequiv (_: ={glob Game_SIM(R,S,A), b} ==> ={res}); first by sim; sim.
  cut Hp2: phoare [Game_SIM(R,S,A).game:
                     (glob Game_SIM(R,S,A)) = (glob Game_SIM(R,S,A)){m} /\ !b ==> res] = p2.
    bypr=> &m' [eqG]; rewrite -neqF=> b'; rewrite /p2 b'.
    by byequiv (_: ={glob Game_SIM(R,S,A), b} ==> ={res}); first by sim; sim.
  cut Hp: phoare [Game_SIM(R,S,A).main:
                    (glob Game_SIM(R,S,A)) = (glob Game_SIM(R,S,A)){m} ==> res] = ((p1+p2)/2%r).
    proc.
    seq 1: real (1%r / 2%r) p1 (1%r / 2%r) p2 ((glob Game_SIM(R,S,A)) = (glob Game_SIM(R,S,A)){m}).
      by auto.
      by rnd ((=) true); skip; smt.
      by call Hp1.
      by rnd ((=) false); skip; smt.
      by call Hp2.
      smt.
  cut ->: Pr[Game_SIM(R,S,A).main(tt) @ &m : res] = ((p1+p2)/2%r).
    by byphoare Hp.
  smt.
qed.

end EncSecurity.
