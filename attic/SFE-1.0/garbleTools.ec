require import Int.
require import Array.

import ForLoop.

op appender (extract:int->'a array->'a) (i:int) (ar:'a array): 'a array =
  ar:::(extract (i-1) ar).

op appendInit (ar:'a array) (size:int) (extract:int->'a array->'a):'a array =
  let n = Array.length ar in
  range n (n+size) ar (appender extract).

lemma appendInit_length (ar:'a array) (n:int) (extract:int->'a array->'a):
  0 <= n =>
  Array.length (appendInit ar n extract) = (Array.length ar) + n.
proof.
intros=> leq0_n.
pose m:= n.
cut: 0 <= m by smt.
elim/Induction.induction m; first smt.
move => i Hi IH /=.
rewrite /appendInit /= range_ind_lazy /=; first smt.
rewrite /appender length_snoc; smt.
qed.

lemma appendInit_ind (ar:'a array) (n:int) (extract:int->'a array->'a) (k:int):
  0 <= k < (Array.length ar) + n - 1 =>
  0 < n =>
  (appendInit ar n extract).[k] = (appendInit ar (n-1) extract).[k].
proof.
by move => [leq0_k ltk_len] lt0_n;
   rewrite /appendInit /= range_ind_lazy; smt.
qed.

lemma appendInit_get1 (ar:'a array) (n:int) (extract:int->'a array->'a) (k:int):
  0 <= k < Array.length ar =>
  0 <= n =>
  (appendInit ar n extract).[k] = ar.[k].
proof.
intros=> [leq0_k ltk_len] leq0_n.
pose m := n; cut: 0 <= m by smt.
elim/Induction.induction m; smt.
qed.

lemma appendInit_get2 (ar:'a array) (n:int) (extract:int->'a array->'a) (k:int):
  Array.length ar <= k < (Array.length ar) + n =>
  0 < n =>
  (appendInit ar n extract).[k] = (extract (k - 1) (appendInit ar (k - length ar) extract)).
proof.
intros=> [leqlen_k lt_k_len] lt0_n.
cut ->: n = (k - length ar) + 1 + (n - (k - length ar) - 1) by smt.
cut: 0 <= (n - (k - length ar) - 1) by smt.
elim/Induction.induction (n - (k - length ar) - 1); last smt.
rewrite /appendInit /=.
cut ->: length ar + (k - length ar + 1) = k + 1 by smt.
cut ->: length ar + (k - length ar) = k by smt.
rewrite range_ind_lazy; first smt.
cut ->: k + 1 - 1 = k by smt.
cut :length (appendInit ar (k - length ar) extract) = k; smt.
qed.

lemma appendInit_getFinal (ar:'a array) (n:int) (extract:int->'a array->'a) (k:int):
  Array.length ar <= k < (Array.length ar) + n =>
  0 < n =>
  extract (k-1) (appendInit ar n extract) = extract (k-1) (sub (appendInit ar n extract) 0 k) =>
  (appendInit ar n extract).[k] = (extract (k-1) (appendInit ar n extract)).
proof.
intros=> [leqlen_k ltk_len lt0_n] hypExtract.
cut ->:= appendInit_get2 ar n extract k _ _=> //.
rewrite hypExtract.
cut ->: sub (appendInit ar n extract) 0 k = appendInit ar (k - length ar) extract; last by reflexivity.
apply array_ext; split.
  by rewrite length_sub; smt.
  by intros=> i leq0_i; rewrite get_sub; smt.
qed.

lemma ext_appendInit (q:int) (x:'a array) (extract1 extract2:int -> 'a array -> 'a) :
  0 <= q =>
  (forall g, length x - 1 <= g < length x + q - 1 => extract1 g = extract2 g) =>
  appendInit x q extract1 = appendInit x q extract2.
proof.
simplify appendInit.
intros hq h.
pose j := q.
cut: j <= q by smt.
cut: 0 <= j by smt.
elim/Induction.induction j=> //; first smt.
intros i hi hr H.
rewrite (range_ind_lazy _ _ _ (appender extract1));first smt.
rewrite (range_ind_lazy _ _ _ (appender extract2));first smt.
cut ->: length x + (i + 1) - 1 = length x + i by smt.
smt.
qed.

op evalComplete (q:int) (x:'a array) (extract:int -> 'a array -> 'a): 'a array =
  appendInit x q extract.

op extract (eval:int -> 'a -> 'a -> 'a) (aa:int array) (bb:int array) (g:int) (x:'a array): 'a =
  let g = g + 1 in
  eval g x.[aa.[g]] x.[bb.[g]].

type topo_t = int*int*int*(int array)*(int array).

theory Tweak.
  require ExtWord.
  clone import ExtWord as W.

  op bti (x:bool) = if x then 1 else 0.

  (* Tweak definition and injectivity *)
  op tweak (g:int) (a:bool) (b:bool) : word = W.from_int (4 * g + 2 * bti a + bti b).

  lemma nosmt tweak_bounds g a b:
    0 <= g < 2^(W.length - 2) =>
    0 <= 4 * g + 2 * bti a + bti b <= 2^W.length - 1.
  proof. case a; case b => _ _; rewrite /bti /=; smt. qed.

  lemma nosmt decomp g g' a a' b b':
    0 <= g =>
    0 <= g' =>
    0 <= a <= 1 =>
    0 <= a' <= 1 =>
    0 <= b <= 1 =>
    0 <= b' <= 1 =>
    4 * g + 2 * a + b = 4 * g' + 2 * a' + b' =>
    g = g' /\ a = a' /\ b = b'
  by [].

  lemma tweak_inj g g' a a' b b':
    0 <= g  < 2 ^ (W.length - 2) =>
    0 <= g' < 2 ^ (W.length - 2) =>
    tweak g a b = tweak g' a' b' =>
    g = g' /\ a = a' /\ b = b'.
  proof.
  intros=> hg hg'.
  rewrite /tweak=> fi_g_g'.
  cut:= decomp g g' (bti a) (bti a') (bti b) (bti b') _ _ _ _ _ _ _; first 6 smt.
    by cut t_bound := tweak_bounds g  a  b _=> //;
       cut t'_bound:= tweak_bounds g' a' b' _=> //;
       smt.
    smt.
  qed.
end Tweak.