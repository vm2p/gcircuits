require import Bool.
require import Pair.
require import Real.
require import Distr.

lemma dbool_mu_id:
 mu {0,1} (fun (x : bool), x) = 1%r / 2%r.
proof.
cut ->: mu {0,1} (fun (x : bool), x) = mu_x {0,1} true.
 rewrite /mu_x //; congr.
 by rewrite -fun_ext => x /=; case x.
by rewrite Dbool.mu_x_def.
qed.

lemma dbool_mu_not:
 mu {0,1} (fun (x : bool), !x) = 1%r / 2%r.
proof.
cut ->: mu {0,1} (fun (x : bool), !x) = mu_x {0,1} false.
 rewrite /mu_x //; congr.
 by rewrite -fun_ext => x /=; case x.
by rewrite Dbool.mu_x_def.
qed.

require import Prot.

theory ProtSecurity.
  clone import Protocol.

  (* Alias for the correctness assertion *) 
  (* This will need to be proven for all protocols *)
  pred Correct (x:unit) = forall i1 r1 i2 r2, 
    validInputs i1 i2 =>
    validRands i1 i2 r1 r2 =>
    f i1 i2 = snd (prot i1 r1 i2 r2).

  (* following Goldreich, we include the random coins in each party view
  (but leave the input implicit in the context...) *)
  type view1_t = rand1_t * conv_t.
  type view2_t = rand2_t * conv_t.

  module type Rand1_t = { proc gen(i1info : leak1_t) : rand1_t }.
  module type Rand2_t = { proc gen(i2info : leak2_t) : rand2_t }.

  (* Simulator & Adversaries *)
  module type Sim_t = {
    proc sim1(i1: input1_t, o1: output1_t, l2: leak2_t) : view1_t
    proc sim2(i2: input2_t, o2: output2_t, l1: leak1_t) : view2_t
  }.

  module type Adv1_t = { 
    proc gen_query() : input1_t * input2_t
    proc dist(view: view1_t) : bool 
  }.

  module type Adv2_t = {
    proc gen_query() : input1_t * input2_t
    proc dist(view: view2_t) : bool 
  }.

  module Game1(R1: Rand1_t, R2: Rand2_t, S: Sim_t, A1: Adv1_t) = {
    proc game(b:bool) : bool = {
      var guess : bool;
      var view1 : view1_t;
      var o1 : output1_t;
      var r1 : rand1_t;
      var r2 : rand2_t;
      var i1 : input1_t;
      var i2 : input2_t;
    
      (i1,i2) = A1.gen_query();

      if (!validInputs i1 i2)
        guess = ${0,1};
(*         guess = !b;*)
      else {
        if (b) {
          r1 = R1.gen(phi1 i1);
          r2 = R2.gen(phi2 i2);
          view1 = (r1, fst (prot i1 r1 i2 r2));
        } else {
          o1 = fst (f i1 i2);
          view1 = S.sim1 (i1, o1, phi2(i2));
        }       
        guess = A1.dist(view1);
      }
      return guess=b;
    }
    proc main() : bool = {
      var real, adv: bool;
      real = ${0,1};
      adv = game(real);
      return adv;
    }
  }.

  module Game2(R1: Rand1_t, R2: Rand2_t, S: Sim_t, A2: Adv2_t) = {
    proc game(b: bool) : bool = {
      var i1 : input1_t;
      var i2 : input2_t;
      var guess : bool;
      var view2 : view2_t;
      var o2 : output2_t;
      var r1 : rand1_t;
      var r2 : rand2_t;
    
      (i1,i2) = A2.gen_query();
      
      if (!validInputs i1 i2) {
        guess = ${0,1};
(*        guess = !b;*)
      } else {
        if (b) {
          r1 = R1.gen(phi1 i1);
          r2 = R2.gen(phi2 i2);
          view2 = (r2, fst (prot i1 r1 i2 r2));
        } else {
          o2 = snd (f i1 i2);
          view2 = S.sim2 (i2, o2, phi1(i1));      
        }  
        guess = A2.dist(view2);
      }
      return guess=b;
    }
    proc main() : bool = {
      var real, adv: bool;
      real = ${0,1};
      adv = game(real);
      return adv;
    }
  }.

(** GENERIC ADVANTAGE DEFINITIONS *)
(** Advantage definition *)
lemma Game1_adv &m (A1 <: Adv1_t) (S <: Sim_t)
                   (R1 <: Rand1_t {A1}) (R2 <: Rand2_t {A1}):
  islossless A1.gen_query =>
  islossless A1.dist =>
  islossless R1.gen =>
  islossless R2.gen =>
  islossless S.sim1 =>
  2%r * Pr[Game1(R1,R2,S,A1).main()  @ &m: res] - 1%r
  = Pr[Game1(R1,R2,S,A1).game(true)  @ &m: res] 
    - Pr[Game1(R1,R2,S,A1).game(false)  @ &m: !res].
proof.
  move=> A1gen_ll A1dist_ll R1gen_ll R2gen_ll Ssim1_ll.
  rewrite Pr [mu_not].
  pose p1:= Pr[Game1(R1,R2,S,A1).game(true) @ &m : res].
  pose p2:= Pr[Game1(R1,R2,S,A1).game(false) @ &m : res].
  (* lossless condition *)
  cut ->: Pr[Game1(R1,R2,S,A1).game(false) @ &m : true] = 1%r.
    byphoare (_: true)=> //.
    proc.
    seq 1: true=> //; first by call (_:true).
    case (validInputs i1 i2).
      by rcondf 1=> //; if; do !(call (_: true)=> //; wp).
      by rcondt 1 => //; auto; smt.
  cut Hp1: phoare [Game1(R1,R2,S,A1).game:
                  (glob Game1(R1,R2,S,A1)) = (glob Game1(R1,R2,S,A1)){m} /\ b ==> res] = p1.
    bypr=> &m' [eqG] b'; rewrite /p1 b'.
    by byequiv (_: ={glob Game1(R1,R2,S,A1), b} ==> ={res})=> //; sim; sim.
  cut Hp2: phoare [Game1(R1,R2,S,A1).game:
                 (glob Game1(R1,R2,S,A1)) = (glob Game1(R1,R2,S,A1)){m} /\ !b ==> res] = p2.
    bypr=> &m' [eqG]; rewrite -neqF=> b'; rewrite /p2 b'.
    by byequiv (_: ={glob Game1(R1,R2,S,A1), b} ==> ={res})=> //; first sim; sim.
  cut Hp: phoare [Game1(R1,R2,S,A1).main:
            (glob Game1(R1,R2,S,A1)) = (glob Game1(R1,R2,S,A1)){m} ==> res] = ((p1+p2)/2%r).
    proc.
    seq 1: real (1%r / 2%r) p1 (1%r / 2%r) p2
           ((glob Game1(R1,R2,S,A1)) = (glob Game1(R1,R2,S,A1)){m}).
      by auto.
      by rnd ((=) true); skip; smt.
      by call Hp1.
      by rnd ((=) false); skip; smt.
      by call Hp2.
      smt.
  cut ->: Pr[Game1(R1,R2,S,A1).main(tt) @ &m : res] = ((p1+p2)/2%r).
    by byphoare Hp.
  smt.
qed.

(** Advantage definition *)
lemma Game2_adv &m (A2<:Adv2_t {Game2}) (S<:Sim_t {A2}) (R1<:Rand1_t {A2}) (R2<:Rand2_t {A2}):
  islossless A2.gen_query =>
  islossless A2.dist =>
  islossless R1.gen =>
  islossless R2.gen =>
  islossless S.sim2 =>
  2%r * Pr[Game2(R1,R2,S,A2).main()  @ &m: res] - 1%r
  = Pr[Game2(R1,R2,S,A2).game(true)  @ &m: res] 
    - Pr[Game2(R1,R2,S,A2).game(false)  @ &m: !res].
proof.
  move=> A2gen_ll A2dist_ll R1gen_ll R2gen_ll Ssim2_ll.
  rewrite Pr [mu_not].
  pose p1:= Pr[Game2(R1,R2,S,A2).game(true) @ &m : res].
  pose p2:= Pr[Game2(R1,R2,S,A2).game(false) @ &m : res].
  (* lossless condition *)
  cut ->: Pr[Game2(R1,R2,S,A2).game(false) @ &m : true] = 1%r.
    byphoare (_:true)=> //.
    proc.
    seq 1: true => //; first by call (_: true).
    case (validInputs i1 i2). 
      by rcondf 1=> //; if; do !(wp; call (_: true)); auto.
      by rcondt 1=> //; auto; smt.
  cut Hp1: phoare [Game2(R1,R2,S,A2).game:
                     (glob Game2(R1,R2,S,A2)) = (glob Game2(R1,R2,S,A2)){m} /\ b ==> res] = p1.
    bypr=> &m' [eqG] b'; rewrite /p1 b'.
    byequiv (_: ={glob Game2(R1,R2,S,A2), b} ==> ={res})=> //; first by sim.
  cut Hp2: phoare [Game2(R1,R2,S,A2).game:
                     (glob Game2(R1,R2,S,A2)) = (glob Game2(R1,R2,S,A2)){m} /\ !b ==> res] = p2.
    bypr=> &m' [eqG]; rewrite -neqF=> b'; rewrite /p2 b'.
    byequiv (_: ={glob Game2(R1,R2,S,A2), b} ==> ={res})=> //; first by sim.
  cut Hp: phoare [Game2(R1,R2,S,A2).main:
                    (glob Game2(R1,R2,S,A2)) = (glob Game2(R1,R2,S,A2)){m} ==> res] = ((p1+p2)/2%r).
    proc.
    seq 1: real (1%r / 2%r) p1 (1%r / 2%r) p2 ((glob Game2(R1,R2,S,A2)) = (glob Game2(R1,R2,S,A2)){m}).
      by auto.
      by rnd ((=) true); skip; smt.
      by call Hp1.
      by rnd ((=) false); skip; smt.
      by call Hp2.
      smt.
  cut ->: Pr[Game2(R1,R2,S,A2).main(tt) @ &m : res] = ((p1+p2)/2%r).
    by byphoare Hp.
  smt.
qed.

end ProtSecurity.
