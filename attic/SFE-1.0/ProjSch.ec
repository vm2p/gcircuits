require import Array.
require import Pair.
require import Int.

require        Sch.

(***************)

theory ProjScheme.
  type token_t.

  clone import Sch with
    type Scheme.Input.input_t = bool array,
    type Scheme.Input.inputK_t = (token_t * token_t) array,
    type Scheme.Input.inputG_t = token_t array,
    op Scheme.Input.encode (iK:inputK_t) (x:input_t) =
      init (length iK) (fun k, (if x.[k] then snd else fst) iK.[k]).
  import Scheme.

  (* Some lemmata for the encode operation *)
  lemma encode_cat iK1 iK2 i1 i2:
   length iK1 = length i1 => length iK2 = length i2 =>
   encode (iK1 || iK2)%Array (i1 || i2)%Array =
   ((encode iK1  i1) || (encode iK2 i2)).
  proof strict.
  delta=> H1 H2 /=.
  rewrite !H1 !H2 !length_append.
  apply array_ext; split.
    rewrite length_init; first smt.
    by rewrite !length_append !length_init; smt.
  intros=> k.
  rewrite length_init; first smt.
  intros=> Hk.
  case (k < length i1)=> Hcase.
    rewrite get_append_left; first smt.
    rewrite get_init //= get_append_left; first smt.
    rewrite (get_append_left iK1 iK2 k); first smt.
    by rewrite get_init //=; first smt.
    rewrite get_append_right; first smt.
    rewrite length_init; first smt.
    rewrite get_init //= get_append_right; first smt.
    rewrite (get_append_right iK1 iK2 k); first smt.
    by rewrite H1 get_init //=; first smt.
  qed.

  lemma encode_take iK i1 i2:
   length iK = length (i1||i2)%Array =>
   encode (take (length i1) iK) i1 =
    take (length i1) (encode iK (i1||i2)).
  proof strict.
  rewrite length_append=> Hlen.
  apply array_ext; split; first smt.
  intros=> k; rewrite /encode length_init; first smt.
  rewrite length_take; first smt.
  intros=> Hk.
  rewrite get_init // get_take //=; first smt.
  rewrite get_init //=; first smt.
  rewrite get_append_left //.
  by rewrite get_take /=; first smt.
  qed.

  lemma encode_drop iK i1 i2:
   length iK = length (i1 || i2)%Array =>
   encode (drop (length i1) iK) i2 =
    drop (length i1) (encode iK (i1||i2)).
  proof strict.
  rewrite length_append => Hlen.
  apply array_ext; split; first smt.
  intros=> k; rewrite /encode length_init; first smt.
  rewrite length_drop; first smt.
  intros=> Hk.
  rewrite get_init /=; first smt.
  rewrite get_drop //=; first smt.
  rewrite get_drop /=; first 2 smt.
  rewrite get_init //=; first smt.
  rewrite get_append_right; first smt.
  smt.
  qed.

  lemma encode_take_drop iK i1 i2:
   length iK = length (i1||i2)%Array =>
   (encode iK (i1 || i2)%Array) =
   ((encode (take (length i1) iK) i1)||(encode (drop (length i1) iK) i2)).
  proof strict.
  intros=> H.
  rewrite -{1}(take_drop iK (length i1)).
    by split; smt.
  rewrite encode_cat //.
    by rewrite length_take; smt.
  by rewrite length_drop; smt.
  qed.
end ProjScheme.
