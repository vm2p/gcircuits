require import Option.
require import Pair.
require import Real.
require import Int.
require import FMap.
require import Array.
require import List.
require import Fun.
 
import ForLoop.
import Dprod.
 
require import DKCSch2.
import DKCScheme2.
import SchAuth.
import Scheme.
require import FHE.
 
require import VC.
require import VCSec.
 
(* pragma nocheck.*)
 
theory SomeVC.
 
   theory F.
   op fn : fun_t.
   end F.
 
   clone import FHE with
     theory Scheme = DKCScheme2.SchAuth.Scheme.
 
   (* Our Concrete VC Scheme *)
 
   clone VC with
     type randg_t = rand_t,
     type randp_t = FHE.randg_t * FHE.rande_t,
     type fun_t = fun_t,
     type inp_t = Input.input_t,
     type out_t = output_t,
     type pkey_t = funG_t,
     type skey_t = fun_t * Input.inputK_t * outputK_t,
     type xpub_t = FHE.pkey_t * FHE.cipheri_t,
     type xprv_t = FHE.skey_t,
     type proof_t = FHE.ciphero_t,
     op eval = DKCScheme2.SchAuth.Scheme.eval,
     op kGen (fn : fun_t, rg: randg_t) = 
                     let fG = funG fn rg in
                     let oK = outputK fn rg in
                     let iK = inputK fn rg in
                         (fG, (fn,iK,oK)),
     op pGen (x:inp_t, sk : skey_t, rp : randp_t) = 
                       let (fn,iK,oK) = sk in
                       let (rgfhe,refhe) = rp in 
                       let (pkfhe,skfhe) = FHE.gen rgfhe in
                       let xG =  Input.encode iK x in
                       let c = FHE.enc pkfhe xG refhe in
                           ((pkfhe,c), skfhe),
     op comp (fg : pkey_t, xp : xpub_t) = let (pkfhe,c) = xp in 
                           hom_eval (evalG fg) c,
     op vrfy (sk : skey_t, co : proof_t, skfhe : xprv_t) =
                       let (fn,iK,oK) = sk in
                       let oG = FHE.dec skfhe co in
                       let b = valid_outG oK oG in
                           if b 
                           then Some (decode oK oG)
                           else None,
     op valid_i = DKCScheme2.SchAuth.Scheme.validInputs,
     pred valid_r (fn,rg,rp) = DKCScheme2.SchAuth.Scheme.validRand fn rg.
 
   clone import VCSec with
       theory F = F,
       theory VC = VC.
 
axiom max_qr_match : max_qr = max_qr_fhe.
 
     
   (******************************************************)
   (********* CORRECTNESS PROOF **************************)
   (******************************************************)
 
   lemma cv_is_correct : Garble1.DKCSecurity.D.Correct () => VCSec.Correct ().
   proof.
   move => DKC_correct; move: (DKCScheme2.sch_correct _) => //.
   rewrite /DKCScheme2.Correct => Garble2_correct.
   delta Correct; beta.
   intros f x rg rp H1 H2.
   delta correct;beta.
   split.
   smt.
   progress.
   generalize H; generalize H0; generalize x1; generalize x2; generalize x10; generalize x20;intros xs xp sk pk H0 H. 
   delta VC.vrfy VC.comp VC.eval SomeVC.VC.vrfy SomeVC.VC.comp SomeVC.VC.eval evalG valid_outG decode eval.
   beta.
   cut validout : (
   let (fn,iK,oK) = sk in
   let oG =
     dec xs
       (let (pkfhe, c) = xp in
        hom_eval (evalG pk) c) in
          (DKCScheme2.SchAuth.Scheme.valid_outG oK oG)
   ).
   generalize H0;delta VC.pGen; delta SomeVC.VC.pGen; delta inputK; delta Input.encode; beta => H0.
   generalize H; delta VC.kGen; delta SomeVC.VC.kGen; delta funG; beta => H.
   cut H3 : 
     (let fG = funG f rg in
        let iK = inputK f rg in
        let xG = Input.encode iK x in
        let oK = outputK f rg in
               let (pk,sk) = gen (fst rp) in
               let ci = enc pk xG (snd rp) in
               let co = hom_eval (Scheme.evalG fG) ci in
                      dec sk co = evalG fG xG). 
   apply (FHE.homomorphic (f) (x) (rg) (fst rp) (snd rp) _ _).
   generalize H2;smt.
   smt.
   progress.
   generalize H3.
   progress.
   cut -> : (
      (dec xs
      (let (pkfhe, c) = xp in
       hom_eval (evalG pk) c))
     = 
     (evalG (funG f rg)
       (Input.encode (inputK f rg) x))
   ).
   generalize H3;smt.
   cut -> : (x3 = outputK f rg).
   smt.
   smt.
   simplify; move: validout.
   cut ->: sk = (sk.`1, sk.`2, sk.`3) by smt => /= validout.
   cut ->: (Garble2.valid_outG sk.`3
         (dec xs
            (let (pkfhe, c) = xp in hom_eval ((Garble2.evalG pk))%Garble2 c))) = true by smt => /=.
   rewrite oget_some.
   move: validout; cut ->: xp = (xp.`1, xp.`2) by smt => /=. 
   move: (homomorphic f x rg (fst rp) (snd rp) _ _) => /=; first 2 smt.
   cut ->: gen (fst rp) = (fst (gen (fst rp)), snd (gen (fst rp))) by smt.
   move => /= FHEcorrect Hvalid.
   cut ->: dec xs (hom_eval (Garble2.evalG pk) xp.`2)
           = Scheme.evalG (Scheme.funG f rg) (Scheme.Input.encode (Scheme.inputK f rg) x) by smt.
   smt.
   qed.
 
   (******************************************************)
   (********* GENERATION ALGORITHMS **********************)
   (******************************************************)
 
   module RG : VCSec.RandG_t = {
      proc gen(f : VC.fun_t) : VC.randg_t = {
          var rg : VC.randg_t;
          rg = Rand.gen(phi f);
          return rg;
      }
   }.
 
   module RP(RP1 : FHE.RandG_t, RP2 : FHE.RandE_t) : VCSec.RandP_t = {
      proc gen(f : VC.fun_t) : VC.randp_t = {
          var rg : FHE.randg_t;
          var re : FHE.rande_t;
          rg = RP1.gen();
          re = RP2.gen();
          return (rg,re);
      }
   }.
 
   (******************************************************)
   (*********  AUTHENTICITY PROOF  ***********************)
   (******************************************************)
 
  (* GAME 1 -- We guess the forgery query *)
 
  module O_Gm1(RG : RandG_t, RP : RandP_t) = {
     var qlist : (VC.inp_t * VC.xpub_t  * VC.xprv_t) array
     var pk : VC.pkey_t
     var sk : VC.skey_t
     var l, lR : int
     var validQs : bool
 
     proc init(lC : int) : VC.pkey_t = {
        var r : VC.randg_t;
 
        l = lC;
 
        validQs = true;
        qlist = Array.empty;
        r = RG.gen(F.fn);
        (pk,sk) = VC.kGen F.fn r ;
        return pk;
     }
      
     proc pgen(x : VC.inp_t) : VC.xpub_t option = {
        var r : VC.randp_t;
        var xp : VC.xpub_t;
        var xs : VC.xprv_t;
        var rep : VC.xpub_t option;
 
        if (!queryValid_VRF F.fn x) {
            validQs = false;
            rep = None;
        }
        else {
            r = RP.gen(F.fn);
            (xp,xs) = VC.pGen x sk r;
            if (length qlist >= max_qr) { rep = None; } 
            else { qlist =(x,xp,xs) :: qlist; rep = Some xp; }
        }
        return rep;  
     }
 
     proc check(ip : int, pr : VC.proof_t) : bool = {
        var x : VC.inp_t;
        var xp : VC.xpub_t;
        var xs : VC.xprv_t;
        var r : bool;
        var y : VC.out_t option;
 
        lR = ip;
 
        if (ip >= 0 /\ ip < length qlist /\ length qlist = max_qr /\ validQs = true) {
          (x,xp,xs) = qlist.[ip];
          y = VC.vrfy sk pr xs;
          if  (lR = l) {
             r = (y <> None /\ VC.eval F.fn x <> oget y);
          }
          else r = false;
        }
        else r = false;
 
        return r;
     }
  }.
 
(* GAME 1 -- But conditioned on l *)
 
 
  module Game_1C(RG: RandG_t, RP: RandP_t, ADV : Adv_VRF_t) = {
     
    module O = O_Gm1(RG,RP)
    module A = ADV(O)
 
    proc main(lC : int) : bool = {
      var pk : VC.pkey_t;
      var pr : VC.proof_t;
      var i : int;
      var r : bool;
 
      pk = O.init(lC);
      (i,pr) = A.forge(pk);
      r = O.check(i,pr);
      return r;
  }
}.
 
 
  module Game_1(RG: RandG_t, RP: RandP_t, ADV : Adv_VRF_t) = {
    var l : int
     
    module Game1C = Game_1C(RG,RP,ADV)
 
    proc main() : bool = {
      var r : bool;
       
      l = $[0..max_qr-1];
 
      r = Game1C.main(l);
      return r;
  }
}.
 
  (* GAME 2 -- We use a dummy garbling in all but the forging query. *)
 
  module O_Gm2(RG : RandG_t, RP : RandP_t) = {
     var qlist : (VC.inp_t * VC.xpub_t  * VC.xprv_t) array
     var pk, dummy_pk : VC.pkey_t
     var sk, dummy_sk : VC.skey_t
     var l, lR : int
     var validQs : bool
 
     proc init(lC : int) : VC.pkey_t = {
        var r, r_dummy : VC.randg_t;
 
        l = lC;
 
        validQs = true;
        qlist = Array.empty;
        r = RG.gen(F.fn);
        (pk,sk) = VC.kGen F.fn r ;
        r_dummy = RG.gen(F.fn);
        (dummy_pk,dummy_sk) = VC.kGen F.fn r_dummy ;
        return pk;
     }
      
     proc pgen(x : VC.inp_t) : VC.xpub_t option = {
        var r, r_dummy : VC.randp_t;
        var xp : VC.xpub_t;
        var xs : VC.xprv_t;
        var rep :VC.xpub_t option;
 
 
        if (!queryValid_VRF F.fn x) {
            validQs = false;
            rep = None;
        }
        else {
            if (length qlist = max_qr - l - 1) {
               r = RP.gen(F.fn);
               (xp,xs) = VC.pGen x sk r;
            }
            else {
               r_dummy = RP.gen(F.fn);
               (xp,xs) = VC.pGen x dummy_sk r_dummy;
            }
            if (length qlist >= max_qr) { rep = None; } 
            else { qlist =(x,xp,xs) :: qlist; rep = Some xp; }
        }
        return rep;  
     }
 
     proc check(ip : int, pr : VC.proof_t) : bool = {
        var x : VC.inp_t;
        var xp : VC.xpub_t;
        var xs : VC.xprv_t;
        var r : bool;
        var y : VC.out_t option;
         
        lR = ip;
 
        if (ip >= 0 /\ ip < length qlist /\ length qlist = max_qr /\ validQs = true) {
          (x,xp,xs) = qlist.[ip];
          y = VC.vrfy sk pr xs;
          if  (lR = l) {
             r = (y <> None /\ VC.eval F.fn x <> oget y);
          }
          else r = false;
        }
        else r = false;
 
        return r;
     }
  }.
 
  module Game_2(RG: RandG_t, RP: RandP_t, ADV : Adv_VRF_t) = {
     
    module O = O_Gm2(RG,RP)
    module A = ADV(O)
 
    proc main() : bool = {
      var pk : VC.pkey_t;
      var pr : VC.proof_t;
      var i : int;
      var r : bool;
      var l : int;
       
      l = $[0..max_qr-1];
 
      pk = O.init(l);
      (i,pr) = A.forge(pk);
      r = O.check(i,pr);
      return r;
  }
}.
 
  (* Interpolation adversary -- this is used by an adversary that 
     interpolates games 1 and 2 using FHE security. 
     This is only the oracle simulation part, the adversary body
     is presented below. *)
 
  module O_D(O : O_IND'_t, RP1: FHE.RandG_t, RP2 : FHE.RandE_t) = {
     module RP = RP(RP1,RP2)
 
     var qlist : (VC.inp_t * VC.xpub_t * VC.xprv_t) array
     var pk, dummy_pk : VC.pkey_t
     var sk, dummy_sk : VC.skey_t
     var l, lR : int
     var validQs : bool
 
     proc init(lC : int) : VC.pkey_t = {
        var r, dummy_r : VC.randg_t;
 
        l = lC;
 
        validQs = true;
        qlist = Array.empty;
        r = RG.gen(F.fn);
        (pk,sk) = VC.kGen F.fn r ;
        dummy_r = RG.gen(F.fn);
        (dummy_pk,dummy_sk) = VC.kGen F.fn dummy_r ;
        return pk;
     }
      
     proc pgen(x : VC.inp_t) : VC.xpub_t option = {
        var r, dummy_r : VC.randp_t;
        var xp : VC.xpub_t;
        var xpout : VC.xpub_t option;
        var xs : VC.xprv_t;
        var rep :VC.xpub_t option;
        var xG, dummy_xG : Input.inputG_t;
 
        if (!queryValid_VRF F.fn x) {
           validQs = false;
           rep = None;
        }
        else {
            if (length qlist = max_qr - l - 1) {
               r = RP.gen(F.fn);
               (xp,xs) = VC.pGen x sk r;
               xpout = Some xp;
            }
            else {
                xG = let (fn, iK, oK) = sk in
                        Input.encode iK x;
 
                dummy_xG = let (fn, iK, oK) = dummy_sk in
                         Input.encode iK x;
 
                xpout = O.enc((dummy_xG,xG));
            }
            if (length qlist >= max_qr) { rep = None; } 
            else { qlist =(x,xp,xs) :: qlist; rep = xpout; }
        }
        return rep;  
     }
 
     proc check(ip : int, pr : VC.proof_t) : bool = {
        var x : VC.inp_t;
        var xp : VC.xpub_t;
        var xs : VC.xprv_t;
        var r : bool;
        var y : VC.out_t option;
 
        lR = ip;
         
        if (! validQs) {
           r = false;
        }
        else {
           if (ip >= 0 /\ ip < length qlist /\ length qlist = max_qr) {
             (x,xp,xs) = qlist.[ip];
             y = VC.vrfy sk pr xs;
             if  (lR = l) {
                r = (y <> None /\ VC.eval F.fn x <> oget y);
             }
             else r = false;
           }
           else r = false;
        }
        return r;
     }
  }.
 
  (* Forgery adversary -- reduces a successful attack in Game 2
     to the adaptive authenticity of Garble2. 
     This is only the oracle simulation part, the adversary body
     is presented below. *)
 
  op invalid_oG : outputG_t.
  axiom inv_oG : forall oK, ! valid_outG oK invalid_oG.
 
  module O_F(O : SchAuth.O_AUT'_t, RG : RandG_t, RP : RandP_t) = {
     var qlist : (VC.inp_t * VC.xpub_t  * VC.xprv_t) array
     var pk, dummy_pk : VC.pkey_t
     var dummy_sk : VC.skey_t
     var l : int
     var validQs : bool
 
     proc init(lC : int, _pk : VC.pkey_t) : unit = {
        var r, r_dummy : VC.randg_t;
 
        l = lC;
 
        validQs = true;
        qlist = Array.empty;
        pk = _pk;
        r_dummy = RG.gen(F.fn);
        (dummy_pk,dummy_sk) = VC.kGen F.fn r_dummy ;
     }
      
     proc pgen(x : VC.inp_t) : VC.xpub_t option = {
        var r, r_dummy : VC.randp_t;
        var xp : VC.xpub_t;
        var xs : VC.xprv_t;
        var xG : Input.inputG_t option;
        var rep :VC.xpub_t option;
 
        if (!queryValid_VRF F.fn x) {
            validQs = false;
            rep = None;
        }
        else {
            if (length qlist = max_qr - l - 1) {
               r = RP.gen(F.fn);
               xG =  O.enc(x);
               (xp,xs) = let (rgfhe,refhe) = r in 
                         let (pkfhe,skfhe) = FHE.gen rgfhe in
                         let c = FHE.enc pkfhe (oget xG) refhe in
                             ((pkfhe,c), skfhe);
            }
            else {
               r_dummy = RP.gen(F.fn);
               (xp,xs) = VC.pGen x dummy_sk r_dummy;
            }
            if (length qlist >= max_qr) { rep = None; } 
            else { qlist =(x,xp,xs) :: qlist; rep = Some xp; }
        }
        return rep;  
     }
 
     proc getForge(ip : int, pr : VC.proof_t) : outputG_t = {
        var x : VC.inp_t;
        var xp : VC.xpub_t;
        var xs : VC.xprv_t;
        var oG : outputG_t; 
        
        if (ip >= 0 /\ ip < length qlist /\ length qlist = max_qr /\ validQs = true) {
          (x,xp,xs) = qlist.[ip];
          if  (ip = l) {
            oG = FHE.dec xs pr;
          }
          else {
            oG = invalid_oG;
          }
        }
        else {
            oG = invalid_oG;
        }
        return oG; 
     }
  }.
 
(* Now the actual lemmas *)
section.
 
declare module ADV : Adv_VRF_t {O, O_Gm1, Game_Ver, O_D, O_IND', Game_1, O_Gm2, Game_2, SchAuth.O, O_F, SchAuth.Game_AUT'}.
declare module RP1 : FHE.RandG_t  {ADV, FHE.Game_IND, O, O_Gm1, Game_Ver, O_D, O_IND', Game_1, O_Gm2, Game_2, SchAuth.O, O_F, SchAuth.Game_AUT'}.
declare module RP2 : FHE.RandE_t {ADV, FHE.Game_IND, RP1, O, O_Gm1, Game_Ver, O_D, O_IND', Game_1, O_Gm2, Game_2, SchAuth.O, O_F, SchAuth.Game_AUT'}.
 
   (*********************************)
   (* Auxilliary lemmas for 1st hop *)
   (*********************************)
 
   (* Original security game coincides with game 1 when this
      is conditioned on the correct query chosen by the adv *)
 
   lemma conditional_equiv :
      forall k, 0 <= k < max_qr => 
        equiv [ Game_Ver(RG, RP(RP1,RP2), ADV).main ~ 
                Game_1C(RG, RP(RP1, RP2), ADV).main :
                  ={glob ADV,glob RP1,glob RP2} /\ lC{2} = k ==>
                     res{1} /\ Game_Ver.i{1} = k <=> res{2} /\ Game_1C.O.lR{2} = k ].    
   proof.
   intros k kBound.
   proc.
   inline Game_Ver(RG, RP(RP1, RP2), ADV).O.init Game_1C(RG, RP(RP1, RP2), ADV).O.init.
   swap {1} 3 -2.
   swap {2} 5 -4.
   seq 5 7 : (={glob ADV,glob RP1,glob RP2} /\ lC{2} = k /\ ={r0,pk} /\
              O.qlist{1} = Array.empty /\ O_Gm1.qlist{2} = Array.empty /\ O_Gm1.l{2} = lC{2} /\ O.validQs{1} = true  /\ O_Gm1.validQs{2} = true /\
              O.pk{1} = O_Gm1.pk{2} /\ O.sk{1}=O_Gm1.sk{2}).
   wp.
   call (_ : ={f} ==> ={res}).
   proc.
   inline Rand.gen.
   sim;skip;smt.  (*Rand consistent *)
   skip;progress;smt.
   seq 1 1 : (={glob ADV,glob RP1,glob RP2} /\ lC{2} = k /\ ={r0,pk,pr} /\
              O.qlist{1} = O_Gm1.qlist{2} /\ O_Gm1.l{2} = lC{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
              O.pk{1} = O_Gm1.pk{2} /\ O.sk{1}=O_Gm1.sk{2} /\ 
              Game_Ver.i{1} = i{2}).
   call (_ : ={glob ADV,glob RP1,glob RP2,pk}  /\ O.qlist{1} = O_Gm1.qlist{2} /\ 
              O.validQs{1} = O_Gm1.validQs{2} /\
              O.pk{1} = O_Gm1.pk{2} /\ O.sk{1}=O_Gm1.sk{2} ==> 
            ={glob ADV,glob RP1,glob RP2} /\ ={res} /\ 
              O.qlist{1} = O_Gm1.qlist{2} /\ 
              O.validQs{1} = O_Gm1.validQs{2} /\
              O.pk{1} = O_Gm1.pk{2} /\ O.sk{1}=O_Gm1.sk{2}).
   proc ( ={glob RP1,glob RP2}  /\ O.qlist{1} = O_Gm1.qlist{2} /\ 
              O.validQs{1} = O_Gm1.validQs{2} /\
              O.pk{1} = O_Gm1.pk{2} /\ O.sk{1}=O_Gm1.sk{2}).
   by trivial.
   by trivial.
   (* START pgen is same *)
   proc.
   case (! queryValid_VRF (F.fn)%F x{1}).
   rcondt {1} 1.
   intros &m;skip;by trivial.
   rcondt {2} 1.
   intros &m;skip;by smt.
   wp;skip;progress;smt.
   rcondf {1} 1.
   intros &m;skip;by trivial.
   rcondf {2} 1.
   intros &m;skip;by smt.
   inline RP(RP1, RP2).gen.
   seq 1 1 : (={glob RP1,glob RP2,x} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
  O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2} /\
  ={f} /\ f{1} = F.fn).
  wp;skip;by trivial.
   seq 1 1 : (={glob RP1,glob RP2,x} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2} /\
  ={f,rg} /\ f{1} = F.fn).
  call (_ : 
    (={glob RP1,glob RP2} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2})
    ==> 
  (={glob RP1,glob RP2,res} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2})
  ).
  proc (={glob RP2} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2}).
  by trivial.
  by trivial.
  wp;by trivial. 
   seq 1 1 : (={glob RP1,glob RP2,x} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2} /\
  ={f,rg,re} /\ f{1} = F.fn).
  call (_ : 
    (={glob RP1,glob RP2} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2})
    ==> 
  (={glob RP1,glob RP2,res} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2})
  ).
  proc (={glob RP1} /\
  O.qlist{1} = O_Gm1.qlist{2} /\
              O.validQs{1} = O_Gm1.validQs{2} /\
  O.pk{1} = O_Gm1.pk{2} /\ O.sk{1} = O_Gm1.sk{2}).
  by trivial.
  by trivial.
  wp;by trivial. 
  wp;skip;progress;smt.
  (* End pgen is same *)
  skip;progress;smt. 
   inline Game_Ver(RG, RP(RP1, RP2), ADV).O.check Game_1C(RG, RP(RP1, RP2), ADV).O.check.
   wp;skip.
   progress;smt.
   qed.  
 
   (* In Game 1, adversary only wins if he chooses the correct query,
      which is hidden from him *)
 
lemma only_wins_on_k : 
      forall k, 0 <= k < max_qr => 
        equiv [ Game_1C(RG, RP(RP1, RP2), ADV).main ~ 
                Game_1C(RG, RP(RP1, RP2), ADV).main :
                  ={glob ADV,glob RP1,glob RP2} /\ lC{1} = k /\ lC{2} = k ==>
                     res{1} <=> res{2} /\ Game_1C.O.lR{2} = k ].    
   proof.
   intros k kbound.
   proc.
   inline Game_1C(RG, RP(RP1, RP2), ADV).O.init.
   seq 7 7 : (={glob ADV,glob RP1,glob RP2, lC} /\ lC{2} = k /\ ={r0,pk} /\ 
              O_Gm1.qlist{1} = Array.empty /\ O_Gm1.qlist{2} = Array.empty /\ O_Gm1.l{2} = lC{2} /\ O_Gm1.l{1} = lC{1} 
/\ O_Gm1.validQs{1} = true  /\ O_Gm1.validQs{2} = true  /\
              O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1}=O_Gm1.sk{2}).
   wp.
   call (_ : ={f} ==> ={res}).
   proc.
   inline  Rand.gen.
   sim;skip;smt.  (*Rand consistent *)
   wp;skip;smt.
   seq 1 1 : (={glob ADV,glob RP1,glob RP2, lC,i,pr} /\ lC{2} = k /\ 
             ={r0,pk} /\ 
              O_Gm1.qlist{1} =O_Gm1.qlist{2} /\ O_Gm1.l{2} = lC{2} /\ O_Gm1.l{1} = lC{1} /\ 
O_Gm1.validQs{1} = O_Gm1.validQs{2} /\ 
              O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1}=O_Gm1.sk{2}).
   call (_ : ={glob ADV,glob RP1,glob RP2,pk} /\ O_Gm1.qlist{1} = O_Gm1.qlist{2} /\ 
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
              O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1}=O_Gm1.sk{2} ==> 
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
            ={glob ADV,glob RP1,glob RP2} /\ ={res} /\ 
               O_Gm1.qlist{1} = O_Gm1.qlist{2} /\ 
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
              O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1}=O_Gm1.sk{2}).
   proc ( ={glob RP1,glob RP2} /\ O_Gm1.qlist{1} = O_Gm1.qlist{2} /\ 
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
              O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1}=O_Gm1.sk{2}).
   by trivial.
   by trivial.
   (* START pgen is same *)
   proc.
   case (! queryValid_VRF (F.fn)%F x{1}).
   rcondt {1} 1.
   intros &m;skip;by trivial.
   rcondt {2} 1.
   intros &m;skip;by smt.
   wp;skip;progress;smt.
   rcondf {1} 1.
   intros &m;skip;by trivial.
   rcondf {2} 1.
   intros &m;skip;by smt.
   inline RP(RP1, RP2).gen.
   seq 1 1 : (={glob RP1,glob RP2,x} /\
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\ 
O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2} /\
  ={f} /\ f{1} = F.fn).
  wp;skip;by trivial.
   seq 1 1 : (={glob RP1,glob RP2,x} /\
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2} /\
  ={f,rg} /\ f{1} = F.fn).
  call (_ : 
    (={glob RP1,glob RP2} /\
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2})
    ==> 
  (={glob RP1,glob RP2,res} /\
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2})
  ).
  proc (={glob RP2} /\
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2}).
  by trivial.
  by trivial.
  wp;by trivial. 
   seq 1 1 : (={glob RP1,glob RP2,x} /\
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2} /\
  ={f,rg,re} /\ f{1} = F.fn).
  call (_ : 
    (={glob RP1,glob RP2} /\
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2})
    ==> 
  (={glob RP1,glob RP2,res} /\
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2})
  ).
  proc (={glob RP1} /\
  O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
 O_Gm1.validQs{1} =O_Gm1.validQs{2} /\ 
  O_Gm1.pk{1} = O_Gm1.pk{2} /\ O_Gm1.sk{1} = O_Gm1.sk{2}).
  by trivial.
  by trivial.
  wp;by trivial. 
  wp;skip;progress;smt.
  (* End pgen is same *)
  skip;progress;smt.
   inline Game_1C(RG, RP(RP1, RP2), ADV).O.check.
   wp;skip;progress;smt.
   qed.  
 
require (*--*) Monoid.
(*---*) import Monoid.Mrplus.
(*---*) import FSet.Interval.
 
   (* Technicality: the adversary cannot win on negative i *)
 
    local lemma events_overlap1 : 
       forall &m,
       Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res] =
       Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i >= 0].
    proof.
    intros &m.
    cut H : equiv [
       Game_Ver(RG, RP(RP1, RP2), ADV).main ~Game_Ver(RG, RP(RP1, RP2), ADV).main :
        ={glob ADV, glob RP1, glob RP2} ==> res{1} <=>  res{2} /\ Game_Ver.i{2} >= 0 
    ].
    proc.
    inline Game_Ver(RG, RP(RP1, RP2), ADV).O.init  RG.gen.
    seq 7 7 : ( ={glob ADV, glob RP1, glob RP2} /\ 
    O.validQs{1} = true /\
    O.qlist{1} = Array.empty /\ f{1} = F.fn /\
    (O.pk{1}, O.sk{1}) = (VC.kGen (F.fn) r0{1}) /\
    pk{1} = O.pk{1} /\ ={O.qlist,O.validQs,r0,O.pk,O.sk,pk,f}
    ).
    wp.
    call (_ :
    ={glob ADV, glob RP1, glob RP2} /\  O.qlist{1} = Array.empty /\ ={l}
    ==>
    ={glob ADV, glob RP1, glob RP2} /\  O.qlist{1} = Array.empty /\ ={res}
    ).
    proc.
    while (={i,n,q,r,glob ADV, glob RP1, glob RP2} /\ O.qlist{1}=Array.empty).
     wp; rnd; rnd; skip; smt.
    while (={i,n,q,m,r,glob ADV, glob RP1, glob RP2} /\ O.qlist{1}=Array.empty).
     wp; rnd; rnd; rnd; skip; smt.
    wp;skip;smt.  (* Rand is consistent*) 
    wp;skip;progress;smt.
    seq 1 1 : (={glob RP1, glob RP2} /\
  f{1} = (F.fn)%F /\
  (O.pk{1}, O.sk{1}) = (VC.kGen (F.fn)%F r0{1})%VC /\
  pk{1} = O.pk{1} /\ ={O.qlist, O.validQs,r0, O.pk, O.sk, pk, f, Game_Ver.i,pr}
    ).
    call (_ : ={glob RP1, glob RP2,pk} /\
    ={ O.qlist,O.validQs, O.pk, O.sk}
    ==> ={glob RP1, glob RP2} /\
    ={O.qlist, O.validQs,O.pk, O.sk,res}
    ).
    proc ( ={glob RP1, glob RP2} /\
    ={O.qlist, O.validQs,O.pk, O.sk}
    ).
    smt.
    by trivial.
    proc.
   case (! queryValid_VRF (F.fn)%F x{1}).
   rcondt {1} 1.
   intros &m0;skip;by trivial.
   rcondt {2} 1.
   intros &m0;skip;by smt.
   wp;skip;progress;smt.
   rcondf {1} 1.
   intros &m0;skip;by trivial.
   rcondf {2} 1.
   intros &m0;skip;by smt.
    inline RP(RP1, RP2).gen.
    seq 2 2 : 
      ( ={glob RP1, glob RP2} /\ ={x,f,rg} /\  
        ={O.qlist, O.validQs,O.pk, O.sk}).
    call (_ :
      ( ={glob RP1, glob RP2} /\
        ={O.qlist, O.validQs,O.pk, O.sk})
    ==>
      ( ={glob RP1, glob RP2} /\
        ={O.qlist, O.validQs,O.pk, O.sk,res})
    ).
    proc (
      ( ={ glob RP2} /\   
        ={O.qlist, O.validQs,O.pk, O.sk})
    ).
    by trivial.
    by trivial.
    wp;skip;by smt.
    seq 1 1 : 
      ( ={glob RP1, glob RP2} /\ ={x,f,rg,re} /\  
        ={O.qlist, O.validQs,O.pk, O.sk}).
    call (_ :
      ( ={glob RP1, glob RP2} /\
        ={O.qlist, O.validQs,O.pk, O.sk})
    ==>
      ( ={glob RP1, glob RP2} /\
        ={O.qlist, O.validQs,O.pk, O.sk,res})
    ).
    proc (
      ( ={ glob RP1} /\   
        ={ O.qlist, O.validQs,O.pk, O.sk})
    ).
    by trivial.
    by trivial.
    skip;by smt.
    case (length O.qlist{1} >= max_qr).
    rcondt {1} 3.
    intros &m0;wp;skip;by trivial.
    rcondt {2} 3.
    intros &m0;wp;skip;by trivial.
    wp;skip;progress;smt.
    rcondf {1} 3.
    intros &m0;wp;skip;by trivial.
    rcondf {2} 3.
    intros &m0;wp;skip;by trivial.
    wp;skip;progress;smt.
    skip;progress;smt.
    inline Game_Ver(RG, RP(RP1, RP2), ADV).O.check.
    wp;skip;progress;smt.
    byequiv H.
    by trivial.
    by trivial.
    qed.
 
   (* Technicality: the adversary cannot win on out of range i. *)
   lemma outofrange1 : 
        forall &m,
        Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : 
              res /\ Game_Ver.i >= max_qr] = 0%r.
   proof.
   intros &m.
   cut H : (phoare [
      Game_Ver(RG, RP(RP1, RP2), ADV).main : 
          true ==> res /\ Game_Ver.i >= max_qr ] = 0%r).
   proc.
   hoare.
   seq 2 : true.
   call (_ : true ==> true).
   proc (true).
   by trivial.
   by trivial.
   proc.
   inline RP(RP1, RP2).gen.
   wp.
   case ((! queryValid_VRF (F.fn)%F x)).
   rcondt 1.
   by trivial.
   by trivial.
   rcondf 1.
   by trivial.
   by trivial.
   inline Game_Ver(RG, RP(RP1, RP2), ADV).O.init.
   inline RG.gen.
   by trivial.
   inline Game_Ver(RG, RP(RP1, RP2), ADV).O.check.
   wp;skip;by smt.
   byphoare H.
   by trivial.
   by trivial.
   qed.
 
   (* Technicality: the adversary cannot win on negative l. *)
 
    local lemma events_overlap2 : 
       forall &m,
        Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res] =
        Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l >= 0].
    proof.
    intros &m.
    cut H : equiv [
       Game_1(RG, RP(RP1, RP2), ADV).main ~Game_1(RG, RP(RP1, RP2), ADV).main :
        ={glob ADV, glob RP1, glob RP2} ==> res{1} <=>  res{2} /\ Game_1.l{2} >= 0 
    ].
    proc.
    (************)
inline Game_1(RG, RP(RP1, RP2), ADV).Game1C.main Game_1C(RG, RP(RP1, RP2), ADV).O.init  RG.gen.
    seq 11 11 : ( ={glob ADV, glob RP1, glob RP2} /\ 
    Game_1.l{1} = O_Gm1.l{1} /\
    0 <= Game_1.l{1} < max_qr /\
    O_Gm1.validQs{1} = true /\
    O_Gm1.qlist{1} = Array.empty /\ f{1} = F.fn /\
    (O_Gm1.pk{1}, O_Gm1.sk{1}) = (VC.kGen (F.fn) r1{1}) /\
    pk{1} = O_Gm1.pk{1} /\ ={Game_1.l,O_Gm1.l,O_Gm1.qlist,O_Gm1.validQs,r1,rg,O_Gm1.pk,O_Gm1.sk,pk,f}
    ).
    wp.
    call (_ :
    ={glob ADV, glob RP1, glob RP2} /\  O_Gm1.qlist{1} = Array.empty /\ ={l}
    ==>
    ={glob ADV, glob RP1, glob RP2} /\  O_Gm1.qlist{1} = Array.empty /\ ={res}
    ).
    proc.
    while (={i,n,q,r,glob ADV, glob RP1, glob RP2} /\ O_Gm1.qlist{1}=Array.empty).
     wp; rnd; rnd; skip; smt.
    while (={i,n,q,m,r,glob ADV, glob RP1, glob RP2} /\ O_Gm1.qlist{1}=Array.empty).
     wp; rnd; rnd; rnd; skip; smt.
    wp;skip;smt.  (* Rand is consistent*) 
    wp;rnd;skip;progress;smt.
    seq 1 1 : (
={glob RP1, glob RP2} /\ 
    Game_1.l{1} = O_Gm1.l{1} /\
    0 <= Game_1.l{1} < max_qr /\
    f{1} = F.fn /\
    (O_Gm1.pk{1}, O_Gm1.sk{1}) = (VC.kGen (F.fn) r1{1}) /\
    pk{1} = O_Gm1.pk{1} /\ ={Game_1.l,O_Gm1.l,O_Gm1.qlist,O_Gm1.validQs,r1,rg,O_Gm1.pk,O_Gm1.sk,pk,f,i,pr}
    ).
    call (_ : ={glob RP1, glob RP2,pk} /\
    ={ O_Gm1.qlist,O_Gm1.validQs, O_Gm1.pk, O_Gm1.sk}
    ==> ={glob RP1, glob RP2} /\
    ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk,res}
    ).
    proc ( ={glob RP1, glob RP2} /\
    ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk}
    ).
    smt.
    by trivial.
    proc.
   case (! queryValid_VRF (F.fn)%F x{1}).
   rcondt {1} 1.
   intros &m0;skip;by trivial.
   rcondt {2} 1.
   intros &m0;skip;by smt.
   wp;skip;progress;smt.
   rcondf {1} 1.
   intros &m0;skip;by trivial.
   rcondf {2} 1.
   intros &m0;skip;by smt.
    inline RP(RP1, RP2).gen.
    seq 2 2 : 
      ( ={glob RP1, glob RP2} /\ ={x,f,rg} /\  
        ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk}).
    call (_ :
      ( ={glob RP1, glob RP2} /\
        ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk})
    ==>
      ( ={glob RP1, glob RP2} /\
        ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk,res})
    ).
    proc (
      ( ={ glob RP2} /\   
        ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk})
    ).
    by trivial.
    by trivial.
    wp;skip;by smt.
    seq 1 1 : 
      ( ={glob RP1, glob RP2} /\ ={x,f,rg,re} /\  
        ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk}).
    call (_ :
      ( ={glob RP1, glob RP2} /\
        ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk})
    ==>
      ( ={glob RP1, glob RP2} /\
        ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk,res})
    ).
    proc (
      ( ={ glob RP1} /\   
        ={ O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk})
    ).
    by trivial.
    by trivial.
    skip;by smt.
    case (length O_Gm1.qlist{1} >= max_qr).
    rcondt {1} 3.
    intros &m0;wp;skip;by trivial.
    rcondt {2} 3.
    intros &m0;wp;skip;by trivial.
    wp;skip;progress;smt.
    rcondf {1} 3.
    intros &m0;wp;skip;by trivial.
    rcondf {2} 3.
    intros &m0;wp;skip;by trivial.
    wp;skip;progress;smt.
    skip;progress;smt.
    inline Game_1C(RG, RP(RP1, RP2), ADV).O.check.
    wp;skip;progress;smt.
    (************)
    byequiv H.
    by trivial.
    by trivial.
    qed.
 
   (* Technicality: the adversary cannot win out of range l. *)
 
   lemma outofrange2 : 
        forall &m,
        Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : 
              res /\ Game_1.l >= max_qr] = 0%r.
   proof.
   intros &m.
   cut H : (phoare [
      Game_1(RG, RP(RP1, RP2), ADV).main : 
          true ==> res /\ Game_1.l >= max_qr ] = 0%r).
   proc.
   hoare.
   seq 1 : (0 <= Game_1.l < max_qr).
   rnd;wp;skip;by smt.
   inline  Game_1(RG, RP(RP1, RP2), ADV).Game1C.main.
   seq 3 : (0 <= Game_1.l < max_qr).
   call (_ : true ==> true).
   proc (true).
   by trivial.
   by trivial.
   proc.
   inline RP(RP1, RP2).gen.
   wp.
   case ((! queryValid_VRF (F.fn)%F x)).
   rcondt 1.
   by trivial.
   by trivial.
   rcondf 1.
   by trivial.
   by trivial.
   inline  Game_1C(RG, RP(RP1, RP2), ADV).O.init.
   inline RG.gen Rand.gen.
   wp; while (0 <= Game_1.l < max_qr).
    by wp; rnd; rnd; skip; smt.
   wp; while (0 <= Game_1.l < max_qr).
    by wp; rnd; rnd; rnd; skip; smt.
   wp;skip;smt.
   inline Game_1C(RG, RP(RP1, RP2), ADV).O.check.
   wp;skip;by smt.
   byphoare H.
   by trivial.
   by trivial.
   qed.
 
   (* First hop reflects the multiplicative effect of guessing the query. *)
 
   local lemma auth_hop1 : 
      forall  &m,
       Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main()@ &m:res] = 
            VCSec.max_qr%r * Pr[Game_1(RG, RP(RP1,RP2), ADV).main()@ &m:res].
   proof.
   intros &m.
   cut ->: Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main()@ &m:res]
           = sum (fun k,
                   Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main()@ &m:res /\ Game_Ver.i = k])
                 (interval 0 (max_qr-1)).
    cut H : ( forall l, 0 <= l <= max_qr =>
      Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main()@ &m:res]
           = sum (fun k,
                Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main()@ &m:res /\ Game_Ver.i = k])
                 (interval 0 (l-1)) + 
             Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main()@ &m:res /\ Game_Ver.i >= l]
    ).
    intros l [llbound lubound].
    clear lubound; move: llbound; elim/Int.Induction.induction l.
     cut -> : ( interval 0 (0-1) = (FSet.empty)).
     smt.
     cut -> : (sum
  (fun (k : int),
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = k])
  ((FSet.empty))%FSet = 0%r).
   delta sum;beta. 
     rewrite FSet.fold_empty. 
     reflexivity.     
     simplify.
     apply (events_overlap1 &m).
     intros i notbasecase assump.
     simplify.
     rewrite assump.
     cut -> : (
        Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i >= i] =
        Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = i] +
        Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i >= i + 1]
     ).
     cut -> : (
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i >= i] = 
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : 
           ((res /\ Game_Ver.i >= i) /\ Game_Ver.i = i) \/
           ((res /\ Game_Ver.i >= i) /\ !Game_Ver.i = i)]
     ).
     rewrite Pr [mu_eq].
     smt.
     by trivial.
     rewrite Pr [mu_or].
     cut -> : (
Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m :
   ((res /\ Game_Ver.i >= i) /\ Game_Ver.i = i) /\
   (res /\ Game_Ver.i >= i) /\ ! Game_Ver.i = i] = 
    Pr [ Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : false ]
     ).
     rewrite Pr [mu_eq].
     smt.
     by trivial.
     cut -> : (Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : false] = 0%r).
     rewrite Pr [mu_false].
     by trivial.
     simplify.
     cut -> : (
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m :
   (res /\ Game_Ver.i >= i) /\ Game_Ver.i = i] = 
Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m :
   res /\  Game_Ver.i = i]).
     rewrite Pr [mu_eq].
     smt.
     by trivial.
    cut -> : (
Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m :
   (res /\ Game_Ver.i >= i) /\ ! Game_Ver.i = i] =
Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i >= i + 1]).
rewrite Pr [mu_eq].
     smt.
     by trivial.
     reflexivity.
     cut -> : (
sum
  (fun (k : int),
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = k])
  (interval 0 (i - 1)) +
(Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = i] +
 Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i >= i + 1])
 =
(sum
  (fun (k : int),
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = k])
  (interval 0 (i - 1)) +
Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = i]) +
 Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i >= i + 1]
     ).
    pose c := Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i >= i + 1].
    pose a := sum
  (fun (k : int),
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = k])
  (interval 0 (i - 1)). 
    pose b := Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = i].
    cut -> : (sum
  (fun (k : int),
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = k])
  (interval 0 (i - 1)) = a).    
    rewrite /a;reflexivity.
    cut -> : (
      Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = i] = b
    ).
    rewrite /b;reflexivity.
    smt.
    cut -> : (
sum
  (fun (k : int),
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = k])
  (interval 0 (i - 1)) +
Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = i] = 
sum
  (fun (k : int),
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_Ver.i = k])
  (interval 0 (i))
    ).
   rewrite {2} /sum.
   rewrite (FSet.foldC (i)).
   smt.
   smt.
   beta.
   cut -> : (FSet.rm i (interval 0 i) = interval 0 (i-1)).
   cut -> : ((interval 0 i) = FSet.add i (interval 0 (i-1))).
   smt.
   rewrite FSet.rm_add_eq.
   smt. 
   rewrite {1} /sum.
   reflexivity.
   cut -> : ((i + 1 - 1) = (i)).
   smt.
   simplify;reflexivity.
   rewrite (H (max_qr)).
   smt.
    rewrite (outofrange1 &m).
   simplify;reflexivity.
   cut H : (
      forall k, 0 <= k < max_qr =>
         Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main()@ &m:res /\ Game_Ver.i = k] =
         Pr[Game_1C(RG, RP(RP1, RP2), ADV).main(k) @ &m : res  /\ Game_1C.O.lR = k]
   ).
   intros k H.
   by byequiv (conditional_equiv (k) _).
   cut ->:= sum_eq (fun k, Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main() @ &m:res /\ Game_Ver.i = k])
                   (fun k, Pr[Game_1C(RG, RP(RP1,RP2), ADV).main(k) @ &m:res /\ Game_1C.O.lR = k])
                   (interval 0 (max_qr-1)) _.
     by intros=> x x_in_1_q /=; apply (H x); first smt.
   cut H1: forall k, 0 <= k < max_qr =>
             Pr[Game_1C(RG, RP(RP1, RP2), ADV).main(k) @ &m : res]
             = Pr[Game_1C(RG, RP(RP1, RP2), ADV).main(k) @ &m : res  /\ Game_1C.O.lR = k].
   intros k kbound.
   cut H1 : equiv [ Game_1C(RG, RP(RP1, RP2), ADV).main ~ Game_1C(RG, RP(RP1, RP2), ADV).main : ={glob ADV,glob RP1,glob RP2} /\ lC{1} = k /\ lC{2} = k ==> res{1} <=> res{2} /\ Game_1C.O.lR{2} = k].
   apply (only_wins_on_k (k) _).
   by trivial.
   byequiv H1.
   by trivial.
   by trivial.
   cut <-:= sum_eq (fun k, Pr[Game_1C(RG, RP(RP1,RP2), ADV).main(k) @ &m:res])
                   (fun k, Pr[Game_1C(RG, RP(RP1,RP2), ADV).main(k) @ &m:res /\ Game_1C.O.lR = k])
                   (interval 0 (max_qr-1)) _.
     by intros=> x x_in_q_1 /=; apply (H1 x); first smt.
   cut ->: Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res]
           = sum (fun k,
                    Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
                 (interval 0 (max_qr-1)).
    (* Begin This follows from induction on the and split lemma *)
    cut H2 : ( forall ll, 0 <= ll <= max_qr =>
      Pr[Game_1(RG, RP(RP1,RP2), ADV).main()@ &m:res]
           = sum (fun k,
                Pr[Game_1(RG, RP(RP1,RP2), ADV).main()@ &m:res /\ Game_1.l = k])
                 (interval 0 (ll-1)) + 
             Pr[Game_1(RG, RP(RP1,RP2), ADV).main()@ &m:res /\ Game_1.l >= ll]
    ).
    intros ll [llbound X]; clear X; move: llbound.
    elim/Int.Induction.induction ll.
     cut -> : ( interval 0 (0-1) = (FSet.empty)).
     smt.
     cut -> : (sum
  (fun (k : int),
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
  ((FSet.empty))%FSet = 0%r).
   delta sum;beta. 
     rewrite FSet.fold_empty. 
     reflexivity.     
     simplify.
     apply (events_overlap2 &m).
     intros i notbasecase assump.
     simplify.
     rewrite assump.
     cut -> : (
        Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l >= i] =
        Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = i] +
        Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l >= i + 1]
     ).
     cut -> : (
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l >= i] = 
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : 
           ((res /\ Game_1.l >= i) /\ Game_1.l = i) \/
           ((res /\ Game_1.l >= i) /\ !Game_1.l = i)]
     ).
     rewrite Pr [mu_eq].
     smt.
     by trivial.
     rewrite Pr [mu_or].
     cut -> : (
Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m :
   ((res /\ Game_1.l >= i) /\ Game_1.l = i) /\
   (res /\ Game_1.l >= i) /\ ! Game_1.l = i] = 
    Pr [ Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : false ]
     ).
     rewrite Pr [mu_eq].
     smt.
     by trivial.
     cut -> : (Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : false] = 0%r).
     rewrite Pr [mu_false] //.
     simplify.
     cut -> : (
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m :
   (res /\ Game_1.l >= i) /\ Game_1.l = i] = 
Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m :
   res /\  Game_1.l = i]).
     rewrite Pr [mu_eq].
     smt.
     by trivial.
    cut -> : (
Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m :
   (res /\ Game_1.l >= i) /\ ! Game_1.l = i] =
Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l >= i + 1]).
rewrite Pr [mu_eq].
     smt.
     by trivial.
     reflexivity.
     cut -> : (
sum
  (fun (k : int),
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
  (interval 0 (i - 1)) +
(Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = i] +
 Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l >= i + 1])
 =
(sum
  (fun (k : int),
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
  (interval 0 (i - 1)) +
Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = i]) +
 Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l >= i + 1]
     ).
    pose c := Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l >= i + 1].
    pose a := sum
  (fun (k : int),
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
  (interval 0 (i - 1)). 
    pose b := Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = i].
    cut -> : (sum
  (fun (k : int),
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
  (interval 0 (i - 1)) = a).    
    rewrite /a;reflexivity.
    cut -> : (
      Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = i] = b
    ).
    rewrite /b;reflexivity.
    smt.
    cut -> : (
sum
  (fun (k : int),
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
  (interval 0 (i - 1)) +
Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = i] = 
sum
  (fun (k : int),
     Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
  (interval 0 (i))
    ).
   rewrite {2} /sum.
   rewrite (FSet.foldC (i)).
   smt.
   smt.
   beta.
   cut -> : (FSet.rm i (interval 0 i) = interval 0 (i-1)).
   cut -> : ((interval 0 i) = FSet.add i (interval 0 (i-1))).
   smt.
   rewrite FSet.rm_add_eq.
   smt. 
   rewrite {1} /sum.
   reflexivity.
   cut -> : ((i + 1 - 1) = (i)).
   smt.
   simplify;reflexivity.
   rewrite (H2 (max_qr)).
   smt.
   rewrite (outofrange2 &m).
   simplify;reflexivity.
    (* End inductive argument *)
   cut H2: forall k, 0 <= k < max_qr =>
             Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k ]
             = 1%r / max_qr%r * Pr[Game_1C(RG, RP(RP1, RP2), ADV).main(k) @ &m : res].
     intros=> k k_bnd.
     byphoare (_: true ==> res /\ Game_1.l = k)=> //.
       pose prb:= Pr[Game_1C(RG,RP(RP1,RP2),ADV).main(k) @ &m: res].
       proc.
       seq  1: (Game_1.l = k) (1%r/max_qr%r) prb _ 0%r=> //.
         rnd; skip; progress.
         cut ->: (fun x, x = k) = ((=) (k)).
           by apply ExtEq.fun_ext=> x /=; smt.
         by rewrite -/(Distr.mu_x _ _) Distr.Dinter.mu_x_def_in; smt.
       call (_ : Game_1.l = k /\ lC = k ==>  res /\ Game_1.l = k) => //.
       delta prb. 
       bypr. 
       intros &m0 kval.
       byequiv (_ : Game_1.l{1} = k /\ lC{1} = k /\ lC{2} = k
                    ==> 
     res{1} /\ Game_1.l{1} = k <=> res{2} 
     ) => //.     
     proc.
     (* Begin games match*)
     inline Game_1C(RG, RP(RP1, RP2), ADV).O.init RG.gen.
     seq 5 5 : (
Game_1.l{1} = k /\ lC{1} = k /\ lC{2} = k /\
lC0{1} = lC{1} /\  lC0{2} = lC{2} /\                                  
O_Gm1.l{1} = lC0{1} /\  O_Gm1.l{2} = lC0{2} /\                              
O_Gm1.qlist{1} = Array.empty /\  O_Gm1.qlist{2} = Array.empty /\
O_Gm1.validQs{1} = true /\ O_Gm1.validQs{2} = true /\
f{1} = (F.fn)%F /\ f{2} = (F.fn)%F). 
     wp;skip;by trivial.
     seq 1 1 : (
Game_1.l{1} = k /\ lC{1} = k /\ lC{2} = k /\
lC0{1} = lC{1} /\  lC0{2} = lC{2} /\                                  
O_Gm1.l{1} = lC0{1} /\  O_Gm1.l{2} = lC0{2} /\                              
O_Gm1.validQs{1} = true /\ O_Gm1.validQs{2} = true /\
O_Gm1.qlist{1} = Array.empty /\  O_Gm1.qlist{2} = Array.empty /\
f{1} = (F.fn)%F /\ f{2} = (F.fn)%F /\ ={rg}).
     call (_ :
O_Gm1.validQs{1} = true /\ O_Gm1.validQs{2} = true /\
O_Gm1.qlist{1} = Array.empty /\  O_Gm1.qlist{2} = Array.empty /\
 ={l}
==>
O_Gm1.validQs{1} = true /\ O_Gm1.validQs{2} = true /\
O_Gm1.qlist{1} = Array.empty /\  O_Gm1.qlist{2} = Array.empty
/\ ={res}
     ).
     proc.
     inline Rand.gen.
     while (={O_Gm1.validQs,O_Gm1.qlist,l,i,n,q,r}/\ O_Gm1.validQs{1}=true /\ O_Gm1.qlist{1}=Array.empty).
      by wp; rnd; rnd; skip; progress.
     while (={O_Gm1.validQs,O_Gm1.qlist,l,i,n,q,m,r}/\ O_Gm1.validQs{1}=true /\ O_Gm1.qlist{1}=Array.empty).
      by wp; rnd; rnd; rnd; skip; progress.
     by wp;skip;progress.  (* Rand is consistent*) 
     by skip; progress. 
     seq 3 3 : (
Game_1.l{1} = k /\ lC{1} = k /\ lC{2} = k /\
lC0{1} = lC{1} /\  lC0{2} = lC{2} /\                                  
O_Gm1.l{1} = lC0{1} /\  O_Gm1.l{2} = lC0{2} /\                              
O_Gm1.validQs{1} = true /\ O_Gm1.validQs{2} = true /\
O_Gm1.qlist{1} = Array.empty /\  O_Gm1.qlist{2} = Array.empty /\
f{1} = (F.fn)%F /\ f{2} = (F.fn)%F /\ ={rg,r0,O_Gm1.pk,O_Gm1.sk,pk}).
    wp;skip;by trivial.
     seq 1 1 : (
Game_1.l{1} = k /\ lC{1} = k /\ lC{2} = k /\
lC0{1} = lC{1} /\  lC0{2} = lC{2} /\                                  
O_Gm1.l{1} = lC0{1} /\  O_Gm1.l{2} = lC0{2} /\                              
O_Gm1.validQs{1} = O_Gm1.validQs{2} /\
O_Gm1.qlist{1} =  O_Gm1.qlist{2} /\
f{1} = (F.fn)%F /\ f{2} = (F.fn)%F /\ ={rg,r0,O_Gm1.pk,O_Gm1.sk,pk,i,pr}).
    call (_ : 
O_Gm1.validQs{1} = O_Gm1.validQs{2}  /\
O_Gm1.qlist{1} = O_Gm1.qlist{2} /\
={O_Gm1.pk,O_Gm1.sk,pk}
==>
O_Gm1.validQs{1} = O_Gm1.validQs{2} /\
O_Gm1.qlist{1} =  O_Gm1.qlist{2}  /\
={O_Gm1.pk,O_Gm1.sk,res}
   ).
   proc (O_Gm1.qlist{1}  = O_Gm1.qlist{2}  /\
O_Gm1.validQs{1} = O_Gm1.validQs{2} /\
={O_Gm1.pk,O_Gm1.sk}).
   by progress.
   by progress.
   proc.
   case (! queryValid_VRF (F.fn)%F x{1}).
   rcondt {1} 1.
   intros &m1;skip;by trivial.
   rcondt {2} 1.
   by intros &m1;skip;progress.
   wp;skip;progress;smt.
   rcondf {1} 1.
   intros &m1;skip;by trivial.
   rcondf {2} 1.
   intros &m1;skip;by progress.
   inline RP(RP1, RP2).gen.
   seq 2 2 : (={x, O_Gm1.qlist,O_Gm1.validQs, O_Gm1.pk, O_Gm1.sk,f,rg}).
   call (_ : ={O_Gm1.qlist,O_Gm1.validQs, O_Gm1.pk, O_Gm1.sk} ==> ={O_Gm1.qlist,O_Gm1.validQs, O_Gm1.pk, O_Gm1.sk,res}).
   proc (={O_Gm1.qlist,O_Gm1.validQs, O_Gm1.pk, O_Gm1.sk}).
   by progress.
   by progress.
   wp;skip;by trivial.
   seq 1 1 : (={x, O_Gm1.qlist,O_Gm1.validQs, O_Gm1.pk, O_Gm1.sk,f,rg,re}).
   call (_ : ={O_Gm1.qlist, O_Gm1.validQs,O_Gm1.pk, O_Gm1.sk} ==> ={O_Gm1.qlist,O_Gm1.validQs, O_Gm1.pk, O_Gm1.sk,res}).
   proc (={ O_Gm1.qlist,O_Gm1.validQs, O_Gm1.pk, O_Gm1.sk}).
   by progress.
   by progress.
   wp;skip;by trivial.
   wp;skip;by progress.
   skip;by progress.   
   inline Game_1C(RG, RP(RP1, RP2), ADV).O.check.
   seq 3 3 : (
Game_1.l{1} = k /\ lC{1} = k /\ lC{2} = k /\
  lC0{1} = lC{1} /\
  lC0{2} = lC{2} /\
  O_Gm1.l{1} = lC0{1} /\
  O_Gm1.l{2} = lC0{2} /\
  ={O_Gm1.qlist,O_Gm1.validQs} /\
  f{1} = (F.fn)%F /\
  f{2} = (F.fn)%F /\ ={rg, r0, O_Gm1.pk, O_Gm1.sk, pk, i, pr} /\
ip{1} = i{1} /\ ip{2} = i{2} /\                                 
pr0{1} = pr{1} /\ pr0{2} = pr{2} /\                                
O_Gm1.lR{1} = ip{1} /\ O_Gm1.lR{2} = ip{2}     
   ).
   wp;skip;by trivial.
   case (ip{1} >= 0 /\ ip{1} < length O_Gm1.qlist{1} /\ length O_Gm1.qlist{2} = max_qr /\ O_Gm1.validQs{1} = true).
   rcondt {1} 1.
   intros &m1. skip;by progress.
   rcondt {2} 1.
   intros &m1. skip;by progress.
   case (O_Gm1.lR{1} = O_Gm1.l{1}).
   rcondt {1} 3.
   intros &m1. wp;skip;by progress.
   rcondt {2} 3.
   intros &m1. wp;skip;by progress.
   wp;skip;progress;smt.
   rcondf {1} 3.
   intros &m1. wp;skip;by progress.
   rcondf {2} 3.
   intros &m1. wp;skip;by progress.
   wp;skip;progress;smt.
   rcondf {1} 1.
   intros &m1. skip;by progress.
   rcondf {2} 1.
   intros &m1. skip;by progress.
   wp;skip;progress;smt.
     (* End games match *)
     hoare.
     inline Game_1(RG, RP(RP1, RP2), ADV).Game1C.main Game_1C(RG, RP(RP1, RP2), ADV).O.init RG.gen.
     seq 11 : (! Game_1.l = k).
     wp.
     call (_ : (! Game_1.l = k) ==> (! Game_1.l = k)).
     proc (! Game_1.l = k) .
     by trivial.
     by trivial.
     proc.
     inline RP(RP1, RP2).gen.
     wp.
   case (! queryValid_VRF (F.fn)%F x).
   rcondt 1.
   skip;by trivial.
   wp;skip;progress;smt.
   rcondf 1.
   skip;by trivial.
   wp.
     call (_ : (! Game_1.l = k) ==> (! Game_1.l = k)).
     proc (! Game_1.l = k) .
     by trivial.
     by trivial.
     call (_ : (! Game_1.l = k) ==> (! Game_1.l = k)).
     proc (! Game_1.l = k) .
     by trivial.
     by trivial.     
     wp;skip;smt. 
     wp.
     inline Rand.gen.
     wp; while (true).
      by wp; rnd; rnd; skip; progress.
     while (true).
      by wp; rnd; rnd; rnd; skip; progress.
     by wp;skip;progress.  (* Doesn't touch Game_1.l *)
     inline Game_1C(RG, RP(RP1, RP2), ADV).O.check.
     wp;skip;by smt.
  cut ->:= sum_eq (fun k, Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res /\ Game_1.l = k])
                  (fun k, 1%r / max_qr%r * Pr[Game_1C(RG, RP(RP1, RP2), ADV).main(k) @ &m : res])
                  (interval 0 (max_qr-1)) _.
    by intros=> x x_in_1_q /=; apply (H2 x); smt.
  cut ->: sum (fun k, 1%r/max_qr%r * Pr[Game_1C(RG,RP(RP1,RP2),ADV).main(k) @ &m: res]) (interval 0 (max_qr-1))
          = 1%r/max_qr%r * sum (fun k, Pr[Game_1C(RG,RP(RP1,RP2),ADV).main(k) @ &m: res]) (interval 0 (max_qr-1)).
    generalize (1%r/max_qr%r)=> factor.
    cut: 0 <= max_qr by smt. 
    elim/Induction.induction max_qr.
      by rewrite interval_neg 2:!sum_empty; smt.
      intros=> k leq0_k IH.
      cut:= sum_ij_le_r 0 (k + 1 - 1) (fun k, factor * Pr[Game_1C(RG,RP(RP1,RP2),ADV).main(k) @ &m: res]) _.
        smt.
      rewrite /sum_ij=> -> /=.
      cut:= sum_ij_le_r 0 (k + 1 - 1) (fun k, Pr[Game_1C(RG,RP(RP1,RP2),ADV).main(k) @ &m: res]) _.
        smt.
      rewrite /sum_ij=> -> /=.
      rewrite (_: k + 1 - 1 = k) 2:IH; first smt.
      smt.
   cut: 0 < max_qr by smt. 
move => Hmaxqr.
pose x := sum _ _.
generalize x; smt.
   qed.
 
  (* Hop 1 - 2 down to multi-query fhe game *)
 
   (* Here's the body of the distinguishing adversary *)
 
local module D(O : O_IND'_t) : FHE.Adv_IND'_t(O) = {
  module O_D = O_D(O,RP1,RP2)
  module A = ADV(O_D)
 
  proc guess() : bool  = {
      var pk : VC.pkey_t;
      var pr : VC.proof_t;
      var i : int;
      var r : bool;
      var l : int;
       
      l = $[0..max_qr-1];
 
      pk = O_D.init(l);
      (i,pr) = A.forge(pk);
      r = O_D.check(i,pr);
      return r;
  }
}.
 
   (* Left-hand side of interpolation *)
 
local lemma gm1_eq_true : 
       equiv [ Game_1(RG, RP(RP1,RP2),ADV).main ~ 
               FHE.Game_IND'(RP1,RP2,D).main : ={glob ADV, glob RP1, glob RP2} /\ b{2} ==> res{1} <=> res{2} ].
     proof.
     proc.
     inline Game_IND'(RP1, RP2, D).O.init Game_1(RG, RP(RP1, RP2), ADV).Game1C.main Game_IND'(RP1, RP2, D).A.guess Game_1C(RG, RP(RP1, RP2), ADV).O.init Game_IND'(RP1, RP2, D).O.init D(O_IND'(RP1, RP2)).O_D.init .
     seq 9 14 : (
       ={glob ADV, glob RP1, glob RP2} /\ b{2} /\
       0<=  Game_1.l{1} < max_qr /\
       Game_1.l{1} = l{2} /\ lC{1} = Game_1.l{1} /\ 
       O_Gm1.l{1} = Game_1.l{1} /\ Game_1.l{1} =  O_D.l{2} /\
       lC{2} = l{2} /\  O_D.l{2} = lC{2} /\
       O_Gm1.qlist{1} = Array.empty /\ O_D.qlist{2} = Array.empty /\
       bval{2} = b{2} /\ O_IND'.count{2} = 0 /\
       O_IND'.b{2} = bval{2} /\ r1{1} = r0{2} /\ O_Gm1.pk{1} = O_D.pk{2} /\
       O_Gm1.sk{1} = O_D.sk{2} /\ pk{2} = O_D.pk{2} /\ pk{1} = O_Gm1.pk{1} /\ 
       O_D.validQs{2} = true /\
       O_Gm1.validQs{1} = true /\ O_IND'.validQs{2} = true).
     swap {2} 5 -4.
     wp.
     call {2} (_ : true ==> true).
     proc.
     inline Rand.gen.
     wp; while (true) (n+q-i).
      move => z; wp; rnd; rnd; skip; progress; smt.
     wp; while (true) (n+q-m-i).
      by move=> z; auto; progress; expect 4 smt.
     by auto; smt.

     inline RG.gen.
     wp.
     call (_ : 
  ={glob ADV, glob RP1, glob RP2,l} /\ O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
  O_Gm1.l{1} = Game_1.l{1} /\ Game_1.l{1} =  O_D.l{2} /\
  O_Gm1.validQs{1} = true /\
       O_D.validQs{2} = true /\
  O_IND'.validQs{2} = true /\ O_IND'.count{2} = 0 /\
  O_Gm1.qlist{1} = Array.empty /\
  O_D.qlist{2} = Array.empty
 ==>  
  ={glob ADV, glob RP1, glob RP2,res} /\  O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
  O_Gm1.l{1} = Game_1.l{1} /\ Game_1.l{1} =  O_D.l{2} /\
  O_Gm1.validQs{1} = true /\
       O_D.validQs{2} = true /\
  O_IND'.validQs{2} = true /\ O_IND'.count{2} = 0 /\
  O_Gm1.qlist{1} = Array.empty /\
  O_D.qlist{2} = Array.empty
     ).
  proc.
  inline Rand.gen.
   while (={glob ADV, glob RP1, glob RP2, l, i, n, q, r} /\ 
  O_IND'.b{2} /\
  0 <= Game_1.l{1} < max_qr /\
  O_Gm1.l{1} = Game_1.l{1} /\
  Game_1.l{1} = O_D.l{2} /\
  O_Gm1.validQs{1} = true /\
  O_D.validQs{2} = true /\
  O_IND'.validQs{2} = true /\
  O_IND'.count{2} = 0 /\ O_Gm1.qlist{1} = Array.empty /\ O_D.qlist{2} = Array.empty).
    wp; rnd; rnd; skip; smt.
   while (={glob ADV, glob RP1, glob RP2, l, i, n, q, m, r} /\ 
  O_IND'.b{2} /\
  0 <= Game_1.l{1} < max_qr /\
  O_Gm1.l{1} = Game_1.l{1} /\
  Game_1.l{1} = O_D.l{2} /\
  O_Gm1.validQs{1} = true /\
  O_D.validQs{2} = true /\
  O_IND'.validQs{2} = true /\
  O_IND'.count{2} = 0 /\ O_Gm1.qlist{1} = Array.empty /\ O_D.qlist{2} = Array.empty).
    wp; rnd; rnd; rnd; skip; smt.
   wp;skip;progress.
  wp.
   rnd;skip;progress;smt.
  (* Before forge is launched -> forge is finished *)
  seq 1 1 : (
 ={glob ADV, glob RP1, glob RP2,i,pr} /\  O_IND'.b{2} /\
  b{2} /\
       0<=  Game_1.l{1} < max_qr /\
  Game_1.l{1} = l{2} /\
  lC{1} = Game_1.l{1} /\
  lC{2} = l{2} /\
  O_D.l{2} = lC{2} /\
  O_Gm1.l{1} = Game_1.l{1} /\ Game_1.l{1} =  O_D.l{2} /\
  length O_Gm1.qlist{1} = length O_D.qlist{2} /\
  0 <= length O_Gm1.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm1.qlist{1} /\
  (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
  O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] = 
  O_D.qlist{2}.[length O_D.qlist{2}- max_qr + O_D.l{2}]) /\
  bval{2} = b{2} /\
  O_IND'.b{2} = bval{2} /\
  r1{1} = r0{2} /\
  O_Gm1.pk{1} = O_D.pk{2} /\
  O_Gm1.sk{1} = O_D.sk{2} /\ 
  pk{2} = O_D.pk{2} /\ pk{1} = O_Gm1.pk{1} /\ 
  O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true
  ).
  call (_ :
 ={glob ADV, glob RP1, glob RP2,pk} /\ O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
  O_Gm1.l{1} = Game_1.l{1} /\ Game_1.l{1} =  O_D.l{2} /\
  length O_Gm1.qlist{1} = length O_D.qlist{2} /\
  0 <= length O_Gm1.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm1.qlist{1} /\
  (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
  O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] = 
  O_D.qlist{2}.[length O_D.qlist{2}- max_qr + O_D.l{2}]) /\
  O_Gm1.pk{1} = O_D.pk{2} /\
  O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true
  ==>
 ={glob ADV, glob RP1, glob RP2,res} /\
       0<=  Game_1.l{1} < max_qr /\
  O_Gm1.l{1} = Game_1.l{1} /\ Game_1.l{1} =  O_D.l{2} /\
  length O_Gm1.qlist{1} = length O_D.qlist{2} /\
  0 <= length O_Gm1.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm1.qlist{1} /\
  (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
  O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] = 
  O_D.qlist{2}.[length O_D.qlist{2}- max_qr + O_D.l{2}]) /\
  O_Gm1.pk{1} = O_D.pk{2} /\
  O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true
  ).
  proc (
  ={glob RP1, glob RP2} /\ O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
  O_Gm1.l{1} = Game_1.l{1} /\ Game_1.l{1} =  O_D.l{2} /\
  length O_Gm1.qlist{1} = length O_D.qlist{2} /\
  0 <= length O_Gm1.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm1.qlist{1} /\
  (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
  O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] = 
  O_D.qlist{2}.[length O_D.qlist{2}- max_qr + O_D.l{2}]) /\
  O_Gm1.pk{1} = O_D.pk{2} /\
  O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true
  ).
  smt.
  smt.
  (* begin pgen preserves invariant *)
  proc.
  case (queryValid_VRF (F.fn)%F x{1}).
  rcondf {1} 1.
  intros &m;skip;by trivial.
  rcondf {2} 1.
  intros &m;skip;by smt.
  case (length O_D.qlist{2} = max_qr - 1 - O_D.l{2}).
  rcondt {2} 1.
  intros &m;skip;smt.
  wp.
  call (_ : ={glob RP1, glob RP2,f} ==> ={glob RP1, glob RP2,res}).
  proc.
  call (_ : ={glob RP1,glob RP2} ==> ={glob RP1, glob RP2,res}).
  proc (={glob RP1}).
  by trivial.
  by trivial.
  call (_ : ={glob RP1, glob RP2} ==> ={res,glob RP1, glob RP2}).
  proc (={glob RP2}).
  by trivial.
  by trivial.
  skip;smt.
  skip;progress.
  smt.
  smt.
  smt.
  smt.
  smt.
  smt.
  cut -> : forall x1 x2, (((x{2}, x1, x2) :: O_Gm1.qlist{1}).[length ((x{2}, x1, x2) :: O_Gm1.qlist{1}) - max_qr +
                                    O_D.l{2}] = ((x{2}, x1, x2))).
   move => x1 x2; rewrite get_cons.
   smt.
   smt.
  cut -> : forall x1 x2, (((x{2}, x1, x2) :: O_D.qlist{2}).[length ((x{2}, x1, x2) :: O_D.qlist{2}) -
                                  max_qr + O_D.l{2}] = ((x{2}, x1, x2))).
   move => x1 x2; rewrite get_cons.
   smt.
   smt.
  reflexivity.  
  rcondf {2} 1.
  intros &m;skip;smt.
  wp.
  inline O_IND'(RP1, RP2).enc RP(RP1, RP2).gen.
  rcondf{2} 4.
  intros &m;wp;skip.
  progress.
  delta FHE.queryValid_IND phi Scheme.Input.inputG_len inputG_len Garble2.Input.inputG_len Garble1.C2.Input.inputG_len;beta.
  cut ->: (
 (fst
     (let (fn, iK, oK) = O_D.dummy_sk{hr} in encode iK x{hr},
      let (fn, iK, oK) = O_D.sk{hr} in encode iK x{hr})) =
(let (fn, iK, oK) = O_D.dummy_sk{hr} in encode iK x{hr})
  ).
   smt.
  cut ->: (
 (snd
     (let (fn, iK, oK) = O_D.dummy_sk{hr} in encode iK x{hr},
      let (fn, iK, oK) = O_D.sk{hr} in encode iK x{hr})) =
(let (fn, iK, oK) = O_D.sk{hr} in encode iK x{hr})
  ).
   smt.
   smt.
  swap {1} [2..3] -1.
  swap {2} 4 -3.
  swap {2} 7 -5.
  seq 2 2 : (={x,rg,re} /\
    ={glob RP1, glob RP2} /\
    O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
    O_Gm1.l{1} = Game_1.l{1} /\
    Game_1.l{1} = O_D.l{2} /\
    length O_Gm1.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm1.qlist{1} <= max_qr /\
    O_IND'.count{2} <= length O_Gm1.qlist{1} /\
    (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
     O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm1.pk{1} = O_D.pk{2} /\
    O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
   queryValid_VRF (F.fn)%F x{1} /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}).
  call (_ : 
    ={glob RP1, glob RP2} /\
    O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
    O_Gm1.l{1} = Game_1.l{1} /\
    Game_1.l{1} = O_D.l{2} /\
    length O_Gm1.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm1.qlist{1} <= max_qr /\
   O_IND'.count{2} <= length O_Gm1.qlist{1} /\
    (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
     O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm1.pk{1} = O_D.pk{2} /\
    O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ==>
={res} /\ 
    ={glob RP1, glob RP2} /\
    O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
    O_Gm1.l{1} = Game_1.l{1} /\
    Game_1.l{1} = O_D.l{2} /\
    length O_Gm1.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm1.qlist{1} <= max_qr /\
    O_IND'.count{2} <= length O_Gm1.qlist{1} /\
    (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
     O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm1.pk{1} = O_D.pk{2} /\
    O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ).
  proc (={glob RP1} /\
    O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
    O_Gm1.l{1} = Game_1.l{1} /\
    Game_1.l{1} = O_D.l{2} /\
    length O_Gm1.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm1.qlist{1} <= max_qr /\
    O_IND'.count{2} <= length O_Gm1.qlist{1} /\
    (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
     O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm1.pk{1} = O_D.pk{2} /\
    O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ).
  smt.
  smt.
  call (_ : 
    ={glob RP1, glob RP2} /\
    O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
    O_Gm1.l{1} = Game_1.l{1} /\
    Game_1.l{1} = O_D.l{2} /\
    length O_Gm1.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm1.qlist{1} <= max_qr /\
    O_IND'.count{2} <= length O_Gm1.qlist{1} /\
    (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
     O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm1.pk{1} = O_D.pk{2} /\
    O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ==>
={res} /\
    ={glob RP1, glob RP2} /\
    O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
    O_Gm1.l{1} = Game_1.l{1} /\
    Game_1.l{1} = O_D.l{2} /\
    length O_Gm1.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm1.qlist{1} <= max_qr /\
    O_IND'.count{2} <= length O_Gm1.qlist{1} /\
    (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
     O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm1.pk{1} = O_D.pk{2} /\
    O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ).
  proc (={ glob RP2} /\
    O_IND'.b{2} /\
       0<=  Game_1.l{1} < max_qr /\
    O_Gm1.l{1} = Game_1.l{1} /\
    Game_1.l{1} = O_D.l{2} /\
    length O_Gm1.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm1.qlist{1} <= max_qr /\
    O_IND'.count{2} <= length O_Gm1.qlist{1} /\
    (length O_Gm1.qlist{1} > max_qr - 1 - Game_1.l{1} =>
     O_Gm1.qlist{1}.[length O_Gm1.qlist{1} - max_qr + Game_1.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm1.pk{1} = O_D.pk{2} /\
    O_Gm1.sk{1} = O_D.sk{2} /\
    O_Gm1.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true/\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ).
  smt.
  smt.
  skip;progress;smt.
  wp;skip;progress.
  smt.
  smt.
  smt.
  smt.
  smt.
  smt.
  smt.
  smt.
  rewrite get_cons.
  rewrite Array.length_cons.
  smt.
  cut -> : forall x10 x20, ((0 = length ((x{2}, x10, x20) :: O_Gm1.qlist{1}) - max_qr + O_D.l{2}) = false).
   move => x10 x20; rewrite Array.length_cons.
   smt.
  simplify.
  rewrite get_cons.
  rewrite Array.length_cons.
  smt.
  cut -> : ((0 = length ((x{2}, xp{2}, xs{2}) :: O_D.qlist{2}) - max_qr + O_D.l{2} ) = false).
  rewrite Array.length_cons.
  smt.
  simplify.
  smt.
  smt.
  smt.
  smt.
  smt.
  rcondt {1} 1.
  intros &m;wp;skip;by trivial.
  rcondt {2} 1.
  intros &m;wp;skip;by smt.
  inline O_IND'(RP1, RP2).enc.
  wp;skip;progress;smt.
  skip;progress;smt.
  (* END pgen PRESERVES INVARIANT *)
  inline   Game_1C(RG, RP(RP1, RP2), ADV).O.check Game_IND'(RP1, RP2, D).O.check D(O_IND'(RP1, RP2)).O_D.check.
  rcondt {2} 8.
  intros &m.
  wp;skip;smt.
  case (! O_D.validQs{2}).
  rcondt {2} 4.
  intros &m;wp;skip;smt.
  rcondf {1} 4.
  intros &m;wp;skip;smt.
  wp;skip;progress;smt.
  (*** HERE ***)
  rcondf {2} 4.
  intros &m;wp;skip;smt.
  wp;skip.
  intros &1 &2 H1.
  case (i{2} >= 0 /\ i{2} < length O_D.qlist{2} /\
     length O_D.qlist{2} = max_qr /\ O_IND'.validQs{2} = true).
  intros goodi.
  cut -> : ( i{1} >= 0 /\
       i{1} < length O_Gm1.qlist{1} /\
       length O_Gm1.qlist{1} = max_qr /\ O_Gm1.validQs{1} = true).
  smt.
  simplify.
  case (i{2} = O_D.l{2}).
  intros goodl.
  cut -> : (i{1} = O_Gm1.l{1}).
  cut -> : (i{1} = i{2}).
  smt.
  cut -> : (O_Gm1.l{1} = O_D.l{2}).
  smt.
  smt.
  smt.
  smt.
  smt.
  qed.
      
(* Right-hand side of the interpolation *)
 
local lemma gm2_eq_false : 
       equiv [ Game_2(RG, RP(RP1,RP2),ADV).main ~ 
               FHE.Game_IND'(RP1,RP2,D).main : ={glob ADV, glob RP1, glob RP2} /\ !b{2} ==> res{1} <=> !res{2} ].
     proof.
     proc.
     inline Game_IND'(RP1, RP2, D).O.init Game_2(RG, RP(RP1, RP2), ADV).main Game_IND'(RP1, RP2, D).A.guess Game_2(RG, RP(RP1, RP2), ADV).O.init Game_IND'(RP1, RP2, D).O.init D(O_IND'(RP1, RP2)).O_D.init .
     seq 10 14 : (={glob ADV, glob RP1, glob RP2} /\ !b{2} /\
                  O_Gm2.l{1} = l{2} /\ lC{1} = O_Gm2.l{1} /\ 
       0<=  O_Gm2.l{1} < max_qr /\ O_IND'.count{2} = 0 /\
  O_Gm2.l{1} = O_Gm2.l{1} /\ O_Gm2.l{1} =  O_D.l{2} /\
         lC{2} = l{2} /\  O_D.l{2} = lC{2} /\
    O_Gm2.qlist{1} = Array.empty /\ O_D.qlist{2} = Array.empty /\ bval{2} = b{2} /\ 
            O_IND'.b{2} = bval{2} /\ r0{1} = r0{2} /\ O_Gm2.pk{1} = O_D.pk{2} /\
           O_Gm2.sk{1} = O_D.sk{2} /\ pk{2} = O_D.pk{2} /\ pk{1} = O_Gm2.pk{1} /\ 
O_IND'.validQs{2} = true /\ O_Gm2.validQs{1} = true /\
  O_D.validQs{2} = true /\
O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2}).
     swap {2} 4 -3.
     wp.
     call (_ : ={f} ==> ={res}).
     proc.
     inline Rand.gen.
      wp; while (={f,i,n,q,r}).
       by wp; rnd; rnd; skip; progress.
      wp; while (={f,i,n,q,m,r}).
       by wp; rnd; rnd; rnd; skip; progress.
      by wp; skip; progress.
     wp.
  call (_ : ={glob ADV, glob RP1, glob RP2, f} ==> ={res}).
  proc; inline RG.gen Rand.gen.
  wp; while (={glob ADV, glob RP1, glob RP2, i, n, q, r}).
   by wp; rnd; rnd; skip; progress.
  wp; while (={glob ADV, glob RP1, glob RP2, i, n, q, m, r}).
   by wp; rnd; rnd; rnd; skip; progress.
  by wp;skip;progress.
  by wp; rnd; wp; skip; progress; smt.
  (* Before forge is launched -> forge is finished *)
  seq 1 1 : (
 ={glob ADV, glob RP1, glob RP2,i,pr} /\  !O_IND'.b{2} /\
  !b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
  O_Gm2.l{1} = l{2} /\
  lC{1} = O_Gm2.l{1} /\
  lC{2} = l{2} /\
  O_D.l{2} = lC{2} /\
  O_Gm2.l{1} = O_Gm2.l{1} /\ O_Gm2.l{1} =  O_D.l{2} /\
  length O_Gm2.qlist{1} = length O_D.qlist{2} /\
  0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
  (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
  O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] = 
  O_D.qlist{2}.[length O_D.qlist{2}- max_qr + O_D.l{2}]) /\
  bval{2} = b{2} /\
  O_IND'.b{2} = bval{2} /\
  r0{1} = r0{2} /\
  O_Gm2.pk{1} = O_D.pk{2} /\
  O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
  pk{2} = O_D.pk{2} /\ pk{1} = O_Gm2.pk{1} /\ 
     O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true
  ).
  call (_ :
 ={glob ADV, glob RP1, glob RP2,pk} /\ !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
  O_Gm2.l{1} = O_Gm2.l{1} /\ O_Gm2.l{1} =  O_D.l{2} /\
  length O_Gm2.qlist{1} = length O_D.qlist{2} /\
  0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
  (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
  O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] = 
  O_D.qlist{2}.[length O_D.qlist{2}- max_qr + O_D.l{2}]) /\
  O_Gm2.pk{1} = O_D.pk{2} /\
  O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
     O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true
  ==>
 ={glob ADV, glob RP1, glob RP2,res} /\!O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
  O_Gm2.l{1} = O_Gm2.l{1} /\ O_Gm2.l{1} =  O_D.l{2} /\
  length O_Gm2.qlist{1} = length O_D.qlist{2} /\
  0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
  (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
  O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] = 
  O_D.qlist{2}.[length O_D.qlist{2}- max_qr + O_D.l{2}]) /\
  O_Gm2.pk{1} = O_D.pk{2} /\
  O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
     O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true
  ).
  proc (
  ={glob RP1, glob RP2} /\ !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
  O_Gm2.l{1} = O_Gm2.l{1} /\ O_Gm2.l{1} =  O_D.l{2} /\
  length O_Gm2.qlist{1} = length O_D.qlist{2} /\
  0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
  (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
  O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] = 
  O_D.qlist{2}.[length O_D.qlist{2}- max_qr + O_D.l{2}]) /\
  O_Gm2.pk{1} = O_D.pk{2} /\
  O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
     O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true  ).
  smt.
  smt.
  (* begin pgen preserves invariant *)
  proc.
  case (queryValid_VRF (F.fn)%F x{1}).
  rcondf {1} 1.
  intros &m;skip;by trivial.
  rcondf {2} 1.
  intros &m;skip;by smt.
  case (length O_D.qlist{2} = max_qr - 1 - O_D.l{2}).
  rcondt {1} 1.
  intros &m;skip;smt.
  rcondt {2} 1.
  intros &m;skip;smt.
  inline  RP(RP1, RP2).gen O_D(O_IND'(RP1, RP2), RP1, RP2).RP.gen.
  wp.
  call (_ : ={glob RP1, glob RP2} ==> ={glob RP1, glob RP2,res}).
  proc (={glob RP1}).
  by trivial.
  by trivial. 
  call (_ : ={glob RP1,glob RP2} ==> ={glob RP1, glob RP2,res}).
  proc (={glob RP2}).
  by trivial.
  by trivial.
  wp;skip;progress;smt.
  rcondf {1} 1.
  intros &m;skip;by smt.
  rcondf {2} 1.
  intros &m;skip;by smt.
  inline O_IND'(RP1, RP2).enc RP(RP1, RP2).gen.
  rcondf {2} 4.
  intros &m;wp;skip;progress.
  delta FHE.queryValid_IND phi Scheme.Input.inputG_len inputG_len Garble1.C2.Input.inputG_len;beta.
  cut ->: (
 (fst
     (let (fn, iK, oK) = O_D.dummy_sk{hr} in encode iK x{hr},
      let (fn, iK, oK) = O_D.sk{hr} in encode iK x{hr})) =
(let (fn, iK, oK) = O_D.dummy_sk{hr} in encode iK x{hr})
  ).
   smt.
  cut ->: (
 (snd
     (let (fn, iK, oK) = O_D.dummy_sk{hr} in encode iK x{hr},
      let (fn, iK, oK) = O_D.sk{hr} in encode iK x{hr})) =
(let (fn, iK, oK) = O_D.sk{hr} in encode iK x{hr})
  ).
   smt.
   smt.
  swap {1} [2..3] -1.
  swap {2} 4 -3.
  swap {2} 7 -5.
  seq 2 2 : (={x,rg,re} /\
    ={glob RP1, glob RP2} /\
    !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
    O_Gm2.l{1} = O_Gm2.l{1} /\
    O_Gm2.l{1} = O_D.l{2} /\
    length O_Gm2.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
    (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm2.pk{1} = O_D.pk{2} /\
    O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
      O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true/\
   queryValid_VRF (F.fn)%F x{1} /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}).
  call (_ : 
    ={glob RP1, glob RP2} /\
    !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
    O_Gm2.l{1} = O_Gm2.l{1} /\
    O_Gm2.l{1} = O_D.l{2} /\
    length O_Gm2.qlist{1} = length O_D.qlist{2} /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
    0 <= length O_Gm2.qlist{1} <= max_qr /\
    (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm2.pk{1} = O_D.pk{2} /\
    O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
      O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ==>
={res} /\ 
    ={glob RP1, glob RP2} /\
    !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
    O_Gm2.l{1} = O_Gm2.l{1} /\
    O_Gm2.l{1} = O_D.l{2} /\
    length O_Gm2.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
    (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm2.pk{1} = O_D.pk{2} /\
    O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
      O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ).
  proc (={glob RP1} /\
    !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
    O_Gm2.l{1} = O_Gm2.l{1} /\
    O_Gm2.l{1} = O_D.l{2} /\
    length O_Gm2.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
    (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm2.pk{1} = O_D.pk{2} /\
    O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
      O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ).
  smt.
  smt.
  call (_ : 
    ={glob RP1, glob RP2} /\
    !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
    O_Gm2.l{1} = O_Gm2.l{1} /\
    O_Gm2.l{1} = O_D.l{2} /\
    length O_Gm2.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
    (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm2.pk{1} = O_D.pk{2} /\
    O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
      O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ==>
={res} /\
    ={glob RP1, glob RP2} /\
    !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
    O_Gm2.l{1} = O_Gm2.l{1} /\
    O_Gm2.l{1} = O_D.l{2} /\
    length O_Gm2.qlist{1} = length O_D.qlist{2} /\
    0 <= length O_Gm2.qlist{1} <= max_qr /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
    (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm2.pk{1} = O_D.pk{2} /\
    O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
      O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ).
  proc (={ glob RP2} /\
    !O_IND'.b{2} /\
       0<=  O_Gm2.l{1} < max_qr /\
    O_Gm2.l{1} = O_Gm2.l{1} /\
    O_Gm2.l{1} = O_D.l{2} /\
    length O_Gm2.qlist{1} = length O_D.qlist{2} /\
  O_IND'.count{2} <= length O_Gm2.qlist{1} /\
    0 <= length O_Gm2.qlist{1} <= max_qr /\
    (length O_Gm2.qlist{1} > max_qr - 1 - O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_D.qlist{2}.[length O_D.qlist{2} - max_qr + O_D.l{2}]) /\
    O_Gm2.pk{1} = O_D.pk{2} /\
    O_Gm2.sk{1} = O_D.sk{2} /\
  O_Gm2.dummy_pk{1} = O_D.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_D.dummy_sk{2} /\
      O_Gm2.validQs{1} = O_D.validQs{2} /\
  O_IND'.validQs{2} = true /\
  ! length O_D.qlist{2} = max_qr - 1 - O_D.l{2}
  ).
  smt.
  smt.
  skip;progress;smt.
  wp;skip;progress;smt.
  rcondt {1} 1.
  intros &m;wp;skip;by trivial.
  rcondt {2} 1.
  intros &m;wp;skip;by smt.
  wp;skip;progress;smt.
  skip;progress;smt.
(* END pgen PRESERVES INVARIANT *)
  inline   Game_2(RG, RP(RP1, RP2), ADV).O.check Game_IND'(RP1, RP2, D).O.check D(O_IND'(RP1, RP2)).O_D.check.
  rcondt {2} 8.
  intros &m.
  wp;skip;smt.
  case (! O_D.validQs{2}).
  rcondt {2} 4.
  intros &m;wp;skip;smt.
  rcondf {1} 4.
  intros &m;wp;skip;smt.
  wp;skip;progress;smt.
  rcondf {2} 4.
  intros &m;wp;skip;smt.
  wp;skip.
  intros &1 &2 H1.
  case (i{2} >= 0 /\ i{2} < length O_D.qlist{2} /\
     length O_D.qlist{2} = max_qr /\ O_IND'.validQs{2} = true).
  intros goodi.
  cut -> : ( i{1} >= 0 /\
       i{1} < length O_Gm2.qlist{1} /\
       length O_Gm2.qlist{1} = max_qr /\ O_Gm2.validQs{1} = true).
  smt.
  simplify.
  case (i{2} = O_D.l{2}).
  intros goodl.
  cut -> : (i{1} = O_Gm2.l{1}).
  cut -> : (i{1} = i{2}).
  smt.
  cut -> : (O_Gm2.l{1} = O_D.l{2}).
  smt.
  smt.
  smt.
  smt.
  smt.
    qed.
 
   (* Writing down the interpolation properly *)
 
    local lemma gm1_gm2_hop : 
      forall  &m,
       Pr[Game_1(RG, RP(RP1,RP2), ADV).main()@ &m:res] -
       Pr[Game_2(RG, RP(RP1,RP2), ADV).main()@ &m:res] = 
               Pr[ FHE.Game_IND'(RP1,RP2,D).main(true)@ &m:res] -
               Pr[ FHE.Game_IND'(RP1,RP2,D).main(false)@ &m:!res].
     proof.
     intros &m.
     cut -> : (Pr[Game_1(RG, RP(RP1,RP2), ADV).main()@ &m:res] =
               Pr[ FHE.Game_IND'(RP1,RP2,D).main(true)@ &m:res]).
     byequiv gm1_eq_true.
    smt.
    smt.
     cut -> : (Pr[Game_2(RG, RP(RP1,RP2), ADV).main()@ &m:res] =
               Pr[ FHE.Game_IND'(RP1,RP2,D).main(false)@ &m:!res]).
     byequiv gm2_eq_false.
     smt.
     smt.
     reflexivity.
     qed.
 
    (* Final step: Reduction to SCH AUTH *)
 
  (* Here is the body of the reduction algorithm *)
 
local module Fg(O : SchAuth.O_AUT'_t) : SchAuth.Adv_AUT'_t(O) = {
  module O_F = O_F(O,RG,RP(RP1,RP2))
  module A = ADV(O_F)
 
  proc gen_fun() : VC.fun_t  = {
     return F.fn;
  }
 
  proc forge(fG : VC.pkey_t) : outputG_t = { 
      var pk : VC.pkey_t;
      var pr : VC.proof_t;
      var i : int;
      var r : bool;
      var l : int;
      var xG : outputG_t;
  
      l = $[0..max_qr-1];
 
      O_F.init(l,fG);
      (i,pr) = A.forge(fG);
      xG = O_F.getForge(i,pr);
      return xG;
  }
}.
 
 (* The actual reduction to adaptive authenticity *)
     local lemma gm2_red_equiv :
       equiv [Game_2(RG, RP(RP1,RP2), ADV).main ~
              SchAuth.Game_AUT'(Rand,SchAuth.O,Fg).main : 
                 ={glob ADV, glob RP1, glob RP2} ==> ={res}].
     proof.
     proc.
     inline SchAuth.Game_AUT'(Rand,SchAuth.O, Fg).A.gen_fun Game_2(RG, RP(RP1, RP2), ADV).O.init SchAuth.O.init SchAuth.Game_AUT'(Rand,SchAuth.O, Fg).A.forge Fg(SchAuth.O).O_F.init RG.gen .
     swap {2} [10..11] -9.
     swap {2} [13..15] -10.
     swap {1} 10 -2.
     seq 14 20 : (
     ={glob ADV, glob RP1, glob RP2, l} /\ 
     O_Gm2.l{1} = l{1} /\ O_F.l{2} = l{2} /\ 
     0<= O_Gm2.l{1} < max_qr /\
     Game_AUT'.fn{2} = F.fn /\
     O_Gm2.qlist{1} = Array.empty /\ O_F.qlist{2} = Array.empty /\
     O_Gm2.validQs{1} = true  /\ O_F.validQs{2} = true /\
     r0{1} = rg{2} /\ O_Gm2.pk{1} =  O_F.pk{2} /\
     O_Gm2.sk{1} = (F.fn,SchAuth.O.iK{2},oK{2}) /\ 
     f0{1} = F.fn /\ pk{1} = fG0{2} /\
     fG0{2} = O_F.pk{2} /\ SchAuth.O.queryDone{2} = false /\
     ={r_dummy} /\ O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     O_Gm2.dummy_sk{1} = O_F.dummy_sk{2}).
     wp.
     call (_ : ={l} ==> ={res}).
      proc.
      wp; while (={l,i,n,q,r}).
       by wp; rnd; rnd; skip; progress.
      wp; while (={l,i,n,q,m,r}).
       by wp; rnd; rnd; rnd; skip; progress.
      wp;skip;smt.
     wp.
     call (_ : ={l} ==> ={res}).
      proc.
      wp; while (={l,i,n,q,r}).
       by wp; rnd; rnd; skip; progress.
      wp; while (={l,i,n,q,m,r}).
       by wp; rnd; rnd; rnd; skip; progress.
      wp;skip;smt.
     by auto; progress; expect 2 smt.
       seq 1 1 : (
   ={glob ADV, glob RP1, glob RP2, l, i, pr} /\ 
     Game_AUT'.fn{2} = F.fn /\ 
     O_Gm2.l{1} = O_F.l{2} /\
     0<= O_Gm2.l{1} < max_qr /\
     O_Gm2.pk{1} =  O_F.pk{2} /\
     (oK{2} = let (a,b,c) = O_Gm2.sk{1} in c) /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
     O_Gm2.validQs{1} = O_F.validQs{2} /\
     O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\ 
     length O_Gm2.qlist{1} = length O_F.qlist{2}  /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
    (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
  ).
     call (_ :
   ={glob ADV, glob RP1, glob RP2,pk} /\ 
     O_Gm2.l{1} = O_F.l{2} /\
     Game_AUT'.fn{2} = F.fn /\
     0<= O_Gm2.l{1} < max_qr /\
     O_Gm2.pk{1} =  O_F.pk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
     O_Gm2.validQs{1} = O_F.validQs{2} /\
     O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\ 
     length O_Gm2.qlist{1} = length O_F.qlist{2}  /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
    (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
==>
   ={glob ADV, glob RP1, glob RP2,res} /\ 
     O_Gm2.l{1} = O_F.l{2} /\
     Game_AUT'.fn{2} = F.fn /\
     0<= O_Gm2.l{1} < max_qr /\
     O_Gm2.pk{1} =  O_F.pk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
     O_Gm2.validQs{1} = O_F.validQs{2} /\
     O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\ 
     length O_Gm2.qlist{1} = length O_F.qlist{2}  /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
    (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
      ).
    proc (
    ={glob RP1, glob RP2} /\ 
     Game_AUT'.fn{2} = F.fn /\
     O_Gm2.l{1} = O_F.l{2} /\
     0<= O_Gm2.l{1} < max_qr /\
     O_Gm2.pk{1} =  O_F.pk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
     O_Gm2.validQs{1} = O_F.validQs{2} /\
     O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\ 
     length O_Gm2.qlist{1} = length O_F.qlist{2}  /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
    (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
      ).
     smt.
     smt.
      (* begin pgen preserves invariant ... *)
     proc.
     case (queryValid_VRF (F.fn) x{1}).
     rcondf {1} 1.
     intros &m;skip;progress;by trivial.
     rcondf {2} 1.
     intros &m;skip;progress;by smt.
     case (length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1).
     rcondt {1} 1.
     intros &m;skip;by trivial.
      rcondt {2} 1.
     intros &m;skip;progress;by smt.
     inline SchAuth.O.enc RP(RP1, RP2).gen.
     seq 2 2 : (
  ={x,f,rg} /\
    ={glob RP1, glob RP2} /\
    O_Gm2.l{1} = O_F.l{2} /\
     Game_AUT'.fn{2} = F.fn /\
    0 <= O_Gm2.l{1} < max_qr /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      ) /\
   queryValid_VRF (F.fn)%F x{1} /\
  length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
     ).
   call (_ :
   ={glob RP1, glob RP2} /\
     Game_AUT'.fn{2} = F.fn /\
    O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
       (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
   ==>
 ={glob RP1, glob RP2,res} /\
     Game_AUT'.fn{2} = F.fn /\
    O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
       (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
    ).
    proc (
={glob RP2} /\
    O_Gm2.l{1} = O_F.l{2} /\
     Game_AUT'.fn{2} = F.fn /\
    0 <= O_Gm2.l{1} < max_qr /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
    ).
   by trivial.
   by trivial.
   wp;skip;by trivial.
     seq 1 1 : (
  ={x, f, rg,re} /\
  ={glob RP1, glob RP2} /\
     Game_AUT'.fn{2} = F.fn /\
  O_Gm2.l{1} = O_F.l{2} /\
  0 <= O_Gm2.l{1} < max_qr /\
  O_Gm2.pk{1} = O_F.pk{2} /\
  O_Gm2.validQs{1} = O_F.validQs{2} /\
  O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
  O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
  length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
      (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  queryValid_VRF (F.fn)%F x{1} /\
  length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
     ).
   call (_ :
   ={glob RP1, glob RP2} /\
     Game_AUT'.fn{2} = F.fn /\
  O_Gm2.l{1} = O_F.l{2} /\
  0 <= O_Gm2.l{1} < max_qr /\
  O_Gm2.pk{1} = O_F.pk{2} /\
  O_Gm2.validQs{1} = O_F.validQs{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
  O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
  length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
      (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
   ==>
 ={glob RP1, glob RP2,res} /\
  O_Gm2.l{1} = O_F.l{2} /\
     Game_AUT'.fn{2} = F.fn /\
  0 <= O_Gm2.l{1} < max_qr /\
  O_Gm2.pk{1} = O_F.pk{2} /\
  O_Gm2.validQs{1} = O_F.validQs{2} /\
  O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
  O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
  length O_Gm2.qlist{1} = length O_F.qlist{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
      (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
    ).
    proc (
 ={glob RP1} /\
  O_Gm2.l{1} = O_F.l{2} /\
     Game_AUT'.fn{2} = F.fn /\
  0 <= O_Gm2.l{1} < max_qr /\
  O_Gm2.pk{1} = O_F.pk{2} /\
  O_Gm2.validQs{1} = O_F.validQs{2} /\
  O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     (let (a,b,c) = O_Gm2.sk{1} in  b = SchAuth.O.iK{2}) /\
  O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
  length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 -O_Gm2.l{1} =>
           SchAuth.O.queryDone{2} = false) /\
      (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
    ).
   by trivial.
   by trivial.
   wp;skip;by trivial.
   rcondf {2} 4.
   intros &m;wp;skip;by smt.
   wp;skip;progress.
   smt.
  smt.
  smt.
   smt.
   smt.   
   smt.
   smt.
   delta VC.pGen SomeVC.VC.pGen;beta.
   congr.
   move: H1.
   cut ->: O_Gm2.sk{1} = (O_Gm2.sk{1}.`1, O_Gm2.sk{1}.`2, O_Gm2.sk{1}.`3) by smt.
   simplify => H1.
   cut ->: gen rg{2} = ((gen rg{2}).`1, (gen rg{2}).`2) by smt.
   simplify; congr.
   by rewrite oget_some H1.
   smt.
   smt.
   rewrite Array.length_cons.
   rewrite Array.length_cons.
   generalize H6;rewrite H2 => H6.
   rewrite H6.
   cut -> : ((1 + (max_qr - O_F.l{2} - 1) - max_qr +
                                      O_F.l{2}) = 0).
   smt.
   rewrite get_cons.
   smt.
   simplify.
   rewrite get_cons.
   smt.
   simplify.
   move: H1 H9;delta VC.pGen SomeVC.VC.pGen;beta.
   cut ->: O_Gm2.sk{1} = (O_Gm2.sk{1}.`1, O_Gm2.sk{1}.`2, O_Gm2.sk{1}.`3) by smt.
   simplify => H1.
   cut ->: gen rg{2} = ((gen rg{2}).`1, (gen rg{2}).`2) by smt.
   by simplify => H9; rewrite oget_some; congr; congr.
   smt.
   smt.
   by move: H5; rewrite /queryValid_VRF /VC.valid_i /SomeVC.VC.valid_i /validInputs /Garble2.validInputs.
   rcondf {1} 1.
   by trivial.
   rcondf {2} 1.
   intros &m;skip;progress;smt.
   inline RP(RP1, RP2).gen.
   seq 2 2 : (
={x,f,rg} /\
     Game_AUT'.fn{2} = F.fn /\
    ={glob RP1, glob RP2} /\
    O_Gm2.l{1} = O_F.l{2} /\
  O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    (let (a, b, c) = O_Gm2.sk{1} in b = SchAuth.O.iK{2}) /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 - O_Gm2.l{1} =>
     SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
   queryValid_VRF (F.fn)%F x{1} /\
  ! length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
   ).
call (_ :
={glob RP1, glob RP2} /\
  O_Gm2.l{1} = O_F.l{2} /\
     Game_AUT'.fn{2} = F.fn /\
    O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    (let (a, b, c) = O_Gm2.sk{1} in b = SchAuth.O.iK{2}) /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 - O_Gm2.l{1} =>
     SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  ! length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
==>
={glob RP1, glob RP2,res} /\
    O_Gm2.l{1} = O_F.l{2} /\
  O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
     Game_AUT'.fn{2} = F.fn /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    (let (a, b, c) = O_Gm2.sk{1} in b = SchAuth.O.iK{2}) /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 - O_Gm2.l{1} =>
     SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  ! length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
).  
proc (
={glob RP2} /\
    O_Gm2.l{1} = O_F.l{2} /\
  O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
     Game_AUT'.fn{2} = F.fn /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    (let (a, b, c) = O_Gm2.sk{1} in b = SchAuth.O.iK{2}) /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 - O_Gm2.l{1} =>
     SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  ! length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
).
by trivial.
by trivial.
wp;skip;by trivial.
   seq 1 1 : (
={x,f,rg,re} /\
    ={glob RP1, glob RP2} /\
    O_Gm2.l{1} = O_F.l{2} /\
  O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    (let (a, b, c) = O_Gm2.sk{1} in b = SchAuth.O.iK{2}) /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
     Game_AUT'.fn{2} = F.fn /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 - O_Gm2.l{1} =>
     SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
   queryValid_VRF (F.fn)%F x{1} /\
  ! length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
   ).
call (_ :
={glob RP1, glob RP2} /\
    O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
  O_Gm2.l{1} = O_F.l{2} /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    (let (a, b, c) = O_Gm2.sk{1} in b = SchAuth.O.iK{2}) /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
     Game_AUT'.fn{2} = F.fn /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 - O_Gm2.l{1} =>
     SchAuth.O.queryDone{2} = false) /\
       (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  ! length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
==>
={glob RP1, glob RP2,res} /\
    O_Gm2.l{1} = O_F.l{2} /\
  O_Gm2.l{1} = O_F.l{2} /\
     Game_AUT'.fn{2} = F.fn /\
    0 <= O_Gm2.l{1} < max_qr /\
    O_Gm2.pk{1} = O_F.pk{2} /\
    (let (a, b, c) = O_Gm2.sk{1} in b = SchAuth.O.iK{2}) /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 - O_Gm2.l{1} =>
     SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  ! length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
).  
proc (
={glob RP1} /\
    O_Gm2.l{1} = O_F.l{2} /\
    0 <= O_Gm2.l{1} < max_qr /\
     Game_AUT'.fn{2} = F.fn /\
    O_Gm2.pk{1} = O_F.pk{2} /\
  O_Gm2.l{1} = O_F.l{2} /\
    (let (a, b, c) = O_Gm2.sk{1} in b = SchAuth.O.iK{2}) /\
    O_Gm2.validQs{1} = O_F.validQs{2} /\
    O_Gm2.dummy_pk{1} = O_F.dummy_pk{2} /\
    O_Gm2.dummy_sk{1} = O_F.dummy_sk{2} /\
    length O_Gm2.qlist{1} = length O_F.qlist{2} /\
    (length O_Gm2.qlist{1} <= max_qr - 1 - O_Gm2.l{1} =>
     SchAuth.O.queryDone{2} = false) /\
        (length O_Gm2.qlist{1} > max_qr - 1 -O_Gm2.l{1} =>
     O_Gm2.qlist{1}.[length O_Gm2.qlist{1} - max_qr + O_Gm2.l{1}] =
     O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}] /\
      let (a,b,c) = O_F.qlist{2}.[length O_F.qlist{2} - max_qr + O_F.l{2}]
            in SchAuth.O._x{2} = a /\ validInputs (F.fn)%F SchAuth.O._x{2}
      )
 /\
  ! length O_Gm2.qlist{1} = max_qr - O_Gm2.l{1} - 1
).
by trivial.
by trivial.
wp;skip;by trivial.
case (length O_Gm2.qlist{1} >= max_qr).
rcondt {1} 3.
intros &m;wp;skip;by smt.
rcondt {2} 3.
intros &m;wp;skip;by smt.
wp;skip;progress;smt.
rcondf {1} 3.
intros &m;wp;skip;by smt.
rcondf {2} 3.
intros &m;wp;skip;by smt.
wp;skip;smt.
rcondt{1} 1.
by trivial.
rcondt{2} 1.
by trivial.
wp;skip;progress;smt.
      (* end pgen preserves invariant ... *)
skip;progress;smt.
     inline Game_2(RG, RP(RP1, RP2), ADV).O.check SchAuth.O.getX Fg(SchAuth.O).O_F.getForge.
     wp;skip;progress.
     delta VC.vrfy SomeVC.VC.vrfy;beta.
     delta VC.eval SomeVC.VC.eval;beta.
   move: H1 H10.
   cut ->: O_Gm2.sk{1} = (O_Gm2.sk{1}.`1, O_Gm2.sk{1}.`2, O_Gm2.sk{1}.`3) by smt.
   simplify => H1 H10.
   pose x := (O_Gm2.qlist{1}.[O_F.l{2}]).
   case (valid_outG O_Gm2.sk{1}.`3 (dec x.`3 pr{2})); rewrite /x => {x}.
    move => case1 /=.
    pose validG := valid_outG _ _. 
    cut ->: validG = true by smt.
    rewrite /= oget_some => {validG}.
   cut HH: length O_Gm2.qlist{1} > max_qr - 1 - O_F.l{2} by smt.
   move: (H4 _) => // {H4}.
   rewrite H2 H7; cut ->: max_qr - max_qr + O_F.l{2} = O_F.l{2} by smt.
   smt.

    intros casen1.
    smt.

   move: H1.
   cut ->: O_Gm2.sk{1} = (O_Gm2.sk{1}.`1, O_Gm2.sk{1}.`2, O_Gm2.sk{1}.`3) by smt.
   simplify => H1; smt.
   move: H1.
   cut ->: O_Gm2.sk{1} = (O_Gm2.sk{1}.`1, O_Gm2.sk{1}.`2, O_Gm2.sk{1}.`3) by smt.
   simplify => H1; smt.
   move: H1.
   cut ->: O_Gm2.sk{1} = (O_Gm2.sk{1}.`1, O_Gm2.sk{1}.`2, O_Gm2.sk{1}.`3) by smt.
   simplify => H1; smt.
    smt.
    smt.
    smt.
     qed.
 
    (* Expressing the reduction as a probability *)
 
    local lemma gm2_red : 
      forall  &m,
       Pr[Game_2(RG, RP(RP1,RP2), ADV).main()@ &m:res] = 
               Pr [SchAuth.Game_AUT'(Rand,SchAuth.O,Fg).main()@ &m:res].
     proof.
     intros &m.
     byequiv (gm2_red_equiv).
     by trivial.
     by trivial.
     qed.
 
   (* Wrapping up the proof: FHE + Adaptive Auth => Auth *)
 
   local lemma vc_is_auth : 
      forall  &m,
       Pr[Game_Ver(RG, RP(RP1,RP2), ADV).main()@ &m:res] = 
            VCSec.max_qr%r * (Pr [SchAuth.Game_AUT'(Rand,SchAuth.O,Fg).main()@ &m:res] + 
               Pr[ FHE.Game_IND'(RP1,RP2,D).main(true)@ &m:res] -
               Pr[ FHE.Game_IND'(RP1,RP2,D).main(false)@ &m:!res]).
   proof.
   intros &m.
   cut -> : (
     Pr[Game_Ver(RG, RP(RP1, RP2), ADV).main() @ &m : res] = 
     VCSec.max_qr%r * Pr[Game_1(RG, RP(RP1,RP2), ADV).main()@ &m:res]
   ).
   apply (auth_hop1 &m).
   cut -> : (
      Pr[Game_1(RG, RP(RP1, RP2), ADV).main() @ &m : res] =
      Pr[Game_2(RG, RP(RP1,RP2), ADV).main()@ &m:res] +
               Pr[ FHE.Game_IND'(RP1,RP2,D).main(true)@ &m:res] -
               Pr[ FHE.Game_IND'(RP1,RP2,D).main(false)@ &m:!res]
   ).
   smt.
   cut -> : (
      Pr[Game_2(RG, RP(RP1, RP2), ADV).main() @ &m : res] = 
      Pr [SchAuth.Game_AUT'(Rand,SchAuth.O,Fg).main()@ &m:res]
   ).
   apply (gm2_red &m).
   reflexivity.
   qed.
 
end section.
 
   (******************************************************)
   (*********  CONFIDENTIALITY PROOF  ********************)
   (******************************************************)
 
   (* This adversary reduces privacy to FHE security (oracle part) *)
 
  op inv_indquery : FHE.query_IND.
  axiom inv_invindq : !queryValid_IND inv_indquery.
 
  module O_Red(O : O_IND'_t) = {
 
     var validQs : bool
     var pk : VC.pkey_t
     var sk : VC.skey_t
     var counter : int
 
     proc init() : VC.pkey_t = {
         var rg:VC.randg_t;
         validQs = true;
         counter = 0;
         rg = RG.gen(F.fn);
         (pk,sk) = VC.kGen F.fn rg;
         return pk;
     }
 
    proc probGen(x : VC.inp_t): VC.xpub_t option = {
      var xp:VC.xpub_t option;
      var xs:VC.xprv_t;
      var rp:VC.randp_t;
      var ret : VC.xpub_t option;
      var xG : FHE.plaini_t;
 
      if (VC.valid_i F.fn x)
      {
        if (counter < max_qr) {
          xG = let (fn, iK, oK) = sk in
                   Input.encode iK x;
          xp = O.enc((xG,xG));
          ret = xp;
          counter = counter + 1;
        }
        else
          ret = None;
      }
      else {
        validQs = false;
        ret = None;
        xp = O.enc(inv_indquery);
      }
      return ret;
    }
 
   proc challenge(query : query_IND): VC.xpub_t option = {
      var xp:VC.xpub_t option;
      var xs:VC.xprv_t;
      var rp:VC.randp_t;
      var ret : VC.xpub_t option;
      var xG0 : FHE.plaini_t;
      var xG1 : FHE.plaini_t;
 
      if (queryValid_IND F.fn query)
      {
        if (counter < max_qr) {
          xG0 = let (fn, iK, oK) = sk in
                   Input.encode iK (fst query);
          xG1 = let (fn, iK, oK) = sk in
                   Input.encode iK (snd query);
          xp = O.enc((xG0,xG1));
          ret = xp;
          counter = counter + 1;
        }
        else
          ret = None;
      }
      else {
        validQs = false;
        ret = None;
        xp = O.enc(inv_indquery);
      }
      return ret;
    }
 
   }.
 
(* Now the actual lemma *)
section.
 
declare module ADV : Adv_IND_t {Game_IND, O_IND', O_Red}.
declare module RP1 : FHE.RandG_t  {ADV, FHE.Game_IND,Game_IND, O_IND', O_Red}.
declare module RP2 : FHE.RandE_t {ADV, FHE.Game_IND, RP1, Game_IND, O_IND',O_Red}.
 
   local module B(O : O_IND'_t) : FHE.Adv_IND'_t(O) = {
      module O_Red = O_Red(O)
      module A = ADV(O_Red)
 
     proc guess() : bool  = {
       var pk : VC.pkey_t;
       var r : bool;
       
      pk = O_Red.init();
      r = A.guess(pk);
      return r;
  }
}.
 
   (* The actual reduction *)
 
   local lemma vc_is_ind_eq : 
       equiv [ Game_IND(RG, RP(RP1,RP2), ADV).main ~ 
               FHE.Game_IND'(RP1,RP2,B).main : ={glob ADV, glob RP1, glob RP2,b} ==> ={res} ].
   proof.
   proc.
   inline  Game_IND(RG, RP(RP1, RP2), ADV).O.init  Game_IND'(RP1, RP2, B).O.init Game_IND'(RP1, RP2, B).A.guess B(O_IND'(RP1, RP2)).O_Red.init.
   seq 7 9 : (
={glob ADV, glob RP1, glob RP2,b} /\
bval{1} = b{1} /\
bval{2} = b{2} /\                            
O_IND.b{1} = bval{1} /\
O_IND'.b{2} = bval{2} /\                  
O_IND.validQs{1} = true /\
O_IND'.validQs{2} = true /\             
O_IND.count{1} = 0 /\
O_IND'.count{2} = 0 /\                    
={rg} /\
O_Red.validQs{2} = true /\                
(O_IND.pk{1}, O_IND.sk{1}) = (VC.kGen (F.fn)%F rg{1})%VC /\
O_Red.counter{2} = 0 /\                   
pk{1} = O_IND.pk{1} /\
(O_Red.pk{2}, O_Red.sk{2}) = (VC.kGen (F.fn)%F rg{2})%VC /\          
pk{2} = O_Red.pk{2}
   ).
   inline RG.gen Rand.gen.
    wp; while (={glob ADV, glob RP1, glob RP2, b, i, n, q} /\ r{1}=r0{2}). 
     by wp; rnd; rnd; progress.
    wp; while (={glob ADV, glob RP1, glob RP2, b, i, n, q, m} /\ r{1}=r0{2}). 
     by wp; rnd; rnd; rnd; progress.
    wp;skip;progress;smt.
   seq 1 2 : (O_IND.b{1} = O_IND'.b{2} /\ ={b'} /\ O_IND.validQs{1} = O_IND'.validQs{2}). 
   wp.
   call (_ :
={glob ADV, glob RP1, glob RP2,pk} /\
O_IND.b{1} = O_IND'.b{2} /\                  
O_IND.validQs{1} = true /\
O_IND'.validQs{2} = true /\             
O_IND.count{1} = 0 /\
O_IND'.count{2} = 0 /\                    
O_Red.validQs{2} = true /\                
(O_IND.pk{1}, O_IND.sk{1}) = (O_Red.pk{2}, O_Red.sk{2}) /\
O_Red.counter{2} = 0                    
==>
={res} /\ O_IND.b{1} = O_IND'.b{2} /\ O_IND.validQs{1} = O_IND'.validQs{2}
   ).
   proc (
={ glob RP1, glob RP2} /\
O_IND.b{1} = O_IND'.b{2} /\                  
O_IND.validQs{1} = O_IND'.validQs{2}  /\             
O_IND.count{1} = O_IND'.count{2} /\                    
O_Red.validQs{2} = O_IND'.validQs{2} /\
O_IND'.count{2} = O_Red.counter{2} /\                
(O_IND.pk{1}, O_IND.sk{1}) = (O_Red.pk{2}, O_Red.sk{2})
   ).
   progress;smt.
   progress;smt.
   (*** start pgen  ***)
   proc.
   case ((VC.valid_i (F.fn)%F x{1})%VC).
   rcondt {1} 1.
   intros &m;skip;by trivial.
   rcondt {2} 1.
   intros &m;skip;by trivial.
   case (O_IND.count{1} < max_qr).
    rcondt {1} 1.
   intros &m;skip;by trivial.
   rcondt {2} 1.
   intros &m;skip;by trivial.
   inline O_IND'(RP1, RP2).enc.
   rcondf {2} 3.
   intros &m.
   wp;skip;smt.
   inline RP(RP1, RP2).gen.
   rcondt {2} 8.
   intros &m.
   wp.
   call (_ : true ==> true).
   proc (true).
   by trivial.
   by trivial.
    wp.
   call (_ : true ==> true).
   proc (true).
   by trivial.
   by trivial.
   intros;wp;skip;progress;smt.
   wp.
   call (_ : ={glob RP2} ==> ={res,glob RP2}).
   proc (true).
   by trivial.
   by trivial.
    wp.
   call (_ : ={glob RP1} ==> ={res,glob RP1}).
   proc (true).
   by trivial.
   by trivial.
   intros;wp;skip;progress;smt.
   rcondf {1} 1.
   intros &m;skip;by trivial.
   rcondf {2} 1.
   intros &m;skip;by trivial.
   wp;by trivial.
   rcondf {1} 1.
   intros &m;skip;by trivial.
   rcondf {2} 1.
   intros &m;skip;by trivial.
   inline O_IND'(RP1, RP2).enc.
   rcondt {2} 4.
   intros &m;wp;skip;progress;smt.
   wp;skip;progress;smt.
   (*** end pgen ***)
   (*** start chal ***)
   proc.
   case ((queryValid_IND (F.fn)%F query{1})%VC).
   rcondt {1} 1.
   intros &m;skip;by trivial.
   rcondt {2} 1.
   intros &m;skip;by trivial.
   case (O_IND.count{1} < max_qr).
    rcondt {1} 1.
   intros &m;skip;by trivial.
   rcondt {2} 1.
   intros &m;skip;by trivial.
   inline O_IND'(RP1, RP2).enc.
   rcondf {2} 4.
   intros &m.
   wp;skip;progress.
  delta FHE.queryValid_IND phi Scheme.Input.inputG_len inputG_len Garble1.C2.Input.inputG_len;beta.
  cut ->: (
(fst
     (let (fn, iK, oK) = O_Red.sk{hr} in encode iK (fst query{hr}),
      let (fn, iK, oK) = O_Red.sk{hr} in encode iK (snd query{hr}))) =
(let (fn, iK, oK) = O_Red.sk{hr} in encode iK (fst query{hr}))
  ).
   smt.
  cut ->: (
(snd
     (let (fn, iK, oK) = O_Red.sk{hr} in encode iK (fst query{hr}),
      let (fn, iK, oK) = O_Red.sk{hr} in encode iK (snd query{hr}))) =
(let (fn, iK, oK) = O_Red.sk{hr} in encode iK (snd query{hr}))
  ).
   smt.
   generalize H;delta queryValid_IND VC.valid_i SomeVC.VC.valid_i validInputs Garble2.validInputs Garble1.C2.validInputs;beta => H.
   delta encode Garble2.Input.encode Garble1.C2.Input.encode;beta.
   pose n := (let (n, m, q, aa, bb) = fst (F.fn)%F in n).
   cut -> :(length (fst query{hr}) = n).
   generalize H;smt.
   cut -> :(length (snd query{hr}) = n).
   generalize H;smt.
   pose iK := let (fn, iK, oK) = O_Red.sk{hr} in iK.
cut -> : (
  (let (fn, iK0, oK) = O_Red.sk{hr} in
   init n (fun (k : int), oget iK0.[(k, (fst query{hr}).[k])]))
= 
  (
   (init n (fun (k : int), oget iK.[(k, (fst query{hr}).[k])])))
).
by rewrite /iK; cut ->: O_Red.sk{hr} = (O_Red.sk{hr}.`1, O_Red.sk{hr}.`2, O_Red.sk{hr}.`3) by smt => /=.
cut -> : (
  (let (fn, iK0, oK) = O_Red.sk{hr} in
   init n (fun (k : int), oget iK0.[(k, (snd query{hr}).[k])]))
= 
  (
   (init n (fun (k : int), oget iK.[(k, (snd query{hr}).[k])])))
).
by rewrite /iK; cut ->: O_Red.sk{hr} = (O_Red.sk{hr}.`1, O_Red.sk{hr}.`2, O_Red.sk{hr}.`3) by smt => /=.
rewrite /Garble2.Input.inputG_len /Garble1.C2.Input.inputG_len length_init.
generalize H;smt.
rewrite length_init.
generalize H;smt.
reflexivity.
   inline RP(RP1, RP2).gen.
   rcondt {2} 9.
   intros &m.
   wp.
   call (_ : true ==> true).
   proc (true).
   by trivial.
   by trivial.
    wp.
   call (_ : true ==> true).
   proc (true).
   by trivial.
   by trivial.
   intros;wp;skip;progress;smt.
   wp.
   call (_ : ={glob RP2} ==> ={res,glob RP2}).
   proc (true).
   by trivial.
   by trivial.
    wp.
   call (_ : ={glob RP1} ==> ={res,glob RP1}).
   proc (true).
   by trivial.
   by trivial.
   intros;wp;skip;progress;smt.
   rcondf {1} 1.
   intros &m;skip;by trivial.
   rcondf {2} 1.
   intros &m;skip;by trivial.
   wp;by trivial.
   rcondf {1} 1.
   intros &m;skip;by trivial.
   rcondf {2} 1.
   intros &m;skip;by trivial.
   inline O_IND'(RP1, RP2).enc.
   rcondt {2} 4.
   intros &m;wp;skip;progress;smt.
   wp;skip;progress;smt.
   (*** end chal ***)
   skip;progress;smt.
   inline Game_IND(RG, RP(RP1, RP2), ADV).O.check Game_IND'(RP1, RP2, B).O.check.
   case (O_IND.validQs{1}).
   rcondt {1} 2. 
   intros &m;wp;skip;smt.
   rcondt {2} 2.
   intros &m;wp;skip;smt.
   wp;skip;smt.
   rcondf {1} 2. 
   intros &m;wp;skip;smt.
   rcondf {2} 2.
   intros &m;wp;skip;smt.
   wp;rnd;wp;skip;smt.
   qed.
    
   (* Privacy lemma *)   
 
   local lemma vc_is_ind :       
     forall  &m b,
       Pr[ Game_IND(RG, RP(RP1,RP2), ADV).main(b)@ &m:res] = 
               Pr[FHE.Game_IND'(RP1,RP2,B).main(b)@ &m:res].
   proof.
   intros &m b.
   byequiv (vc_is_ind_eq).
   smt.
   by trivial.
   qed.
 
end section.

end SomeVC.