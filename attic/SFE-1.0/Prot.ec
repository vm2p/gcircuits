(* This theory abstracts two-party MPC protocols *)
(* We use it to abstract both OT and SFE *)

theory Protocol.
  (* Party 1 types *)
  type rand1_t.
  type input1_t.
  type output1_t.
  type leak1_t.

  (* Party 2 types *)
  type rand2_t.
  type input2_t.
  type output2_t.
  type leak2_t.

  (* Functionality *)
  op f : input1_t -> input2_t -> output1_t * output2_t.

  (* execution trace and views *)
  type conv_t.

  (* protocol execution & outcomes *)
  op validInputs : input1_t -> input2_t -> bool.
  pred validRands : (input1_t,input2_t,rand1_t, rand2_t).
  op prot : input1_t -> rand1_t -> input2_t -> rand2_t -> conv_t * (output1_t * output2_t).

  (* Leakage functions --- leakage tell us what info, from secret data,
     is expected to be public... (e.g. lengths) *)
  op phi1 : input1_t -> leak1_t.
  op phi2 : input2_t -> leak2_t.
end Protocol.
