require import Bool.
require import Int.
require import Pair.
require import Real.
require import Distr.

require import Array.

(** Entropy Smoothing security assumption *)
require import Prime_field.
require import Cyclic_group_prime.

(** to be moved elsewhere... *)
lemma dbool_mu_id:
 mu {0,1} (fun (x : bool), x) = 1%r / 2%r.
proof.
cut ->: mu {0,1} (fun (x : bool), x) = mu_x {0,1} true.
 rewrite /mu_x //; congr.
 by rewrite -fun_ext => x /=; case x.
by rewrite Dbool.mu_x_def.
qed.

lemma dbool_mu_not:
 mu {0,1} (fun (x : bool), !x) = 1%r / 2%r.
proof.
cut ->: mu {0,1} (fun (x : bool), !x) = mu_x {0,1} false.
 rewrite /mu_x //; congr.
 by rewrite -fun_ext => x /=; case x.
by rewrite Dbool.mu_x_def.
qed.


theory KeyedHash.
 type dom_t.
 type codom_t.
 type hkey_t.
 op dkey:hkey_t distr.

 op hash:hkey_t -> dom_t -> codom_t.
end KeyedHash.

(** Entropy-Smoothing *)
theory ES.
  type dom_t.
  type codom_t.
  type hkey_t.
  op ddom:dom_t distr.
  op dcodom:codom_t distr.
  op dkey:hkey_t distr.

  clone KeyedHash as H with
   type dom_t = dom_t,
   type codom_t = codom_t,
   type hkey_t = hkey_t,
   op dkey = dkey.

  module type Adv_t = {
    proc init(): unit
    proc solve(k:hkey_t, x:codom_t): bool
  }.

  module Game (A:Adv_t) = {
    proc game(b:bool): bool = {
      var x:dom_t;
      var k:hkey_t;
      var b':bool;
      var y:codom_t;

      A.init();
      x = $ddom;
      k = $dkey;
      y = $dcodom;
      if (b) b' = A.solve(k,H.hash k x); else b' = A.solve(k,y);
      return b = b';
    }
    proc main(): bool = {
      var b, adv: bool;
      b = ${0,1};
      adv = game(b);
      return adv;
    }
  }.

(** conditional probability expansion *)
lemma Game_xpnd &m:
forall (A<:Adv_t),
 Pr[Game(A).main()  @ &m: res]
 = 1%r/2%r * (Pr[Game(A).game(true)  @ &m: res] +
              Pr[Game(A).game(false)  @ &m: res]).
proof.
move => A.
pose p1 := Pr[Game(A).game(true) @ &m : res].
pose p2 := Pr[Game(A).game(false) @ &m : res].
byphoare (_: (glob A) = (glob A){m} ==> _) => //.
proc.
seq 1: b (1%r/2%r) p1 (1%r/2%r) p2 ((glob A) = (glob A){m}).
 by auto.
 by rnd; skip; smt.
 call (_: (glob A) = (glob A){m} /\ b ==> res)=> //.
  rewrite /p1.
  bypr=> &m' [eqA] b'.
  byequiv (_: ={glob A,b} /\ b{1} ==> ={res})=> //.
    by sim.
    by rewrite eqA b'.
 by rnd; skip; smt.
 call (_: (glob A) = (glob A){m} /\ !b ==> res)=> //.
  rewrite /p2.
  bypr=> &m' [eqA] b'.
  byequiv (_: ={glob A,b} /\ !b{1} ==> ={res})=> //.
    by sim.
    by move: b'; rewrite -neqF eqA=> ->.
smt.
qed.

(** Advantage definition *)
lemma Game_adv &m: 
forall (A<:Adv_t),
 weight dkey = 1%r =>
 weight ddom = 1%r =>
 weight dcodom = 1%r => 
 islossless A.init =>
 islossless A.solve =>
 2%r * Pr[Game(A).main()  @ &m: res] - 1%r
 = Pr[Game(A).game(true)  @ &m: res] 
   - Pr[Game(A).game(false)  @ &m: !res].
proof.
move => A H1 H2 H3 Ainit_ll Asolve_ll.
rewrite (Game_xpnd &m A).
rewrite Pr [mu_not].
(* lossless condition *)
cut ->: Pr[Game(A).game(false) @ &m : true] = 1%r.
 byphoare (_:true); trivial; proc.
 case (b).
  rcondt 5; first by rnd; rnd; rnd; call (_:true); skip; progress.
  call (_:true); trivial.
  rnd; rnd; rnd; call (_:true); first by proc true.
  by skip; progress.
 rcondf 5; first by rnd; rnd; rnd; call (_:true); skip; progress.
 call (_:true); trivial.
 rnd; rnd; rnd; call (_:true); first by proc true.
 by skip; progress.
smt.
qed.

op epsilon: real.

axiom ES_assumption:
  isuniform dkey =>
  isuniform dcodom =>
  exists (epsilon:real),
   forall (A<:Adv_t) &m,
    `|2%r * Pr[Game(A).main()@ &m:res] - 1%r| <= epsilon.

end ES.

(** List variation of the Entropy-Smoothing assumption 

   Inputs: list (array) of elements in codom_t
   Output: a boolean b, trying to guess if those elements are the image of the hash, or random elements
*)
theory ESn.
  type dom_t.
  type codom_t.
  type hkey_t.
  op ddom:dom_t distr.
  op dcodom:codom_t distr.
  op dkey:hkey_t distr.

  op nmax: int.

  clone KeyedHash as H with
   type dom_t = dom_t,
   type codom_t = codom_t,
   type hkey_t = hkey_t,
   op dkey = dkey.
  
  module type Adv_t = {
    proc choose_n(): int
    proc solve(key: hkey_t, a:codom_t array): bool
  }.

  module Game (A:Adv_t) = {
    proc game(b:bool): bool = {
      var n:int;
      var key:hkey_t;
      var x:dom_t array;
      var y,z:codom_t array;
      var guess:bool;

      n = A.choose_n();
      n = (0 <= n <= nmax) ? n : 0;

      key = $H.dkey;
      x = $Darray.darray n ddom;
      y = $Darray.darray n dcodom;

      if (b)
       z = init n (fun k, H.hash key x.[k]);
      else
       z = y;
      
      guess = A.solve(key, z);
      return guess = b;
    }
    proc main(): bool = {
      var b, adv: bool;

      b = ${0,1};
      adv = game(b);
      return adv;
    }
  }.

(** conditional probability expansion *)
lemma Game_xpnd &m:
forall (A<:Adv_t),
 Pr[Game(A).main()  @ &m: res]
 = 1%r/2%r * (Pr[Game(A).game(true)  @ &m: res] +
              Pr[Game(A).game(false)  @ &m: res]).
proof.
move => A.
pose p1 := Pr[Game(A).game(true) @ &m : res].
pose p2 := Pr[Game(A).game(false) @ &m : res].
byphoare (_: (glob A) = (glob A){m} ==> _) => //.
proc.
seq 1: b (1%r/2%r) p1 (1%r/2%r) p2 ((glob A) = (glob A){m}); trivial.
 by auto.
 by rnd; skip; smt.
 call (_: (glob A) = (glob A){m} /\ b ==> res)=> //.
  rewrite /p1.
  bypr=> &m' [eqA] b'.
  byequiv (_: ={glob A,b} /\ b{1} ==> ={res})=> //.
    by sim.
    by rewrite eqA b'.
 by rnd; skip; smt.
 call (_: (glob A) = (glob A){m} /\ !b ==> res)=> //.
  rewrite /p2.
  bypr=> &m' [eqA] b'.
  byequiv (_: ={glob A,b} /\ !b{1} ==> ={res})=> //.
    by sim.
    by move: b'; rewrite -neqF eqA=> ->.
smt.
qed.

(** Advantage definition *)
lemma Game_adv &m: 
forall (A<:Adv_t),
 weight dkey = 1%r =>
 weight ddom = 1%r =>
 weight dcodom = 1%r => 
 islossless A.choose_n =>
 islossless A.solve =>
 2%r * Pr[Game(A).main()  @ &m: res] - 1%r
 = Pr[Game(A).game(true)  @ &m: res] 
   - Pr[Game(A).game(false)  @ &m: !res].
proof.
move => A H1 H2 H3 Ainit_ll Asolve_ll.
rewrite (Game_xpnd &m A).
rewrite Pr [mu_not].
(* lossless condition *)
cut ->: Pr[Game(A).game(false) @ &m : true] = 1%r.
 byphoare (_:true); trivial; proc.
 case (b).
  rcondt 6; first by rnd; rnd; rnd; wp; call (_:true); skip; progress.
  call (_:true); trivial.
  wp; rnd; rnd; rnd; wp; call (_:true); trivial.
  by skip; progress; smt.
 rcondf 6; first by rnd; rnd; rnd; wp; call (_:true); skip; progress.
 call (_:true); trivial.
 wp; rnd; rnd; rnd; wp; call (_:true); trivial.
 by skip; progress; smt.
smt.
qed.

op epsilon: real.

axiom ES_assumption:
  isuniform dkey =>
  isuniform dcodom =>
  exists (epsilon:real),
   forall (A<:Adv_t) &m,
    `|2%r * Pr[Game(A).main()@ &m:res] - 1%r| <= epsilon.

end ESn.
