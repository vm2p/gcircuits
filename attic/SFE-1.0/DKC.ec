require import Int.
require import Bitstring.
require import Distr.
require import Bool.
require import Pair.

(* Theory representing a DKC scheme used in the garbling of a function *)
theory DKC.
  type tweak_t.
  type key1_t.
  type key2_t.
  type msg_t.
  type cipher_t.

  (* encrypton and decyprtion operator *)
  op E: tweak_t -> key1_t -> key2_t -> msg_t -> cipher_t.
  op D: tweak_t  -> key1_t -> key2_t -> cipher_t -> msg_t.

  pred Correct (x:unit) = forall t k1 k2 x, D t k1 k2 (E t k1 k2 x) = x.
end DKC.