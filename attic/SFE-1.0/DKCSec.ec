require import Array.
require import FMap.
require import FSet.
require import Bool.
require import Pair.
require import Option.
require import Int.
require import Distr.

require import DKC.
require ExtWord.

theory DKCSecurity.
  clone import ExtWord as W.

  clone export DKC as D with
    type tweak_t = word,
    type key1_t = word,
    type key2_t = word,
    type msg_t = word,
    type cipher_t = word.

  type query = (int * bool) * (int * bool) * bool * word.
  type answer = word * word * word.

  op defaultQ: query. (* Why use this instead of option? What happens if the adversary queries defaultQ? *)
  op bad: answer.     (* Same question: can any query other than defaultQ return bad? *)
                      (* Response : we need this in order to initlaize array *)
  module type Dkc_t = {
    proc * preInit(): unit
    proc initialize(): bool
    proc encrypt(q:query): answer
    proc get_challenge(): bool
  }.

  module type Adv_t = {
    proc preInit(): unit
    proc gen_queries(info:bool): query array
    proc get_challenge(answers:answer array) : bool
  }.

  module DKCm = {
    var b:bool
    var ksec:word
    var r:(int * bool,word) map
    var kpub:(int * bool,word) map
    var used:word set
  }.

  module Dkc : Dkc_t = {
    proc preInit(): unit = {
      DKCm.kpub = witness;
      DKCm.ksec = witness;
      DKCm.r    = witness;
      DKCm.used = witness;
      DKCm.b    = $Dbool.dbool;
    }
      
    proc initialize() : bool = {
      var t : bool;
      t = $Dbool.dbool;
      DKCm.ksec = $Dword.dwordLsb t;
      DKCm.kpub = FMap.empty;
      DKCm.used = FSet.empty;
      DKCm.r = FMap.empty;
      return t;
    }

    proc get_challenge() : bool = {
      return DKCm.b;
    }

    proc get_k(i:int * bool): word = {
      var r:word;

      r = $Dword.dwordLsb (snd i);
      if (!in_dom i DKCm.kpub) DKCm.kpub.[i] = r;
      return oget DKCm.kpub.[i];
    }

    proc get_r(i:int * bool): word = {
      var r:word;

      r = $Dword.dword;
      if (!in_dom i DKCm.r) DKCm.r.[i] = r;
      return oget DKCm.r.[i];
    }

    proc encrypt(q:query): answer = {
      var ka, kb, ki, kj, rj, msg, t:word;
      var i, j:int * bool;
      var pos:bool;
      var out:answer = bad;

      (i,j,pos,t) = q;
      if (!(mem t DKCm.used) /\ fst i < fst j)
      {
        DKCm.used = add t DKCm.used;
        ki = get_k(i);
        kj = get_k(j);
        rj = get_r(j);

        (ka,kb) = if pos then (DKCm.ksec,ki) else (ki,DKCm.ksec);
        msg = if (DKCm.b) then kj else rj;
        out = (ki,kj,E t ka kb msg);
      }
      return out;
    }
  }.

  op Dkc_encrypt (Dkc_r:(int * bool,word) map)
                 (Dkc_kpub:(int * bool,word) map)
                 (Dkc_ksec:word)
                 (Dkc_b:bool) (Dkc_used:word set) (q:query) = 
    let (i,j,pos,t) = q in
    if (!(mem t Dkc_used) /\ fst i < fst j) then
      let Dkc_used = add t Dkc_used in
      let ki = oget Dkc_kpub.[i] in
      let kj = oget Dkc_kpub.[j] in
      let rj = oget Dkc_r.[j] in
      let (ka,kb) = if pos then (Dkc_ksec,ki) else (ki,Dkc_ksec) in
      let msg = if (Dkc_b) then kj else rj in
      (Dkc_used, (ki,kj,E t ka kb msg))
    else (Dkc_used, bad).

  lemma get_kL :
    islossless Dkc.get_k.
  proof.
  by proc; wp; rnd; skip; smt.
  qed.

  lemma get_rL: islossless Dkc.get_r.
  proof. by proc; auto; smt. qed.

  lemma encryptL: islossless Dkc.encrypt.
  proof.
    proc.
    seq 2: true 1%r 1%r 0%r _ (q = (i,j,pos,t))=> //; first 2 by auto=> &hr; elim (q{hr}).
    by if=> //; wp; call get_rL=> //; do !(call get_kL=> //); wp.
  qed.

  module type Exp = {
    proc preInit():unit
    proc work():bool
    proc main():bool
  }.

  module type Batch_t = {
    proc encrypt(qs:query array): answer array
  }.

  module BatchDKC (D:Dkc_t): Batch_t = {
    proc encrypt(qs:query array): answer array = {
      var i, n:int;
      var answers:answer array;

      i = 0;
      n = length qs;
      answers = Array.init n (fun x, bad);
      while (i < n)
      {
        answers.[i] = D.encrypt(qs.[i]);
        i = i + 1;
      }

      return answers;
    }
  }.

  lemma encryptBatchL (D <: Dkc_t { BatchDKC }):
    islossless D.encrypt =>
    islossless BatchDKC(D).encrypt.
  proof strict.
  intros=> encryptL; proc.
  while true (n - i);
    first by intros=> z; wp; call encryptL; skip; smt.
  by wp; skip; smt.
  qed.

  module Game(D:Dkc_t, A:Adv_t) : Exp = {
    module B = BatchDKC(D)

    proc preInit() : unit = {
      D.preInit();
      A.preInit();
    }

    proc work() : bool = {
      var queries : query array;
      var answers : answer array;
      var a : answer array;
      var i : int;
      var info : bool;
      var advChallenge : bool;
      var realChallenge : bool;
      var nquery : int;
      var answer : answer;

      info = D.initialize();
      queries = A.gen_queries(info);
      answers = B.encrypt(queries);
      advChallenge = A.get_challenge(answers);
      realChallenge = D.get_challenge();
      return advChallenge = realChallenge;
    }

    proc main() : bool = {
      var r : bool;
      preInit();
      r = work();
      return r;
    }
  }.

lemma preInitE (D<:Dkc_t) (A<:Adv_t{D}) (D'<:Dkc_t) (A'<:Adv_t{D'}) :
  equiv[A.preInit ~ A'.preInit: true ==> true] =>
  equiv[D.preInit ~ D'.preInit: true ==> ={res}] =>
  equiv[Game(D, A).preInit ~ Game(D', A').preInit: ={glob Game} ==> ={glob Game} /\ ={res}].
proof. by intros h1 h2; proc; call h1; call h2. qed.

lemma workE (D<:Dkc_t) (A<:Adv_t{D}) (D'<:Dkc_t) (A'<:Adv_t{D'}) :
  equiv[A.gen_queries ~ A'.gen_queries: ={info} ==> ={res}] =>
  equiv[A.get_challenge ~ A'.get_challenge: ={answers} ==> ={res}] =>
  equiv[D.initialize ~ D'.initialize: true ==> ={res}] =>
  equiv[D.encrypt ~ D'.encrypt: ={q} ==> ={res}] =>
  equiv[D.get_challenge ~ D'.get_challenge: true ==> ={res}] =>
  equiv[Game(D, A).work ~ Game(D', A').work: ={glob Game} ==> ={glob Game} /\ ={res}].
proof.
intros h1 h2 h3 h4 h5.
proc.
inline Game(D, A).B.encrypt.
inline Game(D', A').B.encrypt.
call h5;call h2;wp.
while (={i0, qs, n, answers0});
  first by wp;call h4.
by wp;call h1;call h3.
qed.

lemma mainE (D<:Dkc_t) (A<:Adv_t{D}) (D'<:Dkc_t) (A'<:Adv_t{D'}) :
  equiv[A.preInit ~ A'.preInit: true ==> true] =>
  equiv[A.gen_queries ~ A'.gen_queries: ={info} ==> ={res}] =>
  equiv[A.get_challenge ~ A'.get_challenge: ={answers} ==> ={res}] =>
  equiv[D.preInit ~ D'.preInit: true ==> ={res}] =>
  equiv[D.initialize ~ D'.initialize: true ==> ={res}] =>
  equiv[D.encrypt ~ D'.encrypt: ={q} ==> ={res}] =>
  equiv[D.get_challenge ~ D'.get_challenge: true ==> ={res}] =>
  equiv[Game(D, A).main ~ Game(D', A').main: ={glob Game} ==> ={glob Game} /\ ={res}].
proof.
progress.
proc.
call (workE D A D' A' _ _ _ _ _)=> //.
by call (preInitE D A D' A' _ _).
qed.

  (** Adaptive adversary: is it possible to find an expressible
      condition on A.work that guarantees the desired equivalence?
   INSIGHT: it looks like it should be sufficient to look at Query
      to prove the adaptive -> non-adaptive result:
        1) it is never called with a = b;
        2) for a given g, it is called three times with rnd = false,
           but different (alpha,beta) pairs (hence different tweaks);
        3) for a given g, it is called once with rnd = true (fresh tweak, random j)
        4) for different g, the tweaks are necessarily different
      **5) the queries (i,j,pos,T) depend only on a, b, g, the two constants alpha and beta,
           and pre-initialized variables (the circuit and the t.[i]s) *)
  module type AdvAda_t(DKC:Dkc_t) = {
    proc preInit() : unit {}
    proc work(info:bool) : bool {DKC.encrypt}
  }.

  module GameAda(D:Dkc_t, Adv:AdvAda_t): Exp = {
    module A = Adv(D)

    proc preInit(): unit = {
      D.preInit();
      A.preInit();
    }

    proc work(): bool = {
      var info: bool;
      var advChallenge: bool;
      var realChallenge: bool;

      info = D.initialize();

      advChallenge = A.work(info);
      realChallenge = D.get_challenge();
      return advChallenge = realChallenge;
    }

    proc main(): bool = {
      var r: bool;

      preInit();
      r = work();
      return r;
    }
  }.

  (* Eager *)
  op all_pair_int_aux (js:int * (int*bool) set) = 
      let (j,s) = js in
      let s = add (j, true) s in
      let s = add (j, false) s in
      (j+1, s).
      
   op all_pair_int (n:int) = 
     snd (Int.fold all_pair_int_aux (0, FSet.empty) n).

   lemma all_pair_int_spec (n:int) :
      forall i v, 0 <= i < n => mem (i,v) (all_pair_int n).
   proof.
     intros i v Hi.
     rewrite /all_pair_int.
     cut Hn : 0 <= n by smt.
     cut H : fst (fold all_pair_int_aux (0, FSet.empty) n) = n /\
         (forall i, 
          0 <= i < n => 
          mem (i, v) (snd (fold all_pair_int_aux (0, FSet.empty) n))).
     apply (Int.Induction.induction (fun x,
          fst (fold all_pair_int_aux (0, FSet.empty) x) = x /\
          forall i, 0 <= i < x => 
          mem (i, v) (snd (fold all_pair_int_aux (0, FSet.empty) x))) _ _ n _) => //=.
       smt.
       move => {Hi} {i} k Hk. 
       rewrite foldS //; case (fold _ _ k) => j s; rewrite /all_pair_int_aux /fst /snd /= 
         => [-> Hrec2] /= i Hi; rewrite !mem_add.
       case (i < k) => Hix. smt.
       cut ->: (i = k) by smt.
       by case v.          
     by elim H => _ H;apply H.
   qed.

   op bound : int.

   module DkcE = {
  
     proc preInit = Dkc.preInit

     proc get_challenge = Dkc.get_challenge

     proc get_k = Dkc.get_k

     proc get_r = Dkc.get_r

     proc encrypt = Dkc.encrypt
  
     proc resample_r () : unit = {
       var work, f, y;
       work = all_pair_int bound;
       while (work <> FSet.empty) {
         f = pick work;
         y = Dkc.get_r(f);
         work = rm f work;
       }
     }

     proc resample_k () : unit = {
       var work, f, y;
       work = all_pair_int bound;
       while (work <> FSet.empty) {
         f = pick work;
         y = Dkc.get_k(f);
         work = rm f work;
       }
     }

     proc resample():unit = {
       resample_r ();
       resample_k ();
     }

     proc initialize() : bool = {
       var t;
       t = Dkc.initialize ();
       resample();
       return t;
     }
   }.

  section.

   declare module B:AdvAda_t {Dkc}.
   local module Ada = GameAda(Dkc,B).
   local module AdaE = GameAda(DkcE,B).

   lemma DkcE_resampleL : phoare [DkcE.resample : true ==> true] = 1%r.
   proof.
     proc; inline *.
     while (true) (card work0).   
      intros z;auto;smt.
     wp;conseq (_: _ ==> true);first smt.
     while (true) (card work).   
      intros z;auto;smt.
     auto; smt.
   qed.

   local equiv Dkcget_r_in ivb1 (w1:word):
     Dkc.get_r ~ Dkc.get_r : 
       ={i, DKCm.r} /\ DKCm.r{1}.[ivb1] = Some w1 ==>
       ={res, DKCm.r} /\ DKCm.r{1}.[ivb1] = Some w1.
   proof.
     proc;wp;rnd;skip;progress [-split];smt.
   qed.

   local equiv Dkcget_r_diff ivb1 tok1 :
     Dkc.get_r ~ Dkc.get_r : 
       ={i} /\ !in_dom ivb1 DKCm.r{1} /\
         DKCm.r{2} = DKCm.r{1}.[ivb1 <- tok1] /\ ivb1 <> i{1} ==>
       ={res} /\ !in_dom ivb1 DKCm.r{1} /\ DKCm.r{2} = DKCm.r{1}.[ivb1 <- tok1].
   proof.
     proc;wp;rnd;skip;progress [-split];smt.
   qed.

   local lemma eager_get_r:
     eager [DkcE.resample(); , Dkc.get_r ~ Dkc.get_r, DkcE.resample(); :
        ={arg, glob DKCm} ==>  ={DKCm.r, DKCm.kpub} /\ ={res}].
   proof.
    eager proc.
    inline DkcE.resample. 
    swap{1} 2 3. sim.
    inline DkcE.resample_r.
    swap{2} 4 -3; sp 1 1.
    exists * i{1}, DKCm.r{1}; elim* => ivb1 xx1;wp.
    case (in_dom i{1} DKCm.r{1}).
       pose w1 := oget (xx1.[ivb1]).
       rnd{1}; while (={work, DKCm.r} /\  DKCm.r{1}.[ivb1] = Some w1).
         by wp;call (Dkcget_r_in ivb1 w1);auto.
       auto;progress[-split];smt. 
    case (!mem i{1} work{1}).
        swap{1} 2 -1.
        while (={work} /\ !in_dom ivb1 DKCm.r{1} /\ !mem ivb1 work{1} /\
                 DKCm.r{2} = DKCm.r{1}.[ivb1 <- r{1}]).
         exists* r{1};elim* => w1.
         by wp;call (Dkcget_r_diff ivb1 w1);auto;progress;smt. 
        auto;progress [-split];smt. 
    conseq* (_ : _ ==> ={DKCm.r} /\ in_dom i{1} DKCm.r{1} /\ 
                        result{2} = oget (DKCm.r{2}.[i{2}])); first by progress;smt.
    transitivity{1} {r = $Dword.dword;
                     while (! work = (FSet.empty)%FSet) {
                       f = pick work;                    
                       y = $Dword.dword;
                       DKCm.r = 
                        if !in_dom f DKCm.r then DKCm.r.[f <- if f = i then r else y]
                        else DKCm.r;
                       work = rm f work;
                    }}
       (={i,glob DKCm, work} /\ ! in_dom ivb1 DKCm.r{1} /\ mem ivb1 work{1}
             ==> ={i, glob DKCm}) 
        ( ivb1 = i{1} /\ xx1 = DKCm.r{1} /\
            ={i,work,glob DKCm} /\
           ! in_dom i{1} DKCm.r{1} /\ mem i{1} work{1} ==>
           ={glob DKCm} /\
             in_dom i{1} DKCm.r{1} /\ result{2} = oget DKCm.r{2}.[i{2}]) => //.
      progress. exists DKCm.kpub{2}, DKCm.ksec{2}, DKCm.b{2}, DKCm.r{2}, DKCm.used{2},
            i{2}, (all_pair_int bound) => //.
      symmetry.
      conseq (_:  ={glob DKCm, work, i} ==> _) => //.
      eager while (H:r = $Dword.dword; ~ r = $Dword.dword; :
                ={i} ==> ={r})=> //;[by sim | | by sim].
      inline Dkc.get_r. swap{2} 7 -6; wp.
      case ((i = pick work){1}).
        by rnd{1};wp;rnd;wp;rnd{2};skip;progress;smt.
      auto;progress;smt. 
    while (={work} /\ (mem i work = !in_dom i DKCm.r){1} /\
           DKCm.r{2}.[i{1}] = Some r{1} /\
           DKCm.r{2} = (if !in_dom i DKCm.r then DKCm.r.[i <- r] else DKCm.r){1}).
      inline Dkc.get_r;auto;progress;smt. 
    auto;progress;smt. 
  qed. 

   local equiv Dkcget_k_in ivb1 (w1:word):
     Dkc.get_k ~ Dkc.get_k : 
       ={i, DKCm.kpub} /\ DKCm.kpub{1}.[ivb1] = Some w1 ==>
       ={res, DKCm.kpub} /\ DKCm.kpub{1}.[ivb1] = Some w1.
   proof.
     proc;wp;rnd;skip;progress [-split];smt.
   qed.

   local equiv Dkcget_k_diff ivb1 tok1 :
     Dkc.get_k ~ Dkc.get_k : 
       ={i} /\ !in_dom ivb1 DKCm.kpub{1} /\
         DKCm.kpub{2} = DKCm.kpub{1}.[ivb1 <- tok1] /\ ivb1 <> i{1} ==>
       ={res} /\ !in_dom ivb1 DKCm.kpub{1} /\ DKCm.kpub{2} = DKCm.kpub{1}.[ivb1 <- tok1].
   proof.
     proc;wp;rnd;skip;progress [-split];smt.
   qed.

   local lemma eager_get_k:
     eager [DkcE.resample(); , Dkc.get_k ~ Dkc.get_k, DkcE.resample(); :
        ={arg, glob DKCm} ==>  ={DKCm.r, DKCm.kpub} /\ ={res}].
   proof.
    eager proc.
    inline DkcE.resample. 
    swap{1} 1 4. swap{2} 4 1; sim.
    inline DkcE.resample_k.
    swap{2} 4 -3; sp 1 1.
    exists * i{1}, DKCm.kpub{1}; elim* => ivb1 xx1;wp.
    case (in_dom i{1} DKCm.kpub{1}).
       pose w1 := oget (xx1.[ivb1]).
       rnd{1}; while (={work, DKCm.kpub} /\  DKCm.kpub{1}.[ivb1] = Some w1).
         by wp;call (Dkcget_k_in ivb1 w1);auto.
       auto;progress[-split];smt. 
    case (!mem i{1} work{1}).
        swap{1} 2 -1.
        while (={work} /\ !in_dom ivb1 DKCm.kpub{1} /\ !mem ivb1 work{1} /\
                 DKCm.kpub{2} = DKCm.kpub{1}.[ivb1 <- r{1}]).
         exists* r{1};elim* => w1.
         by wp;call (Dkcget_k_diff ivb1 w1);auto;progress;smt. 
        auto;progress [-split];smt. 
    conseq* (_ : _ ==> ={DKCm.kpub} /\ in_dom i{1} DKCm.kpub{1} /\ 
                        result{2} = oget (DKCm.kpub{2}.[i{2}])); first by progress;smt.
    transitivity{1} {r = $Dword.dwordLsb (snd i);
                     while (! work = (FSet.empty)%FSet) {
                       f = pick work;                    
                       y = $Dword.dwordLsb (snd f);
                       DKCm.kpub = 
                        if !in_dom f DKCm.kpub then DKCm.kpub.[f <- if f = i then r else y]
                        else DKCm.kpub;
                       work = rm f work;
                    }}
       (={i,glob DKCm, work} /\ ! in_dom ivb1 DKCm.kpub{1} /\ mem ivb1 work{1}
             ==> ={i, glob DKCm}) 
        ( ivb1 = i{1} /\ xx1 = DKCm.kpub{1} /\
            ={i,work,glob DKCm} /\
           ! in_dom i{1} DKCm.kpub{1} /\ mem i{1} work{1} ==>
           ={glob DKCm} /\
             in_dom i{1} DKCm.kpub{1} /\ result{2} = oget DKCm.kpub{2}.[i{2}]) => //.
      progress. exists DKCm.kpub{2}, DKCm.ksec{2}, DKCm.b{2}, DKCm.r{2}, DKCm.used{2},
            i{2}, (all_pair_int bound) => //.
      symmetry.
      conseq (_:  ={glob DKCm, work, i} ==> _) => //.

      eager while (H:r = $Dword.dwordLsb (snd i); ~ r = $DKCSecurity.W.Dword.dwordLsb (snd i); :
                ={i} ==> ={r})=> //;[by sim | | by sim].
      inline Dkc.get_k. swap{2} 7 -6; wp.
      case ((i = pick work){1}).
        by rnd{1};wp;rnd;wp;rnd{2};skip;progress;smt.
      auto;progress;smt. 
    while (={work} /\ (mem i work = !in_dom i DKCm.kpub){1} /\
           DKCm.kpub{2}.[i{1}] = Some r{1} /\
           DKCm.kpub{2} = (if !in_dom i DKCm.kpub then DKCm.kpub.[i <- r] else DKCm.kpub){1}).
      inline Dkc.get_k;auto;progress;smt. 
    auto;progress;smt. 
  qed. 

   equiv GameAdaEager :
        DKCSecurity.GameAda(Dkc,B).main ~ 
        DKCSecurity.GameAda(DkcE,B).main : ={glob B}  ==> ={glob B, res}.
   proof.
     proc.
     transitivity{1} 
       { Ada.preInit(); r = Ada.work(); DkcE.resample(); } 
       (={glob B} ==> ={glob B, r}) (={glob B} ==> ={glob B, r}) => //.
       by progress; exists (glob B){2}.
       call{2} DkcE_resampleL;sim.
     inline Ada.work. inline GameAda(DkcE, B).work.
     inline DkcE.initialize. swap{2} 4 -1.
     seq 2 3 : (={info, glob B, glob Dkc}); first by sim.
     symmetry.
     eager (H: DkcE.resample(); ~ DkcE.resample();:
                  ={DKCm.kpub, DKCm.r} ==> ={DKCm.kpub, DKCm.r}): 
            (={glob B, glob Dkc}) => //;first by sim.
      eager proc; swap{1} 1;sim.
      eager proc H (={glob DKCm})=> //; last by sim.
      eager proc.       
      eager (H0: DkcE.resample(); ~ DkcE.resample();:
                  ={DKCm.kpub, DKCm.r} ==> ={DKCm.kpub, DKCm.r}):
          (={glob DKCm}) => //.
     apply eager_get_r. apply eager_get_k.
   qed.

   declare module B':Adv_t {Dkc}.
   local module G = Game(Dkc,B').
   local module GE = Game(DkcE,B').

   equiv GameEager :
        DKCSecurity.Game(Dkc,B').main ~ 
        DKCSecurity.Game(DkcE,B').main : ={glob B'}  ==> ={glob B', res}.
   proof.
     proc.
     transitivity{1} 
       { G.preInit(); r = G.work(); DkcE.resample(); } 
       (={glob B'} ==> ={glob B', r}) (={glob B'} ==> ={glob B', r}) => //.
       by progress; exists (glob B'){2}.
       call{2} DkcE_resampleL;sim.
     inline G.work. inline Game(DkcE, B').work.
     inline DkcE.initialize. swap{2} 4 -1.
     seq 2 3 : (={info, glob B', glob Dkc}); first by sim.
     symmetry.
     eager (H: DkcE.resample(); ~ DkcE.resample();:
                  ={DKCm.kpub, DKCm.r} ==> ={DKCm.kpub, DKCm.r}): 
            (={glob B', glob Dkc}) => //;first by sim.
      eager proc; swap{1} 1;sim.
      eager proc H (={glob DKCm})=> //.
      eager proc.       
      eager (H0: DkcE.resample(); ~ DkcE.resample();:
                  ={DKCm.kpub, DKCm.r} ==> ={DKCm.kpub, DKCm.r}):
          (={glob DKCm}) => //.
      eager proc.       
      eager (H0: DkcE.resample(); ~ DkcE.resample();:
                  ={DKCm.kpub, DKCm.r} ==> ={DKCm.kpub, DKCm.r}):
          (={glob DKCm}) => //.
     apply eager_get_r. apply eager_get_k.
     eager proc H (={glob DKCm})=> //.
   qed.

  end section.

end DKCSecurity.
