
 

(*
local lemma initG_adaCore : hoare [Hybrid2(IB).initG :
  inv_xxG IB.xxG C.n C.q G.randG
  ==>
  inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t G.randG CV.l /\
  inv_Gpp_dom2 C.n C.aa C.bb R.t G.pp (C.n + C.q)
].
proof.
  proc.
  inline *.
  wp.
  while (
    inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t G.randG CV.l /\
    inv_Gpp_dom2 C.n C.aa C.bb R.t G.pp G.g /\
    inv_xxG IB.xxG C.n C.q G.randG
  ).
    seq 62 : (
      inv_G G.g G.a G.b C.aa C.bb /\
      inv_Gpp G.pp C.v C.aa C.bb C.gg R.xx R.t G.randG CV.l /\
      inv_Gpp_dom2 C.n C.aa C.bb R.t G.pp G.g /\
      inv_xxG IB.xxG C.n C.q G.randG /\
      G.yy{hr}.[G.g{hr}] = (oget G.pp{hr}.[(G.g{hr}, ! R.t{hr}.[C.aa{hr}.[G.g{hr}]],! R.t{hr}.[C.bb{hr}.[G.g{hr}]])]) /\
      F.flag_sp = (G.a <= CV.l < G.b /\ C.gg.[(G.g, ! C.v.[G.a], false)] = C.gg.[(G.g, ! C.v.[G.a], true)])
    ).
    admit.
    if.
    wp.
    skip.
    rewrite /inv_G /inv_Gpp /inv_Gpp_dom2.
    progress zeta.
    rewrite !(FMap.get_set, (Bool.xorC true), (Bool.xorC false), xor_true, xor_false).
(*
    by rewrite ifF 1:smt H 1:smt.

    rewrite !(FMap.get_set, (Bool.xorC true), (Bool.xorC false), xor_true, xor_false).
    case (g = G.g{hr});progress.
rewrite H5 H6 H7 H8 /=.
cut -> : forall (x:bool), (x = ! x) = false by smt.
by rewrite oget_some.




congr=> //=.
split.
print theory Bool.
case (_ = ( _)).


      intros ->.
    rewrite ifF.
smt.

 1:smt H 1:smt.*)
admit.
admit.
admit.
admit.
admit.
admit.
admit.
admit.
qed.
*)


(*
local lemma query_adaCore : hoare [RedI_AdaRO(IB, MyDkc(IB)).query :
  (0 <= G.g) /\
  ((C.n + C.q) <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) R.xx => R.xx.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) DKCm.kpub => DKCm.kpub.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool) (a:bool) (b:bool),
    0 <= j < C.n + C.q => in_dom (j, v) DKCm.r => DKCm.r.[(j, v)] =
    IB.xxG.[(j, if (CV.l = C.aa.[j]) then true else false, if (CV.l = C.aa.[j]) then false else true)]) /\
  (forall (j : int) (a b : bool),
    0 <= j < C.n + C.q => in_dom (j, a, b) G.randG => G.randG.[(j, a, b)] =
    IB.xxG.[(j, a, b)]) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (a = CV.l) <=> in_dom (g, !R.t.[a],  R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (a = CV.l) <=> in_dom (g, !R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (b = CV.l) <=> in_dom (g,  R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (b = CV.l) <=> in_dom (g, !R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, R.t.[a], R.t.[b]) G.pp =>
      oget G.pp.[(g, R.t.[a], R.t.[b])] = oget R.xx.[(g, (oget C.gg.[(g, C.v.[a],C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, !R.t.[a], R.t.[b]) G.pp =>
      oget G.pp.[(g, !R.t.[a], R.t.[b])] =
        if a <= CV.l
        then (if CV.l < b /\ ((oget C.gg.[(g, !C.v.[a],false)]) = (oget C.gg.[(g, !C.v.[a],true)]))
                then oget G.pp.[(g, !R.t.[a], !R.t.[b])]
                else oget G.randG.[(g, true, false)])
        else oget R.xx.[(g, (oget C.gg.[(g, !C.v.[a],C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, R.t.[a], !R.t.[b]) G.pp =>
      oget G.pp.[(g, R.t.[a], !R.t.[b])] =
        if b <= CV.l
        then oget G.randG.[(g, false, true)]
        else oget R.xx.[(g, (oget C.gg.[(g, C.v.[a],!C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, !R.t.[a], !R.t.[b]) G.pp =>
      oget G.pp.[(g, !R.t.[a], !R.t.[b])] =
        if b <= CV.l
        then oget G.randG.[(g, true, true)]
        else oget R.xx.[(g, (oget C.gg.[(g, !C.v.[a],!C.v.[b])]))])
==>
  (0 <= G.g) /\
  ((C.n + C.q) <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) R.xx => R.xx.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) DKCm.kpub => DKCm.kpub.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool) (a:bool) (b:bool),
    0 <= j < C.n + C.q => in_dom (j, v) DKCm.r => DKCm.r.[(j, v)] =
    IB.xxG.[(j, if (CV.l = C.aa.[j]) then true else false, if (CV.l = C.aa.[j]) then false else true)]) /\
  (forall (j : int) (a b : bool),
    0 <= j < C.n + C.q => in_dom (j, a, b) G.randG => G.randG.[(j, a, b)] =
    IB.xxG.[(j, a, b)]) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (a = CV.l) <=> in_dom (g, !R.t.[a],  R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (a = CV.l) <=> in_dom (g, !R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (b = CV.l) <=> in_dom (g,  R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (b = CV.l) <=> in_dom (g, !R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, R.t.[a], R.t.[b]) G.pp =>
      oget G.pp.[(g, R.t.[a], R.t.[b])] = oget R.xx.[(g, (oget C.gg.[(g, C.v.[a],C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, !R.t.[a], R.t.[b]) G.pp =>
      oget G.pp.[(g, !R.t.[a], R.t.[b])] =
        if a <= CV.l
        then (if CV.l < b /\ ((oget C.gg.[(g, !C.v.[a],false)]) = (oget C.gg.[(g, !C.v.[a],true)]))
                then oget G.pp.[(g, !R.t.[a], !R.t.[b])]
                else oget G.randG.[(g, true, false)])
        else oget R.xx.[(g, (oget C.gg.[(g, !C.v.[a],C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, R.t.[a], !R.t.[b]) G.pp =>
      oget G.pp.[(g, R.t.[a], !R.t.[b])] =
        if b <= CV.l
        then oget G.randG.[(g, false, true)]
        else oget R.xx.[(g, (oget C.gg.[(g, C.v.[a],!C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, !R.t.[a], !R.t.[b]) G.pp =>
      oget G.pp.[(g, !R.t.[a], !R.t.[b])] =
        if b <= CV.l
        then oget G.randG.[(g, true, true)]
        else oget R.xx.[(g, (oget C.gg.[(g, !C.v.[a],!C.v.[b])]))])
].
proof.
proc.
admit.
inline *.
seq 12 : (
  (0 <= G.g) /\
  ((C.n + C.q) <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) R.xx => R.xx.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) DKCm.kpub => DKCm.kpub.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool) (a:bool) (b:bool),
    0 <= j < C.n + C.q => in_dom (j, v) DKCm.r => DKCm.r.[(j, v)] =
    IB.xxG.[(j, if (CV.l = C.aa.[j]) then true else false, if (CV.l = C.aa.[j]) then false else true)]) /\
  (forall (j : int) (a b : bool),
    0 <= j < C.n + C.q => in_dom (j, a, b) G.randG => G.randG.[(j, a, b)] =
    IB.xxG.[(j, a, b)]) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (a = CV.l) <=> in_dom (g, !R.t.[a],  R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (a = CV.l) <=> in_dom (g, !R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (b = CV.l) <=> in_dom (g,  R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (b = CV.l) <=> in_dom (g, !R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, R.t.[a], R.t.[b]) G.pp =>
      oget G.pp.[(g, R.t.[a], R.t.[b])] = oget R.xx.[(g, (oget C.gg.[(g, C.v.[a],C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, !R.t.[a], R.t.[b]) G.pp =>
      oget G.pp.[(g, !R.t.[a], R.t.[b])] =
        if a <= CV.l
        then (if CV.l < b /\ ((oget C.gg.[(g, !C.v.[a],false)]) = (oget C.gg.[(g, !C.v.[a],true)]))
                then oget G.pp.[(g, !R.t.[a], !R.t.[b])]
                else oget G.randG.[(g, true, false)])
        else oget R.xx.[(g, (oget C.gg.[(g, !C.v.[a],C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, R.t.[a], !R.t.[b]) G.pp =>
      oget G.pp.[(g, R.t.[a], !R.t.[b])] =
        if b <= CV.l
        then oget G.randG.[(g, false, true)]
        else oget R.xx.[(g, (oget C.gg.[(g, C.v.[a],!C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, !R.t.[a], !R.t.[b]) G.pp =>
      oget G.pp.[(g, !R.t.[a], !R.t.[b])] =
        if b <= CV.l
        then oget G.randG.[(g, true, true)]
        else oget R.xx.[(g, (oget C.gg.[(g, !C.v.[a],!C.v.[b])]))])
); first by auto.

seq 21 : (
  (0 <= G.g) /\
  ((C.n + C.q) <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) R.xx => R.xx.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool),
    0 <= j < C.n + C.q => in_dom (j, v) DKCm.kpub => DKCm.kpub.[(j, v)] =
    IB.xx.[(j, v, v ^^ C.v.[j] ^^ R.t.[j])]) /\
  (forall (j : int) (v : bool) (a:bool) (b:bool),
    0 <= j < C.n + C.q => in_dom (j, v) DKCm.r => DKCm.r.[(j, v)] =
    IB.xxG.[(j, if (CV.l = C.aa.[j]) then true else false, if (CV.l = C.aa.[j]) then false else true)]) /\
  (forall (j : int) (a b : bool),
    0 <= j < C.n + C.q => in_dom (j, a, b) G.randG => G.randG.[(j, a, b)] =
    IB.xxG.[(j, a, b)]) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (a = CV.l) <=> in_dom (g, !R.t.[a],  R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (a = CV.l) <=> in_dom (g, !R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (b = CV.l) <=> in_dom (g,  R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int), 0 <= g < G.g =>
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    (b = CV.l) <=> in_dom (g, !R.t.[a], !R.t.[b]) G.pp) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, R.t.[a], R.t.[b]) G.pp =>
      oget G.pp.[(g, R.t.[a], R.t.[b])] = oget R.xx.[(g, (oget C.gg.[(g, C.v.[a],C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, !R.t.[a], R.t.[b]) G.pp =>
      oget G.pp.[(g, !R.t.[a], R.t.[b])] =
        if a <= CV.l
        then (if CV.l < b /\ ((oget C.gg.[(g, !C.v.[a],false)]) = (oget C.gg.[(g, !C.v.[a],true)]))
                then oget G.pp.[(g, !R.t.[a], !R.t.[b])]
                else oget G.randG.[(g, true, false)])
        else oget R.xx.[(g, (oget C.gg.[(g, !C.v.[a],C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, R.t.[a], !R.t.[b]) G.pp =>
      oget G.pp.[(g, R.t.[a], !R.t.[b])] =
        if b <= CV.l
        then oget G.randG.[(g, false, true)]
        else oget R.xx.[(g, (oget C.gg.[(g, C.v.[a],!C.v.[b])]))]) /\
  (forall (g:int),
    let a = C.aa.[g] in
    let b = C.bb.[g] in
    in_dom (g, !R.t.[a], !R.t.[b]) G.pp =>
      oget G.pp.[(g, !R.t.[a], !R.t.[b])] =
        if b <= CV.l
        then oget G.randG.[(g, true, true)]
        else oget R.xx.[(g, (oget C.gg.[(g, !C.v.[a],!C.v.[b])]))]) /\
  (kki = if G.a = CV.l
    then oget IB.xx{hr}.[(G.b, C.v.[G.b] ^^ bet1, R.t.[G.b] ^^ bet1)]
    else oget IB.xx{hr}.[(G.a, C.v.[G.a] ^^ alpha1, R.t.[G.a] ^^ alpha1)]
  )
); first last.
wp.
skip.
progress [-split].
split.
(*
rewrite FMap.get_set.
case ((G.b{hr}, C.v{hr}.[G.b{hr}] ^^ bet1{hr}) = (j1, v1));progress.
  by rewrite in_dom_some;smt.
  apply H3.
progress.
assumption.
rewrite 
  smt.
*)
admit.
admit.
admit.
qed.
*)

(* adaCore *)

while{1} (
  (0 <= G.g){1} /\
  ((C.n + C.q){1} <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){1} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG){1} /\
  (forall (j : int) (v : bool),
    0 <= j < C.n{1} + C.q{1} => in_dom (j, v) R.xx{1} => R.xx{1}.[(j, v)] =
    IB.xx{1}.[(j, v, v ^^ C.v{1}.[j] ^^ R.t{1}.[j])]) /\
  (forall (j : int) (a b : bool),
    0 <= j < C.n{1} + C.q{1} => in_dom (j, a, b) G.randG{1} => G.randG{1}.[(j, a, b)] =
    IB.xxG{1}.[(j, a, b)]) /\
  (forall g, ! C.bb{1}.[g] = C.aa{1}.[g])
) ((C.n + C.q - G.g){1}).
progress.
wp.
conseq (_: _ ==> true) (_: _ ==>
  ((C.n + C.q - (G.g + 1)) < z) /\
  (0 <= G.g{hr} + 1) /\
  ((C.n + C.q){hr} <= maxGates) /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xx){hr} /\
  (forall x, mem x (all_tuple_int maxGates) <=> in_dom x IB.xxG){hr} /\
  (forall (j : int) (v : bool),
    0 <= j < C.n{hr} + C.q{hr} => in_dom (j, v) R.xx{hr} => R.xx{hr}.[(j, v)] =
    IB.xx{hr}.[(j, v, v ^^ C.v{hr}.[j] ^^ R.t{hr}.[j])]) /\
  (forall (j : int) (a b : bool),
    0 <= j < C.n{hr} + C.q{hr} => in_dom (j, a, b) G.randG{hr} => G.randG{hr}.[(j, a, b)] =
    IB.xxG{hr}.[(j, a, b)]) /\
  (forall g, ! C.bb{hr}.[g] = C.aa{hr}.[g])
)=> //;[|admit (*ll *)].
sp.
if.
rcondf 3;first (inline *;(kill 1!78;first admit (*ll*));skip;progress;apply H5;smt).
admit.
if.
admit.
skip;progress;smt.

inline *.
wp.
rnd{1}.
wp.
skip.
progress [-split];split;first smt.
cut h : in_dom (CV.l{1}, ! C.v{2}.[CV.l{1}], t{1}) IB.xx{2} by smt.
progress [-split];split.
progress [-split];split;first done.
progress [-split];split;first done.
rewrite h /=.
split.
smt.
split.
smt.
split.
smt.
split.
smt.
admit. (* ???? *)

progress.
smt.
smt.
rewrite h /=.
case (i0 = CV.l{1} && v = ! C.v{2}.[CV.l{1}])=> //.
rewrite H8. smt.
progress.
generalize h.
cut -> : t{1} = !R.t{2}.[CV.l{1}] by smt.
smt.

(************************)


  local equiv GameAda_GameAdaRO :  DKCSecurity.GameAda(Dkc,RedI_Ada(A)).work ~ MIB(GameAdaRO).mainE : 
          ={glob A, DKCm.b, CV.l} ==> 
         ={DKCm.b, res}.
  proof.
admit.
(*
    transitivity MIB(GameAdaRO).mainL (={glob A, DKCm.b, CV.l} ==> ={res, DKCm.b}) (={glob GameAdaRO, x} ==> ={res, DKCm.b});[|done| |conseq (eager_aux GameAdaRO)=> //].

progress.
exists (glob A){2}, DKCm.kpub{2}, DKCm.ksec{2}, DKCm.b{2}, DKCm.r{2}, DKCm.used{2}, R.t{2}, R.xx{2}, C.gg{2}, C.aa{2}, C.m{2}, C.x{2}, C.f{2},
    C.n{2}, C.q{2}, C.bb{2}, C.v{2}, G.b{2}, G.randG{2}, G.pp{2}, G.yy{2}, G.a{2}, G.g{2},
        CV.l{2}, RedComon.query{2}, RedComon.c{2}, x{2}=> //.

    proc*. 
    inline MIB(GameAdaRO).mainL.
    inline MIB(GameAdaRO).A.init.
    inline IB.init.

    wp.
    call (_: ={glob A, DKCm.b, CV.l} /\ IB.xx{2}=FMap.empty  ==> ={res});last by wp.
    proc.
    inline Dkc.get_challenge MyDkc(IB).get_challenge.
    inline GameAda(Dkc, RedI_Ada(A)).A.work GameAda(MyDkc(IB), RedI_AdaRO(IB)).A.work.
    inline Dkc.initialize.
    inline  RedI_AdaRO(IB, MyDkc(IB)).pre RedI_Ada(A, Dkc).Com.pre.
    inline RedI_Ada(A, Dkc).Com.post RedI_AdaRO(IB, MyDkc(IB)).post.

    (* gen_query Adversary call *)
    wp.
    swap{1} 2 12.
    swap{1} 1 3.
    swap{1} [4..7] 5.

    swap{2} 9 -8.
    swap{1} 4 -3.
    seq 1 1 : (={glob A, RedComon.query, DKCm.b, CV.l} /\ IB.xx{2}=FMap.empty).
    call (_: ={glob A, DKCm.b} /\ IB.xx{2}=FMap.empty ==> ={glob A, res, DKCm.b} /\ IB.xx{2}=FMap.empty)=> //;proc (={DKCm.b} /\ IB.xx{2}=FMap.empty)=> //.

    (* Handling query valid test *)
    case (EncSecurity.queryValid_IND RedComon.query{1});first last.

    inline RedI_Ada(A, Dkc).Com.post RedI_AdaRO(IB, MyDkc(IB)).post.

    (* Query invalid Case *)
    seq 17 19 : (={DKCm.b} /\ (!EncSecurity.queryValid_IND RedComon.query{2}) /\ (!EncSecurity.queryValid_IND RedComon.query{1})).
      kill{1} 1!17.
        call (RedComon_garble_ll A).
        call (RedComon_sampleTokens_ll A).
        call (RedI_Ada_coreAdaL A Dkc _).
          by apply encryptL.
        inline *.
        auto; while true (C.n + C.q - i0); first by auto; smt.
        auto; while true (C.n + C.q - i); first by auto; smt.
        by auto; progress; expect 4 smt.
      kill{2} 1!19. admit. (* ll *)
      done.
    rcondf{1} 1=> //.
    rcondf{2} 1=> //.
    by auto; smt.
     
    (* Query valid Case *)
    rcondt{1} 18. 
      move=> &m; kill 1!17.
        call (RedComon_garble_ll A).
        call (RedComon_sampleTokens_ll A).
        call (RedI_Ada_coreAdaL A Dkc _).
          by apply encryptL.
        inline *.
        auto; while true (C.n + C.q - i0); first by auto; smt.
        auto; while true (C.n + C.q - i); first by auto; smt.
        by auto; progress; expect 4 smt.
      done.
    rcondt{2} 20. 
      move=> &m; kill 1!19.
        admit. (* ll *)
      done.

    wp.
    seq 15 17 : (={glob DKCm, glob C, glob R, RedComon.c, glob A, CV.l} /\ (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2});first last.

    seq 1 1 : (={glob DKCm, glob C, glob R, RedComon.c, glob A, CV.l} /\
      (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2}
    );[by call sampleTokensRO=> // |by inline *;sim].

    seq 14 16 : (={glob DKCm, glob C, glob R, RedComon.query, RedComon.c, glob A, CV.l} /\
validCircuitP bound ((C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}), C.gg{2}) /\
  DKCm.kpub{2} = FMap.empty /\
 (forall i v, (i, v) <> (CV.l, true) => !in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx){2} /\
 (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2});
   last call coreAdaRO;auto;progress;smt.

    inline *.
    seq 22 24 : (={DKCm.b, DKCm.r, DKCm.kpub, DKCm.used, glob C, glob R, RedComon.c, RedComon.query, glob A, CV.l, useVisible, i0} /\ (IB.xx = FMap.empty /\  R.xx = FMap.empty /\ DKCm.kpub = FMap.empty){2} /\ info1{1} = t{1} /\ t{1} = t0{2} /\
validCircuitP bound ((C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}), C.gg{2}));last by auto;progress;smt.
    auto.
    while (={DKCm.b, DKCm.r, DKCm.kpub, DKCm.used, glob C, glob R, RedComon.c, i, glob A, CV.l, useVisible, i0} /\ (IB.xx = FMap.empty /\  R.xx = FMap.empty /\ DKCm.kpub = FMap.empty){2} /\
validCircuitP bound ((C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}), C.gg{2})).
    by auto.
auto.
    while (={DKCm.b, DKCm.r, DKCm.kpub, DKCm.used, glob C, RedComon.c, glob A, CV.l, i, p0} /\ (IB.xx = FMap.empty /\  R.xx = FMap.empty /\ DKCm.kpub = FMap.empty){2} /\
validCircuitP bound ((C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}), C.gg{2})).
    by auto.
    auto.
    progress [-split];split;first smt.
    progress [-split];split;first smt.
    progress [-split];split;first done.
    generalize H.
    rewrite /EncSecurity.queryValid_IND /EncSecurity.Encryption.valid_plain /validInputs /C2.validInputs.
    elim (RedComon.query{2})=> l r.
    elim l=> lf li.
    elim lf=> lt lg.
    elim lt=> ln lm lq laa lbb.
    elim r=> rf ri.
    elim rf=> rt rg.
    elim rt=> rn rm rq raa rbb.
    by case cL;
    rewrite !(fst_pair, snd_pair) /=;
    rewrite -!valid_wireinput.
*)
   qed.



queryRO
(*
case (rand{2}).
admit. (* Need the other eager to do this case correctly *)
inline *.
seq 12 15 : (
  ={t, pos0, q0, rand, alpha, bet, gamma, i0, j0, glob DKCm, glob RedI_Ada, out} /\ (gamma = gamma0){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
  (gamma0 = C.v.[G.g] ^^ oget C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]){2} /\
    (forall i v, (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (forall i v,i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
  (v1 = (if G.a = CV.l then C.v.[G.b] ^^ bet else C.v.[G.a] ^^ alpha)){2} /\
  (v2 = (C.v.[G.g] ^^ gamma)){2} /\
  (i0 = (if G.a = CV.l then (G.b, R.t.[G.b] ^^ bet) else (G.a, R.t.[G.a] ^^ alpha)) /\
  (j0 = (G.g, R.t.[G.g] ^^ gamma0)){2} /\
  rand{2} = false){2}
); first by auto; progress; expect 2 smt.
seq 1 1 : (
  ={t, q0, pos0, rand, out, alpha, bet, gamma, i0, j0, glob DKCm, glob RedI_Ada, out} /\ (gamma = gamma0){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
  (in_dom (if G.a = CV.l then (G.b, R.t.[G.b] ^^ bet) else (G.a, R.t.[G.a] ^^ alpha)) DKCm.kpub /\ in_dom (G.g, R.t.[G.g] ^^ gamma0) DKCm.kpub){2} /\
  (gamma0= C.v.[G.g] ^^ oget C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]){2} /\
    (forall i v,
(i, v) <> (if CV.l = G.a then (G.b, C.v.[G.b] ^^ bet) else (G.a, C.v.[G.a] ^^ alpha)) => (i, v) <> (G.g, C.v.[G.g] ^^ gamma0) =>  (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
  (rand{2} = false){2}
).

(* This two admit need an invariant for the tweak, the idea is that we perform
request with G.g increasing and two different G.g implies two tweaks *)
rcondt{1} 1;first by admit.
rcondt{2} 1;first by admit.

seq 4 4 : (={i1, pos0, t, q0, rand, alpha, bet, gamma, i0, j0, glob DKCm, glob RedI_Ada, out} /\ (gamma = gamma0){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
  (in_dom (if G.a = CV.l then (G.b, R.t.[G.b] ^^ bet) else (G.a, R.t.[G.a] ^^ alpha)) DKCm.kpub){2} /\
  (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
    (forall i v,
(i, v) <> (if CV.l = G.a then (G.b, C.v.[G.b] ^^ bet) else (G.a, C.v.[G.a] ^^ alpha)) => (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (j0 = (G.g, R.t.[G.g] ^^ gamma0)){2} /\
  (v2 = (C.v.[G.g] ^^ gamma)){2} /\
  (gamma0= C.v.[G.g] ^^ oget C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]){2} /\
  rand{2} = false
).

case ((in_dom i0 DKCm.kpub){1}).
rcondf{1} 4;first by auto.
rcondf{2} 4;first by auto.
auto;progress;smt.

rcondt{1} 4;first by auto.
rcondt{2} 4;first by auto.

case ( G.a{2} = CV.l{2} )=> /=.
auto;progress [-split].
split;first done.
auto;progress [-split].
generalize H8.
simplify.
rewrite !fst_pair !snd_pair.
rewrite H7 //;first smt => -> /=.
progress.
smt.
by rewrite H8 /= !FMap.get_set /= oget_some.
by rewrite H8 /= !FMap.get_set /= oget_some in_dom_set.
by rewrite H8 /= !in_dom_set H7;case v3;case (bet{2});case (i4 = G.b{2});rewrite ?(xor_true, xor_false) //=;progress;smt. 
by rewrite H8 /= !in_dom_set H6;smt.

auto;progress [-split].
cut h : (G.a{2} = CV.l{2}) = false by smt.
rewrite !h /=.
generalize H8.
simplify.
rewrite !fst_pair !snd_pair.
rewrite !h /=.
rewrite H7 //;first smt => -> /=.
progress.
smt.
by rewrite H8 /= !FMap.get_set /= oget_some.
by rewrite H8 /= !FMap.get_set /= oget_some in_dom_set.
by rewrite H8 /= !in_dom_set H7;case v3;case (bet{2});case (i4 = G.a{2});rewrite ?(xor_true, xor_false) //=;progress;smt. 
by rewrite H8 /= !in_dom_set H6;smt.

seq 4 4 : (={i2, pos0, j0, ki, t, q0, rand, alpha, bet, gamma, i0, j0, glob DKCm, glob RedI_Ada, out} /\ (gamma = gamma0){2} /\
  (G.a < C.n + C.q /\ G.b < C.n + C.q /\ G.g < C.n + C.q /\ G.g <> G.b /\ G.g <> G.a /\ G.a <> G.b /\ G.g <> CV.l){2} /\ 
  (in_dom (if G.a = CV.l then (G.b, R.t.[G.b] ^^ bet) else (G.a, R.t.[G.a] ^^ alpha)) DKCm.kpub /\ in_dom (G.g, R.t.[G.g] ^^ gamma0) DKCm.kpub){2} /\
      (forall i v,
(i, v) <> (if CV.l = G.a then (G.b, C.v.[G.b] ^^ bet) else (G.a, C.v.[G.a] ^^ alpha)) => (i, v) <> (G.g, C.v.[G.g] ^^ gamma0) => (i, v) <> (CV.l, !C.v.[CV.l]) => in_dom (i, v, R.t.[i]^^C.v.[i]^^v) IB.xx = in_dom (i, v) R.xx){2} /\
  (gamma0= C.v.[G.g] ^^ oget C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]){2} /\
  (forall i v, i < C.n + C.q => (i, v) <> (CV.l, true) => in_dom (i, C.v.[i]^^v, R.t.[i]^^v) IB.xx = in_dom (i, R.t.[i]^^v) DKCm.kpub){2} /\
  rand{2} = false
).

case ((in_dom j0 DKCm.kpub){1}).
rcondf{1} 4;first by auto.
rcondf{2} 4;first by auto.
auto;progress;smt.

rcondt{1} 4;first by auto.
rcondt{2} 4;first by auto.

auto;progress [-split].
split.
smt.
auto;progress [-split].
generalize H9.
simplify.
rewrite !fst_pair !snd_pair.
rewrite H7 //;first smt => -> /=.
progress.
smt.
by rewrite H9 /= !FMap.get_set /= oget_some.
by rewrite H9 /= !FMap.get_set /= !oget_some !in_dom_set;left.
by rewrite H9 /= !FMap.get_set /= !oget_some !in_dom_set;right.
by rewrite H9 /= !in_dom_set H8;smt.

rewrite H9 /= !FMap.get_set /= !oget_some !in_dom_set H7 //.
progress.
case (in_dom (i4, R.t{2}.[i4] ^^ v3) DKCm.kpub{2})=> //=.
progress.
case (i4 = G.g{2} && v3 = (C.v{2}.[G.g{2}] ^^ oget C.gg{2}.[(G.g{2}, C.v{2}.[G.a{2}], C.v{2}.[G.b{2}])]))=> //.
progress.
case (i4 = G.g{2})=> //=.
smt.

by auto.

(* BEGIN R.xx *)
auto.
progress.
rewrite !FMap.in_dom_set.
pose v3' := C.v{2}.[i4] ^^ v3.
cut -> : R.t{2}.[i4] ^^ C.v{2}.[i4] ^^ v3 = R.t{2}.[i4] ^^ v3' by smt.
cut -> : v3 = C.v{2}.[i4] ^^ v3' by smt.

case ((i4, v3') = (G.g{2}, C.v{2}.[G.g{2}] ^^
  oget C.gg{2}.[(G.g{2}, C.v{2}.[CV.l{2}], C.v{2}.[G.b{2}])]));progress.
smt.

case (((i4, v3') = (G.b, bet)){2});progress.
smt.
generalize H7=> /= H7.
rewrite -H8;progress.
smt.
generalize H11;smt.
generalize H10;smt.
generalize H11.
generalize H10.
cut -> : forall a b c,  a ^^ b ^^ (b ^^ c) = a ^^ c by smt.
cut lem : forall a b c,  (a ^^ b = a ^^ c) = (b = c) by smt.
case (i4 = G.g{2}).
progress; smt.
case (i4 = G.b{2}).
progress.
rewrite lem.
smt.
smt.

rewrite !FMap.in_dom_set.
pose v3' := C.v{2}.[i4] ^^ v3.
cut -> : R.t{2}.[i4] ^^ C.v{2}.[i4] ^^ v3 = R.t{2}.[i4] ^^ v3' by smt.
cut -> : v3 = C.v{2}.[i4] ^^ v3' by smt.

case ((i4, v3') = (G.g{2}, C.v{2}.[G.g{2}] ^^
  oget C.gg{2}.[(G.g{2}, C.v{2}.[G.a{2}], C.v{2}.[G.b{2}])]));progress.
smt.

generalize H10;rewrite -neqF=> H10.
case (((i4, v3') = (G.a, alpha)){2});progress.
generalize H6;rewrite H10 /= => H6.
smt.
rewrite -H8;progress.
smt.
generalize H10;smt.
generalize H9;smt.
generalize H10.
generalize H9.
cut -> : forall a b c,  a ^^ b ^^ (b ^^ c) = a ^^ c by smt.
cut lem : forall a b c,  (a ^^ b = a ^^ c) = (b = c) by smt.
case (i4 = G.g{2}).
progress;smt.
case (i4 = G.a{2}).
progress.
rewrite lem.
smt.
smt.

(* END R.xx *)
*)