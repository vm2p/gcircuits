require import Array.
require import Pair.
require import Int.
require import Real.
require import Option.

require Enc.
require EncSec.

theory VC.

 type randg_t.
 type randp_t.

 type fun_t.
 type inp_t.
 type out_t.

 type pkey_t.
 type skey_t.
 type xpub_t.
 type xprv_t.
 type proof_t.

 op eval : fun_t -> inp_t -> out_t.

 op kGen : fun_t -> randg_t -> pkey_t * skey_t.
 op pGen : inp_t -> skey_t -> randp_t -> xpub_t * xprv_t.
 op comp : pkey_t -> xpub_t -> proof_t.
 op vrfy : skey_t -> proof_t -> xprv_t -> out_t option.

 op valid_i : fun_t ->inp_t -> bool.
 pred valid_r : (fun_t, randg_t, randp_t).

end VC.

