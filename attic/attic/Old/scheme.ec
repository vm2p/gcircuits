require import Real.
require import Pair.
require import Bool.
require import Array.
require import Bitstring.
require import Int.

theory Scheme.
  type random.

  type funct.
  type functLen.
  type input = bitstring.
  type output = bitstring.

  op eval : funct -> input -> output.

  type inputG = bitstring Array.array.
  type outputG = bitstring Array.array.
  type functG.
  type keyInput = bitstring Array.array.
  type keyOutput = bitstring Array.array.  

  op funLen : funct -> functLen.
  op keyInputPattern : funct -> bitstring Array.array.

  op functCorrect : funct -> bool.
  op inputCorrect : funct -> input -> bool.

  op garble : random -> funct -> functG*keyInput*keyOutput.
  op encode : keyInput -> input -> inputG.
  op decode : keyOutput -> outputG -> output.
  op evalG : functG -> inputG -> outputG.

  axiom inverse :
    forall (f : funct) , functCorrect f =>
    forall (x : input) , inputCorrect f x =>
    forall (r : random),
      let (g, ki, ko) = garble r f in
      let xG = encode ki x in
           eval f x = decode ko (evalG g xG) /\
           (forall (xGp : inputG),
              (Array.length xGp = length x /\ 
                 (forall(i : int), (0 <= i /\ i < Bits.length x) => 
                    xGp.[i] = xG.[i])) => evalG g xG = evalG g xGp).    
    
  axiom projective : 
    forall (f : funct) , functCorrect f =>
    forall (x : input) , inputCorrect f x =>
    forall (r : random), let (g, ki, ko) = garble r f in
          (Array.length ki = 2*length x) /\ 
          (map Bits.length (keyInputPattern f) = map Bits.length ki) /\
          let gi = encode ki x in
              (Array.length gi = Bits.length x) /\ 
                 (forall(i : int), (0 <= i /\ i < Bits.length x) => 
                    if x.[i] then gi.[i] = ki.[2*i+1] else gi.[i] = ki.[2*i]).

  (* Needed to make sure OT inputs are invalid when coming from an
     invalid adversarial query. E.g. garble returns perp. *)
  axiom invalidInputs1 : 
    forall (f : funct) , !functCorrect f =>
    forall (x : input) ,forall (r : random), 
      let (g, ki, ko) = garble r f in
          Array.length ki <> 2*length x.

  (* Needed to make sure OT inputs are invalid when coming from an
     invalid adversarial query. This means x is only wrong due to 
     length. *)
  axiom invalidInputs2 : 
    forall (f : funct) , functCorrect f =>
    forall (r : random), 
      let (g, ki, ko) = garble r f in
      forall (x : input) , !inputCorrect f x =>
          Array.length ki <> 2*length x.

  type tPhi.

  op phi : funct -> tPhi.
  op phiG : functG -> tPhi.

  module type Rand_t = { fun gen(f_len : functLen) : random }.

  (* IND Model *) 
  type ind_query = (funct*input)*(funct*input).
  type ind_answer = functG*inputG*keyOutput.
  op ind_queryValid : ind_query -> bool.

  module type IND_GARBLE = {
    fun garb(query:ind_query) : ind_answer
    fun get_challenge() : bool
  }.

  module type IND_Adv = {
    fun gen_query() : ind_query
    fun get_challenge(answer:ind_answer) : bool
  }.

  module PrvInd(Rand:Rand_t) : IND_GARBLE = {
    var b : bool
  
    fun garb(query:ind_query) : ind_answer = {
      var f, f0, f1 : funct;
      var x, x0, x1 : input;
      var e : keyInput;
      var d : keyOutput;
      var g : functG;
      var y : inputG;
      var r : random;

      (f0, x0) = Pair.fst query;
      (f1, x1) = Pair.snd query;
      b = $Dbool.dbool;
      if (b) { x = x1;f = f1; } else {x=x0;f=f0;}
      r = Rand.gen(funLen f);
      (g, e, d) = garble r f;
      y = encode e x;
      return (g, y, d);
    }

    fun get_challenge() : bool = {
      return b;
    }
  }.

  module IND_Game(Garble:IND_GARBLE, ADV:IND_Adv) = {
    fun main() : bool = {
      var query : ind_query;
      var answer : ind_answer;
      var adv, real : bool;
    
      query = ADV.gen_query();
      if (ind_queryValid query)
      {
        answer = Garble.garb(query);
        real = Garble.get_challenge();
        adv = ADV.get_challenge(answer);
      }
      else
        real = $Dbool.dbool;
      return (adv = real);
    }
  }.

  (* SIM Model *) 
  type sim_query = (funct*input).
  type sim_answer = functG*inputG*keyOutput.
  op sim_queryValid(query : sim_query) : bool = 
     let (i1,i2) = query in
         Scheme.functCorrect i1 && Scheme.inputCorrect i1 i2.

  module type SIM_GARBLE = {
    fun garb(query:sim_query) : sim_answer
    fun get_challenge() : bool
  }.

  module type SIM_Adv = {
    fun gen_query() : sim_query
    fun get_challenge(answer:sim_answer) : bool
  }.

  module type SIM_Sim = {
    fun simulate(o : output, leak : tPhi) : sim_answer
  }.

  module PrvSim(Rand:Rand_t, SIM : SIM_Sim) : SIM_GARBLE = {
    var b : bool
  
    fun garb(query:sim_query) : sim_answer = {
      var f : funct;
      var x : input;
      var e : keyInput;
      var d : keyOutput;
      var xG : inputG;
      var g : functG;
      var y : output;
      var r : random;
      var answer : sim_answer;

      f = Pair.fst query;
      x= Pair.snd query;
      b = $Dbool.dbool;
      r = Rand.gen(funLen f);
      (g, e, d) = garble r f;
      xG = encode e x;
      if (b) { answer = (g, xG, d); } 
      else {  y = eval f x; answer = SIM.simulate(y, phi(f)); }
      return answer; 
    }

    fun get_challenge() : bool = {
      return b;
    }
  }.

  module SIM_Game(Garble:SIM_GARBLE, ADV:SIM_Adv) = {
    var adv, real : bool

    fun main() : bool = {
      var query : sim_query;
      var answer : sim_answer;
    
      query = ADV.gen_query();
      if (sim_queryValid query)
      {
        answer = Garble.garb(query);
        real = Garble.get_challenge();
        adv = ADV.get_challenge(answer);
      }
      else
        real = $Dbool.dbool;
      return (adv = real);
    }
  }.

  (* Our security assumption, can be proven down to IND *)

  axiom Sim_Security :
    exists (epsilon:real), exists (RandGen <: Rand_t), exists (Sim <: SIM_Sim),
      forall (A<:SIM_Adv), forall &m, epsilon > 0%r /\
        `|2%r * Pr[SIM_Game(PrvSim(RandGen,Sim), A).main()@ &m:res] - 1%r| <
          epsilon.

end Scheme.