require import TwoPartyProtocol.
require import Scheme.
require import Array.
require import Bitstring.
require import Int.
require import Real.

(* Generic 3 pass OT protocol for arbitrary length bit strings 
 * Party 2 holds labels and Party 1 holds selection string 
 * Party 2 initiates the protocol *)

clone Protocol as OT with
   type PInput1 = Scheme.input,
   type PInLen1 = int,
   type POutput1 = Scheme.inputG,
   type PLeak1 = int Array.array,
   type PInput2 = Scheme.keyInput,
   type PInLen2 = int Array.array,
   type POutput2 = unit,
   type PLeak2 = int,
   type PTrace = PMsg * PMsg * PMsg,

   op lenI1(i1 : PInput1) = length i1,
   op lenI2(i2 : PInput2) = map Bits.length i2,

   op validInputs(i1 : PInput1, i2 : PInput2) = 
      2*Bits.length i1 = Array.length i2,

   op phi1(X : PInput2) = 
      (map Bits.length X),

   op phi2(x : PInput1) = 
      Bits.length x,

   op viewP(i1 : PInput1  * PRandom1, i2 : PInput2  * PRandom2) =
     let st10 = initP1 i1 in
     let (msg0,st20) = initP2 i2 in
     let (msg1,st11) = transP1 msg0 st10 in
     let (msg2,st21) = transP2 msg1 st20 in
     let o1 = haltP1 msg2 st11 in
     let o2 = haltP2 st21 in
     let trace = (msg0,msg1,msg2) in
     (trace, o1, o2).
     
axiom OTcorrect: 
  forall (x : bitstring, r1 : OT.PRandom1, X : bitstring Array.array, r2 : OT.PRandom2,
       t : OT.PTrace, o1 : OT.POutput1, o2 : OT.POutput2),
           OT.validInputs x X = true =>
              (t,o1,o2) = OT.viewP (x,r1) (X,r2) => 
                  length o1 = Bits.length x /\ 
                     forall(i : int), 0 <= i /\ i < Bits.length x => 
                        if x.[i] then o1.[i] = X.[2*i+1] else o1.[i] = X.[2*i].
 

axiom OT_Security :
    exists (epsilon:real), exists (RandGen1 <: OT.Rand1_t), 
    exists (RandGen2 <: OT.Rand2_t), forall (A1 <: OT.Adv1),
    forall (A2 <: OT.Adv2), exists (Sim <: OT.PSim),
    forall &m, epsilon > 0%r /\
        `|2%r * Pr[OT.Game1(Sim,A1,RandGen1,RandGen2).main()@ &m:res] - 1%r| <
          epsilon /\
        `|2%r * Pr[OT.Game2(Sim,A2,RandGen1,RandGen2).main()@ &m:res] - 1%r| <
          epsilon.      
