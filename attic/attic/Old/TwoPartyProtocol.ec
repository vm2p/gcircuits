require import Bool.
require import Pair.

theory Protocol.
  type PRandom1.
  type PInput1.
  type PInLen1.
  type PState1.
  type POutput1.
  type PLeak1.
  type PRandom2.
  type PInput2.
  type PInLen2.
  type PState2.
  type POutput2.
  type PLeak2.
  type PMsg.
  type PTrace.
  type PView1 = PTrace * POutput1.
  type PView2 =  PTrace * POutput2.

  op lenI1 : PInput1 -> PInLen1.
  op lenI2 : PInput2 -> PInLen2.

    (* Party 1 gets the last message in the conversation *)
  op initP1 : PInput1 * PRandom1 -> PState1.
  op transP1 : PMsg -> PState1 -> PMsg * PState1.
  op haltP1 : PMsg -> PState1 -> POutput1.
  
    (* Party 2 initiates the conversation *)
  op initP2 : PInput2 * PRandom2 -> PMsg * PState2.
  op transP2 : PMsg -> PState2 -> PMsg * PState2.
  op haltP2 : PState2 -> POutput2.

  op validInputs : PInput1 -> PInput2 -> bool.
  
  op viewP : PInput1 * PRandom1 -> PInput2 * PRandom2 -> PTrace * POutput1 * POutput2.

  op viewP1 (i1 : PInput1 * PRandom1, i2 : PInput2 * PRandom2) : PTrace * POutput1 = 
     let (t,o1,o2) = (viewP i1 i2) in (t,o1).

  op viewP2 (i1 : PInput1 * PRandom1, i2 : PInput2 * PRandom2) : PTrace * POutput2 = 
     let (t,o1,o2) = (viewP i1 i2) in (t,o2).

  op phi1 : PInput2 -> PLeak1.

  op phi2 : PInput1 -> PLeak2.

  module type Rand1_t = { fun gen(len_i1 : PInLen1) : PRandom1 }.
  module type Rand2_t = { fun gen(len_i2 : PInLen2) : PRandom2 }.

  module type PSim = {
     fun sim1(I1 : PInput1, o1: POutput1, l1 : PLeak1) : PView1
     fun sim2(I2 : PInput2, o2: POutput2, l2 : PLeak2) : PView2
  }.

  module type Adv1 = { 
     fun gen_query() : PInput1 * PInput2
     fun dist(view : PView1) : bool 
  }.

  module type Adv2 = { 
     fun gen_query() : PInput1 * PInput2
     fun dist(view : PView2) : bool 
  }.

  module Game1(SIM : PSim, ADV : Adv1, Rand1 : Rand1_t, Rand2 : Rand2_t) = {
     var real : bool
     var adv : bool

     fun main() : bool = {
       var view : PView1;
       var t : PTrace;
       var o1 : POutput1;
       var l1 : PLeak1;
       var r1 : PRandom1;
       var r2 : PRandom2;
       var i1 : PInput1;
       var i2 : PInput2;
      
       (i1,i2) = ADV.gen_query();

       if (!validInputs i1 i2) {
          adv = $Dbool.dbool;
       }
       else {
         r1 = Rand1.gen(lenI1 i1);
         r2 = Rand2.gen(lenI2 i2); 
         view = viewP1 (i1,r1) (i2,r2); 

         real = $Dbool.dbool;
      
         if (!real) {
           (t,o1) = view;
           l1 = phi1(i2);
           view = SIM.sim1(i1,o1,l1);
         }
         
         adv = ADV.dist(view);
       }
       
       return (adv = real);
     }
   }.

   module Game2(SIM : PSim, ADV : Adv2, Rand1 : Rand1_t, Rand2 : Rand2_t) = {
     var real : bool
     var adv : bool

     fun main() : bool = {
       var view : PView2;
       var t : PTrace;
       var o2 : POutput2;
       var l2 : PLeak2;
       var r1 : PRandom1;
       var r2 : PRandom2;
       var i1 : PInput1;
       var i2 : PInput2;
      
       (i1,i2) = ADV.gen_query();      

       if (!validInputs i1 i2) {
          adv = $Dbool.dbool;
       }
       else {
         r1 = Rand1.gen(lenI1 i1);
         r2 = Rand2.gen(lenI2 i2);

         view = viewP2 (i1,r1) (i2,r2);

         real = $Dbool.dbool;
      
         if (!real) {
           (t,o2) = view;
           l2 = phi2(i1);
           view = SIM.sim2(i2,o2,l2);
         }

         adv = ADV.dist(view);
       }

       return (adv = real);
     }
   }.
end Protocol.