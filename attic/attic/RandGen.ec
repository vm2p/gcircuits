require import Distr.
require import Pair.

theory RandGen.
 type t.
 type rg_arg_t.

 op drand : rg_arg_t -> t distr.
 axiom drand_distr : forall x, weight (drand x) = 1%r.
end RandGen.


theory RandGenProd.
 clone RandGen as R1.
 clone RandGen as R2.
 op drand (rg_arg: R1.rg_arg_t*R2.rg_arg_t) : (R1.t*R2.t) distr =
  Dprod.dprod (R1.drand (fst rg_arg)) (R2.drand (snd rg_arg)).
 lemma drand_distr : forall x, weight (drand x) = 1%r.
 proof. by smt. save.
end RandGenProd.


