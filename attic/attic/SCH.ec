require import Bitstring.
require import Bool.
require import Array.
require import ArrayExt.
require import Int.
require import Real.
require import Pair.

require import RandGen.

type input_t = bitstring.
type ciphertext_t = bitstring.


(* A theory for Projective Garbling Schemes *)
theory SCHdefs.

 (* function description, inputs, outputs, and leakage *) 
 type fun_t.

 op inputLen : int.
 op validInputs : fun_t -> input_t -> bool.
 axiom validInputsLen : forall f i,
  validInputs f i => Bits.length i = inputLen.

 type output_t.

 type leak_t.
 op phi : fun_t -> leak_t.

 op eval : fun_t -> input_t -> output_t.

 type funG_t.
 type rand_t.
 op funG : fun_t -> rand_t -> funG_t.

(*
 type inputG_t = ciphertext_t array.
 type inputK_t = (ciphertext_t*ciphertext_t) array.
*)
 op inputK : fun_t -> rand_t -> (bitstring*bitstring) array (*inputK_t*).

 axiom inputKLen : forall r f,
  Array.length (inputK f r) = inputLen.

 op ciphertextLen : int.
 axiom inputKLenC : forall r f k,
  0 <= k < inputLen =>
  Bits.length (fst (inputK f r).[k]) = ciphertextLen
  && Bits.length (snd (inputK f r).[k]) = ciphertextLen.

 op encode : (bitstring*bitstring) array (*inputK_t*) -> bitstring (*input_t*) -> bitstring array (*inputG_t*).
 axiom encodeLen : forall ki i,
  Array.length (encode ki i) = inputLen.
 axiom encodeLen2 : forall ki i k,
  0 <= k < inputLen =>
  Bits.length (encode ki i).[k] = ciphertextLen.

 type outputK_t.
 op outputK : fun_t -> rand_t -> outputK_t.
 type outputG_t.
 op decode : outputK_t -> outputG_t -> output_t.

 op evalG : funG_t -> bitstring array (*inputG_t*) -> outputG_t.

 axiom inverse : forall f r i,
  eval f i = decode (outputK f r)
                    (evalG (funG f r)
                           (encode (inputK f r) i)).

 axiom projective : forall f r iK i iG,
   iK = inputK f r =>
   iG = encode iK i =>
   forall k, 0 <= k < inputLen =>
    if i.[k] then iG.[k] = fst iK.[k] else iG.[k] = snd iK.[k].

end SCHdefs.

theory SCHprot.
 clone import SCHdefs as Defs.

 (* Random generator *)
 clone RandGen as GR with type t = rand_t, type rg_arg_t = leak_t.

 (* useful type alias and results on Params *)
 type inputG_t = ciphertext_t array.
 type inputK_t = (ciphertext_t*ciphertext_t) array.

lemma encode_def: forall f r (i:input_t),
   forall k, 0 <= k < inputLen =>
    (encode (inputK f r) i).[k]
    = if i.[k]
      then  fst (inputK f r).[k]
      else  snd (inputK f r).[k].
proof.
intros f r i k Hk.
cut H : if i.[k]
        then (encode (inputK f r) i).[k]
             = fst (inputK f r).[k]
        else (encode (inputK f r) i).[k]
             = snd (inputK f r).[k].
 by apply (projective f r (inputK f r) i
                      (encode (inputK f r) i)).
by generalize H; case i.[k]; intros Hik.
save.

lemma encode_fillk: forall r f i,
 encode (inputK f r) i
 = fillk (lambda k, if i.[k]
                    then fst (inputK f r).[k]
                    else snd (inputK f r).[k]) inputLen.
intros r f i.
apply Array.extensionality; delta Array.(==); simplify.
split; first by rewrite encodeLen fillk_len.
rewrite encodeLen; intros k Hk1 Hk2.
rewrite fillk_forall; first by trivial.
by rewrite encode_def.
save.

  (* SIM Model *) 
  (* obs: changed to turn it more consistent with TPP *)
  type sim_query_t = (fun_t*input_t).
  type sim_answer_t = funG_t*inputG_t*outputK_t.
(*
  type sim_view_t = rand_t * sim_answer_t.
*)
  module type SAdv_t = {
    fun gen_query() : sim_query_t
    fun dist(ans: sim_answer_t) : bool
  }.

  module type SSim_t = {
    fun sim(i: input_t, o : output_t, leak : leak_t) : sim_answer_t
  }.


  (* Our security assumption, can be proven down to IND *)
  module SGame((*R: Rand_t,*) S: SSim_t, A: SAdv_t) = {
    var adv, real : bool

    fun main() : bool = {
      var r : rand_t;
      var f : fun_t;
      var i : input_t;
      var o : output_t;
      var ans : sim_answer_t;

      (f, i) = A.gen_query();
      real = $Dbool.dbool;

      if (!validInputs f i)
        adv = $Dbool.dbool;
      else {
        o = eval f i;
        if (real) {
         r = $GR.drand (phi f)(*R.gen(phi f)*);
         ans = (funG f r, encode (inputK f r) i, outputK f r);
        } else {
         ans = S.sim(i, o, phi f);
        }

        adv = A.dist(ans);
      }
      return (adv = real);
    }
  }.

(* useful lemmas about success probabilities *)
lemma SGame_lossless : forall (A<:SAdv_t) (S<:SSim_t),
 islossless S.sim =>
 islossless A.gen_query => islossless A.dist =>
 islossless SGame(S, A).main.
proof.
intros A S S_ll A_gen_ll A_dist_ll.
fun.
seq 2 : true; trivial.
 rnd; first by apply Dbool.lossless. 
 by call (_: true ==> true); trivial.
case (validInputs f i).
 rcondf 1; trivial.
 case (SGame.real).
  rcondt 2; wp; trivial. 
  call (_:true ==> true); first by trivial.
  wp; rnd; progress; trivial.
   by apply (GR.drand_distr (phi f{hr})).
  by wp; skip; trivial. 
 rcondf 2; first by wp; skip; trivial.
 call (_:true ==> true); first by trivial.
 call (_:true ==> true); first by trivial.
 by wp; skip; trivial. 
rcondt 1; trivial.
rnd; progress; trivial.
by apply Dbool.lossless. 
save.

lemma SGame_true_pr :
 forall (A<:SAdv_t) (S<:SSim_t) &m,
 islossless S.sim =>
 islossless A.gen_query => islossless A.dist =>
 Pr [SGame(S, A).main() @ &m : true] = 1%r.
proof.
intros A S &m S_ll A_gen_ll A_dist_ll.
bdhoare_deno (_: true ==> true); trivial.
by apply (SGame_lossless (A) (S)).
save.

lemma SGame_real_pr :
 forall (A<:SAdv_t) (S<:SSim_t) &m,
 islossless S.sim =>
 islossless A.gen_query => islossless A.dist =>
 Pr [SGame(S, A).main() @ &m : SGame.real] = (1%r/2%r).
proof.
intros A S &m S_ll A_gen_ll A_dist_ll.
bdhoare_deno (_: true ==> SGame.real); trivial.
fun.
seq 1 : true 1%r (1%r/2%r) 0%r 0%r; trivial. 
 by call (_: true ==> true); trivial.
seq 1 : SGame.real (1%r/2%r) 1%r (1%r/2%r) 0%r; trivial.
  rnd; skip; progress; trivial. 
   by rewrite Dbool.mu_def //=.
 case (validInputs f i); last first.
  rcondt 1; first by trivial.
   rnd; first by apply Dbool.lossless.
   by skip; trivial.
 rcondf 1; first by skip; trivial.
 call (_: true ==> true); trivial.
 rcondt 2; trivial.
  wp; skip; trivial.
 wp; rnd; progress; trivial.
 by apply (GR.drand_distr (phi f{hr})).
 by wp; skip; trivial.
hoare.
case (validInputs f i); last first.
 rcondt 1; first by trivial.
 by rnd; skip; trivial.
rcondf 1; first by skip; trivial.
call (_: true ==> true); trivial.
rcondf 2; first by wp; skip; trivial.
call (_: true ==> true); trivial.
by wp; skip; trivial.
save.

lemma SGame_Nreal_pr :
 forall (A<:SAdv_t) (S<:SSim_t) &m,
 islossless S.sim =>
 islossless A.gen_query => islossless A.dist =>
 Pr [SGame(S, A).main() @ &m : !SGame.real] = (1%r/2%r).
proof.
intros A S &m S_ll A_gen_ll A_dist_ll.
rewrite Pr mu_not.
rewrite (SGame_true_pr (A) (S) &m) //.
rewrite (SGame_real_pr (A) (S) &m) //.
cut H: 1%r = 2%r / 2%r by trivial.
by rewrite {1}H.
save.

lemma ADV_xpand' : forall (A <: SAdv_t) (S <: SSim_t) &m,
 islossless S.sim =>
 islossless A.gen_query =>
 islossless A.dist => 
 2%r * Pr [ SGame(S, A).main() @ &m : res] - 1%r
  = 2%r * ( Pr [ SGame(S, A).main() @ &m : res && SGame.real]
        - Pr [ SGame(S, A).main() @ &m : !res && !SGame.real] ).
proof.
intros A S &m S_ll A_gen_ll A_distr_ll.
cut Hpr1: ( Pr [ SGame(S, A).main() @ &m : res]
           = Pr [ SGame(S, A).main() @ &m : res && SGame.real]
             + Pr [ SGame(S, A).main() @ &m : res && !SGame.real]).
 cut ->: (Pr[SGame(S, A).main() @ &m : res]
          = Pr[SGame(S, A).main() @ &m
               : res && SGame.real \/ res && !SGame.real]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_disjoint; smt.
cut Hpr2 : ( Pr [ SGame(S, A).main() @ &m : res && !SGame.real]
           = Pr [ SGame(S, A).main() @ &m : !SGame.real]
            - Pr [ SGame(S, A).main() @ &m : !res && !SGame.real]).
 cut ->: ( Pr[SGame(S, A).main() @ &m : !SGame.real]
          = Pr[SGame(S, A).main() @ &m 
               : res && !SGame.real \/ !res && !SGame.real]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_disjoint; smt.
rewrite Hpr1 Hpr2 (SGame_Nreal_pr A S &m) //.
smt.
save.

lemma ADV_xpand : forall (A <: SAdv_t) (S <: SSim_t) &m,
 islossless S.sim =>
 islossless A.gen_query =>
 islossless A.dist => 
 2%r * Pr [ SGame(S, A).main() @ &m : res] - 1%r
 = 2%r * ( Pr [ SGame(S, A).main() @ &m : ! SGame.real => res]
           - Pr [ SGame(S, A).main() @ &m : SGame.real => !res] ).
proof.
intros A S &m S_ll A_gen_ll A_dist_ll.
cut ->: ( Pr [ SGame(S, A).main() @ &m : !SGame.real => res]
          = Pr [ SGame(S, A).main() @ &m : SGame.real]
            + Pr [ SGame(S, A).main() @ &m : res]
            - Pr [ SGame(S, A).main() @ &m : res && SGame.real]).
 cut ->: (Pr[SGame(S, A).main() @ &m : !SGame.real => res]
          = Pr[SGame(S, A).main() @ &m : res \/ SGame.real]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; smt.
cut ->:  Pr [ SGame(S, A).main() @ &m : SGame.real => !res]
  = Pr [ SGame(S, A).main() @ &m : !SGame.real] + 1%r
    - Pr [ SGame(S, A).main() @ &m : res]
    - Pr [ SGame(S, A).main() @ &m : !res && !SGame.real].
 cut ->: (Pr[SGame(S, A).main() @ &m : SGame.real => !res]
         = Pr[SGame(S, A).main() @ &m : !res \/ !SGame.real]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; rewrite Pr mu_not.
 rewrite (SGame_true_pr A S &m) //; smt.
rewrite (SGame_Nreal_pr A S &m) // (SGame_real_pr A S &m) //.
cut H1: forall (x y:real), x - y = x + [-] y by smt.
cut H2: forall (x y:real), [-] (x + y) = [-] x + [-] y by smt.
cut H3: forall (x:real), [-] ([-] x) = x by smt.
cut H4: forall (x:real), 2%r * x = x + x.
 intros x; cut ->:2%r = (1%r + 1%r) by smt.
 by rewrite Mul_distr_r.
cut H4': forall (x y:real), 2%r * (x-y) = 2%r * x - 2%r * y by smt.
cut H5: forall (x:real), 2%r * -x = - 2%r * x by smt.
rewrite ?H1 ?H2 ?H3.
cut ->: (1%r / 2%r + Pr[SGame(S, A).main() @ &m : res] +
 -Pr[SGame(S, A).main() @ &m : res && SGame.real] +
 (-(1%r / 2%r) + -1%r + Pr[SGame(S, A).main() @ &m : res] +
  Pr[SGame(S, A).main() @ &m : ! res && ! SGame.real]))
 = ( (Pr[SGame(S, A).main() @ &m : res] + Pr[SGame(S, A).main() @ &m : res]) - 1%r) - (Pr[SGame(S, A).main() @ &m : res && SGame.real] -
      Pr[SGame(S, A).main() @ &m : ! res && ! SGame.real]) by smt.
rewrite -H4 H4'  -(ADV_xpand' A S &m) //.
smt.
save.

end SCHprot.

theory SCHsec.
 clone SCHdefs.
 clone import SCHprot as SCH with theory Defs = SCHdefs.

(* passar para aqui as propriedades de correcção??? *)

axiom Security :
 exists (epsilon:real) (*(R <: Rand_t)*) (S <: SSim_t),
 islossless S.sim /\
 forall (A<:SAdv_t (*{S}*)) &m,
 islossless A.gen_query => islossless A.dist =>
   `|2%r * Pr[SGame((*R,*)S,A).main()@ &m:res] - 1%r| <= epsilon.

end SCHsec.
