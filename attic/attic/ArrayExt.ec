(* extensions to the Array theory *)
require import Array.
require import Int.


op allk: (int -> 'x -> bool) -> 'x array -> bool.

axiom allk_def: forall (f:int -> 'x -> bool, xs:'x array),
  allk f xs <=> forall k, (0 <= k < length xs => f k xs.[k]).

lemma allk_intro: forall (f:int -> 'x -> bool, xs:'x array),
  (forall k, 0 <= k < length xs => f k xs.[k]) => allk f xs.
proof. by intros f xs H; rewrite (allk_def f xs). save.

lemma allk_forall: forall (f:int -> 'x -> bool, xs:'x array, k),
  allk f xs => (0 <= k < length xs => f k xs.[k]).
proof. by intros f xs k; rewrite allk_def => H Hk; apply H. save.

op fillk: (int -> 'x) -> int -> 'x array.

axiom fillk_len: forall (f:int -> 'x, len:int),
 length (fillk f len) = len.

axiom fillk_forall: forall (f:int -> 'x, len:int, k:int),
  0 <= k < len => (fillk f len).[k] = f k.

lemma fillk_allk : forall (f:int -> 'x) (xs: 'x array),
 allk (lambda k x, x = f k) xs => xs = fillk f (length xs).
proof.
intros f xs.
rewrite allk_def => H.
apply extensionality; delta (==); beta; split; first by rewrite fillk_len.
intros k Hk1 Hk2.
rewrite fillk_forall; first by trivial.
change ((lambda k x, x=f k) k xs.[k]).
by apply H; trivial.
save.

