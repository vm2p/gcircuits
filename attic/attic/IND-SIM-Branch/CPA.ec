require import Real.
require import Pair.
require import Bool.

(** begin def *)
theory CPA_Def.

  (* Abstract definitions common to all IND and SIM secure schemes *)
  theory CPA_Scheme.
    type plain.
    type cipher.
    type leakage.

    op enc : plain -> cipher distr.
    op valid_plain : plain -> bool.
    op leak : plain -> leakage.
    op pi_sampler : leakage -> plain.

  end CPA_Scheme.
  import CPA_Scheme.

  (* Definitions needed for IND model *)
  type query_IND = plain * plain.
  op queryValid_IND(query : query_IND) =
     valid_plain (fst query) /\ 
     valid_plain (snd query) /\
     leak (fst query) = leak (snd query).

  module type Adv_IND_t = {
    fun gen_query() : query_IND
    fun get_challenge(cipher:cipher) : bool
  }.

  module Game_IND(ADV:Adv_IND_t) = {
    fun main() : bool = {
      var query : query_IND;
      var p : plain;
      var c : cipher;
      var b, adv, ret : bool;
    
      query = ADV.gen_query();
      if (queryValid_IND query)
      {
        b = $Dbool.dbool;
        if (b) p = fst query;
        else   p = snd query;
        c = $enc p;
        adv = ADV.get_challenge(c);
        ret = (b = adv);
      }
      else
        ret = $Dbool.dbool;
      return ret;
    }
  }.

  (* Definitions needed for SIM model *)
  type query_SIM = plain.
  op queryValid_SIM(query : query_SIM) =
     valid_plain query.

  pred pi_sampler_works(plain : plain) =
       let leakage = leak plain in
       let pi = pi_sampler leakage in
           leak pi = leakage /\ valid_plain pi.

  module type Adv_SIM_t = {
    fun gen_query() : query_SIM
    fun get_challenge(cipher: cipher) : bool
  }.

  module type Sim_SIM_t = {
    fun sim(leakage : leakage) : cipher
  }.

  module Game_SIM(ADV:Adv_SIM_t, SIM:Sim_SIM_t) = {
    fun main() : bool = {
      var query : query_SIM;
      var c : cipher;
      var b, adv, ret : bool;
    
      query = ADV.gen_query();
      if (queryValid_SIM query)
      {
        b = $Dbool.dbool;
        if (b) c = $enc query;
        else   c = SIM.sim(leak query);
        adv = ADV.get_challenge(c);
        ret = (b = adv);
      }
      else
        ret = $Dbool.dbool;
      return ret;
    }
  }.

 (* Algorithms used in the IND => SIM proof *)
 module SIM : Sim_SIM_t = {
   fun sim(leakage : leakage) : cipher = {
       var pi : plain;
       var c : cipher;
       pi = pi_sampler leakage;
       c = $enc pi;
       return c;
   }
 }.

 module INDADV(ADV:Adv_SIM_t) = {
   fun gen_query() : query_IND = {
       var plain0 : plain;
       var plain1 : plain;
       var leakage0 : leakage;

       plain0 = ADV.gen_query();
       leakage0 = leak plain0;
       plain1 = pi_sampler leakage0;
       return (plain0,plain1);
   }

   fun get_challenge(cipher : cipher) : bool = {
       var answer : bool;

       answer = ADV.get_challenge(cipher);
       return answer;
   }
 }.

(* Losslessness properties *)

lemma INDADV_gen_query_lossless:
 forall (ADV<:Adv_SIM_t),
 islossless ADV.gen_query => islossless INDADV(ADV).gen_query.
proof.
intros ADV H; fun; wp; call (_: true ==> true); last by trivial.
by apply H.
save.

lemma INDADV_get_challenge_lossless:
 forall (ADV<:Adv_SIM_t),
 islossless ADV.get_challenge => islossless INDADV(ADV).get_challenge.
proof.
intros ADV H; fun; wp; call (_: true ==> true); last by trivial.
by apply H.
save.

(*
lemma sim_lossless : 
    islossless SIM.sim.
proof.
fun.
rnd.
 admit.
by wp; skip.
save.
*)

(* If scheme is IND, then INDADV must have negl advantage *)
lemma indadv_bounded : 
  forall (epsilon:real), epsilon > 0%r
=>
  (forall (ADV<:Adv_IND_t  { Game_IND }), forall &m,
      islossless ADV.gen_query =>
      islossless ADV.get_challenge =>
        `|Pr[Game_IND(ADV).main()@ &m:res] - 1%r / 2%r| < epsilon)
=> 
   (forall (ADV<:Adv_SIM_t  {INDADV, Game_IND, Game_SIM}), forall &m,
      islossless ADV.gen_query =>
      islossless ADV.get_challenge =>
        `|Pr[Game_IND(INDADV(ADV)).main()@ &m:res] - 1%r / 2%r| < epsilon).
proof.
intros epsilon epsilon_pos ind_secure ADV &m ADV_gen_query_lossless 
       ADV_get_challenge_lossless.
cut gen_query_lossless : (islossless INDADV(ADV).gen_query).
fun.
wp.
bd_eq.
call (_ : true ==> true).
apply ADV_gen_query_lossless.
skip;trivial.
cut get_challenge_lossless : (islossless INDADV(ADV).get_challenge).
fun.
bd_eq.
call (_ : true ==> true).
apply ADV_get_challenge_lossless.
skip;trivial.
apply (ind_secure (INDADV(ADV)) &m).
apply gen_query_lossless.
apply get_challenge_lossless.
save.

(* The INDADV runs the SIMADV in exactly the SIM model environment *)
lemma indadv_equiv : 
  (* this assumption should be probabilistic *)
  (forall (plain : plain), valid_plain plain => pi_sampler_works plain) 
=>
  (forall (ADV<:Adv_SIM_t {INDADV, Game_IND, Game_SIM} ),
      islossless ADV.gen_query =>
      islossless ADV.get_challenge =>
      equiv [ Game_IND(INDADV(ADV)).main ~ Game_SIM(ADV,SIM).main :  ={glob ADV} ==> ={res} ]).
proof.
intros pi_samplable ADV ADV_gen_query_lossless ADV_get_challenge_lossless.
fun.
inline INDADV(ADV).gen_query.
seq 4 1 : ( ={glob ADV} /\ plain0{1} = query{2} /\ 
           leakage0{1} = leak plain0{1} /\ plain1{1} = pi_sampler leakage0{1} /\ 
           query{1} = (plain0{1}, plain1{1}) ).
wp.
call (_ : ={glob ADV} ==> ={res} /\ ={glob ADV}).
fun true;by trivial.
skip;by trivial.
case (queryValid_SIM query{2}).
rcondt {1} 1.
intros &m; skip;smt.
rcondt {2} 1.
intros &m;skip;by trivial.
seq 1 1 :  (={glob ADV} /\ plain0{1} = query{2} /\ leakage0{1} = leak plain0{1} /\ 
           plain1{1} = pi_sampler leakage0{1} /\ query{1} = (plain0{1}, plain1{1}) /\ ={b}). 
rnd; skip; by trivial.
case b{1}.
rcondt {1} 1.
intros &m.
skip; by trivial.
rcondt {2} 1.
intros &m.
skip; by trivial.
seq 1 0 :  (={glob ADV} /\ ={b} /\ p{1} = query{2}).
wp; by trivial.
wp.
inline INDADV(ADV).get_challenge.
seq 2 1 : (={glob ADV} /\ ={b} /\ cipher{1} = c{2}).
wp;rnd;skip;by trivial.
wp.
call (_ : ={glob ADV} /\ ={cipher} ==> ={res}).
fun true;by trivial.
skip;by trivial.
rcondf {1} 1.
intros &m; by trivial.
rcondf {2} 1.
intros &m; skip; smt.
inline SIM.sim.
wp.
seq 1 2 : (={glob ADV} /\ ={b} /\ p{1} = pi{2}).
wp; skip; smt.
inline INDADV(ADV).get_challenge.
seq 2 2 : (={glob ADV} /\ ={b} /\ cipher{1} = c{2}).
wp;rnd;skip;by trivial.
wp.
call (_ : ={glob ADV} /\ ={cipher} ==> ={res}).
fun true;by trivial.
skip;by trivial.
rcondf {1} 1.
intros &m; skip;smt.
rcondf {2} 1.
intros &m; skip; by trivial.
rnd;wp;skip;by trivial.
save.

lemma simadv_indadv : 
  forall (epsilon:real), epsilon > 0%r
=>
  (* this should be probabilistic *)
  (forall (plain : plain), valid_plain plain => pi_sampler_works plain) 
=>
  (forall (ADV<:Adv_SIM_t {INDADV, Game_IND, Game_SIM}), forall &m,
      islossless ADV.gen_query =>
      islossless ADV.get_challenge =>
       Pr[Game_IND(INDADV(ADV)).main()@ &m:res] = Pr[Game_SIM(ADV,SIM).main()@ &m:res]).
proof.
intros epsilon epsilon_pos pi_samplable ADV &m gen_query_lossless_ADV 
       get_challenge_lossless_ADV.
cut indadv_equiv' : (
(forall (ADV<:Adv_SIM_t  {INDADV, Game_IND, Game_SIM}),
      islossless ADV.gen_query =>
      islossless ADV.get_challenge =>
      equiv [ Game_IND(INDADV(ADV)).main ~ Game_SIM(ADV,SIM).main : ={glob ADV} ==> ={res} ])
).
apply (indadv_equiv).
trivial.
cut indadv_equiv'' : (
  (equiv [ Game_IND(INDADV(ADV)).main ~ Game_SIM(ADV,SIM).main : ={glob ADV} ==> ={res} ])
).
apply (indadv_equiv' ADV).
apply gen_query_lossless_ADV.
apply get_challenge_lossless_ADV.
equiv_deno (indadv_equiv'').
trivial.
trivial.
save.

lemma ind_implies_sim : 
  forall (epsilon:real), epsilon > 0%r
=>
  (* this should be probabilistic *)
  (forall (plain : plain), valid_plain plain => pi_sampler_works plain) 
=>
  (forall (ADV<:Adv_IND_t { Game_IND }), forall &m,
      islossless ADV.gen_query =>
      islossless ADV.get_challenge =>
        `|Pr[Game_IND(ADV).main()@ &m:res] - 1%r / 2%r| < epsilon)
=> 
  (forall (ADV'<:Adv_SIM_t {INDADV, Game_IND, Game_SIM}), forall &m,
      islossless ADV'.gen_query =>
      islossless ADV'.get_challenge =>
        `|Pr[Game_SIM(ADV',SIM).main()@ &m:res] - 1%r / 2%r| < epsilon).
proof.
intros epsilon epsilon_pos pi_samplable ind_secure ADV' &m
       gen_query_lossless_ADV' get_challenge_lossless_ADV'.
cut <- : Pr[Game_IND(INDADV(ADV')).main()@ &m:res]
         = Pr[Game_SIM(ADV',SIM).main()@ &m:res].
 equiv_deno (_:  ={glob ADV'} ==> ={res} ).
   by apply (indadv_equiv _ (ADV')); trivial.
  smt.
 smt.
apply (ind_secure (INDADV(ADV')) &m).
 by apply (INDADV_gen_query_lossless ADV').
by apply (INDADV_get_challenge_lossless ADV').
save.

end CPA_Def.
(** end def *)