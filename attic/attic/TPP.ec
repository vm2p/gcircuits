require import Bool.
require import Pair.
require import Real.

require import RandGen.

(* Two-Party Protocols *)

theory TPPdefs.

 (* Party 1 types *)
 type rand1_t.
 type input1_t.
 type output1_t.
 type leak1_t.

 (* Party 2 types *)
 type rand2_t.
 type input2_t.
 type output2_t.
 type leak2_t.

 (* execution trace and views *)
 type trace_t.

 (* protocol execution & outcomes *)
 op validInputs : input1_t -> input2_t -> bool.
 op trace : input1_t -> rand1_t -> input2_t -> rand2_t -> trace_t.
 op res1 : input1_t -> rand1_t * trace_t -> output1_t.
 op res2 : input2_t -> rand2_t * trace_t -> output2_t.

 (* Leakage functions --- leakage tell us what info, from secret data,
   is expected to be public... (e.g. lengths) *)
 op phi1 : input1_t -> leak1_t.
 op phi2 : input2_t -> leak2_t.

 (* aqui poder-se-ia usar "pred" para beneficiar do acréscimo de
  poder expressivo na construção das fórmulas lógicas (como a
  utilização de quantificadores). Mas acabei por me inclinar para
  versões operacionais destes predicados. *)
 op correct: input1_t -> rand1_t -> input2_t -> rand2_t -> bool.

end TPPdefs.
 

theory TPPprot.

 clone import TPPdefs as Defs.

 (* Alias for the correctness assertion *) 
 pred Correct (x:unit) = forall i1 r1 i2 r2, 
  validInputs i1 i2 => correct i1 r1 i2 r2.

 (* following Goldreich, we include the random coins in each party view
 (but leave the input implicit in the context...) *)
 type view1_t = (* input1_t * *) rand1_t * trace_t.
 type view2_t = (* input2_t * *) rand2_t * trace_t.

 (* randomness can only depend on public (leaked) info *)
 module type Rand1_t = { fun gen(i1info : leak1_t) : rand1_t }.
 module type Rand2_t = { fun gen(i2info : leak2_t) : rand2_t }.
(*
 (* Ficava mais consistente se fizesse isto (como SCH). MAS NÃO
 DÁ!!! o problema é que as teorias não nos oferecem as capacidades
 composicionais dos módulos (requerido em PFE, quando se constrói
 R2 à custa de OT.R2 e GR)*)
 clone RandGen as R1 with type t = rand1_t, type rg_arg_t = leak1_t.
 clone RandGen as R2 with type t = rand2_t, type rg_arg_t = leak2_t.
*)

 (* Simulator & Adversaries *)
 module type Sim_t = {
  fun sim1(i1: input1_t, o1: output1_t, l2: leak2_t) : view1_t
  fun sim2(i2: input2_t, o2: output2_t, l1: leak1_t) : view2_t
 }.

 module type Adv1_t = { 
  fun gen_query() : input1_t * input2_t
  fun dist(view: view1_t) : bool 
 }.

 module type Adv2_t = { 
  fun gen_query() : input1_t * input2_t
  fun dist(view: view2_t) : bool 
 }.

 module Game1(R1: Rand1_t, R2: Rand2_t, A1: Adv1_t, S: Sim_t) = {
  var real : bool

  fun main() : bool = {
   var adv : bool;
   var view1 : view1_t;
   var o1 : output1_t;
   var r1 : rand1_t;
   var r2 : rand2_t;
   var i1 : input1_t;
   var i2 : input2_t;
    
   (i1,i2) = A1.gen_query();
   real = $Dbool.dbool;

   if (!validInputs i1 i2) {
    adv = $Dbool.dbool;
   } else {
    r1 = R1.gen(phi1 i1);
    r2 = R2.gen(phi2 i2);
    view1 = (r1, trace i1 r1 i2 r2);
    o1 = res1 i1 view1;

    if (!real)
     view1 = S.sim1 (i1, o1, phi2(i2));
         
    adv = A1.dist(view1);
   }
       
   return (adv = real);
  }
 }.

 module Game2(R1: Rand1_t, R2: Rand2_t, A2: Adv2_t, S: Sim_t) = {
  var real : bool

  fun main() : bool = {
   var adv : bool;
   var view2 : view2_t;
   var o2 : output2_t;
   var r1 : rand1_t;
   var r2 : rand2_t;
   var i1 : input1_t;
   var i2 : input2_t;
    
   (i1,i2) = A2.gen_query();
   real = $Dbool.dbool;
      
   if (!validInputs i1 i2) {
    adv = $Dbool.dbool;
   } else {
    r1 = R1.gen(phi1 i1);
    r2 = R2.gen(phi2 i2);
    view2 = (r2, trace i1 r1 i2 r2);
    o2 = res2 i2 view2;

    if (!real)
     view2 = S.sim2 (i2, o2, phi1(i1));
         
    adv = A2.dist(view2);
   }
       
   return (adv = real);
  }
 }.

(* Useful lemmas for reasoning about success probabilities *)

lemma Game1_lossless : forall (R1<:Rand1_t) (R2<:Rand2_t) (A1<:Adv1_t) (S<:Sim_t),
 islossless R1.gen => islossless R2.gen =>
 islossless S.sim1 =>
 islossless A1.gen_query =>
 islossless A1.dist =>
 islossless Game1(R1, R2, A1, S).main.
proof.
intros R1 R2 A1 S R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll.
fun.
seq 2 : true; trivial.
 rnd; progress; trivial; first by apply Dbool.lossless. 
 by call (_: true ==> true); trivial.
case (validInputs i1 i2).
 rcondf 1; first by skip; trivial.
 call (_:true ==> true); first by trivial.
 seq 2 : true; trivial.
 call (_:true ==> true); first by trivial.
 call (_:true ==> true); first by trivial.
 by wp; skip; trivial.
 case (Game1.real).
  by rcondf 3; wp; skip; trivial.
 rcondt 3.
  by wp; skip; trivial.
 call (_:true ==> true); first by trivial.
 by wp; skip; trivial. 
rcondt 1; trivial.
rnd; trivial.
by apply Dbool.lossless.
save.

lemma Game1_true_pr :
 forall (R1<:Rand1_t) (R2<:Rand2_t) (A1<:Adv1_t) (S<:Sim_t) &m,
 islossless R1.gen => islossless R2.gen =>
 islossless S.sim1 =>
 islossless A1.gen_query =>
 islossless A1.dist =>
 Pr [Game1(R1, R2, A1, S).main() @ &m : true] = 1%r.
proof.
intros R1 R2 A1 S &m R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll.
bdhoare_deno (_: true ==> true); trivial.
by apply (Game1_lossless (R1) (R2) (A1) (S)).
save.

lemma Game1_real_pr :
 forall (R1<:Rand1_t) (R2<:Rand2_t) (A1<:Adv1_t) (S<:Sim_t) &m,
 islossless R1.gen => islossless R2.gen =>
 islossless S.sim1 =>
 islossless A1.gen_query =>
 islossless A1.dist =>
 Pr [Game1(R1, R2, A1, S).main() @ &m : Game1.real] = (1%r/2%r).
proof.
intros R1 R2 A1 S &m R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll.
bdhoare_deno (_: true ==> Game1.real); trivial.
fun.
seq 1 : true 1%r (1%r/2%r) 0%r 0%r; trivial. 
 by call (_: true ==> true); trivial.
seq 1 : Game1.real (1%r/2%r) 1%r (1%r/2%r) 0%r; trivial.
  rnd; skip; progress; trivial. 
   by rewrite Dbool.mu_def //=.
 case (validInputs i1 i2); last first.
  rcondt 1; first by trivial.
   rnd; first by apply Dbool.lossless.
   by skip; trivial.
 rcondf 1; first by skip; trivial.
 call (_: true ==> true); trivial.
 rcondf 5; trivial.
  wp; call (_: true ==> true); trivial.
  by call (_: true ==> true); trivial.
 wp; call (_: true ==> true); trivial.
 by call (_: true ==> true); trivial.
hoare.
case (validInputs i1 i2); last first.
 rcondt 1; first by trivial.
 by rnd; skip; trivial.
rcondf 1; first by skip; trivial.
call (_: true ==> true); trivial.
rcondt 5; trivial.
 wp; call (_: true ==> true); trivial.
 by call (_: true ==> true); trivial.
call (_: true ==> true); trivial.
wp; call (_: true ==> true); trivial.
by call (_: true ==> true); trivial.
save.

lemma Game1_Nreal_pr :
 forall (R1<:Rand1_t) (R2<:Rand2_t) (A1<:Adv1_t) (S<:Sim_t) &m,
 islossless R1.gen => islossless R2.gen =>
 islossless S.sim1 =>
 islossless A1.gen_query =>
 islossless A1.dist =>
 Pr [Game1(R1, R2, A1, S).main() @ &m : !Game1.real] = (1%r/2%r).
proof.
intros R1 R2 A1 S &m R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll.
rewrite Pr mu_not.
rewrite (Game1_true_pr (R1) (R2) (A1) (S) &m) //.
rewrite (Game1_real_pr (R1) (R2) (A1) (S) &m) //.
cut H: 1%r = 2%r / 2%r by smt.
rewrite {1} H.
smt.
save.

lemma Game2_lossless : forall 
 (R1<:Rand1_t) (R2<:Rand2_t) (A2<:Adv2_t) (S<:Sim_t),
 islossless R1.gen => islossless R2.gen =>
 islossless S.sim2 =>
 islossless A2.gen_query =>
 islossless A2.dist =>
 islossless Game2(R1, R2, A2, S).main.
proof.
intros R1 R2 A2 S R1_ll R2_ll S_ll A2_gen_ll A2_dist_ll.
fun.
seq 2 : true; trivial.
 rnd; progress; trivial; first by apply Dbool.lossless. 
 by call (_: true ==> true); trivial.
case (validInputs i1 i2).
 rcondf 1; first by skip; trivial.
 call (_:true ==> true); first by trivial.
 seq 2 : true; trivial.
  wp; call (_: true ==> true); trivial.
  by call (_: true ==> true); trivial.
 case (Game2.real).
  by rcondf 3; wp; skip; trivial.
 rcondt 3.
  by wp; skip; trivial.
 call (_:true ==> true); first by trivial.
 by wp; skip; trivial. 
rcondt 1; trivial.
rnd; trivial.
by apply Dbool.lossless.
save.

lemma ADV1_xpand : 
 forall (R1 <: Rand1_t) (R2 <: Rand2_t) (A1 <: Adv1_t) (S <: Sim_t) &m,
 islossless R1.gen => islossless R2.gen =>
 islossless S.sim1 =>
 islossless A1.gen_query =>
 islossless A1.dist => 
 2%r * Pr [ Game1(R1, R2, A1, S).main() @ &m : res] - 1%r
 = 2%r * ( Pr [ Game1(R1, R2, A1, S).main() @ &m : !Game1.real => res]
           - Pr [ Game1(R1, R2, A1, S).main() @ &m : Game1.real => !res] ).
proof.
intros R1 R2 A1 S &m R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll.
cut ->: ( Pr [ Game1(R1, R2, A1, S).main() @ &m : !Game1.real => res]
          = Pr [ Game1(R1, R2, A1, S).main() @ &m : Game1.real]
            + Pr [ Game1(R1, R2, A1, S).main() @ &m : res]
            - Pr [ Game1(R1, R2, A1, S).main() @ &m : res && Game1.real]).
 cut ->: (Pr[Game1(R1, R2, A1, S).main() @ &m : !Game1.real => res]
          = Pr[Game1(R1, R2, A1, S).main() @ &m : res \/ Game1.real]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; smt.
cut ->:  Pr [ Game1(R1, R2, A1, S).main() @ &m : Game1.real => !res]
  = Pr [ Game1(R1, R2, A1, S).main() @ &m : !Game1.real] + 1%r
    - Pr [ Game1(R1, R2, A1, S).main() @ &m : res]
    - Pr [ Game1(R1, R2, A1, S).main() @ &m : !res && !Game1.real].
 cut ->: (Pr[Game1(R1, R2, A1, S).main() @ &m : Game1.real => !res]
         = Pr[Game1(R1, R2, A1, S).main() @ &m : !res \/ !Game1.real]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; rewrite Pr mu_not.
 rewrite (Game1_true_pr R1 R2 A1 S &m) //; smt.
rewrite (Game1_Nreal_pr R1 R2 A1 S &m) // (Game1_real_pr R1 R2 A1 S &m) //.
cut H1: forall (x y:real), x - y = x + [-] y by smt.
cut H2: forall (x y:real), [-] (x + y) = [-] x + [-] y by smt.
cut H3: forall (x:real), [-] ([-] x) = x by smt.
cut H4: forall (x:real), 2%r * x = x + x.
 intros x; cut ->:2%r = (1%r + 1%r) by smt.
 by rewrite Mul_distr_r.
cut H4': forall (x y:real), 2%r * (x-y) = 2%r * x - 2%r * y by smt.
cut H5: forall (x:real), 2%r * -x = - 2%r * x by smt.
rewrite ?H1 ?H2 ?H3.
cut ->: (1%r / 2%r + Pr[Game1(R1,R2,A1,S).main() @ &m : res] +
 -Pr[Game1(R1,R2,A1,S).main() @ &m : res && Game1.real] +
 (-(1%r / 2%r) + -1%r + Pr[Game1(R1,R2,A1,S).main() @ &m : res] +
  Pr[Game1(R1,R2,A1,S).main() @ &m : ! res && ! Game1.real]))
 = ( (Pr[Game1(R1,R2,A1,S).main() @ &m : res] + Pr[Game1(R1,R2,A1,S).main() @ &m : res]) - 1%r) - (Pr[Game1(R1,R2,A1,S).main() @ &m : res && Game1.real] -
      Pr[Game1(R1,R2,A1,S).main() @ &m : ! res && ! Game1.real]) by smt.
rewrite -H4 H4'.
cut ADV1_xpand' : 2%r * Pr [ Game1(R1, R2, A1, S).main() @ &m : res] - 1%r
  = 2%r * ( Pr [ Game1(R1, R2, A1, S).main() @ &m : res && Game1.real]
        - Pr [ Game1(R1, R2, A1, S).main() @ &m : !res && !Game1.real] ).
 cut Hpr1: Pr [ Game1(R1, R2, A1, S).main() @ &m : res]
            = Pr [ Game1(R1, R2, A1, S).main() @ &m : res && Game1.real]
             + Pr [ Game1(R1, R2, A1, S).main() @ &m : res && !Game1.real].
  cut ->: Pr[Game1(R1, R2, A1, S).main() @ &m : res]
           = Pr[Game1(R1, R2, A1, S).main() @ &m
               : res && Game1.real \/ res && !Game1.real].
   rewrite Pr mu_eq; smt.
  rewrite Pr mu_disjoint; smt.
 cut Hpr2 : Pr [ Game1(R1, R2, A1, S).main() @ &m : res && !Game1.real]
            = Pr [ Game1(R1, R2, A1, S).main() @ &m : !Game1.real]
            - Pr [ Game1(R1, R2, A1, S).main() @ &m : !res && !Game1.real].
  cut ->: Pr[Game1(R1, R2, A1, S).main() @ &m : !Game1.real]
           = Pr[Game1(R1, R2, A1, S).main() @ &m 
               : res && !Game1.real \/ !res && !Game1.real].
   rewrite Pr mu_eq; smt.
  rewrite Pr mu_disjoint; smt.
 rewrite Hpr1 Hpr2 (Game1_Nreal_pr R1 R2 A1 S &m) //.
 smt.
rewrite -ADV1_xpand' //.
smt.
save.

end TPPprot.

(* OBS: esta teoria não pretende ser clonada! Funciona antes como um
 template para cada TwoPartyProtocol concreto (OT, PFE, ...) *)
theory TPPsec.
 clone TPPdefs.
 clone TPPprot as TPP with theory Defs = TPPdefs.
 import TPPdefs.
 import TPP.

(* MAIN PROPERTIES *)
(* =============== *)

 (* Protocol correctness *)
 axiom Correctness: forall i1 r1 i2 r2, 
  validInputs i1 i2 => correct i1 r1 i2 r2.

 (* Protocol security *)
 axiom Security :
  exists (epsilon:real) (R1 <: Rand1_t) (R2 <: Rand2_t (*{R1}*))
         (S <: Sim_t (*{R1, R2, A1, A2}*)),
  islossless R1.gen /\ islossless R2.gen /\
  islossless S.sim1 /\ islossless S.sim2 /\  
  forall (A1 <: Adv1_t (*{R1, R2}*)) (A2 <: Adv2_t (*{R1, R2}*)) &m,
   islossless A1.gen_query => islossless A1.dist =>
   islossless A2.gen_query => islossless A2.dist =>
    `|2%r * Pr[Game1(R1,R2,A1,S).main()@ &m:res] - 1%r| <= epsilon
    /\ `|2%r * Pr[Game2(R1,R2,A2,S).main()@ &m:res] - 1%r| <= epsilon.

end TPPsec.

