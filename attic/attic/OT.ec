require import TPP.
require import Bitstring.
require import Array.
require import ArrayExt.
require import Int.
require import Real.
import Pair.


theory OTdefs.
(* Generic OT protocol for fixed length bit strings 
 * Party 2 holds labels and Party 1 holds selection string 
 * Party 2 initiates the protocol *)

 (* length of input bitstrings *)
 op inputsLen : int.
 (* o que se pretendia era qualquer coisa na linha:

 clone Word as Inputs with op length = inputsLen.
 type inputs_t = Inputs.word.
 import Inputs.

 No entanto, o easycrypt não permite depois unificar o tipo
 Word.word (com um comprimento determinado) com outra instância
 da teoria instanciada com o mesmo comprimento. Por exemplo:

 require Word.
 op l : int.
 clone Word as W1 with op length = l.
 clone Word as W2 with op length = l.
 op idW (x:W1.word) : W2.word = x.

 Resulta no erro:

 This expression has type
   W2.word
 but is expected to have type
   W1.word

 Assim, o tipo inputs_t será bitstring, recorrendo ao predicado
 validInputs para impor o requisito nos comprimentos...
 *)

 (* number of selections *)
 op selLen : int.
 (* A mesma questão de inputs_t... 
 clone Word as Selection with op length = selLen.
 type selection_t = Selection.word.
 import Selection.
 *)
end OTdefs.

theory OTprot.
 clone import OTdefs.

 clone import TPPdefs with
  type input2_t = (bitstring*bitstring(*inputs_t*inputs_t*)) array,
  type output2_t = unit,
  type leak2_t = int,
  type input1_t = bitstring(*selection_t*),
  type output1_t = bitstring(*inputs_t*) array,
  type leak1_t = int,
  op phi1(i1: input1_t) = selLen,
  op phi2(i2: input2_t) = inputsLen,
  op validInputs(i1 : input1_t, i2 : input2_t) =
   length i1 = selLen && length i2 = selLen
   && allk (lambda k (x:bitstring*bitstring),
              length (fst x) = inputsLen && length (snd x) = inputsLen) i2,
  op correct (i1: input1_t, r1: rand1_t, i2: input2_t, r2 : rand2_t) =
   res1 i1 (r1, trace i1 r1 i2 r2)
   = fillk (lambda k, if i1.[k] then fst i2.[k] else snd i2.[k]) selLen.
 
 (* Oblivious-Transfer is an instance of Two-Party Protocol *)
 clone import TPPprot as P with theory Defs = TPPdefs.

 op badOTinput : input2_t  = (zeros 0, zeros 1)::Array.empty.

lemma badOTinput_validInputs: forall i1,
 ! validInputs i1 badOTinput.
proof.
intros i1.
rewrite -rw_neqF; delta validInputs badOTinput; simplify.
rewrite !anda_and.
cut Hrw: allk
   (lambda (k : int) (x : bitstring * bitstring),
      length (fst x) = inputsLen && length (snd x) = inputsLen)
   ((zeros 0, zeros 1) :: (Array.empty)%Array) = false.
 rewrite allk_def; simplify;  apply neqF.
 change (!(forall k, (lambda k, 0 <= k < length ((zeros 0, zeros 1) :: (Array.empty)) =>
     length (fst ((zeros 0, zeros 1) :: (Array.empty)%Array).[k]) = inputsLen &&
     length (snd ((zeros 0, zeros 1) :: (Array.empty)%Array).[k]) = inputsLen) k)).
 apply nforall; exists 0; simplify.
 rewrite Array.cons_length Array.empty_length /=.
 rewrite Array.cons_get //=.
  by rewrite Array.empty_length.
 delta fst snd; rewrite /= !zeros_length //= anda_and.
 apply nand.
 cut H: (0=inputsLen \/ !0=inputsLen) by smt.
 elim H => {H} H; first by rewrite -H.
 by left.
by rewrite Hrw !andF.
save.

lemma valid_inputs2kLen : forall i1 i2 k,
  validInputs i1 i2 =>
  0 <= k < length i2 =>
  length (fst i2.[k]) = inputsLen && length (snd i2.[k]) = inputsLen.
proof.
simplify validInputs.
intros i1 i2 k H Hk.
change ((lambda k (x : (bitstring * bitstring)),
        length (fst x) = inputsLen && length (snd x) = inputsLen) k i2.[k]).
rewrite allk_forall; smt.
save.

end OTprot.

theory OTsec.
 clone import OTdefs.
 clone import OTprot as OT with theory OTdefs = OTdefs.

 import OT.P.Defs.
 import OT.P.


(* MAIN PROPERTIES *)
(* =============== *)

 axiom Correctness: forall i1 r1 i2 r2, 
  validInputs i1 i2 => correct i1 r1 i2 r2.

 axiom Security :
  exists (epsilon:real) (R1 <: Rand1_t) (R2 <: Rand2_t (*{R1}*))
         (S <: Sim_t (*{R1, R2, A1, A2}*)),
  islossless R1.gen /\ islossless R2.gen /\
  islossless S.sim1 /\ islossless S.sim2 /\  
  forall (A1 <: Adv1_t (*{R1, R2}*)) (A2 <: Adv2_t (*{R1, R2}*)) &m,
   islossless A1.gen_query => islossless A1.dist =>
   islossless A2.gen_query => islossless A2.dist =>
    `|2%r * Pr[Game1(R1,R2,A1,S).main()@ &m:res] - 1%r| <= epsilon
    /\ `|2%r * Pr[Game2(R1,R2,A2,S).main()@ &m:res] - 1%r| <= epsilon.

end OTsec.
