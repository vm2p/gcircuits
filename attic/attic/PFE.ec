require import RandGen.
require import TPP.
require import SCH.
require import OT.

require import Real.
require import Array.
require import ArrayExt.
require import Bitstring.
require import Int.
require import Bool.
import Pair.

lemma unitProp: forall (x:unit), x=().
proof. admit. save.

(* Theory of the actual Private Function Evaluation protocol *)
theory PFEprot.

 (* Parameters for the PFE Protocol are just
  the parameters of the underlying Garble Scheme *)
 clone SCHdefs as Gdefs.
 clone SCHprot as G with theory Defs = Gdefs.

 clone import OTdefs with
  op inputsLen = Gdefs.ciphertextLen,
  op selLen = Gdefs.inputLen.

 clone OTprot as OT with theory OTdefs = OTdefs.

 (* Protocol parameters... *)
 clone TPPdefs as Pdefs with
  type rand1_t = OT.P.Defs.rand1_t,
  type input1_t = bitstring (*SCH.input_t*),
  type leak1_t = OT.P.Defs.leak1_t,
  type output1_t = G.Defs.output_t,
  op phi1(i1:input1_t) = G.Defs.inputLen,

  type rand2_t = OT.P.Defs.rand2_t * G.Defs.rand_t,
  type input2_t = G.Defs.fun_t,
  type leak2_t = OT.P.Defs.leak2_t * G.Defs.leak_t,
  type output2_t = unit,
  op phi2(i2:input2_t) = (G.Defs.ciphertextLen, G.Defs.phi i2),

  type trace_t = (G.Defs.funG_t * G.Defs.outputK_t) * OT.P.Defs.trace_t,
  op trace(i1:input1_t, r1:rand1_t, i2:input2_t, r2:rand2_t)
   = ( ( G.Defs.funG i2 (snd r2)
       , G.Defs.outputK i2 (snd r2))
     , OT.P.Defs.trace i1 r1 (G.Defs.inputK i2 (snd r2))
                 (fst r2)),

  op res1 (i1, v1) =
   let (r1,t) = v1 in 
   let (dataG,ot_t) = t in
   let (fG,oK) = dataG in
   G.Defs.decode oK 
     (G.Defs.evalG fG (OT.P.Defs.res1 i1 (r1, ot_t))),
  op res2 (i2: input2_t, v2: rand2_t*trace_t) = (),

  op validInputs(i1: input1_t, i2: input2_t) =
   G.Defs.validInputs i2 i1,

  op correct (i1: input1_t, r1: rand1_t, i2: input2_t, r2: rand2_t) =
   let o1 = res1 i1 (r1, trace i1 r1 i2 r2)
   in o1 = G.Defs.eval i2 i1.

 (* PFE is an instance of a Two Party Protocol *)
 clone import TPPprot as P with theory Defs = Pdefs.
 (* In TPPprot we use modules for Randomness Generation (instead of
  a theory, as in SCH). This is because we need to specify that we 
  sample rand2_t values according to the product distribution of two
  other sample distributions. What we would like to say is something
  like:
 clone RandGenProd with theory R1 = OT.Prot.R2, theory R2 = Scheme.GR.
 clone TPPprot with theory R2 = RandGenProd.
  But easycrypt complaints, since R2 is a _concrete_ theory :-(...
 *)

 import G.Defs.
 import P.Defs.

(*********************)
(* Correctness proof *)
(*********************)
print op res1.
lemma res1_def : forall i1 v1,
 res1 i1 v1
 = G.Defs.decode (snd (fst (snd v1)))
    (G.Defs.evalG (fst (fst (snd v1))) (OT.P.Defs.res1 i1 (fst v1, snd (snd v1)))).
proof. by smt. save.

lemma PFE_validInputs_OT: forall i1 i2 r,
 P.Defs.validInputs i1 i2 => OT.P.Defs.validInputs i1 (G.Defs.inputK i2 r).
proof.
intros i1 i2 r Hvalid.
split; first by rewrite (G.Defs.validInputsLen i2 i1). 
rewrite G.Defs.inputKLen => i1Len; split; first by trivial. 
intros H; rewrite allk_def G.Defs.inputKLen.
intros k Hk; beta.
rewrite (_:OT.OTdefs.inputsLen = G.Defs.ciphertextLen); first by trivial.
by apply G.Defs.inputKLenC.
save.

lemma PFE_validInputs_OT_n: forall i1 i2 r,
 !P.Defs.validInputs i1 i2 =>
 !OT.P.Defs.validInputs i1 (if validInputs i1 i2 then G.Defs.inputK i2 r else OT.badOTinput).
proof.
intros i1 i2 r.
rewrite -rw_neqF => -> /=. 
apply (OT.badOTinput_validInputs i1).
save.

lemma PFE_Correctness :
 OT.P.Correct () =>
 forall (i1 : input1_t) (r1 : rand1_t) (i2 : input2_t) (r2 : rand2_t),
  validInputs i1 i2 => correct i1 r1 i2 r2.
proof.
intros OTcorrect i1 r1 i2 r2 Hvalid.
delta correct Pdefs.correct trace Pdefs.res1 Pdefs.trace; simplify.
rewrite (G.Defs.inverse i2 (snd r2)); simplify.
congr; last by trivial.
congr; last by trivial.
cut Hot : OT.P.Defs.correct i1 r1 ((G.Defs.inputK i2 (snd r2))) (fst r2)
 by smt.
generalize Hot; delta OT.P.Defs.correct OT.TPPdefs.correct; simplify.
by rewrite G.encode_fillk.
save.

(* RANDOM GENERATORS *)
module PFE_R1 (OT_R1: OT.P.Rand1_t): Rand1_t = {
 fun gen(i1info: leak1_t): rand1_t = {
  var r: rand1_t;
  r = OT_R1.gen(i1info);
  return r;
  }
}.

lemma PFE_R1_ll:
 forall (OT_R1<: OT.P.Rand1_t),
 islossless OT_R1.gen =>
 islossless PFE_R1(OT_R1).gen.
proof.
intros OT_R1 OT_R1_ll.
fun; inline PFE_R1(OT_R1).gen.
call (_: true ==> true); first by apply OT_R1_ll.
by skip; trivial.
save.

module PFE_R2(OT_R2 : OT.P.Rand2_t(*, G_R : PScheme.Rand_t*)) : Rand2_t = {
     fun gen(i2info : leak2_t) : rand2_t = { 
         var r1 : OT.P.Defs.rand2_t;
         var r2 : G.Defs.rand_t;
         r1 = OT_R2.gen(Pair.fst i2info);
         r2 = $G.GR.drand (snd i2info) (*G_R.gen(Pair.snd i2info)*);
         return (r1,r2);
     }
}.

lemma PFE_R2_ll:
 forall (OT_R2<: OT.P.Rand2_t),
 islossless OT_R2.gen =>
 islossless PFE_R2(OT_R2).gen.
proof.
intros OT_R2 OT_R2_ll.
fun; inline PFE_R2(OT_R2).gen.
rnd; progress; trivial.
call (_: true ==> true); first by apply OT_R2_ll.
skip; trivial.
by intros &hr; apply (G.GR.drand_distr (snd i2info{hr})).
save.

(* SIMULATOR *)
 module PFE_S((*G_R: PG.Rand_t,*) G_S : G.SSim_t, OT_S : OT.P.Sim_t)
 : Sim_t = {
     fun sim1(i1: input1_t, o1: output1_t, l2 : leak2_t)
     : view1_t = {
       var fG: G.Defs.funG_t;
       var xG: G.inputG_t;
       var xK: G.inputK_t;
       var oK: G.Defs.outputK_t;
       var ot_t: OT.P.Defs.trace_t;
       var ot_r1: OT.P.Defs.rand1_t;

       (fG, xG, oK) = G_S.sim(i1, o1, snd l2);
       (ot_r1,ot_t) = OT_S.sim1(i1, xG, fst l2);

       return (ot_r1, ((fG, oK), ot_t));
     }

     fun sim2(i2: input2_t, o2: output2_t, l1: leak1_t)
     : view2_t = {
          var view : trace_t;
          var fG : G.Defs.funG_t;
          var xK : G.inputK_t;
          var oK : G.Defs.outputK_t;
          var g_r : G.Defs.rand_t;
          var ot_t : OT.P.Defs.trace_t;
          var ot_r2 : OT.P.Defs.rand2_t;

          g_r = $G.GR.drand (G.Defs.phi i2);
          (ot_r2, ot_t) = OT_S.sim2(G.Defs.inputK i2 g_r, o2, l1);
         
          return ((ot_r2,g_r),((G.Defs.funG i2 g_r
                               ,G.Defs.outputK i2 g_r), ot_t));
     }
  }.

lemma PFE_Ssim1_ll: forall (G_S <: G.SSim_t) (OT_S <: OT.P.Sim_t),
 islossless G_S.sim => islossless OT_S.sim1 =>
 islossless PFE_S(G_S, OT_S).sim1.
proof.
intros G_S OT_S G_S_ll OT_S_ll.
fun.
call (_:true ==> true); first by apply OT_S_ll.
wp; call (_:true ==> true); first by apply G_S_ll.
by skip; trivial.
save.

lemma PFE_Ssim2_ll: forall (G_S <: G.SSim_t) (OT_S <: OT.P.Sim_t),
 islossless OT_S.sim2 =>
 islossless PFE_S(G_S, OT_S).sim2.
proof.
intros G_S OT_S OT_S_ll.
fun.
call (_:true ==> true); first by apply OT_S_ll.
rnd; skip; progress; trivial.
by apply (G.GR.drand_distr (G.Defs.phi i2{hr})).
save.

(* ADVERSARIES *)
module B_OT1((*G_R : PG.Rand_t,*) PFE_A1: Adv1_t) : OT.P.Adv1_t = {
    var o : G.Defs.output_t
    var fG : G.Defs.funG_t
    var oK : G.Defs.outputK_t

    fun gen_query() : OT.P.Defs.input1_t * OT.P.Defs.input2_t = {
        var x : SCH.input_t;
        var f : G.Defs.fun_t;
        var y : G.inputG_t;
        var r : G.Defs.rand_t;

        (x,f) = PFE_A1.gen_query();
        o = G.Defs.eval f x;
        r = $G.GR.drand (*G_R.gen*) (G.Defs.phi f);
        fG = G.Defs.funG f r;
        oK = G.Defs.outputK f r;
        return (x, if validInputs x f
                   then G.Defs.inputK f r
                   else OT.badOTinput);
    }

    fun dist(view: OT.P.view1_t): bool = {
        var guess : bool;
        guess = PFE_A1.dist((fst view,((fG, oK), snd view)));

        return guess;
    }
}.

lemma B_OT1_gen_ll : forall (A1<:Adv1_t),
 islossless A1.gen_query =>
 islossless B_OT1(A1).gen_query.
proof.
intros A1 A1_gen_lossless.
fun; wp; rnd; wp.
wp; call (_:true ==> true); trivial.
skip; progress; trivial.
by apply (G.GR.drand_distr (phi x2)).
save.

lemma B_OT1_dist_ll : forall (A1<:Adv1_t),
 islossless A1.dist =>
 islossless B_OT1(A1).dist.
proof.
intros A1 A1_dist_lossless.
fun; wp; progress; trivial.
by wp; call (_:true ==> true); trivial.
save.

module B_G(PFE_A1 : Adv1_t, OT_S : OT.P.Sim_t) : G.SAdv_t = {
    var x : SCH.input_t
    var f : G.Defs.fun_t
    var o : G.Defs.output_t

    fun gen_query() : G.sim_query_t = {
        (x,f) = PFE_A1.gen_query();
        o = G.Defs.eval f x;
        return (f,x);
    }

    fun dist(ans: G.sim_answer_t) : bool = {
      var fG : G.Defs.funG_t;
      var xG : G.inputG_t;
      var xK : G.inputK_t;
      var oK : G.Defs.outputK_t;
      var ot_r1 : OT.P.Defs.rand1_t;
      var ot_t : OT.P.Defs.trace_t;
      var guess : bool;

      (fG,xG,oK) = ans;
      (ot_r1,ot_t) = OT_S.sim1(x, xG, G.Defs.ciphertextLen);
      guess = PFE_A1.dist((ot_r1, ((fG, oK), ot_t))); 

      return guess;
    }
}.

lemma B_G_gen_ll : forall (A1<:Adv1_t) (OT_S<:OT.P.Sim_t),
 islossless A1.gen_query =>
 islossless B_G(A1, OT_S).gen_query.
proof.
intros A1 OT_S A1_gen_lossless.
fun; wp; progress; trivial.
by wp; call (_:true ==> true); trivial.
save.

lemma B_G_dist_ll : forall (A1<:Adv1_t) (OT_S<:OT.P.Sim_t),
 islossless A1.dist =>
 islossless OT_S.sim1 =>
 islossless B_G(A1,OT_S).dist.
proof.
intros A1 OT_S A1_gen_lossless OT_lossless.
fun; wp; progress; trivial.
call (_:true ==> true); trivial.
call (_:true ==> true); trivial.
by wp; skip; trivial.
save.

module B_OT2((*G_R : PG.Rand_t,*) PFE_A2 : Adv2_t) : OT.P.Adv2_t = {
    var fG : G.Defs.funG_t
    var oK : G.Defs.outputK_t
    var o : G.Defs.output_t
    var r : G.Defs.rand_t

    fun gen_query(): OT.P.Defs.input1_t * OT.P.Defs.input2_t  = {
        var x : SCH.input_t;
        var f : G.Defs.fun_t;
        var xK : G.inputK_t;

        (x,f) = PFE_A2.gen_query();
        o = G.Defs.eval f x;
        r = $G.GR.drand (*G_R.gen*) (G.Defs.phi f);
        fG = G.Defs.funG f r;
        oK = G.Defs.outputK f r;
        return (x, if validInputs x f
                   then G.Defs.inputK f r
                   else OT.badOTinput);
    }

    fun dist(view: OT.P.view2_t) : bool = {
        var guess : bool;

        guess = PFE_A2.dist(((fst view, r), ((fG, oK), snd view)));
        
        return guess;
    }
}.

lemma B_OT2_gen_ll : forall (A2<:Adv2_t),
 islossless A2.gen_query =>
 islossless B_OT2(A2).gen_query.
proof.
intros A2 A2_gen_lossless.
fun; wp; rnd; progress; trivial.
wp; call (_:true ==> true); trivial.
skip; progress.
by apply (G.GR.drand_distr (phi x2)).
save.

lemma B_OT2_dist_ll : forall (A2<:Adv2_t),
 islossless A2.dist =>
 islossless B_OT2(A2).dist.
proof.
intros A2 A2_dist_lossless.
fun; wp; progress; trivial.
by wp; call (_:true ==> true); trivial.
save.



(* Lemmata for Party 1 simulation *)
lemma Game1_real_equiv :
  forall (G_S <: G.SSim_t {Game1})
         (OT_S <: OT.P.Sim_t {OT.P.Game1, Game1})
         (OT_R1 <: OT.P.Rand1_t {B_OT1, Game1, OT.P.Game1}) 
         (OT_R2 <: OT.P.Rand2_t  {B_OT1, OT_R1, Game1, OT.P.Game1})
(*         (G_R <: PG.Rand_t { OT_R1, OT_R2, B_OT1 })*)
         (PFE_A1 <: Adv1_t {Game1, OT.P.Game1, (*G_R,*) B_OT1, OT_R1,OT_R2,OT_S,G_S }),
  islossless OT_S.sim1 =>
  islossless PFE_A1.dist =>
  islossless G_S.sim =>
(*  islossless G_R.gen =>*)
  equiv 
  [ OT.P.Game1(OT_R1, OT_R2, B_OT1((*G_R,*) PFE_A1), OT_S).main ~ 
    Game1(PFE_R1(OT_R1),PFE_R2(OT_R2(*,G_R*)),PFE_A1,PFE_S((*G_R,*)G_S,OT_S)).main
  : ={glob PFE_A1, (*glob G_R,*) glob OT_R1, glob OT_R2}
    ==> OT.P.Game1.real{1} /\ res{1} 
        <=> Game1.real{2} /\ res{2}
(*        => (*OTP.Game1.adv{1} = PFE.Game1.adv{2} *) !res{1} = !res{2} *)
  ].
proof.
intros G_S OT_S OT_R1 OT_R2 (*G_R*) PFE_A1.
intros OT_S_ll PFE_A1_ll G_S_ll (*G_R_ll*).
fun.
inline B_OT1((*G_R,*) PFE_A1).gen_query.
swap {1} 7 -5.
seq 2 2 : (x{1} = i1{2} /\ f{1} = i2{2}
          /\ OT.P.Game1.real{1} = Game1.real{2}
          /\ ={(*glob G_R,*) glob OT_R1, glob OT_R2, glob PFE_A1}).
 rnd; wp; call (_: ={glob PFE_A1} ==> ={res, glob PFE_A1}).
  by fun true; trivial.
 by skip; trivial.

case (validInputs i1{2} i2{2}); last first.
 rcondt {2} 1; first by trivial.
 rcondt {1} 6.
  intros &m; wp; rnd; wp; skip; progress; trivial. 
  by apply PFE_validInputs_OT_n.
 rnd; wp; rnd{1} (*call {1} (_ : true ==> true); first by apply G_R_ll.*).
 wp; skip; progress; trivial.
 by apply G.GR.drand_distr.

(* validInputs i1 i2 *)
rcondf {2} 1; first  by intros &m; skip;trivial.
inline PFE_R1(OT_R1).gen PFE_R2(OT_R2(*, G_R*)).gen.
swap {2} 4 -3.
swap {2} 6 -4.
swap {1} 2 -1.
seq 1 2 : (x{1} = i1{2} /\ f{1} = i2{2}
          /\ OT.P.Game1.real{1} = Game1.real{2}
          /\ validInputs i1{2} i2{2}
          /\ i2info{2} = phi2 i2{2}
          /\ r{1} = r20{2}
          /\ ={glob PFE_A1, glob OT_R1, glob OT_R2}).
(*
 call (_ : ={finfo, glob G_R} ==> ={res}).
  by fun true; trivial.
 wp; skip; smt.
*)
rnd; wp; skip;trivial.
rcondf {1} 5.
 intros &m; wp; skip; trivial.
 cut HH: (forall i1 i2 r,
   P.Defs.validInputs i1 i2 => OT.P.Defs.validInputs i1 ((G.Defs.inputK i2 r)))
  by apply PFE_validInputs_OT.
 intros &hr; smt.

seq 6 4 : (x{1} = i1{2} /\ f{1} = i2{2}
          /\ OT.P.Game1.real{1} = Game1.real{2}
          /\ validInputs i1{2} i2{2}
          /\ i2info{2} = phi2 i2{2}
          /\ i1info{2} = phi1 i1{2}
          /\ r{1} = r20{2}
          /\ B_OT1.o{1} = G.Defs.eval f{1} x{1}
          /\ B_OT1.fG{1} = (G.Defs.funG f{1} r{1})
          /\ B_OT1.oK{1} = (G.Defs.outputK f{1} r{1})
          /\ i1{1} = x{1}
          /\ i2{1} = G.Defs.inputK f{1} r{1}
          /\ r1{1} = r1{2}
          /\ r2{1} = r10{2}
          /\ ={glob PFE_A1}).
 call (_ : ={i2info, glob OT_R2} ==> ={res}); first by fun true;trivial.
 wp; call (_ : ={i1info, glob OT_R1} ==> ={res}); first by fun true;trivial.
 wp; skip; progress; trivial.
 by rewrite H.

inline B_OT1((*G_R,*) PFE_A1).dist.
case (OT.P.Game1.real{1}).
 rcondf {1} 3; first by intros &m; wp; skip; trivial.
 rcondf {2} 4; first by intros &m; wp; skip; trivial.
 wp; call (_ : ={view, glob PFE_A1} ==> ={res}); first by fun true; trivial.
 wp; skip; smt.

rcondt {1} 3; first by intros &m; wp; skip; trivial.
rcondt {2} 4; first by intros &m; wp; skip; trivial.
inline PFE_S((*G_R,*) G_S, OT_S).sim1.
conseq (_: ( ={glob PFE_A1} /\ OT.P.Game1.real{1} = Game1.real{2} /\ !OT.P.Game1.real{1}) ==> _).
 intros &1 &2;smt.
call {2} (_ : true ==> true); first by apply PFE_A1_ll.
wp; call {2} (_ : true ==> true); first by apply OT_S_ll.
wp; call {2} (_ : true ==> true); first by apply G_S_ll.
wp; call {1} (_ : true ==> true); first by apply PFE_A1_ll.
wp; call {1} (_ : true ==> true); first by apply OT_S_ll.
wp; skip; smt.
save.

lemma Game1_real_pr : 
  forall (G_S <: G.SSim_t {Game1})
         (OT_S <: OT.P.Sim_t {OT.P.Game1, Game1})
         (OT_R1 <: OT.P.Rand1_t {B_OT1, Game1, OT.P.Game1}) 
         (OT_R2 <: OT.P.Rand2_t  {B_OT1, OT_R1, Game1, OT.P.Game1})
         (PFE_A1 <: Adv1_t {Game1, OT.P.Game1, (*G_R,*) B_OT1, OT_R1,OT_R2,OT_S,G_S })
         &m,
  islossless OT_S.sim1 =>
  islossless PFE_A1.dist =>
  islossless G_S.sim =>
  Pr[OT.P.Game1(OT_R1, OT_R2, B_OT1((*G_R,*) PFE_A1), OT_S).main()@ &m: OT.P.Game1.real => !res]
=   Pr[Game1(PFE_R1(OT_R1),PFE_R2(OT_R2(*,G_R*)),PFE_A1,PFE_S((*G_R,*)G_S,OT_S)).main()@ &m: Game1.real => !res]
.
proof.
intros G_S OT_S OT_R1 OT_R2 PFE_A1 &m OT_S_ll PFE_A1_ll G_S_ll.
equiv_deno (_: ={glob PFE_A1, glob OT_R1, glob OT_R2}
    ==> OT.P.Game1.real{1} /\ res{1} <=> Game1.real{2} /\ res{2}).
 apply (Game1_real_equiv (G_S) (OT_S) (OT_R1) (OT_R2) (PFE_A1)); trivial.
 by trivial.
smt.
save.

(* TODO: move this elsewhere... *)
lemma OT_Correct: OT.P.Correct () => forall i1 r1 i2 r2,
 OT.P.Defs.validInputs i1 i2 => OT.P.Defs.correct i1 r1 i2 r2.
proof. by smt. save.

(* We know: |B_OT1 = 1 | real = 1 - B_OT1 = 0 | real = 0| < epsilon *)
lemma encode_OT: forall f i r1 r2 rG,
 OT.P.Correct () =>
 OT.P.Defs.validInputs i (G.Defs.inputK f rG) =>
 OT.P.Defs.res1 i (r1, OT.TPPdefs.trace i r1 (G.Defs.inputK f rG) r2)
 = G.Defs.encode (G.Defs.inputK f rG) i.
proof.
intros f i r1 r2 rG OTcorrect Hvalid.
cut Hot :
 OT.P.Defs.correct i r1 ((G.Defs.inputK f rG)) r2.
 by apply OT_Correct; trivial.
rewrite G.encode_fillk.
generalize Hot; delta; simplify => Hot.
by rewrite Hot.
save.

lemma Game1_hybrid_equiv : 
 forall (G_S <: G.SSim_t)
        (OT_S <: OT.P.Sim_t {B_G,  B_OT1, OT.P.Game1, G.SGame})
        (OT_R1 <: OT.P.Rand1_t {OT.P.Game1})
        (OT_R2 <: OT.P.Rand2_t {OT.P.Game1})
(*        (G_R <: PG.Rand_t {PG.SGame, OTP.Game1, OT_S, B_G, B_OT1} )*)
        (PFE_A1 <: Adv1_t {G.SGame, OT.P.Game1, OT_S(*, G_R*), B_OT1, B_G} ),
 islossless PFE_A1.dist =>
 islossless OT_R1.gen =>
 islossless OT_R2.gen =>
 islossless OT_S.sim1 =>
(* islossless G_R.gen =>*)
 islossless G_S.sim =>
 OT.P.Correct () =>
 equiv 
 [ OT.P.Game1(OT_R1,OT_R2,B_OT1((*G_R,*) PFE_A1),OT_S).main
 ~ G.SGame((*G_R,*)G_S,B_G(PFE_A1, OT_S)).main
 : ={glob PFE_A1(*, glob G_R*), glob OT_S}
 ==> !OT.P.Game1.real{1} /\ !res{1} <=> G.SGame.real{2} /\ res{2}
 ].
proof.
intros G_S OT_S OT_R1 OT_R2 (*G_R*) PFE_A1.
intros PFE_A1_ll OT_R1_ll OT_R2_ll OT_S_ll (*G_R_ll*) G_S_ll OTcorrect.
fun.
inline B_OT1((*G_R,*) PFE_A1).gen_query B_OT1((*G_R,*) PFE_A1).dist.
inline B_G(PFE_A1, OT_S).gen_query B_G(PFE_A1, OT_S).dist.
seq 1 1 : (x{1} = B_G.x{2} /\ f{1} = B_G.f{2}
          /\ ={(*glob G_R,*) glob PFE_A1, glob OT_S}).
 call (_: ={glob PFE_A1} ==> ={res, glob PFE_A1}); first by fun true; trivial.
 by skip; progress; trivial.

case (validInputs x{1} f{1}); last first.
 rcondt {2} 4.
  intros &m; rnd; wp; skip; smt.
 rcondt {1} 7.
  intros &m.
  wp; (*call (_:true ==> true); first by fun true; trivial*) rnd.
  cut _: (forall r, validInputs B_G.x{m} f{m}
                    => OT.P.Defs.validInputs B_G.x{m} (G.Defs.inputK f{m} r)).
   by intros r H; apply PFE_validInputs_OT.
  wp; rnd; wp; skip; smt.
 rnd; rnd (lambda x, !x).
 wp; (*call {1} (_: true ==> true); first by apply G_R_ll.*) rnd {1}.
 wp; skip; smt. 
(* validInputs *)
swap {1} 6 -5;  swap {2} 3 -2.
seq 1 1 : (x{1} = B_G.x{2} /\ f{1} = B_G.f{2}
          /\ !OT.P.Game1.real{1} = G.SGame.real{2}
          /\ validInputs x{1} f{1}
          /\ ={(*glob G_R,*) glob PFE_A1, glob OT_S}).
 rnd (lambda z, !z); skip; smt.
rcondf {2} 3; first by intros &m; wp; skip; smt.
rcondf {1} 6.
 intros &m.
 wp; (*call (_:true ==> true); first by fun true; trivial*) rnd.
  cut _: (forall r, validInputs B_G.x{m} f{m}
      => OT.P.Defs.validInputs B_G.x{m} (G.Defs.inputK f{m} r)).
  by intros r; apply PFE_validInputs_OT.
 wp; skip; smt.

case (!OT.P.Game1.real{1}).
 rcondt {1} 10.
  intros &m.
  wp; call (_: true ==> true); first by trivial.
  wp; call (_: true ==> true); first by trivial.
  wp; rnd (*call (_: true ==> true); first by trivial*).
  by wp; skip; trivial. 
 rcondt {2} 4; first by intros &m; wp; skip; smt.
 wp; call (_: ={view, glob PFE_A1} ==> ={res}); first by fun true; trivial.
 wp; call (_: ={i1, o1, l2, glob OT_S} ==> ={res}); first by fun true;trivial.
 wp; call {1} (_: true ==> true); first by apply OT_R2_ll.
 call {1} (_: true ==> true); first by apply OT_R1_ll.
 wp; rnd (*call (_: ={finfo, glob G_R} ==> ={res}); first by fun true; trivial*).
 cut _: (forall f i r1 r2 rG, P.Defs.validInputs i f =>
   OT.P.Defs.res1 i (r1, OT.P.Defs.trace i r1 (G.Defs.inputK f rG) r2)
   = G.Defs.encode (G.Defs.inputK f rG) i) by smt.
 wp; skip; smt.
(* OTP.Game1.real{1} *)
rcondf {1} 10.
  intros &m.
  wp; call (_: true ==> true); first by trivial.
  wp; call (_: true ==> true); first by trivial.
  wp; rnd (*call (_: true ==> true); first by trivial*).
  by wp; skip; trivial. 
rcondf {2} 4; first by intros &m; wp; skip; smt.
conseq (_ : _ ==> OT.P.Game1.real{1} /\ !G.SGame.real{2}); first by smt.
wp; call {1} (_: true ==> true); first by apply PFE_A1_ll.
call {2} (_: true ==> true); first by apply PFE_A1_ll.
wp; call {2} (_: true ==> true); first by apply OT_S_ll.
wp; call {2} (_: true ==> true); first by apply G_S_ll.
call {1} (_: true ==> true); first by apply OT_R2_ll.
call {1} (_: true ==> true); first by apply OT_R1_ll.
wp; rnd {1} (*call {1} (_: true ==> true); first by apply G_R_ll*).
wp; skip; smt.
save.

lemma Game1_hybrid_pr : 
 forall (G_S <: G.SSim_t)
        (OT_S <: OT.P.Sim_t {B_G,  B_OT1, OT.P.Game1, G.SGame})
        (OT_R1 <: OT.P.Rand1_t {OT.P.Game1})
        (OT_R2 <: OT.P.Rand2_t {OT.P.Game1})
        (PFE_A1 <: Adv1_t {G.SGame, OT.P.Game1, OT_S, B_OT1, B_G} )
        &m,
 islossless PFE_A1.dist =>
 islossless OT_R1.gen =>
 islossless OT_R2.gen =>
 islossless OT_S.sim1 =>
 islossless G_S.sim =>
 OT.P.Correct () =>
 Pr [ OT.P.Game1(OT_R1,OT_R2,B_OT1(PFE_A1),OT_S).main() @ &m
      : !OT.P.Game1.real => res ] =
 Pr [ G.SGame(G_S,B_G(PFE_A1, OT_S)).main() @ &m
      : G.SGame.real  => !res ].
proof.
intros G_S OT_S OT_R1 OT_R2 PFE_A1 &m PFE_A1_ll OT_R1_ll OT_R2_ll OT_S_ll G_S_ll OTcorrect.
equiv_deno (_: ={glob PFE_A1(*, glob G_R*), glob OT_S}
 ==> !OT.P.Game1.real{1} /\ !res{1} <=> G.SGame.real{2} /\ res{2}).
  by apply (Game1_hybrid_equiv (G_S) (OT_S) (OT_R1) (OT_R2) (PFE_A1)).
 by trivial.
smt.
save.

(* We know: | B_G = 1 | b = 1 - B_G = 1 | b = 0| < epsilon_g *)

lemma Game1_ideal_equiv : 
 forall (G_S <: G.SSim_t { B_G, G.SGame, Game1})
        (OT_S <: OT.P.Sim_t {Game1,G.SGame,B_G})
(*        (G_R <: PG.Rand_t { OT_S, G_S,PFE.Game1, PG.SGame, B_G})*)
        (OT_R1 <: OT.P.Rand1_t {Game1(*, G_R*)})
        (OT_R2 <: OT.P.Rand2_t {Game1, OT_R1(*, G_R*)} )
        (PFE_A1 <: Adv1_t {B_G, Game1,G.SGame(*, G_R*), G_S, OT_S}),
 islossless PFE_A1.dist =>
 islossless OT_R1.gen =>
 islossless OT_R2.gen =>
(* islossless G_R.gen =>*)
 islossless OT_S.sim1 =>
 OT.P.Correct () =>
 equiv
 [ Game1(PFE_R1(OT_R1),PFE_R2(OT_R2(*,G_R*)),PFE_A1,PFE_S((*G_R,*)G_S,OT_S)).main
 ~ G.SGame((*G_R,*)G_S,B_G(PFE_A1,OT_S)).main
 : ={glob PFE_A1(*, glob G_R*), glob G_S, glob OT_S}
   ==> !Game1.real{1} /\ !res{1} <=> !G.SGame.real{2} /\ !res{2}
(*   ==> !Game1.real{1} && !G.SGame.real{2} => ={res}*)
 ].
proof.
intros G_S OT_S (*G_R*) OT_R1 OT_R2 PFE_A1.
intros PFE_A1_ll OT_R1_ll OT_R2_ll (*G_R_ll*) OT_S_ll OTcorrect.
fun.
inline PFE_R1(OT_R1).gen PFE_R2(OT_R2(*, G_R*)).gen.
inline PFE_S((*G_R,*) G_S, OT_S).sim1.
inline B_G(PFE_A1, OT_S).gen_query B_G(PFE_A1, OT_S).dist.
swap {2} 4 -2.
seq 2 2 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
          /\ Game1.real{1} = G.SGame.real{2}
          /\ ={(*glob G_R,*) glob PFE_A1, glob G_S, glob OT_S}).
 (* EasyCrypt BUG: try...catch in line 87 of ecLogic hides informative
  error message. To reproduce:
  - remove OT_S from PFE_A1 restriction list;
  - call tactic triggers "anomaly: File "src/ecPhl.ml", line 87, ..."
    instead of "The module OT_S can write PFE_A1 (add..."
 *)
 rnd.
 call (_: ={glob PFE_A1} ==> ={res, glob PFE_A1}).
  fun true; by trivial.
 skip; progress; trivial.

case (validInputs i1{1} i2{1}); last first.
 rcondt {1} 1; first by intros &m; wp;skip; trivial.
 rcondt {2} 3; first by intros &m; wp;skip; trivial.
 rnd; wp; skip; smt.
(* validInputs *)
rcondf {1} 1; first by intros &m; wp;skip; trivial.
rcondf {2} 3; first by intros &m; wp;skip; trivial.

case (Game1.real{1}).
 rcondf {1} 10.
  intros &m.
  wp; rnd (*call (_: true ==> true); first by trivial*).
  wp; call (_: true ==> true); first by trivial.
  wp; call (_: true ==> true); first by trivial.
  by wp; skip; trivial. 
 rcondt {2} 4; first by intros &m; wp; skip; smt.
 conseq (_ : _ ==> Game1.real{1} /\ G.SGame.real{2}); first by smt.
 wp; call {1} (_: true ==> true); first by apply PFE_A1_ll.
 wp; call {2} (_: true ==> true); first by apply PFE_A1_ll.
 wp; call {2} (_: true ==> true); first by apply OT_S_ll.
 wp; rnd (*call (_: ={finfo, glob G_R} ==> ={res}); first fun true*; smt*).
 call {1} (_: true ==> true); first by apply OT_R2_ll.
 wp; call {1} (_: true ==> true); first by apply OT_R1_ll.
 by wp; skip; trivial.
(* !Game1.real{1} *)
rcondt {1} 10.
 intros &m.
 wp; rnd (*call (_: true ==> true); first by trivial*).
 wp; call (_: true ==> true); first by trivial.
 wp; call (_: true ==> true); first by trivial.
 by wp; skip; trivial. 
rcondf {2} 4; first by intros &m; wp; skip; smt.
wp; call (_: ={view, glob PFE_A1} ==> ={res}); first by fun true; trivial.
wp; call (_: ={i1, o1, l2, glob OT_S} ==> ={res}); first by fun true; trivial.
wp; call (_: ={i, o, leak, glob G_S} ==> ={res}); first by fun true; trivial.
wp; rnd {1} (*call {1} (_: true ==> true); first by apply G_R_ll*).
wp; call {1} (_: true ==> true); first by apply OT_R2_ll.
wp; call {1} (_: true ==> true); first by apply OT_R1_ll.
cut _: forall l, Distr.weight (G.GR.drand l) = 1%r
 by apply G.GR.drand_distr.
cut HH: (forall f i r1 r2 rG, P.Defs.validInputs i f =>
  OT.P.Defs.res1 i (r1, OT.TPPdefs.trace i r1 (G.Defs.inputK f rG) r2)
   = G.Defs.encode (G.Defs.inputK f rG) i) by smt.
generalize HH; delta; simplify => HH.
wp; skip; progress; smt.
save.

lemma Game1_ideal_pr : 
 forall (G_S <: G.SSim_t { B_G, G.SGame, Game1})
        (OT_S <: OT.P.Sim_t {Game1,G.SGame,B_G})
        (OT_R1 <: OT.P.Rand1_t {Game1(*, G_R*)})
        (OT_R2 <: OT.P.Rand2_t {Game1, OT_R1(*, G_R*)} )
        (PFE_A1 <: Adv1_t {B_G, Game1,G.SGame(*, G_R*), G_S, OT_S})
        &m,
 islossless PFE_A1.dist =>
 islossless OT_R1.gen =>
 islossless OT_R2.gen =>
 islossless OT_S.sim1 =>
 OT.P.Correct () =>
 Pr [Game1(PFE_R1(OT_R1),PFE_R2(OT_R2),PFE_A1,PFE_S(G_S,OT_S)).main() @ &m
      : !Game1.real => res ] =
 Pr [ G.SGame((*G_R,*)G_S,B_G(PFE_A1,OT_S)).main() @ &m
      : !G.SGame.real => res ].
proof.
intros G_S OT_S OT_R1 OT_R2 PFE_A1 &m PFE_A1_ll OT_R1_ll OT_R2_ll OT_S_ll OTcorrect.
equiv_deno (_: ={glob PFE_A1(*, glob G_R*), glob G_S, glob OT_S}
   ==> !Game1.real{1} /\ !res{1} <=> !G.SGame.real{2} /\ !res{2}).
  by apply (Game1_ideal_equiv (G_S) (OT_S) (OT_R1) (OT_R2) (PFE_A1)).
 by trivial.
smt.
save.

(* Lemma for Party 2 simulation *)

lemma Reduction2 : 
 forall (*(G_R <: PG.Rand_t {OTP.Game2, PFE.Game2, B_OT2})*)
        (G_S <: G.SSim_t) 
        (PFE_A2 <: Adv2_t {(*G_R,*) B_OT2, Game2, OT.P.Game2} )
        (OT_S <: OT.P.Sim_t {B_OT2, PFE_A2, B_OT2((*G_R,*) PFE_A2), (*G_R,*) Game2, OT.P.Game2} )    
        (OT_R1 <: OT.P.Rand1_t {B_OT2, PFE_A2, OT.P.Game2, Game2(*, G_R*)})
        (OT_R2 <: OT.P.Rand2_t {B_OT2, OT_S, PFE_A2, OT.P.Game2, Game2(*, G_R*)}),
(* islossless G_R.gen =>*)
 equiv 
 [ OT.P.Game2 (OT_R1, OT_R2, B_OT2((*G_R,*) PFE_A2), OT_S).main
 ~ Game2 (PFE_R1(OT_R1), PFE_R2(OT_R2(*,G_R*)), PFE_A2, PFE_S((*G_R,*)G_S,OT_S)).main 
 : ={glob PFE_A2(*, glob G_R*), glob OT_R1, glob OT_R2, glob OT_S} ==> ={res}
 ].
proof.
intros (*G_R*) G_S PFE_A2 OT_S OT_R1 OT_R2.
(*intros G_R_ll.*)
fun.
inline B_OT2((*G_R,*) PFE_A2).gen_query.
swap {1} 7 -5.
seq 2 2 : (x{1} = i1{2} /\ f{1}=i2{2}
          /\ OT.P.Game2.real{1} = Game2.real{2}
          /\ ={glob PFE_A2(*, glob G_R*), glob OT_R1, glob OT_R2, glob OT_S} ).
 wp; rnd; call (_: ={glob PFE_A2} ==> ={res, glob PFE_A2}); first by fun true; trivial.
 by skip; trivial.

case (validInputs i1{2} i2{2}); last first.
 rcondt {2} 1; first by intros &m; wp; skip; smt.
 rcondt {1} 6.
  intros &m; wp.
  rnd; trivial.
  wp; skip; progress; trivial.
  by apply PFE_validInputs_OT_n.
 rnd; wp; rnd {1}; wp; skip; smt.
(* validInputs *)
rcondf {2} 1; first by intros &m; wp; skip; trivial.
rcondf {1} 6.
 intros &m; wp; rnd (*call (_: true ==> true); first by fun true; trivial*).
 wp; skip; progress; trivial.
   cut Hrw: length i1{m} = G.Defs.inputLen.
    by apply (G.Defs.validInputsLen i2{m} i1{m}).
   by rewrite Hrw.
  by rewrite H2 /= G.Defs.inputKLen.
 apply allk_intro.
 cut ->: OT.OTdefs.inputsLen = G.Defs.ciphertextLen by trivial.
 rewrite H2 /= G.Defs.inputKLen => k Hk; beta.
 by apply (G.Defs.inputKLenC r i2{m} k).

inline PFE_R1(OT_R1).gen PFE_R2(OT_R2(*, G_R*)).gen PFE_S((*G_R,*) G_S, OT_S).sim2.
inline B_OT2((*G_R,*) PFE_A2).dist.
(*
swap {1} 9 -8; swap {2} 10 -9. 
seq 1 1 : (OTP.Game2.real{1} = PFE.Game2.real{2}
          /\ x{1} = i1{2}
          /\ f{1} = i2{2}
          /\ (PFE.validInputs i1{2} i2{2})
          /\ ={glob PFE_A2(*, glob G_R*), glob OT_R1, glob OT_R2, glob OT_S}).
 by rnd; skip; smt.
*)
swap {2} 6 -1; swap {2} [4..5] -3.
case (OT.P.Game2.real{1}).
 (* case: real = 1 *)
 rcondf {1} 10.
  intros &m.
  wp; call (_: true ==> true); first by fun true; trivial.
  wp; call (_: true ==> true); first by fun true; trivial.
  wp; rnd (*call (_: true ==> true); first by fun true; trivial*).
  by wp; skip; trivial.
 rcondf {2} 10.
  intros &m.
  wp; call (_: true ==> true); first by fun true; trivial.
  wp; call (_: true ==> true); first by fun true; trivial.
  wp; rnd (*call (_: true ==> true); first by fun true; trivial*).
  by wp; skip; trivial.
 wp; call (_: ={view, glob PFE_A2} ==> ={res}); first by fun true; trivial.
 wp; call (_: ={i2info, glob OT_R2} ==> ={res}); first  by fun true; trivial.
 wp; call (_: ={i1info, glob OT_R1} ==> ={res}); first  by fun true; trivial.
 wp; rnd (*call (_: ={finfo, glob G_R} ==> ={res}); first  by fun true; trivial*).
 wp; skip; progress; trivial.
 by rewrite H2 /=.
(* case: real = 0 *)
rcondt {1} 10.
 intros &m; wp.
 call (_: true ==> true); first by fun true; trivial.
 wp; call (_: true ==> true); first by fun true; trivial.
 wp; rnd (*call (_: true ==> true); first by fun true; trivial*).
 by wp; skip; trivial.
rcondt {2} 10.
 intros &m.
 wp; call (_: true ==> true); first by fun true; trivial.
 wp; call (_: true ==> true); first by fun true; trivial.
 wp; rnd (*call (_: true ==> true); first by fun true; trivial*).
 by wp; skip; trivial.
wp; call (_: ={view, glob PFE_A2} ==> ={res}); first by fun true; trivial.
wp; call (_: ={i2, o2, l1, glob OT_S} ==> ={res}); first  by fun true; trivial.
swap {2} 13 -2; swap {2} [10..11] -7.
wp. call (_: ={i2info, glob OT_R2} ==> ={res}); first  by fun true; trivial.
wp; call (_: ={i1info, glob OT_R1} ==> ={res}); first  by fun true; trivial.
wp; rnd (*call (_: ={finfo, glob G_R} ==> ={res}); first  by fun true; trivial*).
wp; rnd{2}; wp; skip; progress; trivial.
  by apply G.GR.drand_distr.
 by rewrite H2 /=.
by rewrite unitProp; apply eq_sym; rewrite unitProp.
save.

(* Main theorems *)

lemma real_abs_sum : forall (a b c:real),
 a = b + c => `|a| <= `|b| + `|c|
by smt.

lemma real_le_trans: forall (a b c:real), 
 a <= b => b <= c => a <= c
by [].

lemma PFE_Security_aux :
 (* OT_Correct *)
 OT.P.Correct () =>
 (* OT Security *)
 (  exists (epsilon:real) (R1 <: OT.P.Rand1_t {B_OT1, B_OT2, OT.P.Game1, OT.P.Game2, Game1, Game2}) (R2 <: OT.P.Rand2_t {R1, B_OT1, B_OT2, OT.P.Game1, OT.P.Game2, Game1, Game2})
         (S <: OT.P.Sim_t {R1, R2, B_OT1, B_OT2, B_G, Game1, Game2, OT.P.Game1, OT.P.Game2, G.SGame}),
  islossless R1.gen /\ islossless R2.gen /\
  islossless S.sim1 /\ islossless S.sim2 /\  
  forall (A1 <: OT.P.Adv1_t {R1, R2, S}) (A2 <: OT.P.Adv2_t {R1, R2, S}) &m,
   islossless A1.gen_query => islossless A1.dist =>
   islossless A2.gen_query => islossless A2.dist =>
    `|2%r * Pr[OT.P.Game1(R1,R2,A1,S).main()@ &m:res] - 1%r| <= epsilon
    /\ `|2%r * Pr[OT.P.Game2(R1,R2,A2,S).main()@ &m:res] - 1%r| <= epsilon
 ) =>
 (* Scheme Security *)
 (
 exists (epsilon:real) (*(R <: Rand_t)*) (S <: G.SSim_t {B_G, G.SGame, Game1}),
 islossless S.sim /\
 forall (A<:G.SAdv_t {S}) &m,
 islossless A.gen_query => islossless A.dist =>
   `|2%r * Pr[G.SGame((*R,*)S,A).main()@ &m:res] - 1%r| <= epsilon

 ) =>
 exists (epsilon : real) (OT_R1 <: OT.P.Rand1_t) (OT_R2 <: OT.P.Rand2_t {OT_R1})
        (*exists (RandG <: G.Rand_t)*) (OT_S <: OT.P.Sim_t {OT_R1, OT_R2}) (G_S <: G.SSim_t {G.SGame, Game1}),
 islossless OT_R1.gen /\ islossless OT_R2.gen /\
 islossless OT_S.sim1 /\ islossless OT_S.sim2 /\ islossless G_S.sim /\
 forall (PFE_A1<:Adv1_t {OT_R1, OT_R2, OT_S, G_S, OT.P.Game1, G.SGame, Game1, B_OT1, B_G}) (PFE_A2<:Adv2_t {OT_R1, OT_R2, OT_S, G_S, OT.P.Game2, Game2, B_OT2}) &m, 
  islossless PFE_A1.gen_query => islossless PFE_A1.dist =>
  islossless PFE_A2.gen_query => islossless PFE_A2.dist =>
   `|2%r * Pr[Game1(PFE_R1(OT_R1), PFE_R2(OT_R2), PFE_A1, PFE_S(G_S,OT_S)).main()@ &m:res] - 1%r| <= epsilon /\
   `|2%r * Pr[Game2(PFE_R1(OT_R1),PFE_R2(OT_R2),PFE_A2,PFE_S(G_S,OT_S)).main()@ &m:res] - 1%r| <= epsilon.   
proof.
intros OT_correct OT_Security Scheme_Security.
elim OT_Security => {OT_Security} OTepsilon OT_R1 OT_R2 OT_S H.
elim H => {H} OT_R1_ll H.
elim H => {H} OT_R2_ll H.
elim H => {H} OT_Ssim1_ll H.
elim H => {H} OT_Ssim2_ll OTsecurity.
elim Scheme_Security => {Scheme_Security} Gepsilon G_S H.
elim H => {H} G_S_ll Gsecurity.
exists (Gepsilon + OTepsilon), OT_R1, OT_R2, OT_S, G_S.
do (split; first by trivial).
intros PFE_A1 PFE_A2 &m PFE_A1gen_ll PFE_A1dist_ll PFE_A2gen_ll PFE_A2dist_ll.
cut PFE_R1_ll : islossless PFE_R1(OT_R1).gen.
 by apply (PFE_R1_ll OT_R1).
cut PFE_R2_ll : islossless PFE_R2(OT_R2).gen.
 by apply (PFE_R2_ll OT_R2).
cut B_OT1gen_ll : islossless B_OT1(PFE_A1).gen_query.
 by apply (B_OT1_gen_ll PFE_A1).
cut B_OT1dist_ll : islossless B_OT1(PFE_A1).dist.
 by apply (B_OT1_dist_ll PFE_A1).
cut B_Ggen_ll : islossless B_G(PFE_A1, OT_S).gen_query.
 by apply (B_G_gen_ll PFE_A1 OT_S).
cut B_Gdist_ll : islossless B_G(PFE_A1, OT_S).dist.
 by apply (B_G_dist_ll PFE_A1 OT_S).
cut B_OT2gen_ll : islossless B_OT2(PFE_A2).gen_query.
 by apply (B_OT2_gen_ll PFE_A2).
cut B_OT2dist_ll : islossless B_OT2(PFE_A2).dist.
 by apply (B_OT2_dist_ll PFE_A2).
cut PFE_Ssim1_ll : islossless PFE_S(G_S, OT_S).sim1.
 by apply (PFE_Ssim1_ll G_S OT_S).
cut PFE_Ssim2_ll : islossless PFE_S(G_S, OT_S).sim2.
 by apply (PFE_Ssim2_ll G_S OT_S).
elim (OTsecurity (B_OT1(PFE_A1)) (B_OT2(PFE_A2)) &m _ _ _ _); trivial
 => {OTsecurity} OTsec1 OTsec2.
cut Gsec : (`|2%r * Pr[G.SGame(G_S, B_G(PFE_A1, OT_S)).main() @ &m : res] - 1%r|
            <= Gepsilon).
 (* ??? como é que se pode cumprir com este requisito??? 
 by apply (Gsecurity (B_G(PFE_A1,OT_S)) &m). *)
 admit.
clear Gsecurity; split.
 (* Game1 *)
 apply (real_le_trans _ 
  (`|2%r * Pr[G.SGame(G_S, B_G(PFE_A1, OT_S)).main() @ &m : res] - 1%r|
  +`|2%r * Pr[OT.P.Game1(OT_R1, OT_R2, B_OT1(PFE_A1), OT_S).main() @ &m : res] - 1%r|));
 last by smt.
 apply real_abs_sum.
 rewrite (ADV1_xpand (PFE_R1(OT_R1)) (PFE_R2(OT_R2)) (PFE_A1) (PFE_S(G_S, OT_S)) &m) //.
 rewrite (OT.P.ADV1_xpand (OT_R1) (OT_R2) (B_OT1(PFE_A1)) (OT_S) &m) //.
 rewrite (G.ADV_xpand (B_G(PFE_A1, OT_S)) (G_S) &m) //.
 rewrite (Game1_real_pr (G_S) (OT_S) (OT_R1) (OT_R2) (PFE_A1) &m) //.
 rewrite (Game1_hybrid_pr (G_S) (OT_S) (OT_R1) (OT_R2) (PFE_A1) &m) //.
 rewrite (Game1_ideal_pr (G_S) (OT_S) (OT_R1) (OT_R2) (PFE_A1) &m) //.
 smt. 
(* Game2 *)
cut eqPr: ( Pr[ OT.P.Game2(OT_R1,OT_R2,B_OT2(PFE_A2),OT_S).main()@ &m:res ]
          = Pr[ Game2(PFE_R1(OT_R1),PFE_R2(OT_R2),PFE_A2, PFE_S(G_S,OT_S)).main()@ &m:res ]).
equiv_deno (Reduction2 (G_S) (PFE_A2) (OT_S) (OT_R1) (OT_R2)); trivial.
rewrite -eqPr.
apply (real_le_trans _ OTepsilon); first by assumption.
smt.
save.

lemma Security : 
 (* OT_Correct *)
 OT.P.Correct () =>
 (* OT Security *)
 (  exists (epsilon:real) (R1 <: OT.P.Rand1_t {B_OT1, B_OT2, OT.P.Game1, OT.P.Game2, Game1, Game2}) (R2 <: OT.P.Rand2_t {R1, B_OT1, B_OT2, OT.P.Game1, OT.P.Game2, Game1, Game2})
         (S <: OT.P.Sim_t {R1, R2, B_OT1, B_OT2, B_G, Game1, Game2, OT.P.Game1, OT.P.Game2, G.SGame}),
  islossless R1.gen /\ islossless R2.gen /\
  islossless S.sim1 /\ islossless S.sim2 /\  
  forall (A1 <: OT.P.Adv1_t {R1, R2, S}) (A2 <: OT.P.Adv2_t {R1, R2, S}) &m,
   islossless A1.gen_query => islossless A1.dist =>
   islossless A2.gen_query => islossless A2.dist =>
    `|2%r * Pr[OT.P.Game1(R1,R2,A1,S).main()@ &m:res] - 1%r| <= epsilon
    /\ `|2%r * Pr[OT.P.Game2(R1,R2,A2,S).main()@ &m:res] - 1%r| <= epsilon
 ) =>
 (* Scheme Security *)
 (
 exists (epsilon:real) (*(R <: Rand_t)*) (S <: G.SSim_t {B_G, G.SGame, Game1}),
 islossless S.sim /\
 forall (A<:G.SAdv_t {S}) &m,
 islossless A.gen_query => islossless A.dist =>
   `|2%r * Pr[G.SGame((*R,*)S,A).main()@ &m:res] - 1%r| <= epsilon

 ) =>
  exists (epsilon : real) (R1 <: P.Rand1_t) (R2 <: P.Rand2_t {R1})
   (S <: P.Sim_t {R1,R2}),
  islossless R1.gen /\
  islossless R2.gen /\
  islossless S.sim1 /\
  islossless S.sim2 /\
  forall (A1 <: P.Adv1_t {R1, R2, S}) (A2 <: P.Adv2_t {R1, R2, S}) &m,
    islossless A1.gen_query =>
    islossless A1.dist =>
    islossless A2.gen_query =>
    islossless A2.dist =>
    `|2%r * Pr[P.Game1(R1, R2, A1, S).main() @ &m : res] - 1%r| <= epsilon /\
    `|2%r * Pr[P.Game2(R1, R2, A2, S).main() @ &m : res] - 1%r| <= epsilon.
 proof.
intros OT_Correct OT_Security G_Security.
elim (PFE_Security_aux _ _ _) => // epsilon OT_R1 OT_R2 OT_S G_S H.
elim H => {H} OT_R1_ll H.
elim H => {H} OT_R2_ll H.
elim H => {H} OT_Ssim1_ll H.
elim H => {H} OT_Ssim2_ll H.
elim H => {H} G_S_ll Hsec.
(* MAIS UMA RESTRIÇÃO QUE NÃO SEI CUMPRIR!!!:
exists epsilon, (PFE_R1(OT_R1)), (PFE_R2(OT_R2)), (PFE_S(G_S,OT_S)).
split; first by apply (PFE_R1_ll OT_R1).
split; first by apply (PFE_R2_ll OT_R2).
split; first by apply (PFE_Ssim1_ll G_S OT_S).
split; first by apply (PFE_Ssim2_ll G_S OT_S).
intros PFE_A1 PFE_A2 &m PFE_A1gen_ll PFE_A1dist_ll PFE_A2gen_ll PFE_A2dist_ll.
by elim (Hsec (PFE_A1) (PFE_A2) &m _ _ _ _).
*) admit.
save.

end PFEprot.


