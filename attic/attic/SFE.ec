require import TwoPartyProtocol.
require import Scheme.
require import ObliviousTransfer.
require import Real.
require import Array.
require import Bitstring.
require import Int.
require import Bool.

op emptyOutput : Scheme.output. 
op emptyFunctG : Scheme.functG.
op emptyKeyO : Scheme.keyOutput.

clone Protocol as SFE with
   type PRandom1 = OT.PRandom1,
   type PInput1 = Scheme.input,
   type PInLen1 = OT.PInLen1,
   type PState1 = OT.PState1 * Scheme.functG * Scheme.keyOutput * Scheme.output,
   type POutput1 = Scheme.output,
   type PLeak1 = Scheme.tPhi,
   type PRandom2 = OT.PRandom2 * Scheme.random,
   type PInput2 = Scheme.funct,
   type PInLen2 = OT.PInLen2 * Scheme.functLen,
   type PState2 = OT.PState2,
   type POutput2 = unit,
   type PLeak2 = int,
   type PMsg = OT.PMsg * Scheme.functG * Scheme.keyOutput,
   type PTrace = PMsg * PMsg * PMsg,

   op lenI1(i1 : PInput1) = OT.lenI1 i1,
   op lenI2(i2 : PInput2) = (OT.lenI2 (Scheme.keyInputPattern i2),Scheme.funLen i2),

   op validInputs(i1 : PInput1, i2 : PInput2) = 
      Scheme.functCorrect i2 && Scheme.inputCorrect i2 i1,

   op initP1(i1 : PInput1 * PRandom1) = 
       let (x,r) = i1 in 
       let st0 = OT.initP1 (x,r) in
           (st0,emptyFunctG,emptyKeyO,emptyOutput),

   op transP1(msg_in : PMsg, st : PState1) = 
      let (ot_msg_in,fg_in,ko_in) = msg_in in
      let (sti,fg,ko,o) = st in
      let (ot_msg_out,st1) = OT.transP1 ot_msg_in sti in
          ((ot_msg_out,emptyFunctG,emptyKeyO), (st1,fg_in,ko_in,emptyOutput)),

   op haltP1(msg_in : PMsg, st : PState1)  = 
      let (ot_msg_in,fg_in,ko_in) = msg_in in
      let (sti,fg,ko,o) = st in
      let ot_out = OT.haltP1 ot_msg_in sti in
      let out1G = Scheme.evalG fg ot_out in
          Scheme.decode ko out1G,

    op phi1(in2 : PInput2) = 
       Scheme.phi in2,

   op initP2(i2 : PInput2 * PRandom2) = 
       let (f,r) = i2 in 
       let (r_ot,rG) = r in
       let (fg, ki, ko) = Scheme.garble rG f in
       let (msg0, st0) = OT.initP2 (ki,r_ot) in
           ((msg0,fg,ko),st0),

   op transP2(msg_in : PMsg, st : PState2) = 
      let (ot_msg_in,fg_in,ko_in) = msg_in in
      let (ot_msg_out,st1) = OT.transP2 ot_msg_in st in
          ((ot_msg_out,emptyFunctG,emptyKeyO),st1),

   op haltP2(st : PState2) = 
      OT.haltP2(st),

   op phi2(in1 : PInput1) = 
      OT.phi2(in1),

   op viewP(i1 : PInput1  * PRandom1, i2 : PInput2  * PRandom2) =
     let st10 = initP1 i1 in
     let (msg0,st20) = initP2 i2 in
     let (msg1,st11) = transP1 msg0 st10 in
     let (msg2,st21) = transP2 msg1 st20 in
     let o1 = haltP1 msg2 st11 in
     let o2 = haltP2 st21 in
     let trace = (msg0,msg1,msg2) in
     (trace, o1, o2).

module RandSFE1(RandOT : OT.Rand1_t) : SFE.Rand1_t = {
      fun gen(len_i1 : SFE.PInLen1) : SFE.PRandom1 = {
         var r : SFE.PRandom1;
         r = RandOT.gen(len_i1); 
         return r;
     }
}.

module RandSFE2(RandOT : OT.Rand2_t, RandG : Scheme.Rand_t) : SFE.Rand2_t = {
     fun gen(len_i2 : SFE.PInLen2) : SFE.PRandom2 = { 
         var r1 : OT.PRandom2;
         var r2 : Scheme.random;
         r1 = RandOT.gen(Pair.fst len_i2);
         r2 = RandG.gen(Pair.snd len_i2);
         return (r1,r2);
     }
}.

module B_OT1(RandG : Scheme.Rand_t, A : SFE.Adv1) : OT.Adv1 = {
    var g : Scheme.functG
    var d : Scheme.keyOutput
    var o : Scheme.output

    fun gen_query() : OT.PInput1 * OT.PInput2 = {
        var x : Scheme.input;
        var f : Scheme.funct;
        var y : Scheme.inputG;
        var e : Scheme.keyInput;
        var r : Scheme.random;

        (x,f) = A.gen_query();
        o = Scheme.eval f x;
        r = RandG.gen(Scheme.funLen f);
        (g,e,d) = Scheme.garble r f;
        return (x,e);    
    }

    fun dist(view : OT.PView1) : bool = {
        var viewP : SFE.PView1;
        var msg0 : SFE.PMsg;
        var msg1 : SFE.PMsg;
        var msg2 : SFE.PMsg;
        var ot_trace : OT.PTrace;
        var ot_msg0 : OT.PMsg;
        var ot_msg1 : OT.PMsg;
        var ot_msg2 : OT.PMsg;
        var ot_out1 : OT.POutput1;
        var trace : SFE.PTrace;
        var sim_view : SFE.PView1;
        var guess : bool;

        (ot_trace,ot_out1) = view;
        (ot_msg0,ot_msg1,ot_msg2) = ot_trace;
        
        msg0 = (ot_msg0,g,d);
        msg1 = (ot_msg1,emptyFunctG,emptyKeyO);
        msg2 = (ot_msg2,emptyFunctG,emptyKeyO);

        trace = (msg0,msg1,msg2);
        sim_view =  (trace,o);
        guess = A.dist(sim_view);
        
        return guess;
    }
}.

module B_G(A : SFE.Adv1, OTSim : OT.PSim) : Scheme.SIM_Adv = {
    var x : Scheme.input
    var f : Scheme.funct
    var o : Scheme.output

    fun gen_query() : Scheme.sim_query = {
        (x,f) = A.gen_query();
        o = Scheme.eval f x;
        return (f,x);
    }

    fun get_challenge(answer : Scheme.sim_answer) : bool = {
        var g : Scheme.functG;
        var xG : Scheme.inputG;
        var d : Scheme.keyOutput;
        var ki : Scheme.keyInput;
        var i : int;
        var msg0 : SFE.PMsg;
        var msg1 : SFE.PMsg;
        var msg2 : SFE.PMsg;
        var ot_msg0 : OT.PMsg;
        var ot_msg1 : OT.PMsg;
        var ot_msg2 : OT.PMsg;
        var ot_st10 : OT.PState1;
        var ot_st11 : OT.PState1;
        var ot_st20 : OT.PState2;
        var ot_st21 : OT.PState2;
        var ot_out1 : OT.POutput1;
        var ot_view1 : OT.PView1;
        var ot_trace : OT.PTrace;
        var trace : SFE.PTrace;
        var sim_view : SFE.PView1;
        var guess : bool;

        (g,xG,d) = answer;

        i = 0;
        while(i < Bits.length(x)) {
           if (x.[i]) {
              ki.[2*i] = zeros (length xG.[i]);
              ki.[2*i + 1] = xG.[i];
           } else {
              ki.[2*i] = xG.[i];
              ki.[2*i + 1] = zeros (length xG.[i]);
           }
           i = i + 1;
        } 

        ot_view1 = OTSim.sim1(x, xG, (OT.phi1 ki));
        (ot_trace,ot_out1) = ot_view1;
        (ot_msg0,ot_msg1,ot_msg2) = ot_trace;
        
        msg0 = (ot_msg0,g,d);
        msg1 = (ot_msg1,emptyFunctG,emptyKeyO);
        msg2 = (ot_msg2,emptyFunctG,emptyKeyO);

        trace = (msg0,msg1,msg2);
        sim_view =  (trace,o);

        guess = A.dist(sim_view); 
        
        return guess;
    }
}.

module B_OT2(RandG : Scheme.Rand_t, A : SFE.Adv2) : OT.Adv2 = {
    var g : Scheme.functG
    var d : Scheme.keyOutput
    var o : Scheme.output

    fun gen_query() : OT.PInput1 * OT.PInput2 = {
        var x : Scheme.input;
        var f : Scheme.funct;
        var y : Scheme.inputG;
        var e : Scheme.keyInput;
        var r : Scheme.random;

        (x,f) = A.gen_query();
        o = Scheme.eval f x;
        r = RandG.gen(Scheme.funLen f);
        (g,e,d) = Scheme.garble r f;
        return (x,e);    
    }

    fun dist(view : OT.PView2) : bool = {
        var viewP : SFE.PView2;
        var msg0 : SFE.PMsg;
        var msg1 : SFE.PMsg;
        var msg2 : SFE.PMsg;
        var ot_trace : OT.PTrace;
        var ot_msg0 : OT.PMsg;
        var ot_msg1 : OT.PMsg;
        var ot_msg2 : OT.PMsg;
        var ot_out2 : OT.POutput2;
        var trace : SFE.PTrace;
        var sim_view : SFE.PView2;
        var guess : bool;

        (ot_trace,ot_out2) = view;
        (ot_msg0,ot_msg1,ot_msg2) = ot_trace;
        
        msg0 = (ot_msg0,g,d);
        msg1 = (ot_msg1,emptyFunctG,emptyKeyO);
        msg2 = (ot_msg2,emptyFunctG,emptyKeyO);

        trace = (msg0,msg1,msg2);
        sim_view =  (trace,ot_out2);
        guess = A.dist(sim_view);
        
        return guess;
    }
}.

 module MainSim(RandG : Scheme.Rand_t, GSim : Scheme.SIM_Sim, OTSim : OT.PSim) : SFE.PSim = {
     fun sim1(I1 : SFE.PInput1, o1: SFE.POutput1, l1 : SFE.PLeak1) : SFE.PView1 = {
          var view : SFE.PView1;
          var g : Scheme.functG;
          var ki : Scheme.keyInput;
          var xG : Scheme.inputG;
          var d : Scheme.keyOutput;
          var ot_view1 : OT.PView1;
          var msg0 : SFE.PMsg;
          var msg1 : SFE.PMsg;
          var msg2 : SFE.PMsg;
          var ot_trace : OT.PTrace;
          var ot_msg0 : OT.PMsg;
          var ot_msg1 : OT.PMsg;
          var ot_msg2 : OT.PMsg;
          var ot_out1 : OT.POutput1;
          var trace : SFE.PTrace;
          var i : int;

          (g, xG, d) = GSim.simulate(o1,l1);

          i = 0;
          while(i < Bits.length(I1)) {
              if (I1.[i]) {
                ki.[2*i] = zeros (length xG.[i]);
                ki.[2*i + 1] = xG.[i];
              } else {
                ki.[2*i] = xG.[i];
                ki.[2*i + 1] = zeros (length xG.[i]);
              }
              i = i + 1;
          } 

          ot_view1 = OTSim.sim1(I1, xG, (OT.phi1 ki));
          (ot_trace,ot_out1) = ot_view1;
          (ot_msg0,ot_msg1,ot_msg2) = ot_trace;
        
          msg0 = (ot_msg0,g,d);
          msg1 = (ot_msg1,emptyFunctG,emptyKeyO);
          msg2 = (ot_msg2,emptyFunctG,emptyKeyO);

          trace = (msg0,msg1,msg2);
          view =  (trace,o1);

         return view;
     }

     fun sim2(I2 : SFE.PInput2, o2: SFE.POutput2, l2 : SFE.PLeak2) : SFE.PView2 = {
          var view : SFE.PView2;
          var ot_view2 : OT.PView2;
          var msg0 : SFE.PMsg;
          var msg1 : SFE.PMsg;
          var msg2 : SFE.PMsg;
          var ot_trace : OT.PTrace;
          var ot_msg0 : OT.PMsg;
          var ot_msg1 : OT.PMsg;
          var ot_msg2 : OT.PMsg;
          var ot_out2 : OT.POutput2;
          var trace : SFE.PTrace;
          var r : Scheme.random;
          var g : Scheme.functG;
          var e : Scheme.keyInput;
          var d : Scheme.keyOutput;

          r = RandG.gen(Scheme.funLen I2);
          (g,e,d) = Scheme.garble r I2;
          ot_view2 = OTSim.sim2(e, o2, l2);
         
          (ot_trace,ot_out2) = ot_view2;
          (ot_msg0,ot_msg1,ot_msg2) = ot_trace;
        
          msg0 = (ot_msg0,g,d);
          msg1 = (ot_msg1,emptyFunctG,emptyKeyO);
          msg2 = (ot_msg2,emptyFunctG,emptyKeyO);

          trace = (msg0,msg1,msg2);
          view =  (trace,o2);

         return view;
     }
  }.

lemma SFEcorrect: 
forall (x : SFE.PInput1, r1 : SFE.PRandom1, f : SFE.PInput2, r2 : SFE.PRandom2),
     forall (t : SFE.PTrace, o1 : SFE.POutput1, o2 : SFE.POutput2),
           SFE.validInputs x f = true =>
              (t,o1,o2) = SFE.viewP (x,r1) (f,r2) => 
                 o1 = Scheme.eval f x.
proof.
(*cut OTcorrectness: (
  forall (x : bitstring, r1 : OT.PRandom1, X : bitstring Array.array, r2 : OT.PRandom2,
       t : OT.PTrace, o1 : OT.POutput1, o2 : OT.POutput2),
           OT.validInputs x X = true =>
              (t,o1,o2) = OT.viewP (x,r1) (X,r2) => 
                  length o1 = Bits.length x /\ 
                     forall(i : int), 0 <= i /\ i < Bits.length x => 
                        if x.[i] then o1.[i] = X.[2*i+1] else o1.[i] = X.[2*i]).
admit.
cut SchemeCorrectness :(
    forall (f : Scheme.funct) , Scheme.functCorrect f =>
    forall (x : Scheme.input) , Scheme.inputCorrect f x =>
    forall (r : Scheme.random),
      let (g, ki, ko) = Scheme.garble r f in
      let xG = Scheme.encode ki x in
           Scheme.eval f x = Scheme.decode ko (Scheme.evalG g xG) /\
           (forall (xGp : Scheme.inputG),
              (Array.length xGp = length x /\ 
                 (forall(i : int), (0 <= i /\ i < Bits.length x) => 
                    xGp.[i] = xG.[i])) => Scheme.evalG g xG = Scheme.evalG g xGp)).
admit.
cut SchemeProjective : (
 forall (f : Scheme.funct) , Scheme.functCorrect f =>
    forall (x : Scheme.input) , Scheme.inputCorrect f x =>
    forall (r : Scheme.random), let (g, ki, ko) = Scheme.garble r f in
          Array.length ki = 2*length x /\ 
          map Bits.length (Scheme.keyInputPattern f) = map Bits.length ki /\
          let gi = Scheme.encode ki x in
              Array.length gi = Bits.length x /\ 
                 forall(i : int), 0 <= i /\ i < Bits.length x => 
                    if x.[i] then gi.[i] = ki.[2*i+1] else gi.[i] = ki.[2*i]).
admit (*smt*).
intros x r1 f r2 t o1 o2.
cut validInputsSFE : (forall (x : SFE.PInput1, f : SFE.PInput2),  
       SFE.validInputs x f = (Scheme.functCorrect f && Scheme.inputCorrect f x)).
smt.
cut SFEViewPDef : (forall (x : SFE.PInput1, r1 :SFE.PRandom1, f : SFE.PInput2, r2 : SFE.PRandom2),
    SFE.viewP (x,r1) (f,r2) =
     let st10 = SFE.initP1 (x,r1) in
     let (msg0,st20) = SFE.initP2 (f,r2) in
     let (msg1,st11) = SFE.transP1 msg0 st10 in
     let (msg2,st21) = SFE.transP2 msg1 st20 in
     let o10 = SFE.haltP1 msg2 st11 in
     let o20 = SFE.haltP2 st21 in
     let trace = (msg0,msg1,msg2) in
     (trace, o10, o20)).
smt.
cut SFEinitP1 : (forall (x : SFE.PInput1, r1 : SFE.PRandom1),
     SFE.initP1 (x,r1) = 
       let st0 = OT.initP1 (x,r1) in
           (st0,emptyFunctG,emptyKeyO,emptyOutput)).
admit.
cut SFEtransP1 : (forall (msg_in : SFE.PMsg, st : SFE.PState1), 
    SFE.transP1 msg_in st =
      let (ot_msg_in,fg_in,ko_in) = msg_in in
      let (sti,fg,ko,o) = st in
      let (ot_msg_out,st1) = OT.transP1 ot_msg_in sti in
          ((ot_msg_out,emptyFunctG,emptyKeyO), (st1,fg_in,ko_in,emptyOutput))).
admit.
cut SFEhaltP1 : (forall (msg_in : SFE.PMsg, st : SFE.PState1),  
     SFE.haltP1 msg_in st = 
      let (ot_msg_in,fg_in,ko_in) = msg_in in
      let (sti,fg,ko,o) = st in
      let ot_out = OT.haltP1 ot_msg_in sti in
      let out1G = Scheme.evalG fg ot_out in
          Scheme.decode ko out1G).
admit.
cut SFEinitP2 : (forall (f : SFE.PInput2, r2 : SFE.PRandom2),
     SFE.initP2 (f,r2) = 
       let (r_ot,rG) = r2 in
       let (fg, ki, ko) = Scheme.garble rG f in
       let (msg0, st0) = OT.initP2 (ki,r_ot) in
           ((msg0,fg,ko),st0)).
admit.
cut SFEtransP2 : (forall (msg_in : SFE.PMsg, st : SFE.PState2),
     SFE.transP2 msg_in st = 
      let (ot_msg_in,fg_in,ko_in) = msg_in in
      let (ot_msg_out,st1) = OT.transP2 ot_msg_in st in
          ((ot_msg_out,emptyFunctG,emptyKeyO),st1)).
admit.
cut SFEhaltP2 : (forall (st : SFE.PState2),
    SFE.haltP2 st = OT.haltP2(st)).
admit. 
rewrite (validInputsSFE). 
rewrite (SFEViewPDef).
rewrite (SFEinitP1).
rewrite (SFEinitP2).
rewrite (SFEtransP1).
rewrite (SFEtransP2).
rewrite (SFEhaltP1).
rewrite (SFEhaltP2). *)
intros x r1 f r2 t o1 o2 allIsSFEValid.
cut bridgeValid : (
    forall ( fg : Scheme.functG, X : Scheme.keyInput, ko : Scheme.keyOutput),
            (fg, X, ko) = (let (r_ot,rG) = r2 in Scheme.garble rG f) 
                => OT.validInputs x X
).
admit. (*works*)
cut bridgeViews : (
   forall (fg : Scheme.functG, X : Scheme.keyInput, ko : Scheme.keyOutput),
           (fg, X, ko) = (let (r_ot,rG) = r2 in Scheme.garble rG f) 
=> (let (r_ot, rG) = r2 in
    let (t_ot, ot_o1, ot_o2) = OT.viewP (x, r1) (X, r_ot) in
    let (ot_msg0,ot_msg1,ot_msg2) = t_ot in
    let (t,o1,o2) = SFE.viewP (x,r1) (f,r2) in
    let (msg0,msg1,msg2) = t in
    let (ot_msg_in0, fg_in0, ko_in0) = msg0 in
    let (ot_msg_in1, fg_in1, ko_in1) = msg1 in
    let (ot_msg_in2, fg_in2, ko_in2) = msg2 in
    ot_msg0 = ot_msg_in0 /\ 
    fg_in0 = fg /\ 
    ko_in0 = ko /\ 
    ot_msg1 = ot_msg_in1 /\ 
    ot_msg2 = ot_msg_in2)
    ).
timeout 10.
progress;admit. (* works *)
cut partial2 : forall ( fg : Scheme.functG, X : Scheme.keyInput, 
                      ko : Scheme.keyOutput),
((fg, X, ko) = let (r_ot,rG) = r2 in
       Scheme.garble rG f)
=>
(let (r_ot, rG) = r2 in
let (t_ot, ot_o1, ot_o2) = OT.viewP (x, r1) (X, r_ot) in
let xG = Scheme.encode X x in
    Scheme.evalG fg ot_o1 = Scheme.evalG fg xG).
cut partial0 : (
    forall ( fg : Scheme.functG, X : Scheme.keyInput, ko : Scheme.keyOutput),
      (fg, X, ko) = (let (r_ot,rG) = r2 in Scheme.garble rG f) 
        =>
        (let (r_ot, rG) = r2 in
         let (t_ot, ot_o1, ot_o2) = OT.viewP (x, r1) (X, r_ot) in
             ((Array.length ot_o1 = length x) /\
              (forall (i : int), (0 <= i /\ i < length x) =>
                   if x.[i] then ot_o1.[i] = X.[2 * i + 1]
                   else ot_o1.[i] = X.[2 * i])))               
       /\
       (let Xp = Scheme.encode X x in
            (Array.length Xp = Bits.length x) /\ 
            (forall(i : int), (0 <= i /\ i < Bits.length x) => 
                  if x.[i] then Xp.[i] = X.[2*i+1] else Xp.[i] = X.[2*i]))
).
timeout 30.
progress.
admit.
admit.
admit.
admit.
cut projectiveDef : (
    forall (f : Scheme.funct) , Scheme.functCorrect f =>
    forall (x : Scheme.input) , Scheme.inputCorrect f x =>
    forall (r : Scheme.random), let (g, ki, ko) = Scheme.garble r f in
          (Array.length ki = 2*length x) /\ 
          (map Bits.length (Scheme.keyInputPattern f) = map Bits.length ki) /\
          let gi = Scheme.encode ki x in
              (Array.length gi = Bits.length x) /\ 
                 (forall(i : int), (0 <= i /\ i < Bits.length x) => 
                    if x.[i] then gi.[i] = ki.[2*i+1] else gi.[i] = ki.[2*i])
).
admit.
timeout 30.
prover "Z3".
smt.
smt.
cut partial1 : (
 forall ( fg : Scheme.functG, X : Scheme.keyInput, ko : Scheme.keyOutput),
      (fg, X, ko) = (let (r_ot,rG) = r2 in Scheme.garble rG f)
      => (let (r_ot, rG) = r2 in
          let (t_ot, ot_o1, ot_o2) = OT.viewP (x, r1) (X, r_ot) in
          let xG = Scheme.encode X x in
              (Array.length ot_o1 = length x) /\ 
                 (forall(i : int), (0 <= i /\ i < Bits.length x) => 
                    Array.__get xG i = ot_o1.[i]))
).
prover "Alt-Ergo".
smt.
cut concrete2 : (
       let (g, ki, ko) = Scheme.garble (Pair.snd r2) f in
       let xG = Scheme.encode ki x in
         forall (xGp : Scheme.inputG),
            (Array.length xGp = length x) /\
               ((forall (i : int), (0 <= i /\ i < Bits.length x) => 
                    xGp.[i] = Array.__get xG i) =>
                           Scheme.evalG g xG = Scheme.evalG g xGp)
).
cut SchemeCorrect : (
 forall (f : Scheme.funct) , Scheme.functCorrect f =>
    forall (x : Scheme.input) , Scheme.inputCorrect f x =>
    forall (r : Scheme.random),
      let (g, ki, ko) = Scheme.garble r f in
      let xG = Scheme.encode ki x in
           Scheme.eval f x = Scheme.decode ko (Scheme.evalG g xG) /\
           (forall (xGp : Scheme.inputG),
              (Array.length xGp = length x /\ 
                 (forall(i : int), (0 <= i /\ i < Bits.length x) => 
                    xGp.[i] = xG.[i])) => Scheme.evalG g xG = Scheme.evalG g xGp)
).
prover "Z3".
timeout 30.
smt.
prover "Z3".
timeout 40.
smt.

admit. (* Work with previous *)
smt.
cut partial3 : forall ( fg : Scheme.functG, X : Scheme.keyInput, 
                      ko : Scheme.keyOutput),
((fg, X, ko) = let (r_ot,rG) = r2 in
       Scheme.garble rG f)
=>
(let (r_ot, rG) = r2 in
let (t_ot, ot_o1, ot_o2) = OT.viewP (x, r1) (X, r_ot) in
let xG = Scheme.encode X x in
    Scheme.decode ko (Scheme.evalG fg ot_o1) = Scheme.eval f x).
smt.
admit (* What is missing *)
save.

(* Lemmata for Party 1 simulation *)

lemma glueGame1_real :
  forall (GSim <: Scheme.SIM_Sim {SFE.Game1} ) (OTSim <: OT.PSim {OT.Game1, SFE.Game1}) (RandOT1 <: OT.Rand1_t {B_OT1}) 
         (RandOT2 <: OT.Rand2_t  {B_OT1, RandOT1})
         (Rand_g <: Scheme.Rand_t { RandOT1, RandOT2, B_OT1 })
         (SFE_A1 <: SFE.Adv1 {SFE.Game1, OT.Game1, Rand_g, B_OT1, RandOT1,RandOT2,OTSim,GSim }),
  islossless OTSim.sim1 =>
  islossless SFE_A1.dist =>
  islossless GSim.simulate =>
  islossless Rand_g.gen =>
  equiv 
  [ OT.Game1(OTSim,B_OT1(Rand_g, SFE_A1),RandOT1,RandOT2).main ~ 
    SFE.Game1(MainSim(Rand_g,GSim,OTSim),SFE_A1,RandSFE1(RandOT1),RandSFE2(RandOT2,Rand_g)).main
  : (glob SFE_A1){1} = (glob SFE_A1){2}
    /\ (glob Rand_g){1} = (glob Rand_g){2}  
     /\ (glob RandOT1){1} = (glob RandOT1){2}
        /\ (glob RandOT2){1} = (glob RandOT2){2}
      ==> OT.Game1.real{1} /\ SFE.Game1.real{2}  => OT.Game1.adv{1} = SFE.Game1.adv{2}
  ].
proof.
intros GSim OTSim RandOT1 RandOT2 Rand_g SFE_A1.
intros Sim1Lossless SFEA1DistLossless GSimSimulateLossless RandgLossless.
fun.
inline B_OT1(Rand_g, SFE_A1).gen_query.
seq 1 1 : (x{1} = i1{2} /\ f{1} = i2{2}
          /\ (glob Rand_g){1} = (glob Rand_g){2}
          /\ (glob RandOT1){1} = (glob RandOT1){2}
           /\ (glob RandOT2){1} = (glob RandOT2){2}
          /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
 call (_: (glob SFE_A1){1} = (glob SFE_A1){2} ==> ={res} /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
  by fun true; trivial.
 by  skip;trivial.
case ((SFE.validInputs x{1} f{1}) /\ (SFE.validInputs i1{2} i2{2})).
 rcondf {2} 1; first  by intros &m; skip;trivial.
 inline RandSFE1(RandOT1).gen.
 inline RandSFE2(RandOT2, Rand_g).gen.
 swap {2} 4 -3.
 swap {2} 6 -4.
 swap {1} 2 -1.
 seq 1 2 : (x{1} = i1{2} /\ f{1} = i2{2} /\ (SFE.validInputs x{1} f{1})
           /\ SFE.validInputs i1{2} i2{2} /\ r{1} = r20{2}
           /\ len_i2{2} = SFE.lenI2 i2{2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}  
           /\ (glob RandOT2){1} = (glob RandOT2){2}
           /\ (glob RandOT1){1} = (glob RandOT1){2}).
  call (_ : ={f_len} /\ (glob Rand_g){1} = (glob Rand_g){2} ==> ={res}).
   by fun true; trivial.
  wp; skip;smt.
 seq 3 1 : ((x{1} = i1{2} /\ f{1} = i2{2}) /\ (SFE.validInputs x{1} f{1})
           /\ SFE.validInputs i1{2} i2{2} /\ r{1} = r20{2}
           /\ B_OT1.o{1} = Scheme.eval f{1} x{1}
           /\ (i1{1}, i2{1}) = (x{1}, e{1})
           /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = (Scheme.garble r{1} f{1})
           /\ len_i1{2} = SFE.lenI1 i1{2}   
           /\ len_i2{2} = SFE.lenI2 i2{2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}  
           /\ (glob RandOT2){1} = (glob RandOT2){2}
           /\ (glob RandOT1){1} = (glob RandOT1){2}).
  wp; skip; smt.
 rcondf {1} 1.
  intros &m.
  skip.
  cut projective : (forall (f : Scheme.funct) , Scheme.functCorrect f =>
                        forall (x : Scheme.input) , Scheme.inputCorrect f x =>
                          forall (r : Scheme.random), let (g, ki, ko) = Scheme.garble r f in
                             Array.length ki = 2*length x). 
   smt.
  intros &hr.
  cut bridgeValids : ((B_OT1.g{hr}, e{hr}, B_OT1.d{hr}) = (Scheme.garble r{hr} f{hr}) /\ (i1{hr}, i2{hr}) = (x{hr}, e{hr}) /\ (SFE.validInputs x{hr} f{hr}) => Scheme.functCorrect f{hr} /\ Scheme.inputCorrect f{hr} x{hr} /\ (B_OT1.g{hr}, i2{hr}, B_OT1.d{hr}) = (Scheme.garble r{hr} f{hr})).
   smt.
  progress;smt.
 seq 1 1 : ((x{1} = i1{2} /\ f{1} = i2{2}) /\ (SFE.validInputs x{1} f{1})
           /\ SFE.validInputs i1{2} i2{2} /\ r{1} = r20{2}
           /\ B_OT1.o{1} = Scheme.eval f{1} x{1}
           /\ (i1{1}, i2{1}) = (x{1}, e{1})
           /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = (Scheme.garble r{1} f{1})
           /\ len_i1{2} = SFE.lenI1 i1{2}   
           /\ len_i2{2} = SFE.lenI2 i2{2}
           /\ r1{1} = r{2} 
           /\ (glob RandOT2){1} = (glob RandOT2){2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
  call (_ : ={len_i1} /\ (glob RandOT1){1} = (glob RandOT1){2} ==> ={res}).
   by fun true;trivial.
  by wp; skip;smt.
 seq 0 1 : ((x{1} = i1{2} /\ f{1} = i2{2}) /\ (SFE.validInputs x{1} f{1})
           /\ SFE.validInputs i1{2} i2{2} /\ r{1} = r20{2}
           /\ B_OT1.o{1} = Scheme.eval f{1} x{1}
           /\ (i1{1}, i2{1}) = (x{1}, e{1})
           /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = (Scheme.garble r{1} f{1})
           /\ len_i1{2} = SFE.lenI1 i1{2}   
           /\ len_i2{2} = SFE.lenI2 i2{2}
           /\ r1{1} = r{2} 
           /\ r1{2} = r{2}
           /\ (glob RandOT2){1} = (glob RandOT2){2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
  by wp;skip;trivial.
 seq 1 1 : ((x{1} = i1{2} /\ f{1} = i2{2}) /\ (SFE.validInputs x{1} f{1})
           /\ SFE.validInputs i1{2} i2{2} /\ r{1} = r20{2}
           /\ B_OT1.o{1} = Scheme.eval f{1} x{1}
           /\ (i1{1}, i2{1}) = (x{1}, e{1})
           /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = (Scheme.garble r{1} f{1})
           /\ len_i1{2} = SFE.lenI1 i1{2}   
           /\ len_i2{2} = SFE.lenI2 i2{2}
           /\ r1{1} = r{2} 
           /\ r1{2} = r{2}
           /\ r2{1} = r10{2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
  call (_ : ={len_i2} /\ (glob RandOT2){1} = (glob RandOT2){2} ==> ={res}).
   by fun true;trivial.
  by wp; skip;progress;smt.
 swap {1} 2 -1.
 swap {2} 3 -2.
 seq 1 1 : ((x{1} = i1{2} /\ f{1} = i2{2}) /\ (SFE.validInputs x{1} f{1})
           /\ SFE.validInputs i1{2} i2{2} /\ r{1} = r20{2}
           /\ B_OT1.o{1} = Scheme.eval f{1} x{1}
           /\ (i1{1}, i2{1}) = (x{1}, e{1})
           /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = (Scheme.garble r{1} f{1})
           /\ len_i1{2} = SFE.lenI1 i1{2}   
           /\ len_i2{2} = SFE.lenI2 i2{2}
           /\ r1{1} = r{2} 
           /\ r1{2} = r{2}
           /\ r2{1} = r10{2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
  rnd; skip; smt.
 case (OT.Game1.real{1} /\ SFE.Game1.real{2}).
  rcondf {1} 2; first by intros &m; wp; skip;trivial.
  rcondf {2} 3; first by intros &m; wp; skip;trivial.
  inline B_OT1(Rand_g, SFE_A1).dist.
  seq 9 2 : (sim_view{1} = view{2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
   conseq (_ : (forall (XX : Scheme.functG,
                        YY : Scheme.keyInput,
                        ZZ : Scheme.keyOutput), 
           (XX,YY,ZZ) = (Scheme.garble r20{2} i2{2}) => e{1} = YY)
           /\ (let (a,b) = SFE.viewP1 (i1{2}, r1{2}) (i2{2}, r2{2}) in b = B_OT1.o{1})
           /\ (x{1} = i1{2} /\ f{1} = i2{2}) /\ (SFE.validInputs x{1} f{1})
           /\ SFE.validInputs i1{2} i2{2} /\ r{1} = r20{2}
           /\ B_OT1.o{1} = Scheme.eval f{1} x{1}
           /\ (i1{1}, i2{1}) = (x{1}, e{1})
           /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = (Scheme.garble r{1} f{1})
           /\ len_i1{2} = SFE.lenI1 i1{2}   
           /\ len_i2{2} = SFE.lenI2 i2{2}
           /\ r1{1} = r{2} 
           /\ r1{2} = r{2}
           /\ r2{1} = r10{2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
    by progress;smt.
   timeout 20.
   by wp;skip;progress;smt.
  wp.
  call (_ : ={view} /\  (glob SFE_A1){1} = (glob SFE_A1){2} ==> ={res}).
   by fun true; trivial.
  skip.
  intros &1 &2 H1. 
  split; first by split; smt.
  intros H2; elim H2; clear H2; intros H2 H3 resL resR Hres Hreal.
  elim Hreal; clear Hreal; intros H4 H5.
  smt.
 seq 1 2 : (!(OT.Game1.real{1} /\ SFE.Game1.real{2}) /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
  wp;skip;smt.
 case (!OT.Game1.real{1} /\ SFE.Game1.real{2}).
  rcondt {1} 1; first by intros &m; skip; trivial.
  rcondf {2} 1; first by intros &m; skip; trivial.
  inline B_OT1(Rand_g, SFE_A1).dist.
  wp.
  seq 11 0 : (!OT.Game1.real{1} /\ SFE.Game1.real{2}).
   wp.
   seq 2 0 : (!OT.Game1.real{1} /\ SFE.Game1.real{2}).
    by wp;skip;smt.
   conseq (_: (!OT.Game1.real{1} /\ SFE.Game1.real{2})
              ==> (!OT.Game1.real{1} /\ SFE.Game1.real{2})).
   call {1} (_ : true ==> true); first by  apply Sim1Lossless.
   by skip; trivial. 
  call {1} (_ : true ==> true); first by apply SFEA1DistLossless. 
  call {2} (_ : true ==> true); first by apply SFEA1DistLossless. 
  skip; smt.
 conseq (_: ( (glob SFE_A1){1} = (glob SFE_A1){2}
              /\ !SFE.Game1.real{2})
            ==> (OT.Game1.real{1} /\ SFE.Game1.real{2}) 
                 => (OT.Game1.adv{1} = SFE.Game1.adv{2})).
  intros &1 &2;smt.
 rcondt {2} 1; first by intros &m;skip;trivial.
 inline MainSim(Rand_g, GSim, OTSim).sim1.
 seq 0 5 : ((glob SFE_A1){1} = (glob SFE_A1){2} /\ !SFE.Game1.real{2}).
  by wp;skip;trivial.
 seq 0 1 :  ((glob SFE_A1){1} = (glob SFE_A1){2} /\ !SFE.Game1.real{2}).
  call {2} (_ : true ==> true); first by apply GSimSimulateLossless.
  by skip;trivial.
 case (!OT.Game1.real{1}).
  rcondt {1} 1; first by intros &m;skip;trivial.
  seq 2 2 :  ((glob SFE_A1){1} = (glob SFE_A1){2} /\ !SFE.Game1.real{2}
             /\ !OT.Game1.real{1}).
   wp.
   seq 0 1 :  ((glob SFE_A1){1} = (glob SFE_A1){2} /\ !SFE.Game1.real{2}
              /\  !OT.Game1.real{1}).
    by wp;skip;trivial.
   while {2} ((glob SFE_A1){1} = (glob SFE_A1){2} /\ !SFE.Game1.real{2}
             /\ !OT.Game1.real{1}) (length I1{2}-i{2}).
    intros &m z; if; wp; skip; smt.
   skip;smt.
  inline B_OT1(Rand_g, SFE_A1).dist.
  seq 9 9 : (={glob SFE_A1} /\ ! SFE.Game1.real{2} /\ ! OT.Game1.real{1}).
   wp.
   call {1} (_ : true ==> true); first by  apply Sim1Lossless.
   call {2} (_ : true ==> true); first by  apply Sim1Lossless.
   by skip; trivial.
  wp; call {1} (_ : true ==> true); first by apply SFEA1DistLossless.
  call {2} (_ : true ==> true); first by  apply SFEA1DistLossless.
  skip;smt.
 rcondf {1} 1; first by intros &m;skip;smt.
 seq 0 1 : (={glob SFE_A1} /\ ! SFE.Game1.real{2} /\ OT.Game1.real{1}
           /\ i{2} = 0); first by wp;skip;smt.
 seq 0 9 : (={glob SFE_A1} /\ ! SFE.Game1.real{2} /\ OT.Game1.real{1}).
  seq 0 1 : (={glob SFE_A1} /\ ! SFE.Game1.real{2} /\ OT.Game1.real{1}).
   while {2} ((glob SFE_A1){1} = (glob SFE_A1){2} /\ !SFE.Game1.real{2}
              /\ OT.Game1.real{1}) (length I1{2}-i{2}).
    by intros &m z;  if; wp; skip; smt.
   skip;smt.
  wp; call {2} (_ : true ==> true); first by  apply Sim1Lossless.
  by wp;skip;trivial.
 inline B_OT1(Rand_g, SFE_A1).dist.
 seq 8 0 : (={glob SFE_A1} /\ ! SFE.Game1.real{2} /\ OT.Game1.real{1}).
  by wp;skip;trivial.
 wp; call {1} (_ : true ==> true); first by apply SFEA1DistLossless.
 call {2} (_ : true ==> true); first by apply SFEA1DistLossless.
 by wp;skip;smt.
case ((!SFE.validInputs x{1} f{1}) /\ (!SFE.validInputs i1{2} i2{2})).
 rcondt {2} 1; first by intros &m;wp;skip;smt.
 rcondt {1} 5.
  intros &m;wp; call (_ : true ==> true).
   by fun true; trivial.
  wp; skip; intros &hr.
  cut bridgeInvalids : (forall (result : Scheme.random),
   let (g, e, d) = 
    Scheme.garble result f{hr} in
    (!Scheme.functCorrect f{hr} => !(OT.validInputs x{hr} e)) /\
    (Scheme.functCorrect f{hr} => !Scheme.inputCorrect f{hr} x{hr}
     => !(OT.validInputs x{hr} e))).
   smt.
  smt.
 seq 4 0 : true.
  wp.
  call {1} (_ : true ==> true); first by apply RandgLossless.
  by wp; skip; trivial.
 by rnd;wp;skip;trivial.
conseq (_ : false /\ !(OT.validInputs i1{1} i2{1})
            /\ !(SFE.validInputs i1{1} i2{2}) ==> false).
 smt.
trivial.
save.

(* We know: |B_OT1 = 1 | real = 1 - B_OT1 = 0 | real = 0| < epsilon *)


lemma glueHybrid : 
 forall (GSim <: Scheme.SIM_Sim)
        (OTSim <: OT.PSim {B_G,  B_OT1, OT.Game1, Scheme.PrvSim})
        (RandOT1 <: OT.Rand1_t {OT.Game1}) (RandOT2 <: OT.Rand2_t {OT.Game1})
        (Rand_g <: Scheme.Rand_t {OT.Game1, OTSim, B_G, B_OT1, Scheme.PrvSim} )
        (SFE_A1 <: SFE.Adv1 {Scheme.PrvSim, OT.Game1, OTSim, Rand_g, B_OT1, B_G} ),
 islossless SFE_A1.dist =>
 islossless OTSim.sim1 =>
 islossless GSim.simulate =>
 equiv 
 [ OT.Game1(OTSim,B_OT1(Rand_g, SFE_A1),RandOT1,RandOT2).main
 ~ Scheme.SIM_Game(Scheme.PrvSim(Rand_g,GSim), B_G(SFE_A1, OTSim)).main
 : (glob SFE_A1){1} = (glob SFE_A1){2}
   /\ (glob Rand_g){1} = (glob Rand_g){2}
   /\ (glob OTSim){1} = (glob OTSim){2}
 ==> !OT.Game1.real{1} /\ Scheme.PrvSim.b{2}  => res{1} = res{2}
 ].
proof.
intros GSim OTSim RandOT1 RandOT2 Rand_g SFE_A1.
intros SFEA1_lossless OTSim_sim1_lossless GSim_simulate_lossless.
fun.
inline B_OT1(Rand_g, SFE_A1).gen_query.
inline B_OT1(Rand_g, SFE_A1).dist.
inline B_G(SFE_A1, OTSim).gen_query.
inline Scheme.PrvSim(Rand_g, GSim).garb.
inline Scheme.PrvSim(Rand_g, GSim).get_challenge.
inline B_G(SFE_A1, OTSim).get_challenge.
seq 1 1 : (x{1} = B_G.x{2} /\ f{1} = B_G.f{2}
          /\ (glob Rand_g){1} = (glob Rand_g){2}
          /\ (glob SFE_A1){1} = (glob SFE_A1){2}
          /\ (glob OTSim){1} = (glob OTSim){2}).
 call (_:(glob SFE_A1){1} = (glob SFE_A1){2} ==> ={res} /\ ( (glob SFE_A1){1} = (glob SFE_A1){2})).
  fun true; by trivial.
 skip; progress; trivial.

case ((SFE.validInputs x{1} f{1}) /\ (SFE.validInputs B_G.x{2} B_G.f{2})).
 rcondt {2} 3.
  intros &m; wp;skip; smt.
 rcondf {1} 5.
  intros &m; wp.
  call (_:true ==> true).
   fun true; by trivial.
  wp; skip. 
  admit (* That SFE valie inputs implies OT valid inputs *).
 swap {1} 8 -7.
 swap {2} 6 -5.
 seq 1 1 : (x{1} = B_G.x{2} /\ f{1} = B_G.f{2}
           /\ !OT.Game1.real{1} = Scheme.PrvSim.b{2}
           /\ (glob Rand_g){1} = (glob Rand_g){2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}
           /\ (glob OTSim){1} = (glob OTSim){2}).
  rnd (lambda z, !z) (lambda z, !z); skip; smt.
 seq 2 6 : (x{1} = B_G.x{2} /\ f{1} = B_G.f{2}
           /\ !OT.Game1.real{1} = Scheme.PrvSim.b{2}
           /\ f{2} = B_G.f{2} /\ x{2} = B_G.x{2}
           /\ r{1} = r{2} /\ B_OT1.o{1} = B_G.o{2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}
           /\ (glob OTSim){1} = (glob OTSim){2}).
  call (_: ={f_len} /\ (glob Rand_g){1} = (glob Rand_g){2} ==> ={res}).
   fun true; by trivial.
  wp; skip; by progress.
 seq 2 2 :  (x{1} = B_G.x{2} /\ f{1} = B_G.f{2} /\ f{2} = B_G.f{2}
            /\ !OT.Game1.real{1} = Scheme.PrvSim.b{2}
            /\ x{2} = B_G.x{2} /\ r{1} = r{2}
            /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = Scheme.garble r{1} f{1}
            /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
            /\ xG{2} = Scheme.encode e{2} x{2} /\ i1{1} = x{1}
            /\ i2{1} = e{1}
            /\ (glob SFE_A1){1} = (glob SFE_A1){2}
            /\ (glob OTSim){1} = (glob OTSim){2}).
  wp; skip; by trivial.
 seq 3 0 : (x{1} = B_G.x{2} /\ f{1} = B_G.f{2} /\ f{2} = B_G.f{2}
           /\ !OT.Game1.real{1} = Scheme.PrvSim.b{2}
           /\ x{2} = B_G.x{2} /\ r{1} = r{2} /\ B_OT1.o{1} = B_G.o{2}
           /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = Scheme.garble r{1} f{1}
           /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
           /\ xG{2} = Scheme.encode e{2} x{2}/\ i1{1} = x{1}
           /\ i2{1} = e{1} /\ Pair.snd view{1} = Scheme.encode e{1} x{1}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}
           /\ (glob OTSim){1} = (glob OTSim){2}).
  wp.
  admit (* One sided call + property of OT correctness and Scheme.encode *).
 case (!OT.Game1.real{1}). (* /\ Scheme.PrvSim.b{2}).*)
  rcondt {1} 1; first by intros &m; skip; trivial.
  rcondt {2} 1; first by intros &m; skip; smt.

  seq 2 7 :  (x{1} = B_G.x{2} /\ f{1} = B_G.f{2} /\ f{2} = B_G.f{2}
             /\ OT.Game1.real{1} =  Scheme.SIM_Game.real{2}
             /\ x{2} = B_G.x{2} /\ r{1} = r{2} /\ B_OT1.o{1} = B_G.o{2}
             /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = Scheme.garble r{1} f{1}
             /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
             /\ xG{2} = Scheme.encode e{2} x{2}/\ i1{1} = x{1}
             /\ i2{1} = e{1} /\ Pair.snd view{1} = Scheme.encode e{1} x{1}
             /\ (g0{2}, xG0{2}, d0{2}) = (g{2}, xG{2}, d{2})
             /\ l1{1} = OT.phi1 ki{2}
             /\ ={glob SFE_A1, glob OTSim} ).
   admit (* one-sided while + all lengths of ki{2} are same as i2{1} *).
  seq 1 1 :  (x{1} = B_G.x{2} /\ f{1} = B_G.f{2} /\ f{2} = B_G.f{2}
             /\ OT.Game1.real{1} =  Scheme.SIM_Game.real{2}
             /\ x{2} = B_G.x{2} /\ r{1} = r{2} /\ B_OT1.o{1} = B_G.o{2}
             /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = Scheme.garble r{1} f{1}
             /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
             /\ xG{2} = Scheme.encode e{2} x{2}/\ i1{1} = x{1}
             /\ i2{1} = e{1} /\ Pair.snd view{1} = Scheme.encode e{1} x{1}
             /\ (g0{2}, xG0{2}, d0{2}) = (g{2}, xG{2}, d{2})
             /\ l1{1} = OT.phi1 ki{2} /\ view{1} = ot_view1{2}
             /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
   call (_: ={I1, o1, l1, glob OTSim} ==> ={res}).
    fun true; by trivial.
   skip; progress. 
    admit (* o1{1} = encode(e{2}, B_G.x{2}) *).
    smt (* (B_OT1.g{1}, e{1}, B_OT1.d{1}) = garble(r{2}, f{2}) *).
    smt (* (g{2}, e{2}, d{2}) = garble(r{2}, f{2}) *).
    admit (* snd(result_R) = encode (e{1}, x{2}) *).
  seq 8 7 : (sim_view{1} = sim_view{2}
            /\ x{1} = B_G.x{2} /\ f{1} = B_G.f{2} /\ f{2} = B_G.f{2}
            /\ OT.Game1.real{1} = Scheme.SIM_Game.real{2}
            /\ x{2} = B_G.x{2} /\ r{1} = r{2} /\ B_OT1.o{1} = B_G.o{2}
            /\ (B_OT1.g{1}, e{1}, B_OT1.d{1}) = Scheme.garble r{1} f{1}
            /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
            /\ xG{2} = Scheme.encode e{2} x{2}/\ i1{1} = x{1}
            /\ i2{1} = e{1} /\ Pair.snd view{1} = Scheme.encode e{1} x{1}
            /\ (g0{2}, xG0{2}, d0{2}) = (g{2}, xG{2}, d{2})
            /\ l1{1} = OT.phi1 ki{2} /\ view{1} = ot_view1{2}
            /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
   wp; skip; smt.
  wp; call (_: ={view, glob SFE_A1} ==> ={res}).
   fun true; by trivial.
  skip; progress; smt.

 (* case: OT.Game1.real{1} /\ !Scheme.PrvSim.b{2}) *)
 rcondf {1} 1; first by intros &m; skip; trivial.
 rcondf {2} 1; first by intros &m; skip; smt.
 conseq (_ : _ ==> OT.Game1.real{1}); first by smt.
 wp; call {1} (_: true ==> true); first by apply SFEA1_lossless.
 call {2} (_: true ==> true); first by apply SFEA1_lossless.
 wp; conseq (_ : _ ==> OT.Game1.real{1}).
 call {2} (_: true ==> true); first by apply OTSim_sim1_lossless.
 while {2} true (length B_G.x{2}-i{2}).
  by intros &m z; if; wp; skip; smt.
 wp; call {2} (_: true ==> true); first by apply GSim_simulate_lossless.
 wp; skip; smt.

(* !(SFE.validInputs x{1} f{1} /\ SFE.validInputs B_G.x{2} B_G.f{2}) *)
exfalso. 
admit. (* prove pre-condition is contradiction due to valid inputs. *)
save.


(* We know: | B_G = 1 | b = 1 - B_G = 1 | b = 0| < epsilon_g *)


lemma glue_Game1_ideal : 
 forall (GSim <: Scheme.SIM_Sim { B_G, Scheme.PrvSim, SFE.Game1})
        (OTSim <: OT.PSim {SFE.Game1,Scheme.PrvSim,Scheme.SIM_Game,B_G})
        (RandOT1 <: OT.Rand1_t {SFE.Game1})
        (RandOT2 <: OT.Rand2_t {SFE.Game1, RandOT1} )
        (Rand_g <: Scheme.Rand_t { OTSim, GSim,SFE.Game1, RandOT1, RandOT2, B_G, Scheme.PrvSim})
        (SFE_A1 <: SFE.Adv1 {Scheme.PrvSim, B_G, SFE.Game1,Scheme.SIM_Game, Rand_g }),
 equiv
 [ SFE.Game1(MainSim(Rand_g,GSim,OTSim),SFE_A1,RandSFE1(RandOT1),RandSFE2(RandOT2,Rand_g)).main
 ~ Scheme.SIM_Game(Scheme.PrvSim(Rand_g,GSim), B_G(SFE_A1,OTSim)).main
 : (glob SFE_A1){1} = (glob SFE_A1){2} /\ (glob Rand_g){1} = (glob Rand_g){2}
 ==> !SFE.Game1.real{1} && !Scheme.PrvSim.b{2} => res{1} = res{2}
 ].
proof.
intros GSim OTSim RandOT1 RandOT2 Rand_g SFE_A1.
fun.
inline RandSFE1(RandOT1).gen.
inline RandSFE2(RandOT2, Rand_g).gen.
inline MainSim(Rand_g, GSim, OTSim).sim1.
inline B_G(SFE_A1, OTSim).gen_query.
inline Scheme.PrvSim(Rand_g, GSim).garb.
inline Scheme.PrvSim(Rand_g, GSim).get_challenge.
inline B_G(SFE_A1, OTSim).get_challenge.
seq 1 1 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
          /\ (glob Rand_g){1} = (glob Rand_g){2}
          /\ (glob SFE_A1){1} = (glob SFE_A1){2}
          /\ (glob GSim){1} = (glob GSim){2}
          /\ (glob OTSim){1} = (glob OTSim){2}).
 admit
 (* porque é que agora isto não dá???
 call (_: ={glob SFE_A1} ==> ={res, glob SFE_A1}). 
      (={res} /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
  fun true; by trivial.
 skip; by trivial.
 *).
case ((SFE.validInputs i1{1} i2{1}) /\ (SFE.validInputs B_G.x{2} B_G.f{2})).
 rcondf {1} 1; first by intros &m; wp;skip; trivial.
 seq 0 2 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
           /\ (SFE.validInputs i1{1} i2{1})
           /\ (SFE.validInputs B_G.x{2} B_G.f{2})
           /\ B_G.o{2} = Scheme.eval B_G.f{2} B_G.x{2}
           /\ query{2} = (B_G.f{2}, B_G.x{2}) 
           /\ (glob Rand_g){1} = (glob Rand_g){2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}
           /\ (glob GSim){1} = (glob GSim){2}
           /\ (glob OTSim){1} = (glob OTSim){2}).
  wp; skip; smt.
 rcondt {2} 1; first by intros &m; wp; skip; smt.
 swap {1} 9 -8.
 swap {2} 4 -3.
 seq 1 1 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
           /\ (SFE.validInputs i1{1} i2{1})
           /\ (SFE.validInputs B_G.x{2} B_G.f{2})
           /\ B_G.o{2} = Scheme.eval B_G.f{2} B_G.x{2}
           /\ query{2} = (B_G.f{2}, B_G.x{2})
           /\ (glob Rand_g){1} = (glob Rand_g){2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}
           /\ (glob GSim){1} = (glob GSim){2}
           /\ (glob OTSim){1} = (glob OTSim){2}).
  rnd; skip; smt.
 swap {1} 4 -3.
 swap {1} 6 -4.
 seq 1 3 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
           /\ len_i2{1} = SFE.lenI2 i2{1}
           /\ (SFE.validInputs i1{1} i2{1})
           /\ (SFE.validInputs B_G.x{2} B_G.f{2})
           /\ B_G.o{2} = Scheme.eval B_G.f{2} B_G.x{2}
           /\ query{2} = (B_G.f{2}, B_G.x{2})
           /\ query0{2} = query{2} /\ f{2} = Pair.fst query0{2}
           /\ x{2} = Pair.snd query0{2} 
           /\ (glob Rand_g){1} = (glob Rand_g){2}
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}
           /\ (glob GSim){1} = (glob GSim){2}
           /\ (glob OTSim){1} = (glob OTSim){2}).
  wp; skip; smt.
 seq 1 1 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
           /\ len_i2{1} = SFE.lenI2 i2{1}
           /\ (SFE.validInputs i1{1} i2{1})
           /\ (SFE.validInputs B_G.x{2} B_G.f{2})
           /\ B_G.o{2} = Scheme.eval B_G.f{2} B_G.x{2}
           /\ query{2} = (B_G.f{2}, B_G.x{2}) /\ query0{2} = query{2}
           /\ f{2} = Pair.fst query0{2} /\ x{2} = Pair.snd query0{2}
           /\ r20{1} = r{2} 
           /\ (glob SFE_A1){1} = (glob SFE_A1){2}
           /\ (glob GSim){1} = (glob GSim){2}
           /\ (glob OTSim){1} = (glob OTSim){2}).
  call (_: ={f_len, glob Rand_g} ==> ={res}).
   fun true; by trivial.
  skip; progress; smt.
 case (!SFE.Game1.real{1} && !Scheme.PrvSim.b{2}).
  rcondf {2} 3; first by intros &m; wp; skip; smt.
  seq 6 3 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
            /\ len_i2{1} = SFE.lenI2 i2{1}
            /\ (SFE.validInputs i1{1} i2{1})
            /\ (SFE.validInputs B_G.x{2} B_G.f{2})
            /\ B_G.o{2} = Scheme.eval B_G.f{2} B_G.x{2}
            /\ query{2} = (B_G.f{2}, B_G.x{2})
            /\ query0{2} = query{2} /\ f{2} = Pair.fst query0{2}
            /\ x{2} = Pair.snd query0{2} /\ r20{1} = r{2} 
            /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
            /\ xG{2} = Scheme.encode e{2} x{2}
            /\ y{2} = Scheme.eval f{2} x{2} /\ len_i1{1} = SFE.lenI1 i1{1}
            /\ Pair.snd view{1} = Scheme.eval i2{1} i1{1}
            /\ !SFE.Game1.real{1} && !Scheme.PrvSim.b{2}
            /\ (glob SFE_A1){1} = (glob SFE_A1){2}
            /\ (glob GSim){1} = (glob GSim){2}
            /\ (glob OTSim){1} = (glob OTSim){2}).
   admit. (* One sided call + property of SFE correctness and Scheme.eval -> 
          this should be changed in game *)
  rcondt {1} 1; first by intros &m; skip; trivial.
  seq 7 6 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
            /\ len_i1{1} = SFE.lenI1 i1{1}
            /\ len_i2{1} = SFE.lenI2 i2{1}
            /\ (SFE.validInputs i1{1} i2{1})
            /\ (SFE.validInputs B_G.x{2} B_G.f{2})
            /\ B_G.o{2} = Scheme.eval B_G.f{2} B_G.x{2}
            /\ query{2} = (B_G.f{2}, B_G.x{2})
            /\ query0{2} = query{2} /\ f{2} = Pair.fst query0{2}
            /\ x{2} = Pair.snd query0{2} /\ r20{1} = r{2}
            /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
            /\ xG{2} = Scheme.encode e{2} x{2}
            /\ y{2} = Scheme.eval f{2} x{2}
            /\ Pair.snd view{1} = Scheme.eval i2{1} i1{1}
            /\ !SFE.Game1.real{1} && !Scheme.PrvSim.b{2}
            /\ (g{1}, xG{1}, d{1}) = (g0{2}, xG0{2}, d0{2})
            /\ i{2} = 0 /\ I1{1} = i1{1}
            /\ o1{1} = Scheme.eval i2{1} i1{1} /\ o10{1} = o1{1}
            /\ (glob SFE_A1){1} = (glob SFE_A1){2}
            /\ (glob OTSim){1} = (glob OTSim){2}).
   wp; call (_: ={o, leak, glob GSim} ==> ={res}).
    fun true; by trivial.
   wp; skip; progress; smt.
  seq 1 1 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
            /\ len_i1{1} = SFE.lenI1 i1{1}
            /\ len_i2{1} = SFE.lenI2 i2{1}
            /\ (SFE.validInputs i1{1} i2{1})
            /\ (SFE.validInputs B_G.x{2} B_G.f{2})
            /\ B_G.o{2} = Scheme.eval B_G.f{2} B_G.x{2}
            /\ query{2} = (B_G.f{2}, B_G.x{2}) /\ query0{2} = query{2}
            /\ f{2} = Pair.fst query0{2} /\ x{2} = Pair.snd query0{2}
            /\ r20{1} = r{2} 
            /\  (glob Rand_g){1} = (glob Rand_g){2}
            /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
            /\ xG{2} = Scheme.encode e{2} x{2}
            /\ y{2} = Scheme.eval f{2} x{2}
            /\ Pair.snd view{1} = Scheme.eval i2{1} i1{1}
            /\ !SFE.Game1.real{1} && !Scheme.PrvSim.b{2}
            /\ (g{1}, xG{1}, d{1}) = (g0{2}, xG0{2}, d0{2})
            (*/\ i3{1} = 0*) /\ i{2} = 0 /\ I1{1} = i1{1} 
            /\ o1{1} = Scheme.eval i2{1} i1{1} 
            /\ o10{1} = o1{1}
            /\ (forall (k:int), 0<=k && k < length B_G.x{2}
                 => length ki{1}.[2*k] = length ki{2}.[2*k])
            /\ (glob SFE_A1){1} = (glob SFE_A1){2}
            /\ (glob OTSim){1} = (glob OTSim){2}).
   while ((*i3{1} = i{2} /\*)
          (forall (k:int), 0<=k && k < i{2}
            => length ki{1}.[2*k] = length ki{2}.[2*k]
              /\ length ki{1}.[2*k+1] = length ki{2}.[2*k+1])
          /\ i{2} <= length B_G.x{2}).
    admit.    (*That this is boring *)
   admit. (*That this is boring *)
  seq 1 1 : (i1{1} = B_G.x{2} /\ i2{1} = B_G.f{2}
            /\ SFE.validInputs i1{1} i2{1}
            /\ SFE.validInputs B_G.x{2} B_G.f{2}
            /\ B_G.o{2} = Scheme.eval B_G.f{2} B_G.x{2}
            /\ query{2} = (B_G.f{2}, B_G.x{2})
            /\ query0{2} = query{2} /\ f{2} = Pair.fst query0{2}
            /\ x{2} = Pair.snd query0{2} /\ r20{1} = r{2}
            /\ (g{2}, e{2}, d{2}) = Scheme.garble r{2} f{2}
            /\ xG{2} = Scheme.encode e{2} x{2}
            /\ y{2} = Scheme.eval f{2} x{2}
            /\ Pair.snd view{1} = Scheme.eval i2{1} i1{1}
            /\ !SFE.Game1.real{1} && !Scheme.PrvSim.b{2}
            /\ (g{1}, xG{1}, d{1}) = (g0{2}, xG0{2}, d0{2})
            /\ i{2} = 0  /\ I1{1} = i1{1}
            /\ o1{1} = Scheme.eval i2{1} i1{1}
            /\ o10{1} = o1{1} /\ ot_view1{1} = ot_view1{2}
            /\ (glob SFE_A1){1} = (glob SFE_A1){2}).
   call (_: ={I1, o1, l1, glob OTSim} ==> ={res}).
    fun true; by trivial.
   skip; progress; try smt.
   admit (* phi1(ki{1}) = phi1(ki{2}) *).

  wp; call (_: ={view, glob SFE_A1} ==> ={res}).
   fun true; by trivial.
  wp; skip; progress.
  admit (* real{2} = false *).

 admit. (* No changes here *)
 
admit. (* prove pre-condition is contradiction due to valid inputs. *)
save. 


(* Lemma for Party 2 simulation *)

op proj1_3 (p:'a * 'b * 'c): 'a = 
  let (a,b,c) = p in a.
op proj2_3 (p:'a * 'b * 'c): 'b = 
  let (a,b,c) = p in b.
op proj3_3 (p:'a * 'b * 'c): 'c = 
  let (a,b,c) = p in c.

import Scheme.

lemma Reduction2 : 
 forall (Rand_g <: Scheme.Rand_t{OT.Game2, SFE.Game2})
        (GSim <: Scheme.SIM_Sim) 
        (SFE_A2 <: SFE.Adv2 {Rand_g, B_OT2, SFE.Game2, OT.Game2} )
        (OTSim <: OT.PSim {B_OT2, SFE_A2, B_OT2(Rand_g, SFE_A2), Rand_g} )    
        (RandOT1 <: OT.Rand1_t {B_OT2(*(Rand_g, SFE_A2)*), OT.Game2, SFE.Game2})
        (RandOT2 <: OT.Rand2_t {B_OT2(*(Rand_g, SFE_A2)*), OTSim, SFE_A2, OT.Game2, SFE.Game2}),
 islossless Rand_g.gen =>
 equiv 
 [ OT.Game2 ( OTSim
            , B_OT2(Rand_g, SFE_A2)
            , RandOT1,RandOT2
            ).main
 ~ SFE.Game2 ( MainSim(Rand_g,GSim,OTSim)
             , SFE_A2,RandSFE1(RandOT1)
             , RandSFE2(RandOT2,Rand_g)
             ).main 
 : ={glob SFE_A2} ==> ={res}
 ].
proof.
intros Rand_G Sim_G  Adv2_G Sim_OT Rand1_OT Rand2_OT.
intros RandG_lossless.
fun.
inline B_OT2(Rand_G, Adv2_G).gen_query.
seq 1 1 : (x{1} = i1{2} /\ f{1}=i2{2}
          /\ ={glob Adv2_G} ).

 call (_: ={glob Adv2_G} ==> ={res, glob Adv2_G}).
  by fun true; trivial.
 by skip; intros &1 &2 H; split; trivial.

seq 1 0 : (x{1} = i1{2} /\ f{1} = i2{2}
           /\ B_OT2.o{1} = eval f{1} x{1}
           /\ ={glob Adv2_G} ).
 by wp; skip; trivial.

case (!(SFE.validInputs i1{2} i2{2})).
 rcondt {2} 1; first by intros &m; wp; skip; smt.
 seq 3 0 : (={i1} /\ i2{1}= proj2_3(garble r{1} f{1})
           /\ !(SFE.validInputs i1{2} i2{2})
           /\ ={glob Adv2_G} ).
  wp; call {1} (_: true ==> true); first assumption.
  skip; by progress.
 rcondt {1} 1.
  intros &m; wp; skip. 
  admit (* !(SFE.validInputs i1 i2 => !OT.validInputs i1 i2) *).
 (* isto é estranho, porque real{1,2} não é inicalizado... mas por casos
     até se prova. *)
 case (OT.Game2.real{1}).
  case (SFE.Game2.real{2}).
   rnd; skip; smt.
  rnd (lambda z, !z) (lambda z, !z); skip; smt.
  case (SFE.Game2.real{2}).
   rnd (lambda z, !z) (lambda z, !z); skip; smt.
  rnd; skip; smt.

rcondf {2} 1; first by intros &m; wp; skip; smt.
rcondf {1} 4.
 intros &m; wp; call (_: true ==> true).
  by fun true; trivial.
 skip; progress.
 admit (* (OT.validInputs i1{m} x2)%OT *).

inline RandSFE1(Rand1_OT).gen.
inline RandSFE2(Rand2_OT, Rand_G).gen.
inline MainSim(Rand_G, Sim_G, Sim_OT).sim2.
inline B_OT2(Rand_G, Adv2_G).dist.

swap {1} 7 -6.
swap {2} 9 -8.
seq 1 1 : (OT.Game2.real{1} = SFE.Game2.real{2}
          /\ x{1} = i1{2}
          /\ f{1} = i2{2}
          /\ (SFE.validInputs i1{2} i2{2})
          /\ ={glob Adv2_G}).
 rnd; skip; smt.

case (OT.Game2.real{1}).
 (* case: real = 1 *)
 rcondf {1} 7.
  intros &m; wp.
  call (_: true ==> true); first by fun true; progress.
  call (_: true ==> true); first by fun true; progress.
  wp; call (_: true => true).
  skip; progress; assumption.
 rcondf {2} 9.
  intros &m; wp; call (_: true ==> true); first by fun true; progress.
  call (_: true ==> true); first by fun true; progress.
  wp; call (_: true ==> true); first by fun true; progress.
  wp; skip; progress; assumption.

 wp; call (_: ={view, glob Adv2_G} ==> ={res}); first by fun true; progress.
 wp.
(* swap {2} 6 -1. *)
 admit (* swaps estranhos, mais keyInputPattern... *).

(* case: real = 0 *)

wp; call {1} (_: true ==> true).
 admit (* Rand_G.gen lossless *).


admit (* coisas... *).
save.

(* Main theorem *)

lemma SFE_Security :
    exists (epsilon : real), exists (RandOT1 <: OT.Rand1_t), 
    exists (RandOT2 <: OT.Rand2_t), exists (RandG <: Scheme.Rand_t),
    forall (A1<:SFE.Adv1), forall (A2<:SFE.Adv2), 
      exists (OTSim <: OT.PSim), exists (GSim <: Scheme.SIM_Sim), forall &m, epsilon > 0%r /\
      `|2%r * Pr[SFE.Game1(MainSim(RandG,GSim,OTSim),A1,RandSFE1(RandOT1),RandSFE2(RandOT2,RandG)).main()@ &m:res] - 1%r| < epsilon /\
      `|2%r * Pr[SFE.Game2(MainSim(RandG,GSim,OTSim),A2,RandSFE1(RandOT1),RandSFE2(RandOT2,RandG)).main()@ &m:res] - 1%r| < epsilon.      
proof.
(* from ec-experiments/SFE/SFE2.ec
cut OT_Security :
    (exists (epsilon:real), exists (RandGen1 <: OT.Rand1_t), 
    exists (RandGen2 <: OT.Rand2_t), forall (A1 <: OT.Adv1),
    forall (A2 <: OT.Adv2), exists (Sim <: OT.PSim),
    forall &m, epsilon > 0%r /\
        `|2%r * Pr[OT.Game1(Sim,A1,RandGen1,RandGen2).main()@ &m:res] - 1%r| <
          epsilon /\
        `|2%r * Pr[OT.Game2(Sim,A2,RandGen1,RandGen2).main()@ &m:res] - 1%r| <
          epsilon). 
apply OT_Security.
elim OT_Security.
intros OTepsilon OTrand1 OTrand2 OTprop.
clear OT_Security.
cut Sim_Security :
    (exists (epsilon:real), exists (RandGen <: Scheme.Rand_t), exists (Sim <: Scheme.SIM_Sim),
      forall (A<:Scheme.SIM_Adv), forall &m, epsilon > 0%r /\
        `|2%r * Pr[Scheme.SIM_Game(Scheme.PrvSim(RandGen,Sim), A).main()@ &m:res] - 1%r| <
          epsilon).
apply Scheme.Sim_Security.
elim Sim_Security; clear Sim_Security.
intros epsilon_g Rand_g SimG prop_g.
exists (epsilon_g + OTepsilon).
exists (<:OTrand1).
exists (<:OTrand2).
exists (<:Rand_g).
intros SFE_A1 SFE_A2.
cut getOTSim: (exists (Sim <: OT.PSim),
    forall &m, OTepsilon > 0%r /\
        `|2%r * Pr[OT.Game1(Sim,B_OT1(Rand_g, SFE_A1),OTrand1,OTrand2).main()@ &m:res] - 1%r| <
          OTepsilon /\
        `|2%r * Pr[OT.Game2(Sim,B_OT2(Rand_g, SFE_A2),OTrand1,OTrand2).main()@ &m:res] - 1%r| <
          OTepsilon).
apply (OTprop (<:B_OT1(Rand_g, SFE_A1)) (<:B_OT2(Rand_g, SFE_A2))).
clear OTprop.
elim getOTSim; clear getOTSim.
intros SimOT simOTprop.
exists (<:SimOT).
exists (<:SimG).
intros &m.
cut getEpsilonG : (epsilon_g > 0%r /\
        `|2%r * Pr[Scheme.SIM_Game(Scheme.PrvSim(Rand_g,SimG), B_G(SFE_A1, OTrand1, OTrand2)).main()@ &m:res] - 1%r| <
          epsilon_g).
apply (prop_g (<: B_G(SFE_A1, OTrand1, OTrand2)) &m).
split.
trivial.
split.
admit.
cut equalProb : (Pr[ OT.Game2(SimOT,B_OT2(Rand_g, SFE_A2),OTrand1,OTrand2).main()@ &m:res ] =
                 Pr[ SFE.Game2(MainSim(Rand_g,SimG,SimOT),SFE_A2,RandSFE1(OTrand1),RandSFE2(OTrand2,Rand_g)).main()@ &m:res ]).
equiv_deno (Reduction2  (<:SimG) (<:SimOT) (<:OTrand1)  (<:OTrand2) (<:Rand_g) (<:SFE_A2)).
trivial.
trivial.
rewrite <- equalProb.
cut breakProp: (OTepsilon > 0%r /\
        `|2%r * Pr[OT.Game1(SimOT,B_OT1(Rand_g, SFE_A1),OTrand1,OTrand2).main()@ &m:res] - 1%r| <
          OTepsilon /\
        `|2%r * Pr[OT.Game2(SimOT,B_OT2(Rand_g, SFE_A2),OTrand1,OTrand2).main()@ &m:res] - 1%r| <
          OTepsilon).
apply (simOTprop &m).
elim breakProp; clear breakProp. 
intros OTepsilon_pos H; elim H; clear H.
intros H1 H2; clear H1.
cut add_epsilons : (OTepsilon < epsilon_g + OTepsilon).
trivial.
clear getEpsilonG.
clear simOTprop.
clear equalProb.
clear OTepsilon_pos.
clear prop_g.
cut trans_lt:
(forall (r1 r2 r3:real), r1 < r2 => r2 < r3 => r1 < r3).
 trivial.
apply (trans_lt (`|2%r * Pr[OT.Game2(SimOT,B_OT2(Rand_g, SFE_A2),OTrand1,OTrand2).main()@ &m:res] - 1%r|) OTepsilon (Real.(+) epsilon_g OTepsilon) _ _);
assumption.
*)
admit.
save.
