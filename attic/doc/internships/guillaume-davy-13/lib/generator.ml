
let error msg =
  print_string msg;
  exit (-1)

let name = Sys.argv.(0)

let checkAndGetArg_equal () =
  if Sys.argv.(1) = Sys.argv.(2) then
    int_of_string Sys.argv.(1)
  else
    error (name^": Size of operands should be equal")

let bitwise op =
  match Array.length Sys.argv with
    | 2 -> let n = int_of_string Sys.argv.(1) in
      for i = 0 to n-1 do
        Printf.printf ".input a%i 0 1\n" i;
      done;
      for i = 0 to n-3 do
        Printf.printf "r%i %s a%i r%i\n" i op i (i+1);
      done;
      Printf.printf "r%i %s a%i a%i\n" (n-2) op (n-2) (n-1);
      Printf.printf ".output r0\n"
    | 3 -> let n = checkAndGetArg_equal () in
      for i = 0 to n-1 do
        Printf.printf ".input a%i 0 1\n" i;
      done;
      for i = 0 to n-1 do
        Printf.printf ".input b%i 0 1\n" i;
      done;
      for i = 0 to n-1 do
        Printf.printf "r%i %s a%i b%i\n" i op i i;
        Printf.printf ".output r%i\n" i
      done
    | _ -> error (name^" needs one or two operands, not "^(string_of_int ((Array.length Sys.argv)-1)))
