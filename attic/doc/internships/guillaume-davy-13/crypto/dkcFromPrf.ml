module CreateDkc = functor (Prf: Prf.PRF) -> struct
  
  module Number = Prf.Number
  
  type key = Number.t
  type tweak = Number.t
  type cipher = Number.t
  type msg = Number.t


  let k = Prf.k + 1
  let tau = Prf.sizeInput
  let sb = Prf.sizeOutput

  let genRandKey () = Number.genRand k

  let removeLast n = Number.truncate n (k-1)

  let encode keyA keyB tweak msg =
    let a = (Prf.compute (removeLast keyA) tweak) in
    let b = (Prf.compute (removeLast keyB) tweak) in
    Number.xor (Number.xor a b) msg

  let decode = encode
end
