module CreatePrf = functor (Block : Block.BLOCK) -> struct
  
  module Number = Block.Number
  
  type key = Block.key
  type input = Number.t
  type output = Number.t


  let k = Block.k
  let sizeInput = Block.sb-1
  let sizeOutput = Block.sb+1

  let compute key input =
    Number.truncate
      (Number.concat
        (Block.encrypt key (Number.concat input (Number.bZero ())))
        (Block.encrypt key (Number.concat input (Number.bOne ())))
      )
      (k+1)
end
