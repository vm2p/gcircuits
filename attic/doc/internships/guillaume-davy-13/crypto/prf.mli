module type PRF = sig
  module Number : Number.NUMBER
  
  type key = Number.t
  type input = Number.t
  type output = Number.t

  val k : int
  val sizeInput : int
  val sizeOutput : int
  
  val compute : key -> input -> output
end
