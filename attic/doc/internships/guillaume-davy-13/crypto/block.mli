module type BLOCK = sig
  module Number : Number.NUMBER
  
  type key = Number.t
  type input = Number.t
  type output = Number.t
  
  val k : int
  val sb : int
  
  val encrypt : key -> input -> output
  val decrypt : key -> output -> input
end

