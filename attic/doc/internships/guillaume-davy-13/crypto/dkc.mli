module type DKC = sig
  module Number : Number.NUMBER
  
  type key = Number.t
  type tweak = Number.t
  type cipher = Number.t
  type msg = Number.t
  
  val k : int
  val tau : int
  val sb : int
  
  val genRandKey : unit -> key
  val encode : key -> key -> tweak -> msg -> cipher
  val decode : key -> key -> tweak -> cipher -> msg
end
