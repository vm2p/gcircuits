module AES = functor (Number: Number.NUMBER) -> struct
  module Number = Number
  
  type key = Number.t
  type input = Number.t
  type output = Number.t
  
  let k = 128
  
  let sb = 128
  
  let transform dir key value =
    let key = Number.to_string key in
    let value = Number.to_string value in
    let transform = (Cryptokit.Cipher.aes key dir) in
    Number.from_string sb (Cryptokit.transform_string transform value)
  
  let encrypt = transform Cryptokit.Cipher.Encrypt
  let decrypt = transform Cryptokit.Cipher.Decrypt
end

