%{
  
%}

%token COLON PERIOD COMMA LINE_ENDING OPEN_PAREN CLOSE_PAREN
%token INPUT OUTPUT INCLUDE VERSION START_PARTY END_PARTY
%token <Ast.hint> HINT
%token <string> PATH
%token <string> IDENTIFIER
%token <int> INTEGER
%token COMMENT
%token EOF

%start net
%type <Ast.t> net

%%

comment:
  | COMMENT               { }


operand:
  | IDENTIFIER            { Ast.Var $1 }
  | INTEGER COLON INTEGER { Ast.Cst ($1, $3) }
  | INTEGER               { Ast.Static $1 }

operand_list:
  | operand               { [$1] }
  | operand operand_list  { $1::$2 }


arg:
  | IDENTIFIER COLON IDENTIFIER   { Ast.Map ($1, $3) }

arg_list:
  | arg                 { [$1] }
  | arg COMMA arg_list  { $1::$3 }


include_out:
  | OUTPUT OPEN_PAREN arg_list CLOSE_PAREN  { $3 }

include_in:
  | INPUT OPEN_PAREN arg_list CLOSE_PAREN   { $3 }


hint:
  | HINT  { Some $1 }
  |       { None }


cmd:
  | INPUT IDENTIFIER INTEGER INTEGER    { Some (Ast.Input ($2, $3, $4)) }
  | OUTPUT IDENTIFIER hint              { Some (Ast.Output ($2, $3)) }
  | IDENTIFIER IDENTIFIER operand_list  { Some (Ast.Gate ($1, $2, $3)) }
  | INCLUDE PATH include_out include_in { Some (Ast.Include ($2, $3, $4)) }
  | START_PARTY INTEGER                 { None }
  | END_PARTY INTEGER                   { None }
  | comment                             { None }
  |                                     { None }


cmd_list:
  | cmd                       { match $1 with Some x -> [x] | None -> [] }
  | cmd LINE_ENDING cmd_list  { match $1 with Some x -> x::$3 | None -> $3 }


net:
  | cmd_list EOF { $1 }

;





