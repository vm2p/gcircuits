{
open Parser
exception Eof
}
rule token = parse
  | [' ' '\t']                                  { token lexbuf }
  
  | ":"                                         { COLON }
  | "."                                         { PERIOD }
  | ","                                         { COMMA }
  | "\n"                                        { LINE_ENDING }
  | "("                                         { OPEN_PAREN }
  | ")"                                         { CLOSE_PAREN }
  
  | ".input"                                    { INPUT }
  | ".output"                                   { OUTPUT }
  | ".include"                                  { INCLUDE }
  | ".version"                                  { VERSION }
  | ".startparty"                               { START_PARTY }
  | ".endparty"                                 { END_PARTY }
  
  | "signed"                                    { HINT(Ast.Signed) }
  | "unsigned"                                  { HINT(Ast.Unsigned) }
  
  | '<' ('~'[^'>''\n'])* '>' as lxm             { PATH(lxm) }
  | ['a'-'z'] ( ['a'-'z'] | ['0'-'9'] )* as lxm { IDENTIFIER(lxm) }
  | ('-')? (['0'-'9'])+ as lxm                  { INTEGER(int_of_string lxm) }
  | '/''/' ([^'\n'])*                           { COMMENT }
  
  | eof                                         { EOF }
