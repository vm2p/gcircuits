(** Contains a type representing an input file *)

(** Unused for now *)
type hint = Signed | Unsigned | None

(** Type for variable *)
type var = string
(** Type for circuit name *)
type name = string
(** Type for path of files *)
type path = string

(** Operand type of gate *)
type operand = Cst of int*int | Static of int | Var of var

(** Permit to bind variable between multiple files *)
type map = Map of string*string

(** Main type corresping to a line in the file *)
type cmd =
  | Input of var * int * int
  | Output of var * (hint option)
  | Gate of var*name*(operand list)
  | Include of path*(map list)*(map list)

(** Ast type just a list of cmd *)
type t = cmd list
