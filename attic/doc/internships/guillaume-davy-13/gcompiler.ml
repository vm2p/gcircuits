let actions = ref [] in
let register f = actions := f::!actions in
let doAll chan = List.iter (fun f -> f chan) (!actions); actions := [] in

let draw input output =
  let chan = open_in input in
  let n = in_channel_length chan in
  let str = String.create n in
  really_input chan str 0 n;
  let f = Modules.Scheme.IOFunct.input str in
  close_in chan;
  Printf.fprintf output "%s" (Modules.Scheme.IOFunct.output f);
in


let bench max output =
  Printf.fprintf output "n compil garble eval encrypt evalg decrypt nbrGate nbrInput nbrOutput nbrConstant\n";
  let infos = ["nbrGate"; "nbrInput"; "nbrOutput"; "nbrConstant"] in
  let printInfo infos info =
    let Modules.Scheme.IOFunctG.Int info = (List.assoc info infos) in
    Printf.fprintf output "%i " info
  in
  for n = 2 to max do
    let inp = "0:"^(string_of_int n) in
    
    Printf.fprintf output "%i " n;
    
    let before = Sys.time () in
    
    let f = Modules.Scheme.IOFunct.input (Printf.sprintf ".input a 1 %i\n.output r\nr fibo a" n) in
    
    let after = Sys.time () in
    Printf.fprintf output "%f " (after-.before);
    
    
    let before = Sys.time () in
    
    let (g, i, o) = Modules.Scheme.garble f in
    
    let after = Sys.time () in
    Printf.fprintf output "%f " (after-.before);
    
    
    let before = Sys.time () in
    
    let out = Modules.Scheme.IOOutput.output (Modules.Scheme.eval f (Modules.Scheme.IOInput.input inp)) in
    
    let after = Sys.time () in
    Printf.fprintf output "%f " (after-.before);
    
    
    let before = Sys.time () in
    
    let iEnc =  Modules.Scheme.encrypt i (Modules.Scheme.IOInput.input inp) in
    
    let after = Sys.time () in
    Printf.fprintf output "%f " (after-.before);
    
    
    let before = Sys.time () in
    
    let oEnc =  Modules.Scheme.evalG g iEnc in
    
    let after = Sys.time () in
    Printf.fprintf output "%f " (after-.before);
    
    
    let before = Sys.time () in
    
    let outG =  Modules.Scheme.IOOutput.output (Modules.Scheme.decrypt o oEnc) in
    
    let after = Sys.time () in
    Printf.fprintf output "%f " (after-.before);
    
    List.iter (printInfo (Modules.Scheme.IOFunctG.getInfo g)) infos;
    
    Printf.fprintf output "\n"
  done
in

let evalG circuit outchan =
  let chan = open_in circuit in
  let n = in_channel_length chan in
  let str = String.create n in
  really_input chan str 0 n;
  let f = Modules.Scheme.IOFunct.input str in
  close_in chan;
  let (g, i, o) = Modules.Scheme.garble f in
  let inp = "0:0" in
  let out = Modules.Scheme.IOOutput.output (Modules.Scheme.eval f (Modules.Scheme.IOInput.input inp)) in
  let iEnc = Modules.Scheme.encrypt i (Modules.Scheme.IOInput.input inp) in
  let oEnc = Modules.Scheme.evalG g iEnc in
  let outG = Modules.Scheme.IOOutput.output (Modules.Scheme.decrypt o oEnc) in
  Printf.fprintf outchan "\toutput : %s\n" out;
  Printf.fprintf outchan "\toutputG : %s\n" outG;
in

let args = [
    ("-test", Arg.Unit (fun () -> register Test.test), "Launch some test");
    ("-draw", Arg.String (fun arg -> register (draw arg)), "Output a dot representation of the circuit");
    ("-evalg", Arg.String (fun arg -> register (evalG arg)), "Garble and eval a circuit");
    ("-bench", Arg.Int (fun arg -> register (bench arg)), "Compute some bench");
  ] in



let outputFile file =
  let chan = open_out file in
  doAll chan;
  close_out chan
in

let usage = "Blablabla" in

Arg.parse args outputFile usage;
doAll stdout
