theory INDCPA_Scheme.
  type query.
  type answer.
  type random.

  op queryValid : query*query -> bool.
  op enc : query -> random -> answer.
end INDCPA_Scheme.