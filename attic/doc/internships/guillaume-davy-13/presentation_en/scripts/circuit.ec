theory Cst.
  op bound : int.
  axiom boundInf : bound > 1.
end Cst.
import Cst.

clone Dkc.DKC_Abs as DKC.

clone Garble as GarbleCircuit with
  type funct = int*int*int*(bool array)*(bool array)*((bool*bool*bool*bool) array),
  ...
export GarbleCircuit.

require import INDCPA.

clone PrvInd as PrvInd_Circuit with
  theory Garble = GarbleCircuit.

clone INDCPA_Def as PrvIndSec with
  theory INDCPA_Scheme = PrvIndSec.

module RandGarble : PrvIndSec.Rand_t = {
  fun gen(query :funct*input) : random = {
    ...
  }
}.