theory PrvInd.

  clone import Garble.

  clone INDCPA_Scheme with
    type query = funct*input,
    type answer = functG*inputG*keyOutput,
    type random = (token*token) array,

    op queryValid(queries:query*query) =
      let (query0, query1) = queries in
      let (f0, x0) = query0 in
      let (f1, x1) = query1 in
      functCorrect f0 /\ functCorrect f1 /\
      inputCorrect f0 x0 /\ inputCorrect f1 x1 /\
      eval f0 x0 = eval f1 x1 /\
      phi f0 = phi f1,

    op enc (q:query) (r:random) =
      let (f, x) = q in
      let (g, ki, ko) = garble r f in
      (g, encrypt ki x, ko).

end PrvInd.