lemma Theorem :
  forall (ADV<:PrvIndSec.Adv_t) &m,
      `|Pr[PrvIndSec.Game(Rand, ADV).main()@ &m:res] - 1%r / 2%r| =
          2%r * Cst.bound%r * `|Pr[DKCS.Game(DKCS.Dkc, Red(ADV)).main()@ &m:res] - 1%r / 2%r|.