theory INDCPA_Def.

  clone import INDCPA_Scheme.

  module type Adv_t = {
    fun gen_query() : query*query
    fun get_challenge(answer:answer) : bool
  }.

  module type Rand_t = {
    fun gen(query:query) : random
  }.

  module Game(Rand:Rand_t, ADV:Adv_t) = {
    fun main() : bool = {
      var queries : query*query;
      var q : query;
      var r : random;
      var answer : answer;
      var adv, b, ret : bool;
    
      query = ADV.gen_query();
      if (queryValid query)
      {
        b = $Dbool.dbool;
        if (b) q = fst query;
        else q = snd query;
        r = Rand.gen(q);
        adv = ADV.get_challenge(enc q r);
        ret = (b = adv);
      } else ret = $Dbool.dbool;
      return ret;
    }
  }.
end INDCPA_Def.