module Reduction(A:PrvIndSec.Adv_t, Dkc:DKCS.Dkc_t) : DKC.Adv_t = {
  var l : int
  var c : bool
  var fc : funct
  var xc : input
  var tau : bool
  var answer : functG*inputG*keyOutput

  fun garble() : unit = { ... }

  fun work(info:bool) : bool = {
    var challenge, ret : bool;
    var ret : bool;
    var queries : query * query;
    l = $Dinter.dinter 0 (Cst.bound-1);
    tau = info;
    query = A.gen_query();
    c = $Dbool.dbool;
    if (c) (fc, xc) = fst queries;
    else (fc, xc) = snd queries;
    (fc, xc) = q;
    if (queryValid query)
    {
      garble();
      challenge = A.get_challenge(answer);
      ret = (c = challenge);
    }
    else ret = $Dbool.dbool;
    return ret;
  }
}.