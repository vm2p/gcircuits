forall B, Pr[b = B;l = $Dinter.dinter 0 (Cst.bound-1);C(b, l)] =
  (sum (lambda L, Pr[b = B;l = L;C(b, l)]) (interval 0 (Cst.bound-1))) / Cst.bound%r.