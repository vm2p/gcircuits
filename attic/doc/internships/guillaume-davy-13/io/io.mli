module type IO = sig
  type t
  
  type name = string
  type value = Int of int | String of string
  
  val input : string -> t
  val output : t -> string
  val getInfo : t -> (name*value) list
end
