module type IOMANAGER = sig
  module IOCircuit : functor (IO:Io.IO) -> functor (Circuit:Circuit.CIRCUIT) -> Io.IO with type t = IO.t Circuit.t
  module IOBool : Io.IO with type t = bool
  module IONumber : functor (Number:Number.NUMBER) -> Io.IO with type t = Number.t
  module IOArray : functor (IO:Io.IO) -> Io.IO with type t = IO.t array
end
