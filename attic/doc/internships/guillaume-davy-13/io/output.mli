(** Module permiting to print in a fancy way intermediate structure *)
(*module OUTPUT : functor (C : Circuit.CIRCUIT) -> sig
  module Circuit : Circuit.CIRCUIT

  (** Print a dot representation of the ast on the buffer *)
  val astToDot : Buffer.t -> Ast.t -> unit

  (** Print a dot representation of the circuit on the buffer *)
  val circuitToDot : Buffer.t -> 'a Circuit.t -> unit
end with module Circuit = C*)
