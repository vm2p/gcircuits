module DOT = struct
  module IOCircuit = functor (Io:Io.IO) -> functor (Circuit:Circuit.CIRCUIT) -> struct
    
    type t = Io.t Circuit.t
    
    type name = string
    type value = Int of int | String of string
    
    module Context = Context.CONTEXT(Circuit)
    
    let input str : t =
      Obj.magic (Context.parseFromString (Context.create ["lib/"]) str)
    
    let output circuit =
      let buf = Buffer.create 100 in
      Printf.bprintf buf "digraph G {\n";
      
      let outputs = Circuit.fold circuit
        (fun i -> ((fun () -> Printf.bprintf buf "input"), i))
        (fun x -> ((fun () -> Printf.bprintf buf "Cst_%s" (Io.output x)), 0))
        (fun name id gate inputs -> 
          let nameGate = name^"_"^(string_of_int id) in
          Array.iteri (fun i (g, j) ->
            g ();
            Printf.bprintf buf " -> %s [label=\"%i -> %i\"]\n" nameGate j i;
          ) inputs;
          ((fun () -> Printf.bprintf buf "%s" nameGate), 0)
        )
      in
      Array.iteri (fun i (g, j) -> g (); Printf.bprintf buf " -> output [label=\"%i -> %i\"]\n" j i) outputs;
      Printf.bprintf buf "}";
      Buffer.contents buf
      
    let getInfo circuit = [
      ("nbrInput", Int (Circuit.getNbrInput circuit));
      ("nbrOutput", Int (Circuit.getNbrOutput circuit));
      ("nbrGate", Int (Circuit.getNbrGate circuit));
      ("nbrConstant", Int (Circuit.getNbrConstant circuit))
      ]
  end
  
  module IOBool = struct
    
    type t = bool
    
    type name = string
    type value = Int of int | String of string
    
    let input channel =
      assert(false)
    
    let output x =
      Printf.sprintf "%i" (if x then 1 else 0)
      
    let getInfo circuit =
      []
  end
  
  module IONumber = functor (Number:Number.NUMBER) -> struct
    
    type t = Number.t
    
    type name = string
    type value = Int of int | String of string
    
    let input str =
      Scanf.sscanf str "%i:%i" (fun x size -> Number.from_int size x)
      
    let output x =
      Printf.sprintf "%i:%i" (Number.to_int x) (Number.size x)
      
    let getInfo circuit =
      []
  end
  
  module IOArray = functor (Io:Io.IO) -> struct
    
    type t = Io.t array
    
    type name = string
    type value = Int of int | String of string
    
    let input str =
      assert(false)
    
    let output ar =
      assert(false)
      
    let getInfo circuit =
      []
  end
end
