(*module OUTPUT = functor (Circuit : Circuit.CIRCUIT) -> struct
  module Circuit = Circuit
  
  let astToDot b cmds =
    Printf.bprintf b "%s" "digraph G {\n";
    Printf.bprintf b "input [shape=box]\n";
    Printf.bprintf b "output [shape=box]\n";
    let counter = ref 0 in
    let get () =  counter := !counter + 1; "node_" ^ string_of_int (!counter-1) in
    let rec parseCmd = function
      | Ast.Input (v, party, size) ->
        Printf.bprintf b "input -> %s\n" v;
      | Ast.Output (v, hint) ->
        Printf.bprintf b "%s -> output\n" v;
      | Ast.Gate (output, name, l) ->
        let gate = get () in
        Printf.bprintf b "%s -> %s\n" gate output;
        Printf.bprintf b "%s [shape=box, label=%s]\n" gate name;
        let rec aux = function
          | Ast.Var input -> Printf.bprintf b "%s -> %s\n" input gate
          | Ast.Cst (value, _) ->
            let cons = get () in
            Printf.bprintf b "%s -> %s\n" cons gate;
            Printf.bprintf b "%s [shape=box, label=%i]\n" cons value;
          | Ast.Static value ->
            let cons = get () in
            Printf.bprintf b "%s -> %s\n" cons gate;
            Printf.bprintf b "%s [shape=box, label=%i]\n" cons value;
        in
        List.iter aux l
      | Ast.Include (file, input, output) -> Printf.bprintf b "\n";
    in
    List.iter parseCmd cmds;
    Printf.bprintf b "%s" "}"

  let boolArrayToString b =
    Array.iter (fun x ->
      if x then
        Printf.bprintf b "1"
      else
        Printf.bprintf b "0"
    )

  let circuitToDot b circuit =
    Printf.bprintf b "digraph G {\n";
    
    (* print the graph *)
    let index = ref 0 in
    let outputs = Circuit.fold circuit
      (fun i -> ("input", i))
      (fun x -> ((if x then "Cst_1" else "Cst_0"), 0))
      (fun name id gate inputs -> 
        let h = !index in
        index := !index + 1;
        Printf.bprintf b "Gate_%i [label=%s_%i]\n" h name id;
        Array.iteri (fun i (g, j) -> Printf.bprintf b "%s -> Gate_%i [label=\"%i -> %i\"]\n" g h j i) inputs;
        ("Gate_"^(string_of_int h), 0)
      )
    in
    Array.iteri (fun i (g, j) -> Printf.bprintf b "%s -> output [label=\"%i -> %i\"]\n" g j i) outputs;
    
    (* evaluate on all input*)
    (*
    let size = Circuit.getSizeInput circuit in
    let rec iterate x = function
      | -1 -> 
        boolArrayToString b x;
        Printf.bprintf b "->";
        boolArrayToString b (Circuit.fold circuit (fun i -> x.(i)) (fun x -> x) (fun _ _ -> Circuit.compute));
        Printf.bprintf b "\n";
      | i ->
        let xT = Array.copy x in
        xT.(i) <- true;
        iterate xT (i-1);
        iterate x (i-1)
      in
      iterate (Array.make size false) (size-1);*)
    Printf.bprintf b "}"
end*)
