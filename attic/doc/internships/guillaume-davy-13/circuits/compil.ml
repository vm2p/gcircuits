module COMPIL = functor (Circuit : Circuit.CIRCUIT) -> struct
  module Circuit = Circuit
  
  type origin = TodoG of string*(Ast.operand list) | TodoI of (int * int) | Connector of ((bool Circuit.wire) list)

  (*TODO: Clean this function*)
  let compil getter cmds =
    let vars = Hashtbl.create (List.length cmds) in
    let init (i, o, n) = function
      | Ast.Gate (output, name, l) ->
        assert (not (Hashtbl.mem vars output));
        Hashtbl.add vars output (TodoG (name, l));
        (i, o, n+1)
      | Ast.Input (var, _, size) ->
        assert (not (Hashtbl.mem vars var));
        Hashtbl.add vars var (TodoI (i, size));
        (i + size, o, n)
      | Ast.Output (v, hint) -> (i, (Ast.Var v)::o, n)
      (* For include you must add the file to the context, add a subcircuit to
      the current function by a recursive call. The only problem is the order
      of the argument and the possibility to have multiple output in an include
      file but not in an infile gate*)
    in
    let (sizeInput, outputs, n) = List.fold_left init (0, [], 0) cmds in
    
    (*
    Create the Gate that produce name and link con to this gate and return the size
    *)
    let circuit = Circuit.create "User input" sizeInput in
    
    let getConstant value n =
      let boolarray = Array.to_list (Numboolarray.NUMBOOLARRAY.to_boolArray (Numboolarray.NUMBOOLARRAY.from_int n value)) in
      List.map (Circuit.getConstant circuit) boolarray
    in
    
    let rec createSubCircuit nameSub inputs =
      let inputs = List.map parseVar inputs in
      let list_size = List.map List.length inputs in
      let args = (List.map string_of_int list_size) in
      let subCircuit = getter nameSub args in
      Circuit.addSubCicruit circuit (List.flatten inputs) subCircuit
    and
    
    createInput i size =
      Array.to_list (Array.init size (fun j -> Circuit.getInput circuit (i + j)))
    and
    
    processSelect = function
      | [ op ; Ast.Static x ; Ast.Static y ] when x < y ->
        let output = parseVar op in
        List.rev (snd (List.fold_left (fun (i, l) t -> if x <= i && i < y then (i+1, t::l) else (i+1, l)) (0, []) output))
      | _ -> failwith "misused of select" (*TODO*)
    and
    
    procesSextend = function
      | [ op ; Ast.Static x] ->
        (parseVar op)@(List.flatten (Array.to_list (Array.init x (fun i -> getConstant 0 1)))) (*TODO: Should preserve value and sign*)
      | _ -> failwith "misused of sextend" (*TODO*)
    and
    
    procesZextend = function
      | [ op ; Ast.Static x] ->
        let output = parseVar op in
        (parseVar op)@(List.flatten (Array.to_list (Array.init x (fun i -> getConstant 0 1))))
      | _ -> failwith "misused of zextend" (*TODO*)
    and
    
    processTruncate = function
      | [ op ; Ast.Static x ] -> processSelect [ op ; Ast.Static 0 ; Ast.Static x]
      | _ -> failwith "misused of truncate" (*TODO*)
    and
    
    parseVar  = function
      | Ast.Static x -> assert (false)
      | Ast.Cst (x, size) -> getConstant x size
      | Ast.Var name ->
        if not (Hashtbl.mem vars name) then
          failwith ("Unknown var : "^name)
        else
        let value = Hashtbl.find vars name in
        (match value with
          | Connector input -> input
          | _ ->
            let newValue = (match value with
              | TodoG ("select", inputs) -> processSelect inputs
              | TodoG ("sextend", inputs) -> procesSextend inputs
              | TodoG ("zextend", inputs) -> procesZextend inputs
              | TodoG ("truncate", inputs) -> processTruncate inputs
              | TodoG (nameSub, inputs) -> createSubCircuit nameSub inputs
              | TodoI (i, size) -> createInput i size
              | Connector _ -> assert(false)
            ) in
            Hashtbl.add vars name (Connector newValue);
            newValue
        )
    in
    
    let outputs = List.map parseVar (List.rev outputs) in
    Circuit.setOutput circuit (List.flatten outputs);
    circuit
end
