module CONTEXT = functor (Circuit : Circuit.CIRCUIT) -> struct
  module Circuit = Circuit
  module Compil = Compil.COMPIL(Circuit)
  
  type t = {
    context : (string, bool Circuit.t) Hashtbl.t;
    mutable paths : string list;
    }

  let create paths =
    {
      context = Hashtbl.create 20;
      paths = paths
    }

  let search context name =
    match List.filter (fun path -> Sys.file_exists (path^"/"^name)) context.paths with
      | [] -> raise Not_found
      | t::q -> t

  let _parse lexer getter input =
    try
      let lexbuf = lexer input in
      let l = Parser.net Lexer.token lexbuf in
      Compil.compil getter l
    with
      | Lexer.Eof -> exit 0
      | Parsing.Parse_error -> failwith "Erreur de syntaxe\n"(*TODO*)
  
  let rec _get dep genId context name arg =
    if List.mem name dep then failwith "Circular compilation"(*TODO*) else
    match (name, arg) with
      | ("exor", ["1"; "1"]) -> Circuit.create_simple (false, true, true, false) "eXor" (genId ())
      | ("eand", ["1"; "1"]) -> Circuit.create_simple (false, false, false, true) "eAnd" (genId ())
      | ("eor", ["1"; "1"]) -> Circuit.create_simple (false, true, true, true) "eOr" (genId ())
      | _ ->
      let chan =
        try
          let nameNl = name^".nl" in
          open_in ((search context nameNl)^"/"^nameNl)
        with Not_found -> (
          let path = (try
            search context name
          with Not_found -> failwith (name^" not found")) in
          let cur = Unix.getcwd () in
          Unix.chdir path;
          let chan = Unix.open_process_in (List.fold_left (fun out x -> out^" "^x) ("./"^name) arg) in
          Unix.chdir cur;
          chan)
      in
      let circuit = _parse Lexing.from_channel (_get (name::dep) genId context) chan in
      close_in chan;
      circuit

  let get =
    let id = ref (-1) in
    let genId () = id := !id + 1; !id in
    _get [] genId

  let parseFromChannel context = _parse Lexing.from_channel (get context)

  let parseFromString context = _parse Lexing.from_string (get context)
end
