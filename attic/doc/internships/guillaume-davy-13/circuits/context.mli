(** Module to handle context *)

(** The context type contain a list of circuits that can be used. You can
give it the name of a circuit and it will try to find the one corresponding
to by opening the write file. Obviously it must be inited with a directory
or so. With the name of the circuit you must give the list of the size
of the input because circuits files can be executable that if you give them
the list as argument will print you the circuit corresponding.
At the begining the structure just contains the simple circuit : the 3 gates,
xor, and, or and you can add file by calling addFile(TODO)  *)
module CONTEXT : functor (C : Circuit.CIRCUIT) -> sig
  type t

  module Circuit : Circuit.CIRCUIT

  val create : string list -> t

  val parseFromChannel : t -> in_channel -> bool Circuit.t
  val parseFromString : t -> string -> bool Circuit.t
end
