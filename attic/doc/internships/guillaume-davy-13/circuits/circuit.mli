(** Module permiting to handle circuit *)

(** The circuit structure is a structure that permits easy parsing on it.
It can be created through the create/addSubCicruit function and use through
the fold function *)

module type CIRCUIT = sig
  (** Type of a circuit *)
  type 'a t

  (** Represent a gate *)
  type 'a gate = 'a*'a*'a*'a

  (** Represent a wire *)
  type 'a wire

  (** Represent an input for a gate *)
  type input = bool array

  (** Represent an output for a gate *)
  type 'a output = 'a

  (** Get the number of bit of the input *)
  val getNbrInput : 'a t -> int

  (** Get the number of bit of the output *)
  val getNbrOutput : 'a t -> int

  (** Get the number of constant *)
  val getNbrConstant : 'a t -> int

  (** Get the number of gate *)
  val getNbrGate : 'a t -> int

  (** Get the name of the circuit *)
  val getName : 'a t -> string

  (** Check that the circuit is correct.
  TODO
  *)
  val check : 'a t -> bool

  (** Create a new circuit *)
  val create : string -> int -> 'a t

  (** Create a new circuit *)
  val create_simple : 'a gate -> string -> int -> 'a t

  (** Compute the value of the gate on a specific input *)
  val compute : 'a gate -> input -> 'a output

  (** Set the subCircuit to a new value *)
  val addSubCicruit : 'a t -> ('a wire) list -> 'a t -> ('a wire) list

  (** Create a new constant in the circuit and return its wires *)
  val getConstant : 'a t -> 'a -> 'a wire

  (** Return the input wire corresponding to the number *)
  val getInput : 'a t -> int -> 'a wire

  (** Set the output of the circuit *)
  val setOutput : 'a t -> 'a wire list -> unit

  (** Set the output of the circuit *)
  val fold : 'a t -> (int -> 'b) -> ('a -> 'b) -> (string -> int -> 'a gate -> 'b array -> 'b) -> 'b array
end

