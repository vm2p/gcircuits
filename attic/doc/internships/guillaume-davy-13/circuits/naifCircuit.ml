module NAIFCIRCUIT = struct
  type 'a addrInGate = (('a t) ref)*int

  and 'a realWire = (('a t) ref)*((('a pin) list) ref)

  and 'a inGate = Todo | Connected of ('a wire)

  and 'a pin = Pin of ('a addrInGate) | Output of ((('a t) ref)*int)

  and 'a wire = Wire of ('a realWire) | Input of ((('a t) ref)*int) | Constant of 'a

  and 'a gate = 'a*'a*'a*'a

  (*Unuseful structure that can be removed. In fact pin and wire should directly
  point to the gate, maybe without reference, depends which is faster*)
  and 'a data = CompCircuit of (('a t) list) | Gate of (('a gate)*(('a wire) array)*int)

  and 'a t = {
    name                : string;
    input               : ('a pin list) array;
    mutable nbrConstant : int;
    mutable nbrGate     : int;
    mutable output      : ('a wire) array;
    mutable data        : 'a data;
    }

  type input = bool array

  type 'a output = 'a






  let getNbrInput circuit = Array.length (circuit.input)

  let getNbrOutput circuit = Array.length (circuit.output)

  let getNbrConstant circuit = circuit.nbrConstant

  let getNbrGate circuit = circuit.nbrGate

  let check circuit =
    true

  let getName circuit = circuit.name

  let getNameGate (name, _, _) = name

  let _create data name input =
    {
      name;
      input = Array.create input [];
      nbrConstant = 0;
      nbrGate = 0;
      output = [| |];
      data = data;
    }

  let create name input =
    _create (CompCircuit []) name input

  let create_simple data name ident =
    let gate = _create (Gate (data, [| |], ident)) name 2 in
    gate.data <- Gate (data, [| Input (ref gate, 0) ; Input (ref gate, 1) |], ident);
    gate.output <- [|Wire (ref gate, ref [])|];
    gate.input.(0) <- [Pin (ref gate, 0)];
    gate.input.(1) <- [Pin (ref gate, 1)];
    gate.nbrGate <- 1;
    gate

  let addSubCicruit circuit inputs subCircuit =
    circuit.nbrGate <- circuit.nbrGate + subCircuit.nbrGate; 
    match circuit.data with
      CompCircuit circuits -> circuit.data <- CompCircuit (subCircuit::circuits);
      let connect pos input =
        let aux output =
          match (input, output) with
          | (Wire wire, Pin pin) ->
            (snd wire) := (Pin pin)::(!(snd wire));
            (match (!(fst pin)).data with
              | Gate (_, inputs, _) -> inputs.(snd pin) <- Wire wire
              | _ -> assert false)
          | (Input (circuit, n), Pin pin) ->
            (!circuit).input.(n) <- (Pin pin)::(!circuit).input.(n);
            (match (!(fst pin)).data with
              | Gate (_, inputs, _) -> inputs.(snd pin) <- Input (circuit, n)
              | _ -> assert false)
          | (a, Pin pin) ->
            (match (!(fst pin)).data with
              | Gate (_, inputs, _) -> inputs.(snd pin) <- a
              | _ -> assert false)
          | (value, Output (circuit, n)) ->
            (!circuit).output.(n) <- value
          | (Input (circuit1,n1), Output (circuit2,n2)) ->
            (!circuit1).input.(n1) <- (Output (circuit2,n2))::(!circuit1).input.(n1);
            (!circuit2).output.(n2) <- Input (circuit1,n1)
        in
        List.iter aux subCircuit.input.(pos)
      in
      List.iteri connect inputs;
      Array.to_list subCircuit.output

  let getInput circuit i = Input (ref circuit, i)
  let setOutput circuit outputs = circuit.output <- Array.of_list outputs
  let getConstant circuit value = circuit.nbrConstant <- circuit.nbrConstant + 1; Constant value

  let compute (zz, zu, uz, uu) inputs =
    match inputs with
    | [| false; false |] -> zz
    | [| true; false |] -> uz
    | [| false; true |] -> zu
    | [| true; true |] -> uu
    | _ -> assert(false)
  
  type idInput = IdGate of int | IdInput of int | IdConstant of int
  
  let fold circuit fInput fConst fGate =
    let save = Hashtbl.create 100 in
    let saver x f =
      if Hashtbl.mem save x then
        Hashtbl.find save x
      else
        (let ret = f () in Hashtbl.add save x ret;ret)
    in
    let rec aux = function
        | Wire (g, _) -> (match (!g).data with
          | Gate (truth, input, ident) -> saver (IdGate ident)
            (fun () ->
            let inputs = Array.map aux input in
            fGate (!g).name ident truth inputs)
          | _ -> assert(false)
          )
        | Input (_, i) -> saver (IdInput i) (fun () -> fInput i)
        | Constant x -> fConst x
    in
    Array.map aux circuit.output
end

