(** Transform an ast into circuit *)

(** Transform an ast into a circuit *)
module COMPIL : functor (C : Circuit.CIRCUIT) -> sig
  module Circuit : Circuit.CIRCUIT
  
  val compil : (string -> string list -> bool Circuit.t) -> Ast.t -> bool Circuit.t
  
end with module Circuit = C
