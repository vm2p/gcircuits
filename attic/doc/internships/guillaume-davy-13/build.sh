#!/bin/bash
ocamlbuild -libs unix,nums,cryptokit -tag debug -lflags -I,`ocamlfind query cryptokit` -cflags -I,`ocamlfind query cryptokit` -Is circuits,io,garbling,parser,tools,crypto gcompiler.byte gcompiler.native gcompiler.docdir/index.html

#OCAMLRUNPARAM=b ./gcompiler.byte -test
#OCAMLRUNPARAM=b ./gcompiler.byte -draw input.nl | dot -Tpng -otest.png
