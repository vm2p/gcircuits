module Number = Numboolarray.NUMBOOLARRAY
module Circuit = NaifCircuit.NAIFCIRCUIT
module Block = Aes.AES(Number)
module Prf = PrfFromBlock.CreatePrf(Block)
module Dkc = DkcFromPrf.CreateDkc(Prf)
module IoManager = Dot.DOT
module Scheme = Garbled.GARBLED(Circuit)(Dkc)(IoManager)
