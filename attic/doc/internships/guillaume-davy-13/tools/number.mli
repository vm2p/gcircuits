module type NUMBER = sig
  type t

  val size : t -> int

  val zero : int -> t
  
  val bZero : unit -> t
  
  val bOne : unit -> t
  
  val genRand : int -> t
  
  val xor : t -> t -> t
  
  val truncate : t -> int -> t
  
  val from_string : int -> string -> t
  
  val from_boolArray : bool array -> t
  
  val from_bool : bool -> t
  
  val from_int : int -> int -> t
  
  val to_string : t -> string
  
  val to_boolArray : t -> bool array
  
  val to_bool : t -> bool
  
  val to_int : t -> int
  
  val concat : t -> t -> t
  
  val get : t -> int -> bool
  
  val print : t -> string
  
  (*val from_int : int -> t
  val from_bool_array : bool array -> t
  
  val bitwiseAnd : int -> t
  val bitwiseOr : int -> t
  val bitwiseXor : int -> t
  val dec : t -> t
  val inc : t -> t

  val to_int : t -> int
  val to_bool_array : t -> bool array*)
end
