open Modules

let test_block chan =
  let key = Number.from_string Block.k "1234567890123456" in
  let msg = Number.from_string Block.sb "42" in
  let cipher = Block.encrypt key msg in
  let msg2 = Block.decrypt key cipher in
  Printf.fprintf chan "AES :\n";
  Printf.fprintf chan "\tkey : %s\n" (Number.to_string key);
  Printf.fprintf chan "\tmsg : %s\n" (Number.to_string msg);
  Printf.fprintf chan "\tcipher : %s\n" (Number.to_string cipher);
  Printf.fprintf chan "\tmsg2 : %s\n" (Number.to_string msg2)

let test_prf chan =
  let key = Number.from_string Prf.k "1234567890123456" in
  let msg = Number.from_string Prf.sizeInput "42" in
  let cipher = Prf.compute key msg in
  Printf.fprintf chan "PRF :\n";
  Printf.fprintf chan "\tkey : %s\n" (Number.to_string key);
  Printf.fprintf chan "\tmsg : %s\n" (Number.to_string msg);
  Printf.fprintf chan "\tcipher : %s\n" (Number.to_string cipher)

let test_dkc chan =
  let key1 = Number.from_string Dkc.k "aaaaaaaaaaaaaaaa" in
  let key2 = Number.from_string Dkc.k "bbbbbbbbbbbbbbbb" in
  let tweak = Number.from_string Dkc.tau "128" in
  let msg = Number.from_string Dkc.sb "42" in
  let cipher = Dkc.encode key1 key2 tweak msg in
  let msg2 = Dkc.decode key1 key2 tweak cipher in
  Printf.fprintf chan "DKC :\n";
  Printf.fprintf chan "\tkey1 : %s\n" (Number.to_string key1);
  Printf.fprintf chan "\tkey2 : %s\n" (Number.to_string key2);
  Printf.fprintf chan "\tmsg : %s\n" (Number.to_string msg);
  Printf.fprintf chan "\tcipher : %s\n" (Number.to_string cipher);
  Printf.fprintf chan "\tmsg2 : %s\n" (Number.to_string msg2)

let test_garble chan =
  let f = Scheme.IOFunct.input ".input a 1 32\n.input b 1 32\n.output r\nr add a b" in
  let (g, i, o) = Scheme.garble f in
  let inp1 = 210000 in
  let inp2 = 210000 in
  let inp = (string_of_int (Number.to_int (Number.concat (Number.from_int 32 inp1) (Number.from_int 32 inp2))))^":64" in
  let expected = string_of_int (inp1 + inp2) in
  let out = Scheme.IOOutput.output (Scheme.eval f (Scheme.IOInput.input inp)) in
  let iEnc =  Scheme.encrypt i (Scheme.IOInput.input inp) in
  let oEnc =  Scheme.evalG g iEnc in
  let outG =  Scheme.IOOutput.output (Scheme.decrypt o oEnc) in
  Printf.fprintf chan "GARBLE :\n";
  Printf.fprintf chan "\tinput : %s\n" inp;
  Printf.fprintf chan "\texpected : %s\n" expected;
  Printf.fprintf chan "\toutput : %s\n" out;
  Printf.fprintf chan "\toutputG : %s\n" outG

let test chan =
  test_block chan;
  test_prf chan;
  test_dkc chan;
  test_garble chan
