module NUMSTR = struct
  type t = string*int
  
  let rng = Cryptokit.Random.secure_rng
  
  let size n = snd n
  
  let zero size = (String.make (size/8) (Char.chr 0), size)
  
  let bZero () = (String.make 1 (Char.chr 0), 1)
  
  let bOne () = (String.make 1 (Char.chr 1), 1)
  
  let mask x = (1 lsr x) - 1
  
  let genRand size =
    let octets = size / 8 in
    let remaining = size mod 8 in
    let value = Cryptokit.Random.string rng (octets+1) in
    value.[octets] <- Char.chr ((Char.code value.[octets]) land (mask remaining));
    (value, size)
  
  let xor (a, sa) (b, sb) =
    let (ret, size) = (if sa > sb then (String.copy a, sa) else (String.copy b, sb)) in
    Cryptokit.xor_string a 0 ret 0 ((min sa sb)/8+1);
    (ret, size)
  
  let truncate (n, osize) size =
    let n = String.copy n in
    for i = (size/8)+1 to (osize/8) do
      n.[i] <- (Char.chr 0)
    done;
    n.[(size/8)] <- Char.chr (mask (size mod 8));
    (n, size)
  
  let to_string = fst
  
  let from_string str = (str, String.length str)
  
  let from_boolArray arr =
    assert(false)(*TODO*)
  
  let from_int value =
    assert(false)(*TODO*)
  
  let concat (a, sa) (b, sb) =
    assert(false)(*TODO*)
  
  let from_bool b =
    assert(false)(*TODO*)
  
  let to_bool n =
    assert(false)(*TODO*)
  
  let get n i =
    assert(false)(*TODO*)
end

