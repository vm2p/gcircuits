module NUMBOOLARRAY = struct
  type t = bool array
  
  let size = Array.length
  
  let zero size = Array.make size false
  
  let bZero () = [| false |]
  
  let bOne () = [| true |]
  
  let mask x = (1 lsr x) - 1
  
  let genRand n = Array.init n (fun i -> Random.bool ())
  
  let bxor a b = match (a, b) with
    | (true, true) -> false
    | (false, false) -> false
    | _ -> true
  
  let xor a b =
    let sa = Array.length a in
    let sb = Array.length b in
    let (ret, min) = (if sa > sb then (Array.copy a, sb) else (Array.copy b, sa)) in
    for i = 0 to min-1 do
      ret.(i) <- bxor a.(i) b.(i)
    done;
    ret
  
  let truncate n = Array.sub n 0
  
  let bool_to_int = function false -> 0 | true -> 1
  
  let to_int x =
    let rec aux p i =
      if i = Array.length x then
        0
      else
        (aux (2*p) (i+1)) + (if x.(i) then p else 0)
    in
    aux 1 0
  
  let to_string x =
    let n = Array.length x in
    let remain = n mod 8 in
    let s = n / 8+(if remain = 0 then 0 else 1) in
    let str = String.create s in
    for i = 0 to s-1 do
      str.[i] <- Char.chr (to_int (Array.sub x (i*8) (if i = s-1 then remain else 8)))
    done;
    str
  
  let from_boolArray = Array.copy
  
  let to_boolArray = Array.copy
  
  let from_int n value =
    let f i = value land (1 lsl i) != 0 in
    Array.init n f
  
  let from_string n str =
    let ar = Array.create n false in
    for i = 0 to (String.length str) - 1 do
      let octet = from_int 8 (Char.code str.[i]) in
      for j = 0 to 7 do
        ar.(i*8+j) <- octet.(j)
      done
    done;
    ar
  
  let concat = Array.append
  
  let from_bool b = [| b |]
  
  let to_bool = Array.fold_left (fun x y -> x || (y == true)) false
  
  let get n i = n.(i)
  
  let print =
    let print_bool x = if x then "1" else "0" in
    Array.fold_left (fun str c -> str^(print_bool c)) ""
end

