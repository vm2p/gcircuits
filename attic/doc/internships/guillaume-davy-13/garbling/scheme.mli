(** Module representing a garbling scheme *)

(** It contains the five function require to use the garbling scheme,
following `Bellare & Rogaway, CCS 2012` specification *)
module type SCHEME = sig

  (** Represent an unencrypted function *)
  type funct
  (** Represent an input to an unencrypted function *)
  type input
  (** Represent an output to an unencrypted function *)
  type output
  (** Represent an encrypted function *)
  type functG
  (** Represent an input to an encrypted function *)
  type inputG
  (** Represent an output to an encrypted function *)
  type outputG
  (** Represent a key to encrypt an input *)
  type keyInput
  (** Represent a key to decrypt an output( *)
  type keyOutput
  
  module IOFunct : (Io.IO with type t = funct)
  module IOInput : (Io.IO with type t = input)
  module IOOutput : (Io.IO with type t = output)
  module IOFunctG : (Io.IO with type t = functG)
  module IOInputG : (Io.IO with type t = inputG)
  module IOOutputG : (Io.IO with type t = outputG)
  module IOKeyInput : (Io.IO with type t = keyInput)
  module IOKeyOutput : (Io.IO with type t = keyOutput)


  (** Encrypt a function and generate the key for input and output *)
  val garble : funct -> functG*keyInput*keyOutput

  (** Encrypt an input *)
  val encrypt : keyInput -> input -> inputG

  (** Decrypt an output *)
  val decrypt : keyOutput -> outputG -> output

  (** Compute the value of a function on a specific input *)
  val eval : funct -> input -> output

  (** Compute the value of an encrypted function on a specific encrypted input *)
  val evalG : functG -> inputG -> outputG
end
