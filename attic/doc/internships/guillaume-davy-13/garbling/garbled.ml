module GARBLED = functor (Circuit: Circuit.CIRCUIT) -> functor (Dkc: Dkc.DKC) -> functor (IOManager: Iomanager.IOMANAGER) -> struct
  module Number = Dkc.Number
  module Funct = Circuit
  
  module IONumber = IOManager.IONumber(Number)
  module IOArrayNumber = IOManager.IOArray(IONumber)
  
  module IOFunct = IOManager.IOCircuit(IOManager.IOBool)(Funct)
  module IOInput = IONumber
  module IOOutput = IONumber
  module IOFunctG = IOManager.IOCircuit(IONumber)(Funct)
  module IOInputG = IOArrayNumber
  module IOOutputG = IOArrayNumber
  module IOKeyInput = IOManager.IOArray(IOArrayNumber)
  module IOKeyOutput = IOManager.IOArray(IOArrayNumber)
  
  type funct = IOFunct.t
  type input = IOInput.t
  type output = IOOutput.t
  type functG = IOFunctG.t
  type inputG = IOInputG.t
  type outputG = IOOutputG.t
  type keyInput = IOKeyInput.t
  type keyOutput = IOKeyOutput.t

  let k = Dkc.k
  let tau = Dkc.k - 2

  let bool_to_int = function true -> 1 | false -> 0

  let genKey () =
    let t = (Number.genRand 1) in
    [|
      Number.concat t (Number.genRand (k-1));
      Number.concat (Number.from_bool (not (Number.to_bool t))) (Number.genRand (k-1))
    |]

  let garble f =
    let x = Hashtbl.create 100 in
    let xInput = Array.init (Circuit.getNbrInput f) (fun i -> genKey ()) in
    let circuit = Circuit.create "User input" (Circuit.getNbrInput f) in
    let getKey i =
      if Hashtbl.mem x i then
        Hashtbl.find x i
      else
      (
        let v = genKey () in
        Hashtbl.add x i v;
        v
      )
    in
    let input i = (Circuit.getInput circuit i, xInput.(i)) in
    let cst i =
      let key = genKey () in
      (Circuit.getConstant circuit key.(bool_to_int i), key)
    in
    let gate _ g truth inputs =
      let keys = getKey g in
      let [|(wireA, keysA); (wireB, keysB)|] = inputs in
      let aux x =
        let keyA = keysA.(bool_to_int x.(0)) in
        let keyB = keysB.(bool_to_int x.(1)) in
        let a = Number.from_bool (Number.get keyA 0) in
        let b = Number.from_bool (Number.get keyB 0) in
        let t = Number.concat (Number.from_int (tau-2) g) (Number.concat a b) in
        let value = Dkc.encode keyA keyB t keys.(bool_to_int (Circuit.compute truth x)) in
        ( (Number.get keyA 0, Number.get keyB 0), value )
      in
      let values = List.map aux [[|false;false|];[|false;true|];[|true;false|];[|true;true|]] in
      let truth = (List.assoc (false,false) values,List.assoc (false,true) values,List.assoc (true,false) values,List.assoc (true,true) values) in
      let gate = Circuit.create_simple truth "enc" g in
      let wire::[] = Circuit.addSubCicruit circuit [wireA; wireB] gate in
      (wire, keys)
    in
    let (outputs, xOutput) = List.split (Array.to_list (Circuit.fold f input cst gate)) in
    Circuit.setOutput circuit outputs;
    (circuit, xInput, Array.of_list xOutput)

  let encrypt k x =
    Array.init (Number.size x) (fun i -> k.(i).(bool_to_int (Number.get x i)))

  let decrypt k x =
    let f i =
      if x.(i) = k.(i).(0) then
        false
      else
        if x.(i) = k.(i).(1) then
          true
        else
          assert(false)
    in
    Number.from_boolArray (Array.init (Array.length x) f)

  let eval circuit input =
    Number.from_boolArray (Circuit.fold circuit (Number.get input) (fun x -> x) (fun _ _ -> Circuit.compute))

  let evalG circuit input =
    let compute _ g gate inputs =
      let realInputs = Array.map (fun x -> Number.get x 0) inputs in
      let a = Number.from_bool realInputs.(0) in
      let b = Number.from_bool realInputs.(1) in
      let t = Number.concat (Number.from_int (tau-2) g) (Number.concat a b) in
      Dkc.decode inputs.(0) inputs.(1) t (Circuit.compute gate realInputs)
    in
    Circuit.fold circuit (fun i -> input.(i)) (fun i -> i) compute
end

