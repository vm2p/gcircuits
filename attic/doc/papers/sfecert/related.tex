\section{Related work}\label{sec:related}
We concentrate on closely related work on the verification of
multi-party computation protocols and cryptographic
software implementations.
We refer to Appendix~\ref{app:related} for other related work in cryptography, 
and to~\cite{Blanchet:2012} for a more extensive account of the 
use of formal methods in (symbolic and computational) cryptography.

%Our work is most closely related to previous efforts that target
%multi-party computation. 
Dahl and Damg{\aa}rd et al.~\cite{DD14}
consider the symbolic analysis of specifications extracted from
two-party SFE protocol descriptions, and show that the symbolic proofs
of security are computationally sound in the sense that they imply
security in the standard UC model for the original protocols.  This
complements earlier work by Backes et al.~\cite{BackesMM10}, who
develop computationally sound methods for protocols that use secure
multi-party computation as a primitive. However, these works do not
consider verified implementations.

{\sc Wysteria}~\cite{SP:RasHamHic14} is a new programming language for
mixed-mode multiparty computations. Its design is supported by a
rigorous pen-and-paper proof that typable programs do not leak
information in unintended ways. However, their guarantees are cast in
the setting of language-based security, rather than in the usual style
of provable security. Independently, Pettai and Laud~\cite{PettaiL14}
have developed a static analysis for proving that {\sc Sharemind}
applications are secure against active adversaries. They show that
programs accepted by their analysis satisfy a simulation-based notion
called black-box security. 

There have also been efforts to use formal methods for generating
circuits. For instance, Holzer \emph{et al}~\cite{HFKV12} present a
compiler that uses CBMC to translate {\sf ANSI C} programs into
circuits that can be used as inputs to the secure computation
framework of Huang et al.~\cite{HEKM11}.  This compiler can also be
used as a front-end to our verified implementation of Yao's protocol.

There have been many attempts to develop tools that support verified
implementations. Many of these tools support proofs in the symbolic
model of cryptography. There are some notable exceptions, however. For
instance, Cad\'e and Blanchet~\cite{CadeB13} present a mechanism to
generate functional code from {\sf CryptoVerif} models, and use it to
generate a verified implementation of SSH. Similarly, Almeida \emph{et
  al}~\cite{Almeida:2013} use a verified compiler to generate a
verified x86 implementation of PKCS\#1 v2.1 encryption from an \EasyCrypt
formalization.  Finally, Kuesters \emph{et al}~\cite{Kuesters+14}
develop a framework to verify cryptographic applications written in
Java, and use it to verify a non-trivial cloud storage system.
Another practical approach to achieve high-confidence cryptographic
implementations is to use prototyping systems for cryptography~\cite{CHARM,SCAPI,CAO,CRYPTOL}. In a series of works starting 
from~\cite{AkinyeleGHP12}, Akinyele \emph{et
  al} advocate the convergence between such systems and verification
tools; in particular, their latest work~\cite{AkinyeleBGSS14} combines
\textsf{AutoBatch} and \EasyCrypt to generate verified implementations
of batch verifiers.
