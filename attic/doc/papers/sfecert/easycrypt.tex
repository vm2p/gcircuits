\section{Computer-aided cryptographic proofs}\label{sec:easycrypt}
\EasyCrypt~\cite{Barthe:2011a} is a tool-assisted framework for
verifying the security of cryptographic constructions in the
computational model. Following suggestions by Bellare and
Rogaway~\cite{Bellare:2006} and Halevi~\cite{Halevi:2005}, \EasyCrypt
adopts a code-based approach, in which cryptographic constructions,
security notions and computational assumptions are modelled as
probabilistic programs in a core, but extensible, probabilistic
programming language with imperative constructs and procedure
calls. Procedures can be concrete, in which case they are provided
with a body consisting of a command and a return expression, or
abstract, in which case only a type signature is provided. The primary
purpose of abstract procedures is to model adversaries, but they are
also an essential ingredient for compositional reasoning.

\subsection{Program logics}
Reasoning in \EasyCrypt is supported by two program logics: a
probabilistic relational Hoare logic (\pRHL), which allows to
reason about judgments of the form
\begin{displaymath}
\Equiv{c_1}{c_2}{\Pre}{\Post}
\end{displaymath}
where $c_1$ and $c_2$ are probabilistic programs, and $\Pre$ and
$\Post$ are relations on memories, and a probabilistic Hoare logic
(\pHL), which allows to reason about judgments of the form
\begin{displaymath}
  \HoareRel{c}{\Pre}{\Post}{\delta}
\end{displaymath}
where $c$ is probabilistic program, $\Pre$ and $\Post$ predicates on
memories, $\delta$ is a real-valued expression, and $\Rel$ is a
comparison operator, i.e.\, $\leq$, $=$, or $\geq$. The logics are
respectively used to establish a formal connection between two games
in game-based proofs, and to resolve the probability of an event in a
game. As an illustration, consider two programs $c_1$ and $c_2$ that
are equivalent up to a failure event $F$ in the execution of $c_2$;
the equivalence is formalized in \pRHL by the judgment:
\begin{displaymath}
\Equiv{c_1}{c_2}{\true}{\neg F\sider \Rightarrow \equiv}
\end{displaymath}
where $\equiv$ denotes equality of memories, and $\sider$ indicates
that the interpretation of $F$ is taken in the output memory of $c_2$.
Moreover, assume that the probability of $F$ in $c_2$ is upper bounded
by some constant $\delta$; this bound can be formalized in \pHL by
the judgment:
\begin{displaymath}
\HoareLe{c_2}{\true}{F}{\delta}
\end{displaymath}
\EasyCrypt also provides rules to convert \pRHL and \pHL judgments
into probability claims. Using these rules, one obtains that for
every event $E$,
\begin{displaymath}
\Pr{c_1}{E} - \Pr{c_2}{E} \leq \delta
\end{displaymath}
Note that one can recover the more traditional formulation of the
Fundamental Lemma (where the left-hand side of the above inequality is
replaced by its absolute value) by strenghtening the post-condition of
the \pRHL judgment with the assertion $F\sidel \Leftrightarrow
F\sider$.

\EasyCrypt also provides an ambient logic to reason about operators.
It can be used, for instance, to state that a decoding function is the
inverse of an encoding function. A novel feature of \EasyCrypt 1.0 is
to allow \pRHL and \pHL judgments as first-class formulae in the
ambient logic. In other words, one can use the ambient logic to reason
about formulae that freely use \pRHL and \pHL judgments; for instance,
one can perform a case analysis on the probability of an event to
build an adversary such that a reduction is valid; or, one
can use the available induction principles in the ambient logic to
formalize hybrid arguments; see Section~\ref{sec:examples}.

\subsection{Modules}

\EasyCrypt features a module system that provides a
structuring mechanism for describing cryptographic constructions.
%
A module consists of global variable declarations and procedure definitions.
(By construction, all the procedures of a given module share memory)
%
Modules are mainly used for representing cryptographic games - either
concrete or abstract. For example, the ElGamal encryption scheme is
represented as the following concrete module (where the code for \ec{enc}
and \ec{dec} has been omitted):

\medskip
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
module ElGamal = {
  fun kg() : skey * pkey = {
    var x : int = $[0..q-1];
    return (x, g^x);
  }
  fun enc(pk:pkey, m:plaintext) : ciphertext = { ... }
  fun dec(sk:skey, c:ciphertext) : plaintext = { ... }
}.
\end{lstlisting} 

The constituents of a module and their types are reflected in their
module type: a module $M$ has module type $I$ if all procedures declared
in $I$ are also defined in $M$, with the same type and parameters.
%
For instance, the previously defined \ec{ElGamal} module can be equipped
with the following module type:

\medskip
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
module type Scheme = { 
  fun kg () : skey * pkey 
  fun enc(pk:pkey, m:plaintext) : ciphertext 
  fun dec(sk:skey, c:ciphertext) : plaintext
}.
\end{lstlisting}

Not only can modules use previously defined modules, but they can also
be parametrized. For example, the following parametrized
definition captures chosen-plaintext security, where the public-key
encryption scheme \ec{S} and the adversary \ec{A} are module parameters.

\medskip
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
module type ADV = { 
  fun choose (pk:pkey) : msg * msg 
  fun guess (c:cipher) : bool
}.

module CPA (S:Scheme, A:ADV) = { 
  fun main () : bool = {
    var pk,sk,m0,m1,b,b',challenge;
    (pk,sk)   = S.kg();
    (m0,m1)   = A.choose(pk);
    b         =$ {0,1};
    challenge = S.enc(pk, b?m1:m0);
    b'        = A.guess(challenge);
    return b' = b;
   }
}.
\end{lstlisting}

The key to compositional reasoning in \EasyCrypt relies in its
ability to quantify (either universally or existentially) over modules
in its ambient logic. See Section~\ref{sec:examples} for examples
using such quantifications.
%
An essential feature of the module system is to allow restricting
memory access between quantified modules. This is used for example
in Figure~\ref{fig:hybrid} (\ec{Hybrid} lemma).

\subsection{Theories}

\EasyCrypt features a theory mechanism for organizing and reusing
the axiomatizations of the different algebraic and data structures
used in cryptographic constructions. In its simplest form, a theory
consists of a collection of type and operator declarations, and a set
of axioms; cyclic groups, finite fields, matrices, finite maps, lists
or arrays are instances of such forms of theories in \EasyCrypt's core
libraries.
%
Theories might also contain modules, allowing the definition
of libraries of standard games depending on abstract algebraic
and data structures.

Theories enjoy a cloning mechanism that is useful when formalizing
examples that involve multiple objects of the same nature, e.g. cyclic
groups in bilinear pairings. Moreover, operators of a theory can be
realized, i.e.\, instantiated by expressions, during cloning. We
also use cloning as a substitute for polymorphic modules.

In the following example, the \ec{ElGamal} scheme is defined in
the scope of a theory \ec{ElGamalT} that first clones a fresh copy of
the theory of cyclic groups. The \ec{ElGamalT} theory is then cloned
as \ec{InstantiatedElGamalT}. The cyclic group of \ec{InstantiatedElGamalT}
is no more abstract but is realized using a concrete cyclic group.

\medskip
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
theory CyclicGroup.
  type t.
  op g : t. (* generator *)
  ...
end CyclicGroup.

theory ElGamalT.
  clone import CyclicGroup as CG.
  module ElGamal = { ... (* g is CG.g in this context *) }.
end ElGamalT.l

clone ElGamalT as InstantiatedElGamalT
  with CG.t <- $\mathbb{Z}^*_5$, CG.g <- $2$, CG.( * ) x y = $x * y$ mod $5$, proving * by smt.
\end{lstlisting}
\subsection{Code generation}
\label{s:extraction}

We have developed an extraction mechanism that generates \OCaml
code from functional programs written in \EasyCrypt, allowing the
production of correct-by-construction implementations from
an \EasyCrypt proof. The nature of the source and
target languages being close, the extraction mechanism
is simple enough that one can have a high confidence in the
generated code.

The presence of abstract types/operators does not prevent the
use of the extraction mechanism. Indeed, when encountering an abstract
library, stubs for its operators, that have to be filled manually, are
generated.
%
For instance, we provide \OCaml implementations for most
of the algebraic and data structures that have been abstractly
formalized in the core libraries, such as fixed-length bitstrings
or functional arrays.

\subsection{Security Claims}

Security claims in \EasyCrypt are expressed
in the form of reductions relating the advantages of two algorithms as
in $\Adv_\A(\lambda) \le \Adv_{\B(\A)} (\lambda)$. Such statements
have the advantage of making the adversary $\B$ explicit, and of
supporting concrete as well as asymptotic security. For instance,
\EasyCrypt claims could be converted into asymptotic security claims
by making the notion of security parameter implicit, and by requiring
in all computational assumptions and security definitions that the
adversaries and simulators execute in polynomial-time. In a similar
way, \EasyCrypt claims could be converted to concrete security claims
by reasoning about the execution time of algorithms. Currently,
\EasyCrypt offers no support for reasoning about program complexity,
so the only way to check that our reductions and simulations are
indeed performed in polynomial time is by direct inspection of the
code. However, adding support for reasoning about complexity is work
in progress; once available, it will be possible to take full advantage
of existential quantification over modules.

