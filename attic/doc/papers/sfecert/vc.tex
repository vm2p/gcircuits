\section{Verifiable Computation}
\label{sec:vc}

We now move to our second example of a verifiable computation
protocol, in order to further demonstrate the wide applicability of
our techniques. In Appendix~\ref{app:background}, the interested
reader will find a description of the functionality and security
properties required of a verifiable computation protocol. Here we give
only the highlights of how we formalize the seminal construction
proposed by Gennaro, Gentry and Parno (GGP)~\cite{GGP10}.

For the GGP protocol we require a garbling scheme that provides
authenticity guarantees, and so we need a slightly different
construction from that presented in the previous
sections. Intuitively, authenticity imposes that, given
one opening of the garbled circuit, it is infeasible to find a valid
encoding of an incorrect output. For Yao's construction to display
this property, it is necessary that the decoding key
includes the labels for the output wires, so that garbled outputs can
be checked for consistency. In our formalisation, this consistency
check is handled by a new algorithm called
\ec{valid_outG}. Formalising this new garbling scheme requires little
effort, as one can simply (re-)define the operators shown in
Figure~\ref{fig:redefine} and inherit all the original definitions
from the formalisation presented before. The resulting scheme
corresponds to \ecTheory{Garble2} in~\cite{DBLP:conf/ccs/BellareHR12}.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
  op outputK (fn:fun_t) (r:rand_t) =
      let (n, m, q, aa, bb) = fst fn in
      init m (lambda i, (proj r.[(n+q-m+i,false)], proj r.[(n+q-m+i,true)])).
  
  op valid_outG (oK:outputK_t) (oG:outputG_t) =
    alli (lambda i x, x = fst oK.[i] \/ x = snd oK.[i]) oG.
  
  op decode (oK:outputK_t) (oG:outputG_t) =
    mapi (lambda i x, x=snd oK.[i]) oG.
\end{lstlisting}
\vspace{-1em}
\caption{Modified garbling scheme}
\label{fig:redefine}
\end{figure}

The GGP verifiable computation protocol can be explained as follows.
Take the garbling of the circuit to be delegated to be a public key that is given to the worker. 
The authenticity property immediately yields a weak form of verifiability, whereby the delegating party can securely outsource the computation of a single input. Verifiability is lost when more than one computation is delegated because, intuitively, if the worker sees more than one opening of the circuit, then it may be able to {\em mix} the two garbled outputs it obtained to generate a new forged one. 
To enable the verifiable delegation of multiple inputs using the same garbled circuit, the client in the GGP protocol encrypts each newly encoded input using a fresh FHE key pair. This ensures that, whilst the worker is still able to evaluate the garbled circuit (homomorphically) it will never be able to combine two garbled outputs into a new one: intuitively, the FHE guarantees that the different evaluations become independent, in the sense that one cannot (even homomorphically) use data from previous evaluations to produce a forgery on the next one.

Our formalisation of an abstract FHE scheme in \EasyCrypt
(Figure~\ref{fig:FHE}) is extremely simple.  It is a standard public
key encryption scheme, for which there exists an additional associated
algorithm which, given a transformation function $g$, maps fresh
ciphertexts encrypting any message $x$ to homomorphically transformed
ones encrypting $g(x)$. As shown below, for our purposes it suffices
to formalize and axiomatize the homomorphism w.r.t. the concrete
transformation $g$ that performs the evaluation of a garbled
circuit. Additionally, the FHE scheme is assumed to be
\ec{IND-CPA}-secure.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
  op gen : randg_t -> pkey_t * skey_t.
  op enc : pkey_t -> plaini_t -> rande_t -> cipheri_t.
  op hom_eval : (inputG_t -> outputG_t) -> cipheri_t -> ciphero_t.
  op dec : skey_t -> ciphero_t -> plaino_t.

  axiom homomorphic : forall f x r rg re,
        Scheme.validRand f r => validInputs f x =>
        let fG = funG f r in
        let iK = inputK f r in
        let xG = Input.encode iK x in
        let oK = outputK f r in
               let (pk,sk) = gen rg in
               let ci = enc pk xG re in
               let co = hom_eval (Scheme.evalG fG) ci in
                      dec sk co = evalG fG xG.
\end{lstlisting}
\vspace{-1em}
\caption{Abstract Fully-Homomorphic Encryption scheme}
\label{fig:FHE}
\end{figure}

We present our proof of security for the GGP protocol as an additional contribution,
which may be of independent interest. The structure of our argument is markedly simpler
than that presented in~\cite{GGP10}, as it directly relies on the formalisation of garbling schemes subsequently put forth by Bellare et al.~\cite{DBLP:conf/ccs/BellareHR12,BHR:2012a}. On the other hand, our formalisation complements the presentation 
in~\cite{BHR:2012a}, where a detailed proof is presented only for one-time secure schemes.

We show that the security of the GGP protocol can be proven solely based on the security of the underlying FHE scheme and the adaptive authenticity of the underlying garbling scheme denoted {\sf aut1} in~\cite{BHR:2012a}.
In this form of authenticity, the attacker gets to see the garbled circuit before she chooses the input for which she wishes to produce a forged output.
We do not require obliviousness in our proof. As shown in~\cite{BHR:2012a}, this is needed for a one-time-secure variant of the GGP construction that does not rely on FHE and uses only Yao's garbled circuits. However, it is no longer required when we can rely on FHE for privacy.

We split our presentation in two parts: first we describe 
the necessary steps to establish the correctness and authenticity properties of the  garbling scheme; then we sketch the proof of security for the GGP protocol.

\subsection{Correctness and authenticity of the garbling scheme}

Proving the correctness of the modified garbling scheme involved
little overhead with respect to the proof constructed for the original
one presented in the previous sections. Indeed, the bulk of the
inductive argument that establishes a correct opening of all the gates
throughout the circuit is identical for both constructions, and only
the final decoding operation needs to be handled differently.

Our proof of adaptive authenticity for \ecTheory{Garble2} reflects the
current state of the art: to the best of our knowledge, there is currently 
no proof that this construction meets the notion of {\sf aut1} security 
proposed in~\cite{BHR:2012a} down to standard assumptions. 
Indeed, Bellare, Hoang and Rogaway point out a gap in the original proof 
of security of the GGP protocol that corresponds precisely to this issue. 
One plausible solution to this problem, suggested in~\cite{BHR:2012a}, is 
to simply assume that \ecTheory{Garble2} provides this level of security. 
We go a bit further, and reduce the adaptive authenticity of 
\ecTheory{Garble2} to a simpler property, which we call {\sf aPriv}. 
The question of whether \ecTheory{Garble2} satisfies {\sf aPriv} can be seen 
both as an interesting open problem and a computational assumption that
underlies the security of our instantiation of the GGP protocol.
The details follow.

\begin{theorem}[Adaptive Authenticity]
\label{thm:auth-theorem}
For all efficient authenticity adversaries $\A$ against
\ecTheory{Garble2}, we can construct efficient adversary
$\B$ against the $\mathsf{aPriv}$ property of \ecTheory{Garble2} such
that:
\[
\mathsf{Adv}^{\mathsf{Auth}}_{\mbox{\ec{Garble2}}}(\A) \leq 2 \cdot
\mathsf{m} \cdot
\left(\mathsf{Adv}^{\mathsf{aPriv}}_{\mbox{\ec{Garble2}}}(\B) +
2^{-\lambda} \right).
\]
where $\mathsf{m}$ is an upper bound on the number of output wires,
and $\lambda$ is the length of the random tokens used in the dual-key
cipher.
\end{theorem}

\begin{proof}[Proof (Sketch)]
Our proof combines the proof strategies of Bellare et
al.~\cite{DBLP:conf/ccs/BellareHR12} for (non-adaptive) authenticity
and the proof sketch presented in the GGP paper~\cite{GGP10} for the
verifiability of the GGP protocol. We first guess (one of) the output bit(s) $i$
that will be flipped by the adversary's forgery, and we also bet on which will be the
correct value of this bit $\beta$: this costs us a factor of $2 \cdot
\mathsf{m}$ in the reduction.  We then rely on the $\mathsf{aPriv}$
property to jump to a final game where we replace the function $f$
chosen by the adversary with another function $f'$. Function $f'$ is
identical to $f$ in all gates except output gate $i$, where all
entries in the truth table now encode bit $\beta$.
%
The $\mathsf{aPriv}$ property has been tailored to be as weak as possible and still allow this transition. It imposes that, for adversarially chosen $(f,f',i,\beta)$ such that $f$ and $f'$ are related as described above, the garblings of $f$ and $f'$ are indistinguishable. The attacker gets to adaptively choose an input $x$, for which it will receive an encoding (under the usual restriction that $f$ and $f'$ collide on input $x$); and it is also given the two decoding tokens for output bit $i$ (not the entire decoding key).%

Observing that, by construction, the garbling of $f'$ is independent from the token that the adversary needs to guess in order to successfully forge (this corresponds to $\bar{\beta}$), the proof is completed by showing that a forgery in this final game is equivalent to guessing a uniformly distributed token. 
\end{proof}

\subsection{GGP protocol security proof sketch}

The proof of correctness for the GGP protocol is straightforward once
the correctness of the underlying FHE is assumed and that of the
underlying garbling scheme is proven.  Similarly, as presented
in~\cite{GGP10}, the privacy result follows via a hybrid argument from the
\ec{IND-CPA} security of the underlying FHE.  We sketch here, in more
detail, our security proof for the verifiability property, which is
where we depart from~\cite{GGP10} and establish an equivalent result
using a simpler game hopping argument.

\begin{theorem}[Verifiability]
\label{thm:verif-theorem}
For all efficient verifiability adversaries $\A$ against the GGP
protocol, we can construct efficient adversaries $\B^{\mathsf{FHE}}$
and $\B^{\mathsf{Garble2}}$ such that:
%\begin{align*}
%\mathsf{Adv}&^{\mathsf{Verif}}_{\mbox{\ec{GGP}}}(\A) \leq\\
%& \mathsf{q}^2 \cdot \mathsf{Adv}^{\mathsf{IND}\mbox{-}\mathsf{CPA}}_{\mbox{\ec{FHE}}}(\B^{\mathsf{FHE}})
%+ \mathsf{q} \cdot \mathsf{Adv}^{\mathsf{Auth}}_{\mbox{\ec{Garble2}}}(\B^{\mathsf{Garble2}}).
%\end{align*}
\[
\mathsf{Adv}^{\mathsf{Verif}}_{\mbox{\ec{GGP}}}(\A) \leq
 \mathsf{q}^2 \cdot \mathsf{Adv}^{\mathsf{IND}\mbox{-}\mathsf{CPA}}_{\mbox{\ec{FHE}}}(\B^{\mathsf{FHE}})
+ \mathsf{q} \cdot \mathsf{Adv}^{\mathsf{Auth}}_{\mbox{\ec{Garble2}}}(\B^{\mathsf{Garble2}}).
\]
where $\mathsf{q}$ is an upper bound on the number of problem generation 
queries placed by the adversary.
\end{theorem}
\begin{proof}[Proof (Sketch)]
The verifiability adversary can place up to $\mathsf{q}$ queries to a
problem generation oracle. Our first step in the proof is to guess
which of these queries will be selected by the adversary to produce a
forgery. We call this query $l$. This introduces a factor of
$\mathsf{q}$ in the reduction. We then use the security of the
underlying FHE to modify the answers to all the problem generation
queries except query $l$, so that the encoding provided to the
adversary in encrypted form is totally unrelated to the garbled
circuit that has been delegated. In this final game, the proof can be
concluded by reducing directly to the (adaptive) authenticity property
for the underlying garbling scheme that we have presented above.
\end{proof}
