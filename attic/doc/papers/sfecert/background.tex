\vspace{-2mm}

\section{Background}
\label{app:background}

\subsection{Yao's Garbled Circuits and Yao's SFE Protocol}
\label{app:garble}

Excellent descriptions of Yao's contributions can be found in~\cite{LP09,DBLP:conf/ccs/BellareHR12}.
Yao's idea of garbling the circuit computing $f$ consists informally of: \begin{inparaenum}[i.]
\item expressing such a circuit as a set of truth tables (one for each
  gate) and meta information describing the wiring between gates;
\item replacing the actual Boolean values in the truth tables with
  random cryptographic keys, called {\em labels}; and 
\item translating the wiring relations using a system of {\em locks}:
  truth tables are encrypted one label at a time so that, for each
  possible combination of the input wires, the corresponding labels
  are used as encryption keys that lock the label for the correct
  Boolean value at the output of that gate.\end{inparaenum} ~Then,
given a garbled circuit for $f$ and a set of labels representing
(unknown) values for the input wires encoding $x_1$ and $x_2$, one can
obliviously evaluate the circuit by sequentially computing one gate
after another: given the labels of the input wires to a gate, only one
entry in the corresponding truth table will be decryptable, revealing
the label of the output wire. The output of the circuit will comprise
the labels at the output wires of the output gates. 

To build a SFE protocol between two honest-but-curious parties, one can use Yao's garbled circuits as follows. Bob (holding $x_2$) garbles the circuit and provides this to Alice (holding $x_1$) along with: i. the label assignment for the input wires corresponding to $x_2$, and ii. all the information required to decode the Boolean values of the output wires.
In order for Alice to be able to evaluate the circuit, she should be able to obtain an the correct label assignment for $x_1$. Obviously, Alice cannot reveal $x_1$ to Bob, as this would totally destroy the goal of SFE. Furthermore, Bob cannot reveal information that would allow Alice to encode anything other than $x_1$, since this would reveal more than $f(x_1,x_2)$. To solve this problem, Yao proposed the use of an {\em oblivious transfer} (OT) protocol. This is a (lower-level) SFE protocol for a very simple functionality that allows Alice to obtain the labels that encode $x_1$ from Bob, without revealing anything about $x_1$ and learning nothing more than the labels she requires.\footnote{Luckily, efficient OT protocols exist that can be used for this specific purpose, thereby eliminating what could otherwise be a circular dependency.} The protocol is completed by Alice evaluating the circuit, recovering the output, and providing the output value back to Bob.\footnote{This is a simplified view of Yao's protocol. It suffices because we are dealing with honest-but-curious adversaries assumed to follow the protocol.} The combined security of the garbled circuit technique and the OT protocol guarantee that $f$ can be securely evaluated in this manner.

\subsection{Verifiable Computation}

We recall the notion of a Verifiable Computation (VC) scheme introduced by Gennaro, Gentry and Parno~\cite{GGP10} and informally describe the associated security notions. Subsequent works have taken slightly different approaches to the formalisation of security, namely by strengthening the verifiability requirement to allow for fully adaptive queries and, sometimes, dropping the privacy requirement. However, the essence of the primitive remains the same.


A VC scheme is a protocol between two polynomial-time parties, a client and a worker, that enables them to collaborate on the computation of a function $f : \{0,1\}^n \longrightarrow \{0,1\}^m$. 
It consists of four steps:
\begin{description}
\item[Preprocessing] A one-time stage in which the client computes some auxiliary (public and private) information associated with $f$. This phase can take time comparable to computing the function from scratch, but it is performed only once, and its cost is amortized over all the future executions.
\item[Input Preparation] When the client wants the worker to compute $f(x)$, it prepares some auxiliary (public and private) information about $x$. The public information is sent to the worker.
\item[Output Computation] Once the worker has the public information associated with $f$ and $x$, it computes a string $\pi_x$ which encodes the value $f(x)$ and returns it to the client. 
\item[Verification] From the value $\pi_x$, the client can compute the value $f(x)$ and verify its correctness.
\end{description}
The crucial efficiency requirement is that Input Preparation and Verification must take less time than computing $f$ from scratch (ideally linear time, $O(n + m)$). Also, the Output Computation stage should take roughly the same amount of computation as $f$, so as not to overburden the worker.

Two security properties are required from a VC protocol, verifiability and privacy. Privacy imposes that, given the output of the Preprocessing stage, a malicious worker is unable to distinguish between two input-prepared values of its choosing, given access to an oracle from which it can obtain arbitrary input-prepared values.

Verifiability is defined by means of a game in which the malicious worker gets access to the output of the Preprocessing stage, and can request up to $\mathsf{q}$ input-prepared values of its choosing. The adversary is then required to output a proof which causes the verification procedure to accept an incorrect value for the delegated computation on one of the $\mathsf{q}$ inputs it requested from its oracle. 

In the definition put forth in the GGP~\cite{GGP10} paper, which we adopt, the worker cannot be allowed to know whether verification procedures were successful or not. This is due to a limitation of the GGP construction, which has been addressed in subsequent work. This discussion is not relevant in the context of this paper, where we simply use the GGP protocol to demonstrate the capabilities of our formal verification infrastructure.
