%!TeX Root=sfecert.tex
\section{Garbling schemes}\label{sec:garbled}
Garbling schemes~\cite{DBLP:conf/ccs/BellareHR12}
(Figure~\ref{fig:garble}) are operators on \emph{functionalities} of
type \ec{func}.
%
Such functionalities can be evaluated on some input using an \ec{eval}
operator.  In addition, a functionality can be \emph{garbled} using
three operators (all of which may consume randomness). \ec{funG}
produces the garbled functionality, \ec{inputK} produces an
input-encoding key, and \ec{outputK} produces an output-encoding key.
%
The garbled evaluation \ec{evalG} takes a garbled functionality and
some encoded input and produces the corresponding encoded output. The
input-encoding and output-decoding functions are self-explanatory.
\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
type func, input, output.
op eval : func -> input -> output.
op valid: func -> input -> bool.

type rand, funcG, inputK, outputK.
op funcG  : func -> rand -> funcG.
op inputK : func -> rand -> inputK.
op outputK: func -> rand -> outputK.

type inputG, outputG.
op evalG : funcG -> inputG -> outputG.
op encode: inputK -> input -> inputG.
op decode: outputK -> outputG -> output.
\end{lstlisting}
\vspace{-1em}
\caption{Abstract Garbling Scheme.\label{fig:garble}}
\end{figure}
In practice, we are interested in garbling functionalities encoded as
Boolean circuits and therefore fix the \ec{func} and \ec{input} types
and the \ec{eval} function. Circuits themselves are represented by
their topology and their gates.
%
A topology is a tuple $(n,m,q,\mathbb{A},\mathbb{B})$, where $n$ is
the number of input wires, $m$ is the number of output wires, $q$ is
the number of gates, and $\mathbb{A}$ and $\mathbb{B}$ map to each
gate its first and second input wire respectively.
%
A circuit's gates are modelled as a map $\mathbb{G}$ associating
output values to a triple containing a gate number and the values of
the input wires.  Gates are modelled polymorphically, allowing us to
use the same notion of circuit for Boolean circuits and their garbled
counterparts.
%
We only consider \emph{projective schemes}~\cite{DBLP:conf/ccs/BellareHR12}, 
where Boolean values
on each wire are encoded using a fixed-length random \emph{token}.
% and the values of output wires can be decoded by simply taking the
%token's least significant bit.
This fixes the type \ec{funcG} of garbling schemes, and the
\ec{outputK} and \ec{decode} operators.

Following the \textsf{Garble1} construction of Bellare et
al.~\cite{DBLP:conf/ccs/BellareHR12}, we construct our garbling scheme
using a variant of Yao's garbled circuits based on a pseudo-random
permutation, via an intermediate Dual-Key Cipher (DKC)
construction. We denote the DKC encryption with \ec{E}, and DKC
decryption with \ec{D}. Both take four tokens as argument: a tweak
that we generate with an injective function and use as unique IV, two
keys, and a plaintext (or ciphertext).
%
Some intuition about the construction is given in
Appendix~\ref{app:garble}.
%
We give functional specifications to the garbling algorithms in
Figure~\ref{fig:somegarble}. For clarity, we denote
functional \ec{fold}s using stateful \ec{for} loops.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type topo = int * int * int * int array * int array.
type 'a circuit = topo * (int * 'a * 'a,'a) map.

type leak = topo.

type input, output = bool array.
type func = bool circuit.

type funcG = token circuit.
type inputG, outputG = token array.
op evalG f i =
  let ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}$) = f in
  let evalGate = lambda g x$_1$ x$_2$,
    let x$_{1,0}$ = lsb x$_1$ and x$_{2,0}$ = lsb x$_2$ in
    D (tweak g x$_{1,0}$ x$_{2,0}$) x$_1$ x$_2$ $\mathbb{G}$[g,x$_{1,0}$,x$_{2,0}$] in
  let wires = extend i q in (* extend the array with q zeroes  *)
  let wires = map (lambda g, evalGate g $\mathbb{A}$[g] $\mathbb{B}$[g]) wires in (* decrypt wires *)
  sub wires (n + q - m) m.

type rand, inputK = ((int * bool),token) map.
op encode iK x = init (length x) (lambda k, iK[k,x.[k]]).

op inputK (f:func) (r:((int * bool),token) map) =
  let ((n,_,_,_,_),_) = f in filter (lambda x y, 0 <=  fst x < n) r.

op funcG (f:func) (r:rand) =
  let ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\mathbb{G}$) = f in
  for (g,x$_\mathsf{a}$,x$_\mathsf{b}$) $\in$ [0..q] * bool * bool
    let a = $\mathbb{A}$[g] and b = $\mathbb{B}$[g] in
    let t$_\mathsf{a}$ = r[a,x$_\mathsf{a}$] and t$_\mathsf{b}$ = r[b,x$_\mathsf{b}$] in
    $\widetilde{\mathbb{G}}$[g,t$_\mathsf{a}$,t$_\mathsf{b}$] = E (tweak g t$_\mathsf{a}$ t$_\mathsf{b}$) t$_\mathsf{a}$ t$_\mathsf{b}$ r[g,$\mathbb{G}$[g,x$_\mathsf{a}$,x$_\mathsf{b}$]]
  ((n,m,q,$\mathbb{A}$,$\mathbb{B}$),$\widetilde{\mathbb{G}}$).
\end{lstlisting}
\vspace{-1em}
\caption{\ec{SomeGarble}: our Concrete Garbling Scheme.\label{fig:somegarble}}
\end{figure}

\heading{Security of Garbling Schemes} The privacy property of
garbling schemes required by Yao's SFE protocol is more conveniently
captured using a simulation-based definition.  Like the security
notions for protocols, the privacy definition for garbling schemes is
parameterized by a leakage function upper-bounding the information
about the functionality that may be leaked to the adversary. (We
consider only schemes that leak at most the topology of the circuit.)
%
Consider efficient non-adaptive adversaries that provide two
procedures:
\begin{inparaenum}[i.]
\item \ec{choose} takes no input and outputs a pair \ec{(f,x)}
  composed of a functionality and some input to that functionality;
\item on input a garbled circuit and garbled input pair \ec{(F,X)},
  \ec{distinguish} outputs a bit $b$ representing the adversary's
  guess as to whether he is interacting with the real or ideal
  functionality.
\end{inparaenum}
%
Formally, we define the $\SIMCPA_\Phi$ advantage of an adversary $\A$
of type $\Ad^\mathsf{Gb}$ against garbling scheme $\Gb$ =
\ec{(funcG,inputK,outputK)} and simulator $\Sim$ as
%
$$ \mathsf{Adv}^{\SIMCPA_\Phi}_{\Gb,\mbox{\ec{R}},\mbox{\ec{S}}}(\A) = \left| 2 \cdot \Pr[\mbox{\ec{SIM}}(\mbox{\ec{R}},\mbox{\ec{S}},\A): \result] - 1\right|.$$
%
A garbling scheme $\Gb$ using randomness generator \ec{R} is
$\SIMCPA_\Phi$-secure if, for all adversary $\A$ of type
$\Ad^\mathsf{Gb}$, there exists an efficient simulator $\Sim$ of type
\ec{Sim} such that
$\mathsf{Adv}^{\SIMCPA_\Phi}_{\Gb,\mbox{\ec{R}},\mbox{\ec{S}}}(\A)$ is
small.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type leak.
op $\Phi$: func -> leak.

module type Sim = {
  fun sim(x: output, l: leak): funcG * inputG
}.

module type $\Ad^\mathsf{Gb}$ = {
  fun choose(): func * input
  fun distinguish(F: funcG, X: inputG) : bool
}.

module SIM(R: Rand, $\Sim$: Sim, $\A$: $\Ad^\mathsf{Gb}$) = {
  fun main() : bool = {
    var real, adv, f, x, F, X;
    (f,x) = $\A$.gen_query();
    real =$ {0,1};
    if (!valid f x)
      adv =$ {0,1};
    else {
      if (real) {
        r = R.gen($\Phi$ f);
        F = funcG f r;
        X = encode (inputK f r) x;
      } else {
        (F,X) = $\Sim$.sim(f(x),$\Phi$ f);
      }
      adv = $\A$.dist(F,X);
    }
    return (adv = real);
  }
}.
\end{lstlisting}
\vspace{-1em}
\caption{Security of garbling schemes.\label{fig:garblesec}}
\end{figure}

%% Repeated intuition.
%% Intuitively, the definition states that if a valid $\Sim$ exists, then
%% real ciphertexts do not (computationally) leak more information than
%% simulated ones, and these cannot possibly contain more than the
%% information given to the simulator.

%%% Old DKC stuff. Do we want it?
%\begin{lstlisting}[language=easycrypt,style=easycrypt-pretty,xleftmargin=0pt,xrightmargin=0pt]
%op dkc_enc: token -> token -> token -> token.
%
%module Game_DKC = {
%  var K:token
%  var Ks, Rs: (token * token) array
%  var b:bool
%
%  fun init(nq:int, m:int): bool = {
%    var tau, i;
%    b =${0,1}; tau =${0,1};
%    K =$ { t:token | lsb t = tau };
%    Ks = init (lambda x, (0,0));
%    Rs = init (lambda x, (0,0));
%    i = 0;
%    while (i < nq) {
%      Rs.[i] =$ token * token;
%      if (i < nq - m)
%        Ks.[i] =$ { (x,y):token * token | lsb y = !lsb x };
%      else
%        Ks.[i] =$ { (x,y):token * token | !lsb x /\ lsb y };
%      i = i + 1;
%    }
%  }
%
%  type query = token * (int * bool) * pos * (int * bool).
%  type response = token * token * token.
%  fun encrypt(qs: queryarray): response array = {
%    var n, used, ret;
%    used = empty;
%    ret = init (lambda x, (0,0,0));
%    n = length qs - 1;
%    while (0 <= n) {
%      (tok,(i,iv),pos,(j,jv)) = qs.[n];
%      if (mem tok used) {
%        ret = empty;
%        n = 0;
%      }
%      else {
%        (Ka,Kb) = if pos = Left then (select Ks.[i] iv,K) else (K,select Ks.[i] iv);
%        Kj = if b then (select Ks.[j] jv) else (select R.[j] jv)
%        ret.[n] = dkc_enc tok Ka Kb Kj;
%      }
%      n = n - 1;
%    }
%  }
%}.
%\end{lstlisting}

Following~\cite{DBLP:conf/ccs/BellareHR12}, we establish simulation-based
security via a general result that leverages a more convenient 
indistinguishability-based security notion denoted
$\INDCPA_{\Phi_{\mathsf{topo}}}$: we formalize a general theorem
stating that, under certain restrictions on the leakage function $\Phi$,
$\INDCPA_\Phi$-security implies $\SIMCPA_\Phi$ security. This result is
discussed below as Lemma~\ref{lem:ind-implies-sim}.

\heading{A modular proof}
The general lemma stating that $\INDCPA$-security implies
$\SIMCPA$-security is easily proved in a very abstract model, and is then
as easily instantiated to our concrete garbling setting. We describe the
abstract setting to illustrate the proof methodology enabled by \EasyCrypt
modules on this easy example.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,style=easycrypt-pretty,xleftmargin=0pt,xrightmargin=0pt,mathescape]
module type $\Ad^{\mathsf{IND}}$ = {
  fun choose(): ptxt * ptxt
  fun distinguish(c:ctxt): bool
}.

module IND (R:Rand, $\A$:$\Ad^{\mathsf{IND}}$) = {
  fun main(): bool = {
    var p$_0$, p$_1$, p, c, b, b', ret, r;
    (p$_0$,p$_1$) = $\A$.choose();
    if (valid p$_0$ /\ valid p$_1$ /\ $\Phi$ p$_0$ = $\Phi$ p$_1$) {
      b =${0,1};
      p = if b then p$_1$ else p$_0$;
      r = R.gen(|p|);
      c = enc p r;
      b' = $\A$.distinguish(c);
      ret = (b = adv);
    }
    else ret =${0,1};
    return ret;
  }
}.
\end{lstlisting}
\vspace{-1em}
\caption{Indistinguishability-based Security for Garbling Schemes.\label{fig:INDCPA}}
\end{figure}
\noindent The module shown in Figure~\ref{fig:INDCPA} is a slight
generalization of the standard $\INDCPA$ security notions for
symmetric encryption, where some abstract leakage operator $\Phi$
replaces the more usual check that the two adversary-provided
plaintexts have the same length.We formally prove an abstract result
that is applicable to any circumstances where
indistinguishability-based and simulation-based notions of security
interact.
%
We define the $\INDCPA$ advantage of an adversary $\A$ of type
$\Ad^{\mathsf{IND}}$ against the encryption operator \ec{enc} using
randomness generator \ec{R} with leakage $\Phi$ as
\begin{align*}
\mathsf{Adv}^{\INDCPA_\Phi}_{\mathsf{enc},\mbox{\ec{R}}}&(\A) =
\left|2 \cdot \mbox{\ec[basicstyle=\small\sffamily,literate={A}{{$\A$}}{1}]{Pr[Game_IND(R,A): res]}} - 1\right|
\end{align*}
\noindent where \ec{R} is the randomness generator used in the concrete theory.

In the rest of this subsection, we use the following notion of
invertibility.  A leakage function $\Phi$ on plaintexts (when we
instantiate this notion on garbling schemes these plaintexts are
circuits and their inputs) is \emph{efficiently invertible} if there
exists an efficient algorithm that, given the leakage corresponding to
a given plaintext, can find a plaintext consistent with that leakage.

\begin{lemma}[$\INDCPA$-security implies $\SIMCPA$-security]%
\label{lem:ind-implies-sim}
If $\Phi$ is efficiently invertible, then for every efficient
$\SIMCPA$ adversary $\A$ of type $\Ad^{\mathsf{Gb}}$, one can build an
efficient $\INDCPA$ adversary $\B$ and an efficient simulator
$\Sim$ such that
$$\mathsf{Adv}^{\SIMCPA_\Phi}_{\mathsf{enc},\Sim}(\A) =
\mathsf{Adv}^{\INDCPA_\Phi}_{\mathsf{enc}}(\B).$$
\end{lemma}
\begin{proof}[Proof (Sketch)]
Using the inverter for $\Phi$, $\B$ computes a second plaintext from
the leakage of the one provided by $\A$ and uses this as the second
part of her query in the $\INDCPA$ game. Similarly, simulator $\Sim$
generates a simulated view by taking the leakage it receives and
computing a plaintext consistent with it using the $\Phi$-inverter.
%% \begin{figure}[ht]
%% \begin{lstlisting}[language=easycrypt,style=easycrypt-pretty,xleftmargin=0pt,xrightmargin=0pt,mathescape]
%% module $\B$ ($\A$ : Adv): Adv$^{\mathsf{IND}}$ = {
%%   fun choose(): ptxt * ptxt = {
%%     var p$_0$, p$_1$;
%%     p$_0$ = $\A$.choose();
%%     p$_1$ = pi_sampler ($\Phi$ p$_0$);
%%     return (p$_0$,p$_1$);
%%   }
%%
%%   fun distinguish(c:ctxt): bool = {
%%     var resp;
%%     resp = $\A$.distinguish(c);
%%     return resp;
%%   }
%% }.
%% \end{lstlisting}
%% \vspace{-1em}
%% \caption{A simple reduction.\label{fig:sim-ind}}
%% \end{figure}
%
The proof consists in establishing that $\A$ is called by $\B$ in a
way that coincides with the $\SIMCPA$ experiment when $\Sim$ is used
in the ideal world, and is performed by code motion.
\end{proof}

%{\color{red} {\bf Francois:} Discuss the application of the lemma... This may require us to anticipate lemma 2 *and* the SFE security lemma, so we can show the interplay between adversaries and how we can easily compose them into towers of functor applications. We should still discuss this here so that we can (informally) discuss the memory restrictions on a simple example before diving into huge stacks of compositions. The restrictions are hugely important to the soundness of the EasyCrypt approach to composition and anyone familiar with UC frameworks will no doubt be confused if we do not mention it.}

\heading{Finishing the proof}
We reduce the $\INDCPA_{\Phi{\sf topo}}$-security of \ec{SomeGarble}
to the $\mathsf{DKC}$-security of the underlying DKC primitive
(see~\cite{DBLP:conf/ccs/BellareHR12}).
% {\color{red} DKC security should make it into an appendix.} (no space)).
In the lemma statement, \ec{c} is an abstract upper bound on the size
of circuits (in number of gates) that are considered valid. The lemma
holds for all values of \ec{c} that can be encoded in a token minus
two bits.

\begin{lemma}[\textsf{SomeGarble} is $\INDCPA_{\Phi_{\sf topo}}$-secure]
\label{lem:SomeGarble-INDCPA}
For every efficient $\INDCPA$ adversary $\A$ of type
$\Ad^{\mathsf{Gb-IND}}$, we can construct a efficient $\mathsf{DKC}$
adversary $\B$ such that
\begin{align*}
\mathsf{Adv}^{\INDCPA_{\Phi_{\sf topo}}}_{\mathsf{SomeGarble}}&(\A) \leq
(\mathsf{c} + 1) \cdot
\mathsf{Adv}^{\mathsf{DKC}}_{\mathsf{SomeGarble}}(\B).
\end{align*}
\end{lemma}
\begin{proof}[Proof (Sketch)]
The constructed adversary $\B$, to simulate the garbling scheme's oracle,
samples a wire $\ell_0$ which is used as pivot in a hybrid construction where:
\begin{inparaenum}[i.]
\item all tokens that are revealed by the garbled evaluation on the
  adversary-chosen inputs are garbled normally, using the real DKC scheme;
  otherwise
\item all tokens for wires less than $\ell_0$ are garbled using encryptions of
  random tokens (instead of the real tokens representing the gates' outputs);
\item tokens for wire $\ell_0$ uses the real-or-random DKC oracle; and
\item all tokens for wires greater than $\ell_0$ are garbled normally.
\end{inparaenum}

Here again, the generic hybrid argument (Figure~\ref{fig:hybrid}) can
be instantiated and applied without having to be proved again,
yielding a reduction to an adaptive DKC adversary. A further reduction
allows us to then build a non-adaptive DKC adversary, since all DKC
queries made by $\B$ are in fact random and independent.
\end{proof}

From Lemmas~\ref{lem:ind-implies-sim} and~\ref{lem:SomeGarble-INDCPA}, we can
conclude with a security theorem for our garbling scheme.

\begin{theorem}[\textsf{SomeGarble} is $\SIMCPA_{\Phi_{\sf topo}}$-secure]
\label{thm:SomeGarble-SIMCPA}
For every $\SIMCPA$ adversary $\A$ that implements
$\Ad^{\mathsf{Gb}}$, one can construct an efficient simulator $\Sim$
and a DKC adversary $\B$ such that
\begin{align*}
\mathsf{Adv}&^{\SIMCPA_{\Phi_{\sf topo}}}_{\mathsf{SomeGarble},\Sim}(\A) \leq
(\mathsf{c} + 1) \cdot \mathsf{Adv}^{\mathsf{DKC}}_{\mathsf{SomeGarble}}(\B).
\end{align*}
\end{theorem}
\begin{proof}[Proof (Sketch)]
Lemma~\ref{lem:ind-implies-sim} allows us to construct from $\A$ the
simulator \ec{S} and an \INDCPA adversary $\mathcal{C}$. From
$\mathcal{C}$, Lemma~\ref{lem:SomeGarble-INDCPA} allows us to
construct $\B$ and conclude.
\end{proof}
