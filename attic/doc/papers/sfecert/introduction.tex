%!Tex Root=sfecert.tex
\section{Introduction} \label{sec:intro}
Research on formally verifying the security of software systems was
initiated almost 40 years ago, and has established a number of
significant landmarks, including security proofs for separation
kernels, hypervisors, virtual machines, and web browsers. For their
most part, these works have focused on idealized models. However, a
recent trend is to build \emph{verified implementations}. Transposing
the guarantees from models to implementations offers a much higher
degree of assurance, and prevents the possibility of introducing 
attack vectors via implementation mistakes or abstraction gaps.

The recent security controversy has placed cryptographic software
under the spotlight and has raised justified concerns with respect to
the potential exploitation of {\em backdoors} at two different
levels: \begin{inparaenum}[i.] \item those that undermine the
  theoretical security guarantees offered by protocols included in
  cryptographic standards; \item those that thwart the practical
  security of cryptographic software implementations.\end{inparaenum}~
One potential way to rebuild trust in cryptographic software is to
employ formal methods to rule out the existence of such backdoors, by
building and deploying formally verified cryptographic
implementations.

One application area where the adoption of this strategy is both
particularly pressing {\em and} challenging is that of cloud
computing.  Here, the need to reduce the level of trust placed in
service providers can only be addressed by deploying a new generation
of advanced cryptographic protocols that allow end-users to protect
their data, whilst taking advantage of the technical and economic
benefits of the cloud paradigm.  These protocols enable secure and
verifiable computation over encrypted data and can be seen as
cryptographic virtual machines supporting arbitrary computations; to
highlight this fact, we will refer to them as {\em cryptographic
  systems}.

Developing verified implementations of cryptographic systems seems
unfathomable because their security hinges on rather sophisticated
cryptographic constructions, whose security must itself be verified
formally.  We identify at least three significant challenges towards
achieving this goal, and argue below that these challenges have not
yet been met satistactorily by existing tools for verifying the
security of cryptographic constructions such as
\textsf{CryptoVerif}~\cite{Blanchet08CV} and earlier versions
(v0.2) of \EasyCrypt~\cite{Barthe:2011a}; we discuss
\textsf{F7}~\cite{Bhargavan:2010} at the end of the introduction.

The first challenge is to provide support for a broader scopus of
cryptographic proof techniques. \textsf{EasyCrypt} v0.2 and
\textsf{CryptoVerif} excel at modelling basic game-playing techniques
such as equivalences, failure events, reductions, eager/lazy sampling.
However, a number of standard concepts and techniques from provable
security, such as hybrid arguments or rewinding, have resisted thus
far formalization in these systems. In addition, comparatively little
work has been done in formalising computational security proofs for
simulation-based notions of security.  Indeed, simulation-based proofs
are markedly distinct from the reductionistic arguments typically
addressed by \textsf{CryptoVerif} or \textsf{EasyCrypt} v0.2;
they intrinsically require existential quantification over adversarial
algorithms and the ability to instantiate security models with
concrete algorithms (the simulators) that serve as witnesses as to the
validity of the security claims. Notable exceptions include formal
accounts of zero-knowledge protocols~\cite{Almeida:2012} and
computational differential privacy~\cite{Barthe:2013}; however, these
simulation-based proofs are developed in simplified settings, as
further elaborated in the related work section.

The second challenge is to ensure that the verification tools permit
taming the complexity of cryptographic systems.  \textsf{CryptoVerif}
and \textsf{EasyCrypt} v0.2 have primarily been used to verify
cryptographic primitives, such as Full-Domain Hash signatures or OAEP,
or standalone protocols, such as Kerberos or SSH.  While these
examples can be intricate to verify, there is a difference of scale
with cryptographic systems, which typically involve several layers of
cryptographic constructions. Scalability is a well-known bottleneck
for formal verification, that has been addressed with varying degrees
of success over the last 40 years. However, \textsf{CryptoVerif} and
\textsf{EasyCrypt} v0.2 do not offer any mechanism that specifically
addresses this issue, and it is not even clear how (and which)
mechanisms used elsewhere could be added in order for formalizations
to scale to larger systems.

The third challenge is to enable a new workflow of specification,
validation, and prototyping for building verified implementations of
cryptographic systems. To ensure that these implementations can have
a practical impact, one must develop integrated environments in
which cryptographers can specify cryptographic algorithms, perform
lightwheight validation (e.g., using a type-checking mechanism), 
formalize security proofs, and generate reasonably efficient
implementations. What we envision here is a formal counterpart to
existing prototyping systems for cryptography~\cite{CHARM,SCAPI,CAO,CRYPTOL}.
Although \textsf{CryptoVerif} and \textsf{EasyCrypt} v0.2 have made
preliminary steps in this direction, they fall short of providing 
the functionalities that are expected by practice-oriented cryptographers
(see section~\ref{sec:related}).

These challenges can be summarised as an important open question: can
we realistically hope to build practical verified implementations of 
cryptographic systems in the near future?

\heading{Contributions} In this paper we answer this open question
positively, by presenting a new version of \EasyCrypt and an
associated methodology that enables us to obtain the first verified
implementations of cryptographic systems. More specifically, we
provide:
\begin{enumerate}
\item verified implementations of two variants of Yao's garbled
  circuits, following the foundational framework put forth by Bellare,
  Hoang and Rogaway~\cite{DBLP:conf/ccs/BellareHR12,BHR:2012a}, and an $n$-fold
  extension of the oblivious transfer protocol by Bellare and
  Micali~\cite{BM89}, in the hashed version presented by Naor and
  Pinkas~\cite{Naor:2001} ($n$ being the size of the selection
  string). These are generic components that can be used in many
  cryptographic systems;

\item a verified implementation of Yao's two-party secure function
  evaluation (SFE) protocol based on garbled circuits and oblivious
  transfer.  This protocol allows two parties holding private
  inputs $x_1$ and $x_2$, to jointly evaluate any function
  $f(x_1,x_2)$ and learn its result, whilst being assured that no
  additional information about their respective inputs is
  revealed. Two-party SFE provides a general distributed solution to
  the problem of computing over encrypted data in cloud scenarios;
  
\item a verified (albeit partial) implementation of a verifiable
  computation (VC) protocol due to Gennaro, Gentry, and Parno
  (GGP). This protocol allows a client to delegate the computation of
  an arbitrary function $f$ to an untrusted worker, while keeping its
  data secret {\em and} being able to verify that the result is
  correct. It is based on Yao's garbled circuits and fully homomorphic
  encryption (FHE),\footnote{The fact that it relies on FHE explains
    why we only have a partial implementation: there does not yet
    exist a practical instantiation for this component. The modularity
    of our approach implies that a working implementation can be
    derived if and when a suitable FHE scheme becomes available. 

    We also note that that many exciting
    developments~\cite{GGPR13,Pinocchio} have taken place in VC since 
    the original GGP paper. However, this protocol uses Yao's garbled 
    circuits in a totally different way than that required by
    Yao's SFE protocol, which makes it ideal for our demonstration purposes.}
  and it was one of the first to address verifiability in the specific
  scenario of cloud computing.
\end{enumerate}
In addition to their practical relevance, these examples are appealing
because they crystallize many facets of the issues that arise in the
formalization of cryptographic systems.

We adopt the latest version%
\footnote{\url{http://www.easycrypt.info}} (v1.0) of \EasyCrypt, which
enhances the initial prototype described in~\cite{Barthe:2011a} in
multiple directions. In particular, it provides a lightweight module
system that is able to reflect the logical design of cryptographic
systems and the modular structure of their proofs.  This version also
incorporates a new code generation mechanism to produce reasonably
efficient functional implementations of cryptographic algorithms from
\EasyCrypt descriptions.
%
Using the new features of \EasyCrypt and our own extensions, we are able 
to formalize complex reasonings that arise in our examples and were 
previously out of reach. 

Highlights of our formalization include:
\begin{itemize} 
\item a formally verified library of generic hybrid arguments. The
  library is based on formalizing security games as parametrisable
  modules, and critically uses quantification over modules. We use
  hybrid arguments in the security proofs for oblivious transfer and
  for garbled circuits. However, the library can be used for other
  purposes, and will contribute to build a broad scopus of formally
  verified cryptographic techniques; 

\item simulation-based proofs where simulators and attackers can be
  seen as interchangeable adversarial algorithms, which can either be
  concrete, existentially quantified, or universally quantified. In
  particular, the generic statements of security for the SFE protocol
  involve two alternations of quantifiers, which would be challenging
  to handle even in a hand-written proof.
  %%
  %% Intuitively, such statements say that
  %%
  %% \noindent\begin{tabular}{p{7.5cm}} $\forall$ low-level protocols and
  %% randomness generators for them,\\ $\exists$ a randomness generator
  %% for the SFE protocol such that,\\ $\forall$ SFE adversaries and
  %% low-level protocol simulators,\\
  %% \parbox{7.5cm}{$\exists$ adversaries against the low-level protocols and an SFE simulator such that,}\\
  %% \end{tabular}
  %%
  %% the advantage of the SFE attacker adversary in distinguishing the
  %% SFE protocol from the simulator is upper bounded by the advantages
  %% of the low-level adversaries in distinguishing the low-level
  %% protocols and their respective simulators.
  
\item layered security proofs in which general compositional theorems
  can be proven using abstract views of cryptographic primitives, and
  instantiations of these primitives can be proven secure down to one
  or more computational assumptions. This type of modular reasoning,
  which is not necessary when performing reductionistic proofs (even
  complex ones), is essential to permit tackling security proofs of
  high-level protocols.
\end{itemize}
Moreover, using the new code generation mechanism provided by \EasyCrypt
we are able to derive a verified implementation for the SFE protocol, and 
a verified (but partial) implementation of the VC protocol. 
We include experimental results showing the practicality of our verified 
implementation of Yao's SFE protocol.

\heading{Comparison with ${\sc F}7$} Bhargavan et
al.~\cite{Bhargavan:2010} develop a type-based approach for the
verification of cryptographic implementations written in a dialect of
ML. Their approach is based on refinement types, which are
counterparts to assertions in typed functional languages, and allows
proving security in a symbolic setting where cryptographic primitives
are idealized. Subsequently, Fournet et al.~\cite{Fournet:2011} extend
this approach towards proving security in the computational setting,
using an intricate combination of type abstraction and refinement
types. They then use their system to build verified implementations of
TLS~\cite{Bhargavan:2013}. In particular, they have currently verified
the TLS handshake protocol.

However, ${\sc F}7$ does not provide any support for relational nor
probabilistic reasonings, both of which are central to cryptographic
proofs. As a consequence, large and critical parts of cryptographic
proofs cannot be stated (let alone verified) using ${\sc F}7$ and must
instead be carried out using pen and paper. Accordingly, there is a
significant gap between ${\sc F}7$ statements and the provable
security guarantees that are commonly sought by cryptographers. We
believe that this gap is not only hard to apprehend, but might also be
problematic to close. As evidence, their most recent work on TLS uses
\EasyCrypt (v1.0) to carry out some intricate parts of the proof.


\heading{Implementation-level descriptions} It is common practice in
cryptographic implementations to separate the randomness sampling
operations from the rest of the implementation.  There are two reasons
for this: on one hand, general-purpose programming languages are not
probabilistic, and so randomness must invariably be obtained via some
sort of system/library call and handled explicitly; on the other hand,
developers (particularly in open-source scenarios) often give the
end-users the possibility (and responsibility) to choose their
preferred randomness sampling procedure (see, for example, the 
NaCl cryptographic library\footnote{\url{http://nacl.cr.yp.to}}).  

We take the same approach, describing
various cryptographic algorithms as deterministic functional
operators, so that we immediately get for free a one-to-one mapping between the
\EasyCrypt functional definitions and ML code.  The randomness
generation procedures, which we see as ideal specifications of the
required distributions that must be available in practical
applications, are described separately as \EasyCrypt modules. 
%written imperatively.


