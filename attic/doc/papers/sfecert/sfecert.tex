\documentclass[nocopyrightspace]{sigplanconf}
%\usepackage[utf8]{inputenc}
%\usepackage{compactbib}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
%\usepackage{url} 
%%\usepackage{pdflscape} 
\usepackage[ocgcolorlinks,
            bookmarks=true,
            bookmarksopen=true,
            colorlinks=true,
            linkcolor=Fuchsia,
            citecolor=Fuchsia,
            urlcolor=Fuchsia,
            pdfusetitle,pdftex]{hyperref}
%\usepackage[T1]{fontenc}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{xspace}
% \usepackage{times}
\usepackage{listings}
\usepackage{paralist}
\usepackage{amsthm} %% overriding "almost invisible" lemma style
\input{defs.tex}

% Lower-case calligraphy
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}

\setlength{\bibsep}{1.65pt}

\newtheorem{lemma}{Lemma}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}

\renewcommand{\INDCPA}{\ensuremath{\mathsf{IND\mbox{-}CPA}}\xspace}
\newcommand{\SIMCPA}{\mathsf{SIM\mbox{-}CPA}}
\newcommand{\Sim}{\mathsf{S}}

% Different font in captions
\newcommand{\captionfonts}{\small} 
\makeatletter 
 
% Allow the use of @ in command names
\long
\def\@makecaption#1#2{

%
\vskip\abovecaptionskip \sbox\@tempboxa{{\captionfonts #1: #2}}

%
\ifdim \wd\@tempboxa >\hsize {\captionfonts #1: #2\par} \else \hbox to\hsize{\hfil\box\@tempboxa\hfil}

%
\fi \vskip\belowcaptionskip} 
\makeatother 

% Cancel the effect of \makeatletter
% LaTeX Heading
\newcommand{\heading}[1]{{\vspace{6pt}
\noindent\sc{#1.}}} 
\newcommand{\headingg}[1]{{$\;$\vspace{6pt}\\
\noindent\sc{#1}}}

% Common
\DeclareMathOperator{\sample}{{\leftarrow\!\!\mbox{\tiny${\$}$\normalsize}}\,}
\mathchardef\mhyphencar ="2D
\newcommand{\dash}{\ensuremath{\mhyphencar\allowbreak}}
\newcommand{\Lst}{\mathsf{L}}
\newcommand{\st}{\mathrm{\,\,st\,\,}}
\newcommand{\OO}{\textsc{Oracles}} 
\newcommand{\A}{\mathcal{A}} 
\newcommand{\B}{\mathcal{B}} 
\newcommand{\C}{\mathcal{C}}
\newcommand{\K}{\mathcal{K}}
\newcommand{\Gm}{\mathsf{Game}} 
\newcommand{\Adv}{\mathbf{Adv}} 
\newcommand{\List}{\mathsf{List}} 
\newcommand{\Bad}{\mathsf{Bad}}
\newcommand{\poly}{\mathsf{poly}} 
\newcommand{\secpar}{{\lambda}} 

\title{Verified implementations for secure and verifiable computation}
\subtitle{}

\authorinfo{Jos\'e Bacelar Almeida\and \\Manuel Barbosa}  {INESC TEC and Universidade do Minho}  {\{jba,mbb\}@di.uminho.pt}
\authorinfo{Gilles Barthe}  {IMDEA Software Institute}  {gjbarthe@gmail.com}

\authorinfo{Guillaume Davy}
  {ENS Cachan and \\IMDEA Software Institute}
  {davyg@davyg.fr}

\authorinfo{Fran\c cois Dupressoir}  {IMDEA Software Institute}  {fdupress@gmail.com}

\authorinfo{Benjamin Gr\'egoire}
  {INRIA Sophia-Antipolis}
  {Benjamin.Gregoire@inria.fr}
  
\authorinfo{Pierre-Yves Strub}  {IMDEA Software Institute}  {pierre-yves@strub.nu}%The following two blank lines appear to be **really** important: don't remove them.


\begin{document}

\maketitle

\begin{abstract}
Formal verification of the security of software systems is gradually
moving from the traditional focus on idealized models, to the more
ambitious goal of producing \emph{verified implementations}.  This
trend is also present in recent work targeting the verification of
cryptographic software, but the reach of existing tools has so far
been limited to cryptographic primitives, such as RSA-OAEP encryption,
or standalone protocols, such as SSH.  This paper presents a scalable
approach to formally verifying implementations of higher-level
cryptographic systems, directly in the computational model.

We consider circuit-based cloud-oriented cryptographic protocols for
secure and verifiable computation over encrypted data.  Our examples
share as central component Yao's celebrated transformation
of a boolean circuit into an equivalent \lq\lq garbled\rq\rq\ form
that can be evaluated securely in an untrusted environment.  We
leverage the foundations of garbled circuits set forth by Bellare,
Hoang, and Rogaway (CCS 2012, ASIACRYPT 2012) to build \emph{verified implementations}
of garbling schemes, a verified implementation of Yao's secure
function evaluation protocol, and a verified (albeit partial)
implementation of the verifiable computation protocol by Gennaro,
Gentry, and Parno (CRYPTO 2010).  The implementations are formally
verified using \EasyCrypt, a tool-assisted framework for building
high-confidence cryptographic proofs, and critically rely on two novel
features: a module and theory system that supports compositional
reasoning, and a code extraction mechanism for generating
implementations from formalizations.

%A celebrated construction by Yao defines an efficient transformation
%of a boolean circuit into an equivalent \lq\lq garbled\rq\rq\ form
%that can be evaluated securely in an untrusted environment. Thirty
%years after their inception, garbled circuits still play a fundamental
%role in cryptography. They are used for instance in recent general 
%constructions of circuit-based primitives like functional encryption and
%verifiable computation.
%Their importance is also witnessed by a series of
%recent papers by Bellare, Hoang, and Rogaway (CCS 2012, S\&P 2013),
%who develop rigorous foundations for garbled circuits and propose
%implementations that achieve an unprecedented speed.
%
%In this paper, we show how to leverage these foundations for building
%\emph{verified implementations} of garbled circuits, from which we
%derive a verified implementation of a secure function evaluation
%protocol, and a verified (albeit partial) implementation of the 
%verifiable computation protocol by Gennaro, Gentry, and Parno
%(CRYPTO 2010). 
%The implementations are formally verified using \EasyCrypt, a tool-assisted 
%framework for building
%high-confidence cryptographic proofs, and critically rely on two novel
%features: a module and theory system that supports compositional
%reasoning, and a code extraction mechanism for generating efficient
%implementations from formalizations. Our work evidences for the first
%time the feasibility of formally verifying implementations of advanced
%cryptographic protocols, directly in the computational model. Our work
%also makes several contributions of independent interest, including a
%verified library for hybrid arguments, and a clarification on the 
%nature and role of the authenticity and obliviousness properties 
%put forth by Bellare, Hoang, and Rogaway in the verifiable computation
%protocol of Gennaro, Gentry, and Parno.

\medskip 
\noindent {\small\bf Keywords.}  Garbled Circuits, Secure Function
Evaluation, Verifiable Computation, Verified Implementations,
\textsf{EasyCrypt}.
\end{abstract}

%----------------------------------------------------------------------
 
%\newpage
\input{introduction.tex}
\input{someot.tex}
\input{garblingscheme.tex}
\input{yao.tex}
%\input{concrete.tex}
\input{vc.tex}
\input{evaluation.tex}
\input{related.tex}
\input{conclusion.tex}

%% Bibliography
\bibliographystyle{IEEEtran}
\bibliography{sfecert,pl}

\appendix
%\input{easycrypt.tex}
\input{background.tex}
\input{crypto-related.tex}

\end{document}
