%!TeX Root=sfecert.tex
\newcommand{\G}{\mathcal{G}}
\newcommand{\GS}{\mathcal{G}}
\newcommand{\Gb}{\mathsf{Gb}}
\newcommand{\En}{\mathsf{En}}
\newcommand{\De}{\mathsf{De}}
\newcommand{\Ev}{\mathsf{Ev}}
\newcommand{\ev}{\mathsf{ev}}
\renewcommand{\Pr}{\mathsf{Pr}}
\newcommand{\GarbAdv}{\textsf{GAdv}}
\newcommand{\arrayVar}[2][]{{\overline{\mathsf{#2}_{#1}}}}
\newcommand{\Ad}{\mathpzc{Adv}}

\section{Two-party protocols and oblivious transfer}\label{sec:ot}
We present a variant of a classic oblivious transfer
protocol~\cite{BM89,Naor:2001} and discuss its security proof. Its
small size and relative simplicity make it a good introductory example
to \EasyCrypt formalization.

\heading{Two-Party Protocols} We first start by generically defining
two-party protocols, that generalize both Secure Function Evaluation
and Oblivious Transfer, and their security. In \EasyCrypt,
declarations pertaining to abstract concepts meant to later be refined
can be grouped into named theories such as the one shown in
Figure~\ref{fig:Prot}. Any lemma proved in such a theory is also a
lemma of any implementation (or instantiation) where the theory axioms
hold.

\begin{figure}[ht]
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
theory Protocol.
 type input$_1$, output$_1$. $\quad$ type input$_2$, output$_2$.
 op validInputs: input$_1$ -> input$_2$ -> bool.
 op f: input$_1$ -> input$_2$ -> output$_1$ * output$_2$.

 type rand$_1$, rand$_2$, conv.
 op prot: input$_1$->rand$_1$->input$_2$->rand$_2$->conv * output$_1$ * output$_2$.
  $\ldots$
end Protocol.
\end{lstlisting}
\vspace{-1em}
\caption{Abstract Two-Party Protocol.\label{fig:Prot}}
\end{figure}

Two parties want to compute a \emph{functionality} \ec{f} on their joint
inputs, each obtaining their share of the output. This may be done interactively
via a \emph{protocol} \ec{prot} that may make use of additional randomness
(passed in explicitly for each of the parties) and produces, in addition to the
result, a \emph{conversation trace} of type \ec{conv} that describes
the messages publicly exchanged by the parties during the protocol execution.
%
In addition, the input space may be restricted by a validity predicate
\ec{validInputs}. This predicate expresses restrictions on the
adversary-provided values, typically used to exclude trivial
attacks not encompassed by the security definition.

Following the standard approach for secure multi-party computation protocols,
security is defined using simulation-based definitions. In this case we capture
honest-but-curious (or semi-honest, or passive) adversaries. We consider each
party's \emph{view} of the protocol (typically containing its randomness and the
list of messages exchanged during a run), and a notion of \emph{leakage} for
each party, modelling how much of that party's input may be leaked by the
protocol execution (for example, its length). 
% MANUEL: commented this out.
%In this paper, we do not consider output secrecy and therefore do not need to consider output leakage.
%
Informally, we say that such a protocol is secure if each party's view can be
efficiently simulated using only its inputs, its outputs and the other party's
leakage.
%
Formally, we express this security notion using two games (one for
each party).  We display one of them in Figure~\ref{fig:protsec}, in
the form of an \EasyCrypt \emph{module}. Note that modules are used to
model games and experiments, but also schemes, oracles and
adversaries.

%Modules are composed of a \emph{memory} (a set of global variables,  here
%empty), and a set of \emph{procedures}. In addition, modules can be
%parameterized by other modules (in which case, we often call them
%\emph{functors}) whose procedures they can query like \emph{oracles}.
%Which oracles may be accessed by which procedure is specified using
%\emph{module types}. A module is said to fulfill a module type if it implements
%all the procedures declared in that type. Any procedures implemented in addition
%to those appearing in the module type are not accessible as oracles. For
%example, even if a module that implements module type \ec{Sim} is used to
%instantiate the $\Sim$ parameter of the \ec{Sec}$_1$ module, none of the
%procedures in \ec{Sec}$_1$ may call the \ec{sim}$_2$ oracle.

Module type $\Ad^\mathsf{Prot}_i$ ($i \in \{1,2\}$) tells us that an adversary
impersonating Party $i$ is defined by two procedures:
\begin{inparaenum}[i.]
\item \ec{choose} that takes no argument and chooses a full input pair
for the functionality, and
\item \ec{distinguish}, that uses Party $i$'s view of the protocol execution to
produce a boolean guess as to whether it was produced by the real system or the
simulator.
\end{inparaenum}
Since the module type is not parameterized, the adversary is not given access
to any oracles (modelling a non-adaptive adversary). We later show how oracle
access can be given to both abstract and concrete modules.
%
Note that procedures in the same module may share state, and it is therefore not
necessary to explicitly add state to the module signature.
%
A module that implements all procedures in a given module type is said to
\emph{implement} that type. Note that, for example, any module implementing
\ec{Sim} also implements \ec{Sim}$_1$.
%
We omit module types for the randomness generators \ec{R}$_1$ and
\ec{R}$_2$, as they only provide a single procedure \ec{gen} taking
some leakage and producing some randomness. We also omit the dual
security game for Party 2.

The security game, modelled as module \ec{Sec}$_1$, is explicitly parameterized
by two randomness-producing modules \ec{R}$_1$ and \ec{R}$_2$, a simulator
\ec{S}$_1$ and an adversary $\A_1$. This enables the code of procedures defined
in \ec{Sec}$_1$ to make queries to any procedure that appears in the module
types of its parameters. However, they may not directly access the internal state or
procedures that are implemented by concrete instances of the module parameters, when
these are hidden by the module type.
%
The game implements, in a single experiment, both the real and ideal worlds. In
the real world, the protocol \ec{prot} is used with adversary-provided inputs to
construct the adversary's view of the protocol execution.
%
In the ideal world, the functionality is used to compute Party 1's output, which
is then passed along with Party 1's input and Party 2's leakage to the
simulator, which produces the adversary's view of the system.
%
We prevent the adversary from trivially winning by denying it any advantage when
it chooses invalid inputs.

A two-party protocol \ec{prot} (parameterized by its
randomness-producing modules) is said to be secure with leakage $\Phi
= (\phi_1,\phi_2)$ whenever, for any adversary $\A_i$ implementing
$\Ad^\mathsf{Prot}_i$ ($i \in \left\{1,2\right\}$), there exists a
simulator $\Sim_i$ implementing \ec{Sim}$_i$ such that
\begin{align*}
\mathsf{Adv}^{\mathsf{Prot}^i_\Phi}_{\mbox{\ec{prot}},\mbox{\ec{S}$_i$},\mbox{\ec{R}$_1$},\mbox{\ec{R}$_2$}}&(\A_i) =
 \left| 2 \cdot \Pr[{\mbox{\ec{Sec}}_i(\mbox{\ec{R}$_1$},\mbox{\ec{R}$_2$},\mbox{\ec{S}$_i$},\A_i)}: {\result}] - 1\right|
\end{align*}
is small, where \ec{res} denotes the Boolean output of procedure \ec{main}.

Intuitively, the existence of such a simulator $\mbox{\ec{S}}_i$
implies that the protocol conversation and output cannot reveal any
more information than the information revealed by the simulator's
input.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty,mathescape]
type leak$_1$, leak$_2$. $\quad$ op $\phi_1$ : input$_1$ -> leak$_1$. $\quad$ op $\phi_2$ : input$_2$ -> leak$_2$.
type view$_1$ = rand$_1$ * conv. $\quad$ type view$_2$ = rand$_2$ * conv.

module type Sim = {
  proc sim$_1$(i$_1$: input$_1$, o$_1$: output$_1$, l$_2$: leak$_2$) : view$_1$
  proc sim$_2$(i$_2$: input$_2$, o$_2$: output$_2$, l$_1$: leak$_1$) : view$_2$
}.

module type Sim$_i$ = {
  proc sim$_i$(i$_i$: input$_i$, o$_i$: output$_i$, l$_{3-i}$: leak$_{3-i}$) : view$_i$
}.

module type $\Ad^\mathsf{Prot}_i$ = {
  proc choose(): input$_1$ * input$_2$
  proc distinguish(v: view$_i$) : bool
}.

module Sec$_1$(R$_1$: Rand$_1$, R$_2$: Rand$_2$, $\Sim$: Sim$_1$, $\A_1$: $\Ad^\mathsf{Prot}_1$) = {
  proc main() : bool = {
    var real, adv, view$_1$, o$_1$, r$_1$, r$_2$, i$_1$, i$_2$;
    (i$_1$,i$_2$) = $\A_1$.choose();
    real =$ {0,1};
    if (!validInputs i$_1$ i$_2$)
      adv =$ {0,1};
    else {
      if (real) {
        r$_1$ = R$_1$.gen($\phi_1$ i$_1$);
        r$_2$ = R$_2$.gen($\phi_2$ i$_2$);
        (conv,_) = prot i$_1$ r$_1$ i$_2$ r$_2$;
        view$_1$ = (r$_1$, conv);
      } else {
        (o1,_) = f i$_1$ i$_2$;
        view$_1$ = $\Sim$.sim$_1$(i$_1$, o$_1$, $\phi_2$ i$_2$);
      }
      adv = $\A_1$.distinguish(view$_1$);
    }
    return (adv = real);
  }
}.
\end{lstlisting}
\vspace{-1em}
\caption{Security of a two-party protocol protocol.\label{fig:protsec}}
\end{figure}

Throughout the paper we omit the indices representing randomness
generators whenever they are clear from the context.

%%%% Francois: This cut may be more violent than expected.
%% Our formalisation accommodates generic protocols (e.g.,
%% oblivious transfer of an arbitrary, albeit polynomial, number of
%% messages) which justifies the technicality of parametrising the
%% randomness generation procedures with public information (or leakage)
%% associated with the protocol inputs.

\heading{Oblivious Transfer Protocols}
We can now define oblivious transfer, restricting our attention to a specific
notion useful for constructing general SFE functionalities. To do so, we
\emph{clone} the \ec{Protocol} theory, which makes a literal copy of it and
allows us to instantiate its abstract declarations with concrete definitions.
%
When cloning a theory, everything it declares or defines is part of the clone,
including axioms and lemmas. Note that lemmas proved in the original theory
are also lemmas in the clone.
%
The partial instantiation is shown in Figure~\ref{fig:ot}.
%
\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty, mathescape]
clone Protocol as OT with
  type input$_1$ = bool array,
  type output$_1$ = msg array,
  type leak$_1$ = int,
  type input$_2$ = (msg * msg) array,
  type output$_2$ = unit,
  type leak$_2$ = int,
  op $\phi_1$ ($\arrayVar[1]{i}$: bool array) = length $\arrayVar[1]{i}$,
  op $\phi_2$ ($\arrayVar[2]{i}$: (msg * msg) array) = length $\arrayVar[2]{i}$,
  op f ($\arrayVar[1]{i}$: bool array) ($\arrayVar[2]{i}$: (msg * msg) array) = $\arrayVar[1]{i}_{\arrayVar[2]{i}}$.
  op validInputs($\arrayVar[1]{i}$: bool array) ($\arrayVar[2]{i}$: (msg * msg) array) =
      0 < length $\arrayVar[1]{i}$ $\leq$ n$_{max}$ /\ length $\arrayVar[1]{i}$ = length $\arrayVar[2]{i}$,
$\ldots$
\end{lstlisting}
\vspace{-1em}
\caption{Instantiating Two-Party Protocols into Abstract SFE.\label{fig:ot}}
\end{figure}
%
We restrict the input, output and leakage types for the parties,
as well as the leakage functions and the functionality \ec{f}. The chooser
(Party 1) takes as input a list of Boolean values (i.e., a bit-string) 
she needs to encode, and the sender (Party 2), takes as input a list of 
pairs of messages (which can also be seen as alternative encodings for the 
Boolean values in Party 1's inputs). Together, they compute
the array encoding the chooser's input, revealing only the lengths of each
other's inputs. We declare an abstract constant \ec{n} that bounds the size
of the chooser's input. This introduces an implicit quantification on the bound
\ec{n} in all results we prove.
%
Defining OT security is then simply a matter of instantiating the
general notion of security for two-party protocols via cloning. 
Looking ahead, we use
%
$\mathsf{Adv}^{\mathsf{OT}^i}$ to denote the resulting instance of
$\mathsf{Adv}^{\mathsf{Prot}^i_{(\mbox{\ec[basicstyle=\tiny\sffamily]{length}},\mbox{\ec[basicstyle=\tiny\sffamily]{length}})}}$,
and similarly, we write $\Ad^\mathsf{OT}_i$ the types for adversaries
against the OT instantiation.

\heading{An Oblivious Transfer Protocol} To define a concrete
two-party OT protocol, one now only has to define the types of
randomness and conversations and the protocol itself, with its
individual computation and message exchange steps.

In the following, we abuse notation and denote operators, their
lifting to arrays of the same length, and their lifting to one array
argument and one scalar argument in the same way, writing
$\arrayVar{v}$ to single out array variables.
%
For an array of pairs $\arrayVar{a}$, we write $\arrayVar{a}_0$ for
the array of its first components (i.e. \ec{fst} $\arrayVar{a}$), and
$\arrayVar{a}_1$ for the array of its second components.
%
We also denote multiplicatively operations in a group $\G$ of prime
order $q$, using a given generator $g$.

In Figure~\ref{fig:someot}, we describe our OT protocol in a purely
functional manner, making any local state shared between the various
stages of a given party explicit. For example, \ec{step}$_1$ outputs
the sender's local state \ec{st}$_s$, for later use by \ec{step}$_3$.

\begin{figure}[ht]
\begin{lstlisting}[language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty, mathescape]
op step$_1$ ($\arrayVar{m}$:(msg * msg) array) (r:int array * $\G$) =
  let ($\arrayVar{c}$,hkey) = r in
  let st$_s$ = ($\arrayVar{m}$,$g^{\arrayVar{c}}$,hkey) in
  let m$_1$ = (hkey,$g^{\arrayVar{c}}$) in
  (st$_s$,m$_1$).

op step$_2$ ($\arrayVar{b}$:bool array) ($\arrayVar{r}$:$\G$ array) m$_1$ =
  let (hkey,$\arrayVar{gc}$) = m$_1$ in
  let st$_c$ = ($\arrayVar{b}$,hkey,$\arrayVar{r}$) in
  let $\arrayVar[2]{m}$ = if $\arrayVar{b}$ then $\arrayVar{gc}$ / $g^{\arrayVar{r}}$ else $g^{\arrayVar{r}}$ in
  (st$_c$,$\arrayVar[2]{m}$).

op step3 st$_s$ (r:$\G$) $\arrayVar[2]{m}$ =
  let ($\arrayVar{m}$,$\arrayVar{gc}$,hkey) = st$_s$ in
  let $\arrayVar{e}$ = (H(hkey,$\arrayVar[2]{m}^\mathsf{r}$) $\oplus$ $\arrayVar{m}_0$,H(hkey,($\arrayVar{gc} / \arrayVar[2]{m}$)$^\mathsf{r}$) $\oplus$ $\arrayVar{m}_1$) in
  let m$_3$ = ($g^\mathsf{r}$,$\arrayVar{e}$) in
  m$_3$.

op finalize st$_c$ m$_3$ =
  let ($\arrayVar{b}$,hkey,$\arrayVar{x}$) = st$_c$ in
  let ($\arrayVar{gr}$,$\arrayVar{e}$) = m$_3$ in
  let res = H(hkey,$\arrayVar{gr}^{\arrayVar{x}}$) $\oplus$ $\arrayVar{e}_{\arrayVar{b}}$ in
  res.

clone OTProt as SomeOT with
  type rand$_1$ = $\G$ array,
  type rand$_2$ = ($\G$ array * $\G$) * $\G$,
  op prot ($\arrayVar{b}$:input$_1$) ($\arrayVar[c]{r}$:rand$_1$) ($\arrayVar{m}$:input$_2$) (r$_s$:rand$_2$) =
    let (st$_s$,m$_1$) = step1 $\arrayVar{m}$ (fst r$_s$) in
    let (st$_c$,$\arrayVar[2]{m}$) = step2 $\arrayVar{b}$ $\arrayVar[c]{r}$ m$_1$ in
    let m$_3$ = step3 st$_s$ (snd r$_s$) $\arrayVar[2]{m}$ in
    let res = finalize st$_c$ m$_3$ in
    let conv = (m$_1$,$\arrayVar[2]{m}$,m$_3$) in
    (conv,(res,())).
\end{lstlisting}
\vspace{-1em}
\caption{Our Concrete Oblivious Transfer Protocol.\label{fig:someot}}
\end{figure}

We prove this protocol secure in the standard model via a
reduction to the decisional Diffie-Hellman assumption and an
entropy-smoothing assumption on the hash function.
%
We let $\mathsf{Adv}^{\mathsf{DDH}}(\A)$ and
$\mathsf{Adv}^{\mathsf{ES}}(\A)$ be the advantage of an adversary
$\mathcal{A}$ breaking the DDH and the Entropy Smoothing assumptions,
respectively.

\begin{theorem}[$\mathsf{OT}$-security of \textsf{SomeOT}]
\label{thm:SomeOT-SIMCPA}
For all $i \in \{1,2\}$ and $\mathsf{OT}^i$ adversary $\A_i$ of type
$\Ad^\mathsf{OT}_i$ against the \textsf{SomeOT} protocol, we can
construct two efficient adversaries $\mathcal{D}^{\mathsf{DDH}}$ and
$\mathcal{D}^{\mathsf{ES}}$, and a efficient simulator $\Sim$ such
that
$$
\mathsf{Adv}^{\mathsf{OT}^i}_{\mathsf{SomeOT},\Sim}(\A_i) \leq \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{DDH}}(\mathcal{D}^{\mathsf{DDH}}) + \mathsf{n} \cdot \mathsf{Adv}^{\mathsf{ES}}(\mathcal{D}^{\mathsf{ES}}).
$$
\end{theorem}
\begin{proof}[Proof (Sketch)]
We first reduce to $n$-ary variants of the assumptions, constructing
adversaries $\mathcal{D}^{\mathsf{DDHn}}$ and
$\mathcal{D}^{\mathsf{ESn}}$. Security against malicious senders ($i =
1$) is information theoretic, since the sender's view of a protocol
execution is statistically indistinguishable from a random view. To
prove security against malicious choosers ($i = 2$), we consider a
simulator $\Sim$ that replaces the chosen components of the
ciphertexts $\arrayVar{e}_0$ and $\arrayVar{e}_1$ (in \ec{step}$_2$)
by random bitstrings. The proof that the real world is computationally
close to the ideal world follows the following strategy:
$\mathcal{D}^{\mathsf{DDHn}}$ is used to justify the replacement of
the public keys corresponding to chosen messages by random group
elements, and $\mathcal{D}^{\mathsf{ESn}}$ to replace the hash of
random group elements by random bitstrings. We further reduce both
$n$-ary variants to the corresponding standard assumption using a
single generic lemma described below.
\end{proof}

\heading{Using Generic Lemmas}
In the proof of Theorem~\ref{thm:SomeOT-SIMCPA}, both reductions first go to
$n$-ary versions of the DDH and Entropy-Smoothing hypotheses before reducing
these further to standard assumptions. Both of these two ``$n$-ary to unary''
reductions are in fact proved by simply applying a generic lemma, which we
formalize independently of these particular applications as part of \EasyCrypt's
library of verified transformations. The objective of this library is to
formalize often-used proof techniques once and for all, enabling the user to
perform proofs ``by a hybrid argument'', or ``by eager sampling'', whilst
formally checking that all side conditions are fulfilled at the time the lemma
is applied.
%
We now describe the generic hybrid argument used in the proof of
Theorem~\ref{thm:SomeOT-SIMCPA} and others.

\begin{figure}[ht]
\begin{lstlisting}[mathescape,language=easycrypt,xleftmargin=0pt,xrightmargin=0pt,style=easycrypt-pretty]
type input, output, inleaks, outleaks. 

module type Orcl = { proc o(_:input) : output }.

module type Orcl$_b$ = {
  proc leaks(_:inleaks): outleaks
  proc o$_L$(_:input) : output
  proc o$_R$(_:input) : output
}.

module type $\Ad^{\mathsf{Hy}}$ (O$_b$:Orcl$_b$, O:Orcl) = { proc main () : bool }.

module L$_n$ (O$_b$:Orcl$_b$, $\A$:$\Ad^{\mathsf{Hy}}$) = {
  module O: Orcl = { $\ldots$ } (* increment C.c and call O$_b$.o$_L$ *)
  module $\A$' = $\A$(O$_b$, O);
  proc main () : bool = { C.c = 0; return $\A$'.main(); }
}.
module R$_n$ (O$_b$:Orcl$_b$, $\A$:Adv) = { $\ldots$ (* Same as L$_n$ but use O$_b$.o$_R$ *) }.

op q : int.

module $\B$($\A$:$\Ad^{\mathsf{Hy}}$, O$_b$:Orclb, O:Orcl) = {
  module LR = {
    var l, l$_0$ : int  
    proc orcl(m:input):output = {
      var r : output;
      if (l$_0$ < l) r = O$_b$.o$_L$(m);
      else if (l$_0$ = l) r = O.orcl(m);
      else r = O$_b$.o$_R$(m);
      l = l + 1; return r;
    }    
  }
  module $\A$' = $\A$(O$_b$,LR)
  proc main():outputA = {
    var r:outputA;
    LRB.l$_0$ =$ [0..q-1]; LRB.l  = 0; return $\A$'.main();
  }
}.

lemma Hybrid: forall (O$_b$:Orcl$_b${C,$\B$}) ($\A$:$\Ad^{\mathsf{Hy}}$ {C,$\B$,O$_b$}), 
  Pr[Ln(O$_b$,$\A$): result /\ C.c $\leq$ n] - Pr[Rn(O$_b$,$\A$): result /\ C.c $\leq$ n]
  = q * (Pr[Ln(O$_b$,$\B$($\A$)): result /\ $\B$.l $\leq$ n /\ C.c $\leq$ 1]
       - Pr[Rn(O$_b$,$\B$($\A$)): result /\ $\B$.l $\leq$ n /\ C.c $\leq$ 1]).
\end{lstlisting}
\vspace{-1em}
\caption{Abstract Definitions for Hybrid Argument.}
\label{fig:hybrid}
\end{figure}
% $

As described in Figure~\ref{fig:hybrid}, consider an adversary
parametrized by two modules. The first parameter \ec{O}$_b$,
implementing the module type \ec{Orcl}$_b$, provides a leakage oracle,
a left oracle \ec{o}$_L$ and right \ec{o}$_R$. The second parameter
\ec{O}, implementing module type \ec{Orcl}, provides a single oracle
\ec{o}.  The goal of an adversary implementing type
$\Ad^{\mathsf{Hy}}$ is to guess in at most \ec{n} queries to \ec{O.o}
if it is the left oracle \ec{O}$_b$\ec{.o}$_L$ or the right oracle
\ec{O}$_b$\ec{.o}$_R$. To express the advantage of such an adversary,
we write two modules: the first one, \ec{L}$_n$, defines a game where
the adversary is called with \ec{O.o} equal to \ec{O}$_b$\ec{.o}$_L$,
the second one, \ec{R}$_n$, uses \ec{O}$_b$\ec{.o}$_R$ instead. Both
\ec{L}$_n$ and \ec{R}$_n$ use a variable \ec{C.c} to count the number
of queries made to their oracle by the adversary. We define the
advantage of an adversary $\A$ in distinguishing \ec{O}$_b$\ec{.o}$_L$
from \ec{O}$_b$\ec{.o}$_R$ as the difference of the probability of
games \ec{L}$_n$\ec{(O}$_b$\ec{,}$\A$\ec{)} and
\ec{R}$_n$\ec{(O}$_b$\ec{,}$\A$\ec{)} returning 0. Given any
distinguishing adversary $\A$, we construct a distinguishing adversary
$\B$ that may use $\A$ but always makes at most one query to oracle
\ec{O.o}. 

The \ec{Hybrid} lemma relates the advantages of any
adversary $\A$ with the advantage of its constructed adversary $\B$
when $\A$ is known to make at most \ec{q} queries to \ec{O.o}. Note
that the validity of the \ec{Hybrid} lemma is restricted to
adversaries that do not have a direct access to the counter \ec{C.c},
or to the memories of \ec{B} and \ec{O}$_b$, this is denoted by the
notation $\Ad^{\mathsf{Hy}}$\ec{\{C,B,O}$_b$\ec{\}} in the \EasyCrypt
code. Other lemmas shown in this paper also have such restrictions in
their formalizations, but they are as expected (that is, they simply
enforce a strict separation of the various protocols', simulators' and
adversaries' memory spaces) and we omit them for clarity.
%
The construction of $\B$ is generic in the underlying
adversary $\A$, which can remain completely abstract. We underline
that, for all $\A$ implementing module type $\Ad^{\mathsf{Hy}}$, the
partially-applied module $\B(\A)$ implements $\Ad^{\mathsf{Hy}}$ as
well and can therefore be plugged in anywhere a module of type
$\Ad^{\mathsf{Hy}}$ is expected. This ability to generically construct
over abstract schemes or adversaries is central to handling modularity
in \EasyCrypt.

Finally, we observe that the \ec{Hybrid} lemma applies even to an adversary that
may place queries to the individual \ec{O}$_b$\ec{.o}$_L$ and
\ec{O}$_b$\ec{.o}$_R$ oracles. It is of course applicable (and is in
fact often applied) to adversaries that do not place such queries.
