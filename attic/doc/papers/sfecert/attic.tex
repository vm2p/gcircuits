Also, none of the previous works considers verified implementations,
with the exception of~\cite{Almeida:2013}.  Indeed, this challenge has
been partially addressed in {\sf CryptoVerif} through the
implementation of a code generation mechanism and in
\textsf{EasyCrypt} through a connection with the \textsf{CompCert}
verified compiler~\cite{Almeida:2013}. However, the mechanism used by
\textsf{CryptoVerif} is complex, due to the gap between its input
language and the output language, i.e.\, Objective Caml, and
significantly expands the Trusted Computing Base (although proved
formally, its correctness relies on a 100 pages pen-and-paper
proof). On the other hand, the mechanism used by \EasyCrypt provides
strong guarantees, but introduces a significant amount of additional
proof work.



\section{Conclusions and future work}\label{sec:conclusion}
We have presented a version of the \EasyCrypt tool that supports
modules and theories and shown how this permits dealing with various
forms of composition that arises in cryptographic proofs.  We believe
that our results demonstrate the great potential of leveraging the
programming language community's know-how on formalizing and verifying
compositional proofs for software systems to address the
compositionality of cryptographic proofs.  The example we present
demonstrates that our techniques suffice to deal with the common forms
of additive composition (cf. the introduction) that are employed in
most cryptographic proofs, including simulation-based proofs and
hybrid arguments.

The obvious direction for future work is to tackle stronger
compositional results such as general non-destructive
compositionality, which has recently been identified as a major
challenge for cryptography itself~\cite{LandwehrBMBLL12}.  The natural
starting point are the existing theoretical frameworks that deal with
this issue, namely the Universal Composability~\cite{Canetti:2001} and
indifferentiability~\cite{Maurer:2004} frameworks.  We believe that
the mechanized support for cryptographic proofs provided by \EasyCrypt
is not only mature enough to deal with the formal approach adopted in
the above frameworks (modulo a natural increase in formalization
complexity), but also supported by solid theoretical foundations on
programming languages that may shed new light over the limitations and
positive aspects of different approaches to obtaining stronger
compositional guarantees for crypographic protocols.


For instance, the security proof
  for the SFE protocol is obtained in three
  steps: \begin{inparaenum}[i.] \item prove the security of concrete
    garbled circuits and oblivious transfer protocols; \item prove
    generically the security of the overall protocol assuming the
    security of its sub-components, when these are seen as
    abstractions; \item prove by instantiation of the generic proof
    the security of a concrete implementation that runs the formally
    verified sub-protocols.\end{inparaenum}~


\paragraph{\bf Issue 1 -- Modularity.}
The first issue is the description of the different components that
build up the protocols. Consider for instance Yao's SFE
protocol,\footnote{Note that Yao's protocol offers security against
  passive, i.e., honest-but-curious adversaries that are assumed to
  correctly execute the protocol.}  where one assumes that the
function $f$ to be evaluated is given as a circuit composed of a
standard Boolean gates.  Informally, the protocol consists of three
major steps:
\begin{inparaenum}[i.]
\item party $P_1$ holding $x_1$ garbles the circuit for $f$ and 
sends the garbled circuit to party $P_2$ along with a garbled 
encoding of its input,
\item both parties engage in an oblivious transfer sub-protocol that
enables $P_2$ to obtain a garbled encoding of its own input
(and only valid for that input) in such a way that $P_1$ learns
nothing about $x_2$; and
\item party $P_2$ evaluates the garbled circuit, an operation
which is guaranteed to reveal no more information than the correct
output $f(x_1,x_2)$.
\end{inparaenum} 

The level of abstraction at which one would expect to specify such protocols 
requires the verification tool to offer a level of expressivity and modularity 
that is quite similar to that of a general purpose programming language, but 
with the additional ability to account for probabilistic computations.

\paragraph{\bf Issue 2 -- Structural Complexity.}
The second challenge lies in proving that the sub-protocols actually
meet the prescribed functionality and security requirements. These
sub-protocols are themselves advanced cryptographic primitives, and
their proof must be constructed from elementary computational security
components and security assumptions such as the existence of secure
symmetric ciphers and computational hardness assumptions over discrete
groups.

For garbled circuits, for example, assuming that encryption is sufficiently 
strong, the security intuition is that, unless one explicitly reveals the 
association between Boolean values and secret keys used to encode those values
(e.g., the garbled input to the circuit) then the evaluation of the
garbled circuit is oblivious: it reveals no information about the true 
Boolean values that would be computed in various circuit wires if the 
evaluation were to be carried out in the clear.
%
This intuition is captured formally using indistinguishability-based 
definitions, and the security proof proceeds using a generic {\em hybrid 
argument} in which an arbitrary (albeit polynomial) number of reductions to an
underlying assumption must be composed to derive indistinguishability
at the high-level. 

This implies that the verification tool provides
users with the ability to define a generic security game and a generic
adversary, which can be parametrised to carry out the reduction at
each of the elementary steps, and to derive the high-level result from
the hybrid argument. In order to carry this reasoning, the
verification tool must therefore support parametrized proofs and
mechanisms to reason by induction on the structure of circuits --- quite
similar to inductive proofs in a proof assistant, but with the
additional ability to reason about probabilistic claims.

\gnote{explain reusing the text below why the examples are well-chosen
  and what are the main difficulties, insist on quantification with
  modules, ambient logic} \gnote{say something about F7 and TLS at the
  end of the intro, for instance: our theorems are formally verified}


In this paper, we initiate the formal verification of mechanisms for
computing securely in the cloud, by proving formally the correctness
of implementation-level descriptions of protocols\footnote{In this setting, a protocol can be understood as some form of distributed virtual machine.} for secure
function evaluation (SFE) and verifiable computation (VC). 
Broadly construed, the goal of SFE is to allow two parties to compute a
function $f$ on two inputs $x_1$ and $x_2$, only known to the first
and second parties respectively, without revealing to the other party
nothing else than $f(x_1,x_2)$. In contrast, the goal of VC is to
allow the efficient verification of a computation outsourced to an
untrusted worker; to achieve this goal, VC mechanisms request that a
worker given a function $g$ to be computed on input $y$, returns a
result $g(y)$ and an efficiently verifiable proof $\pi$ that $g(y)$
was computed correctly. 

The realization of these mechanisms involves advanced cryptographic tools: 
for instance, Yao~\cite{Yao86} shows how to realize SFE using garbled circuits and 
oblivious transfer protocols, whereas Gennaro, Gentry and Parno~\cite{GGP10} show how 
to realize VC using garbled circuits and fully homomorphic encryption (FHE).\footnote{The interested reader will find more detailed descriptions of these protocols in Appendix~\ref{app:background}.}
The structural complexity of these protocols and the distinctive type of 
arguments required in the proofs of correctness and security, place both 
constructions out of reach of existing computer-aided verification tools; 
the fact that our goal is to push the formal guarantees down to 
implementations of the protocols further widens the existing gap.

\heading{Challenges for formal verification} 
The two protocols we selected as use cases in this paper provide a
perfect example of the challenges placed by advanced cryptographic
constructions and security proofs when the goal is formal
verification.


\ \\
None of these challenges is addressed satisfactorily by previous tools
for computer-aided cryptography tools. To tackle them, we rely 
%   ~The module system is complemented by a
%theory mechanism that supports 

   {\color{red}{Gilles: explain theory mechanism here or not?}}\\
   {\color{blue}{Manuel: I would say no.}}


\heading{Summary of contributions} 
%
The outstanding contribution of this paper is a formally verified
security proof for an implementation-level description of the Secure
Function Evaluation protocol of Yao~\cite{Yao86}.
This proof constitutes a significant advance with respect to the
state-of-the-art, and shows the feasibility of reasoning about the kind
of complex cryptographic protocols used in the context of secure 
multi-party computation and its practical application to cloud computing.

To support this general claim, we apply the same techniques to the Verifiable
Computation protocol of Gennaro, Gentry and Parno~\cite{GGP10}. 
In this case we cannot claim that we reason about an implementation-level 
description of the {\em entire} protocol, since one of the required components 
is an efficient FHE, which we leave uninstantiated.
Nevertheless, this second use case is extremely interesting from a formal 
verification point of view because, on one hand, it allows us to reuse 
essentially all of the development supporting garbled circuits and, on the
other hand, it illustrates our ability to formal reason about high-level 
cryptographic components offering complex functionalities in a modular way.


%\ \\
%{\color{blue}{Manuel: Not sure where the material below should go}}
%
%\heading{Contribution: machine-support for compositional security proofs}
%We present a rigorous, yet flexible, machine-supported framework for compositional cryptographic proofs. 
%Our framework retains the style and guarantees of pen-and-paper proofs, while guaranteeing a correct usage of compositional principles.
%Technically, we achieve our goal by extending \EasyCrypt~\cite{Barthe:2011a}, an extant tool for reasoning about the security of cryptographic constructions, with theories and modules, two standard mechanisms to achieve modularity in 
%formal verification tools and typed functional languages. 
%Theories provide a means to abstract from concrete mathematical structures and reason
%instead on classes of structures that satisfy a concrete
%property. Modules, on the other hand, provide a natural means to reflect the
%logical design of cryptographic systems and their implementations.
%Through quantification over modules and a careful design of the
%underlying reasoning engine, \EasyCrypt allows formalizing complex cryptographic
%proofs that were previously out of reach for verified
%cryptography. More specifically, the support for modules extends the
%capabilities of \EasyCrypt to deal with proof techniques that are key
%to achieving secure composition smoothly:
%\begin{inparaenum}[i.] \item general hybrid arguments where security 
%games can be treated as parametrisable modules; \item simulation-based
%proofs where simulators and attackers can be seen as interchangeable
%adversarial algorithms, which can either be concrete, existentially
%quantified, or universally quantified, and; \item layered security
%proofs in which general compositional theorems can be proven using
%abstract views of cryptographic primitives, and instantiations of
%these primitives can be proven secure down to one or more
%computational assumptions.\end{inparaenum}
%
%Modules have an additional benefit: they allow to build libraries in
%which standard cryptographic tools are proved once and for all in an
%abstract setting, and then instantiated seamlessly in proofs of
%concrete constructions. This form of \emph{small-scale} composition
%induces a shift of perspective in verified security: rather than
%repeatedly using low-level formalisms---such as the probabilistic
%relational Hoare logic of \EasyCrypt---to prove the validity of
%instances of general cryptographic results, users can perform their
%proofs by direct applications of these results, in a style that is
%arguably much closer to common cryptographic practice.


\ \\
{\color{blue}{Manuel: Not sure what to say about "validate the design of easycrypt"}}

flow policies in separation kernels, and was since considerably
expanded and reinforced by considering other types of execution
platforms, including microkernels, virtual machines and web browsers.
However, for their most part, these formal verifications have been
conducted on models of the execution platforms, whereas one ultimately
would like to prove security for implementations.  \footnote{This can be achieved via rigorous
  model extraction (from implementation to a model); via code
  generation (from a model to an implementation); or by reasoning
  directly about implementations. We adopt code generation.}



There is a significant and widely acknowledged gap between provable
security and applied cryptography. On the one hand,


The recent media coverage of various vulnerabilities that were
introduced (in some cases intentionally) in high-profile software
applications has raised awareness to the fact that our society
critically relies on the effectiveness of the security mechanisms
deployed in our ITC infrastructure.  This strongly justifies efforts
to formalize the operational behavior of computational platforms and
applications in order to verify that they provide the required
guarantees.


Cryptographic proofs are typically structured into multiple smaller
steps: proving the security of a construction from the
security of its components, reducing the security of a component to a
specialized but technically convenient assumption, and showing
that the specialized assumption follows from standard computational
assumptions. Reductionist arguments are generally used to prove most,
if not all, steps in isolation. Then, general composition principles
are invoked to conclude the proof. Unfortunately, the application of
composition theorems is error-prone, as witnessed by the recent
literature.


The notions of security we adopt in the paper are asymptotic. This
means that all algorithms are assumed receive as input a security
parameter; that the execution times of all (abstract) adversaries and
simulators is assumed to be polynomial in the security parameter; and
that the definitions of security are expressed by imposing that
adversarial advantage is negligible when expressed as a function of
the security parameter. 

In the \EasyCrypt formalization, the security parameter is implicit,
which can be justified by the fact that our claims hold for {\em all}
values of the security parameter. For this reason, and for simplicity
of presentation, we omit the security parameter altogether from the
presentation in the remainder of the paper. Moreover, security claims
in \EasyCrypt 

 where the $\Adv_\B(\lambda)$ advantage term
may indeed represent a neglibible term, but only if algorithm $\B$
executes in polynomial time (which it must do in the cases where $\A$
is ppt).

We also observe that, in the semantics of \EasyCrypt, quantification
over adversarial algorithms is {\em not} restricted to ppt
adversaries.  This makes existential claims such as the existence of
an {\em efficient} simulator difficult to express, which explains why
we do not use existential quantification in our claims.  (Instead, we
either quantify universally, or refer to explicitly to concrete
algorithms.)

This issue is {\em not} a restriction for universally quantified claims, 
since they will of course apply to the subset of ppt adversaries.
Nevertheless, this has some  implications for the style in which 
security claims must be expressed.
For example, it makes no sense to state that, for all adversaries $\A$,
the probability that some computational security guarantee is broken
is upper bounded by some small real value $\epsilon < 1$. Such
a claim will only make sense if the security guarantee is {\em statistical}.
Computational security claims must instead be 

Finally, we observe that there is no way to explicitly state in 
\EasyCrypt a claim of ppt execution time for a concrete algorithm,
so the only way to check that our reductions and simulations are
indeed performed in polynomial time is by direct inspection of the code.
