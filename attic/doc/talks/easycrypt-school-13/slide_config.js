var SLIDE_CONFIG = {
  // Slide settings
  settings: {
    title: 'An EasyCrypt formalization of Garbled Circuits',
    subtitle: 'How to prove a "compiler" in easycrypt',
    //eventInfo: {
    //  title: 'Google I/O',
    //  date: '6/x/2013'
    //},
    useBuilds: true, // Default: true. False will turn off slide animation builds.
    usePrettify: true, // Default: true
    enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
    enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
    //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
    favIcon: 'images/imdea_simple.png',
    fonts: [
      'Open Sans:regular,semibold,italic,italicsemibold',
      'Source Code Pro'
    ],
    //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
  },

  // Author information
  presenters: [{
    name: 'Guillaume Davy',
    company: 'IMDEA Software, ENS Cachan'
  }, {
    name: '',
    company: ' Joint work with :'
  }, {
    name: 'José Carlos Bacelar Almeida',
    company: 'Universidade do Minho'
  }, {
    name: 'Manuel Bernardo Barbosa',
    company: 'Universidade do Minho'
  }, 
  {
    name: 'Gilles Barthe',
    company: 'IMDEA Software'
  }, {
    name: 'François Dupressoir',
    company: 'IMDEA Software'
  }, {
    name: 'Pierre-Yves Strub',
    company: 'IMDEA Software'
  }
  ]
};

