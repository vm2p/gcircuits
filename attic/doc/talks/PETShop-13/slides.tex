\documentclass[usenames,dvipsnames]{beamer}

\usepackage[utf8]{inputenc}

\usetheme{Strub}

\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}

\setbeamersize{text margin left=6mm, text margin right=6mm}
\setbeamercovered{transparent=10}

\author[]{%
José~Bacelar~Almeida\and%
Manuel~Barbosa\and%
Gilles~Barthe\and%
Guillaume~Davy\and%
François~Dupressoir\and%
Benjamin~Grégoire\and%
\emph{{Pierre-Yves}~Strub}}

\title[EasyCrypt / Garbled Circuits]{Towards an EasyCrypt Formalization\\ of Garbling Schemes}

\date{November 4, 2013}
\subtitle{}
\institute{IMDEA Software Institute - INRIA - Universidade do Minho}

\usepackage{listings}
\usepackage{xcolor}

\input{defs.tex}
\input{defs1.tex}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\begin{frame}
 \frametitle{}

\alert{Garbled circuits}

\begin{itemize}
 \item Cryptographic technique for implementing secure function evaluation.
   Have been formalized (Bellare et al.) in a generic framework: garbling schemes.
\end{itemize}

\medskip

\begin{block}{}
In this work (in progress):

\begin{itemize}
 \item Formalization of dual-key ciphers, garbling schemes,
 \item Formalization of their security in the \alert{new} EasyCrypt,
   an interactive proof tool geared towards formalizing game-based
   cryptographic security proofs.
\end{itemize}
\end{block}
\end{frame}

\frame{\tableofcontents}

\section{Garbled Circuits}
\subsection{Definition}

\begin{frame}
\frametitle{Garble Scheme}

\begin{center}
 \includegraphics{img/garbling}
\end{center}
\end{frame}

\begin{frame}
\frametitle{From Garble Scheme to Garble Circuits}

We focused on boolean functions represented a (DAG) boolean circuits:

\begin{center}
 \includegraphics{img/gcircuit}
\end{center}

\begin{itemize}
 \item 2 inputs, 1 output per gate,
 \item no restriction on gates,
 \item no feedback,
 \item no pass-through, ...
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{From Garble Scheme to Garble Circuits}

Circuits garbling is based on the \alert{Dual Key Cipher} primitive:

\medskip

\begin{center}
 \includegraphics[scale=.8]{img/dkc}
\end{center}
\end{frame}

\begin{frame}
\frametitle{From Garble Scheme to Garble Circuits}

Garbling a circuit, an example:

\begin{center}
 \includegraphics{img/garbled}
\end{center}
\end{frame}

\subsection{Security notion for Garbling}
\frame{\tableofcontents[currentsubsection]}

\begin{frame}
\frametitle{Security notion for Garbling}

Indistinguishability-based privacy:

\medskip

\begin{center}
 \includegraphics{img/prvind}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Security notation for Garbling}

Can be reduced to a security assumption on DKC:

\medskip

\begin{center}
 \includegraphics{img/dkcsec}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Security notation for Garbling}

Sketch of the reduction:

\medskip

\begin{center}
 \includegraphics[scale=1.2]{img/indprivred}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Security notation for Garbling}

Partial garbling:

\bigskip

\begin{center}
 \includegraphics[width=.7\textwidth]{img/red}
\end{center}
\end{frame}

\section{The EasyCrypt Assistant}

\subsection{Generalities}
\frame{\tableofcontents[currentsubsection]}

\begin{frame}
\frametitle{The EasyCrypt Assistant}

\begin{block}{}
\begin{itemize}
 \item EasyCrypt is a tool-assisted platform for proving security of
   cryptographic constructions in the computational model
 \item Views cryptographic proofs as relational verification of open
   parametric probabilistic programs
\end{itemize}
\end{block}

\begin{itemize}
\item Leverage PL and PV techniques for cryptographic proofs
\item Be accessible to cryptographers (choice of PL)
\item Reuse off-the-shelf verification tools (Why3)
\item Support high-level reasoning principles
\item Provide reasonable level of automation
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The EasyCrypt Assistant}

\begin{center}
 \includegraphics[scale=1.2]{img/easycrypt}
\end{center}
\end{frame}

\begin{frame}{Evolution}
Started in 2009. One older brother (CertiCrypt), started 2006. 
\begin{itemize}
\item At first, mostly automated proofs 
\item v0.2 Interactive proofs in pRHL
\item v1.0 Modular proofs, all layers explicit and with support for
  interactive proofs
\end{itemize}
\begin{warningblock}{}
 Garble dev. dictated the dev. of the v1.0. Still needs to work on 
\begin{itemize}
\item increasing automation
\item high-level proof steps
\item small(er) TCB
\item \ldots
\end{itemize}
\end{warningblock}
\end{frame}

\begin{frame}{EasyCrypt: Languages}
Typed imperative language
$$\begin{array}{rcl@{\qquad}l}
\C
   & ::= & \Skip & \mbox{skip}\\
    &\mid& \Ass{\V}{\E} & \mbox{assignment}\\
   &\mid& \Rand{\V}{\mathcal{D}} &\mbox{random sampling}\\
   &\mid& \Seq{\C}{\C} & \mbox{sequence}\\
   &\mid& \Cond{\E}{\C}{\C} & \mbox{conditional}\\
&\mid& \While{\E}{\C} & \mbox{while loop}\\
   &\mid& \Call{\V}{\F}{\E,\ldots,\E} & \mbox{procedure call} 
  \end{array}
$$ 

Expression language:
\begin{itemize}
\item features first-class distributions \ec{'a distr}
\item allows higher-order expressions
\end{itemize}
\end{frame}

\begin{frame}{EasyCrypt: Logics}
\begin{itemize}
\item Ambient higher-order logic 
\item Hoare Logic $\Hoare{c}{P}{Q}$
\item Probabilistic Hoare Logic
$$\HoareLe{c}{P}{Q}{\delta} \quad\HoareEq{c}{P}{Q}{\delta} 
\quad\HoareGe{c}{P}{Q}{\delta} $$

\item Probabilistic Relational Hoare Logic
$\Equiv{c_1}{c_2}{P}{Q}$
\end{itemize}
\begin{block}{}
\begin{itemize}
\item Logics serve complementary purposes 
\item Some overlaps, many interplays
%% $$\infer{
%% \Equiv{c_1}{c_2}{P_1\sidel\wedge P_2\sider}{Q_1\sidel\wedge Q_2\sider}
%% }{
%% \HoareEq{c_1}{P_1}{Q_1}{1}\qquad \HoareEq{c_2}{P_2}{Q_2}{1}
%% }
%% $$
\item HL, pHL, pRHL embedded in ambient logic
\end{itemize}
\end{block}
\end{frame}

\begin{frame}[fragile]{EasyCrypt: modules and theories}
Modules
\begin{itemize}
\item Instantiating generic transformations (simplified syntax)
\begin{easycrypt}[]{}
forall &m (A <: AdvCCA), exists (B <: AdvCPA), 
     Pr[CCA(FO(S),A) @ &m : b' = b ] <=  
     Pr[CPA(S,B) @ &m : b' = b] + ....
\end{easycrypt}
\item Supporting high-level reasoning steps
\end{itemize}

\medskip

Theories
\begin{itemize}
\item Supports code reuse
\item \lq\lq Polymorphism\rq\rq\ via abstract types
\item \lq\lq Quantification\rq\rq\ via abstract operators
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Benefits}
\begin{itemize}
\item In old EasyCrypt, reasoning principles are \alert{embedded}\\ in
pRHL proofs for the concrete construction\\[.5em]
\item In the new version, one can
\begin{itemize}
\item prove high-level principles in an abstract setting
\item instantiate principles\\[.5em]
\end{itemize}
Benefits: much easier! Also favours
\begin{itemize}
\item libraries of verified high-level principles
\item better proofs (shorter, faster, more robust)
\end{itemize}
\end{itemize}
\end{frame}

\subsection{EasyCrypt in practice (examples)}
\frame{\tableofcontents[currentsubsection]}

\begin{frame}[t,fragile]
Modules in practice: hybrid garbling definition

\medskip

\begin{easycrypt}[]{}
module GInit(Flag:Flag_t) = {
  module Flag = Flag(Gf)
  fun init() : unit = {
    var tok : token;
    ...
    while (G.g < C.n + C.q)
    {
      ...
      Flag.gen();
      Gf.garb(getTok R.xx G.g C.v.[G.g], false, false);
      tok = Gf.garbD(F.flag_ft, false,  true);
      tok = Gf.garbD(F.flag_tf,  true, false);
      ...
    }    
  }
}.
\end{easycrypt}
\end{frame}

\begin{frame}[t,fragile]
High-level steps in practice: Secure Encryption to Secure Scheme.

\vspace{2cm}

\begin{easycrypt}[]{}
lemma equivPrvInd
  (A <: PrvIndSec.Adv_t) (S1 S2 <: PrvIndSec.Scheme_t):
    equiv[S1.enc ~ S2.enc: ... ==> ={res}] =>
    equiv[PrvIndSec.Game(S1, A).main
          ~ PrvIndSec.Game(S2, A).main: ... ==> ={res}].

\end{easycrypt}
\end{frame}


\section{Conclusion}
\frame{\tableofcontents[currentsection]}

\begin{frame}
\frametitle{Conclusion}

\begin{itemize}
 \item We have presented EasyCrypt, a tool for a tool-assisted platform
   for proving security of cryptographic constructions in the computational model,
   via the example of Garble Circuits.\\[1em]

  \item As of today, we have:\\[.3em]
  \begin{itemize}
  \item defined the hybrid games, and proved that both ends correspond to the real and fake garble schemes,\\[.5em]
  \item proved that a Secure Function Evaluation can be done from a Secure Garbling Scheme and a Secure Oblivious Transfer,\\[.5em]
  \item proved secure a concrete implementation of SFE under the assumption that our Garble Scheme is secure.
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Conclusion}

Yet,

\medskip

\begin{itemize}
\item The reduction to the DKC security has to be completed,
\item The reduction from the DKC security to PRP/AES is not done. 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Conclusion}

EasyCrypt is available at:

\begin{center}
 \texttt{http://www.easycrypt.info/}
\end{center}
\end{frame}

\end{document}
