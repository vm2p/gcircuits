require import Pair.
require import Bool.
require import Real.
require import Distr.

require import Enc.

theory EncSecurity.
  clone import Encryption.

  module type Rand_t = {
     fun gen(l:randgenin): rand
  }.

  (* Definitions needed for IND model *)
  type query_IND = plain * plain.

  op queryValid_IND(query : query_IND) =
     valid_plain (fst query) /\ 
     valid_plain (snd query) /\
     leak (fst query) = leak (snd query).

  module type Adv_IND_t = {
    fun gen_query(): query_IND
    fun get_challenge(cipher:cipher): bool
  }.

  module Game_IND(R:Rand_t, ADV:Adv_IND_t) = {
    fun main(): bool = {
      var query:query_IND;
      var p:plain;
      var c:cipher;
      var b,adv,ret:bool;
      var r:rand;
    
      query = ADV.gen_query();
      if (queryValid_IND query)
      {
        b = ${0,1};
        p = if b then snd query else fst query;
        r = R.gen(randfeed p);
        c = enc p r;
        adv = ADV.get_challenge(c);
        ret = (b = adv);
      }
      else
        ret = ${0,1};
      return ret;
    }
  }.

  (* Definitions needed for SIM model *)
  type query_SIM = plain.
  op queryValid_SIM(query:query_SIM) =
     valid_plain query.

  pred pi_sampler_works (x:unit) =
    forall (plain : plain),
      valid_plain plain =>
      let leakage = leak plain in
      let pi = pi_sampler leakage in
        leak pi = leakage /\ valid_plain pi.

  module type Adv_SIM_t = {
    fun gen_query():query_SIM
    fun get_challenge(cipher:cipher): bool
  }.

  module type Sim_SIM_t = {
    fun sim(leakage:leakage): cipher
  }.

  module Game_SIM(R:Rand_t, SIM:Sim_SIM_t, ADV:Adv_SIM_t) = {
    fun main(): bool = {
      var query:query_SIM;
      var c:cipher;
      var real, adv:bool;
      var r:rand;
    
      query = ADV.gen_query();
      real = ${0,1};

      if (!queryValid_SIM query)
         adv = $Dbool.dbool;
      else
      {
        if (real)
        {
          r = R.gen(randfeed query); 
          c = enc query r; 
        }
        else c = SIM.sim(leak query);
        adv = ADV.get_challenge(c);
      }
      return (real = adv);
    }
  }.

  module Game_SIM'(R:Rand_t, SIM:Sim_SIM_t, ADV:Adv_SIM_t) = {
    fun main(): bool*bool = {
      var query:query_SIM;
      var c:cipher;
      var real, adv:bool;
      var r:rand;
    
      query = ADV.gen_query();
      real = ${0,1};

      if (!queryValid_SIM query)
         adv = $Dbool.dbool;
      else
      {
        if (real)
        {
          r = R.gen(randfeed query); 
          c = enc query r; 
        }
        else c = SIM.sim(leak query);
        adv = ADV.get_challenge(c);
      }
      return (real, real = adv);
    }
  }.

 module SIM(R:Rand_t): Sim_SIM_t = {
   fun sim(leakage:leakage): cipher = {
       var pi:plain;
       var c:cipher;
       var r:rand;

       pi = pi_sampler leakage;
       r = R.gen(randfeed pi);
       c = enc pi r;
       return c;
   }
 }.

 section.
 (*******************************************)
 (* IND => SIM proof                        *)
 (*******************************************)
 declare module R:Rand_t.

 declare module A:Adv_SIM_t {R}.
 axiom AgenL: islossless A.gen_query.
 axiom AgetL: islossless A.get_challenge.

 (* Building an IND adversary from a SIM adversary *)
 module RedSI(A:Adv_SIM_t) = {

   fun gen_query():query_IND = {
       var plain0:plain;
       var plain1:plain;
       var leakage0:leakage;

       plain0 = A.gen_query();
       leakage0 = leak plain0;
       plain1 = pi_sampler leakage0;
       return (plain1,plain0);
   }

   fun get_challenge(cipher : cipher) : bool = {
       var answer:bool;

       answer = A.get_challenge(cipher);
       return answer;
   }
 }.

 (* Losslessness properties *)
 lemma RedSIgenL : islossless RedSI(A).gen_query.
 proof strict.
 by fun; wp; call AgenL.
 qed.

 lemma RedSIgetL : islossless RedSI(A).get_challenge.
 proof strict.
 by fun; wp; call AgetL.
 qed.

 (* The RedSI(A) runs the A in exactly the SIM model environment *)
 local lemma indadv_equiv (A<:Adv_SIM_t{R}):
  pi_sampler_works () =>
  equiv [ Game_IND(R,RedSI(A)).main ~ Game_SIM(R,SIM(R),A).main:
            ={glob A,glob R} ==> ={res} ].
 proof strict.
 intros=> pi_samplable; fun.
 inline RedSI(A).gen_query.
 seq  4  1: (={glob A,glob R} /\ plain0{1} = query{2} /\ 
             leakage0{1} = leak plain0{1} /\ plain1{1} = pi_sampler leakage0{1} /\ 
             query{1} = (plain1{1}, plain0{1}));
  first by wp; call (_: ={glob R})=> //; fun true.
 case (queryValid_SIM query{2}).
  rcondt{1} 1; first by intros=> &m; skip; smt.
  seq  1  1: (={glob A, glob R} /\ plain0{1} = query{2} /\ 
              leakage0{1} = leak plain0{1} /\ plain1{1} = pi_sampler leakage0{1} /\ 
              query{1} = (plain1{1}, plain0{1}) /\ queryValid_SIM query{2}/\ b{1} = real{2});
   first by rnd; wp.
  rcondf{2} 1; first by intros=> &m; skip; smt.
  inline RedSI(A).get_challenge.
  case b{1}.
   rcondt{2} 1=> //.
   wp;call (_: ={glob A, glob R, cipher} ==> ={glob R, res}); first by fun (={glob R})=> //; fun true.
   wp; call (_: ={glob R, l} ==> ={glob R, res}); first by fun true.
   by wp; skip; smt.
  rcondf{2} 1=> //; inline SIM(R).sim.
  wp; seq  1  2: (={glob A, glob R} /\ b{1} = real{2} /\ p{1} = pi{2} /\ leakage{2} = leak p{1});
    first by wp; skip; smt.
  wp; call (_: ={glob A, glob R, cipher} ==> ={glob R, res}); first by fun (={glob R})=> //; fun true.
  wp; call (_: ={glob R, l} ==> ={glob R, res}); first by fun true.
  by wp; skip; smt.
 
 rcondf{1} 1; first by intros=> &m; skip; smt.
 rcondt{2} 2; first by intros=> &m; rnd.
 seq  0  1: true; first by rnd{2}; wp; skip; smt.
 case (real{2}).
  by rnd; skip; smt.
 by rnd (lambda x, !x); skip;smt.
 qed.

 lemma simadv_indadv &m:
  pi_sampler_works () =>
  Pr[Game_IND(R,RedSI(A)).main()@ &m:res] = Pr[Game_SIM(R,SIM(R),A).main()@ &m:res].
 proof strict.
 by intros=> pi_samplable; equiv_deno (indadv_equiv A _).
 qed.

 lemma ind_implies_sim &m:
  pi_sampler_works () =>
   `|2%r * Pr[Game_SIM(R,SIM(R),A).main()@ &m:res] - 1%r| <=
    `|2%r * Pr[Game_IND(R,RedSI(A)).main()@ &m:res] - 1%r|.
 proof strict.
 intros=> pi_samplable.
 rewrite -(simadv_indadv &m) //.
 qed.
end section.

(************************************************************)
(* Important lemmas for using SIM security in other proofs  *)
(************************************************************)
lemma SGame_equiv (R <: Rand_t) (S <: Sim_SIM_t) (A <: Adv_SIM_t{R,S}):
 equiv [ Game_SIM(R, S, A).main ~ Game_SIM'(R, S, A).main: 
         ={glob R, glob S, glob A} ==> res{1} = snd res{2} ].
proof strict.
fun.
seq 2 2: (={query, real, glob R, glob S, glob A}).
 rnd; call (_: ={glob A} ==> ={res, glob A}); first by fun true.
 by skip; progress.
case (queryValid_SIM query{1}); last first.
 rcondt{1} 1; first by intros &m; skip.
 rcondt{2} 1; first by intros &m; skip.
 rnd; skip; smt.
rcondf{1} 1; first by intros &m; skip.
rcondf{2} 1; first by intros &m; skip.
case (real{1}).
 rcondt{1} 1; first by intros &m; skip.
 rcondt{2} 1; first by intros &m; skip.
 call (_: ={glob A, cipher} ==> ={res}); first by fun true.
 wp; call (_: ={glob R, l} ==> ={res}); first by fun true.
 by skip.
rcondf{1} 1; first by intros &m; skip.
rcondf{2} 1; first by intros &m; skip.
call (_: ={glob A, cipher} ==> ={res}); first by fun true.
wp; call (_: ={glob S, leakage} ==> ={res}); first by fun true.
by wp; skip.
qed.

lemma SGame_pr (R <: Rand_t) (S <: Sim_SIM_t) (A <: Adv_SIM_t{R,S}) &m:
 Pr [Game_SIM(R, S, A).main() @ &m : res] 
 =  Pr [Game_SIM'(R, S, A).main() @ &m : snd res].
proof strict.
by equiv_deno (SGame_equiv R S A).
qed.

lemma SGame_lossless (R <: Rand_t) (S <: Sim_SIM_t) (A <: Adv_SIM_t):
 islossless R.gen =>
 islossless S.sim =>
 islossless A.gen_query =>
 islossless A.get_challenge =>
 islossless Game_SIM'(R, S, A).main.
proof strict.
intros=> R_ll S_ll A_gen_ll A_chall_ll; fun.
seq 2: true=> //;
 first by rnd; call A_gen_ll; skip; smt.
case (queryValid_SIM query).
 rcondf 1=> //; call A_chall_ll.
 case (real).
  by rcondt 1=> //; wp; call R_ll.
  by rcondf 1=> //; call S_ll; wp.
 rcondt 1=> //; by rnd; wp; skip; smt.
qed.

lemma SGame_true_pr (R <: Rand_t) (S <: Sim_SIM_t) (A <: Adv_SIM_t) &m:
 islossless R.gen =>
 islossless S.sim =>
 islossless A.gen_query =>
 islossless A.get_challenge =>
 Pr [Game_SIM'(R, S, A).main() @ &m : true] = 1%r.
proof strict.
by intros=> R_ll S_ll A_gen_ll A_chall_ll;
   bdhoare_deno (SGame_lossless R S A _ _ _ _).
qed.

lemma SGame_real_pr (R <: Rand_t) (S <: Sim_SIM_t) (A <: Adv_SIM_t) &m:
 islossless R.gen =>
 islossless S.sim =>
 islossless A.gen_query =>
 islossless A.get_challenge =>
 Pr [Game_SIM'(R, S, A).main() @ &m : fst res] = (1%r/2%r).
proof strict.
intros R_ll S_ll A_gen_ll A_chall_ll.
bdhoare_deno (_: true ==> fst res)=> //; fun.
seq 1: true 1%r (1%r/2%r) 0%r _=> //;
 first by call A_gen_ll.
seq 1: real (1%r/2%r) 1%r _ 0%r=> //.
  rnd; skip; progress; by rewrite Dbool.mu_def.
 case (queryValid_SIM query); last first.
  rcondt 1=> //; rnd; skip; progress.
  rewrite Dbool.mu_def /= /charfun /= !fst_pair; smt.
 by rcondf 1=> //; rcondt 1=> //; call A_chall_ll; wp; call R_ll.
 hoare; case (queryValid_SIM query).
  by rcondf 1=> //; rcondf 1=> //; conseq* (_: true ==> true).
  by rcondt 1=> //; rnd.
qed.

lemma SGame_Nreal_pr (R <: Rand_t) (S <: Sim_SIM_t) (A <: Adv_SIM_t) &m:
 islossless R.gen =>
 islossless S.sim =>
 islossless A.gen_query =>
 islossless A.get_challenge =>
 Pr [Game_SIM'(R, S, A).main() @ &m : !(fst res)] = 1%r/2%r.
proof strict.
intros=> R_ll S_ll A_gen_ll A_chall_ll.
rewrite Pr mu_not.
rewrite (SGame_true_pr R S A &m) //.
rewrite (SGame_real_pr R S A &m) //.
cut H: 1%r = 2%r / 2%r by smt.
by rewrite {1}H; smt.
qed.

lemma ADV_xpnd (R <: Rand_t) (S <: Sim_SIM_t) (A <: Adv_SIM_t{R,S}) &m:
 islossless R.gen =>
 islossless S.sim =>
 islossless A.gen_query =>
 islossless A.get_challenge =>
 2%r * Pr [ Game_SIM(R, S, A).main() @ &m : res] - 1%r
 = 2%r * ( Pr [ Game_SIM'(R, S, A).main() @ &m : !fst res => snd res]
           - Pr [ Game_SIM'(R, S, A).main() @ &m : fst res => !snd res] ).
proof strict.
intros=> R_ll S_ll A_gen_ll A_chall_ll.
rewrite (SGame_pr R S A &m).
cut ->: ( Pr [ Game_SIM'(R, S, A).main() @ &m : !fst res => snd res]
          = Pr [ Game_SIM'(R, S, A).main() @ &m : fst res]
            + Pr [ Game_SIM'(R, S, A).main() @ &m : snd res]
            - Pr [ Game_SIM'(R, S, A).main() @ &m : snd res && fst res]).
 cut ->: (Pr[Game_SIM'(R, S, A).main() @ &m : !fst res => snd res]
          = Pr[Game_SIM'(R, S, A).main() @ &m : snd res \/ fst res]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; smt.
cut ->:  Pr [ Game_SIM'(R, S, A).main() @ &m : fst res => !snd res]
  = Pr [ Game_SIM'(R, S, A).main() @ &m : !fst res] + 1%r
    - Pr [ Game_SIM'(R, S, A).main() @ &m : snd res]
    - Pr [ Game_SIM'(R, S, A).main() @ &m : !snd res && !fst res].
 cut ->: (Pr[Game_SIM'(R, S, A).main() @ &m : fst res => !snd res]
         = Pr[Game_SIM'(R, S, A).main() @ &m : !snd res \/ !fst res]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; rewrite Pr mu_not.
 rewrite (SGame_true_pr R S A &m) //; smt.
rewrite (SGame_Nreal_pr R S A &m) // (SGame_real_pr R S A &m) //.
cut H1: forall (x y:real), x - y = x + [-] y by smt.
cut H2: forall (x y:real), [-] (x + y) = [-] x + [-] y by smt.
cut H3: forall (x:real), [-] ([-] x) = x by smt.
cut H4: forall (x:real), 2%r * x = x + x.
 intros x; cut ->:2%r = (1%r + 1%r) by smt.
 by rewrite Mul_distr_r.
cut H4': forall (x y:real), 2%r * (x-y) = 2%r * x - 2%r * y by smt.
cut H5: forall (x:real), 2%r * -x = - 2%r * x by smt.
rewrite ?H1 ?H2 ?H3.
cut ->: (1%r / 2%r + Pr[Game_SIM'(R,S,A).main() @ &m : snd res] +
 -Pr[Game_SIM'(R,S,A).main() @ &m : snd res && fst res] +
 (-(1%r / 2%r) + -1%r + Pr[Game_SIM'(R,S,A).main() @ &m : snd res] +
  Pr[Game_SIM'(R,S,A).main() @ &m : !snd res && !fst res]))
 = ( (Pr[Game_SIM'(R,S,A).main() @ &m : snd res]
   + Pr[Game_SIM'(R,S,A).main() @ &m : snd res]) - 1%r)
   - (Pr[Game_SIM'(R,S,A).main() @ &m : snd res && fst res] -
      Pr[Game_SIM'(R,S,A).main() @ &m : !snd res && !fst res]) by smt.
rewrite -H4 H4'.
cut ADV_xpand' : 2%r * Pr [ Game_SIM'(R, S, A).main() @ &m : snd res] - 1%r
  = 2%r * ( Pr [ Game_SIM'(R, S, A).main() @ &m : snd res && fst res]
        - Pr [ Game_SIM'(R, S, A).main() @ &m : !snd res && !fst res] ).
 cut Hpr1: Pr [ Game_SIM'(R, S, A).main() @ &m : snd res]
            = Pr [ Game_SIM'(R, S, A).main() @ &m : snd res && fst res]
             + Pr [ Game_SIM'(R, S, A).main() @ &m : snd res && !fst res].
  cut ->: Pr[Game_SIM'(R, S, A).main() @ &m : snd res]
           = Pr[Game_SIM'(R, S, A).main() @ &m
               : snd res && fst res \/ snd res && !fst res].
   rewrite Pr mu_eq; smt.
  rewrite Pr mu_disjoint; smt.
 cut Hpr2 : Pr [ Game_SIM'(R, S, A).main() @ &m : snd res && !fst res]
            = Pr [ Game_SIM'(R, S, A).main() @ &m : !fst res]
            - Pr [ Game_SIM'(R, S, A).main() @ &m : !snd res && !fst res].
  cut ->: Pr[Game_SIM'(R, S, A).main() @ &m : !fst res]
           = Pr[Game_SIM'(R, S, A).main() @ &m 
               : snd res && !fst res \/ !snd res && !fst res].
   rewrite Pr mu_eq; smt.
  rewrite Pr mu_disjoint; smt.
 rewrite Hpr1 Hpr2 (SGame_Nreal_pr R S A &m) //.
 smt.
rewrite -ADV_xpand' //.
smt.
save.

end EncSecurity.
