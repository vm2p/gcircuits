require import Real.
require import Int.
require import FMap. import OptionGet.
require import FSet.
require import Array.
require import Distr.
require import Bool.
require import Pair.
require import Monoid.
require import Option.
require import Means.

require import GarbleTools.

require        Sch.
require        SchSec.
require        DKCSec.

import ForLoop.

theory DKCScheme.  
(* BEGIN ABSTRACT DEFINITION *)
  require ExtWord.
  clone import ExtWord as W.

  op bound : int.
  axiom boundInf : bound > 1.
(* END ABSTRACT DEFINITION *)

(* BEGIN MEANS CLONE *)
   clone Mean as MeanInt with
     type input <- int,
     type output <- bool,
     op d <- Dinter.dinter 0 bound.

   clone Mean as MeanBool with
     type input <- bool,
     type output <- bool,
     op d <- Dbool.dbool.
(* END MEANS CLONE *)

(* BEGIN INTRO *)
  clone import DKCSec.DKCSecurity with theory W = W.
  clone import Tweak with theory W = W.

  theory Local.
    (* some types reused for garbling scheme definition  *)
    type tokens_t = (int*bool, word) map.
    type 'a gates_t = (int*bool*bool, 'a) map.
    type 'a funct_t = topo_t * 'a gates_t.

    (* Some small function that facilitate the definition of scheme operatots *)
    op validGG (n q:int) (gg:bool gates_t) =
      range n (n + q) true (lambda g b,
                              b /\
                              in_dom (g, false, false) gg /\
                              in_dom (g, false, true) gg /\
                              in_dom (g, true, false) gg /\
                              in_dom (g, true, true) gg).

    op validCircuit (bound:int) (f:bool funct_t) =
      let (n, m, q, aa, bb) = fst f in
      1 < n /\ 0 < m /\ 0 < q /\ m <= q /\
      n + q - m = bound /\
      length aa = n + q /\ length bb = n + q /\ validGG n q (snd f) /\
      (range n (n + q) true (lambda i b,
                               b /\
                               0 <= aa.[i] /\
                               bb.[i] < i /\
                               bb.[i] < n+q-m /\
                               aa.[i] < bb.[i])).

    pred validCircuitP (bound:int) (f:bool funct_t) =
      let (n, m, q, aa, bb) = fst f in
      1 < n /\ 0 < m /\ 0 < q /\ m <= q /\
      n + q - m = bound /\
      Array.length aa = n + q /\ Array.length bb = n + q /\
      (forall g i1 i2, n <= g < n + q => in_dom (g, i1, i2) (snd f)) /\
      (forall i, n <= i < n+q =>
         0 <= aa.[i] /\
         bb.[i] < i /\
         bb.[i] < n+q-m /\
         aa.[i] < bb.[i]).

    lemma valid_wireinput (bound:int) (f:bool funct_t):
      validCircuit bound f <=>  validCircuitP bound f.
    proof strict.
    rewrite /validCircuitP /validCircuit /validGG.
    elim/tuple5_ind (fst f)=> n m q aa bb valF /=.
    (* simplifying the calls to range with boolean accumulators to forall *)
    cut ->: (lambda (g0:int) (b:bool), b /\
               in_dom (g0,false,false) (snd f) /\
               in_dom (g0,false,true ) (snd f) /\
               in_dom (g0,true ,false) (snd f) /\
               in_dom (g0,true ,true ) (snd f)) =
            (lambda (g0:int) (b:bool), b /\
               (lambda k,
                  in_dom (k,false,false) (snd f) /\
                  in_dom (k,false,true ) (snd f) /\
                  in_dom (k,true, false) (snd f) /\
                  in_dom (k,true ,true ) (snd f)) g0) by smt.
    cut ->: (lambda i b, b /\ 0 <= aa.[i] /\ bb.[i] < i /\ bb.[i] < n + q - m /\ aa.[i] < bb.[i]) =
            (lambda i b, b /\
               (lambda k, 0 <= aa.[k] /\ bb.[k] < k /\  bb.[k] < n + q - m /\ aa.[k] < bb.[k]) i) by smt.
    rewrite !rangeb_forall //=.
    progress; last 4 by apply H5.
    by cut:= H5 g _=> //=; case i1; case i2; smt.
    qed.

    op initGates (base:int) size (f:int->bool->bool->'a) : 'a gates_t =
      let fill = lambda g a b gg, gg.[(g, a, b) <- f g a b] in
      range base (base + size) FMap.Core.empty
            (lambda (g:int) gg, fill g false false (fill g false true (fill g true false (fill g true true gg)))).

    lemma get_initGates (base:int) size (f:int->bool->bool->'a) g a b :
      0 <= size =>
      (initGates base size f).[(g, a, b)] = if base <= g < base + size then Some (f g a b) else None.
    proof strict.
    intros=> h.
    simplify initGates.
    simplify FMap.OptionGet."_.[_]".
    generalize g.
    elim/Induction.strongInduction size=> //.
    progress; case (j > 0)=> hj; last smt.
    rewrite range_ind_lazy /=; first smt.
    rewrite !FMap.Core.get_set.
    case (g = base + j - 1)=> hg; first by case a; case b; smt.
    cut neq: forall x y, ((base + j - 1, x, y) = (g, a, b)) = false by smt.
    rewrite !neq /=.
    cut -> : base + j - 1 = base + (j - 1) by smt.
    rewrite H0; smt.
    qed.

    lemma in_dom_init_Gates base size (f:int->bool->bool->'a) (g:int) (a b:bool) :
      0 <= size =>  base <= g < base + size => in_dom (g, a, b) (initGates base size f).
    proof strict.
    intros h.
    simplify initGates.
    simplify in_dom.
    generalize g.
    elim/Induction.strongInduction size=> //.
    progress.
    case (j > 0)=> hj; last smt.
    rewrite range_ind_lazy /=; first smt.
    rewrite ! dom_set ! mem_add.
    case (g = base + j - 1)=> hg; first by case a;case b;smt.
    cut -> : base + j - 1 = base + (j - 1) by smt.
    rewrite H0; smt.
    qed.

    op enc (x:tokens_t) (fn:bool funct_t) (g:int) (x1:bool) (x2:bool) : word =
      let (n, m, q, aa, bb) = fst fn in
      let t1 = (getlsb (proj x.[(aa.[g], false)])) in
      let t2 = (getlsb (proj x.[(bb.[g], false)])) in
      let aa = (proj x.[(aa.[g], x1^^t1)]) in
      let bb = (proj x.[(bb.[g], x2^^t2)]) in
      E (tweak g (getlsb aa) (getlsb bb)) aa bb (proj x.[(g, proj (snd fn).[(g, x1^^t1, x2^^t2)])]).
  end Local.
  import Local.
(* END INTRO *)

    (* Is this relevant in the EC formalization? *)
    (* Francois: My guess is that it is relevant because we need to know that the "tweak" function is injective,
                 and therefore that Token.from_int is called only on integers less than 2^token_length - 1 *)
    op maxGates: int = 2^62 - 1.

(* BEGIN SCHEME DEFINITION *)
  clone Sch.Scheme as C1 with
    type Input.input_t = bool array,
    type Input.inputK_t = tokens_t,
    type Input.inputG_t = word array,
    op Input.encode (iK:inputK_t) (x:input_t) =
      init (length x) (lambda k, proj iK.[(k, x.[k])]),
    op Input.inputG_len(x : word array) = Array.length x.

  clone C1 as C2 with
    type fun_t = bool funct_t,
    type funG_t = word funct_t,
    type output_t = bool array,
    type outputK_t = unit,
    type outputG_t = word array,
    type leak_t = topo_t,
    type rand_t = tokens_t,

    op validInputs (fn:fun_t) (i:Input.input_t) =
      let (n, m, q, aa, bb) = fst fn in
      
      n + q <= maxGates /\
      validCircuit bound fn /\ length i = n,

    pred validRand (fn:fun_t) (x:rand_t) =
      let (n, m, q, aa, bb) = fst fn in
      forall (i:int), 0 <= i < (n + q)%Int =>
        option_rect false (lambda (x:word), true) x.[(i,false)] /\
        option_rect false (lambda (x:word), true) x.[(i,true )] /\
        !getlsb (proj x.[(i,false)]) = getlsb (proj x.[(i,true )]) /\
        (n + q - m <= i => !(getlsb (proj x.[(i,false)]))),

    op phi (fn:fun_t) = fst fn,

    op eval (fn:fun_t) (i:Input.input_t) =
      let (n, m, q, aa, bb) = fst fn in
      Array.sub (evalComplete q i (extract (lambda g x1 x2, (proj (snd fn).[(g, x1, x2)])) aa bb)) (n+q-m) m,

    op evalG (fn:funG_t) (i:Input.inputG_t) =
      let (n, m, q, aa, bb) = fst fn in
      let evalGate =
        lambda g x1 x2,
          D (tweak g (getlsb x1) (getlsb x2)) x1 x2 (proj (snd fn).[(g, getlsb x1, getlsb x2)]) in
      Array.sub (evalComplete q i (extract evalGate aa bb)) (n+q-m) m,

    op funG (fn:fun_t) (r:rand_t) = 
      let (n, m, q, aa, bb) = fst fn in
      (fst fn, initGates n q (enc r fn)),

    op inputK (fn:fun_t) (r:tokens_t) =
      let (n, m, q, aa, bb) = fst fn in
      filter (lambda x y, 0 <= fst x < n) r,

    op outputK (fn:fun_t) (r:rand_t) = tt,

    op decode(k:outputK_t, o:outputG_t) =
      map getlsb o,

    op pi_sampler(im : (topo_t * Input.input_t)) =
      let (n,m,q,aa,bb) = fst im in
      let gg = initGates n q (lambda g (a b:bool), if g  < n + q - m then false else (snd im).[g-(n+q-m)]) in
      let x = init n (lambda k, false) in
      ((fst im, gg), x).

   clone import SchSec.SchSecurity with theory Sch.Scheme = C2.

   import SchSecurity.Sch.Scheme.
(* END SCHEME DEFINITION *)

(* BEGIN THE CORRECTION PROOF *)
   lemma sch_correct : D.Correct () => Sch.Correct ().
   proof strict.

   (*Some simplification before proving the main inductive formula *)
   simplify D.Correct Sch.Correct.
   simplify    validInputs    validRand    eval    decode    outputK    evalG    funG          encode    inputK.
   simplify C2.validInputs C2.validRand C2.eval C2.decode C2.outputK C2.evalG C2.funG.
   intros DKCHyp x fn input.
   elim/tuple2_ind fn=>topo ff h_fn/=.
   elim/tuple5_ind topo=>n m q aa bb h_topo /=.
   simplify fst snd.
   rewrite valid_wireinput.
   simplify validCircuitP.
   simplify fst snd.
   progress.

   pose n := length input.

   pose inputEnc := C2.Input.encode (C2.inputK ((n, m, q, aa, bb), ff) x) input.

   pose eval :=(lambda (g : int) (x1 x2 : word),
               D (tweak g (getlsb x1) (getlsb x2)) x1 x2
                 (proj
                    (initGates n q (enc x ((n, m, q, aa, bb), ff))).[(
                    g, getlsb x1, getlsb x2)])).

  (* This is the inductive formula that we want to prove *)
  cut main : forall (j:int), 0 <= j < n+q =>
   proj x.[(j, (evalComplete q input (extract (lambda g x1 x2, proj ff.[(g, x1, x2)]) aa bb)).[j])]
   = (evalComplete q inputEnc (extract eval aa bb)).[j];first last.
    (* End case : As soon as we have this formula for all j we apply it to the output wire and get the final result with the
        decode and outputK definition *)
    (apply array_ext;split;first smt)=> j hj.
    rewrite get_map;first (rewrite Array.length_sub;smt).
    rewrite !get_sub;first 8 smt.
    rewrite -main;first smt.
    pose v := (evalComplete _ _ _).[_].
    by cut := H9 (j + (n + q - m)) _;smt.
  
  intros j boundJ.
  cut : j < n + q;first smt.
  elim/ Induction.strongInduction j;last smt.
  simplify evalComplete.
  pose ar1 := (appendInit input q (extract (lambda (g : int) (x1 x2 : bool), proj ff.[(g, x1, x2)]) aa bb)).
  pose ar2 := (appendInit inputEnc q (extract eval aa bb)).
  intros k kPos hypRec kbound.
  case (n <= k)=> hk.
    (* Induction case : use the correction of one gate and the induction hypothesis*)
    rewrite /ar1 /ar2 ! appendInit_getFinal. smt. smt. simplify extract;smt. smt. smt. simplify extract;rewrite ! get_sub;smt.
    rewrite -/ar1 -/ar2.
    simplify extract.
    cut -> : (k - 1 + 1 = k) by smt.
    rewrite - ! hypRec;first 4 smt.
    simplify eval.
    rewrite get_initGates;first smt.
    simplify enc fst snd.
    cut -> : (
            getlsb (proj x.[(aa.[k], ar1.[aa.[k]])]) ^^
            getlsb (proj x.[(aa.[k], false)])) = ar1.[aa.[k]].
case ar1.[aa.[k]]=> h;last smt.
cut := H9 aa.[k] _;first clear h;smt.
generalize (getlsb (proj x.[(aa.[k], false)]))=> a.
generalize (getlsb (proj x.[(aa.[k], true)]))=> b.
intros [_ [_ [HH _ ]]].
cut -> : b = ! a by smt;smt.
    cut -> : (
            getlsb (proj x.[(bb.[k], ar1.[bb.[k]])]) ^^
            getlsb (proj x.[(bb.[k], false)])) = ar1.[bb.[k]].
case ar1.[bb.[k]]=> h;last smt.
cut := H9 bb.[k] _;first clear h;smt.
generalize (getlsb (proj x.[(bb.[k], false)]))=> a.
generalize (getlsb (proj x.[(bb.[k], true)]))=> b.
intros [_ [_ [HH _ ]]].
cut -> : b = ! a by smt;smt.
case (n <= k < n + q);smt.

  (* Base case : prove main for the inputs by using encode and inputK definitions *)
  rewrite /ar1 /ar2 ! appendInit_get1;first 4 smt.
  rewrite -/ar1 -/ar2.
  simplify inputEnc C2.Input.encode C2.inputK fst.
  rewrite get_init /=;first smt.
  rewrite /FMap.OptionGet."_.[_]".
  rewrite FMap.Core.get_filter /=.
  by (cut -> : 0 <= k < n = true by smt).
qed.
(* END THE CORRECTION PROOF *)


(* BEGIN THE SECURITY PROOF *)

  op evali(fn:fun_t, i:Input.input_t, k:int) =
    let (n, m, q, aa, bb) = fst fn in
    (evalComplete q i (extract (lambda g x1 x2, (proj (snd fn).[(g, x1, x2)])) aa bb)).[k].

  pred validInputsP (plain:fun_t*input_t) =
    let (n, m, q, aa, bb) = fst (fst plain) in
    (n + q)%Int <= maxGates /\ validCircuitP bound (fst plain) /\ Array.length (snd plain) = n.

  (*Contains the random used for a normal garbling *)
  module R = {
    var t:bool array
    var xx:tokens_t
  }.

  (* This is the circuit to garble (fixed after the adversary runs and the challenge is chosen) *)
  module C = {
    var f:fun_t
    var x:input_t
    var n:int
    var m:int
    var q:int
    var aa:int array
    var bb:int array
    var gg:bool gates_t
    var v:bool array
  }.

    (* handle low level part of garbling *)
    module G = {
      var pp:word gates_t
      var yy:word array
      var randG: word gates_t
      var a:int
      var b:int
      var g:int
    }.

  module Rand:EncSecurity.Rand_t = {
    (* ensuring we capture all of C and R's state *)
    module C' = C
    module R' = R
    module G' = G

    fun gen(l:topo_t): rand_t = {
      var i:int;
      var tok1,tok2:word;
      var trnd:bool;
    
      (C.n, C.m, C.q, C.aa, C.bb) = l;
      R.xx = FMap.Core.empty;
      i = 0;
      while (i < C.n + C.q) {
        trnd = $Dbool.dbool;
        trnd = if (i >= C.n + C.q - C.m) then false else trnd;
        tok1 = $Dword.dwordLsb ( trnd);
        tok2 = $Dword.dwordLsb (!trnd);

        R.xx.[(i, false)] = tok1;
        R.xx.[(i,  true)] = tok2;

        i = i + 1;
      }
      return R.xx;
    }
  }.

  section.
    (* This modules contains values set at the beginning and are not modified *)
    module CV = {
      var l : int
    }.

    module Cf = {
      fun init(p:fun_t*input_t) : unit = {
        var i : int;
        C.f = fst p;
        C.x = snd p;
        (C.n, C.m, C.q, C.aa, C.bb) = fst (fst p);
        C.gg = snd (fst p);
        C.v = Array.init (C.n + C.q) (lambda (i:int), false);
        i = 0;
        while (i < C.n + C.q) {
          C.v.[i] = evali (fst p) C.x i;
          i = i + 1;
        }
      }
    }.

    local lemma CinitL: bd_hoare[Cf.init: true ==> true] = 1%r.
    proof strict.
    by fun; while true (C.n + C.q - i); [intros z | ];
       wp; skip; smt.
    qed.

    local lemma CinitH (plain:fun_t*input_t):
      validInputsP plain =>
      hoare[Cf.init: p = plain ==>
                    C.f = fst plain /\
                    C.x = snd plain /\
                    (C.n, C.m, C.q, C.aa, C.bb) = fst (fst plain) /\
                    C.gg = snd (fst plain) /\
                    Array.length C.v = (C.n + C.q) /\
                    validInputsP (C.f, C.x)].
    proof strict.
    timeout 10.
      by progress;fun;while (Array.length C.v = C.n + C.q); wp; skip; smt.
    timeout 3.
    qed.

    local equiv CinitE: Cf.init ~ Cf.init:
      ={p} /\
      (validInputsP p){1} ==>
      ={glob C} /\
      Array.length C.v{1} = (C.n + C.q){1} /\
      C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
      validInputsP (C.f, C.x){1} /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = proj C.gg.[(i, C.v{2}.[C.aa{2}.[i]], C.v{2}.[C.bb.[i]])]){2}.
    proof strict.
    fun; while (
      (Array.length C.v = C.n + C.q){1} /\
      ={glob C, i, p} /\
      fst p{2} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){2} /\
      0 <= C.n{2} /\
      (forall j, C.n <= j < C.n + C.q => 0 <= C.aa.[j] < j){2} /\
      (forall j, C.n <= j < C.n + C.q => 0 <= C.bb.[j] < j){2} /\
      (forall j, C.n <= j < i => C.v{2}.[j] = proj C.gg.[(j, C.v{2}.[C.aa{2}.[j]], C.v{2}.[C.bb.[j]])]){2} /\
      (forall j,   0 <= j < i => C.v{2}.[j] = evali (fst p) C.x j){2} /\
      0 < C.q{2} /\
      length C.x{2} = C.n{2});
    wp; skip.
     (progress;first smt);last smt.
     cut hj   : 0 <= j < length C.v{2} by smt.
     cut haai : 0 <= C.aa{2}.[j] < i{2} by smt.
     cut hbbi : 0 <= C.bb{2}.[j] < i{2} by smt.
     cut haav : 0 <= C.aa{2}.[j] < length C.v{2} by smt.
     cut hbbv : 0 <= C.bb{2}.[j] < length C.v{2} by smt.
     cut haaf : (i{2} = C.aa{2}.[j]) = false by smt.
     cut hbbf : (i{2} = C.bb{2}.[j]) = false by smt.
     rewrite ! get_set // haaf hbbf /=.
     case (j = i{2})=> h;last by smt.
     cut := H5 C.aa{2}.[j] _=> //.
     cut := H5 C.bb{2}.[j] _=> //.
     rewrite h H0 /evali /evalComplete fst_pair snd_pair /= => h1 h2.
     rewrite appendInit_getFinal {1} /extract //=;[smt|rewrite {3} /extract /= !get_sub //;smt|]. 
     cut -> : i{2} - 1 + 1 = i{2} by smt.
     by rewrite -h1 -h2.
    intros &1 &2 [? valid].
    subst p{1}.
    generalize valid.
    simplify validInputsP validCircuitP.
    elim/tuple2_ind p{2}=> x1 x2.
    elim/tuple2_ind x1=> topo gg.
    elim/tuple5_ind topo=> n m q aa bb.
    rewrite !(fst_pair, snd_pair).
    progress;smt.
    qed.

    local equiv CinitE_rnd: Cf.init ~ Cf.init:
      EncSecurity.queryValid_IND (p{1}, p{2}) ==>
      ={C.n, C.m, C.q, C.aa, C.bb} /\
      (forall i, C.n{1} + C.q{1} - C.m{1} <= i < C.n{1} + C.q{1} => C.v{1}.[i] = C.v{2}.[i]) /\
      (eval C.f C.x){1} = (eval C.f C.x){2} /\
      (((C.n, C.m, C.q, C.aa, C.bb), C.gg) = C.f /\
       Array.length C.v = (C.n + C.q) /\
       validInputsP (C.f, C.x)){1} /\
      (forall i, 0 <= i < C.n{1} => C.v{1}.[i] = C.x{1}.[i]) /\
      (((C.n, C.m, C.q, C.aa, C.bb), C.gg) = C.f /\
       Array.length C.v = (C.n + C.q) /\
       validInputsP (C.f, C.x)){2} /\
      (forall i, 0 <= i < C.n{1} => C.v{2}.[i] = C.x{2}.[i]).
    proof strict.
    fun; while (0 <= C.q{2} /\ 0 <= C.n{2} + C.q{2} - C.m{2} /\
                (Array.length C.v = C.n + C.q){1} /\
                (Array.length C.v = C.n + C.q){2} /\
                ={C.n, C.m, C.q, C.aa, C.bb, i} /\
                (eval C.f C.x){1} = (eval C.f C.x){2} /\
                (forall j, C.n{1} + C.q{1} - C.m{1} <= j < i{1} => C.v{1}.[j] = C.v{2}.[j]) /\
                (forall j, 0 <= j < i{1} /\ j < C.n{1}=> C.v{2}.[j] = C.x{2}.[j]) /\
                (((C.n, C.m, C.q, C.aa, C.bb), C.gg) = C.f){1} /\
                (forall j, 0 <= j < i{1} /\ j < C.n{1} => C.v{1}.[j] = C.x{1}.[j]) /\
                (((C.n, C.m, C.q, C.aa, C.bb), C.gg) = C.f){2} /\
                fst (fst p{1}) = fst (fst p{2}) /\
                C.f{1} = fst p{1} /\
                C.f{2} = fst p{2} /\
                (C.n = length C.x){1} /\ (C.n = length C.x){2}); wp; skip.
      progress; first 2 smt.
        rewrite !Array.get_set; first 2 smt.

    simplify evali.
    rewrite -H8.
    rewrite -H9.
    simplify fst snd.
    pose w := (length C.x{1} + C.q{2} - C.m{2}).
    cut: (sub (evalComplete C.q{2} C.x{1} (extract
               (lambda (g : int) (x1 x2 : bool),
                  proj C.gg{1}.[(g, x1, x2)]) C.aa{2} C.bb{2})) w C.m{2}).[i{2}-w] =
         (sub (evalComplete C.q{2} C.x{2} (extract
               (lambda (g : int) (x1 x2 : bool),
                  proj C.gg{2}.[(g, x1, x2)]) C.aa{2} C.bb{2})) w C.m{2}).[i{2}-w]
      by (by generalize H3; rewrite /w;
             simplify eval C2.eval fst snd=> ->).
    case (i{2} = j){2}=> _; rewrite ! Array.get_sub;smt.
        rewrite Array.get_set;first smt.
        by case (i = j){2}=> _;
           rewrite /evali /evalComplete ?MyTools.appendInit_get1; smt.
        rewrite Array.get_set;first smt.
        by case (i = j){2}=> _;
           rewrite /evali /evalComplete ?MyTools.appendInit_get1; smt.
    intros=> &1 &2.

    rewrite /EncSecurity.queryValid_IND
            /EncSecurity.Encryption.valid_plain
            /EncSecurity.Encryption.leak
            /validInputs /C2.validInputs
            !valid_wireinput !fst_pair !snd_pair.
    simplify phi C2.phi eval C2.eval.
    elim/tuple5_ind (fst (fst p)){1}.
    elim/tuple5_ind (fst (fst p)){2}.
    progress;smt.
    qed.

    local module Rf = {
      fun init(useVisible:bool): unit = {
        var i:int;
        var tok1,tok2:word;
        var v,trnd:bool;

        R.t = Array.init (C.n + C.q) (lambda x, false);
        R.xx = FMap.Core.empty;
        i = 0;
        while (i < C.n + C.q) {
          trnd = $Dbool.dbool;
          v = if useVisible then C.v.[i] else false;
          trnd = if (i >= C.n + C.q - C.m) then v else trnd;
          tok1 = $Dword.dwordLsb ( trnd);
          tok2 = $Dword.dwordLsb (!trnd);

          R.t.[i] = trnd;

          R.xx.[(i, v)] = tok1;
          R.xx.[(i,!v)] = tok2;

          i = i + 1;
        }
      } 
    }.

    local lemma RgenInitL: bd_hoare[Rf.init: true ==> true] = 1%r.
    proof strict.
    fun.
    while true (C.n + C.q - i); [intros=> z; wp; do 3!(wp;rnd);wp | wp]; skip.
      progress; cut:= Dword.dwordLsb_lossless;
      rewrite Dbool.mu_def /charfun /weight /cpTrue //=.
      by intros=> rw; rewrite !rw; smt.
      smt.
    qed.

    lemma split_tuple (a a':'a) (b b':'b): (a, b) = (a', b') <=> (a=a' /\ b=b') by smt.

    local lemma RinitH:
      hoare[Rf.init: 0 <= C.m <= C.n + C.q /\
                     fst C.f = (C.n, C.m, C.q, C.aa, C.bb) ==>
                     validRand C.f R.xx /\
                     Array.length R.t = (C.n+C.q)].
    proof strict.
    (fun; while (0 <= C.m <= C.n + C.q /\
                Array.length R.t = C.n + C.q /\
                (forall (j:int), 0 <= j => j < i =>
                  option_rect false (lambda (x : word), true) R.xx.[(j, false)] /\
                  option_rect false (lambda (x : word), true) R.xx.[(j, true)] /\
                  getlsb (proj R.xx.[(j, false)]) <> getlsb (proj R.xx.[(j, true)]) /\
                  (C.n + C.q - C.m <= j => !(getlsb (proj R.xx.[(j, false)])))));
      last wp; skip; progress;last (simplify validRand C2.validRand;
        elim/tuple5_ind (fst C.f{hr})=> n m q aa bb hf /=);smt);last 5 smt.
    wp; do 3!(wp;rnd);wp; skip; simplify FMap.OptionGet."_.[_]";progress;first smt.
    case (j < i{hr})=> h;
      first by rewrite !FMap.Core.get_setN; smt.
    cut -> : i{hr} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (useVisible{hr} && C.v{hr}.[j]); rewrite /= option_rect_some.
    case (j < i{hr})=> h;
      first by rewrite !FMap.Core.get_setN; smt.
    cut ->: i{hr} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (useVisible{hr} && C.v{hr}.[j]);rewrite /= option_rect_some.
    case (j < i{hr})=> h;
      first by rewrite ! FMap.Core.get_setN; smt.
    cut ->: i{hr} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (useVisible{hr} && C.v{hr}.[j]); smt.
    case (j < i{hr})=> h;
      first by rewrite !FMap.Core.get_setN;smt.
    cut ->: i{hr} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (useVisible{hr} && C.v{hr}.[j]); smt.
    qed.

    local equiv RinitE: Rf.init ~ Rf.init: ={useVisible, glob C}  /\ (0 <= C.m <= C.n + C.q /\ fst C.f = (C.n, C.m, C.q, C.aa, C.bb)){1} ==> ={glob R} /\
                    (validRand C.f R.xx /\
                    Array.length R.t = (C.n+C.q)){1}.
    proof strict.
    (fun; while (={useVisible, glob C, glob R, i} /\
                (0 <= C.m <= C.n + C.q /\
                Array.length R.t = C.n + C.q /\
                (forall (j:int), 0 <= j => j < i =>
                  option_rect false (lambda (x : word), true) R.xx.[(j, false)] /\
                  option_rect false (lambda (x : word), true) R.xx.[(j, true)] /\
                  getlsb (proj R.xx.[(j, false)]) <> getlsb (proj R.xx.[(j, true)]) /\
                  (C.n + C.q - C.m <= j => !(getlsb (proj R.xx.[(j, false)]))))){1} );
      last wp; skip; progress;last (simplify validRand C2.validRand;
        elim/tuple5_ind (fst C.f{2})=> n m q aa bb hf /=);smt);last 5 smt.
    wp; do 3!(wp;rnd);wp; skip; simplify FMap.OptionGet."_.[_]";progress;first smt.
    case (j < i{2})=> h;
      first by rewrite !FMap.Core.get_setN; smt.
    cut ->: i{2} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (useVisible{2} && C.v{2}.[j]); rewrite /= option_rect_some.
    case (j < i{2})=> h;
      first by rewrite !FMap.Core.get_setN; smt.
    cut ->: i{2} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (useVisible{2} && C.v{2}.[j]);rewrite /= option_rect_some.
    case (j < i{2})=> h;
      first by rewrite !FMap.Core.get_setN; smt.
    cut ->: i{2} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (useVisible{2} && C.v{2}.[j]); smt.
    case (j < i{2})=> h;
      first by rewrite !FMap.Core.get_setN; smt.
    cut ->: i{2} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (useVisible{2} && C.v{2}.[j]); smt.
    qed.

    local equiv RinitE_rnd: Rf.init ~ Rf.init:
      ={useVisible, C.n, C.m, C.q, C.aa, C.bb} /\
      (forall i, C.n{1} + C.q{1} - C.m{1} <= i < C.n{1} + C.q{1} =>
         C.v{1}.[i] = C.v{2}.[i]) /\
      (0 <= C.m <= C.n + C.q /\ useVisible = true /\ fst C.f = (C.n, C.m, C.q, C.aa, C.bb)){1} ==>
      ={R.t} /\
      (forall g, 0 <= g < (C.n + C.q){1} => R.xx.[(g, C.v.[g])]{1} = R.xx.[(g, C.v.[g])]{2}) /\
      (forall g, 0 <= g < (C.n + C.q){1} => R.xx.[(g, !C.v.[g])]{1} = R.xx.[(g, !C.v.[g])]{2}) /\
      (0 <= C.m <= C.n + C.q /\
       validRand C.f R.xx /\
       Array.length R.t = (C.n+C.q)){1}.
    proof strict.
    fun; while (={useVisible, C.n, C.m, C.q, C.aa, C.bb, i, R.t} /\
                (forall g, 0 <= g < i{1} =>
                  R.xx.[(g, C.v.[g])]{1} = R.xx.[(g, C.v.[g])]{2}) /\
                (forall g, 0 <= g < i{1} =>
                  R.xx.[(g, !C.v.[g])]{1} = R.xx.[(g, !C.v.[g])]{2}) /\
                (0 <= C.m <= C.n + C.q /\
                Array.length R.t = C.n + C.q /\
                (forall (j:int), 0 <= j => j < i =>
                  option_rect false (lambda (x : word), true) R.xx.[(j, false)] /\
                  option_rect false (lambda (x : word), true) R.xx.[(j, true)] /\
                  getlsb (proj R.xx.[(j, false)]) <> getlsb (proj R.xx.[(j, true)]) /\
                  (C.n + C.q - C.m <= j => !(getlsb (proj R.xx.[(j, false)])))) /\
                  useVisible = true /\
                  (forall i, C.n{1} + C.q{1} - C.m{1} <= i < C.n{1} + C.q{1} => C.v{1}.[i] = C.v{2}.[i])){1} );
      last wp; skip; (progress;last simplify validRand C2.validRand);smt.
    wp; case (C.v.[i]{1} = C.v{2}.[i]{1}); [ | swap 4 1];wp;rnd;rnd;wp;rnd;skip;simplify FMap.OptionGet."_.[_]".
    progress;first 5 smt.
    by case (g < i{2})=> h; rewrite !FMap.Core.get_set !split_tuple; smt.
    by rewrite !FMap.Core.get_set !split_tuple /=; smt.
    smt.
    case (j < i{2})=> h;
      first by rewrite !FMap.Core.get_setN; smt.
    cut ->: i{2} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (C.v{1}.[j]); rewrite /= option_rect_some.
    case (j < i{2})=> h;first rewrite !FMap.Core.get_setN;smt.
    cut ->: i{2} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (C.v{1}.[j]);rewrite /= option_rect_some.
    case (j < i{2})=> h;first rewrite !FMap.Core.get_setN;smt.
    cut ->: i{2} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (C.v{1}.[j]); smt.
    case (j < i{2})=> h;first rewrite !FMap.Core.get_setN;smt.
    cut ->: i{2} = j by smt.
    rewrite !FMap.Core.get_set !split_tuple /=.
    by case (C.v{1}.[j]); smt.

    progress;first 3 smt.
      case (i{2} >= C.n{2} + C.q{2} - C.m{2}).
        by intros=> cond; generalize H15; rewrite cond //= H5; smt.
        by rewrite -rw_neqF=> cond; generalize H15; rewrite cond //=.
      smt.
      case (g < i{2})=> h;first rewrite ! FMap.Core.get_setN;smt.
      cut -> : i{2} = g by smt.
      rewrite ! FMap.Core.get_set ! split_tuple /=.
      case (C.v{2}.[g]);smt.
      rewrite !FMap.Core.get_set !split_tuple; smt.
      smt.
      case (j < i{2})=> h;first rewrite ! FMap.Core.get_setN;smt.
      cut -> : i{2} = j by smt.
      rewrite ! FMap.Core.get_set ! split_tuple /=.
      by case (C.v{1}.[j]);rewrite ?xor_false ?xor_true /= option_rect_some.
      case (j < i{2})=> h;first rewrite ! FMap.Core.get_setN;smt.
      cut -> : i{2} = j by smt.
      rewrite ! FMap.Core.get_set ! split_tuple /=.
      by case (C.v{1}.[j]);rewrite ?xor_false ?xor_true /= option_rect_some.
      case (j < i{2})=> h;first rewrite ! FMap.Core.get_setN;smt.
      cut -> : i{2} = j by smt.
      rewrite ! FMap.Core.get_set ! split_tuple /=.
      by case (C.v{1}.[j]);rewrite ?xor_false ?xor_true;smt.
      case (j < i{2})=> h;first rewrite ! FMap.Core.get_setN;smt.
      cut -> : i{2} = j by smt.
      rewrite ! FMap.Core.get_set ! split_tuple /=.
      by case (C.v{1}.[j]);rewrite ?xor_false ?xor_true;smt.
    qed.

    pred t_xor (sup:int) (t1 t2 v:bool array) = forall i,
      0 <= i < sup =>
      t1.[i] = t2.[i] ^^ v.[i].

    local equiv RgenClassicVisibleE:
      Rf.init ~ Rf.init:
        ={glob C} /\ fst C.f{2} = (C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}) /\
        0 <= C.n{2} + C.q{2} /\
        useVisible{1} = true /\
        useVisible{2} = false ==>
        ={R.xx} /\
        (validRand C.f{1} R.xx{1}) /\
        (forall i v, 0 <= i < C.n + C.q => getlsb (proj R.xx.[(i, v)]) = v^^R.t.[i]){2} /\
        Array.length R.t{1} = (C.n{1}+C.q{1}) /\
        t_xor (C.n{1} + C.q{1}) R.t{1} R.t{2} C.v{1}.
    proof strict.
    fun; while (={i, R.xx, glob C} /\
                useVisible{1} = true /\
                useVisible{2} = false /\
                t_xor i{1} R.t{1} R.t{2} C.v{1} /\
                0 <= i{2} /\
                length R.t{1} = C.n{2} + C.q{2} /\
                length R.t{2} = C.n{2} + C.q{2} /\
                (forall j v, 0 <= j < i => getlsb (proj R.xx.[(j, v)]) = v^^R.t.[j]){2} /\ 
                (forall (j:int), 0 <= j < i =>
                  option_rect false (lambda (x : word), true) R.xx.[(j, false)] /\
                  option_rect false (lambda (x : word), true) R.xx.[(j, true)] /\
                  getlsb (proj R.xx.[(j, false)]) <> getlsb (proj R.xx.[(j, true)]) /\
                  (C.n + C.q - C.m <= j => !(getlsb (proj R.xx.[(j, false)])))){2} /\
                (fst C.f = (C.n, C.m, C.q, C.aa, C.bb)){2}).
      case (C.v{1}.[i{1}] = false).
        do !(wp; rnd); skip; progress=> //;
          first 9 (rewrite /t_xor //=; progress=> //; try (cut := H i0); smt).
        simplify FMap.OptionGet."_.[_]";(rewrite ! FMap.Core.get_set get_set;first smt);rewrite ! split_tuple/=;case (i{2} = j)=> h;[cut := H16;cut := H12;case v0=> hv0 /= |cut := H3 j v0 _];smt.
        simplify FMap.OptionGet."_.[_]";rewrite ! FMap.Core.get_set;rewrite ! split_tuple /=;case (i{2} = j)=> h;[ |cut := H4 j _];smt.
        simplify FMap.OptionGet."_.[_]";rewrite ! FMap.Core.get_set;rewrite ! split_tuple /=;case (i{2} = j)=> h;[ |cut := H4 j _];smt.
        simplify FMap.OptionGet."_.[_]";rewrite ! FMap.Core.get_set;rewrite ! split_tuple /=;case (i{2} = j)=> h;[ |cut := H4 j _];smt.
        simplify FMap.OptionGet."_.[_]";rewrite ! FMap.Core.get_set;rewrite ! split_tuple /=;case (i{2} = j)=> h;[ |cut := H4 j _];smt.
        swap{1} 4 1; do 2!(wp; rnd); wp; rnd (lambda x, !x); skip;(progress=> //;first 6 smt).
          by simplify FMap.OptionGet."_.[_]";rewrite FMap.Core.set_set;smt.
          by simplify t_xor; progress; cut:= H i0; smt.
          smt.
          smt.
          smt.
          simplify FMap.OptionGet."_.[_]".
          rewrite !FMap.Core.get_set get_set; first smt.
          rewrite !split_tuple /=; case (i{2} = j)=> h.
            by cut:= H16; cut:= H12; case v0=> hv0 /=; smt.
            by cut:= H3 j v0; smt.
          simplify FMap.OptionGet."_.[_]";rewrite ! FMap.Core.get_set;rewrite ! split_tuple /=;case (i{2} = j)=> h;[ |cut := H4 j];smt.
          simplify FMap.OptionGet."_.[_]";rewrite ! FMap.Core.get_set;rewrite ! split_tuple /=;case (i{2} = j)=> h;[ |cut := H4 j];smt.
          simplify FMap.OptionGet."_.[_]";rewrite ! FMap.Core.get_set;rewrite ! split_tuple /=;case (i{2} = j)=> h;[ |cut := H4 j _];smt.
          simplify FMap.OptionGet."_.[_]";rewrite ! FMap.Core.get_set;rewrite ! split_tuple /=;case (i{2} = j)=> h;[ |cut := H4 j];smt.
      wp; skip; progress=> //;simplify validRand C2.validRand;smt.
    qed.

    module type G_t = {
      fun getInfo() : int*int*bool
    }.

    module Gf : G_t = {
      fun garb(yy:word, alpha:bool, bet:bool): unit = {
        G.pp.[(G.g, (R.t.[G.a]^^alpha), (R.t.[G.b]^^bet))] =
                     (E
                       (tweak G.g (R.t.[G.a]^^alpha) (R.t.[G.b]^^bet))
                       (proj R.xx.[(G.a, (alpha^^C.v.[G.a]))])
                       (proj R.xx.[(G.b, (  bet^^C.v.[G.b]))])
                       yy);
      }

      fun garbD1(rand:bool, alpha:bool, bet:bool): unit = {
        var tok:word;

        tok = $Dword.dword;
        if (rand) G.randG.[(G.g,alpha,bet)] = tok;
      }

      fun garbD2(rand:bool, alpha:bool, bet:bool) : word = {
        var yy:word;

        yy = if rand
             then proj G.randG.[(G.g,alpha,bet)]
             else proj R.xx.[(G.g, (proj C.gg.[(G.g, C.v.[G.a]^^alpha,C.v.[G.b]^^bet)]))];
        garb(yy, alpha, bet);
        return yy;
      }

      fun garbD(rand:bool, alpha:bool, bet:bool): word = {
        var yy : word;

        garbD1(rand, alpha, bet);
        yy = garbD2(rand, alpha, bet);
        return yy;
      }

      fun getInfo(): int*int*bool = {
        return (G.a, G.b, C.gg.[(G.g,(!C.v.[G.a]), false)] = C.gg.[(G.g,(!C.v.[G.a]), true)]);
      }
    }.

    local lemma GgarbL    : islossless Gf.garb    by (fun; wp).

    local lemma GgarbD1L  : islossless Gf.garbD1  by (fun; wp; rnd; skip; smt).

    local lemma GgarbD2L  : islossless Gf.garbD2  by (fun; call GgarbL; wp).

    local lemma GgarbDL   : islossless Gf.garbD   by (fun; call GgarbD2L; call GgarbD1L).

    local lemma GgetInfoL : islossless Gf.getInfo by (fun; wp).

    local equiv GgarbE  : Gf.garb   ~ Gf.garb  :
      ={glob G, C.n, C.m, C.q, C.aa, C.bb, R.t,   yy, alpha, bet} /\
      (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){1} = (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){2} /\
      (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){1} = (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){2} ==>
      ={glob G, res}
    by (fun; wp; skip; smt).

    local equiv GgarbD1E: Gf.garbD1 ~ Gf.garbD1:
      ={glob G, C.n, C.m, C.q, C.aa, C.bb, rand, alpha, bet} ==>
      ={glob G, res}
    by (fun; wp; rnd; skip; smt).

    local equiv GgarbD2E: Gf.garbD2 ~ Gf.garbD2:
      ={glob G, glob C, glob R, rand, alpha, bet} ==>
      ={glob G, res}
    by (fun;call GgarbE;wp).

    local equiv GgarbD2E_rnd: Gf.garbD2 ~ Gf.garbD2:
      ={glob G, C.n, C.m, C.q, C.aa, C.bb, R.t, rand, alpha, bet} /\
      (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){1} = (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){2} /\
      (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){1} = (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){2} /\
      rand{1} = true ==>
      ={glob G, res}
    by (fun; call GgarbE; wp).

    local equiv GgarbDE: Gf.garbD  ~ Gf.garbD:
      ={glob G, glob C, glob R, rand, alpha, bet} ==>
      ={glob G, res}
    by (fun;call GgarbD2E;call GgarbD1E).

    local equiv GgarbDE_rnd: Gf.garbD  ~ Gf.garbD:
      ={glob G, C.n, C.m, C.q, C.aa, C.bb, R.t, rand, alpha, bet} /\
      (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){1} = (R.xx.[(G.a, (alpha^^C.v.[G.a]))]){2} /\
      (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){1} = (R.xx.[(G.b, (  bet^^C.v.[G.b]))]){2} /\
      rand{1} = true ==>
      ={glob G, res}
    by (fun;call GgarbD2E_rnd;call GgarbD1E).

    (* Contains the flag variables used by GInit, their value depends
       of the type of the garbling : Fake, Real, Hybrid *)
    local module F = {
      var flag_ft : bool
      var flag_tf : bool
      var flag_tt : bool
      var flag_sp : bool
    }.

    (* Type of a module that fill the F modules *)
    module type Flag_t(G:G_t) = { fun gen() : unit{ G.getInfo } }.

    (*handle high level part of garbling *)
    local module GInit(Flag:Flag_t) = {
      module Flag = Flag(Gf)

      fun init() : unit = {
        var tok : word;

        G.yy = Array.init (C.n + C.q) (lambda x, (W.zeros));
        G.pp = FMap.Core.empty;
        G.randG = FMap.Core.empty;
        G.a = 0;
        G.b = 0;

        G.g = C.n;
        while (G.g < C.n + C.q)
        {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          Flag.gen();
          Gf.garb(proj R.xx.[(G.g, C.v.[G.g])], false, false);
          tok = Gf.garbD(F.flag_ft, false,  true);
          tok = Gf.garbD(F.flag_tf,  true, false);
          G.yy.[G.g] = Gf.garbD(F.flag_tt,  true,  true);
          if (F.flag_sp) Gf.garb(G.yy.[G.g], true, false);
          G.g = G.g + 1;
        }
      }
    }.

    local lemma GinitL (F <: Flag_t{G}): islossless F(Gf).gen => islossless GInit(F).init.
    proof strict.
    intros GgenL; fun; while true (C.n + C.q - G.g); last by wp; skip; smt.
    intros z; seq 7: (C.n + C.q - G.g = z) 1%r 1%r 0%r _=> //.
      by do 3!call GgarbDL; call GgarbL; call GgenL; wp.
      by if; wp; [call GgarbL | ]; skip; smt.
      by hoare; do !(call (_: true ==> true)=> //); wp.
    qed.

    op todo (a b g bound:int) = 0 <= a /\ b < g /\ b < bound + 1 /\ a < b.

    local lemma GinitE (F1 <: Flag_t {G}) (F2 <: Flag_t {G}) gCV1:
      equiv[F1(Gf).gen ~ F2(Gf).gen:
             ={glob C, glob R, glob G} /\
             (glob CV){1} = gCV1 /\
             (todo G.a G.b G.g bound){1} ==>
             ={glob C, glob R, glob G, glob F}] =>
      equiv[GInit(F1).init ~ GInit(F2).init:
             ={glob C, glob R} /\
             (glob CV){1} = gCV1 /\
             (C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg) /\
             validInputsP (C.f, C.x)){1} ==>
             ={glob G}].
    proof strict.
    intros FgenE; fun.
    while (={glob G, glob C, glob R} /\
           (C.n <= G.g){1} /\
           (C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg) /\
           validInputsP (C.f, C.x)){1} /\
           (glob CV){1} = gCV1); last by wp.
      seq 7 7: (={F.flag_sp, glob G, glob C, glob R, glob F} /\
                C.n{1} <= G.g{1} /\
                (C.f = ((C.n, C.m, C.q, C.aa, C.bb), C.gg) /\
                validInputsP (C.f, C.x)){1} /\
                (glob CV){1} = gCV1);
        first do 3!call GgarbDE; call GgarbE; wp; call FgenE; wp; skip; simplify validInputsP validCircuitP fst snd;progress=> //;smt.
    by if=> //; wp; [ call GgarbE | ]; skip; simplify validInputsP validCircuitP fst snd;progress=> //;progress=> //; smt.
    qed.


    (* Local definition of PrvInd ... BUT WHY? *)
      module type Scheme_t = {
        fun enc(p:fun_t*input_t) : funG_t*inputG_t*outputK_t { }
      }.

      module Game(S:Scheme_t, ADV:EncSecurity.Adv_IND_t) = {
        fun main() : bool = {
          var query : (fun_t*input_t)*(fun_t*input_t);
          var p : fun_t*input_t;
          var c : funG_t*inputG_t*outputK_t;
          var b, adv, ret : bool;

          query = ADV.gen_query();
          if (EncSecurity.queryValid_IND query)
          {
            b = ${0,1};
            p = if b then snd query else fst query;
            c = S.enc(p);
            adv = ADV.get_challenge(c);
            ret = (b = adv);
          }
          else
            ret = ${0,1};
          return ret;
        }
      }.


    (* This is Gb from figure 7 *)
    local module Garble1 : Scheme_t = {
      fun enc(p:fun_t*input_t) : funG_t*inputG_t*outputK_t = {
        Cf.init(p);
        Rf.init(false);
        return
          (funG C.f R.xx, encode (inputK C.f R.xx) C.x, outputK C.f R.xx);
      }
    }.

    (* This is a alternative definition to Rf that will help to do the reduction
        ideally we should use only this definition *)
    module Rf2 = {
      fun initT(useVisible:bool): unit = {
        var i:int;
        var v,trnd:bool;

        R.t = Array.init (C.n + C.q) (lambda x, false);
        i = 0;
        while (i < C.n + C.q) {
          trnd = $Dbool.dbool;
          v = if useVisible then C.v.[i] else false;
          trnd = if (i >= C.n + C.q - C.m) then v else trnd;
          R.t.[i] = trnd;
          i = i + 1;
        }
      } 
      fun initR(useVisible:bool): unit = {
        var i:int;
        var tok1,tok2:word;
        var v,trnd:bool;

        R.xx = FMap.Core.empty;
        i = 0;
        while (i < C.n + C.q) {
          v = if useVisible then C.v.[i] else false;
          trnd = R.t.[i];
          tok1 = $Dword.dwordLsb ( trnd);
          tok2 = $Dword.dwordLsb (!trnd);

          R.xx.[(i, v)] = tok1;
          R.xx.[(i,!v)] = tok2;

          i = i + 1;
        }
      } 
      fun init(useVisible:bool): unit = {
        initT(useVisible);
        initR(useVisible);
      }
    }.
    local lemma Rf2InitTL: islossless Rf2.initT.
    proof strict.
    fun.
    while true (C.n + C.q - i); [intros=> z; wp; rnd | wp]; skip; progress; smt.
    qed.

    local lemma Rf2InitRL: islossless Rf2.initR.
    proof strict.
    fun.
    while true (C.n + C.q - i); [intros=> z; wp; rnd; rnd; wp | wp]; skip; progress; smt.
    qed.

    local lemma Rf2InitL: islossless Rf2.init.
    proof strict.
    fun.
    by call Rf2InitRL;call Rf2InitTL.
    qed.

    local equiv eqRf : Rf.init ~ Rf2.init : ={useVisible} ==> ={glob R}. 
    proof.
      fun.
      admit(* loop fusion *).
    qed.

    module RedComon(A:EncSecurity.Adv_IND_t) = {
      var yy : word array
      var query : (fun_t * input_t) * (fun_t*input_t)
      var c : bool
      fun preInit(): unit = {
        CV.l = $Dinter.dinter 0 bound;
      }
      fun genQuery(rand:bool, alpha:bool, bet:bool): query = {
        var tw : word;
        var gamma, pos : bool;
        var i, j : int*bool;
        var b: bool;
        tw = tweak G.g (R.t.[G.a] ^^ alpha) (R.t.[G.b] ^^ bet);
        gamma = C.v.[G.g] ^^ (proj C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]);
        if (G.a = CV.l)
        {
          pos = true;
          i = (2*G.b, R.t.[G.b]^^bet);
         }
         else
         {
           pos = false;
           i = (2*G.a, R.t.[G.a]^^alpha);
         }
         b = $Dbool.dbool;
         if (rand)
         {
           j = (2*(G.g+C.n+C.q), b);
         }
         else
         {
           j = (2*G.g, R.t.[G.g]^^gamma);
         }
         return (i, j, pos, tw);
      }
      fun doAnswer(rand:bool, alpha:bool, bet:bool, ans:answer): word = {
        var kki, kkj, zz : word;
        var gamma : bool;
        gamma = C.v.[G.g] ^^ (proj C.gg.[(G.g, C.v.[G.a], C.v.[G.b])]);
        (kki, kkj, zz) = ans;
        G.pp.[(G.g, R.t.[G.a]^^alpha, R.t.[G.b]^^bet)] = zz;
        if (G.a = CV.l)
          R.xx.[(G.b, C.v.[G.b]^^bet)] = kki;
        else
          R.xx.[(G.a, C.v.[G.a]^^alpha)] = kki;
        if (! rand)
          R.xx.[(G.g, C.v.[G.g]^^gamma)] = kkj;
        return kkj;
      }
      fun pre(info:bool): unit = {
        var p : fun_t*input_t;
        query = A.gen_query();
        c = $Dbool.dbool;
        p = (if c then snd query else fst query);
        Cf.init(p);
        Rf2.initT(true);
        R.t.[CV.l] = ! info;
      }
      fun coreNAda_pre(): query array = {
        var q : query;
        var qs : query array;
        qs = Array.empty;
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          if (G.a = CV.l)
          {
            q = genQuery(false, true, false);
            q = genQuery(false, true, true);
          }
          if (G.b = CV.l)
          {
            q = genQuery(false, false, true);
            q = genQuery(true, true, true);
          }
          qs = q::qs;
          G.g = G.g + 1;
        }
        return qs;
      }
      fun coreNAda_post(answers:answer array): unit = {
        var ans : int;
        var trash : word;
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          if (G.a = CV.l)
          {
            trash = doAnswer(false, true, false, answers.[ans]);
            ans=ans+1;
            trash = doAnswer(false, true, true, answers.[ans]);
            ans=ans+1;
          }
          if (G.b = CV.l)
          {
            trash = doAnswer(false, false, true, answers.[ans]);
            ans=ans+1;
            yy.[G.g] = doAnswer(true, true, true, answers.[ans]);
            ans=ans+1;
          }
          G.g = G.g + 1;
        }
      }
      fun sampleTokens() : unit = {
        var i : int;
        var tok1, tok2 : word;
        i = 0;
        while (i < C.n + C.q) {
          tok1 = $Dword.dwordLsb ( R.t.[i]);
          tok2 = $Dword.dwordLsb (!R.t.[i]);
          if (R.xx.[(i, false)] = None /\ i <> CV.l)
            R.xx.[(i, false)] = tok1;
          if (R.xx.[(i, true)] = None /\ i <> CV.l)
            R.xx.[(i, true)] = tok1;
          i = i + 1;
        }
      }
      fun garble() : unit = {
        var tok1, tok2 : word;
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          Gf.garb(proj R.xx.[(G.g, C.v.[G.g])], false, false);
          if (G.a <> CV.l /\ G.b <> CV.l)
          {
            tok2 = Gf.garbD(G.a<=CV.l,  true, false);
            tok2 = Gf.garbD(G.b<=CV.l, false,  true);
            tok1 = Gf.garbD(G.a<=CV.l,  true,  true);
            if ((G.a<=CV.l<=G.b) /\ (C.gg.[(G.g, !C.v.[G.a], false)] = C.gg.[(G.g, !C.v.[G.a], true)]))
              Gf.garb(tok1, true, false);
          }
          else
          {
            if (G.a=CV.l)
            {
              tok2 = Gf.garbD(false, false,  true);
            }
            else
            {
              tok2 = Gf.garbD( true,  true, false);
              if (C.gg.[(G.g, !C.v.[G.a], false)] = C.gg.[(G.g, !C.v.[G.a], true)])
                Gf.garb(yy.[G.g], true, false);
              
            }
          }
          G.g = G.g + 1;
        }
      }
      fun post() : bool = {
        var r : bool;
        sampleTokens();
        garble();
        if ((EncSecurity.queryValid_IND query)%EncSecurity)
        {
          r = A.get_challenge((((C.n, C.m, C.q, C.aa, C.bb), G.pp), encode (inputK C.f R.xx) C.x, tt));
(* init C.n (lambda i, proj R.xx.[(i, C.v.[i])])*) 
          r = (r = c);
        }
        else
          r = $Dbool.dbool;
        return r;
      }
   }.

    declare module A : EncSecurity.Adv_IND_t {Rand, DKCm, RedComon}.
    axiom AgenL: islossless A.gen_query.
    axiom AgetL: islossless A.get_challenge.

    (* This lemma makes the correspondance between B&R prvind def used in this file and 
    the one presented in EncSec.ec *)
    local lemma eqDefs:
      equiv[Game(Garble1,A).main ~ EncSecurity.Game_IND(Rand,A).main:
        ={glob A} ==> ={res}].
    proof strict.
    fun.
    seq 1 1 : (={query, glob A});
      first by call (_: ={glob A} ==> ={glob A,res})=> //; fun true.
    if=> //;last by rnd;skip;smt.
    inline Garble1.enc Rf.init Rand.gen Cf.init.
    wp;call (_: ={glob A, cipher} ==> ={res})=> //; first by fun true.
    wp;while (useVisible{1}= false /\ i0{1} = i{2} /\ ={glob A, C.n, C.m, C.q, R.xx});
      first wp;rnd;rnd;wp;rnd;skip;progress;smt.
    wp;while{1} (true) ((C.n + C.q - i){1});first by intros &m z;wp;skip;smt.
    by wp; rnd; skip; progress; smt. (* progress makes it longer, but more stable... Do not remove. *)
    qed.


    (* This is the garble from Hy_l, figure 10 *)
    local module Garble2(F:Flag_t) : Scheme_t = {
      module GI = GInit(F)

      fun enc(p:fun_t*input_t) : funG_t*inputG_t*outputK_t = {
        var tok1, tok2 : word;

        Cf.init(p);
        Rf.init(true);
        GI.init();
        return (
          ((C.n, C.m, C.q, C.aa, C.bb), G.pp),
          encode (inputK C.f R.xx) C.x,
          tt);
      }
    }.

    local lemma Garble2E (F1 <: Flag_t {G, C, R}) (F2 <: Flag_t {G, C, R}) gCV1:
      equiv[F1(Gf).gen ~ F2(Gf).gen:
              ={glob C, glob R, glob G} /\
              (glob CV){1} = gCV1 /\
              (todo G.a G.b G.g bound){1} ==>
              ={glob C, glob R, glob G, glob F}] =>
      equiv[Garble2(F1).enc ~ Garble2(F2).enc:
              ={p} /\
              (glob CV){1} = gCV1 /\
              (validInputsP p{1}){1} ==> ={res}].
    proof strict.
    intros FgenE; fun;
    call (GinitE F1 F2 gCV1 _)=> //;
    call RinitE;
    call CinitE;
    skip=> {FgenE} //=.
    simplify validCircuitP fst;progress=> //;smt.
    qed.

    local module FR(G:G_t) : Flag_t(G) = {
      fun gen() : unit = {
        F.flag_ft = false;
        F.flag_tf = false;
        F.flag_tt = false;
        F.flag_sp = false;
      }
    }.
    local lemma FReal_genL : islossless FR(Gf).gen by (fun; wp).

    local module FF(G:G_t) : Flag_t(G) = {
      fun gen() : unit = {
        F.flag_ft = true;
        F.flag_tf = true;
        F.flag_tt = true;
        F.flag_sp = false;
      }
    }.
    local lemma FFake_genL : islossless FF(Gf).gen by (fun; wp).

    local module FH(G:G_t) : Flag_t(G) = {
      fun gen() : unit = {
        var a, b : int;
        var val : bool;
        (a, b, val) = G.getInfo();

        F.flag_ft = a <= CV.l;
        F.flag_tf = b <= CV.l;
        F.flag_tt = a <= CV.l;
        F.flag_sp = a <= CV.l < b /\ val;
      }
    }.
    local lemma FHybrid_genL : islossless FH(Gf).gen by (fun; wp; call GgetInfoL).

    local equiv equiv_FH_FR: FH(Gf).gen ~ FR(Gf).gen:
      ={glob C, glob R, glob G} /\
      CV.l{1} = -1 /\
      (todo G.a G.b G.g bound){1} ==>
      ={glob C, glob R, glob G, glob F}
    by (fun; inline Gf.getInfo; wp; skip; smt).

    local equiv equiv_FH_FF: FH(Gf).gen ~ FF(Gf).gen:
      ={glob C, glob R, glob G} /\
      (CV.l = bound /\
      todo G.a G.b G.g bound){1} ==>
      ={glob C, glob R, glob G, glob F}
    by (fun; inline Gf.getInfo; wp; skip; smt).

    local equiv equivGReal: Garble2(FH).enc ~ Garble2(FR).enc:
      ={p} /\ CV.l{1} = -1 /\ validInputsP p{1} ==> ={res}
    by (apply (Garble2E FH FR (-1) _); apply equiv_FH_FR).

    lemma get_initGates2 (base:int) size (f:int->bool->bool->'a) g a b :
      0 <= size =>
      get (initGates base size f) (g, a, b) = if base <= g < base + size then Some (f g a b) else None.
    proof strict.
      smt.
    qed.

    lemma tuple3_eq : forall (a d:'a) (e b:'b) (f c:'c), ((a, b, c) = (d, e, f)) <=> (a = d /\ b = e /\ c = f) by smt.

    local equiv equivGarble1: Garble1.enc ~ Garble2(FR).enc:
      ={p} /\ validInputsP p{1} ==> ={res}.
    proof strict.
    fun; symmetry; inline Garble2(FR).GI.init.
    while{1} (
      t_xor (C.n{1} + C.q{1}) R.t{1} R.t{2} C.v{1} /\
      0 <= C.q{2} /\
      C.n{2} <= G.g{1} /\
      C.f{2} = ((C.n{2}, C.m{2}, C.q{2}, C.aa{2}, C.bb{2}), C.gg{2}) /\ validCircuitP bound C.f{2} /\
      let (topo, gg) = funG C.f{2} R.xx{2} in
      ={glob C} /\
       (forall i v, 0 <= i < C.n + C.q => getlsb (proj R.xx.[(i, v)]) = v ^^ R.t.[i]){2} /\
      (forall g u, 0 <= g < (C.n + C.q){1} => R.xx.[(g, u)]{1} = R.xx.[(g, u)]{2}) /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = proj C.gg.[(i, false ^^ C.v{2}.[C.aa{2}.[i]], false ^^ C.v{2}.[C.bb.[i]])]){2} /\
      topo = (C.n{1}, C.m{1}, C.q{1}, C.aa{1}, C.bb{1}) /\
        G.g{1} <= C.n{1} + C.q{1} /\
        (forall i a b, !(G.g{1} <= i < C.n{1} + C.q{1}) => get gg (i, a, b) = get G.pp{1} (i, a, b)))
      ((C.n + C.q - G.g){1}).
      intros &m z.
      inline Gf.garbD.
      inline Gf.garbD1.
      inline Gf.garbD2.
      inline Gf.garb.
      inline GInit(FR).Flag.gen.
      wp.
      swap 17 -16.
      swap 35 -34.
      swap 53 -52.
      wp.
      simplify.
      do 3 ! rnd.
      skip.
      simplify funG C2.funG fst snd t_xor.
      (progress;first 2 smt);last 4 smt.
      case (G.g{hr} = i)=> hi.
        rewrite hi ! Core.get_set get_initGates2;first smt.
        cut -> : C.n{m} <= i < C.n{m} + C.q{m} = true by smt.
        rewrite ! tuple3_eq ! xor_true ! xor_false /=.
        cut hneq : forall (x:bool), ((! x) = x) = false by smt.
        cut lem : forall u v, Some (enc R.xx{m} ((C.n{m}, C.m{m}, C.q{m}, C.aa{m}, C.bb{m}), C.gg{m}) i
          (u ^^ R.t{hr}.[C.aa{m}.[i]]) (v ^^ R.t{hr}.[C.bb{m}.[i]])) =
            Some (E (tweak i (R.t{hr}.[C.aa{m}.[i]]^^u) (R.t{hr}.[C.bb{m}.[i]]^^v))
              (proj R.xx{hr}.[(C.aa{m}.[i], u ^^ C.v{m}.[C.aa{m}.[i]])]) (proj R.xx{hr}.[(C.bb{m}.[i], v ^^ C.v{m}.[C.bb{m}.[i]])])
              (proj R.xx{hr}.[(i, (proj C.gg{m}.[(i, u^^C.v{m}.[C.aa{m}.[i]], v^^C.v{m}.[C.bb{m}.[i]])]))])).
          intros u v.
          simplify enc fst snd.
          rewrite !H3;first 4 by elim H2;smt.
          rewrite !H ;first 2 by elim H2;smt.
          rewrite !H4 ;first 3 by elim H2;smt.
          rewrite ! (xor_comm false) !xor_false.
          cut xor_simpl : forall x y z, x ^^ (y ^^ z) ^^ y = x ^^ z
            by (intros x y z;case x;case y;case z;do rewrite /= ?(xor_true, xor_false) //).
          rewrite !xor_simpl.
          by do 3 ! congr=> //;rewrite xor_comm;[rewrite (xor_comm v)|rewrite (xor_comm u)];rewrite xor_associative. 
        (case (a = R.t{hr}.[C.aa{m}.[i]])=> ha;[rewrite ? ha|cut -> : a = !R.t{hr}.[C.aa{m}.[i]] by smt]);
        (case (b = R.t{hr}.[C.bb{m}.[i]])=> hb;[rewrite hb|cut -> : b = !R.t{hr}.[C.bb{m}.[i]] by smt]);rewrite ?hneq /=.
          by cut := lem false false;rewrite (H5 i) ?(fst_pair, snd_pair, (xor_comm false), xor_false, (xor_comm true), xor_true) //;smt.
          by cut := lem false true;rewrite /enc !(fst_pair, snd_pair, (xor_comm false), xor_false, (xor_comm true), xor_true) //.
          by cut := lem true false;rewrite /enc !(fst_pair, snd_pair, (xor_comm false), xor_false, (xor_comm true), xor_true) //.
          by cut := lem true true;rewrite /enc !(fst_pair, snd_pair, (xor_comm false), xor_false, (xor_comm true), xor_true) //.
      cut h : forall aa bb, ((G.g{hr}, R.t{hr}.[C.aa{m}.[G.g{hr}]] ^^ aa, R.t{hr}.[C.bb{m}.[G.g{hr}]] ^^ bb) = (i, a, b)) = false by smt.
      by rewrite !Core.get_set !h /=;apply H7;smt.
    wp.
    call RgenClassicVisibleE.
    call CinitE.
    skip.
    simplify funG C2.funG fst OptionGet."_.[_]".
    intros &1 &2 [? valid].
    subst p{1}.
    generalize valid.
    elim/tuple2_ind p{2}=> x1 x2.
    elim/tuple2_ind x1=> topo gg.
    elim/tuple5_ind topo=> n m q aa bb.
    simplify validInputsP validCircuitP.
    progress (rewrite /= !(fst_pair, snd_pair) /=).
      smt.
      smt.
      by rewrite ?(xor_comm false) ?xor_false H30;smt.
      smt.
      smt.
      smt.
      apply map_ext=> y.
      elim/tuple3_ind y=> i a b hy.
      by apply H52;smt.
    qed.

    local equiv equivGFake: Garble2(FH).enc ~ Garble2(FF).enc:
      ={p} /\ CV.l{1} = bound /\ validInputsP p{1} ==> ={res}
    by (apply (Garble2E FH FF bound _); apply equiv_FH_FF).

    local lemma equivPrvInd (S1 <: Scheme_t) (S2 <: Scheme_t) gCV1:
      equiv[S1.enc ~ S2.enc: ={p} /\ (glob CV){1} = gCV1 /\ validInputsP p{1} ==> ={res}] =>
      equiv[Game(S1,A).main ~ Game(S2,A).main: ={glob A} /\ (glob CV){1} = gCV1 ==> ={res}].
    proof strict.
    intros=> SencE; fun.
    seq 1 1: (={glob A, query} /\ (glob CV){1} = gCV1);
      first by call (_: ={glob A} /\ (glob CV){1} = gCV1 ==> ={res,glob A})=> //; fun true.
    if=> //; last by rnd.
    wp; call (_: ={glob A, cipher} ==> ={res})=> //; first by fun true.
    call SencE; wp; rnd; skip.
    progress=> //; elim H;
    simplify EncSecurity.Encryption.valid_plain EncSecurity.Encryption.leak validInputs C2.validInputs EncSecurity.Encryption.leak;smt.
    qed.

    local module Hybrid(A:EncSecurity.Adv_IND_t) = {
      module PrvInd = Game(Garble2(FH), A)
      fun work(x:int) : bool = {
        var b : bool;
        CV.l = x;
        b = PrvInd.main();
        return b;
      }
    }.

    local module PrvInd(A:EncSecurity.Adv_IND_t) = {
      module PrvInd = Game(Garble1, A)
      fun main() : bool = {
        var b : bool;
        b = PrvInd.main();
        return b;
      }
    }.

    local module GarbleFake : Scheme_t = {
      module GI = GInit(FF)

      fun enc(p:fun_t*input_t) : funG_t*inputG_t*outputK_t = {
        var tok1, tok2 : word;

        Cf.init(p);
        Rf.init(true);
        GI.init();
        return (
          ((C.n, C.m, C.q, C.aa, C.bb), G.pp),
          encode (inputK C.f R.xx) C.x,
          tt);
      }
    }.

    local module Fake(A:EncSecurity.Adv_IND_t) = {
      fun main() : bool = {
        var b, b', ret : bool;
        var c : funG_t*inputG_t*outputK_t;
        var query : (fun_t*input_t)*(fun_t*input_t);

        query = A.gen_query();
        if (EncSecurity.queryValid_IND query)
        {
          b = $ {0,1};
          c = GarbleFake.enc(fst query);
          b' = A.get_challenge(c);
          ret = b = b';
        }
        else
          ret = $ {0,1};
        return ret;
      }
    }.

    local lemma prFakeI &m:
      Pr[Fake(A).main() @ &m:res] = 1%r / 2%r.
    proof strict.
    bdhoare_deno (_: true ==> res)=> //.
    fun; seq 1 : true (1%r) (1%r/2%r) 0%r 1%r=> //.
      by call AgenL.
      case (EncSecurity.queryValid_IND query).
        (* VALID *)
        rcondt 1; first by intros.
        inline GarbleFake.enc;
        wp; swap 1 6.
        rnd ((=) b').
        call AgetL.
        wp; call (GinitL FF _);
          first by apply FFake_genL.
        call RgenInitL; call CinitL.
        wp; skip; progress;cut := Dbool.mu_x_def;rewrite /mu_x=> -> //.
        (* INVALID *)
        rcondf 1; first by intros.
        rnd (lambda b, b); skip; progress=> //.
        by rewrite Dbool.mu_def.
    qed.

    local lemma prFake &m:
      Pr[Game(Garble2(FF),A).main() @ &m:res] = 1%r / 2%r.
    proof strict.
    rewrite -(prFakeI &m)=> //.
    equiv_deno (_: CV.l{1} = CV.l{m} /\ ={glob A}  ==> ={res}) => //; fun.
    seq 1 1: (={glob A, query} /\ CV.l{1} = CV.l{m}); first by call (_: true).
    if=> //; last by rnd.
    wp; call (_: true).
    inline Garble2(FF).enc GarbleFake.enc; wp.
    inline Garble2(FF).GI.init GarbleFake.GI.init.
    wp; while (={glob G, C.n, C.m, C.q, C.aa, C.bb, R.t} /\
               C.n{1} <= G.g{1} /\
               validInputsP (((C.n, C.m, C.q, C.aa, C.bb), C.gg), C.x){1} /\
               (forall g, 0 <= g < (C.n + C.q){1} =>
                  R.xx.[(g,C.v.[g])]{1} = R.xx.[(g,C.v.[g])]{2}) /\
               (forall g, 0 <= g < (C.n + C.q){1} =>
                  R.xx.[(g,!C.v.[g])]{1} = R.xx.[(g,!C.v.[g])]{2})).
      wp; seq 7 7: (={glob G, glob F, C.n, C.m, C.q, C.aa, C.bb, R.t} /\
                    C.n{1} <= G.g{1} /\
                    validInputsP (((C.n, C.m, C.q, C.aa, C.bb), C.gg), C.x){1} /\
                    (forall g, 0 <= g < (C.n + C.q){1} =>
                       R.xx.[(g,C.v.[g])]{1} = R.xx.[(g,C.v.[g])]{2}) /\
                    (forall g, 0 <= g < (C.n + C.q){1} =>
                       R.xx.[(g,!C.v.[g])]{1} = R.xx.[(g,!C.v.[g])]{2}) /\
                    0 <= G.a{2} < C.n{2} + C.q{2} /\
                    0 <= G.b{2} < C.n{2} + C.q{2} /\
                    !F.flag_sp{1}).
          do 3!call GgarbDE_rnd; wp; call GgarbE.
          inline GInit(FF).Flag.gen;
          wp; skip; simplify validInputsP validCircuitP fst snd; progress=> //.
            smt.
            
            by rewrite !(xor_comm false) !xor_false; cut:= H10 C.aa{2}.[G.g{2}] _;smt.
            by rewrite !(xor_comm false) !xor_false; cut:= H10 C.bb{2}.[G.g{2}] _;smt.
    timeout 40.
            by rewrite !(xor_comm true) !xor_true; cut:= H11 C.bb{2}.[G.g{2}] _; smt.
            by rewrite !(xor_comm true) !xor_true; cut:= H11 C.bb{2}.[G.g{2}] _; smt.
    timeout 3.
            smt.
            smt.
            smt.
            smt.
        by rcondf{1} 1=> //; rcondf{2} 1=> //; skip; smt.
    wp; call RinitE_rnd.
    call CinitE_rnd.
    wp; rnd; skip; simplify validInputsP C2.validInputs validInputs validCircuitP fst snd;
    progress=> //.
     by case bL; smt.
     by case bL; smt.
     by case bL; smt.
     by case bL; smt.
     smt.
     smt.
     rewrite /encode /C2.Input.encode;
     apply array_ext; split; first smt.
     intros=> i; rewrite length_init; first smt.
     intros=> i_bnd; rewrite !get_init=> //=; first smt.
     cut ->: x_L.[i] = v_L.[i] by smt.
     cut ->: x_R.[i] = v_R.[i] by smt.
     rewrite /inputK /C2.inputK.
     rewrite !fst_pair.
     pose x := (length x_L, m_R, q_R, aa_R, bb_R).
     cut RW: forall (xx:tokens_t),
               (let (n,m,q,aa,bb) = x in filter (lambda x0 y, 0 <= fst x0 < n) xx) =
               (filter (lambda x0 y, 0 <= fst x0 < length x_L) xx)
      by trivial.
     cut ->:= RW xx_L.
     cut ->:= RW xx_R.
     rewrite /FMap.OptionGet."_.[_]" get_filter -/(FMap.OptionGet."_.[_]" _ _) //=.
     rewrite /(FMap.OptionGet."_.[_]" _ (i,v_R.[i])) get_filter -/(FMap.OptionGet."_.[_]" _ _) //=.
     rewrite !fst_pair; case (0 <= i < length x_L)=> //.
     by rewrite H52; smt.
    qed.

    local lemma prH0 &m:
      Pr[Hybrid(A).work((-1)) @ &m :res] = Pr[EncSecurity.Game_IND(Rand,A).main()@ &m:res].
    proof strict.
    cut <-: Pr[Game(Garble1,A).main()@ &m :res] = Pr[EncSecurity.Game_IND(Rand,A).main()@ &m:res]
      by equiv_deno (eqDefs)=> //.
    equiv_deno (_: ={glob A} /\ x{1} = -1 ==> ={res})=> //.
    fun*; inline Hybrid(A).work.
    wp; call (equivPrvInd (Garble2(FH)) Garble1 (-1) _)=> //; last by wp.
    bypr (res{1}) (res{2})=> //;progress.
    apply (eq_trans _ Pr[Garble2(FR).enc(p{1}) @ &m :a = res] _).
    equiv_deno equivGReal=> //.
    apply eq_sym; equiv_deno equivGarble1=> //;rewrite -H //.
    qed.

    local lemma prHB &m:
      2%r * Pr[Hybrid(A).work(bound)@ &m:res] = 1%r.
    proof strict.
    cut : Pr[Hybrid(A).work(bound)@ &m:res] = 1%r / 2%r;last smt.
    rewrite -(prFake &m)=> //.
    equiv_deno (_: ={glob A} /\ x{1} = bound ==> ={res})=> //.
    fun*; inline Hybrid(A).work.
    wp; call (equivPrvInd (Garble2(FH)) (Garble2(FF)) (bound) _)=> //; last by wp.
    bypr (res{1}) (res{2})=> //; progress.
    equiv_deno equivGFake=> //;rewrite -H.
    qed.

    module type RedAda_t(A:EncSecurity.Adv_IND_t, Dkc:Dkc_t) = {
      fun preInit(): unit {}
      fun work(info:bool): bool {A.gen_query Dkc.encrypt A.get_challenge}
    }.

    local module RedI_Ada(A:EncSecurity.Adv_IND_t, D:Dkc_t) : RedAda_t(A D) = {
      module Com = RedComon(A)
      fun preInit(): unit = {Com.preInit();}
      fun query(rand:bool, alpha:bool, bet:bool) : word =
      {
        var q : query;
        var ans : answer;
        var r : word;
        q = Com.genQuery(false, true, false);
        ans = D.encrypt(q);
        r = Com.doAnswer(false, true, false, ans);
        return r;
      }
      fun coreAda(): unit = {
        var trash : word;
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          if (G.a = CV.l)
          {
            trash = query(false,  true, false);
            trash = query(false,  true,  true);
          }
          if (G.b = CV.l)
          {
            trash = query(false, false,  true);
            Com.yy.[G.g] = query( true,  true,  true);
          }
          G.g = G.g + 1;
        }
      }
      fun work(info:bool): bool = {
        var q : int;
        var qs : query array;
        var ans : answer;
        var answers : answer array;
        var r : bool;
        Com.pre(info);
        coreAda();
        r = Com.post();
        return r;
      }
   }.
(*
   local module WI : MeanInt.Worker =
   {
     module H = Hybrid(A)
     fun work(x:int) : bool =
     {
       var r : bool;
       r = H.main(x);
       return r;
     }
   }.

   module type H_t = {
      fun main(l:int) : bool
    }.
    
   equiv eq_H (X:H_t) : X.main ~ X.main

   local lemma Worker_Hybrid &m (l:int) :
       Pr[Hybrid(A).main(l) @ &m : res] = Pr[WI.work(l) @ &m : res].
   proof.
     equiv_deno (_: true ==> ={res}).
     fun*.
     inline WI.work.
     wp.
     call (_: ={l} ==> ={res}).
     fun.
     eqobs_in.
     eqobs_in (={G, C}).
     wp.
     skip.

     fun*.
trivial.
trivial.
simplfy.   
     fun (true).
   qed.
*)

  local module Hybrid1 : MeanInt.Worker = {
    module H = Hybrid(A)
    fun work(x:int) : bool = {
      var r : bool;
      r = H.work(x-1);
      return r;
    }
  }.

  local equiv eqW (W<:MeanInt.Worker) : W.work ~ W.work : ={x, glob W} ==> ={res}
    by fun (true).

  local lemma eqH1 &m (l:int) : Pr[Hybrid(A).work(l-1)@ &m : res] = Pr[Hybrid1.work(l)@ &m : res].
    equiv_deno (_: ={glob Hybrid(A)} /\ x{2} - 1 = x{1} ==> ={res})=> //.
    fun*.
    inline Hybrid1.work.
    by wp;call (eqW (Hybrid(A)));wp.
  qed.

   local equiv garbEq : RedComon(A).garble ~ GInit(FH).init : true ==> ={glob G}.
   proof.
     admit(*todo difficult*).
   qed.

lemma query_valid : forall query c,
  EncSecurity.queryValid_IND query =>
    validInputsP (if c then snd query else fst query).
proof.
intros query c.
elim/tuple2_ind query;progress.
elim/tuple2_ind x1;progress.
elim/tuple2_ind x2;progress.
elim/tuple2_ind x1;progress.
elim/tuple2_ind x10;progress.
elim/tuple5_ind x1;progress.
elim/tuple5_ind x11;progress.
generalize H.
rewrite /EncSecurity.queryValid_IND.
rewrite /EncSecurity.Encryption.valid_plain.
rewrite /validInputs /validInputsP /C2.validInputs.
rewrite !valid_wireinput.
by case c;rewrite !(fst_pair, snd_pair).
qed.

    module type Rf_t = { fun initT(useVisible:bool) : unit }.

    equiv eqinitT : Rf2.initT ~ Rf2.initT : ={glob C, useVisible} ==> ={R.t}.
    proof.
      fun.
      while (={R.t, i, glob C, useVisible});
        first by wp;rnd.
      by wp.
    qed.

    local lemma reductionAdaCore :
       equiv [DKCSecurity.GameAda(Dkc, RedI_Ada(A)).work ~ Hybrid(A).work:
     ={glob A} /\ x{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}
     ==> res{1} = if DKCm.b{1} then res{2} else !res{2} ].
   proof.
     fun.
   
     (* inlining all necessary *)
     inline Dkc.get_challenge.
     inline GameAda(Dkc, RedI_Ada(A)).A.work.
     inline Hybrid(A).PrvInd.main.
     inline RedI_Ada(A, Dkc).Com.post.
     inline RedI_Ada(A, Dkc).Com.pre.
     inline Garble2(FH).enc.
     inline Dkc.initialize.
   
     (* gen_query dversary call *)
     wp.
     swap{1} 9 -8.
     swap{2} 2 - 1.
     seq 1 1 : (={glob A} /\ (x{2} = if DKCm.b{1} then CV.l{1} - 1 else CV.l{1}) /\ (RedComon.query{1} = query{2})).
     call (_: ={glob A} ==> ={glob A, res})=> //;fun true=> //.

     (* Handling query valid test *)
     sp.
     case (EncSecurity.queryValid_IND RedComon.query{1});first last.

     (* Query inalid Case *)
     seq 16 0 : ((!EncSecurity.queryValid_IND query{2}) /\ (!EncSecurity.queryValid_IND RedComon.query{1})).
       by (kill{1} 1!16;first by admit(*termination*)).
     rcondf{1} 1=> //.
     rcondf{2} 1=> //.
     rnd;skip;smt.
     
     (* Query valid Case *)
     rcondt{1} 17;first by (progress;kill 1!16;first by admit(*termination*)).
     rcondt{2} 1=> //.

     (* get_challenge Adversary call *)
     wp.
     call (_: ={glob A, cipher} ==> ={res});first by fun true=> //.

     (* Garbling part *)
     wp.
     call garbEq.

     (* Init variables related to function *)
     swap{1} [1..8] 4.
     seq 3 4 : (={glob C, glob A} /\ RedComon.c{1} = b0{2} /\
      Array.length C.v{1} = (C.n + C.q){1} /\
      C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
      validInputsP (C.f, C.x){1} /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = proj C.gg.[(i, C.v{2}.[C.aa{2}.[i]], C.v{2}.[C.bb.[i]])]){2});
       first by call CinitE;wp;rnd;skip;progress;apply query_valid=> //.

     (* Random generation *)
     seq 12 1 : (={glob C, glob R, glob A} /\ RedComon.c{1} = b0{2} /\
      Array.length C.v{1} = (C.n + C.q){1} /\
      C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
      validInputsP (C.f, C.x){1} /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = proj C.gg.[(i, C.v{2}.[C.aa{2}.[i]], C.v{2}.[C.bb.[i]])]){2}).
     exists * RedComon.c{1}.
     elim* => c.
       transitivity{1} { Rf2.init(true); } (={glob C, glob A} /\  RedComon.c{1} = c /\
      Array.length C.v{1} = (C.n + C.q){1} /\
      C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
      validInputsP (C.f, C.x){1} /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = proj C.gg.[(i, C.v{2}.[C.aa{2}.[i]], C.v{2}.[C.bb.[i]])]){2} ==> ={glob C, glob R, glob A} /\  RedComon.c{1} = c /\
      Array.length C.v{1} = (C.n + C.q){1} /\
      C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
      validInputsP (C.f, C.x){1} /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = proj C.gg.[(i, C.v{2}.[C.aa{2}.[i]], C.v{2}.[C.bb.[i]])]){2}) (={glob C, glob A} /\ c = b0{2} /\
      Array.length C.v{1} = (C.n + C.q){1} /\
      C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
      validInputsP (C.f, C.x){1} /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = proj C.gg.[(i, C.v{2}.[C.aa{2}.[i]], C.v{2}.[C.bb.[i]])]){2} ==> ={glob C, glob R, glob A} /\ c = b0{2} /\
      Array.length C.v{1} = (C.n + C.q){1} /\
      C.f{1} = ((C.n, C.m, C.q, C.aa, C.bb), C.gg){1} /\
      validInputsP (C.f, C.x){1} /\
      (forall i, C.n <= i < C.n + C.q => C.v{2}.[i] = proj C.gg.[(i, C.v{2}.[C.aa{2}.[i]], C.v{2}.[C.bb.[i]])]){2})=> //;[by progress;smt| |by symmetry;call eqRf].

       inline Rf2.init.

       (* t generation *)
       cfold{2} 1.
       seq 1 1 : (={glob C, glob A, R.t}).
         call eqinitT=> //.

       (* token generation *)
       admit(*todo difficult*).
     
     skip.
     progress;smt.
   qed.

  local module WB : MeanBool.Worker = {
    module M = DKCSecurity.GameAda(Dkc, RedI_Ada(A))
    fun work(x:bool) : bool = {
      var r : bool;
      DKCm.b = x;
      CV.l = $Dinter.dinter 0 bound;
      r = M.work();
      return r;
    }
  }.

  equiv eqExp (M<:Exp) : M.work ~ M.work : ={glob M} ==> ={res} by fun true.

  local lemma eqGameAda &m :
    Pr[MeanBool.Rand(WB).randAndWork()@ &m: snd res] = Pr[DKCSecurity.GameAda(Dkc, RedI_Ada(A)).main()@ &m:res].
  proof.
    equiv_deno (_: ={glob DKCSecurity.GameAda, glob Dkc, glob RedI_Ada, glob A} ==> (snd res){1} = res{2}) => //.
    fun.
    inline GameAda(Dkc, RedI_Ada(A)).preInit.
    inline Dkc.preInit.
    inline GameAda(Dkc, RedI_Ada(A)).A.preInit.
    inline RedI_Ada(A, Dkc).Com.preInit.
    inline WB.work.
    wp;call (eqExp WB.M).
    by rnd;wp;rnd.
  qed.

  lemma Mean_uniInt (A<:MeanInt.Worker) &m :
     (bound+1)%r * Pr[MeanInt.Rand(A).randAndWork()@ &m: snd res] =
       Mrplus.sum (lambda (v:int), Pr[A.work(v)@ &m:res]) (Interval.interval 0 bound).
  proof.
    pose ev := lambda (x:int) (gA:glob A) (b:bool), b.
    cut Hcr: forall x, 
               ISet.mem x (ISet.create (support (Dinter.dinter 0 bound))) <=>
               mem x (Interval.interval 0 bound).
      by smt.
    cut Hf : ISet.Finite.finite (ISet.create (support (Dinter.dinter 0 bound))).
      by exists (Interval.interval 0 bound) => x;apply Hcr.
    cut := MeanInt.Mean A &m ev _=> //=;simplify ev=> -> //.
    cut -> : 
      ISet.Finite.toFSet (ISet.create (support (Dinter.dinter 0 bound))) = (Interval.interval 0 bound).
      by apply FSet.set_ext => x; rewrite ISet.Finite.mem_toFSet //;apply Hcr.
    rewrite Mrplus.sum_in /=.
    pose f := (lambda (v : int), if mem v ((Interval.interval 0 bound))%Interval then Pr[A.work(v) @ &m : res] else 0%r).
    pose g :=  (lambda (v : real), (v / (bound + 1)%r)).
    cut -> :  (lambda (v : int), if mem v ((Interval.interval 0 bound))%Interval then mu_x [0..bound] v * Pr[A.work(v) @ &m : res] else 0%r) = lambda v, g (f v).
      apply fun_ext=> v /=.
      simplify g f.
      case (mem v ((Interval.interval 0 bound))%Interval)=> h.
      rewrite Dinter.mu_x_def_in //;smt.
      smt.
    rewrite Mrplus.sum_comp.
    smt.
    smt.
    simplify f g.
    cut -> : (lambda (v : int),
      if mem v ((Interval.interval 0 bound))%Interval then
        Pr[A.work(v) @ &m : res]
      else 0%r) = (lambda (v : int),
      if mem v ((Interval.interval 0 bound))%Interval then
        (lambda v, Pr[A.work(v) @ &m : res]) v
      else 0%r) by smt.
    rewrite -Mrplus.sum_in /=.
    generalize (Mrplus.sum (lambda (v : int), Pr[A.work(v) @ &m : res]) ((Interval.interval 0 bound))%Interval)%Mrplus. generalize (bound + 1)%r.
    admit(* smt ?*).
  qed.

  local lemma Mean_uniBool (A<:MeanBool.Worker) &m :
     2%r * Pr[MeanBool.Rand(A).randAndWork()@ &m: snd res] =
       Pr[A.work(false)@ &m:res] + Pr[A.work(true)@ &m:res].
  proof.
    pose ev := lambda (b:bool) (gA:glob A) (b':bool), b'.
    cut Hcr: forall x, 
               ISet.mem x (ISet.create (support {0,1})) <=>
               mem x (add true (add false (FSet.empty)%FSet)).
      by intros=> x; rewrite !FSet.mem_add; case x=> //=; smt.
    cut Hf : ISet.Finite.finite (ISet.create (support {0,1})).
      by exists (FSet.add true (FSet.add false FSet.empty)) => x;apply Hcr.
    cut := MeanBool.Mean A &m ev _=> //=;simplify ev=> -> //.
    cut -> : 
      ISet.Finite.toFSet (ISet.create (support {0,1})) = (FSet.add true (FSet.add false FSet.empty)).
      by apply FSet.set_ext => x; rewrite ISet.Finite.mem_toFSet //;apply Hcr.
    rewrite Mrplus.sum_add;first smt.
    rewrite Mrplus.sum_add;first smt.
    rewrite Mrplus.sum_empty /= !Bool.Dbool.mu_x_def;simplify ev.
    smt.
  qed.

  local lemma eqHybrid1 &m : Pr[WB.work(true)@ &m:res] = Pr[MeanInt.Rand(Hybrid1).randAndWork() @ &m : snd res].
  proof.
    equiv_deno (_: ={glob WB} /\ x{1} = true ==> res{1} = snd res{2})=> //.
    fun.
    inline Hybrid1.work.
    wp.
    call reductionAdaCore.
    by wp;rnd;wp.
  qed.

  local lemma eqHybridA &m : Pr[WB.work(false)@ &m:res] = Pr[MeanInt.Rand(Hybrid(A)).randAndWork() @ &m : !snd res].
  proof.
    equiv_deno (_: ={glob WB} /\ x{1} = false ==> res{1} = !snd res{2})=> //.
    fun.
    wp.
    call reductionAdaCore.
    by wp;rnd;wp.
  qed.

  local lemma lossless_raW &m :
    Pr[MeanInt.Rand(Hybrid(A)).randAndWork() @ &m : true] = 1%r.
    admit(* termination *).
  qed.

   local lemma reductionAda &m:
      Mrplus.sum (lambda l, let l = l - 1 in Pr[Hybrid(A).work(l) @ &m : res]) (Interval.interval 0 bound) -
        Mrplus.sum (lambda l, Pr[Hybrid(A).work(l) @ &m : res]) (Interval.interval 0 bound) =
        (bound+1)%r * (2%r * Pr[DKCSecurity.GameAda(Dkc, RedI_Ada(A)).main()@ &m:res] - 1%r).
    proof.
      simplify.
      cut -> : Mrplus.sum (lambda (l : int), Pr[Hybrid(A).work(l - 1) @ &m : res]) (Interval.interval 0 bound) =
        Mrplus.sum (lambda (l : int), Pr[Hybrid1.work(l) @ &m : res]) (Interval.interval 0 bound).
        congr=> //.
        apply fun_ext=> x /=.
        by apply (eqH1 &m x).
      rewrite -(Mean_uniInt Hybrid1 &m).
      rewrite -(Mean_uniInt (Hybrid(A)) &m).
      rewrite -(eqGameAda &m).
      rewrite (Mean_uniBool WB &m).
      rewrite (eqHybrid1 &m).
      rewrite (eqHybridA &m).
      rewrite Pr mu_not.
      rewrite (lossless_raW &m).
      smt.
    qed.

    local module RedI_Int(A:EncSecurity.Adv_IND_t, Dkc:Dkc_t) : RedAda_t(A Dkc) = {
      module Com = RedComon(A)
      fun preInit(): unit = {Com.preInit();}
      fun work(info:bool): bool = {
        var q : int;
        var qs : query array;
        var ans : answer;
        var answers : answer array;
        var r : bool;
        Com.pre(info);
        qs = Com.coreNAda_pre();
        G.g = C.n;
        while (G.g < C.n + C.q) {
          G.a = C.aa.[G.g];
          G.b = C.bb.[G.g];
          if ((G.a = CV.l) \/ (G.b = CV.l))
          {
            ans = Dkc.encrypt(qs.[q]);
            answers = ans::answers;
            q=q+1;
            ans = Dkc.encrypt(qs.[q]);
            answers = ans::answers;
            q=q+1;
          }
          G.g = G.g + 1;
        }
        Com.coreNAda_post(answers);
        r = Com.post();
        return r;
      }
   }.

   local equiv Int1 (A<:EncSecurity.Adv_IND_t {G, R, C, RedComon, Dkc}) : DKCSecurity.GameAda(Dkc,RedI_Ada(A)).main ~ DKCSecurity.GameAda(Dkc,RedI_Int(A)).main : ={glob G, glob C, glob R, glob A, glob RedComon}  ==> ={res}.
   proof.
     fun.
     inline GameAda(Dkc, RedI_Ada(A)).work.
     inline GameAda(Dkc, RedI_Int(A)).work.
     inline GameAda(Dkc, RedI_Ada(A)).A.work.
     inline GameAda(Dkc, RedI_Int(A)).A.work.
     inline RedI_Ada(A, Dkc).coreAda.
     inline RedI_Int(A, Dkc).Com.coreNAda_post.
     inline RedI_Int(A, Dkc).Com.coreNAda_pre.
     inline RedI_Ada(A, Dkc).Com.pre.
     inline RedI_Int(A, Dkc).Com.pre.
     eqobs_in : (={r, glob G, glob C, glob R, glob A, glob RedComon}).
     seq 11 12 : (={glob G, glob C, glob R, glob A, glob RedComon});
       last by admit(* loop fusion *).
     eqobs_in : (={glob G, glob C, glob R, glob A, glob RedComon}).
   qed.

    module type Red_t (A:EncSecurity.Adv_IND_t) = {
      fun preInit(): unit {}
      fun gen_queries(info:bool): query array {A.gen_query}
      fun get_challenge(answers:answer array): bool {A.get_challenge}
    }.

    module RedI(A:EncSecurity.Adv_IND_t) : Red_t(A) = {
      module Com = RedComon(A)
      fun preInit(): unit = {Com.preInit();}
      fun gen_queries(info:bool): query array = {
        var qs : query array;
        Com.pre(info);
        qs = Com.coreNAda_pre();
        return qs;
      }
      fun get_challenge(answers:answer array): bool = {
        var r : bool;
        Com.coreNAda_post(answers);
        r = Com.post();
        return r;
      }
   }.

   local equiv Int2 (A<:EncSecurity.Adv_IND_t) :  DKCSecurity.GameAda(Dkc,RedI_Int(A)).main ~ DKCSecurity.Game(Dkc,RedI(A)).main : ={glob G, glob C, glob R, glob A, glob RedComon} ==> ={res}.
   proof.
     fun.
     inline GameAda(Dkc, RedI_Int(A)).work.
     inline DKCSecurity.Game(Dkc, RedI(A)).work.
     inline GameAda(Dkc, RedI_Int(A)).A.work.
     inline RedI(A).get_challenge.
     inline RedI_Int(A, Dkc).Com.coreNAda_post.
     inline RedI(A).Com.coreNAda_post.
     inline  DKCSecurity.Game(Dkc, RedI(A)).B.encrypt.
     eqobs_in : (={r, glob G, glob C, glob R, glob A, glob RedComon}).
     seq 2 2 : (={glob G, glob C, glob R, glob A, glob RedComon});
       last by admit(*loop split*).
     eqobs_in : (={glob G, glob C, glob R, glob A, glob RedComon}).
   qed.

    local lemma reduction &m:
      Mrplus.sum (lambda l, let l = l - 1 in Pr[Hybrid(A).work(l) @ &m : res]) (Interval.interval 0 bound) -
        Mrplus.sum (lambda l, Pr[Hybrid(A).work(l) @ &m : res]) (Interval.interval 0 bound) =
        (bound+1)%r * (2%r * Pr[DKCSecurity.Game(Dkc,RedI(A)).main()@ &m:res] - 1%r).
    proof.
      rewrite (reductionAda &m).
      apply (_:forall x y, x=y => (bound + 1)%r * (2%r * x - 1%r) = (bound + 1)%r * (2%r * y - 1%r)).
        smt.
      apply (eq_trans _ (Pr[GameAda(Dkc, RedI_Int(A)).main() @ &m : res])).
      equiv_deno (Int1 A)=> //.
      equiv_deno (Int2 A)=> //.
    qed.

    local lemma reductionSimplified &m:
      Pr[Hybrid(A).work((-1)) @ &m : res] - Pr[Hybrid(A).work(bound) @ &m : res] =
        (bound + 1)%r * (2%r * Pr[DKCSecurity.Game(Dkc,RedI(A)).main()@ &m:res] - 1%r).
    proof strict.
    rewrite -(reduction &m).
    rewrite (Mrplus.sum_rm _ _ 0) /=; first smt.
    rewrite (Mrplus.sum_chind _ (lambda (x:int), x - 1) (lambda (x:int), x + 1)) /=; first smt.
    rewrite (Interval.dec_interval 0 bound _); first smt.
    rewrite (Mrplus.sum_rm _ (Interval.interval 0 bound) bound) /=; first smt.
    pose s1 := Mrplus.sum _ _.
    pose s2 := Mrplus.sum _ _.
    cut -> : s1 = s2 by (rewrite /s1 /s2; congr; [ | apply Fun.fun_ext]; smt).
    smt.
    qed.

    lemma sch_is_ind &m:
      `|2%r * Pr[EncSecurity.Game_IND(Rand,A).main()@ &m:res] - 1%r| =
        2%r * (bound+1)%r * `|2%r * Pr[DKCSecurity.Game(Dkc,RedI(A)).main()@ &m:res] - 1%r|.
    proof strict.
    rewrite -(prH0 &m) // -{1}(prHB &m) //=.
    cut -> : forall a b, 2%r * a - 2%r * b = 2%r * (a - b) by smt.
    rewrite (reductionSimplified &m).
    cut ->: forall (a b:real), 0%r <= a => `| a * b | = a * `| b | by smt=> //.
    cut ->: forall (a b:real), 0%r <= a => `| a * b | = a * `| b | by smt=> //.
    smt.
    smt.
    qed.
  end section.

lemma sch_is_pi: EncSecurity.pi_sampler_works ().
 proof strict.
 rewrite /EncSecurity.pi_sampler_works=> plain.
 simplify validCircuit C2.validInputs validInputs EncSecurity.Encryption.valid_plain EncSecurity.Encryption.leak EncSecurity.Encryption.pi_sampler pi_sampler C2.pi_sampler phi C2.phi eval C2.eval.
 elim/tuple2_ind plain=> p1 p2 hplain.
 elim/tuple2_ind p1=> topo1 x1 hp1.
 elim/tuple5_ind topo1=> n m q aa bb htopo.
 simplify fst snd.
 progress.
     rewrite {1} /evalComplete /=.
     pose ev := evalComplete _ _ _.
     (apply array_ext;split;first by smt)=> i hi.
     rewrite ! get_sub;first 8 smt.
     rewrite appendInit_get2;first 2 smt.
     rewrite {1} /extract /=.
     rewrite get_initGates;first smt.
     cut -> : (length p2 <= i + (length p2 + q - m) - 1 + 1 < length p2 + q) = true by smt.
     simplify.
     rewrite proj_some.
   by rewrite get_sub;smt.
     simplify validGG.
     pose {1} k := q.
     cut : k <= q by smt.
     (elim/Induction.induction k;first smt);last smt.
     progress.
     rewrite range_ind_lazy /=;first smt.
     cut -> : length p2 + (i + 1) - 1 = length p2 + i by smt.
     rewrite H10 /=;first smt.
   by ((split;last split);last split);apply in_dom_init_Gates;smt.
   by rewrite length_init;smt.
 qed.

module RedS(A : EncSecurity.Adv_SIM_t) = {
  module M = RedI(EncSecurity.RedSI(A))
  fun preInit(): unit = { M.preInit(); }
  fun gen_queries(info:bool): query array = {
    var r: query array;
    r = M.gen_queries(info);
    return r;
  }
  fun get_challenge(answers:answer array): bool = {
    var r: bool;
    r = M.get_challenge(answers);
    return r;
  }
}.

lemma AinitEq : forall (A<:DKCSecurity.Adv_t),
    equiv[A.preInit ~ A.preInit: ={glob A} ==> ={res, glob A}].
proof strict.
by intros A;fun true.
qed.

lemma Agen_queriesEq : forall (A<:DKCSecurity.Adv_t),
    equiv[A.gen_queries ~ A.gen_queries: ={info, glob A} ==> ={res, glob A}].
proof strict.
by intros A;fun true.
qed.

lemma Aget_challengeEq : forall (A<:DKCSecurity.Adv_t),
    equiv[A.get_challenge ~ A.get_challenge: ={answers, glob A} ==> ={res, glob A}].
proof strict.
by intros A;fun true.
qed.

lemma DinitEq : forall (D<:DKCSecurity.Dkc_t),
    equiv[D.preInit ~ D.preInit: ={glob D} ==> ={res, glob D}].
proof strict.
by intros D;fun true.
qed.

lemma DinitializeEq : forall (D<:DKCSecurity.Dkc_t),
    equiv[D.initialize ~ D.initialize: ={glob D} ==> ={res, glob D}].
proof strict.
by intros D;fun true.
qed.

lemma DencryptEq : forall (D<:DKCSecurity.Dkc_t),
    equiv[D.encrypt ~ D.encrypt: ={q, glob D} ==> ={res, glob D}].
proof strict.
by intros D;fun true.
qed.

lemma Dget_challengeEq : forall (D<:DKCSecurity.Dkc_t),
    equiv[D.get_challenge ~ D.get_challenge: ={glob D} ==> ={res, glob D}].
proof strict.
by intros D;fun true.
qed.

(*
lemma RedSEq (A <: EncSecurity.Adv_SIM_t {Rand}) &m :
  Pr[DKCSecurity.Game(Dkc, RedI(EncSecurity.RedSI(A))).main() @ &m : res] =
  Pr[DKCSecurity.Game(Dkc, RedS(A)).main() @ &m : res].
proof strict.
equiv_deno (_: ={glob DKCSecurity.Game} ==> ={res}).
apply (DKCSecurity.mainE Dkc (RedI(EncSecurity.RedSI(A))) Dkc (RedS(A)) _ _ _ _ _ _ _).
  by fun*;inline RedS(A).preInit;call (AinitEq (RedI(EncSecurity.RedSI(A)))).
  by fun*;inline RedS(A).gen_queries;wp;call (Agen_queriesEq (RedI(EncSecurity.RedSI(A))));wp.
  by fun*;inline RedS(A).get_challenge;wp;call (Aget_challengeEq (RedI(EncSecurity.RedSI(A))));wp.
  fun *;call (DinitEq Dkc). skip.
  conseq (_: ={glob Dkc} ==> ={res}). apply (DinitEq Dkc).
  by fun*;inline RedS(A).preInit;call (AinitEq (RedI(EncSecurity.RedSI(A)))).
  by fun*;inline RedS(A).gen_queries;wp;call (Agen_queriesEq (RedI(EncSecurity.RedSI(A))));wp.
  by fun*;inline RedS(A).get_challenge;wp;call (Aget_challengeEq (RedI(EncSecurity.RedSI(A))));wp.
  by fun*;inline RedS(D).get_challenge;wp;call (Dget_challengeEq Dkc);wp.
  fun.
progress.
call (_ : true ==> ={res}).
trivial.

fun true.
fun.
eqobs_in.
fun*.
inline RedS(A).preInit.
call (_: true ==> true).
fun true.
*)

lemma sch_is_sim (A <: EncSecurity.Adv_SIM_t {Rand, DKCm, RedComon}) &m:
 islossless A.gen_query =>
 islossless A.get_challenge =>
  `|2%r * Pr[EncSecurity.Game_SIM(Rand,EncSecurity.SIM(Rand),A).main()@ &m:res] - 1%r| <=
    2%r * (bound+1)%r * `|2%r * Pr[DKCSecurity.Game(Dkc,RedI(EncSecurity.RedSI(A))).main()@ &m:res] - 1%r|.
proof strict.
intros=> ll_ADVp1 ll_ADVp2.
rewrite -(sch_is_ind (EncSecurity.RedSI(A)) _ _ &m).
  by apply (EncSecurity.RedSIgenL Rand A _ _).
  by apply (EncSecurity.RedSIgetL Rand A _ _).
apply (EncSecurity.ind_implies_sim Rand A _ _ &m _)=> //.
  by apply sch_is_pi.
qed.

(* END THE SECURITY PROOF *)

end DKCScheme.
