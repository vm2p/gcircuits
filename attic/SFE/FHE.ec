require import Pair.
require import Option.
require import Int.

require import Sch.

theory FHE.
  clone import Scheme.
  
  type randg_t.  
  type rande_t.  
  type pkey_t.
  type skey_t.
  type cipheri_t.
  type ciphero_t.

  type plaini_t = Input.inputG_t.
  type plaino_t = outputG_t.

  type leak_t = int.
  op phi = Input.inputG_len.

  op gen : randg_t -> pkey_t * skey_t.
  op enc : pkey_t -> plaini_t -> rande_t -> cipheri_t.
  op hom_eval : (inputG_t -> outputG_t) -> cipheri_t -> ciphero_t.
  op dec : skey_t -> ciphero_t -> plaino_t.

  axiom homomorphic : forall f x r rg re,
        Scheme.validRand f r => validInputs f x =>
        let fG = funG f r in
        let iK = inputK f r in
        let xG = Input.encode iK x in
        let oK = outputK f r in
               let (pk,sk) = gen rg in
               let ci = enc pk xG re in
               let co = hom_eval (Scheme.evalG fG) ci in
                      dec sk co = evalG fG xG.

  module type RandG_t = { fun gen() : randg_t {*} }.
  module type RandE_t = { fun gen() : rande_t {*} }.

  (* Definitions needed for IND model *)
  op max_qr_fhe : int.
  axiom maxqr_pos : max_qr_fhe > 0.

  type query_IND = plaini_t * plaini_t.

  op queryValid_IND(query : query_IND) : bool =
      phi (fst query) = phi (snd query).

  module type Adv_IND_t = {
    fun gen_query(pk : pkey_t): query_IND
    fun get_challenge(c : cipheri_t): bool
  }.

  module Game_IND(RG : RandG_t, RE : RandE_t, ADV:Adv_IND_t) = {
    fun main(): bool = {
      var pk : pkey_t;
      var sk : skey_t;
      var query:query_IND;
      var p : plaini_t;
      var c : cipheri_t;
      var b,adv,ret:bool;
      var rg:randg_t;
      var re:rande_t;
    
      rg = RG.gen();
      (pk,sk) = gen rg;
      query = ADV.gen_query(pk);
      if (queryValid_IND query)
      {
        b = ${0,1};
        p = if b then snd query else fst query;
        re = RE.gen();
        c = enc pk p re;
        adv = ADV.get_challenge(c);
        ret = (b = adv);
      }
      else
        ret = ${0,1};
      return ret;
    }
  }.

  module type O_IND'_t = {
      fun enc(query:query_IND) : (pkey_t * cipheri_t) option
  }.

  module type Adv_IND'_t(O : O_IND'_t) = { 
    fun guess() : bool { O.enc }
  }.

  module O_IND'(RG : RandG_t, RE : RandE_t) = {
     var b : bool
     var validQs : bool
     var count : int

     fun init(bval : bool) : unit = {
         b = bval;
         validQs = true;
         count = 0;
     }

     fun enc(query:query_IND) : (pkey_t * cipheri_t) option = {
        var pk : pkey_t;
        var sk : skey_t;
        var p : plaini_t;
        var c : cipheri_t;
        var rg:randg_t;
        var re:rande_t;
        var rep : (pkey_t * cipheri_t) option;

        if (! queryValid_IND query) {
           validQs = false;
           rep = None;
        } else {
           rg = RG.gen();
           (pk,sk) = gen rg;
           p = if b then snd query else fst query;
           re = RE.gen();
           c = enc pk p re;
           if (count < max_qr_fhe) {
              rep = Some (pk,c);
              count = count + 1;
           }
           else
              rep = None;
        }
        return rep;
     }

     fun check(b' : bool) : bool = {
         var ret : bool;

         if (validQs) {
             ret = (b' = b);
         }
         else {
             ret = ${0,1}; 
         }
         return ret;
     }
  }.

  module Game_IND'(RG : RandG_t, RE : RandE_t, ADV:Adv_IND'_t) = {

    module O = O_IND'(RG,RE)
    module A = ADV(O)

    fun main(b : bool): bool = {
      var b',ret : bool;

      O.init(b);
      b' = A.guess();
      ret = O.check(b');
      return ret;
    }
  }.

end FHE.
