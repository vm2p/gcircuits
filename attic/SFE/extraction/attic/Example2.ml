open SFE
open Mpz
open EcIArray

(* Adder 32 *)
module Example1 = struct
  let i1 : EcPervasive.bool0 tarray = clclcl (make 31 false) true
  let i2 : EcPervasive.bool0 tarray = clclcl (make 31 false) true
  let fn : EcPervasive.int0  * EcPervasive.int0  * EcPervasive.int0  * 
           (EcPervasive.int0  tarray) * (EcPervasive.int0  tarray) * 
	        ((EcPervasive.bool0 * EcPervasive.bool0 * EcPervasive.bool0 * EcPervasive.bool0) tarray) =
    (64 ,33 ,408 ,array [|0 ;5 ;4 ;10 ;14 ;24 ;8 ;1 ;7 ;28 ;19 ;2 ;30 ;13 ;18 ;11 ;3 ;16 ;31 ;27 ;15 ;17 ;9 ;32 ;29 ;6 ;25 ;20 ;22 ;21 ;12 ;23 ;26 ;56 ;3 ;40 ;62 ;6 ;28 ;10 ;13 ;27 ;7 ;24 ;54 ;36 ;52 ;50 ;57 ;31 ;55 ;18 ;60 ;5 ;14 ;47 ;8 ;19 ;53 ;29 ;1 ;34 ;20 ;37 ;45 ;11 ;42 ;12 ;38 ;23 ;41 ;61 ;15 ;48 ;26 ;43 ;59 ;9 ;58 ;17 ;44 ;21 ;49 ;16 ;25 ;51 ;4 ;2 ;39 ;46 ;35 ;22 ;63 ;33 ;30 ;154 ;109 ;114 ;142 ;127 ;113 ;146 ;108 ;135 ;92 ;110 ;140 ;66 ;139 ;95 ;67 ;69 ;70 ;71 ;125 ;99 ;72 ;73 ;74 ;75 ;77 ;78 ;90 ;79 ;80 ;82 ;83 ;81 ;84 ;85 ;88 ;86 ;76 ;89 ;68 ;91 ;96 ;144 ;93 ;94 ;65 ;122 ;130 ;100 ;111 ;124 ;149 ;152 ;132 ;128 ;134 ;153 ;137 ;97 ;116 ;119 ;112 ;159 ;160 ;161 ;163 ;164 ;169 ;165 ;166 ;167 ;207 ;170 ;172 ;210 ;211 ;206 ;208 ;205 ;162 ;179 ;201 ;212 ;213 ;214 ;215 ;209 ;216 ;217 ;218 ;178 ;219 ;220 ;245 ;225 ;245 ;252 ;253 ;254 ;257 ;258 ;2 ;259 ;34 ;261 ;263 ;264 ;3 ;265 ;35 ;267 ;269 ;270 ;4 ;271 ;36 ;273 ;275 ;276 ;5 ;277 ;37 ;279 ;281 ;282 ;6 ;283 ;38 ;285 ;287 ;288 ;7 ;289 ;39 ;291 ;293 ;294 ;8 ;295 ;40 ;297 ;299 ;300 ;9 ;301 ;41 ;303 ;305 ;306 ;10 ;307 ;42 ;309 ;311 ;312 ;11 ;313 ;43 ;315 ;317 ;318 ;12 ;319 ;44 ;321 ;323 ;324 ;13 ;325 ;45 ;327 ;329 ;330 ;14 ;331 ;46 ;333 ;335 ;336 ;15 ;337 ;47 ;339 ;341 ;342 ;48 ;343 ;16 ;345 ;347 ;348 ;17 ;349 ;49 ;351 ;353 ;354 ;18 ;355 ;50 ;357 ;359 ;360 ;19 ;361 ;51 ;363 ;365 ;366 ;20 ;367 ;52 ;369 ;371 ;372 ;53 ;373 ;21 ;375 ;377 ;378 ;22 ;379 ;54 ;381 ;383 ;384 ;23 ;385 ;55 ;387 ;389 ;390 ;24 ;391 ;56 ;393 ;395 ;396 ;25 ;397 ;57 ;399 ;401 ;402 ;26 ;403 ;58 ;405 ;407 ;408 ;59 ;409 ;27 ;411 ;413 ;414 ;28 ;415 ;60 ;417 ;419 ;420 ;61 ;421 ;29 ;423 ;425 ;426 ;30 ;427 ;62 ;429 ;431 ;432 ;432 ;434 ;435 ;436 ;437 ;64 ;255 ;262 ;268 ;274 ;280 ;286 ;292 ;298 ;304 ;310 ;316 ;322 ;328 ;334 ;340 ;346 ;352 ;358 ;364 ;370 ;376 ;382 ;388 ;394 ;400 ;406 ;412 ;418 ;424 ;430 ;433 ;438|],array [|32 ;37 ;36 ;42 ;46 ;56 ;40 ;33 ;39 ;60 ;51 ;34 ;62 ;45 ;50 ;43 ;35 ;48 ;63 ;59 ;47 ;49 ;41 ;0 ;61 ;38 ;57 ;52 ;54 ;53 ;44 ;55 ;58 ;56 ;3 ;40 ;62 ;6 ;28 ;10 ;13 ;27 ;7 ;24 ;54 ;36 ;52 ;50 ;57 ;31 ;55 ;18 ;60 ;5 ;14 ;47 ;8 ;19 ;53 ;29 ;1 ;34 ;20 ;37 ;45 ;11 ;42 ;12 ;38 ;23 ;41 ;61 ;15 ;48 ;26 ;43 ;59 ;9 ;58 ;17 ;44 ;21 ;49 ;16 ;25 ;51 ;4 ;2 ;39 ;46 ;35 ;22 ;63 ;33 ;30 ;98 ;150 ;133 ;138 ;117 ;156 ;143 ;155 ;123 ;92 ;126 ;105 ;66 ;129 ;95 ;67 ;69 ;70 ;71 ;151 ;120 ;72 ;73 ;74 ;75 ;77 ;78 ;90 ;79 ;80 ;82 ;83 ;81 ;84 ;85 ;88 ;86 ;76 ;89 ;68 ;91 ;96 ;131 ;93 ;94 ;65 ;145 ;103 ;158 ;115 ;157 ;121 ;106 ;101 ;104 ;141 ;118 ;147 ;107 ;102 ;136 ;148 ;159 ;160 ;161 ;163 ;164 ;169 ;165 ;166 ;167 ;207 ;170 ;172 ;210 ;211 ;206 ;208 ;205 ;162 ;179 ;201 ;212 ;213 ;214 ;215 ;209 ;216 ;217 ;218 ;178 ;219 ;220 ;177 ;189 ;87 ;87 ;253 ;254 ;177 ;258 ;259 ;249 ;260 ;261 ;183 ;264 ;265 ;221 ;266 ;267 ;188 ;270 ;271 ;222 ;272 ;273 ;171 ;276 ;277 ;224 ;278 ;279 ;204 ;282 ;283 ;241 ;284 ;285 ;197 ;288 ;289 ;234 ;290 ;291 ;180 ;294 ;295 ;239 ;296 ;297 ;176 ;300 ;301 ;243 ;302 ;303 ;195 ;306 ;307 ;235 ;308 ;309 ;174 ;312 ;313 ;232 ;314 ;315 ;187 ;318 ;319 ;240 ;320 ;321 ;203 ;324 ;325 ;242 ;326 ;327 ;184 ;330 ;331 ;244 ;332 ;333 ;198 ;336 ;337 ;250 ;338 ;339 ;192 ;342 ;343 ;246 ;344 ;345 ;191 ;348 ;349 ;227 ;350 ;351 ;193 ;354 ;355 ;236 ;356 ;357 ;185 ;360 ;361 ;233 ;362 ;363 ;182 ;366 ;367 ;226 ;368 ;369 ;199 ;372 ;373 ;237 ;374 ;375 ;202 ;378 ;379 ;228 ;380 ;381 ;168 ;384 ;385 ;223 ;386 ;387 ;173 ;390 ;391 ;247 ;392 ;393 ;175 ;396 ;397 ;251 ;398 ;399 ;186 ;402 ;403 ;238 ;404 ;405 ;200 ;408 ;409 ;231 ;410 ;411 ;190 ;414 ;415 ;248 ;416 ;417 ;181 ;420 ;421 ;229 ;422 ;423 ;194 ;426 ;427 ;230 ;428 ;429 ;196 ;256 ;432 ;225 ;435 ;189 ;437 ;64 ;255 ;262 ;268 ;274 ;280 ;286 ;292 ;298 ;304 ;310 ;316 ;322 ;328 ;334 ;340 ;346 ;352 ;358 ;364 ;370 ;376 ;382 ;388 ;394 ;400 ;406 ;412 ;418 ;424 ;430 ;433 ;438|],array [|(false, true, true, false) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(true, false, false, false) ;(false, false, false, true) ;(false, false, false, true) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(false, false, false, true) ;(false, false, false, true) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(true, false, false, false) ;(false, false, false, true) ;(false, false, false, true) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, true, true, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(false, true, true, false) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false, false, false, true) ;(true, false, false, false) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true) ;(false,false,false,true)|])

              



	let r1 : Prime_field.gf_q tarray = init 32 (fun xk -> of_string "2")
		
	let r2 : ((Prime_field.gf_q tarray * SomeOT.SomeOT.ESn.hkey_t) * Prime_field.gf_q) * DKCSch.DKCScheme.Scheme.rand_t = 
		((( init 32 (fun xk -> of_string "2") , ()),of_string "2"),
                   let one = "\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000" in
                    init 1000 (fun xk -> (String.copy DKCSch.Token.zeros, String.copy one)) )
		
end

(*
let _ = print_string (
    let fg = DKCSch.DKCScheme.Scheme.funG Example1.fn (snd  Example1.r2) in 
     let ki = DKCSch.DKCScheme.Scheme.inputK Example1.fn (snd  Example1.r2) in 
    let ko = DKCSch.DKCScheme.Scheme.outputK Example1.fn (snd  Example1.r2) in 
    let xg = DKCSch.DKCScheme.SchSecurity.encode ki 
       (ref (EcFArray.Array [| true; true; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; true;
                                true; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; true |])) in
    let outG = DKCSch.DKCScheme.Scheme.evalG fg xg in
       (*let y = DKCSch.GarbleTools.evalComplete Example1.fn (ref (EcFArray.Array [| 
                                true; true; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; true;
                                true; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; false;
                                false; false; false; false; false; false; false; true |])) DKCSch.GarbleTools.extractGG in*)
     let y = DKCSch.DKCScheme.Scheme.decode ko outG in 
              print_string (if (EcFArray.get y (32)) then "1" else "0");
              print_string (if (EcFArray.get y (31)) then "1" else "0");
              print_string (if (EcFArray.get y (30)) then "1" else "0");
              print_string (if (EcFArray.get y (29)) then "1" else "0");
              print_string (if (EcFArray.get y (28)) then "1" else "0");
              print_string (if (EcFArray.get y (27)) then "1" else "0");
              print_string (if (EcFArray.get y (26)) then "1" else "0");
              print_string (if (EcFArray.get y (25)) then "1" else "0");
              print_string (if (EcFArray.get y (24)) then "1" else "0");
              print_string (if (EcFArray.get y (23)) then "1" else "0");
              print_string (if (EcFArray.get y (22)) then "1" else "0");
              print_string (if (EcFArray.get y (21)) then "1" else "0");
              print_string (if (EcFArray.get y (20)) then "1" else "0");
              print_string (if (EcFArray.get y (19)) then "1" else "0");
              print_string (if (EcFArray.get y (18)) then "1" else "0");
              print_string (if (EcFArray.get y (17)) then "1" else "0");
              print_string (if (EcFArray.get y (16)) then "1" else "0");
              print_string (if (EcFArray.get y (15)) then "1" else "0");
              print_string (if (EcFArray.get y (14)) then "1" else "0");
              print_string (if (EcFArray.get y (13)) then "1" else "0");
              print_string (if (EcFArray.get y (12)) then "1" else "0");
              print_string (if (EcFArray.get y (11)) then "1" else "0");
              print_string (if (EcFArray.get y (10)) then "1" else "0");
              print_string (if (EcFArray.get y (9)) then "1" else "0");
              print_string (if (EcFArray.get y (8)) then "1" else "0");
              print_string (if (EcFArray.get y (7)) then "1" else "0");
              print_string (if (EcFArray.get y (6)) then "1" else "0");
              print_string (if (EcFArray.get y (5)) then "1" else "0");
              print_string (if (EcFArray.get y (4)) then "1" else "0");
              print_string (if (EcFArray.get y (3)) then "1" else "0");
              print_string (if (EcFArray.get y (2)) then "1" else "0");
              print_string (if (EcFArray.get y (1)) then "1" else "0");
              print_string (if (EcFArray.get y (0)) then "1\n" else "0\n");
              "Done!\n";);;  
*)

let _ = print_string (
         let (st2, m1) = Concrete.p2_stage1 (Example1.fn,Example1.i2) Example1.r2 in 
         let (st1, m2) = Concrete.p1_stage1 (Example1.i1) Example1.r1 m1 in
         let m3 = Concrete.p2_stage2 st2 m2 in 
         let y = Concrete.p1_stage2 st1 m3 in 
             print_string (if (_dtlb_rb y (32)) then "1" else "0");
              print_string (if (_dtlb_rb y (31)) then "1" else "0");
              print_string (if (_dtlb_rb y (30)) then "1" else "0");
              print_string (if (_dtlb_rb y (29)) then "1" else "0");
              print_string (if (_dtlb_rb y (28)) then "1" else "0");
              print_string (if (_dtlb_rb y (27)) then "1" else "0");
              print_string (if (_dtlb_rb y (26)) then "1" else "0");
              print_string (if (_dtlb_rb y (25)) then "1" else "0");
              print_string (if (_dtlb_rb y (24)) then "1" else "0");
              print_string (if (_dtlb_rb y (23)) then "1" else "0");
              print_string (if (_dtlb_rb y (22)) then "1" else "0");
              print_string (if (_dtlb_rb y (21)) then "1" else "0");
              print_string (if (_dtlb_rb y (20)) then "1" else "0");
              print_string (if (_dtlb_rb y (19)) then "1" else "0");
              print_string (if (_dtlb_rb y (18)) then "1" else "0");
              print_string (if (_dtlb_rb y (17)) then "1" else "0");
              print_string (if (_dtlb_rb y (16)) then "1" else "0");
              print_string (if (_dtlb_rb y (15)) then "1" else "0");
              print_string (if (_dtlb_rb y (14)) then "1" else "0");
              print_string (if (_dtlb_rb y (13)) then "1" else "0");
              print_string (if (_dtlb_rb y (12)) then "1" else "0");
              print_string (if (_dtlb_rb y (11)) then "1" else "0");
              print_string (if (_dtlb_rb y (10)) then "1" else "0");
              print_string (if (_dtlb_rb y (9)) then "1" else "0");
              print_string (if (_dtlb_rb y (8)) then "1" else "0");
              print_string (if (_dtlb_rb y (7)) then "1" else "0");
              print_string (if (_dtlb_rb y (6)) then "1" else "0");
              print_string (if (_dtlb_rb y (5)) then "1" else "0");
              print_string (if (_dtlb_rb y (4)) then "1" else "0");
              print_string (if (_dtlb_rb y (3)) then "1" else "0");
              print_string (if (_dtlb_rb y (2)) then "1" else "0");
              print_string (if (_dtlb_rb y (1)) then "1" else "0");
              print_string (if (_dtlb_rb y (0)) then "1\n" else "0\n");
              "Done!\n";);; 
