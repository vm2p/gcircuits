open SFE
open Mpz
open EcIArray

(* Add 32 *)
module Example1 = struct
  let i1 : EcPervasive.bool0 tarray = array [|true|]
  let i2 : EcPervasive.bool0 tarray = array [|false|]
  let fn : EcPervasive.int0  * EcPervasive.int0  * EcPervasive.int0  * 
	   (EcPervasive.int0  tarray) * (EcPervasive.int0  tarray) * 
           ((EcPervasive.bool0 * EcPervasive.bool0 * EcPervasive.bool0 * EcPervasive.bool0) tarray) = 
    (2 ,1 ,4 , array [|0;2;0;4|], array [|1;2;1;3|],
     array [|(false,false,false,true);
             (true,true,true,false);
             (false,true,true,true);
             (false,false,false,true)|])
  let r1 : Prime_field.gf_q tarray = make 8 (of_string "2")
  let r2 : ((Prime_field.gf_q tarray * SomeOT.SomeOT.ESn.hkey_t) * Prime_field.gf_q) * DKCSch.DKCScheme.Scheme.rand_t =
    let one = String.concat "" [String.make 1 '\001'; String.make 15 '\000'] in
    (((make 12 (of_string "2"),()),of_string "2"),
     init 6 (fun xk -> (String.copy DKCSch.Token.zeros, String.copy one)))
end

(*
let _ = print_string (
    let fg = DKCSch.DKCScheme.Scheme.funG Example1.fn (snd  Example1.r2) in
    let ki = DKCSch.DKCScheme.Scheme.inputK Example1.fn (snd  Example1.r2) in 
    let ko = DKCSch.DKCScheme.Scheme.outputK Example1.fn (snd  Example1.r2) in 
    let xg = DKCSch.DKCScheme.SchSecurity.encode ki (array [|true;false|]) in
    let outG = DKCSch.DKCScheme.Scheme.evalG fg xg in  
    let y = DKCSch.DKCScheme.Scheme.decode ko outG in
              print_string (if (_dtlb_rb y 0) then "True\n" else "False\n");
              "Done!\n";);; 
*)

let _ = print_string (
         let (st2, m1) = Concrete.p2_stage1 (Example1.fn,Example1.i2) Example1.r2 in 
         let (st1, m2) = Concrete.p1_stage1 (Example1.i1) Example1.r1 m1 in
         let m3 = Concrete.p2_stage2 st2 m2 in 
         let y = Concrete.p1_stage2 st1 m3 in 
             if (_dtlb_rb y 0) then "True\n" else "False\n" );;

