open SFE
open Mpz
open EcIArray

(* 1 XOR GATE *)
module Example0 = struct
  let i1 : EcPervasive.bool0 tarray = array [|true|]
  let i2 : EcPervasive.bool0 tarray = array [|true|]
  let fn : EcPervasive.int0  * EcPervasive.int0  * EcPervasive.int0  * 
           (EcPervasive.int0  tarray) * (EcPervasive.int0  tarray) *
           ((EcPervasive.bool0 * EcPervasive.bool0 * EcPervasive.bool0 * EcPervasive.bool0) tarray) = 
      (2,1,1,array [|0|],array [|1|],array [|(false,true,true,false)|])
  let r1 : Prime_field.gf_q tarray = array [|of_string "2"|]
  let r2 : ((Prime_field.gf_q tarray * SomeOT.SomeOT.ESn.hkey_t) * Prime_field.gf_q) * DKCSch.DKCScheme.Scheme.rand_t = 
    let one = String.concat "" [String.make 1 '\001'; String.make 15 '\000'] in
    (((array [|of_string "2"; of_string "2"|]),()),of_string "2"),
     array [|String.copy DKCSch.Token.zeros,String.copy one;
             String.copy one,String.copy DKCSch.Token.zeros;
             String.copy DKCSch.Token.zeros,String.copy one|]
end

(*
let _ = print_string (
    let fg = DKCSch.DKCScheme.Scheme.funG Example0.fn (snd  Example0.r2) in
    let ki = DKCSch.DKCScheme.Scheme.inputK Example0.fn (snd  Example0.r2) in
    let ko = DKCSch.DKCScheme.Scheme.outputK Example0.fn (snd  Example0.r2) in
    let xg = DKCSch.DKCScheme.SchSecurity.encode ki (array [|true;false|])
    let outG = DKCSch.DKCScheme.Scheme.evalG fg xg in
    let y = DKCSch.DKCScheme.Scheme.decode ko outG in
             if (_dtlb_rb y 0) then "True\n" else "False\n" );;
*)

let _ = print_string (
         let (st2, m1) = Concrete.p2_stage1 (Example0.fn,Example0.i2) Example0.r2 in 
         let (st1, m2) = Concrete.p1_stage1 (Example0.i1) Example0.r1 m1 in
         let m3 = Concrete.p2_stage2 st2 m2 in 
         let y = Concrete.p1_stage2 st1 m3 in 
             if (_dtlb_rb y 0) then "True\n" else "False\n" );;
