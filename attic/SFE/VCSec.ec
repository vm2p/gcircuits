require import Option.
require import Pair.
require import Array.
require import Int.

require import VC.

theory VCSec.

clone import VC.

pred correct (f : fun_t, x : inp_t, rg : randg_t, rp : randp_t)  = 
     (valid_i  f x) /\
     let (pk,sk) = kGen f rg in
     let (xp,xs) = pGen x sk rp in
     let pr = comp pk xp in
     let y = vrfy sk pr xs in
         proj y = eval f x.

pred Correct (x:unit) =
     forall (f : fun_t) (x : inp_t) (rg : randg_t) (rp : randp_t),
        valid_i f x => valid_r f rg rp => correct f x rg rp.

  module type RandG_t = { fun gen(f : fun_t) : randg_t {*} }.
  module type RandP_t = { fun gen(f : fun_t) : randp_t {*} }.

  (* All definitions will be parametrized by a fixed function *)
  theory F.
  op fn : fun_t.
  end F.

   (* Maximum number of queries in all models *)
  op max_qr : int.
  axiom max_qr_pos : max_qr > 0.

  (* Definitions needed for IND model *)
  type query_IND = inp_t * inp_t.

  op queryValid_IND(fn : fun_t, query : query_IND) : bool =
       (valid_i fn (fst query)) /\ (valid_i fn (snd query)).

  module type O_IND_t = {
      fun probGen(x : inp_t) : xpub_t option
      fun challenge(query:query_IND) : xpub_t option
  }.

  module type Adv_IND_t(O : O_IND_t) = { 
    fun guess(pk : pkey_t) : bool { O.probGen O.challenge }
  }.

  module O_IND(RG : RandG_t, RP : RandP_t) = {
     var b : bool
     var validQs : bool
     var pk : pkey_t
     var sk : skey_t
     var count : int

     fun init(bval : bool) : pkey_t = {
         var rg:randg_t;
         b = bval;
         validQs = true;
         count = 0;
         rg = RG.gen(F.fn);
         (pk,sk) = kGen F.fn rg;
         return pk;
     }

    fun probGen(x : inp_t): xpub_t option = {
      var xp:xpub_t;
      var xs:xprv_t;
      var rp:randp_t;
      var ret : xpub_t option;

      if (valid_i F.fn x)
      {
        if (count < max_qr) {
          rp = RP.gen(F.fn);
          (xp,xs) = pGen x sk rp;
          ret = Some xp;
          count = count + 1;
        }
        else
          ret = None;
      }
      else {
        validQs = false;
        ret = None;
      }
      return ret;
    }

    fun challenge(query : query_IND): xpub_t option = {
      var xp:xpub_t;
      var xs:xprv_t;
      var rp:randp_t;
      var x : inp_t;
      var ret : xpub_t option;

      if (queryValid_IND F.fn query)
      {
        if (count < max_qr) {
          x = if b then snd query else fst query;
          rp = RP.gen(F.fn);
          (xp,xs) = pGen x sk rp;
          ret = Some xp;
          count = count + 1;
        }
        else
          ret = None;
      }
      else {
        validQs = false;
        ret = None;
      }
      return ret;
    }

    fun check(b' : bool) : bool = {
         var ret : bool;

         if (validQs) {
             ret = (b' = b);
         }
         else {
             ret = ${0,1}; 
         }
         return ret;
     }
  }.

  module Game_IND(RG : RandG_t, RP : RandP_t, ADV:Adv_IND_t) = {
    module O = O_IND(RG,RP)
    module A = ADV(O)

    fun main(b : bool): bool = {
      var b',ret : bool;
      var pk : pkey_t;

      pk = O.init(b);
      b' = A.guess(pk);
      ret = O.check(b');
      return ret;
    }
  }.

  (* Definitions needed for VRF model *)
 
  op queryValid_VRF(fn : fun_t, query : inp_t) : bool = valid_i fn query.

  module type PGen_t = { fun pgen(x : inp_t) : xpub_t option }.

  module type Adv_VRF_t(O : PGen_t) = { 
    fun forge(pk : pkey_t) : int * proof_t {* O.pgen } 
  }.

  module O(RG : RandG_t, RP : RandP_t) = {
     var qlist : (inp_t * xpub_t  * xprv_t) array
     var pk : pkey_t
     var sk :skey_t
     var validQs : bool

     fun init() : pkey_t = {
        var r : randg_t;

        validQs = true;
        qlist = empty;
        r = RG.gen(F.fn);
        (pk,sk) = kGen F.fn r ;
        return pk;
     }
     
     fun pgen(x : inp_t) : xpub_t option = {
        var r : randp_t;
        var xp : xpub_t;
        var xs : xprv_t;
        var rep :xpub_t option;

        if (!queryValid_VRF F.fn x) {
            validQs = false;
            rep = None;
        }
        else { 
           r = RP.gen(F.fn);
           (xp,xs) = pGen x sk r;
           if (length qlist >= max_qr) { rep = None; } 
           else { qlist =(x,xp,xs) :: qlist; rep = Some xp; }
        }
        return rep;  
     }

     fun check(ip : int, pr : proof_t) : bool = {
        var x : inp_t;
        var y : out_t option;
        var xp : xpub_t;
        var xs : xprv_t;
        var r : bool;
        
        if (ip >= 0 /\ ip < length qlist /\ length qlist = max_qr /\ validQs = true) {
           (x,xp,xs) = qlist.[ip];
           y = vrfy sk pr xs;
           r = (y <> None /\ eval F.fn x <> proj y);
        }
        else r = false;

        return r;
     }
  }.

  module Game_Ver(RG: RandG_t, RP: RandP_t, ADV : Adv_VRF_t) = {
    var i : int
    
    module O = O(RG,RP)
    module A = ADV(O)

    fun main() : bool = {
      var pk : pkey_t;
      var pr : proof_t;
      var r : bool;

      pk = O.init();
      (i,pr) = A.forge(pk);
      r = O.check(i,pr);
      return r;
  }
}.

end VCSec.

