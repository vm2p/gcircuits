require import Option.

require Sch.
require SchSec.

theory SchAuthSec.

  clone import Sch.Scheme as Scheme.

  module type Rand_t = {
     fun gen(l : leak_t): rand_t {*}
  }.

  module type Adv_Auth_t = { 
    fun gen_fun() : fun_t {*}
    fun gen_input(fG : funG_t) : Input.input_t
    fun forge(X : Input.inputG_t) : outputG_t
  }.

  (* We need an adaptive version of Bellare's Authenticity property *)
  module Game_Auth(R: Rand_t, A : Adv_Auth_t) = {
    var fn : fun_t
    var x : Input.input_t
    
    fun main() : bool = {
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r : bool;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      r = validInputs fn x /\ valid_outG oK oG /\ eval fn x <> decode oK oG;

      return r;
    }
  }.


 (* And we need it written in oracle form *)
  module type O_AUT'_t = { fun enc(x : Input.input_t) : Input.inputG_t option }.

  module type Adv_AUT'_t(O : O_AUT'_t) = { 
    fun gen_fun() : fun_t
    fun forge(fG : funG_t) : outputG_t { O.enc }
  }.

  module O = {
     var queryDone : bool
     var iK : Input.inputK_t
     var _x : Input.input_t

     fun init(_iK : Input.inputK_t) : unit = {
        iK = _iK;
        queryDone = false;
     }
     
     fun enc(x : Input.input_t) : Input.inputG_t option = {
        var rep: Input.inputG_t option;
        var xG : Input.inputG_t;

        _x = x;

        if (queryDone) { rep = None; } else { 
           queryDone = true; 
           xG = Input.encode iK x;
           rep = Some xG; 
        }
        return rep;  
     }

     fun getX() : Input.input_t = {
        return _x;
     }
  }.

 module Game_AUT'(R: Rand_t, ADV : Adv_AUT'_t) = {
    module A = ADV(O)

    var fn : fun_t
    var x : Input.input_t
    
    fun main() : bool = {
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var oG : outputG_t;
      var r : bool;
      var x : Input.input_t;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      iK = inputK fn rg;
      oK = outputK fn rg;
      O.init(iK);
      oG = A.forge(fG);
      x = O.getX();
      r = validInputs fn x /\ valid_outG oK oG /\ eval fn x <> decode oK oG;
      return r;
  }
}.

end SchAuthSec.

