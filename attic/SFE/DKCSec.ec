require import Array.
require import Map.
require import FSet.
require import Bool.
require import Pair.
require import Int.
require import Distr.

require import DKC.
require ExtWord.

theory DKCSecurity.
  clone import ExtWord as W.

  clone export DKC as D with
    type tweak_t = word,
    type key1_t = word,
    type key2_t = word,
    type msg_t = word,
    type cipher_t = word.

  type query = (int * bool) * (int * bool) * bool * word.
  type answer = word * word * word.

  op defaultQ: query. (* Why use this instead of option? What happens if the adversary queries defaultQ? *)
  op bad: answer.     (* Same question: can any query other than defaultQ return bad? *)
                      (* Response : we need this in order to initlaize array *)
  module type Dkc_t = {
    fun preInit(): unit
    fun initialize(): bool
    fun encrypt(q:query): answer
    fun get_challenge(): bool
  }.

  module type Adv_t = {
    fun preInit(): unit
    fun gen_queries(info:bool): query array
    fun get_challenge(answers:answer array) : bool
  }.

  module DKCm = {
    var b:bool
    var ksec:word
    var r:(int * bool,word) map
    var kpub:(int * bool,word) map
    var used:word set
  }.

  module Dkc : Dkc_t = {
    fun preInit(): unit = {
      DKCm.b = $Dbool.dbool;
    }
      
    fun initialize() : bool = {
      var t : bool;
      t = $Dbool.dbool;
      DKCm.ksec = $Dword.dwordLsb t;
      DKCm.kpub = Map.empty;
      DKCm.used = FSet.empty;
      DKCm.r = Map.empty;
      return t;
    }

    fun get_challenge() : bool = {
      return DKCm.b;
    }

    fun get_k(i:int * bool): word = {
      var r:word;

      r = $Dword.dwordLsb (snd i);
      if (!in_dom i DKCm.kpub) DKCm.kpub.[i] = r;
      return proj DKCm.kpub.[i];
    }

    fun get_r(i:int * bool): word = {
      var r:word;

      r = $Dword.dword;
      if (!in_dom i DKCm.kpub) DKCm.kpub.[i] = r;
      return proj DKCm.kpub.[i];
    }

    fun encrypt(q:query): answer = {
      var ka, kb, ki, kj, rj, msg, t:word;
      var i, j:int * bool;
      var pos:bool;
      var out:answer = bad;

      (i,j,pos,t) = q;
      if (!(mem t DKCm.used) /\ fst i < fst j)
      {
        DKCm.used = add t DKCm.used;
        ki = get_k(i);
        kj = get_k(j);
        rj = get_r(j);

        (ka,kb) = if pos then (DKCm.ksec,ki) else (ki,DKCm.ksec);
        msg = if (DKCm.b) then kj else rj;
        out = (ki,kj,E t ka kb msg);
      }
      return out;
    }
  }.

  lemma get_kL :
    islossless Dkc.get_k.
  proof strict.
  by fun; wp; rnd; skip; smt.
  qed.

  (*TODO remove condition *)
  lemma get_rL:
    islossless Dkc.get_r.
  proof strict.
  by fun; wp; rnd; skip; smt.
  qed.

  (*TODO remove condition *)
  lemma encryptL:
    islossless Dkc.encrypt.
  proof strict.
  fun; seq 2: (q = (i,j,pos,t))=> //; first by wp.
  by if=> //; wp; call get_rL=> //; do !(call get_kL=> //); wp.
  by hoare; wp.
  qed.

  module type Exp = {
    fun preInit():unit
    fun work():bool
    fun main():bool
  }.

  module type Batch_t = {
    fun encrypt(qs:query array): answer array
  }.

  module BatchDKC (D:Dkc_t): Batch_t = {
    fun encrypt(qs:query array): answer array = {
      var i, n:int;
      var answers:answer array;

      i = 0;
      n = length qs;
      answers = Array.init n (lambda x, bad);
      while (i < n)
      {
        answers.[i] = D.encrypt(qs.[i]);
        i = i + 1;
      }

      return answers;
    }
  }.

  lemma encryptBatchL (D <: Dkc_t { BatchDKC }):
    islossless D.encrypt =>
    islossless BatchDKC(D).encrypt.
  proof strict.
  intros=> encryptL; fun.
  while true (n - i);
    first by intros=> z; wp; call encryptL; skip; smt.
  by wp; skip; smt.
  qed.

  module Game(D:Dkc_t, A:Adv_t) : Exp = {
    module B = BatchDKC(D)

    fun preInit() : unit = {
      D.preInit();
      A.preInit();
    }

    fun work() : bool = {
      var queries : query array;
      var answers : answer array;
      var a : answer array;
      var i : int;
      var info : bool;
      var advChallenge : bool;
      var realChallenge : bool;
      var nquery : int;
      var answer : answer;

      info = D.initialize();
      queries = A.gen_queries(info);
      answers = B.encrypt(queries);
      advChallenge = A.get_challenge(answers);
      realChallenge = D.get_challenge();
      return advChallenge = realChallenge;
    }

    fun main() : bool = {
      var r : bool;
      preInit();
      r = work();
      return r;
    }
  }.

lemma preInitE (D<:Dkc_t) (A<:Adv_t{D}) (D'<:Dkc_t) (A'<:Adv_t{D'}) :
  equiv[A.preInit ~ A'.preInit: true ==> true] =>
  equiv[D.preInit ~ D'.preInit: true ==> ={res}] =>
    equiv[Game(D, A).preInit ~ Game(D', A').preInit: ={glob Game}==> ={glob Game} /\ ={res}].
proof strict.
by intros h1 h2;fun;call h1;call h2.
qed.

lemma workE (D<:Dkc_t) (A<:Adv_t{D}) (D'<:Dkc_t) (A'<:Adv_t{D'}) :
  equiv[A.gen_queries ~ A'.gen_queries: ={info} ==> ={res}] =>
  equiv[A.get_challenge ~ A'.get_challenge: ={answers} ==> ={res}] =>
  equiv[D.initialize ~ D'.initialize: true ==> ={res}] =>
  equiv[D.encrypt ~ D'.encrypt: ={q} ==> ={res}] =>
  equiv[D.get_challenge ~ D'.get_challenge: true ==> ={res}] =>
    equiv[Game(D, A).work ~ Game(D', A').work: ={glob Game} ==> ={glob Game} /\ ={res}].
proof strict.
intros h1 h2 h3 h4 h5.
fun.
inline Game(D, A).B.encrypt.
inline Game(D', A').B.encrypt.
call h5;call h2;wp.
while (={i0, qs, n, answers0});
  first by wp;call h4.
by wp;call h1;call h3.
qed.

lemma mainE (D<:Dkc_t) (A<:Adv_t{D}) (D'<:Dkc_t) (A'<:Adv_t{D'}) :
  equiv[A.preInit ~ A'.preInit: true ==> true] =>
  equiv[A.gen_queries ~ A'.gen_queries: ={info} ==> ={res}] =>
  equiv[A.get_challenge ~ A'.get_challenge: ={answers} ==> ={res}] =>
  equiv[D.preInit ~ D'.preInit: true ==> ={res}] =>
  equiv[D.initialize ~ D'.initialize: true ==> ={res}] =>
  equiv[D.encrypt ~ D'.encrypt: ={q} ==> ={res}] =>
  equiv[D.get_challenge ~ D'.get_challenge: true ==> ={res}] =>
    equiv[Game(D, A).main ~ Game(D', A').main: ={glob Game} ==> ={glob Game} /\ ={res}].
proof strict.
progress.
fun.
call (workE D A D' A' _ _ _ _ _)=> //.
by call (preInitE D A D' A' _ _).
qed.

  (** Adaptive adversary: is it possible to find an expressible
      condition on A.work that guarantees the desired equivalence?
   INSIGHT: it looks like it should be sufficient to look at Query
      to prove the adaptive -> non-adaptive result:
        1) it is never called with a = b;
        2) for a given g, it is called three times with rnd = false,
           but different (alpha,beta) pairs (hence different tweaks);
        3) for a given g, it is called once with rnd = true (fresh tweak, random j)
        4) for different g, the tweaks are necessarily different
      **5) the queries (i,j,pos,T) depend only on a, b, g, the two constants alpha and beta,
           and pre-initialized variables (the circuit and the t.[i]s) *)
  module type AdvAda_t(DKC:Dkc_t) = {
    fun preInit() : unit {}
    fun work(info:bool) : bool {DKC.encrypt}
  }.

  module GameAda(D:Dkc_t, Adv:AdvAda_t): Exp = {
    module A = Adv(Dkc)

    fun preInit(): unit = {
      D.preInit();
      A.preInit();
    }

    fun work(): bool = {
      var info: bool;
      var advChallenge: bool;
      var realChallenge: bool;

      info = D.initialize();

      advChallenge = A.work(info);
      realChallenge = D.get_challenge();
      return advChallenge = realChallenge;
    }

    fun main(): bool = {
      var r: bool;

      preInit();
      r = work();
      return r;
    }
  }.
end DKCSecurity.
