require import Bool.
require import Int.
require import Pair.
require import Real.
require import Distr.

require import Array.

(** Entropy Smoothing security assumption *)
require import Prime_field.
require import Cyclic_group_prime.

theory KeyedHash.
 type dom_t.
 type codom_t.
 type hkey_t.
 op dkey:hkey_t distr.

 op hash:hkey_t -> dom_t -> codom_t.
end KeyedHash.

(** Entropy-Smoothing *)
theory ES.
  type dom_t.
  type codom_t.
  type hkey_t.
  op ddom:dom_t distr.
  op dcodom:codom_t distr.
  op dkey:hkey_t distr.

  clone KeyedHash as H with
   type dom_t = dom_t,
   type codom_t = codom_t,
   type hkey_t = hkey_t,
   op dkey = dkey.

  module type Adv_t = {
    fun solve(k:hkey_t, x:codom_t): bool
  }.

  module Game (A:Adv_t) = {
    fun main(): bool = {
      var x:dom_t;
      var k:hkey_t;
      var b, b':bool;
      var y:codom_t;

      x = $ddom;
      k = $dkey;
      y = $dcodom;
      b = ${0,1};
      if (b) b' = A.solve(k,H.hash k x); else b' = A.solve(k,y);
      return b = b';
    }
  }.

(*
 axiom EntropySmoothing:
  isuniform dkey =>
  isuniform dcodom =>
  exists (epsilon:real),
   forall (A<:Adv_t) &m,
    `|2%r * Pr[Game(A).main()@ &m:res] - 1%r| <= epsilon.
*)
end ES.

(** List variation of the Entropy-Smoothing assumption 

   Inputs: list (array) of elements in codom_t
   Output: a boolean b, trying to guess if those elements are the image of the hash, or random elements
*)
theory ESn.
  type dom_t.
  type codom_t.
  type hkey_t.
  op ddom:dom_t distr.
  op dcodom:codom_t distr.
  op dkey:hkey_t distr.

  clone KeyedHash as H with
   type dom_t = dom_t,
   type codom_t = codom_t,
   type hkey_t = hkey_t,
   op dkey = dkey.
  
  module type Adv_t = {
    fun choose_n(): int
    fun solve(key: hkey_t, a:codom_t array): bool
  }.

  module Game (B:Adv_t) = {
    fun main(): bool = {
      var n:int;
      var key:hkey_t;
      var x:dom_t array;
      var y,z:codom_t array;
      var b, guess:bool;

      b = ${0,1};

      n = B.choose_n();
      n = if n < 0 then 0 else n;

      key = $H.dkey;
      x = $Darray.darray n ddom;
      y = $Darray.darray n dcodom;

      if (b)
       z = init n (lambda k, H.hash key x.[k]);
      else
       z = y;
      
      guess = B.solve(key, z);
      return guess = b;
    }
  }.

  module Game' (B:Adv_t) = {
    fun main(): bool*bool = {
      var n:int;
      var key:hkey_t;
      var x:dom_t array;
      var y,z:codom_t array;
      var b, guess:bool;

      b = ${0,1};

      n = B.choose_n();
      n = if n < 0 then 0 else n;

      key = $H.dkey;
      x = $Darray.darray n ddom;
      y = $Darray.darray n dcodom;

      if (b)
       z = init n (lambda k, H.hash key x.[k]);
      else
       z = y;
      
      guess = B.solve(key, z);
      return (b, guess = b);
    }
  }.

lemma Game_equiv (A <: Adv_t):
 equiv [ Game(A).main ~ Game'(A).main: 
         ={glob A} ==> res{1} = snd res{2} ].
proof strict.
fun.
seq 6 6: (={n, b, key, x, y, glob A}).
 rnd; rnd; rnd; wp.
 call (_: ={glob A} ==> ={res, glob A}); first by fun true.
 by rnd;skip; progress.
case (b{1}).
 rcondt{1} 1; first by intros &m; skip.
 rcondt{2} 1; first by intros &m; skip.
 call (_: ={glob A, key, a} ==> ={res}); first by fun true.
 by wp; skip.
rcondf{1} 1; first by intros &m; skip.
rcondf{2} 1; first by intros &m; skip.
call (_: ={glob A, key, a} ==> ={res}); first by fun true.
by wp; skip.
qed.

lemma Game_pr (A <: Adv_t) &m:
 Pr [Game(A).main() @ &m : res] 
 =  Pr [Game'(A).main() @ &m : snd res].
proof strict.
by equiv_deno (Game_equiv A).
qed.

lemma Game_lossless (A <: Adv_t):
 weight dkey = 1%r =>
 weight ddom = 1%r =>
 weight dcodom = 1%r => 
 islossless A.choose_n =>
 islossless A.solve =>
 islossless Game'(A).main.
proof strict.
intros=> dkeyF ddomF dcodomF A_choose_ll A_solve_ll; fun.
seq 1: true=> //;
 first by rnd; skip; smt.
case (b).
 rcondt 6=> //.
  rnd; rnd; rnd; wp.
  call (_:true ==> true); first by fun true.
  skip; smt.
 call (_:true ==> true); first by fun true.
 wp; rnd; rnd; rnd; wp.
 call (_:true ==> true); first by fun true.
 skip; progress; smt.
rcondf 6=> //.
 rnd; rnd; rnd; wp.
 call (_:true ==> true); first by fun true.
 skip; smt.
call (_:true ==> true); first by fun true.
wp; rnd; rnd; rnd; wp.
call (_:true ==> true); first by fun true.
skip; progress; smt.
qed.

lemma Game_true_pr (A <: Adv_t) &m:
 weight dkey = 1%r =>
 weight ddom = 1%r =>
 weight dcodom = 1%r => 
 islossless A.choose_n =>
 islossless A.solve =>
 Pr [Game'(A).main() @ &m : true] = 1%r.
proof strict.
by intros=>  dkeyF ddomF dcodomF A_choose_ll A_solve_ll;
bdhoare_deno (Game_lossless A _ _ _ _ _).
qed.

lemma Game_real_pr (A <: Adv_t) &m:
 weight dkey = 1%r =>
 weight ddom = 1%r =>
 weight dcodom = 1%r => 
 islossless A.choose_n =>
 islossless A.solve =>
 Pr [Game'(A).main() @ &m : fst res] = (1%r/2%r).
proof strict.
intros dkeyF ddomF dcodomF A_choose_ll A1_solve_ll;
bdhoare_deno (_: true ==> fst res)=> //; fun.
seq 1: b (1%r/2%r) 1%r _ 0%r=> //.
  by rnd; skip; progress; rewrite Dbool.mu_def /charfun /=.
 rcondt 6.
  rnd; rnd; rnd; wp.
  call (_:true ==> true); first by fun true; smt.
  skip; smt.
 call (_:true==>true); first by fun true.
 wp; rnd; rnd; rnd; wp; call (_:true ==> true); first by fun true.
 skip; progress; smt.
hoare. 
rcondf 6.
 rnd; rnd; rnd; wp.
 call (_:true ==> true); first by fun true; smt.
 skip; smt.
call (_:true==>true); first by fun true.
wp; rnd; rnd; rnd; wp; call (_:true ==> true); first by fun true.
skip; progress; trivial.
save.

lemma Game_Nreal_pr (A <: Adv_t) &m:
 weight dkey = 1%r =>
 weight ddom = 1%r =>
 weight dcodom = 1%r => 
 islossless A.choose_n =>
 islossless A.solve =>
 Pr [Game'(A).main() @ &m : !fst res] = 1%r/2%r.
proof strict.
intros dkeyF ddomF dcodomF A_choose_ll A1_solve_ll.
rewrite Pr mu_not.
rewrite (Game_true_pr A &m) //.
rewrite (Game_real_pr A &m) //.
cut H: 1%r = 2%r / 2%r by smt.
by rewrite {1}H; smt.
qed.

 lemma Adv_xpnd (A<: Adv_t) &m:
  weight dkey = 1%r =>
  weight ddom = 1%r =>
  weight dcodom = 1%r => 
  islossless A.choose_n =>
  islossless A.solve =>
  2%r * Pr[Game(A).main()@ &m:res] - 1%r 
  = 2%r * ( Pr[ Game'(A).main() @ &m : !fst res => snd res]
           - Pr[ Game'(A).main() @ &m : fst res => !snd res] ).
proof strict.
intros=> dkeyF ddomF dcodomF A_choose_ll A_solve_ll.
rewrite (Game_pr A &m).
cut ->: ( Pr [ Game'(A).main() @ &m : !fst res => snd res]
          = Pr [ Game'(A).main() @ &m : fst res]
            + Pr [ Game'(A).main() @ &m : snd res]
            - Pr [ Game'(A).main() @ &m : snd res && fst res]).
 cut ->: (Pr[Game'(A).main() @ &m : !fst res => snd res]
          = Pr[Game'(A).main() @ &m : snd res \/ fst res]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; smt.
cut ->:  Pr [ Game'(A).main() @ &m : fst res => !snd res]
  = Pr [ Game'(A).main() @ &m : !fst res] + 1%r
    - Pr [ Game'(A).main() @ &m : snd res]
    - Pr [ Game'(A).main() @ &m : !snd res && !fst res].
 cut ->: (Pr[Game'(A).main() @ &m : fst res => !snd res]
         = Pr[Game'(A).main() @ &m : !snd res \/ !fst res]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; rewrite Pr mu_not.
 rewrite (Game_true_pr A &m) //; smt.
rewrite (Game_Nreal_pr A &m) // (Game_real_pr A &m) //.
cut H1: forall (x y:real), x - y = x + [-] y by smt.
cut H2: forall (x y:real), [-] (x + y) = [-] x + [-] y by smt.
cut H3: forall (x:real), [-] ([-] x) = x by smt.
cut H4: forall (x:real), 2%r * x = x + x.
 intros x; cut ->:2%r = (1%r + 1%r) by smt.
 by rewrite Mul_distr_r.
cut H4': forall (x y:real), 2%r * (x-y) = 2%r * x - 2%r * y by smt.
cut H5: forall (x:real), 2%r * -x = - 2%r * x by smt.
rewrite ?H1 ?H2 ?H3.
cut ->: (1%r / 2%r + Pr[Game'(A).main() @ &m : snd res] +
 -Pr[Game'(A).main() @ &m : snd res && fst res] +
 (-(1%r / 2%r) + -1%r + Pr[Game'(A).main() @ &m : snd res] +
  Pr[Game'(A).main() @ &m : !snd res && ! fst res]))
 = ( (Pr[Game'(A).main() @ &m : snd res] + Pr[Game'(A).main() @ &m : snd res]) - 1%r) - (Pr[Game'(A).main() @ &m : snd res && fst res] -
      Pr[Game'(A).main() @ &m : !snd res && ! fst res]) by smt.
rewrite -H4 H4'.
cut ADV1_xpand' : 2%r * Pr [ Game'(A).main() @ &m : snd res] - 1%r
  = 2%r * ( Pr [ Game'(A).main() @ &m : snd res && fst res]
        - Pr [ Game'(A).main() @ &m : !snd res && !fst res] ).
 cut Hpr1: Pr [ Game'(A).main() @ &m : snd res]
            = Pr [ Game'(A).main() @ &m : snd res && fst res]
             + Pr [ Game'(A).main() @ &m : snd res && !fst res].
  cut ->: Pr[Game'(A).main() @ &m : snd res]
           = Pr[Game'(A).main() @ &m
               : snd res && fst res \/ snd res && !fst res].
   rewrite Pr mu_eq; smt.
  rewrite Pr mu_disjoint; smt.
 cut Hpr2 : Pr [ Game'(A).main() @ &m : snd res && !fst res]
            = Pr [ Game'(A).main() @ &m : !fst res]
            - Pr [ Game'(A).main() @ &m : !snd res && !fst res].
  cut ->: Pr[Game'(A).main() @ &m : !fst res]
           = Pr[Game'(A).main() @ &m 
               : snd res && !fst res \/ !snd res && !fst res].
   rewrite Pr mu_eq; smt.
  rewrite Pr mu_disjoint; smt.
 rewrite Hpr1 Hpr2 (Game_Nreal_pr A &m) //.
 smt.
rewrite -ADV1_xpand' //.
smt.
save.

 (* reduction: epsilon_ESn = n * epsilon_ES *)
(*
 op epsilon : real.

 axiom ArrayEntropySmoothing:
  isuniform dkey => isuniform ddom => isuniform dcodom =>
  forall (A <: Adv_t) &m,
    `|2%r * Pr[Game(A).main()@ &m:res] - 1%r| <= epsilon.
*)
end ESn.
