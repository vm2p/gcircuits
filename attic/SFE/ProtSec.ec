require import Bool.
require import Pair.
require import Real.

require import Prot.

theory ProtSecurity.
  clone import Protocol.

  (* Alias for the correctness assertion *) 
  (* This will need to be proven for all protocols *)
  pred Correct (x:unit) = forall i1 r1 i2 r2, 
    validInputs i1 i2 =>
    validRands i1 i2 r1 r2 =>
    f i1 i2 = snd (prot i1 r1 i2 r2).

  (* following Goldreich, we include the random coins in each party view
  (but leave the input implicit in the context...) *)
  type view1_t = rand1_t * conv_t.
  type view2_t = rand2_t * conv_t.

  (* randomness can only depend on public (leaked) info *)
  module type Rand1_t = { fun gen(i1info : leak1_t) : rand1_t }.
  module type Rand2_t = { fun gen(i2info : leak2_t) : rand2_t }.

  (* Simulator & Adversaries *)
  module type Sim_t = {
    fun sim1(i1: input1_t, o1: output1_t, l2: leak2_t) : view1_t
    fun sim2(i2: input2_t, o2: output2_t, l1: leak1_t) : view2_t
  }.

  module type Adv1_t = { 
    fun gen_query() : input1_t * input2_t
    fun dist(view: view1_t) : bool 
  }.

  module type Adv2_t = { 
    fun gen_query() : input1_t * input2_t
    fun dist(view: view2_t) : bool 
  }.

  module Game1(R1: Rand1_t, R2: Rand2_t, S: Sim_t, A1: Adv1_t) = {
    fun main() : bool = {
      var real, adv : bool;
      var view1 : view1_t;
      var o1 : output1_t;
      var r1 : rand1_t;
      var r2 : rand2_t;
      var i1 : input1_t;
      var i2 : input2_t;
    
      (i1,i2) = A1.gen_query();
      real = $Dbool.dbool;

      if (!validInputs i1 i2)
        adv = $Dbool.dbool;
      else {
        if (real) {
          r1 = R1.gen(phi1 i1);
          r2 = R2.gen(phi2 i2);
          view1 = (r1, fst (prot i1 r1 i2 r2));
        } else {
          o1 = fst (f i1 i2);
          view1 = S.sim1 (i1, o1, phi2(i2));
        }       
        adv = A1.dist(view1);
      }
      return (adv = real);
    }
  }.

  module Game2(R1: Rand1_t, R2: Rand2_t, S: Sim_t, A2: Adv2_t) = {
    fun main() : bool = {
      var real, adv : bool;
      var view2 : view2_t;
      var o2 : output2_t;
      var r1 : rand1_t;
      var r2 : rand2_t;
      var i1 : input1_t;
      var i2 : input2_t;
    
      (i1,i2) = A2.gen_query();
      real = $Dbool.dbool;
      
      if (!validInputs i1 i2) {
        adv = $Dbool.dbool;
      } else {
        if (real) {
          r1 = R1.gen(phi1 i1);
          r2 = R2.gen(phi2 i2);
          view2 = (r2, fst (prot i1 r1 i2 r2));
        } else {
          o2 = snd (f i1 i2);
          view2 = S.sim2 (i2, o2, phi1(i1));      
        }  
        adv = A2.dist(view2);
      }
      return (adv = real);
    }
  }.

  module Game1'(R1: Rand1_t, R2: Rand2_t, S: Sim_t, A1: Adv1_t) = {
    fun main() : bool*bool = {
      var real, adv : bool;
      var view1 : view1_t;
      var o1 : output1_t;
      var r1 : rand1_t;
      var r2 : rand2_t;
      var i1 : input1_t;
      var i2 : input2_t;

      (i1,i2) = A1.gen_query();
      real = $Dbool.dbool;

      if (!validInputs i1 i2)
        adv = $Dbool.dbool;
      else {
        if (real) {
          r1 = R1.gen(phi1 i1);
          r2 = R2.gen(phi2 i2);
          view1 = (r1, fst (prot i1 r1 i2 r2));
        } else {
          o1 = fst (f i1 i2);
          view1 = S.sim1 (i1, o1, phi2(i2));
        }
        adv = A1.dist(view1);
      }
      return (real, adv = real);
    }
  }.

  module Game2'(R1: Rand1_t, R2: Rand2_t, S: Sim_t, A2: Adv2_t) = {
    fun main() : bool*bool = {
      var real, adv : bool;
      var view2 : view2_t;
      var o2 : output2_t;
      var r1 : rand1_t;
      var r2 : rand2_t;
      var i1 : input1_t;
      var i2 : input2_t;

      (i1,i2) = A2.gen_query();
      real = $Dbool.dbool;

      if (!validInputs i1 i2) {
        adv = $Dbool.dbool;
      } else {
        if (real) {
          r1 = R1.gen(phi1 i1);
          r2 = R2.gen(phi2 i2);
          view2 = (r2, fst (prot i1 r1 i2 r2));
        } else {
          o2 = snd (f i1 i2);
          view2 = S.sim2 (i2, o2, phi1(i1));      
        }  
        adv = A2.dist(view2);
      }
      return (real, adv = real);
    }
  }.

  (****************************************************************)
  (* Useful lemmas for plugging protocol security in other proofs *)
  (****************************************************************)

  equiv Game1_equiv (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A1 <: Adv1_t{R1,R2,S}):
    Game1(R1, R2, S, A1).main ~ Game1'(R1, R2, S, A1).main: 
      ={glob R1, glob R2, glob S, glob A1} ==>
      res{1} = snd res{2}.
  proof strict.
  fun; seq 2 2: (={i1, i2, real, glob R1, glob R2, glob A1, glob S}).
    by rnd; call (_: true).
  if=> //; first by rnd.
  if=> //.
    by do !(call (_: true); wp);
       call (_: ={glob R1, i1info} ==> ={res})=> //;
         first by fun true.
    by do !(call (_: true); wp).
  qed.

  lemma Game1_pr (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A1 <: Adv1_t{R1,R2,S}) &m:
    Pr [Game1(R1, R2, S, A1).main() @ &m : res] =
    Pr [Game1'(R1, R2, S, A1).main() @ &m : snd res].
  proof strict.
  by equiv_deno (Game1_equiv R1 R2 S A1).
  qed.

  lemma Game1_lossless (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A1 <: Adv1_t):
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim1 =>
    islossless A1.gen_query =>
    islossless A1.dist =>
    islossless Game1'(R1, R2, S, A1).main.
  proof strict.
  intros=> R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll; fun.
  seq 2: true=> //;
    first by rnd; call A1_gen_ll; skip; smt.
  if; first by rnd; skip; smt.
  call A1_dist_ll; if.
    by wp; call R2_ll; call R1_ll.
    by call S_ll; wp.
  qed.

  lemma Game1_true_pr (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A1 <: Adv1_t) &m:
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim1 =>
    islossless A1.gen_query =>
    islossless A1.dist =>
    Pr [Game1'(R1, R2, S, A1).main() @ &m : true] = 1%r.
  proof strict.
  by intros=> R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll;
     bdhoare_deno (Game1_lossless R1 R2 S A1 _ _ _ _ _).
  qed.

  lemma Game1_real_pr (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A1 <: Adv1_t) &m:
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim1 =>
    islossless A1.gen_query =>
    islossless A1.dist =>
    Pr [Game1'(R1, R2, S, A1).main() @ &m : fst res] = (1%r/2%r).
  proof strict.
  intros=> R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll.
  bdhoare_deno (_: true ==> fst res)=> //; fun.
  seq 1: true 1%r (1%r/2%r) 0%r _=> //;
    first by call A1_gen_ll.
  seq 1: real (1%r/2%r) 1%r _ 0%r=> //.
    by rnd; skip; progress; rewrite Dbool.mu_def.
    if; first by rnd; skip; smt.
    call A1_dist_ll; if.
      by wp; call R2_ll; call R1_ll.
      by call S_ll; wp.
    by hoare; conseq* (_: _ ==> true).
  qed.

  lemma Game1_Nreal_pr (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A1 <: Adv1_t) &m:
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim1 =>
    islossless A1.gen_query =>
    islossless A1.dist =>
    Pr [Game1'(R1, R2, S, A1).main() @ &m : !(fst res)] = 1%r/2%r.
  proof strict.
  intros=> R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll.
  rewrite Pr mu_not.
  rewrite (Game1_true_pr R1 R2 S A1 &m) //.
  rewrite (Game1_real_pr R1 R2 S A1 &m) //.
  cut H: 1%r = 2%r / 2%r by smt.
  by rewrite {1}H; smt.
  qed.

  lemma ADV1_xpnd (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A1 <: Adv1_t{R1,R2,S}) &m:
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim1 =>
    islossless A1.gen_query =>
    islossless A1.dist => 
    2%r * Pr [ Game1(R1, R2, S, A1).main() @ &m : res] - 1%r =
    2%r * (Pr [ Game1'(R1, R2, S, A1).main() @ &m : !fst res =>  snd res] -
           Pr [ Game1'(R1, R2, S, A1).main() @ &m :  fst res => !snd res]).
  proof strict.
  intros=> R1_ll R2_ll S_ll A1_gen_ll A1_dist_ll.
  rewrite (Game1_pr R1 R2 S A1 &m).
  cut ->: Pr [ Game1'(R1, R2, S, A1).main() @ &m : !fst res => snd res] =
           Pr [ Game1'(R1, R2, S, A1).main() @ &m : fst res] +
           Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res] -
           Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res /\ fst res].
    cut ->: Pr[Game1'(R1, R2, S, A1).main() @ &m : !fst res => snd res] =
             Pr[Game1'(R1, R2, S, A1).main() @ &m : snd res \/ fst res]
      by (rewrite Pr mu_eq; smt).
    by rewrite Pr mu_or; smt.
  cut ->: Pr [ Game1'(R1, R2, S, A1).main() @ &m : fst res => !snd res] =
           Pr [ Game1'(R1, R2, S, A1).main() @ &m : !fst res] + 1%r -
           Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res] -
           Pr [ Game1'(R1, R2, S, A1).main() @ &m : !snd res /\ !fst res].
    cut ->: Pr[Game1'(R1, R2, S, A1).main() @ &m : fst res => !snd res] =
             Pr[Game1'(R1, R2, S, A1).main() @ &m : !snd res \/ !fst res]
      by (rewrite Pr mu_eq; smt).
    rewrite Pr mu_or; rewrite Pr mu_not.
    by rewrite (Game1_true_pr R1 R2 S A1 &m) //; smt.
  rewrite (Game1_Nreal_pr R1 R2 S A1 &m) // (Game1_real_pr R1 R2 S A1 &m) //.
  cut H1: forall (x y:real), x - y = x + [-] y by smt.
  cut H2: forall (x y:real), [-] (x + y) = [-] x + [-] y by smt.
  cut H3: forall (x:real), [-] ([-] x) = x by smt.
  cut H4: forall (x:real), 2%r * x = x + x.
    by intros x; cut ->:2%r = (1%r + 1%r) by smt;
       rewrite Mul_distr_r.
  cut H4': forall (x y:real), 2%r * (x-y) = 2%r * x - 2%r * y by smt.
  cut H5: forall (x:real), 2%r * -x = - 2%r * x by smt.
  rewrite ?H1 ?H2 ?H3.
  cut ->: (1%r / 2%r + Pr[Game1'(R1,R2,S,A1).main() @ &m : snd res] +
           -Pr[Game1'(R1,R2,S,A1).main() @ &m : snd res /\ fst res] +
           (-(1%r / 2%r) + -1%r + Pr[Game1'(R1,R2,S,A1).main() @ &m : snd res] +
           Pr[Game1'(R1,R2,S,A1).main() @ &m : !snd res /\ !fst res])) =
            ((Pr[Game1'(R1,R2,S,A1).main() @ &m : snd res] +
              Pr[Game1'(R1,R2,S,A1).main() @ &m : snd res]) - 1%r) -
             (Pr[Game1'(R1,R2,S,A1).main() @ &m : snd res /\ fst res] -
              Pr[Game1'(R1,R2,S,A1).main() @ &m : !snd res /\ !fst res]) by smt.
  rewrite -H4 H4'.
  cut ADV1_xpand': 2%r * Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res] - 1%r =
                    2%r * (Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res /\ fst res] -
                           Pr [ Game1'(R1, R2, S, A1).main() @ &m : !snd res /\ !fst res]).
    cut Hpr1: Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res] =
               Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res /\ fst res] +
               Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res /\ !fst res].
      cut ->: Pr[Game1'(R1, R2, S, A1).main() @ &m : snd res] =
               Pr[Game1'(R1, R2, S, A1).main() @ &m: snd res /\ fst res \/ snd res /\ !fst res].
        by (rewrite Pr mu_eq; smt).
      by rewrite Pr mu_disjoint; smt.
    cut Hpr2: Pr [ Game1'(R1, R2, S, A1).main() @ &m : snd res /\ !fst res] =
               Pr [ Game1'(R1, R2, S, A1).main() @ &m : !fst res] -
               Pr [ Game1'(R1, R2, S, A1).main() @ &m : !snd res /\ !fst res].
      cut ->: Pr[Game1'(R1, R2, S, A1).main() @ &m : !fst res] =
               Pr[Game1'(R1, R2, S, A1).main() @ &m : snd res /\ !fst res \/ !snd res /\ !fst res].
        by (rewrite Pr mu_eq; smt).
      by rewrite Pr mu_disjoint; smt.
    by rewrite Hpr1 Hpr2 (Game1_Nreal_pr R1 R2 S A1 &m) //; smt.
  by rewrite -ADV1_xpand' //; smt.
  qed.

  equiv Game2_equiv (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A2 <: Adv2_t{R1,R2,S}):
    Game2(R1, R2, S, A2).main ~ Game2'(R1, R2, S, A2).main: 
      ={glob R1, glob R2, glob S, glob A2} ==>
      res{1} = snd res{2}.
  proof strict.
  fun; seq 2 2: (={i1, i2, real, glob R1, glob R2, glob A2, glob S});
    first by rnd; call (_: true).
  if=> //; first by rnd.
  if=> //.
    by do !(call (_: true); wp);
       call (_: ={glob R1, i1info} ==> ={res});
         first by fun true.
    by do !(call (_: true); wp).
  qed.

  lemma Game2_pr (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A2 <: Adv2_t{R1,R2,S}) &m:
    Pr [Game2(R1, R2, S, A2).main() @ &m : res] =
     Pr [Game2'(R1, R2, S, A2).main() @ &m : snd res].
  proof strict.
  by equiv_deno (Game2_equiv R1 R2 S A2).
  qed.

  lemma Game2_lossless (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A2 <: Adv2_t):
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim2 =>
    islossless A2.gen_query =>
    islossless A2.dist =>
    islossless Game2'(R1, R2, S, A2).main.
  proof strict.
  intros=> R1_ll R2_ll S_ll A2_gen_ll A2_dist_ll; fun.
  seq 2: true=> //;
    first by rnd; call A2_gen_ll; skip; smt.
  if; first by rnd; skip; smt.
  call A2_dist_ll; if.
    by wp; call R2_ll; call R1_ll.
    by call S_ll; wp.
  qed.

  lemma Game2_true_pr (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A2 <: Adv2_t) &m:
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim2 =>
    islossless A2.gen_query =>
    islossless A2.dist =>
    Pr [Game2'(R1, R2, S, A2).main() @ &m : true] = 1%r.
  proof strict.
  by intros=> R1_ll R2_ll S_ll A2_gen_ll A2_dist_ll;
     bdhoare_deno (Game2_lossless R1 R2 S A2 _ _ _ _ _).
  qed.

  lemma Game2_real_pr (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A2 <: Adv2_t) &m:
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim2 =>
    islossless A2.gen_query =>
    islossless A2.dist =>
    Pr [Game2'(R1, R2, S, A2).main() @ &m : fst res] = (1%r/2%r).
  proof strict.
  intros=> R1_ll R2_ll S_ll A2_gen_ll A2_dist_ll.
  bdhoare_deno (_: true ==> fst res)=> //; fun.
  seq 1: true 1%r (1%r/2%r) 0%r _=> //;
    first by call A2_gen_ll.
  seq 1: real (1%r/2%r) 1%r _ 0%r=> //.
    by rnd; skip; progress; rewrite Dbool.mu_def.
    if; first by rnd; skip; smt.
    call A2_dist_ll; if.
      by wp; call R2_ll; call R1_ll.
      by call S_ll; wp.
    by hoare; conseq* (_: _ ==> true).
  qed.

  lemma Game2_Nreal_pr (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A2 <: Adv2_t) &m:
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim2 =>
    islossless A2.gen_query =>
    islossless A2.dist =>
    Pr [Game2'(R1, R2, S, A2).main() @ &m : !(fst res)] = 1%r/2%r.
  proof strict.
  intros=> R1_ll R2_ll S_ll A2_gen_ll A2_dist_ll.
  rewrite Pr mu_not.
  rewrite (Game2_true_pr R1 R2 S A2 &m) //.
  rewrite (Game2_real_pr R1 R2 S A2 &m) //.
  cut H: 1%r = 2%r / 2%r by smt.
  by rewrite {1}H; smt.
  qed.

  lemma ADV2_xpnd (R1 <: Rand1_t) (R2 <: Rand2_t) (S <: Sim_t) (A2 <: Adv2_t{R1,R2,S}) &m:
    islossless R1.gen =>
    islossless R2.gen =>
    islossless S.sim2 =>
    islossless A2.gen_query =>
    islossless A2.dist => 
    2%r * Pr [ Game2(R1, R2, S, A2).main() @ &m : res] - 1%r =
     2%r * (Pr [ Game2'(R1, R2, S, A2).main() @ &m : !fst res => snd res] -
            Pr [ Game2'(R1, R2, S, A2).main() @ &m : fst res => !snd res]).
  proof strict.
  intros=> R1_ll R2_ll S_ll A2_gen_ll A2_dist_ll.
  rewrite (Game2_pr R1 R2 S A2 &m).
  cut ->: Pr [ Game2'(R1, R2, S, A2).main() @ &m : !fst res => snd res] =
           Pr [ Game2'(R1, R2, S, A2).main() @ &m : fst res] +
           Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res] -
           Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res /\ fst res].
    cut ->: Pr[Game2'(R1, R2, S, A2).main() @ &m : !fst res => snd res] =
             Pr[Game2'(R1, R2, S, A2).main() @ &m : snd res \/ fst res]
      by (rewrite Pr mu_eq; smt).
    by rewrite Pr mu_or; smt.
  cut ->: Pr [ Game2'(R1, R2, S, A2).main() @ &m : fst res => !snd res] =
           Pr [ Game2'(R1, R2, S, A2).main() @ &m : !fst res] + 1%r -
           Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res] -
           Pr [ Game2'(R1, R2, S, A2).main() @ &m : !snd res /\ !fst res].
    cut ->: Pr[Game2'(R1, R2, S, A2).main() @ &m : fst res => !snd res] =
             Pr[Game2'(R1, R2, S, A2).main() @ &m : !snd res \/ !fst res]
      by (rewrite Pr mu_eq; smt).
    rewrite Pr mu_or; rewrite Pr mu_not.
    by rewrite (Game2_true_pr R1 R2 S A2 &m) //; smt.
  rewrite (Game2_Nreal_pr R1 R2 S A2 &m) // (Game2_real_pr R1 R2 S A2 &m) //.
  cut H1: forall (x y:real), x - y = x + [-] y by smt.
  cut H2: forall (x y:real), [-] (x + y) = [-] x + [-] y by smt.
  cut H3: forall (x:real), [-] ([-] x) = x by smt.
  cut H4: forall (x:real), 2%r * x = x + x
    by (by intros x; cut ->:2%r = (1%r + 1%r) by smt; rewrite Mul_distr_r).
  cut H4': forall (x y:real), 2%r * (x-y) = 2%r * x - 2%r * y by smt.
  cut H5: forall (x:real), 2%r * -x = - 2%r * x by smt.
  rewrite ?H1 ?H2 ?H3.
  cut ->: (1%r / 2%r + Pr[Game2'(R1,R2,S,A2).main() @ &m : snd res] +
           -Pr[Game2'(R1,R2,S,A2).main() @ &m : snd res /\ fst res] +
           (-(1%r / 2%r) + -1%r + Pr[Game2'(R1,R2,S,A2).main() @ &m : snd res] +
           Pr[Game2'(R1,R2,S,A2).main() @ &m : !snd res /\ !fst res])) =
           ((Pr[Game2'(R1,R2,S,A2).main() @ &m : snd res] +
             Pr[Game2'(R1,R2,S,A2).main() @ &m : snd res]) - 1%r) -
            (Pr[Game2'(R1,R2,S,A2).main() @ &m : snd res /\ fst res] -
             Pr[Game2'(R1,R2,S,A2).main() @ &m : !snd res /\ !fst res]) by smt.
  rewrite -H4 H4'.
  cut ADV2_xpand': 2%r * Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res] - 1%r =
                    2%r * ( Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res /\ fst res] -
                    Pr [ Game2'(R1, R2, S, A2).main() @ &m : !snd res /\ !fst res]).
    cut Hpr1: Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res] =
               Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res /\ fst res] +
               Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res /\ !fst res].
      cut ->: Pr[Game2'(R1, R2, S, A2).main() @ &m : snd res] =
               Pr[Game2'(R1, R2, S, A2).main() @ &m : snd res /\ fst res \/ snd res /\ !fst res]
        by (rewrite Pr mu_eq; smt).
      by rewrite Pr mu_disjoint; smt.
    cut Hpr2: Pr [ Game2'(R1, R2, S, A2).main() @ &m : snd res /\ !fst res] =
               Pr [ Game2'(R1, R2, S, A2).main() @ &m : !fst res] -
               Pr [ Game2'(R1, R2, S, A2).main() @ &m : !snd res /\ !fst res].
      cut ->: Pr[Game2'(R1, R2, S, A2).main() @ &m : !fst res] =
               Pr[Game2'(R1, R2, S, A2).main() @ &m : snd res /\ !fst res \/ !snd res /\ !fst res]
        by (rewrite Pr mu_eq; smt).
      by rewrite Pr mu_disjoint; smt.
    by rewrite Hpr1 Hpr2 (Game2_Nreal_pr R1 R2 S A2 &m) //; smt.
  by rewrite -ADV2_xpand' //; smt.
  qed.
end ProtSecurity.
