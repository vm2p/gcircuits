require import Bool.
require import Int.
require import Pair.
require import Real.
require import Distr.

require import Array.

require import Prime_field.
require import Cyclic_group_prime.

(** Decisional Diffie-Hellman problem *)
theory DDH.
  module type Adv_t = {
    fun solve(gx:group, gy:group, gz:group): bool
  }.

  module Game (A:Adv_t) = {
    fun main(): bool = {
      var x,y,z:gf_q;
      var b, b':bool;
      var gxy:group;

      x = $Dgf_q.dgf_q;
      y = $Dgf_q.dgf_q;
      z = $Dgf_q.dgf_q;
      b = ${0,1};
      gxy = if b then g^(x*y) else g^z;
      b' = A.solve(g ^ x, g ^ y, gxy);
      return (b = b');
    }
  }.
end DDH.

(** List variation of the Decisional Diffie-Hellman problem

   Inputs: g^a and a list (array) of pairs (g^b1, g^c1), ..., (g^bn, g^cn)
   Output: a boolean b, trying to guess if ci = a * bi

*)
theory DDHn.

(*  import Group.*)

  module type Adv_t = {
    fun choose_n(): int
    fun solve(gx:group, gyzs:(group*group) array): bool
  }.

  module Game (B:Adv_t) = {
    fun main(): bool = {
      var x:gf_q;
      var y,z:gf_q array;
      var gyzs:(group*group) array;
      var b, guess:bool;
      var n:int;

      b = ${0,1};

      n = B.choose_n();
      n = if n<0 then 0 else n;

      x = $Dgf_q.dgf_q;
      y = $Darray.darray n Dgf_q.dgf_q;
      z = $Darray.darray n Dgf_q.dgf_q;

      if (b)
       gyzs = init n (lambda k, (g^y.[k], g^(x*y.[k])));
      else
       gyzs = init n (lambda k, (g^y.[k], g^(z.[k])));
 
      guess = B.solve(g^x, gyzs);

      return guess = b;
    }
  }.

  module Game'(B:Adv_t) = {
    fun main(): bool*bool = {
      var x:gf_q;
      var y,z:gf_q array;
      var gyzs:(group*group) array;
      var b, guess:bool;
      var n:int;

      b = ${0,1};

      n = B.choose_n();
      n = if n<0 then 0 else n;

      x = $Dgf_q.dgf_q;
      y = $Darray.darray n Dgf_q.dgf_q;
      z = $Darray.darray n Dgf_q.dgf_q;

      if (b)
       gyzs = init n (lambda k, (g^y.[k], g^(x*y.[k])));
      else
       gyzs = init n (lambda k, (g^y.[k], g^(z.[k])));
 
      guess = B.solve(g^x, gyzs);

      return (b, guess = b);
    }
  }.

lemma Game_equiv (A <: Adv_t):
 equiv [ Game(A).main ~ Game'(A).main: 
         ={glob A} ==> res{1} = snd res{2} ].
proof strict.
fun.
seq 6 6: (={n, b, x, y, z, glob A}).
 rnd; rnd; rnd; wp.
 call (_: ={glob A} ==> ={res, glob A}); first by fun true.
 by rnd;skip; progress.
case (b{1}).
 rcondt{1} 1; first by intros &m; skip.
 rcondt{2} 1; first by intros &m; skip.
 call (_: ={glob A, gx, gyzs} ==> ={res}); first by fun true.
 by wp; skip.
rcondf{1} 1; first by intros &m; skip.
rcondf{2} 1; first by intros &m; skip.
call (_: ={glob A, gx, gyzs} ==> ={res}); first by fun true.
by wp; skip.
qed.

lemma Game_pr (A <: Adv_t) &m:
 Pr [Game(A).main() @ &m : res] 
 =  Pr [Game'(A).main() @ &m : snd res].
proof strict.
by equiv_deno (Game_equiv A).
qed.

lemma Game_lossless (A <: Adv_t):
 islossless A.choose_n =>
 islossless A.solve =>
 islossless Game'(A).main.
proof strict.
intros=> A_choose_ll A_solve_ll; fun.
seq 1: true=> //;
 first by rnd; skip; smt.
case (b).
 rcondt 6=> //.
  rnd; rnd; rnd; wp.
  call (_:true ==> true); first by fun true.
  skip; smt.
 call (_:true ==> true); first by fun true.
 wp; rnd; rnd; rnd; wp.
 call (_:true ==> true); first by fun true.
 skip; progress; smt.
rcondf 6=> //.
 rnd; rnd; rnd; wp.
 call (_:true ==> true); first by fun true.
 skip; smt.
call (_:true ==> true); first by fun true.
wp; rnd; rnd; rnd; wp.
call (_:true ==> true); first by fun true.
skip; progress; smt.
qed.

lemma Game_true_pr (A <: Adv_t) &m:
 islossless A.choose_n =>
 islossless A.solve =>
 Pr [Game'(A).main() @ &m : true] = 1%r.
proof strict.
by intros=> A_choose_ll A_solve_ll;
bdhoare_deno (Game_lossless A _ _).
qed.

lemma Game_real_pr (A <: Adv_t) &m:
 islossless A.choose_n =>
 islossless A.solve =>
 Pr [Game'(A).main() @ &m : fst res] = (1%r/2%r).
proof strict.
intros A_choose_ll A1_solve_ll;
bdhoare_deno (_: true ==> fst res)=> //; fun.
seq 1: b (1%r/2%r) 1%r _ 0%r=> //.
  by rnd; skip; progress; rewrite Dbool.mu_def /charfun /=.
 rcondt 6.
  rnd; rnd; rnd; wp.
  call (_:true ==> true); first by fun true; smt.
  skip; smt.
 call (_:true==>true); first by fun true.
 wp; rnd; rnd; rnd; wp; call (_:true ==> true); first by fun true.
 skip; progress; smt.
hoare. 
rcondf 6.
 rnd; rnd; rnd; wp.
 call (_:true ==> true); first by fun true; smt.
 skip; smt.
call (_:true==>true); first by fun true.
wp; rnd; rnd; rnd; wp; call (_:true ==> true); first by fun true.
by skip; progress; trivial.
save.

lemma Game_Nreal_pr (A <: Adv_t) &m:
 islossless A.choose_n =>
 islossless A.solve =>
 Pr [Game'(A).main() @ &m : !fst res] = 1%r/2%r.
proof strict.
intros A_choose_ll A1_solve_ll.
rewrite Pr mu_not.
rewrite (Game_true_pr A &m) //.
rewrite (Game_real_pr A &m) //.
cut H: 1%r = 2%r / 2%r by smt.
by rewrite {1}H; smt.
qed.

 lemma Adv_xpnd (A<: Adv_t) &m:
  islossless A.choose_n =>
  islossless A.solve =>
  2%r * Pr[Game(A).main()@ &m:res] - 1%r 
  = 2%r * ( Pr[ Game'(A).main() @ &m : !fst res => snd res]
           - Pr[ Game'(A).main() @ &m : fst res => !snd res] ).
proof strict.
intros=> A_choose_ll A_solve_ll.
rewrite (Game_pr A &m).
cut ->: ( Pr [ Game'(A).main() @ &m : !fst res => snd res]
          = Pr [ Game'(A).main() @ &m : fst res]
            + Pr [ Game'(A).main() @ &m : snd res]
            - Pr [ Game'(A).main() @ &m : snd res && fst res]).
 cut ->: (Pr[Game'(A).main() @ &m : !fst res => snd res]
          = Pr[Game'(A).main() @ &m : snd res \/ fst res]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; smt.
cut ->:  Pr [ Game'(A).main() @ &m : fst res => !snd res]
  = Pr [ Game'(A).main() @ &m : !fst res] + 1%r
    - Pr [ Game'(A).main() @ &m : snd res]
    - Pr [ Game'(A).main() @ &m : !snd res && !fst res].
 cut ->: (Pr[Game'(A).main() @ &m : fst res => !snd res]
         = Pr[Game'(A).main() @ &m : !snd res \/ !fst res]).
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_or; rewrite Pr mu_not.
 rewrite (Game_true_pr A &m) //; smt.
rewrite (Game_Nreal_pr A &m) // (Game_real_pr A &m) //.
cut H1: forall (x y:real), x - y = x + [-] y by smt.
cut H2: forall (x y:real), [-] (x + y) = [-] x + [-] y by smt.
cut H3: forall (x:real), [-] ([-] x) = x by smt.
cut H4: forall (x:real), 2%r * x = x + x.
 intros x; cut ->:2%r = (1%r + 1%r) by smt.
 by rewrite Mul_distr_r.
cut H4': forall (x y:real), 2%r * (x-y) = 2%r * x - 2%r * y by smt.
cut H5: forall (x:real), 2%r * -x = - 2%r * x by smt.
rewrite ?H1 ?H2 ?H3.
cut ->: (1%r / 2%r + Pr[Game'(A).main() @ &m : snd res] +
 -Pr[Game'(A).main() @ &m : snd res && fst res] +
 (-(1%r / 2%r) + -1%r + Pr[Game'(A).main() @ &m : snd res] +
  Pr[Game'(A).main() @ &m : !snd res && ! fst res]))
 = ( (Pr[Game'(A).main() @ &m : snd res] + Pr[Game'(A).main() @ &m : snd res]) - 1%r) - (Pr[Game'(A).main() @ &m : snd res && fst res] -
      Pr[Game'(A).main() @ &m : !snd res && ! fst res]) by smt.
rewrite -H4 H4'.
cut ADV1_xpand' : 2%r * Pr [ Game'(A).main() @ &m : snd res] - 1%r
  = 2%r * ( Pr [ Game'(A).main() @ &m : snd res && fst res]
        - Pr [ Game'(A).main() @ &m : !snd res && !fst res] ).
 cut Hpr1: Pr [ Game'(A).main() @ &m : snd res]
            = Pr [ Game'(A).main() @ &m : snd res && fst res]
             + Pr [ Game'(A).main() @ &m : snd res && !fst res].
  cut ->: Pr[Game'(A).main() @ &m : snd res]
           = Pr[Game'(A).main() @ &m
               : snd res && fst res \/ snd res && !fst res].
   rewrite Pr mu_eq; smt.
  rewrite Pr mu_disjoint; smt.
 cut Hpr2 : Pr [ Game'(A).main() @ &m : snd res && !fst res]
            = Pr [ Game'(A).main() @ &m : !fst res]
            - Pr [ Game'(A).main() @ &m : !snd res && !fst res].
  cut ->: Pr[Game'(A).main() @ &m : !fst res]
           = Pr[Game'(A).main() @ &m 
               : snd res && !fst res \/ !snd res && !fst res].
   rewrite Pr mu_eq; smt.
  rewrite Pr mu_disjoint; smt.
 rewrite Hpr1 Hpr2 (Game_Nreal_pr A &m) //.
 smt.
rewrite -ADV1_xpand' //.
smt.
save.

 (* reduction: epsilon_DDHn = epsilon_DDH (due to the randomness self-reducibility of DDH) *)
(*
 op epsilon : real.

 axiom ArrayDDH:
  forall (A <: Adv_t) &m,
    `|2%r * Pr[Game(A).main()@ &m:res] - 1%r| <= epsilon.
*)
end DDHn.
