require import Bool.
require import Int.
require import Real.
require import Distr.
require import Pair.
require import FMap. import OptionGet.
require import Array.
require import Distr.

import Core.
import ForLoop.
require Monoid.
import Monoid.Mrplus.
require FSet.
import FSet.Interval.

(* Some nice axioms to discharge sometime in the future *) 

axiom sum_mul_distr: forall r (f:'a->real) s,
 r * sum f s = sum (lambda k, r*(f k)) s.

lemma sum_ij_mul_distr: forall r (f:int->real) i j,
 r * sum_ij i j f = sum_ij i j (lambda k, r*(f k)).
proof. by intros r f i j; rewrite /sum_ij sum_mul_distr. qed.

lemma nosmt neg_def: forall b, (!b) = (b => false).
proof. by intros b; case b. qed.
lemma nosmt rw_impl_def: forall a b, (a=>b) = (!a \/ b) by [].
lemma nosmt rw_neg_impl: forall a b, (!(a=>b)) = (a /\ !b) by [].

require import Sch.
require import DKCSch.
require import SchAuthSec.

require import GarbleTools.

theory DKCScheme2.

  require ExtWord.
  clone import ExtWord as W.

  op m_bnd : int.
  axiom m_bnd_pos : m_bnd > 0.

  clone import Tweak with theory W = W.

clone DKCScheme with
  theory W = W.

import W.
import Tweak.
import DKCScheme.Local.
print op DKCScheme.C2.decode.

(** Scheme Garble2 definition.
    ===================== **)

clone import Scheme as C22 with
  theory Input = DKCScheme.C2.Input,
  type fun_t = DKCScheme.C2.fun_t,
  type funG_t = DKCScheme.C2.funG_t,
  type output_t = DKCScheme.C2.output_t,
  type outputK_t = (word*word) array,
  type outputG_t = DKCScheme.C2.outputG_t,
  type leak_t = DKCScheme.C2.leak_t,
  type rand_t = DKCScheme.C2.rand_t,
  op validInputs fn x = 
    (DKCScheme.C2.validInputs fn x) /\ (let (n,m,q,aa,bb) = (fst fn) in m) <= m_bnd,
  pred validRand = DKCScheme.C2.validRand,
  op phi = DKCScheme.C2.phi, 
  op eval = DKCScheme.C2.eval,
  op funG = DKCScheme.C2.funG, 
  op inputK = DKCScheme.C2.inputK,
  op outputK (fn:fun_t) (r:rand_t) =
      let (n, m, q, aa, bb) = fst fn in
      init m (lambda i, (proj r.[(n+q-m+i,false)], proj r.[(n+q-m+i,true)])),
  op valid_outG (oK:outputK_t) (oG:outputG_t) =
    alli (lambda i x, x = fst oK.[i] \/ x = snd oK.[i]) oG,
  op decode (oK:outputK_t) (oG:outputG_t) =
    mapi (lambda i x, x=snd oK.[i]) oG,
  op evalG = DKCScheme.C2.evalG.

  clone import SchAuthSec with
     theory Scheme = C22.

(** Randomness generation
    =====================

  We use array distributions instead of the loops used in DKCSch. Operation 
  [buildRnd] reconstructs the FMap used in DKCSch.
*)
import ForLoop.

(* These should be moved into ExtWord *)
type sword.
op mk_tk : sword -> bool -> word.
op dsword : sword distr.

op foreach (f: int -> 'x -> 'st -> 'st) (z: 'st) (a: 'x array) : 'st =
  range 0 (length a) z (lambda i x, f i a.[i] x).

(* Useful lemmas for dealing with randomness *)
op buildRnd (a:(bool*(sword*sword)) array) : rand_t =
  foreach (lambda i x r, r.[(i,true)<-mk_tk (fst (snd a.[i]))
                                              (fst a.[i])]
                          .[(i,false)<-mk_tk (snd (snd a.[i]))
                                              (!(fst a.[i]))])
          FMap.Core.empty a. 

lemma buildRnd_get: forall a i b,
  get (buildRnd a) (i,b)
  = if 0 <= i < length a
    then Some (if b then mk_tk (fst (snd a.[i]))
                                 (fst a.[i])
                    else mk_tk (snd (snd a.[i]))
                                 (!(fst a.[i])))
    else None.
proof.
intros a i b; rewrite /buildRnd /foreach /=.
generalize i; pose l := length a; elim/Induction.induction l; last by smt.
 rewrite range_base //; smt.
intros i Hi IH k.
rewrite range_ind_lazy /=; first by smt.
cut ->: i+1-1 = i by smt.
case (k=i) => Hki /=.
(* rewrite /FMap.OptionGet."_.[_]" /=.*)
 cut ->: (0 <= k < i+1) = true by smt.
 case b => Hb /=.
  rewrite get_setN; first by smt. 
  by rewrite Hki get_setE.
 by rewrite Hki get_setE.
rewrite (* /FMap.OptionGet."_.[_]" /=*) get_setN; first by smt. 
rewrite get_setN; first by smt. 
rewrite IH; smt.
qed.

import Dprod.

(* The reference randomness generation algorithm *)

  module Rand : Rand_t = {
    fun gen(l:topo_t): rand_t = {
      var r : (bool*(sword*sword)) array;
      var n, m, q:int;
      var aa, bb: int array;

      (n, m, q, aa, bb) = l;
      r = $Darray.darray (n+q) (Dbool.dbool * (dsword*dsword));

      return (buildRnd r);
    }
  }.

lemma tokensamp_ll : forall i,
(mu ((Darray.darray i ({0,1} * (dsword * dsword))%Dprod))%Darray
  (lambda (x : (bool * (sword * sword)) array), true) =
1%r).
proof.
admit. (* Summation of probs in all support is 1 *)
save.


(** Auxiliary functions: 
    ===================
*)

(* compute the positions of valid forgeries *)
op chkForge (out:bool array) (oK:outputK_t) (oG:outputG_t): bool array =
 range 0 (length out) (init m_bnd (lambda i, false))
       (lambda i f, f.[i <- oG.[i] = if out.[i]
                                     then fst oK.[i]
                                     else snd oK.[i]])%Array. 

op or_array (x: bool array) : bool =
 Monoid.Mbor.sum (lambda i, x.[i]) (interval 0 (m_bnd-1)).

(** SECURITY PROOF
    ==============

    Proof strategy:
      1) relate the original Authenticity game with a slightly weakened
        form (game G1) which doesn't check the validity of "all" garbled
        output positions (i.e. succeeds if there exist positions with
        valid forgeries).
      2) guess the position/bit-value of the forgery (game G2) --- the
        adversary only wins when it is able to produce a valid forgery in
        that position/bit;
      3) change the garbling of the given function so that the truth table
        for the (guessed) output gate is the constant function (game F).
        The jump is reduced to the "aPriv" computational assumption.
      4) in Fake game, all entries of the forged output gate are equal
        to the bit adversary knows. Adversary advantage is then 
        "2^-tokLen" (information-theoretically).
      5) combine intermediate results to establish the bound
                  Adv(AUT) <= 1/(2*m) * ( Adv(aPriv) + 2^-tokLen )
*)

(*
   Game 1 - Easier authenticity:  same as Game_Auth, but the adversary
     succeeds as soon as some positions of the forged oG has
     valid forgeries 
*)
  module G1(R: Rand_t, A : Adv_Auth_t) = {
    
    fun main() : bool = {
      var fn : fun_t;
      var x : Input.input_t;
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var forgeChk : bool array;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      forgeChk = init m_bnd (lambda i, false);
      if (validInputs fn x)
        forgeChk = chkForge (eval fn x) oK oG;

      return or_array forgeChk;
    }
  }.

(* The probability of A winning this game cannot be smaller *)

lemma G1_Auth_equiv:
  forall (R<: Rand_t{Game_Auth, G1}) (A<: Adv_Auth_t{Game_Auth, G1,R}),
  equiv [ Game_Auth(R,A).main ~ G1(R,A).main : true  ==> res{1} => res{2}].
proof.
intros R A; fun.
wp; call (_: ={glob A, X} ==> ={res}); first by fun true.
wp; call (_: ={glob A, fG} ==> ={res, glob A}); first by fun true.
wp; call (_: ={l} ==> ={res}); first by fun true.
wp; call (_: true ==> ={res, glob A}); first by fun true.
skip; progress.
 admit (* validOG => forgeCond => or_array chkForge... *).
smt.
qed.

(** Game 2 - guess position/bit of forgery: G2 is like G1 but adversary
    must win on guessed values
*)

  module G2(R: Rand_t, A : Adv_Auth_t) = {
    (* guesses *)
    var pguess: int
    var bguess: bool
    (* positions where A forges the result *)
    var forgeChk: bool array
    var out: bool array
    
    fun game(pguess: int) : bool = {
      var fn : fun_t;
      var x : Input.input_t;
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      out = eval fn x;
      forgeChk = init m_bnd (lambda i, false);
      if (validInputs fn x)
        forgeChk = chkForge out oK oG;

      return forgeChk.[pguess];
    }
    fun main() : bool = {
      var r: bool;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      r = game(pguess);
      return r /\ out.[pguess] = bguess;
    }
  }.

(* The event corresponding to success in G1 is also present in G2 *)

lemma G1G2_equiv: forall (R<: Rand_t{G1, G2}) (A<: Adv_Auth_t{G1, G2,R}),
   equiv [ G1(R,A).main ~ G2(R,A).main : 
           true  ==> res{1} = (or_array G2.forgeChk){2} ].
proof.
intros R A.
fun; inline G2(R,A).game.
wp; call (_: ={glob A, X} ==> ={res}); first by fun true.
wp; call (_: ={glob A, fG} ==> ={res, glob A}); first by fun true.
wp; call (_: ={l} ==> ={res}); first by fun true.
wp; call (_: true ==> ={res, glob A}); first by fun true.
wp; rnd{2}; rnd{2}; skip; progress; smt.
qed.

(* Splitting the probability of success in G1 (as seen in G2) among flips 
   in the different output bits *)

lemma PrG2_xpnd1 &m: 
forall (R<: Rand_t{G2}) (A<: Adv_Auth_t{G2,R}) n,
  n = m_bnd-1 =>
  Pr[G2(R, A).main() @ &m : or_array G2.forgeChk]
  <= sum_ij 0 n (lambda k, Pr[G2(R, A).main() @ &m : G2.forgeChk.[k] ]).
proof.
intros R A n Hn.
cut ->: Pr[G2(R, A).main() @ &m : or_array G2.forgeChk]
  = Pr[G2(R, A).main() @ &m : Monoid.Mbor.sum (lambda i, G2.forgeChk.[i]) (interval 0 n)].
 rewrite Pr mu_eq => // &1.
 by rewrite /or_array -rw_eq_iff Hn; congr.
elim/Induction.induction n.
  apply eq_le.
  rewrite sum_ij_eq /=.
  rewrite Pr mu_eq; trivial => &1.
  rewrite interval_single Monoid.Mbor.sum_add; smt.
 intros k Hk IH.
 apply (Trans _ (Pr[G2(R, A).main() @ &m :
                     Monoid.Mbor.sum (lambda i, G2.forgeChk.[i])
                                     (interval 0 k) ]
                 + Pr[G2(R, A).main() @ &m : G2.forgeChk.[k+1]])).
  admit (* or_ij_split *).
 rewrite (sum_ij_split (k+1)); first smt.
 cut ->: k+1-1 = k by smt.
 rewrite sum_ij_eq /=.
 apply addleM; first by apply IH.
 by apply eq_le.
smt.
qed.

(* Translation between notations *)

lemma G2game_trf &m k i p:
  forall (A<: Adv_Auth_t{G2}) (R<: Rand_t{G2,A}),
    p = Pr[G2(R, A).game(i) @ &m : G2.forgeChk.[k]] =>
    bd_hoare [ G2(R,A).game : pguess=i ==> G2.forgeChk.[k]] = p.
proof.
intros A R H.
bypr; progress.
rewrite H.
equiv_deno (_: ={pguess} ==> ={G2.forgeChk}) => //.
fun.
wp; call (_: ={glob A, X} ==> ={res}); first by fun true.
wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by fun true.
wp; call (_: ={l} ==> ={res, glob R}); first by fun true.
(* ec-bug: adding (glob A) to pre as post conditions does not work *)
call (_: true ==> ={res, glob A}); first by fun true.
skip; progress; smt. 
qed.

(* Conditional probability lemma *)

lemma PrG2_eq &m i: 
  forall (R<: Rand_t{G2}) (A<: Adv_Auth_t{G2,R}),
   0 <= i < m_bnd =>
   Pr[G2(R,A).main() @ &m : G2.forgeChk.[i] /\ G2.pguess = i
                            /\ G2.out.[i] = G2.bguess ]
   = Pr[G2(R, A).main() @ &m : G2.forgeChk.[i]] / (2*m_bnd)%r.
proof.
intros R A Hi.
pose p:= Pr[G2(R, A).main() @ &m : G2.forgeChk.[i]].
rewrite -/p.
bdhoare_deno (_ : true ==> _) => //.
fun; wp. swap 2 1.
seq 1: (G2.pguess=i) (1%r/m_bnd%r) (p/2%r) (1%r/m_bnd%r) 0%r; trivial.
 rnd; skip; progress.
 cut ->:(lambda x, x = i) = ((=) i).
  apply fun_ext => x /=; smt.
 by change (mu_x [0..m_bnd - 1] i = 1%r / m_bnd%r); smt.
 seq 1 : (G2.pguess = i /\ G2.forgeChk.[i]) p (1%r/2%r) (1%r-p) 0%r.
  inline G2(R,A).game.
  do (wp; call (_:true)); wp; skip; smt.
  call (G2game_trf &m i i p A R _ ) => //.
  rewrite /p.
  equiv_deno (_: true ==> ={G2.forgeChk}).
    fun; inline G2(R,A).game.
    wp; call (_: ={glob A, X} ==> ={res}); first by fun true.
    wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by fun true.
    wp; call (_: ={l} ==> ={res, glob R}); first by fun true.
    (* ec-bug: adding (glob A) to pre as post conditions does not work *)
    call (_: true ==> ={res, glob A}); first by fun true.
    by wp; rnd{1}; rnd{1}; skip; progress; smt.
   by trivial.
  smt.
  rnd ((=) G2.out.[i]); skip; progress.
  by change (mu_x {0,1} G2.out{hr}.[G2.pguess{hr}] = 1%r / 2%r); smt.
  hoare.
  rnd; skip; progress; smt.
  by progress; trivial.
  hoare; inline G2(R,A).game; rnd.
  wp; call (_:true).
  wp; call (_:true).
  wp; call (_:true).
  wp; call (_:true).
  wp; skip; progress; smt.
  progress.
  cut ->: (m_bnd%r * 2%r) = (2 * m_bnd)%r by smt.
  cut ->: 0%r / m_bnd%r = 0%r by smt.
  smt.
qed.

(* Split the probability of success in game 2 by the different
   possible guessed positions *)

lemma PrG2_xpnd2 &m: 
 forall (R<: Rand_t{G2}) (A<: Adv_Auth_t{G2,R}),
    Pr[G2(R,A).main() @ &m : G2.forgeChk.[G2.pguess] /\ G2.out.[G2.pguess]=G2.bguess /\ 0 <= G2.pguess < m_bnd ] =
    sum_ij 0 (m_bnd-1) (lambda k, Pr [G2(R,A).main() @ &m : G2.forgeChk.[k] /\ G2.out.[k] = G2.bguess /\ G2.pguess = k ]).
proof.
intros R A.
pose n := m_bnd-1.
cut ->: Pr[G2(R, A).main() @ &m :
            G2.forgeChk.[G2.pguess] /\
            G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < m_bnd]
      = Pr[G2(R, A).main() @ &m :
            G2.forgeChk.[G2.pguess] /\
            G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < n+1].
 rewrite Pr mu_eq; smt.
elim/Induction.induction n.
  rewrite sum_ij_eq /=.
  rewrite Pr mu_eq; smt.
 intros k H1 IH.
 rewrite (sum_ij_split (k+1)); first by smt.
 rewrite sum_ij_eq /=.
 cut ->: Pr[G2(R, A).main() @ &m :
             G2.forgeChk.[G2.pguess] /\
             G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < k + 1 + 1]
  = Pr[G2(R, A).main() @ &m :
        G2.forgeChk.[G2.pguess] /\
             G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < k + 1
        \/ G2.forgeChk.[k+1] /\ G2.out.[k+1] = G2.bguess /\ G2.pguess = k+1 ].
  rewrite Pr mu_eq; progress; smt.
 rewrite Pr mu_or.
 rewrite IH.
 cut ->: k+1-1=k by smt.
 cut ->: Pr[G2(R, A).main() @ &m :
   (G2.forgeChk.[G2.pguess] /\
    G2.out.[G2.pguess] = G2.bguess /\ 0 <= G2.pguess < k + 1) /\
   G2.forgeChk.[k+1] /\
   G2.out.[k+1] = G2.bguess /\ G2.pguess = k + 1] =
   Pr[G2(R, A).main() @ &m : false].
  rewrite Pr mu_eq; smt.
 rewrite Pr mu_false.
smt.
smt. 
qed.

(* Rewriting the success event in G2 to match previous lemma  *)

lemma G2G2_equiv: forall (R<: Rand_t{G1, G2}) (A<: Adv_Auth_t{G1, G2,R}),
   equiv [ G2(R,A).main ~ G2(R,A).main : 
           true
           ==> 
           ={glob G2} /\ res{1} = (G2.forgeChk.[G2.pguess]
                                   /\ G2.out.[G2.pguess]=G2.bguess
                                   /\ 0 <= G2.pguess < m_bnd){2} ].
proof.
intros R A; fun; wp.
call (_: ={pguess, G2.pguess, G2.bguess} /\ (pguess = G2.pguess){1}
      ==> ={glob G2} /\ res{1}=(G2.forgeChk.[G2.pguess]){2}).
 fun.
 wp; call (_: ={glob A, X} ==> ={res}); first by fun true.
 wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by fun true.
 wp; call (_: ={l} ==> ={res, glob R}); first by fun true.
 wp; call (_: true ==> ={res, glob A}); first by fun true.
 skip; progress; smt.
rnd; rnd; skip; progress; smt.
qed.

  (** A version of G2 in which [chkForge] is replaced by a local check *)
  op fn_m (l:topo_t) : int =
   let (n, m, q, aa, bb) = l in m.

  module G2'(R: Rand_t, A : Adv_Auth_t) = {
      var fn : fun_t
      var x : Input.input_t
      var pguess: int
      var bguess: bool

    fun game(pguess: int, bguess: bool) : bool = {
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r: bool;

      fn = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG fn rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      r = false;
      if (validInputs fn x /\ pguess < fn_m (fst fn) /\ 
          (eval fn x).[pguess]=bguess)
        r = (oG.[pguess] = if bguess
                             then fst oK.[pguess]
                             else snd oK.[pguess]);

      return r;
    }
    fun main() : bool = {
      var r: bool;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      r = game(pguess, bguess);
      return r;
    }
  }.

  (** G2 and G2' are exactly the same *)

lemma G2G2_equiv': forall (R<: Rand_t{G1, G2,G2'}) (A<: Adv_Auth_t{G1, G2,G2',R}),
   equiv [ G2(R,A).main ~ G2'(R,A).main : true ==>  ={res} ].
proof.
intros R A; fun; wp.
call (_: ={pguess} /\ pguess{1}=G2.pguess{1} /\ bguess{2}=G2.bguess{1}
      ==> (res=G2.forgeChk.[G2.pguess]){1}
          /\ res{2}=(G2.forgeChk{1}.[G2.pguess{1}]
                     /\ G2.out{1}.[G2.pguess{1}] = G2.bguess{1})).
 fun.
 wp; call (_: ={glob A, X} ==> ={res}); first by fun true.
 wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by fun true.
 wp; call (_: ={l} ==> ={res, glob R}); first by fun true.
 wp; call (_: true ==> ={res, glob A}); first by fun true.
 skip; progress.
delta chkForge;beta.
cut -> : (
(range 0 (length (eval result_R result_R1))
   (init m_bnd (lambda (i : int), false))
   (lambda (i : int) (f : bool array),
      f.[i <-
        result_R2.[i] =
        if (eval result_R result_R1).[i] then
          fst (outputK result_R result_R0).[i]
        else snd (outputK result_R result_R0).[i]])).[G2.pguess{1}]
     = 
     (result_R2.[G2.pguess{1}] =
        if (eval result_R result_R1).[G2.pguess{1}] then
          fst (outputK result_R result_R0).[G2.pguess{1}]
        else snd (outputK result_R result_R0).[G2.pguess{1}])).
admit. (* holds for all indices *)
smt.
smt.
case ( (validInputs result_R result_R1 /\
       G2.pguess{1} < fn_m (fst result_R))).
intros half_true.
cut -> : ( ((eval result_R result_R1).[G2.pguess{1}] = G2.bguess{1}) = false).
generalize half_true;smt.
simplify;by trivial.
intros half_false.
cut leftover : (G2.pguess{1} < fn_m (fst result_R) = false).
smt.
delta chkForge;beta.
cut -> : ((range 0 (length (eval result_R result_R1))
    (init m_bnd (lambda (i : int), false))
    (lambda (i : int) (f : bool array),
       f.[i <-
         result_R2.[i] =
         if (eval result_R result_R1).[i] then
           fst (outputK result_R result_R0).[i]
         else snd (outputK result_R result_R0).[i]])).[G2.pguess{1}] = false).
admit. (* (init ...).[pguess] = false *)
simplify;by trivial.
cut -> : ((init m_bnd (lambda (i : int), false)).[G2.pguess{1}] = false).
admit. (* (init ...).[pguess] = false *)
simplify;by trivial.
rnd; rnd; skip; progress; smt.
qed.

  (** This puts everything together and completes the multiplicative hop *)

lemma PrG2_res &m:
forall (R<: Rand_t{Game_Auth,G2,G2'}) (A<: Adv_Auth_t{Game_Auth,G2,G2',R}),
   islossless A.gen_fun => islossless A.gen_input =>  islossless A.forge =>
   islossless R.gen =>
  Pr[Game_Auth(R,A).main() @ &m: res] <=
  (2*m_bnd)%r * Pr[G2'(R, A).main() @ &m : res].
proof.
intros R A A1L A2L A3L RL.
apply (Trans _ Pr[G1(R, A).main() @ &m : res]).
 by equiv_deno (G1_Auth_equiv R A).
cut ->: Pr[G1(R, A).main() @ &m : res]
        = Pr[G2(R, A).main() @ &m : or_array G2.forgeChk].
 by equiv_deno (G1G2_equiv R A).
apply (Trans _ (sum_ij 0 (m_bnd-1)
                 (lambda k, Pr[G2(R, A).main() @ &m : G2.forgeChk.[k] ]))).
 by apply (PrG2_xpnd1 &m R A (m_bnd-1) _).
cut ->: Pr[G2'(R, A).main() @ &m : res] = Pr[G2(R, A).main() @ &m : res].
 apply eq_sym.
 by equiv_deno (G2G2_equiv' R A). 
cut ->: Pr[G2(R, A).main() @ &m : res]
        = Pr[G2(R, A).main() @ &m : G2.forgeChk.[G2.pguess] /\ 
               G2.out.[G2.pguess]=G2.bguess /\ 0 <= G2.pguess < m_bnd].
 by equiv_deno (G2G2_equiv R A).
cut ->: sum_ij 0 (m_bnd - 1)
           (lambda (k : int), Pr[G2(R, A).main() @ &m : G2.forgeChk.[k]])
      = sum_ij 0 (m_bnd - 1)
           (lambda (k : int), (2*m_bnd)%r * Pr[G2(R, A).main() @ &m :
             G2.forgeChk.[k] /\ G2.pguess = k /\ G2.out.[k] = G2.bguess ]).
 apply sum_eq => k Hk /=.
 rewrite (PrG2_eq &m k R A _); first smt.
 cut _: (2*m_bnd)%r <> 0%r by smt.
 smt.
cut ->: sum_ij 0 (m_bnd - 1)
  (lambda (k : int),
     (2 * m_bnd)%r *
     Pr[G2(R, A).main() @ &m :
        G2.forgeChk.[k] /\ G2.pguess = k /\ G2.out.[k] = G2.bguess]) 
 = (2 * m_bnd)%r * sum_ij 0 (m_bnd - 1)
      (lambda (k : int),
        Pr[G2(R, A).main() @ &m :
            G2.forgeChk.[k] /\ G2.pguess = k /\ G2.out.[k] = G2.bguess]) .
 by rewrite sum_ij_mul_distr.
rewrite (PrG2_xpnd2 &m R A) //.
apply eq_le.
congr => //; congr => //; apply fun_ext => k //=.
rewrite Pr mu_eq; smt.
qed.

(******************************************************************************)
(*** Let's now formalize aPriv                                              ***)
(******************************************************************************)

  module type Adv_APriv_t = { 
    fun gen_fun() : fun_t*int*bool*fun_t {*}
    fun gen_input(fG : funG_t) : Input.input_t
    fun guess(xG : Input.inputG_t, oKi: word*word) : bool
  }.

op fixOutput(fn : fun_t, pguess : int, bguess : bool) = 
   let (n, m, q, aa, bb) = fst fn in
   let gates = snd fn in
   let i = n + q - m + pguess in
   let modgates = gates.[(i,false,false) <- bguess]
                       .[(i,false,true) <- bguess]
                       .[(i,true,false) <- bguess]
                       .[(i,true,false) <- bguess]
   in (fst fn,modgates).

  module APriv(R: Rand_t, A : Adv_APriv_t) = {
    var fn : fun_t
    var x : Input.input_t
    var b, b': bool
    var pos: int
    var posval: bool
    
    fun main(b:bool) : bool = {
      var fn': fun_t;
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var oKi: word*word;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r : bool;

      (fn, pos, posval, fn') = A.gen_fun();
      rg = R.gen(phi fn);
      fG = funG (if b then fn else fn') rg;
      x = A.gen_input(fG);
      iK = inputK fn rg; 
      oK = outputK fn rg;
      oKi = oK.[pos]; 
      xG = Input.encode iK x;
      b' = A.guess(xG, oKi); 

      if (validInputs fn x /\ fn' = fixOutput fn pos posval /\ 
         (eval fn x).[pos] = posval) {
             r = b = b';
      }
      else
      {
             r = ${0,1};
      }   

      return r;
    }
  }.

  (** The algorithm that interpolates between two games down to APriv *)

module Red(R:Rand_t, A:Adv_Auth_t) : Adv_APriv_t = {
  var pguess:int 
  var bguess:bool
  var fn: fun_t
  var x: input_t
  
  fun gen_fun() : fun_t*int*bool*fun_t = {
      var fn': fun_t;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      fn = A.gen_fun();
      fn' = fixOutput fn pguess bguess;
      return (fn,pguess,bguess,fn');
  }
    
  fun gen_input(fG: funG_t) : input_t = {
    x = A.gen_input(fG);
    return x;
  }

  fun guess(xG: inputG_t, oKi: word*word) : bool = {
    var r: bool;
    var oG: outputG_t;

    oG = A.forge(xG);

    if (validInputs fn x /\ pguess < fn_m (fst fn) /\ 
       (eval fn x).[pguess]=bguess) {
       r = (oG.[pguess] = if bguess
                          then fst oKi
                          else snd oKi);
      }
      else {
       r = ${0,1};
      }
      return r;
    }

}.

  (** Left-hand side of interpolation *)


lemma G2_APriv: forall (R<:Rand_t{G2,G2',APriv,Red}) (A<:Adv_Auth_t {G2,G2',APriv,Red,R}),
   equiv [ G2'(R,A).main ~ APriv(R,Red(R,A)).main : 
           b{2} ==> res{1} /\ validInputs G2'.fn{1} G2'.x{1}
                     /\ G2'.pguess{1} < fn_m (fst G2'.fn{1}) 
                     /\ (eval G2'.fn{1} G2'.x{1}).[G2'.pguess{1}]=G2'.bguess{1} <=>
                    res{2} /\ validInputs APriv.fn{2} APriv.x{2}
                     /\ APriv.pos{2} < fn_m (fst APriv.fn{2}) 
                 /\ (eval APriv.fn{2} Red.x{2}).[APriv.pos{2}]=APriv.posval{2}].
proof.
intros R A; fun.
inline Red(R,A).gen_fun Red(R,A).gen_input Red(R,A).guess G2'(R, A).game.
seq 13 17 : (
G2'.pguess{1} = Red.pguess{2} /\
G2'.bguess{1} = Red.bguess{2} /\
pguess{1} = Red.pguess{2} /\
bguess{1} = Red.bguess{2} /\
G2'.fn{1} = Red.fn{2} /\
(APriv.fn{2}, APriv.pos{2}, APriv.posval{2}, fn'{2}) =  (Red.fn{2}, Red.pguess{2}, Red.bguess{2}, fn'0{2}) /\
fn'0{2} = fixOutput Red.fn{2} Red.pguess{2} Red.bguess{2} /\
fG0{2} = funG (b{2} ? APriv.fn{2} : fn'{2}) rg{2}  /\ 
G2'.x{1} = Red.x{2} /\
G2'.x{1} = APriv.x{2} /\
oKi0{2} = oK{2}.[APriv.pos{2}] /\
xG0{2} = xG{2} /\
oG{1} = oG0{2} /\
b{2} /\
={rg,fG,iK,oK,xG} 
).
wp; call (_: ={glob A, X} ==> ={res}); first by fun true.
wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by fun true.
wp; call (_: ={l} ==> ={res}); first by fun true.
wp; call (_: true ==> ={glob A, res}); first by fun true.
wp;rnd;rnd;skip;progress;smt.
case (!validInputs G2'.fn{1} G2'.x{1}).
rcondf {1} 1.
intros &m;skip;smt.
rcondf {2} 1.
intros &m;skip;smt.
rcondf {2} 3.
intros &m;wp;rnd;wp;skip;progress;smt.
rnd {2};wp;rnd{2};skip;smt.
case (pguess{1} < fn_m (fst G2'.fn{1}) /\  
     (eval G2'.fn{1} G2'.x{1}).[pguess{1}] = bguess{1}).
rcondt {1} 1.
intros &m;skip;smt.
rcondt {2} 1.
intros &m;skip;smt.
rcondt {2} 3.
intros &m;wp;skip;smt.
wp;skip;progress.
smt.
rcondf {1} 1.
intros &m;skip;smt.
rcondf {2} 1.
intros &m;skip;smt.
case (validInputs APriv.fn{2} APriv.x{2} /\  
      fn'{2} =  fixOutput APriv.fn{2} APriv.pos{2} APriv.posval{2} /\        
      (eval APriv.fn{2} APriv.x{2}).[APriv.pos{2}] = APriv.posval{2}).
rcondt {2} 3.
intros &m;wp;rnd;skip;progress;smt.
wp;rnd{2};skip;progress;smt.
rcondf {2} 3.
intros &m;wp;rnd;skip;progress;smt.
wp;rnd{2};wp;rnd{2};skip;progress;smt.
qed.
  
  (*******************)
  (** The final Game *)
  (*******************)

  module G3(R: Rand_t, A : Adv_Auth_t) = {
      var fn : fun_t
      var x : Input.input_t
      var pguess: int
      var bguess: bool

    fun game(pguess: int, bguess: bool) : bool = {
      var rg : rand_t;
      var fG : funG_t;
      var iK : Input.inputK_t;
      var oK : outputK_t;
      var xG : Input.inputG_t;
      var oG : outputG_t;
      var r: bool;

      fn = A.gen_fun();
      rg = R.gen(phi fn);

      fG = funG (fixOutput fn pguess bguess) rg;
      x = A.gen_input(fG);
      iK = inputK fn rg;
      oK = outputK fn rg;
      xG = Input.encode iK x;
      oG = A.forge(xG);

      r = false;
      if (validInputs fn x /\ pguess < fn_m (fst fn) /\ 
          (eval fn x).[pguess]=bguess)
        r = (oG.[pguess] = if bguess
                             then fst oK.[pguess]
                             else snd oK.[pguess]);

      return r;
    }
    fun main() : bool = {
      var r: bool;
      pguess = $[0..m_bnd-1];
      bguess = ${0,1};
      r = game(pguess, bguess);
      return r;
    }
  }.

  (** Right-hand side of interpolation *)

lemma G3_APriv: forall (R<:Rand_t{G2,G2',G3,APriv,Red}) (A<:Adv_Auth_t {G2,G2',G3,APriv,Red,R}),
   equiv [ G3(R,A).main ~ APriv(R,Red(R,A)).main : 
           !b{2} ==> res{1} /\ validInputs G3.fn{1} G3.x{1}
                     /\ G3.pguess{1} < fn_m (fst G3.fn{1}) 
                     /\ (eval G3.fn{1} G3.x{1}).[G3.pguess{1}]=G3.bguess{1} <=>
                    !res{2} /\ validInputs APriv.fn{2} APriv.x{2}
                     /\ APriv.pos{2} < fn_m (fst APriv.fn{2}) 
                 /\ (eval APriv.fn{2} Red.x{2}).[APriv.pos{2}]=APriv.posval{2}].
proof.
intros R A; fun.
inline Red(R,A).gen_fun Red(R,A).gen_input Red(R,A).guess G3(R, A).game.
seq 13 17 : (
G3.pguess{1} = Red.pguess{2} /\
G3.bguess{1} = Red.bguess{2} /\
pguess{1} = Red.pguess{2} /\
bguess{1} = Red.bguess{2} /\
G3.fn{1} = Red.fn{2} /\
fn'0{2} = fixOutput Red.fn{2} Red.pguess{2} Red.bguess{2} /\
(APriv.fn{2}, APriv.pos{2}, APriv.posval{2}, fn'{2}) =  (Red.fn{2}, Red.pguess{2}, Red.bguess{2}, fn'0{2}) /\
fn'0{2} = fixOutput Red.fn{2} Red.pguess{2} Red.bguess{2} /\
fG0{2} = funG (b{2} ? APriv.fn{2} : fn'{2}) rg{2}  /\ 
G3.x{1} = Red.x{2} /\
G3.x{1} = APriv.x{2} /\
oKi0{2} = oK{2}.[APriv.pos{2}] /\
xG0{2} = xG{2} /\
oG{1} = oG0{2} /\
!b{2} /\
={rg,fG,iK,oK,xG} 
).
wp; call (_: ={glob A, X} ==> ={res}); first by fun true.
wp; call (_: ={glob A, fG} ==> ={glob A, res}); first by fun true.
wp; call (_: ={l} ==> ={res}); first by fun true.
wp; call (_: true ==> ={glob A, res}); first by fun true.
wp;rnd;rnd;skip;progress;smt.
case (!validInputs G3.fn{1} G3.x{1}).
rcondf {1} 1.
intros &m;skip;smt.
rcondf {2} 1.
intros &m;skip;smt.
rcondf {2} 3.
intros &m;wp;rnd;wp;skip;progress;smt.
rnd {2};wp;rnd{2};skip;smt.
case (pguess{1} < fn_m (fst G3.fn{1}) /\  
     (eval G3.fn{1} G3.x{1}).[pguess{1}] = bguess{1}).
rcondt {1} 1.
intros &m;skip;smt.
rcondt {2} 1.
intros &m;skip;smt.
rcondt {2} 3.
intros &m;wp;skip;smt.
wp;skip;progress.
smt.
smt.
rcondf {1} 1.
intros &m;skip;smt.
rcondf {2} 1.
intros &m;skip;smt.
case (validInputs APriv.fn{2} APriv.x{2} /\  
      fn'{2} =  fixOutput APriv.fn{2} APriv.pos{2} APriv.posval{2} /\        
      (eval APriv.fn{2} APriv.x{2}).[APriv.pos{2}] = APriv.posval{2}).
rcondt {2} 3.
intros &m;wp;rnd;skip;progress;smt.
wp;rnd{2};skip;progress;smt.
rcondf {2} 3.
intros &m;wp;rnd;skip;progress;smt.
wp;rnd{2};wp;rnd{2};skip;progress;smt.
qed.

  (** Summing up the interpolation hop *)

    lemma gm2'_gm3_hop : 
       forall &m (R<:Rand_t{G2,G2',G3,APriv,Red}) 
              (A<:Adv_Auth_t {G2,G2',G3,APriv,Red,R}),
       Pr[G2'(R,A).main()@ &m:res] -
       Pr[G3(R,A).main()@ &m:res] = 
               Pr[  APriv(R,Red(R,A)).main(true)@ &m:res] -
               Pr[  APriv(R,Red(R,A)).main(false)@ &m:!res].
     proof.
     intros &m R A.
     cut -> : (Pr[G2'(R,A).main()@ &m:res] = 
      Pr[G2'(R,A).main()@ &m:res 
                     /\ (validInputs G2'.fn G2'.x
                     /\ G2'.pguess < fn_m (fst G2'.fn) 
                     /\ (eval G2'.fn G2'.x).[G2'.pguess]=G2'.bguess) ] + 
            Pr[G2'(R,A).main()@ &m:res 
                     /\ !(validInputs G2'.fn G2'.x
                     /\ G2'.pguess < fn_m (fst G2'.fn) 
                     /\ (eval G2'.fn G2'.x).[G2'.pguess]=G2'.bguess) ]).
     admit. (* To do: split and *)
     cut -> : (Pr[G2'(R,A).main()@ &m:res 
                     /\ (validInputs G2'.fn G2'.x
                     /\ G2'.pguess < fn_m (fst G2'.fn) 
                     /\ (eval G2'.fn G2'.x).[G2'.pguess]=G2'.bguess) ] =
               Pr[APriv(R,Red(R,A)).main(true)@ &m:res 
                     /\ (validInputs APriv.fn APriv.x
                     /\ APriv.pos < fn_m (fst APriv.fn) 
                 /\ (eval APriv.fn Red.x).[APriv.pos]=APriv.posval) ]).
     equiv_deno (G2_APriv (R) (A)).
    smt.
    smt.
     cut -> : (Pr[G3(R,A).main()@ &m:res] = 
      Pr[G3(R,A).main()@ &m:res 
                     /\ (validInputs G3.fn G3.x
                     /\ G3.pguess < fn_m (fst G3.fn) 
                     /\ (eval G3.fn G3.x).[G3.pguess]=G3.bguess) ] + 
            Pr[G3(R,A).main()@ &m:res 
                     /\ !(validInputs G3.fn G3.x
                     /\ G3.pguess < fn_m (fst G3.fn) 
                     /\ (eval G3.fn G3.x).[G3.pguess]=G3.bguess) ]).
     admit. (* To do: split and *)
     cut -> : ( Pr[G3(R,A).main()@ &m:res 
                     /\ (validInputs G3.fn G3.x
                     /\ G3.pguess < fn_m (fst G3.fn) 
                     /\ (eval G3.fn G3.x).[G3.pguess]=G3.bguess) ] =
               Pr[APriv(R,Red(R,A)).main(false)@ &m:!res 
                     /\ (validInputs APriv.fn APriv.x
                     /\ APriv.pos < fn_m (fst APriv.fn) 
                 /\ (eval APriv.fn Red.x).[APriv.pos]=APriv.posval) ]).
     equiv_deno (G3_APriv (R) (A)).
     smt.
     smt.
     cut -> : (
       Pr[G2'(R,A).main()@ &m:res 
                     /\ !(validInputs G2'.fn G2'.x
                     /\ G2'.pguess < fn_m (fst G2'.fn) 
                     /\ (eval G2'.fn G2'.x).[G2'.pguess]=G2'.bguess) ] = 
       Pr[G3(R,A).main()@ &m:res 
                     /\ !(validInputs G3.fn G3.x
                     /\ G3.pguess < fn_m (fst G3.fn) 
                     /\ (eval G3.fn G3.x).[G3.pguess]=G3.bguess) ]
     ).
     admit. (* To do: adv always loses in both games *)
     cut -> : (
Pr[APriv(R, Red(R, A)).main(true) @ &m :
   res /\
   validInputs APriv.fn APriv.x /\
   APriv.pos < fn_m (fst APriv.fn) /\
   (eval APriv.fn Red.x).[APriv.pos] = APriv.posval] +
Pr[G3(R, A).main() @ &m :
   res /\
   ! (validInputs G3.fn G3.x /\
      G3.pguess < fn_m (fst G3.fn) /\
      (eval G3.fn G3.x).[G3.pguess] = G3.bguess)] -
(Pr[APriv(R, Red(R, A)).main(false) @ &m :
    ! res /\
    validInputs APriv.fn APriv.x /\
    APriv.pos < fn_m (fst APriv.fn) /\
    (eval APriv.fn Red.x).[APriv.pos] = APriv.posval] +
 Pr[G3(R, A).main() @ &m :
    res /\
    ! (validInputs G3.fn G3.x /\
       G3.pguess < fn_m (fst G3.fn) /\
       (eval G3.fn G3.x).[G3.pguess] = G3.bguess)]) =
Pr[APriv(R, Red(R, A)).main(true) @ &m :
   res /\
   validInputs APriv.fn APriv.x /\
   APriv.pos < fn_m (fst APriv.fn) /\
   (eval APriv.fn Red.x).[APriv.pos] = APriv.posval] -
Pr[APriv(R, Red(R, A)).main(false) @ &m :
    ! res /\
    validInputs APriv.fn APriv.x /\
    APriv.pos < fn_m (fst APriv.fn) /\
    (eval APriv.fn Red.x).[APriv.pos] = APriv.posval]
     ).
     smt.
     cut -> : (
Pr[APriv(R, Red(R, A)).main(true) @ &m : res] = 
Pr[APriv(R, Red(R, A)).main(true) @ &m :
   res /\
   (validInputs APriv.fn APriv.x /\
   APriv.pos < fn_m (fst APriv.fn) /\
   (eval APriv.fn Red.x).[APriv.pos] = APriv.posval)] +
Pr[APriv(R, Red(R, A)).main(true) @ &m :
   res /\
   !(validInputs APriv.fn APriv.x /\
   APriv.pos < fn_m (fst APriv.fn) /\
   (eval APriv.fn Red.x).[APriv.pos] = APriv.posval)]
     ).
     admit.  (* To do: split and *)
     cut -> : (
Pr[APriv(R, Red(R, A)).main(false) @ &m : !res] = 
Pr[APriv(R, Red(R, A)).main(false) @ &m :
   !res /\
   (validInputs APriv.fn APriv.x /\
   APriv.pos < fn_m (fst APriv.fn) /\
   (eval APriv.fn Red.x).[APriv.pos] = APriv.posval)] +
Pr[APriv(R, Red(R, A)).main(false) @ &m :
   !res /\
   !(validInputs APriv.fn APriv.x /\
   APriv.pos < fn_m (fst APriv.fn) /\
   (eval APriv.fn Red.x).[APriv.pos] = APriv.posval)]
     ).
     admit.  (* To do: split and *)
cut -> : (
Pr[APriv(R, Red(R, A)).main(true) @ &m :
   res /\
   ! (validInputs APriv.fn APriv.x /\
      APriv.pos < fn_m (fst APriv.fn) /\
      (eval APriv.fn Red.x).[APriv.pos] = APriv.posval)] =
 Pr[APriv(R, Red(R, A)).main(false) @ &m :
    ! res /\
    ! (validInputs APriv.fn APriv.x /\
       APriv.pos < fn_m (fst APriv.fn) /\
       (eval APriv.fn Red.x).[APriv.pos] = APriv.posval)]
).
     admit. (* in this case the output is a random bit in both sides *)
     cut -> : (
(Pr[APriv(R, Red(R, A)).main(true) @ &m :
   res /\
   validInputs APriv.fn APriv.x /\
   APriv.pos < fn_m (fst APriv.fn) /\
   (eval APriv.fn Red.x).[APriv.pos] = APriv.posval] +
Pr[APriv(R, Red(R, A)).main(false) @ &m :
   ! res /\
   ! (validInputs APriv.fn APriv.x /\
      APriv.pos < fn_m (fst APriv.fn) /\
      (eval APriv.fn Red.x).[APriv.pos] = APriv.posval)] -
(Pr[APriv(R, Red(R, A)).main(false) @ &m :
    ! res /\
    validInputs APriv.fn APriv.x /\
    APriv.pos < fn_m (fst APriv.fn) /\
    (eval APriv.fn Red.x).[APriv.pos] = APriv.posval] +
 Pr[APriv(R, Red(R, A)).main(false) @ &m :
    ! res /\
    ! (validInputs APriv.fn APriv.x /\
       APriv.pos < fn_m (fst APriv.fn) /\
       (eval APriv.fn Red.x).[APriv.pos] = APriv.posval)])) = 
(Pr[APriv(R, Red(R, A)).main(true) @ &m :
   res /\
   validInputs APriv.fn APriv.x /\
   APriv.pos < fn_m (fst APriv.fn) /\
   (eval APriv.fn Red.x).[APriv.pos] = APriv.posval] -
Pr[APriv(R, Red(R, A)).main(false) @ &m :
    ! res /\
    validInputs APriv.fn APriv.x /\
    APriv.pos < fn_m (fst APriv.fn) /\
    (eval APriv.fn Red.x).[APriv.pos] = APriv.posval])
     ). 
     smt.
     reflexivity.
     save.

  (** Upper-bounding the probability of success in the last game *)

lemma Fake_pr &m:
 forall (R<:Rand_t{G2,G2',G3,APriv,Red}) (A<:Adv_Auth_t {G2,G2',G3,APriv,Red,R}),
 Pr[G3(R, A).main() @ &m : res] = 1%r / (2 ^ (W.length))%r.
proof.
admit. (* The token to be guessed is independent from what A sees *)
save.

  (** Putting it all together to get adaptive authenticity for Garble 2
      assuming APriv holds. *)

lemma Scheme_is_auth &m: 
 forall (R<:Rand_t{Game_Auth,G2,G2',G3,APriv,Red}) (A<:Adv_Auth_t {Game_Auth,G2,G2',G3,APriv,Red,R}),
islossless A.gen_fun => islossless A.gen_input =>  islossless A.forge =>
   islossless R.gen =>   Pr[Game_Auth(R,A).main() @ &m : res] <= 
      (2*m_bnd)%r * (1%r / (2 ^ (W.length))%r + 
               Pr[  APriv(R,Red(R,A)).main(true)@ &m:res] -
               Pr[  APriv(R,Red(R,A)).main(false)@ &m:!res]).
proof.
intros R A Agenfun Agenin Afor Rgen.
cut H : (
  Pr[Game_Auth(R,A).main() @ &m: res] <=
  (2*m_bnd)%r * Pr[G2'(R, A).main() @ &m : res]
).
apply (PrG2_res &m (R) (A) _ _ _ _).
apply Agenfun.
apply Agenin.
apply Afor.
apply Rgen.
cut H1 : (
Pr[G2'(R,A).main()@ &m:res] -
       Pr[G3(R,A).main()@ &m:res] = 
               Pr[  APriv(R,Red(R,A)).main(true)@ &m:res] -
               Pr[  APriv(R,Red(R,A)).main(false)@ &m:!res]
).
apply (gm2'_gm3_hop &m (R) (A)).
cut H2 : (
Pr[G2'(R,A).main()@ &m:res] = Pr[G3(R,A).main()@ &m:res] +
               Pr[  APriv(R,Red(R,A)).main(true)@ &m:res] -
               Pr[  APriv(R,Red(R,A)).main(false)@ &m:!res]
).
smt.
generalize H;rewrite H2 => H.
generalize H;rewrite (Fake_pr &m (R) (A)) => H.
apply H.
save.

end DKCScheme2.
