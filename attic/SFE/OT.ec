require import Bitstring.
require import Array.

theory OT.
  (* The parametrizable attributes of n simultaneous 1 out of 2 OT protocol *)
  type msg_t.
  (* We hardwire the input types to be bool array for the picker,
     and (msg_t * msg_t) array for the holder. *)

  (* Party 1 types *)
  type rand1_t.

  (* Party 2 types *)
  type rand2_t.

  (* execution trace and views *)
  type conv_t.

  (* protocol execution & outcomes *)
  op prot : bool array -> rand1_t -> (msg_t * msg_t) array -> rand2_t -> conv_t * (msg_t array * unit).
end OT.
