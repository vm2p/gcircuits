require import Array.
require import Bitstring.
require import Int.
require import Pair.
require import FMap. import OptionGet.
require import Option.

theory Scheme.
  (*Need to be separated for OT compatibility *)
  theory Input.
    type input_t.
    type inputK_t.
    type inputG_t.
    op encode : inputK_t -> input_t -> inputG_t.
    op inputG_len : inputG_t -> int. (* for VC proofs *)
  end Input.
  export Input.

  type fun_t.
  type funG_t.
  type output_t.
  type outputK_t.
  type outputG_t.
  type leak_t.
  type rand_t.

  op validInputs : fun_t -> input_t -> bool.
  pred validRand : (fun_t, rand_t).
  op phi : fun_t -> leak_t.
  op eval : fun_t -> input_t -> output_t.
  op funG : fun_t -> rand_t -> funG_t.
  op inputK : fun_t -> rand_t -> inputK_t.
  op outputK : fun_t -> rand_t -> outputK_t.
  op valid_outG : outputK_t -> outputG_t -> bool. (* VC change *)
  op decode : outputK_t -> outputG_t -> output_t.
  op evalG : funG_t -> inputG_t -> outputG_t.
  op pi_sampler : leak_t * output_t  -> fun_t * input_t.
end Scheme.
export Scheme.

pred Correct (x : unit) = forall r fn i,
  validInputs fn i =>
  validRand fn r =>
  eval fn i =
   let fG = funG fn r in
   let iK = inputK fn r in
   let oK = outputK fn r in
   let iG = encode iK i in
   decode oK (evalG fG iG).
