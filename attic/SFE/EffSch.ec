require import Array.
require import Int.
require import Pair.
require import Bool.
require import FMap. import OptionGet.
require import Option.
require import FSet.
require import Real.

require        Sch.
require        SchSec.
require import GarbleTools.
require        DKCSch.
require        ProjSch.

theory EfficientScheme.  
  clone import ExtWord as W.

  clone DKCSch.DKCScheme with theory W = W.

  theory Local.
    (* some types reused for garbling scheme definition  *)
    type 'a tupleGate_t = 'a*'a*'a*'a.
    type tokens_t = (word * word) array.
    type 'a gates_t = ('a tupleGate_t) array.
    type 'a funct_t = topo_t * 'a gates_t.

    (* Semantics of selection is False = fst *)
    op getTok(x:tokens_t) (a:int) (i:bool): word =
      if i then snd x.[a] else fst x.[a].

    op evalTupleGate (f:'a tupleGate_t) (x1 x2:bool) =
      let (ff, ft, tf, tt) = f in
      if (!x1) /\ (!x2) then ff else
      if (!x1) /\ ( x2) then ft else
      if ( x1) /\ (!x2) then tf else
      tt.

    (* a b g should be in range 0 .. n + q - 1 to index tokens *)
    op enc (x:tokens_t) (f:bool tupleGate_t) (a b g:int) (x1 x2:bool) : word =
      let xx1 = (getlsb (getTok x a true) = x1) in (* correct token has matching lsb *)
      let xx2 = (getlsb (getTok x b true) = x2) in (* correct token has matching lsb *)
      DKCScheme.DKCSecurity.D.E
        (DKCScheme.Tweak.tweak g x1 x2) (* tweak is calculated with n offset wrt gate number *)
        (getTok x a xx1)
        (getTok x b xx2)
        (getTok x g (evalTupleGate f xx1 xx2)).

    (* a b g should be in range 0 .. n + q - 1 to index tokens *)
    op garbleGate (x:tokens_t) (f:bool tupleGate_t) (a b g:int): word tupleGate_t =
      (enc x f a b g false false,
       enc x f a b g false true,
       enc x f a b g true  false,
       enc x f a b g true  true).

    (* Definitions *)
    op validCircuit(f:(bool funct_t)) =
      let (n, m, q, aa, bb) = fst f in
      1 < n /\ 0 < m /\ 0 < q /\ m <= q /\
      length aa = q /\ length bb = q /\ length (snd f) = q /\
      n + q - m = DKCScheme.bound /\
      ForLoop.range 0 q true
        (lambda i b,
           b /\ 0 <= aa.[i] /\
           bb.[i] < n + i /\ bb.[i] < n + q - m /\ aa.[i] < bb.[i]).

    pred validCircuitP(f:(bool funct_t)) =
      let (n, m, q, aa, bb) = fst f in
      1 < n /\ 0 < m /\ 0 < q /\ m <= q /\
      n + q - m = DKCScheme.bound /\
      length aa = q /\ length bb = q /\ length (snd f) = q /\
      (forall i, 0 <= i < q =>
           0 <= aa.[i] /\
           bb.[i] < n + i /\ bb.[i] < n + q - m /\ aa.[i] < bb.[i]).

    lemma valid_wireinput fn : validCircuit fn <=> validCircuitP fn.
    proof strict.
    rewrite /validCircuitP /validCircuit.
    elim/tuple5_ind (fst fn)=> n m q aa bb valF /=.
    (* Put the range call in the correct form for rewriting *)
    cut ->: (lambda i b, b /\ 0 <= aa.[i] /\ bb.[i] < n + i /\ bb.[i] < n + q - m /\ aa.[i] < bb.[i]) =
              (lambda i b, b /\ (lambda k,
                                   0 <= aa.[k] /\ bb.[k] < n + k /\
                                   bb.[k] < n + q - m /\ aa.[k] < bb.[k]) i) by smt.
    by rewrite ForLoop.rangeb_forall.
    qed.

    (* Will extend whatever input array is there with length = gate count *)
    (* Extract takes the gate count q starting at 0! *)
    op evalComplete (f:'a funct_t) (x:'a array) (extract:'a funct_t -> int -> 'a array -> 'a) : 'a array =
      let (n, m, q, aa, bb) = fst f in
      init_dep x q (extract f).

    (* Will extend whatever input array is there with length = gate count, then take outputs *)
    (* Extract takes the _count_ starting at 0! *)
    op evalGen (f:'a funct_t) (x:'a array) (extract:'a funct_t -> int -> 'a array -> 'a) : 'a array =
      let (n, m, q, aa, bb) = fst f in
      sub (evalComplete f x extract) (n+q-m) m.

    (* Extraction functions *)
    (* OK to be called from init_dep if length = gate count *)
    op extractB (f:bool funct_t) (g:int) (x:bool array) : bool =
      let (n, m, q, aa, bb) = fst f in
      evalTupleGate (snd f).[g] (x.[aa.[g]]) (x.[bb.[g]]).

    (* OK to be called from appendInit if length = gate count *)
    op extractG (ff:word funct_t) (g:int) (x:word array) =
      let (n, m, q, aa, bb) = fst ff in
      let a = aa.[g] in
      let b = bb.[g] in
      let aA = x.[a] in
      let bB = x.[b] in
      let a = getlsb aA in
      let b = getlsb bB in
      let t = DKCScheme.Tweak.tweak (n + g) a b in (* tweak takes gate number in 0 range n + q - 1 *)
      DKCScheme.DKCSecurity.D.D t aA bB (evalTupleGate ((snd ff).[g]) a b).

    (* OK to be called from appendInit if length = gate count *)
    op garbMap (x:tokens_t) (f:bool funct_t) (g:int): word tupleGate_t =
      let (n, m, q, aa, bb) = fst f in
      garbleGate x (snd f).[g] aa.[g] bb.[g] (n + g).

    (* We define Bellare's preimage sampler Mtopo *)
    op Mtopo (im: topo_t * bool array) : bool funct_t * bool array =
      let (l,y) = im in
      let (n,m,q,A,B) = l in
      let G = init q
                (lambda g,
                   if g  < q - m 
                   then (false,false,false,false) 
                   else let v = y.[g-(q-m)] in (v,v,v,v)) in
      let fn = ((n,m,q,A,B),G) in 
      let x = init n (lambda k, false) in
      (fn,x).

    (* Makes sure randomness has correct length, so correctness works for all input
       randomness arrays. Security will be associated with concrete randomness generator,
       where length is always of the correct size. *)
    op randFormat(nwires : int, nouts : int, r : tokens_t) : tokens_t =
      if length r < nwires
      then init nwires (lambda k, (setlsb W.zeros false, setlsb W.zeros true)) 
      else mapi (lambda i x,
                   if i < (nwires - nouts)
                   then (setlsb (fst x) (getlsb (fst x)), (* to make sure fresh copy *)
                         setlsb (snd x) (! (getlsb (fst x))))
                   else (setlsb (fst x) false, setlsb (snd x) true)) r.

    op validInputs (fn:bool funct_t) (i:bool array) = 
      let (n, m, q, aa, bb) = fst fn in
      validCircuit fn /\
      n + q <= DKCScheme.maxGates /\
      n = length i.

    (* Evaluates boolean circuit *)
    op eval (fn:bool funct_t) (i:bool array) = evalGen fn i extractB.

    (* Evaluates garbled circuit *)
    op evalG (fn:word funct_t) (i:word array) = evalGen fn i extractG.

    op funG (fn:bool funct_t) (r:tokens_t) =
      let (n, m, q, a, b) = fst fn in
      (fst fn, init q (garbMap r fn)).

    op inputK (fn:bool funct_t) (r:tokens_t) =
      let (n, m, q, a, b) = fst fn in
      sub r 0 n.

    op outputK (fn:bool funct_t) (r:tokens_t) = ().

    op decode (k:unit) (o:word array) = map getlsb o.
  end Local.
  import Local.

  clone import ProjSch.ProjScheme with
    type token_t = word,
    type Sch.Scheme.fun_t = bool funct_t,
    type Sch.Scheme.funG_t = word funct_t,
    type Sch.Scheme.output_t = bool array,
    type Sch.Scheme.outputK_t = unit,
    type Sch.Scheme.outputG_t = word array,
    type Sch.Scheme.leak_t = topo_t,
    type Sch.Scheme.rand_t = (word*word) array,

    pred Sch.Scheme.validRand (fn:fun_t) (x:rand_t) =
      let (n, m, q, aa, bb) = fst fn in
      Array.length x = (n + q)%Int,
    op Sch.Scheme.validInputs = validInputs,
    op Sch.Scheme.phi (f:fun_t) = fst f,
    op Sch.Scheme.eval = eval,
    op Sch.Scheme.evalG = evalG,
    op Sch.Scheme.funG (fn:bool funct_t) (r:tokens_t) = 
      let (n, m, q, aa, bb) = fst fn in
      let rf = randFormat (n+q) m r in (* Concrete does not use this op to ensure *)
      funG fn rf,                      (* only one call to randFormat *)
    op Sch.Scheme.inputK(fn : bool funct_t, r : tokens_t) = 
      let (n, m, q, aa, bb) = fst fn in
      let rf = randFormat (n+q) m r in (* Concrete does not use this op to ensure *)
      inputK fn rf,                    (* only one call to randFormat *)
    op Sch.Scheme.outputK = outputK,
    op Sch.Scheme.decode = decode,
    op Sch.Scheme.pi_sampler = Mtopo.

  (* Begin Tools *)
  lemma get_rangeMap (x:(word * word) array) (y:int * bool):
    (get (ForLoop.range 0 (length x) FMap.Core.empty
      (lambda i (gg:(int*bool, word) map),
         gg.[(i, false) <- fst x.[i]].[(i, true) <- snd x.[i]])) y) =
           if (0 <= fst y < length x)
           then Some ((if snd y then snd else fst) x.[(fst y)])
           else None.
  proof strict.
  elim/Induction.induction (length x); first last; last 2 smt.
  intros=> i hi hr.
  rewrite ForLoop.range_ind_lazy; first smt.
  cut ->: (i + 1 - 1 = i) by smt.
  rewrite /= !get_set hr.
  smt.
  qed.

  op mapToArray (x:(int * bool,'a) map): ('a * 'a) array =
    let max = 1 + fold (lambda p (s:int), max (fst p) s) (-1) (dom x) in
    Array.init max (lambda i, (proj x.[(i, false)], proj x.[(i, true)])).

  op arrayToMap (x:('a * 'a) array): (int * bool,'a) map = 
    ForLoop.range 0 (Array.length x) FMap.Core.empty
      (lambda i (gg:(int * bool,'a) map), gg.[(i, false) <- fst x.[i]].[(i, true) <- snd x.[i]]).

  lemma get_arrayToMap (x:('a*'a) array) (y:int*bool):
    get (arrayToMap x) y =
      if (0 <= fst y < length x)
      then Some ((if snd y then snd else fst) x.[(fst y)])
      else None.
  proof strict.
  simplify arrayToMap.
  elim/Induction.induction (length x); first last; last 2 smt.
  intros=> i hi hr.
  rewrite ForLoop.range_ind_lazy; first smt.
  cut ->: (i + 1 - 1 = i) by smt.
  rewrite /= !get_set hr.
  smt.
  qed.

  lemma mem_dom_arrayToMap (x:('a*'a) array) b i:
    (0 <= i < length x) <=> mem (i, b) (dom (arrayToMap x)).
  proof strict.
  simplify arrayToMap.
  rewrite mem_dom.
  elim/array_ind_snoc x; first smt.
  intros=> x0 xs h.
  rewrite ForLoop.range_ind_lazy //=; first smt.
  rewrite !get_set !length_snoc.
  case (i = length xs).
    by intros=> -> /=; case b; smt.
  intros=> hlen.
  cut ->: length xs + 1 - 1 = length xs by smt.
  cut ->: ((length xs, false) = (i, b)) = false by smt.
  cut ->: ((length xs, true) = (i, b)) = false by smt.
  rewrite //= -ForLoop.range_restr; first smt.
  cut ->: (lambda (k : int) (a : (int * bool, 'a) map),
             if 0 <= k < length xs then
               (lambda (i0 : int) (gg : (int * bool, 'a) map),
                  gg.[(i0, false) <- fst (xs ::: x0).[i0]].[(i0, true) <-
                    snd (xs ::: x0).[i0]]) k a
             else a) =
          (lambda (k : int) (a : (int * bool, 'a) map),
             if 0 <= k < length xs then
               (lambda (i0 : int) (gg : (int * bool, 'a) map),
                  gg.[(i0, false) <- fst xs.[i0]].[(i0, true) <-
                    snd xs.[i0]]) k a
             else a).
    apply Fun.fun_ext=> k /=.
    apply Fun.fun_ext=> a /=.
    case (0 <= k < length xs)=> hk; last smt.
    by rewrite get_snoc;smt.
  by rewrite ForLoop.range_restr - ?h; smt.
  qed.

  lemma dom_arrayToMap (x:('a * 'a) array):
    dom (arrayToMap x) =
      union (img (lambda i, (i,false)) (Interval.interval 0 (Array.length x - 1)))
            (img (lambda i, (i, true)) (Interval.interval 0 (Array.length x - 1))).
  proof strict.
  apply set_ext=> y.
  elim/tuple2_ind y=> i b hy.
  rewrite -mem_dom_arrayToMap mem_union; split.
    by intros h; case b=> hb;[right|left];
       cut ->: forall (b:bool), (i, b) = (lambda (i0 : int), (i0, b)) i by smt;
       apply mem_img; rewrite Interval.mem_interval; smt.
    by rewrite !img_def=> [[y'] /= [hy'] | [y'] /= [hy']];
       rewrite Interval.mem_interval; smt.
  qed.

  lemma max_arrayToMap (x:('a * 'a) array):
    1 + fold (lambda (p:int * bool) (s:int),
                max (fst p) s) (-1) (dom (arrayToMap x)) =
      Array.length x.
  proof strict.
  rewrite dom_arrayToMap.
  pose xs:= union _ _.
  apply Int.Antisymm.
    cut: forall y, mem y xs => fst y < length x.
       by intros=> y; rewrite /xs mem_union !img_def=> h; smt.
    by elim/set_comp xs; [|progress; rewrite fold_rm_pick]; smt.

    (case (Array.length x = 0);first smt)=> hlen.
    cut: mem ((Array.length x - 1), false) xs.
      by rewrite /xs mem_union !img_def; left; exists (length x - 1); smt.
    by elim/set_comp xs; [|progress;rewrite fold_rm_pick]; smt.
  qed.

  lemma length_arrayToMap (x:('a * 'a) array) n:
    0 <= n =>
    (forall i b, (0 <= i < n) <=> mem (i, b) (dom (arrayToMap x))) =>
    length x = n.
  proof strict.
  intros=> npos.
  rewrite dom_arrayToMap.
  generalize x.
  elim/Induction.induction n=> //.
    intros=> x h.
    cut:= h 0 false=> /=.
    rewrite mem_union !img_def -rw_nor.
    progress.
    cut: !mem 0 (Interval.interval 0 (length x - 1)) by smt.
    rewrite Interval.mem_interval.
    smt.

    progress.
    cut ineg: i + 1 <= length x.
      cut:= H1 i false=> /=.
      rewrite mem_union !img_def.
      cut: 0 <= i < i + 1 = true by smt.
      progress.
      cut: mem i (Interval.interval 0 (length x - 1)) by smt.
      rewrite Interval.mem_interval.
      smt.
    case (length x > i + 1)=> hi0; last smt.
    cut hh: mem (i+1, false)
             (union
               (img (lambda (i1 : int), (i1, false))
                  ((Interval.interval 0 (length x - 1)))%Interval)
               (img (lambda (i1 : int), (i1, true))
                  ((Interval.interval 0 (length x - 1)))%Interval)) by smt.
    cut: 0 <= i + 1 < i + 1 by smt.
    smt.
  qed.


  lemma map_array (x:(int*bool, 'a) map) :
    (let max = 1 + fold (lambda (p : int * bool) (s : int), max (fst p) s) (-1) (dom x) in
     forall (y:int * bool), (mem (fst y,  true) (dom x) <=> 0 <= fst y < max) /\
                            (mem (fst y, false) (dom x) <=> 0 <= fst y < max) /\ 0 <= max) =>
    arrayToMap (mapToArray x) = x.
  proof strict.
  simplify mapToArray.
  pose max:= 1 + fold (lambda (p : int * bool) (s : int), max (fst p) s) (-1) (dom x).
  intros=> h.
  apply map_ext=> y.
  rewrite get_arrayToMap length_init //; first smt.
  case (0 <= fst y < max)=> hy.
    by rewrite !get_init //;
       case (snd y)=> H; [|cut: snd y = false by smt=> {H} H];
       rewrite (pairS y) H ?fst_pair ?snd_pair; smt.
    by rewrite (pairS y); case (snd y); smt.
  qed.

  lemma array_map (x:('a * 'a) array): mapToArray(arrayToMap x) = x.
  proof strict.
  simplify mapToArray.
  apply array_ext; split; first by rewrite length_init; smt.
  rewrite length_init; first smt.
  rewrite max_arrayToMap.
  intros=> i hi.
  rewrite get_init //=.
  simplify "_.[_]".
  rewrite !get_arrayToMap
          {1 3 4 5}/fst /= {1 2}/snd /=
          {1 2}/fst /=
          hi /= !proj_some.
  smt.
  qed.

  op initGates (size:int) (f:int->bool->bool->'a) : 'a gates_t =
    Array.init size (lambda (g:int), (f g false false, f g false true, f g true false, f g true true)).

op arrayToMap2 (n q:int) (x:('a*'a*'a*'a) array) =
  if length x = q then
    DKCScheme.Local.initGates n q (lambda g a b, evalTupleGate x.[g-n] a b)
  else
    FMap.Core.empty.
op mapToArray2 (n q:int) (x:(int*bool*bool, 'a) map) = initGates q (lambda g a b, proj x.[(g+n, a, b)]).

  lemma init_dep_length (ar:'a array) (size:int) (extract:int -> 'a array -> 'a):
    0 <= size =>
    length (init_dep ar size extract) = (length ar) + size.
  proof strict.
  intros=> hsize.
  rewrite init_dep_def.
  pose {2} n := size.
  elim/Induction.induction n;[ |progress;rewrite ForLoop.range_ind_lazy| ];smt.
  qed.

  lemma appendInit_init_dep (ar:'a array) (size:int) (extract1 extract2:int -> 'a array -> 'a):
    0 <= size =>
    let n = length ar in
    (forall (i:int) (xs1 xs2:'a array), 0 <= i < size =>
      (forall k, 0 <= k < i + n => xs1.[k] = xs2.[k]) =>
      extract1 (n+i-1) xs1 = extract2 i xs2) =>
    appendInit ar size extract1 = init_dep ar size extract2.
  proof strict.
  intros=> hsize.
  progress.
  cut: length (init_dep ar size extract2) = (length ar) + size /\
       (forall k, 0 <= k < length ar + size =>
          (appendInit ar size extract1).[k] = (init_dep ar size extract2).[k]).
    rewrite /appendInit /= !init_dep_def /=.
    pose {1 4 5 6}n:= size.
    cut: n <= size by trivial.
    elim/Induction.induction n=> //.
      by progress; smt.
      progress.
        by rewrite ForLoop.range_ind_lazy; smt.

        rewrite ForLoop.range_ind_lazy; first smt.
        rewrite (ForLoop.range_ind_lazy 0); first smt.
        cut:= appendInit_length ar i extract1.
        simplify appendInit appender=> appLen.
        cut-> : forall x, x + (i + 1) - 1 = x + i by smt.
        cut-> : i + 1 - 1 = i by smt.
        rewrite get_snoc; first by rewrite appLen; smt.
        rewrite Array.get_set; first smt.
        rewrite appLen //.
        cut:= H1 _; first smt.
        intros=> [H11 H12].
        case (k < length ar + i)=> h.
          cut -> : (i + length ar = k) = false by smt.
          simplify.
          rewrite -H12; first smt.
          by simplify appender.
          cut-> : (i + length ar = k) = true by smt.
          simplify.
          apply H; first smt.
          progress.
          rewrite -H12; first smt.
          by simplify appender.
      intros=> h.
      by apply array_ext; smt.
  qed.

  lemma map_array2 (n q:int) (x:(int * bool * bool,'a) map):
    0 <= q =>
    (forall (g:int) a b, get x (g, a, b) <> None <=> n <= g < n + q) =>
    arrayToMap2 n q (mapToArray2 n q x) = x.
  proof strict.
  simplify arrayToMap2 mapToArray2=> hq h.
  apply map_ext=> y.
  elim/tuple3_ind y=> g a b hy.
  cut:= DKCScheme.Local.get_initGates n q (lambda (g : int) (a b : bool),
          evalTupleGate (initGates q
            (lambda (g1 : int) (a1 b1 : bool), proj x.[(g1+n, a1, b1)])).[g-n]
            a b)
          g a b _=> //.
  simplify fst snd OptionGet."_.[_]".
  cut -> : length
        (initGates q
           (lambda (g0 : int) (a0 b0 : bool), proj (get x (g0 + n, a0, b0)))) =
      q by smt=> /= ->.
  case (n <= g < n + q)=> hc; last smt.
  simplify initGates.
  rewrite get_init; first smt.
  simplify evalTupleGate.
  by case a; case b; simplify; rewrite some_proj; smt.
  qed.

  lemma array_map2 (n q:int) (x:('a * 'a * 'a * 'a) array):
    length x = q =>
    mapToArray2 n q (arrayToMap2 n q x) = x.
  proof strict.
  simplify arrayToMap2 mapToArray2 fst snd=> hq.
  apply array_ext.
  simplify initGates; split; first smt.
  rewrite length_init; first smt.
  intros=> i ih.
  rewrite get_init /= // hq /= !DKCScheme.Local.get_initGates; first 4 smt.
  cut ->: (n <= i + n < n + q) = true; first smt.
  simplify evalTupleGate.
  smt.
  qed.

  lemma length_randFormat nq m r: nq = length r => length (randFormat nq m r) = length r.
  proof strict.
  by progress; simplify randFormat; rewrite length_mapi.
  qed.
  (* End Tools *)

  (* Begin Bijection *)
  pred functD_topo_valid (x:topo_t) = let (n, m, q, aa, bb) = x in
    0 <= n /\ 0 <= q /\
    length aa = (n + q)%Int /\
    length bb = (n + q)%Int /\
    (forall (i:int), 0 <= i < n => (aa.[i])%Array = 0 /\ (bb.[i])%Array = 0).

  pred functE_topo_valid (x:topo_t) =  let (n, m, q, aa, bb) = x in
    0 <= n /\ 0 <= q /\
    length aa = q /\
    length bb = q.

  op topoED(x:topo_t) : topo_t =
    let (n, m, q, aa, bb) = x in
    (n, m, q, (Array.init n (lambda i, 0)) || aa, (Array.init n (lambda i, 0)) || bb).

  op topoDE(x:topo_t) : topo_t =
    let (n, m, q, aa, bb) = x in
    (n, m, q, Array.sub aa n q, Array.sub bb n q).

  lemma topo_EDDE x: functD_topo_valid x =>
      topoED (topoDE x) = x.
  proof strict.
  elim/tuple5_ind x=> n m q aa bb.
  progress; apply array_ext; split.
    smt.
    intros=> i; rewrite length_append // length_init ?Array.length_sub; first 4 smt.
    by intros=> i_bnd; case (i < n); cut:= H; rewrite /functD_topo_valid /=; progress;
       [rewrite get_append_left | rewrite get_append_right]; smt.
    smt.
    intros=> i; rewrite length_append // length_init ?Array.length_sub; first 4 smt.
    by intros=> i_bnd; case (i < n); cut:= H; rewrite /functD_topo_valid /=; progress;
       [rewrite get_append_left | rewrite get_append_right]; smt.
  qed.

  lemma topo_DEED x: functE_topo_valid x =>
    topoDE (topoED x) = x
  by (simplify functE_topo_valid;smt).

  op leakED (x:ProjScheme.Sch.Scheme.leak_t): DKCScheme.SchSecurity.Sch.Scheme.leak_t = topoED x.
  op leakDE (x:DKCScheme.SchSecurity.Sch.Scheme.leak_t): ProjScheme.Sch.Scheme.leak_t = topoDE x.

  lemma leak_EDDE x: functD_topo_valid x =>
      leakED (leakDE x) = x by smt.

  lemma leak_DEED x: functE_topo_valid x =>
    leakDE (leakED x) = x by smt.

  op inputED (x:ProjScheme.Sch.Scheme.Input.input_t): DKCScheme.SchSecurity.Sch.Scheme.Input.input_t = x.
  op inputDE (x:DKCScheme.SchSecurity.Sch.Scheme.Input.input_t): ProjScheme.Sch.Scheme.Input.input_t = x.

  lemma input_EDDE x: inputED (inputDE x) = x by delta.

  lemma input_DEED x: inputDE (inputED x) = x by delta.

  op outputED (x:ProjScheme.Sch.Scheme.output_t): DKCScheme.SchSecurity.Sch.Scheme.output_t = x.
  op outputDE (x:DKCScheme.SchSecurity.Sch.Scheme.output_t): ProjScheme.Sch.Scheme.output_t = x.

  lemma output_EDDE x: outputED (outputDE x) = x by delta.

  lemma output_DEED x: outputDE (outputED x) = x by delta.

  op outputKED (x:ProjScheme.Sch.Scheme.outputK_t): DKCScheme.SchSecurity.Sch.Scheme.outputK_t = x.
  op outputKDE (x:DKCScheme.SchSecurity.Sch.Scheme.outputK_t): ProjScheme.Sch.Scheme.outputK_t = x.

  lemma outputK_EDDE x: outputKED (outputKDE x) = x by delta.

  lemma outputK_DEED x: outputKDE (outputKED x) = x by delta.

  op outputGED (x:ProjScheme.Sch.Scheme.outputG_t): DKCScheme.SchSecurity.Sch.Scheme.outputG_t = x.
  op outputGDE (x:DKCScheme.SchSecurity.Sch.Scheme.outputG_t): ProjScheme.Sch.Scheme.outputG_t = x.

  lemma outputG_EDDE x: outputGED (outputGDE x) = x by delta.

  lemma outputG_DEED x: outputGDE (outputGED x) = x by delta.

  op inputGED (x:ProjScheme.Sch.Scheme.Input.inputG_t): DKCScheme.SchSecurity.Sch.Scheme.Input.inputG_t = x.
  op inputGDE (x:DKCScheme.SchSecurity.Sch.Scheme.Input.inputG_t): ProjScheme.Sch.Scheme.Input.inputG_t = x.

  lemma inputG_EDDE x: inputGED (inputGDE x) = x by delta.

  lemma inputG_DEED x: inputGDE (inputGED x) = x by delta.

  op inputKED (x:ProjScheme.Sch.Scheme.Input.inputK_t): DKCScheme.SchSecurity.Sch.Scheme.Input.inputK_t =
    arrayToMap x.
  op inputKDE (x:DKCScheme.SchSecurity.Sch.Scheme.Input.inputK_t): ProjScheme.Sch.Scheme.Input.inputK_t =
    mapToArray x.

  pred tokensD_valid (x:DKCScheme.Local.tokens_t) =
    (let max = 1 + fold (lambda (p : int * bool) (s : int), max (fst p) s) (-1) (dom x) in
     forall (y:int*bool), (mem (fst y, true) (dom x) <=> 0 <= fst y < max) /\
                         (mem (fst y, false) (dom x) <=> 0 <= fst y < max) /\ 0 <= max).

  lemma inputK_EDDE x: tokensD_valid x => inputKED (inputKDE x) = x by smt.
  lemma inputK_DEED x: inputKDE (inputKED x) = x by smt.

  op randED (x:ProjScheme.Sch.Scheme.rand_t): DKCScheme.SchSecurity.Sch.Scheme.rand_t = arrayToMap x.
  op randDE (x:DKCScheme.SchSecurity.Sch.Scheme.rand_t): ProjScheme.Sch.Scheme.rand_t = mapToArray x.

  lemma rand_EDDE x: tokensD_valid x => randED (randDE x) = x by smt.
  lemma rand_DEED x: randDE (randED x) = x by smt.

  op funED (x:ProjScheme.Sch.Scheme.fun_t): DKCScheme.SchSecurity.Sch.Scheme.fun_t =
    let (n, m, q, aa, bb) = fst x in (topoED (fst x), arrayToMap2 n q (snd x)).
  op funDE (x:DKCScheme.SchSecurity.Sch.Scheme.fun_t): ProjScheme.Sch.Scheme.fun_t =
    let (n, m, q, aa, bb) = fst x in (topoDE (fst x), mapToArray2 n q (snd x)).

  pred functD_gg_valid (x:'a DKCScheme.Local.funct_t) = let (n, m, q, aa, bb) = fst x in
    (forall (g:int) a b, get (snd x) (g, a, b) <> None <=> n <= g < n + q).

  pred functE_gg_valid (x:'a funct_t) =  let (n, m, q, aa, bb) = fst x in
    length (snd x) = q.

  pred functD_valid (x:'a DKCScheme.Local.funct_t) = functD_topo_valid (fst x) /\ functD_gg_valid x.

  pred functE_valid (x:'a funct_t) = functE_topo_valid (fst x) /\ functE_gg_valid x.

  lemma fun_EDDE x : functD_valid x => funED (funDE x) = x.
  proof strict.
  simplify funED funDE functD_valid functD_topo_valid functD_gg_valid.
  elim/tuple2_ind x=> topo gg hx.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  simplify fst snd.
  progress.
    apply array_ext; split.
      smt.
      intros=> i; rewrite length_append // length_init ?Array.length_sub; first 4 smt.
      by intros=> i_bnd; case (i < n)=> i_n; [rewrite get_append_left | rewrite get_append_right]; smt.
    apply array_ext; split.
      smt.
      intros=> i; rewrite length_append // length_init ?Array.length_sub; first 4 smt.
      by intros=> i_bnd; case (i < n)=> i_n; [rewrite get_append_left | rewrite get_append_right]; smt.
    smt.
  qed.

  lemma fun_DEED x : functE_valid x => funDE (funED x) = x.
  proof strict.
  simplify funED funDE functE_valid functE_topo_valid functE_gg_valid.
  elim/tuple2_ind x=> topo gg hx.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  simplify fst snd; smt.
  qed.

  op funGED (x:ProjScheme.Sch.Scheme.funG_t) : DKCScheme.SchSecurity.Sch.Scheme.funG_t =
    let (n, m, q, aa, bb) = fst x in
    (topoED (fst x), arrayToMap2 n q (snd x)).
  op funGDE (x:DKCScheme.SchSecurity.Sch.Scheme.funG_t) : ProjScheme.Sch.Scheme.funG_t =
    let (n, m, q, aa, bb) = fst x in
    (topoDE (fst x), mapToArray2 n q (snd x)).

  lemma funG_EDDE x : functD_valid x => funGED (funGDE x) = x.
  proof strict.
  simplify funGED funGDE functD_valid functD_topo_valid functD_gg_valid.
  elim/tuple2_ind x=> topo gg hx.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  simplify fst snd.
  progress.
    apply array_ext; split.
      smt.
      intros=> i; rewrite length_append // length_init ?Array.length_sub; first 4 smt.
      by intros=> i_bnd; case (i < n)=> i_n; [rewrite get_append_left | rewrite get_append_right]; smt.
    apply array_ext; split.
      smt.
      intros=> i; rewrite length_append // length_init ?Array.length_sub; first 4 smt.
      by intros=> i_bnd; case (i < n)=> i_n; [rewrite get_append_left | rewrite get_append_right]; smt.
    smt.
  qed.

  lemma funG_DEED x : functE_valid x => funGDE (funGED x) = x.
  proof strict.
  simplify funGED funGDE functE_valid functE_topo_valid functE_gg_valid.
  elim/tuple2_ind x=> topo gg hx.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  simplify fst snd; smt.
  qed.
  (* End Bijection *)

  (* Begin morphism *)

  pred encode_valid (k:DKCScheme.SchSecurity.Sch.Scheme.Input.inputK_t) (i:DKCScheme.SchSecurity.Sch.Scheme.Input.input_t) = 1 + fold (lambda (p:int * bool) (s:int), max (fst p) s) (-1) (dom k) = length i.

  lemma encode_ED (k:DKCScheme.SchSecurity.Sch.Scheme.Input.inputK_t) (i:DKCScheme.SchSecurity.Sch.Scheme.Input.input_t) :
    encode_valid k i =>
    inputGED (ProjScheme.Sch.Scheme.Input.encode (inputKDE k) (inputDE i)) =
     DKCScheme.SchSecurity.Sch.Scheme.Input.encode k i.
  proof strict.
  simplify inputGED ProjScheme.Sch.Scheme.Input.encode inputKDE inputDE
           DKCScheme.SchSecurity.Sch.Scheme.Input.encode
           DKCScheme.C2.Input.encode
           mapToArray encode_valid.
  pose max:= 1 + fold (lambda p (s:int), max (fst p) s) (-1) (dom k).
  intros=> hmax.
  cut hli: 0 <= length i by apply length_pos.
  subst.
  apply array_ext.
  split; first by rewrite !length_init //.
  rewrite ?length_init //.
  intros=> j hj.
  rewrite !get_init //= get_init //=.
  by case (i.[j]); simplify fst snd.
  qed.

  lemma phi_ED fn : functD_topo_valid (fst fn) =>
    leakED (ProjScheme.Sch.Scheme.phi (funDE fn)) = DKCScheme.SchSecurity.Sch.Scheme.phi fn.
  proof strict.
  simplify delta.
  elim/tuple2_ind fn=> topo gg hx.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  progress; apply array_ext; split.
    smt.
    intros=> i; rewrite length_append // length_init ?Array.length_sub; first 4 smt.
    by intros=> i_bnd; case (i < n)=> i_n; [rewrite get_append_left | rewrite get_append_right]; smt.
    smt.
    intros=> i; rewrite length_append // length_init ?Array.length_sub; first 4 smt.
    by intros=> i_bnd; case (i < n)=> i_n; [rewrite get_append_left | rewrite get_append_right]; smt.
  qed.

  pred eval_valid (fn:'a DKCScheme.Local.funct_t) (i:'a array) =
    let (n, m, q, aa, bb) = fst fn in
    0 <= q /\ length aa = n + q /\ length bb = n + q /\ length i = n /\
    (forall i0, 0 <= i0 < q => 0 <= aa.[i0 + n] < i0 + n) /\
    (forall i0, 0 <= i0 < q => 0 <= bb.[i0 + n] < i0 + n).

  lemma eval_ED fn i : eval_valid fn i =>
    outputED (ProjScheme.Sch.Scheme.eval (funDE fn) (inputDE i)) = DKCScheme.SchSecurity.Sch.Scheme.eval fn i.
  proof strict.
  simplify outputED ProjScheme.Sch.Scheme.eval funDE inputDE DKCScheme.SchSecurity.Sch.Scheme.eval
           evalTupleGate DKCScheme.C2.eval initGates fst snd evalComplete extract mapToArray2
           eval evalGen topoDE topoED eval_valid.
  elim/tuple2_ind fn=> topo gg hx.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  progress.
  congr=> //.
  simplify GarbleTools.evalComplete.
  apply eq_sym.
  apply appendInit_init_dep; first smt.
  progress.
  rewrite /extractB fst_pair snd_pair /= /evalTupleGate get_init; smt.
  qed.

  lemma evalG_ED fn i : eval_valid fn i =>
    outputGED (ProjScheme.Sch.Scheme.evalG (funGDE fn) (inputGDE i)) = DKCScheme.SchSecurity.Sch.Scheme.evalG fn i.
  proof strict.
  simplify outputGED ProjScheme.Sch.Scheme.evalG funGDE inputGDE DKCScheme.SchSecurity.Sch.Scheme.evalG
           evalTupleGate DKCScheme.C2.evalG initGates fst snd evalComplete extract mapToArray2
           evalG evalGen topoDE topoED eval_valid.
  elim/tuple2_ind fn=> topo gg hx.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  progress.
  congr=> //.
  simplify GarbleTools.evalComplete.
  apply eq_sym.
  apply appendInit_init_dep; first smt.
  progress.
  rewrite /extractG fst_pair snd_pair /= /evalTupleGate get_init ?hli; first smt.
  cut -> : length i + i0 - 1 + 1 = length i + i0 by smt.
  rewrite !H6; first 2 smt.
  congr; first 3 smt.
  simplify DKCScheme.W.getlsb.
  rewrite !get_sub; first 8 smt.
  smt.
  qed.

  pred funG_valid (fn:'a DKCScheme.Local.funct_t) (r:(int*bool, word) map) =
    functD_topo_valid (fst fn) /\
    let (n,m,q,aa,bb) = fst fn in 
    1 + fold (lambda (p:int * bool) (s:int), max (fst p) s) (-1) (dom r) = (n + q)%Int /\
    forall (i:int), n <= i < n + q =>
      0 <= (aa.[i])%Array < n + q /\
      0 <= (bb.[i])%Array < n + q.

  op randFormatD (t:topo_t) (r:DKCScheme.SchSecurity.Sch.Scheme.rand_t) = 
    let (n, m, q, aa, bb) = t in
    randED (randFormat (n+q)%Int m (randDE r)).

  pred validRandD (x:'a funct_t) (r:(word * word) array) =
    let (n,m,q,aa,bb) = fst x in
    0 <= (n + q)%Int /\
    length r = n + q.

  lemma validRand_DE fn i:
    validRandD fn i =>
    ProjScheme.Sch.Scheme.validRand fn i =
     DKCScheme.SchSecurity.Sch.Scheme.validRand (funED fn) (randFormatD (fst (funED fn)) (randED i)).
  proof strict.
  intros=> vRand.
  elim/tuple2_ind fn=> t gg fn_def.
  elim/tuple5_ind t=> n m q aa bb t_def.
  rewrite /Sch.Scheme.validRand fst_pair /=.
  cut leni_nq: length i = n + q by smt.
  rewrite /funED fst_pair snd_pair /=.
  rewrite /topoED fst_pair /=.
  rewrite /randFormatD /=.
  rewrite rand_DEED /randED /randFormat.
  cut ->: (length i < n + q) = false by smt=> //=.
  rewrite /DKCScheme.SchSecurity.Sch.Scheme.validRand /DKCScheme.C2.validRand fst_pair /=.
  rewrite leni_nq //= rw_eq_sym rw_eqT=> k k_bnd.
    rewrite /FMap.OptionGet."_.[_]" !get_arrayToMap !fst_pair /=
            length_mapi leni_nq k_bnd //=
            !snd_pair //=
            !proj_some.
    rewrite Array.get_mapi ?leni_nq //=.
    case (k < n + q - m)=> k_nqm.
      rewrite fst_pair snd_pair /=.
      cut ->: (n + q - m <= k) = false by smt=> //=.
      by rewrite set_getlsb /DKCScheme.W.getlsb get_setlsb; smt.
      rewrite fst_pair snd_pair /=.
      cut ->: (n + q - m <= k) = true by smt=> //=.
      by rewrite /DKCScheme.W.getlsb !get_setlsb.
  qed.

  lemma funG_ED (fn:bool DKCScheme.Local.funct_t) (r:(int*bool, word) map):
    funG_valid fn r =>
    funGED (ProjScheme.Sch.Scheme.funG (funDE fn) (randDE r)) =
     DKCScheme.SchSecurity.Sch.Scheme.funG fn (randFormatD (fst fn) r).
  proof strict.
  elim/tuple2_ind fn=> t gg fn_def.
  elim/tuple5_ind t=> n m q aa bb t_def.
  rewrite /funG_valid /functD_topo_valid fst_pair /=.
  progress.
  cut hlen : length (init n (lambda (i0 : int), 0) || sub aa n q) = n + q by rewrite length_append ? length_init ? Array.length_sub ? H1 //.
  apply array_ext;split;first by rewrite hlen H1.
  progress.
  rewrite get_append // length_init //.
  case (0 <= i < n)=> h.
    by rewrite get_init //;smt.
    by rewrite get_sub ? H1 //;smt.
  
  cut hlen : length (init n (lambda (i0 : int), 0) || sub bb n q) = n + q by rewrite length_append ? length_init ? Array.length_sub ? H2 //.
  apply array_ext;split;first by rewrite hlen H2.
  progress.
  rewrite get_append // length_init //.
  case (0 <= i < n)=> h.
    by rewrite get_init //;smt.
    by rewrite get_sub ? H2 //;smt.

  rewrite /Sch.Scheme.funG /randFormatD /funG /funDE /topoDE /= !(fst_pair, snd_pair) /= !(fst_pair, snd_pair) /= !(fst_pair, snd_pair) /arrayToMap2 /mapToArray2 /=.
  apply map_ext=> y.
  elim/tuple3_ind y=> g a b hy.
  rewrite length_init //=.
  rewrite !(_: forall (g:(int*bool*bool, word) map) (y:int*bool*bool), get g y = g.[y]);first 2 by trivial.
  rewrite ! DKCScheme.Local.get_initGates //.
  case (n <= g < n + q)=> h //.
  rewrite /garbMap /garbleGate /initGates /DKCScheme.Local.enc /enc /evalTupleGate /randED !(fst_pair, snd_pair) /=.
  rewrite get_init /=;first smt.
  simplify "_.[_]".
  pose r' := randFormat (n + q) m (randDE r).
  cut h' : 0 <= g - n < q by smt.
  rewrite !get_sub // ?H1 ?H2 //.
  rewrite get_init //.
  rewrite /evalTupleGate /DKCScheme.W.getlsb /getTok /=.
  rewrite ! get_arrayToMap.
  rewrite ! (fst_pair, snd_pair).
  cut -> : length r' = n + q by smt.
  cut -> : 0 <= g < n + q by smt.
  cut -> : 0 <= aa.[g] < n + q by smt.
  cut -> : 0 <= bb.[g] < n + q by smt.
  simplify.
  rewrite /DKCScheme.W.getlsb /getTok /=.
  rewrite !proj_some.

  cut h1 : getlsb (snd r'.[aa.[g]]) = ! getlsb (fst r'.[aa.[g]]).
  simplify r' randFormat.
  case (length (randDE r) < n + q).
    by rewrite get_init ?fst_pair ?snd_pair ?get_setlsb;smt.
    by (rewrite Array.get_mapi /=;first smt);case (aa.[g] < n + q - m);rewrite fst_pair snd_pair !get_setlsb //.

  cut h2 : getlsb (snd r'.[bb.[g]]) = ! getlsb (fst r'.[bb.[g]]).
  simplify r' randFormat.
  case (length (randDE r) < n + q).
    by rewrite get_init ?fst_pair ?snd_pair ?get_setlsb;smt.
    by (rewrite Array.get_mapi /=;first smt);case (bb.[g] < n + q - m);rewrite fst_pair snd_pair !get_setlsb //.

  cut -> : g - n + n = g by smt.
  cut -> : n + (g - n) = g by smt.

  cut -> : ((getlsb
              ((if a ^^ (getlsb (fst r'.[aa.[g]]))%DKCScheme.W
                  then snd
                else fst) r'.[aa.[g]]))) = a.
    case (getlsb (fst r'.[aa.[g]])).
      by rewrite xor_true;(case a=> ha //=;last rewrite h1);intros -> //.
      by rewrite xor_false -rw_neqF;(case a=> ha //=;first rewrite h1);intros -> //.

  cut -> : ((getlsb
              ((if b ^^ (getlsb (fst r'.[bb.[g]]))%DKCScheme.W
                  then snd
                else fst) r'.[bb.[g]]))) = b.
    case (getlsb (fst r'.[bb.[g]])).
      by rewrite xor_true;(case b=> hb //=;last rewrite h2);intros -> //.
      by rewrite xor_false -rw_neqF;(case b=> hb //=;first rewrite h2);intros -> //.

  rewrite h1 h2.

  case a=> ha;case b=> hb /=;rewrite ? (xor_comm true) ? xor_true ? (xor_comm false) ? xor_false;smt.
  qed.

  pred inputK_valid (fn:'a DKCScheme.Local.funct_t) (r:DKCScheme.SchSecurity.Sch.Scheme.rand_t) = 
    let (n, m, q, aa, bb) = fst fn in
    0 <= n /\
    0 <= q /\
    1 + fold (lambda (p:int * bool) (s:int), max (fst p) s) (-1) (dom r) = (n + q)%Int.

  lemma inputK_ED fn r : inputK_valid fn r =>
    inputKED (ProjScheme.Sch.Scheme.inputK (funDE fn) (randDE r)) =
     DKCScheme.SchSecurity.Sch.Scheme.inputK fn (randFormatD (fst fn) r).
  proof strict.
  simplify inputKED funDE randED topoDE topoED Sch.Scheme.inputK
           DKCScheme.SchSecurity.Sch.Scheme.inputK inputK DKCScheme.C2.inputK inputK_valid.
  elim/tuple2_ind fn=> topo gg hx.
  rewrite !fst_pair /=.
  rewrite !snd_pair /=.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  rewrite !fst_pair /=.
  progress.
  apply map_ext=> y.
  cut nq_pos : 0 <= n + q by smt.
  cut len_r : length (randDE r) = n + q by rewrite /randDE /mapToArray /= length_init H1 //.
  simplify randFormatD randED.
  rewrite get_arrayToMap.
  rewrite get_filter !get_arrayToMap //.
  rewrite Array.length_sub //=;
    first by rewrite length_randFormat // len_r //;smt.
  rewrite length_randFormat // len_r //.
  case (0 <= fst y < n)=> h //.
  cut ->: 0 <= fst y < n + q by smt.
  rewrite get_sub // length_randFormat len_r;smt.
  qed.

  lemma outputK_ED fn r:
    outputKED (ProjScheme.Sch.Scheme.outputK (funDE fn) (randDE r)) =
     DKCScheme.SchSecurity.Sch.Scheme.outputK fn (randFormatD (fst fn) r)
  by smt.

  lemma decode_ED k y:
    outputED (ProjScheme.Sch.Scheme.decode (outputKDE k) (outputGDE y)) =
     DKCScheme.SchSecurity.Sch.Scheme.decode k y
  by smt.

lemma pi_sampler_ED x : functD_topo_valid (fst x) =>
  (let y = (ProjScheme.Sch.Scheme.pi_sampler (leakDE (fst x), (outputDE (snd x)))) in (funED (fst y), snd y)) = DKCScheme.SchSecurity.Sch.Scheme.pi_sampler x.
proof strict.
simplify Sch.Scheme.pi_sampler Mtopo DKCScheme.SchSecurity.Sch.Scheme.pi_sampler DKCScheme.C2.pi_sampler funED topoDE topoED arrayToMap2 functD_topo_valid leakDE.
elim/tuple2_ind x=> t i hx /=.
elim/tuple5_ind t=> n m q aa bb htopo /=.
simplify fst snd.
progress.
apply array_ext;split;smt.
apply array_ext;split;smt.
apply map_ext=> y.
elim/tuple3_ind y=> g a b hy.
cut := DKCScheme.Local.get_initGates n q (lambda (g : int) (a b : bool), ! g < n + q - m && i.[g - (n + q - m)]).
simplify "_.[_]".
intros -> //.
cut := DKCScheme.Local.get_initGates n q  (lambda (g0 : int) (a0 b0 : bool),
         evalTupleGate
           (init q
              (lambda (g1 : int),
                 if g1 < q - m then (false, false, false, false)
                 else
                   ((outputDE i).[g1 - (q - m)], (outputDE i).[g1 - (q - m)],
                    (outputDE i).[g1 - (q - m)], (outputDE i).[g1 - (q - m)]))).[
           g0 - n] a0 b0).
simplify "_.[_]".
rewrite length_init /=;first smt.
intros -> //.
case (n <= g < n + q)=> h //.
rewrite get_init //;first smt.
simplify evalTupleGate.
case (g - n < q - m);smt.
qed.

  lemma validInputs_DE fn i :
    ProjScheme.Sch.Scheme.validInputs fn i =
     DKCScheme.SchSecurity.Sch.Scheme.validInputs (funED fn) (inputED i).
  proof strict.
  simplify mapToArray2 DKCScheme.Local.validGG DKCScheme.C2.validInputs
           validInputs ProjScheme.Sch.Scheme.validInputs
           DKCScheme.SchSecurity.Sch.Scheme.validInputs funED inputED topoED.
  rewrite DKCScheme.Local.valid_wireinput valid_wireinput /DKCScheme.Local.validCircuitP /validCircuitP.
  elim/tuple2_ind fn=> topo gg hx.
  elim/tuple5_ind topo=> n m q aa bb htopo /=.
  rewrite !fst_pair /= !snd_pair /= !fst_pair /=.
  rewrite rw_eq_iff.
  progress; first 9 smt.
    by cut := H7 (length i) false false _;smt.
    by cut := H8 (i0 + length i) _;smt.
    by cut := H8 (i0 + length i) _;smt.
    by cut := H8 (i0 + length i) _;smt.
    by cut := H8 (i0 + length i) _;smt.
  qed.
(* End Morphism *)
  lemma valids fn i r : Sch.Scheme.validInputs fn i => Sch.Scheme.validRand fn r => (
    functD_topo_valid (leakED (Sch.Scheme.phi fn)) &&
    functE_topo_valid (Sch.Scheme.phi fn) &&
    functE_valid fn &&
    eval_valid (funED fn) (inputED i) &&
    validRandD fn r &&
    encode_valid (inputKED (Sch.Scheme.inputK fn r)) (inputED i) &&
    funG_valid (funED fn) (randED r) &&
    inputK_valid (funED fn) (randED r) &&
    eval_valid (funGED (Sch.Scheme.funG fn r)) (inputGED (Sch.Scheme.Input.encode (Sch.Scheme.inputK fn r) i)) &&
    functE_valid (Sch.Scheme.funG fn r) &&
    functE_valid (fst ((Sch.Scheme.pi_sampler (Sch.Scheme.phi fn, (Sch.Scheme.eval fn i))))) &&
    functE_valid (Sch.Scheme.funG (fst ((Sch.Scheme.pi_sampler (Sch.Scheme.phi fn, (Sch.Scheme.eval fn i))))) r) &&
    encode_valid
      (inputKED ((Sch.Scheme.inputK (fst ((Sch.Scheme.pi_sampler (Sch.Scheme.phi fn, (Sch.Scheme.eval fn i))))) r)))
      (snd ((Sch.Scheme.pi_sampler (Sch.Scheme.phi fn, (Sch.Scheme.eval fn i)))))
    ).
  proof strict.
  elim/tuple2_ind fn=> topo gg hfn.
  elim/tuple5_ind topo=> n m q aa bb htopo.
  simplify Sch.Scheme.validInputs Sch.Scheme.validRand functE_valid eval_valid funED inputED validRandD encode_valid inputKED Sch.Scheme.inputK inputED funG_valid inputK_valid eval_valid functE_valid Sch.Scheme.funG functE_topo_valid validInputs topoED functE_gg_valid inputGED Sch.Scheme.Input.encode funG funGED inputK Sch.Scheme.phi Sch.Scheme.pi_sampler Mtopo.
  rewrite valid_wireinput /validCircuitP /= !(fst_pair, snd_pair) /= !(fst_pair, snd_pair) /= !(fst_pair, snd_pair) /=.
(* this progress brakes too much thing especially the goal of kind :

forall (i0 : int),
   0 <= i0 < q =>
   0 <= aa.[i0] /\
   bb.[i0] < n + i0 /\ bb.[i0] < n + q - m /\ aa.[i0] < bb.[i0])

this forced you to proved 4 goal separately whereas the proof is the same for the 4
and maybe smt would be more bright without this
*)
  progress.
  by rewrite length_pos.
  by rewrite length_pos.
  by rewrite length_append length_init ? length_pos.
  by rewrite length_append length_init ? length_pos // -H4.
  by rewrite get_append_left ? length_init ? length_pos ? get_init.
  by rewrite get_append_left ? length_init ? length_pos ? get_init.
  by rewrite length_pos.
  by rewrite length_pos.
  by rewrite length_append length_init ? length_pos.
  by rewrite length_append length_init ? length_pos // -H4.
  by cut := H6 i0 _=> //;progress;rewrite get_append_right ? length_init ? length_pos //;smt.
  by cut := H6 i0 _=> //;progress;rewrite get_append_right ? length_init ? length_pos //;smt.
  by cut := H6 i0 _=> //;progress;rewrite get_append_right ? length_init ? length_pos //;smt.
  by cut := H6 i0 _=> //;progress;rewrite get_append_right ? length_init ? length_pos //;smt.
  smt.
  by rewrite max_arrayToMap;smt.
  by rewrite get_append_left ? length_init ? length_pos ? get_init.
  by rewrite get_append_left ? length_init ? length_pos ? get_init.
  by rewrite /randED max_arrayToMap;smt.
  by (cut := H6 (i0 - length i) _;first smt);progress;rewrite get_append_right ? length_init ? length_pos //;smt.
  by (cut := H6 (i0 - length i) _;first smt);progress;rewrite get_append_right ? length_init ? length_pos //;smt.
  by (cut := H6 (i0 - length i) _;first smt);progress;rewrite get_append_right ? length_init ? length_pos //;smt.
  by (cut := H6 (i0 - length i) _;first smt);progress;rewrite get_append_right ? length_init ? length_pos //;smt.
  by rewrite length_init ? Array.length_sub // length_randFormat;smt.
  by rewrite length_init.
  by rewrite length_init.
  by rewrite length_init.
  by rewrite max_arrayToMap;smt.
  qed.

(* Begin Correction Lemma *)
  lemma sch_correct: DKCScheme.DKCSecurity.D.Correct () => Sch.Correct ().
  proof strict.
  intros h.
  rewrite /Sch.Correct=> r fn i.
  intros h1 h2.
  cut := valids fn i r _ _=> //.
  do 12 ! intros [? ];intros ?.
  generalize h1 h2.
  rewrite validInputs_DE validRand_DE //.
  intros hInputs hRand.
  cut:= DKCScheme.sch_correct _ (randFormatD (fst (funED fn)) (randED r)) (funED fn) (inputED i) _ _=> //=.
  by rewrite - ?(eval_ED, decode_ED, outputK_ED, inputK_ED, funG_ED, encode_ED, evalG_ED) ?(fun_DEED, rand_DEED, inputK_DEED, input_DEED, outputK_DEED, outputG_DEED, inputG_DEED, funG_DEED).
  qed.
(* End Correction Lemma *)

  clone import SchSec.SchSecurity with
    theory Sch.Scheme = ProjScheme.Sch.Scheme.

(* Begin Random equivalence *)
  module R1 = {
    module C' = DKCScheme.C
    module R' = DKCScheme.R

    var trnd : bool
    var tok1, tok2 : word

    fun genTok(b:bool) : unit = {
      trnd = $ {0,1};
      trnd = b ? trnd : false;
      tok1 = $Dword.dwordLsb ( trnd);
      tok2 = $Dword.dwordLsb (!trnd);
    }

    fun gen(l:topo_t): DKCScheme.SchSecurity.Sch.Scheme.rand_t = {
      var i:int;
      var x:(int*bool, word) map;
      var (n, m, q, aa, bb) = l;
    
      (n, m, q, aa, bb) = l;
      x = FMap.Core.empty;
      i = 0;
      while (i < n + q) {
        genTok(i < n + q - m);

        x.[(i, false)] = tok1;
        x.[(i,  true)] = tok2;

        i = i + 1;
      }
      return x;
    }
  }.

equiv Rand_R1 : DKCScheme.Rand.gen ~ R1.gen : ={l} ==> ={res}.
proof strict.
  fun.
  inline R1.genTok.
  while (
  ={i} /\
  DKCScheme.R.xx{1} = x{2} /\
  DKCScheme.C.n{1}  = n{2} /\
  DKCScheme.C.m{1}  = m{2} /\
  DKCScheme.C.q{1}  = q{2} /\
  DKCScheme.C.aa{1} = aa{2} /\
  DKCScheme.C.bb{1} = bb{2} /\ true).
    by wp;rnd;rnd;wp;rnd;wp;skip;smt.
    by wp;skip;smt.
qed.

  module Rand:EncSecurity.Rand_t = {
    module R' = DKCScheme.R
    module C' = DKCScheme.C
    module G' = DKCScheme.G
  
    var trnd : bool
    var tok1, tok2 : word

    fun genTok() : unit = {
      tok1 = $Dword.dword;
      tok2 = $Dword.dword;
    }

    fun gen(l:topo_t): Sch.Scheme.rand_t = {
      var i:int;
      var x:(word*word) array;
      var (n, m, q, aa, bb) = l;
      x = Array.init (n + q) (lambda i, (W.zeros, W.zeros));
      i = 0;
      while (i < n + q) {
        genTok();

        x.[i] = (tok1, tok2);

        i = i + 1;
      }
      return x;
    }
  }.

op base = 1%r / (2 ^ (W.length - 1))%r.

lemma dlsb': forall x b,  mu Dword.dword (lambda (y : word), x = setlsb y b) = if getlsb x = b then base else 0%r.
proof strict.
intros x b.
case (getlsb x = b)=> h;
  last (cut -> : (lambda (y : word), x = setlsb y b) = Fun.cpFalse by (apply Fun.fun_ext;smt);smt).
pose s := add (setlsb x true) (add (setlsb x false) FSet.empty).
cut -> : (lambda (y : word), x = setlsb y b) = cpMem s.
  apply Fun.fun_ext=> y.
  simplify cpMem s.
  rewrite ! mem_add.
  cut -> : forall (y:word), mem y FSet.empty = false by smt.
  by case (getlsb x);smt.
rewrite Dword.mu_cpMemw.
cut -> : card s = 2 by (rewrite /s ! card_add_nin ? card_empty;smt).
simplify base.
cut -> : forall x0, 0 < x0 => 2 ^ x0 = 2 * (2 ^ (x0-1)) by smt.
smt.
cut : 0 < 2 ^ (W.length - 1) by smt.
simplify DKCScheme.W.length W.length.
generalize (2 ^ (W.length - 1)).
smt.
qed.

lemma eqR : forall bb, equiv[R1.genTok ~ Rand.genTok : b{1} = bb ==> 
    R1.trnd{1} =  (if bb then getlsb Rand.tok1{2} else false) /\
    R1.tok1{1} = setlsb Rand.tok1{2} (if bb then  (getlsb Rand.tok1{2}) else false) /\
    R1.tok2{1} = setlsb Rand.tok2{2} (if bb then !(getlsb Rand.tok1{2}) else  true)].
proof strict.
intros bb.

cut := Dword.dwordLsb_mu_x.
simplify Distr.mu_x.
rewrite -/base.
intros dlsb.

cut := Dword.dwordLsb_lossless.
rewrite /Distr.weight.
intros dlsb_ll.

bypr
(R1.trnd{1}, R1.tok1{1}, R1.tok2{1})
(bb && getlsb Rand.tok1{2}, setlsb Rand.tok1{2} (bb && getlsb Rand.tok1{2}), setlsb Rand.tok2{2} (bb => !(getlsb Rand.tok1{2})))
=> //.
intros &1 &2 a hb1.

cut : a = a by trivial.
elim/tuple3_ind a=> a1 a2 a3 ha.
intros h;clear h.

apply (eq_trans _ (if getlsb a2 = a1 /\ getlsb a3 = !a1 then (if bb then 1%r/2%r else (if a1 = false then 1%r else 0%r))*base*base else 0%r)).

bdhoare_deno (_ : b = bb ==> (a1=R1.trnd /\ a2=R1.tok1 /\ a3=R1.tok2))=> //.
fun.
seq 3 : (a1=R1.trnd /\ a2=R1.tok1)
  (if getlsb a2 =  a1 then (if bb then 1%r/2%r else (if a1 = false then 1%r else 0%r))*base else 0%r)
  (if getlsb a3 = !a1 then base else 0%r)
  1%r
  0%r
  true.
by rnd;wp;rnd.
(******)
seq 2 : (a1=R1.trnd)
  (if bb then 1%r/2%r else (if a1 = false then 1%r else 0%r))
  (if getlsb a2 = a1 then base else 0%r)
  1%r
  0%r
  true.
by wp;rnd.
by wp;rnd;skip;case bb=> h;progress;[cut -> : (lambda (x : bool), a1 = x) = (=) a1 by (apply Fun.fun_ext;smt);smt|case a1=> /=;smt].
by rnd;skip;progress;cut -> : (lambda (x : word), a2 = x) = (=) a2 by (apply Fun.fun_ext;smt);rewrite dlsb.
by rnd;skip;progress;cut -> : (lambda (x : word), a1 = R1.trnd{hr} /\ a2 = x) = Fun.cpFalse by (apply Fun.fun_ext;smt);smt.
by case (getlsb a2 = a1);case (bb);progress=> //=.
(******)
by rnd;skip;progress;cut -> : (lambda (x : word), a3 = x) = (=) a3 by (apply Fun.fun_ext;smt);rewrite dlsb.
by rnd;skip;progress;cut -> : (lambda (x : word), a1 = R1.trnd{hr} /\ a2 = R1.tok1{hr} /\ a3 = x) = Fun.cpFalse by (apply Fun.fun_ext;smt);smt.
by case (getlsb a2 = a1);case (getlsb a3 = ! a1);case (bb)=> //=.

bdhoare_deno (_ : true ==>
  a1 = (bb && getlsb Rand.tok1) /\
  a2 = setlsb Rand.tok1 (bb && getlsb Rand.tok1) /\
  a3 = setlsb Rand.tok2 (bb => ! getlsb Rand.tok1))=> //.

fun.

seq 1 : (a1 = (bb && getlsb Rand.tok1) /\ a2 = setlsb Rand.tok1 (bb && getlsb Rand.tok1))
  (if getlsb a2 = a1 then
    (if bb then 1%r/2%r else (if a1 = false then 1%r else 0%r))*base
   else 0%r)
  (if getlsb a3 = !a1 then base else 0%r)
  1%r
  0%r.
by rnd;skip.
(******)
rnd;skip;progress.
case bb=> hb /=.

case (getlsb a2 = a1)=> h.
cut -> : (lambda (x : word), a1 = getlsb x /\ a2 = setlsb x (getlsb x)) = (=) a2
  by (apply Fun.fun_ext=> y /=;rewrite -h set_getlsb;smt).
cut := Dword.mu_x_def.
simplify Distr.mu_x base.
intros ->.
cut : 0 < W.length by smt.
by (generalize (W.length);intros x hx;cut <- : (2 ^ x)%r = ((2 ^ (x - 1))%r * 2%r) by smt);smt.

by cut -> : (lambda (x : word), a1 = getlsb x /\ a2 = setlsb x (getlsb x)) = Fun.cpFalse by (apply Fun.fun_ext;smt);smt.

case (a1=false)=> /=.
by rewrite dlsb';smt.
by cut -> : (lambda (x : word), false) = Fun.cpFalse by (apply Fun.fun_ext=> //);smt.
(*******)
by rnd;skip;progress;rewrite dlsb';smt.
by rnd;skip;progress;cut -> : (lambda (x : word),
     a1 = (bb && getlsb Rand.tok1{hr}) /\
     a2 = setlsb Rand.tok1{hr} (bb && getlsb Rand.tok1{hr}) /\
     a3 = setlsb x (bb => ! getlsb Rand.tok1{hr})) = Fun.cpFalse by (apply Fun.fun_ext;smt);smt.
by case (getlsb a2 = a1);case (getlsb a3 = !a1);case (bb);case a1=> //=.
qed.

lemma RandEq fn :
  equiv[DKCScheme.Rand.gen ~ Rand.gen :  let (n, m, q, aa, bb) = fn in
  l{1} = topoED fn /\
  l{2} = fn /\
  0 <= n /\
  0 <= q ==>  let (n, m, q, aa, bb) = fn in
    (res{1} = randED (randFormat (n+q)%Int m res{2})) /\
    (length res{2} = n + q)].
proof strict.
  cut : fn = fn by trivial.
  elim/tuple5_ind fn=> n' m' q' aa' bb' hfn /=.  
  bypr (res{1}, n'+q') (randED (randFormat (n' + q') m' res{2}), length res{2})=> //.
  intros &o1 &o2 a.
  simplify topoED.
  progress.
  apply (eq_trans _ Pr[R1.gen(l{o1}) @ &o1 : a = (res, n' + q')]);
    first by equiv_deno Rand_R1=> //.
  equiv_deno (_:l{1}=l{o1} /\ l{2}=l{o2} ==> (res{1} = randED (randFormat (n'+q') m' res{2})) /\ (length res{2} = n' + q'))=> //;
    last by progress;rewrite H3.
  fun.
  while (
    ={i} /\
    0 <= i{2} /\
    i{1} <= n' + q' /\
    (n  = n' /\
    m  = m' /\
    q  = q' /\
    aa = (init n' (lambda (i : int), 0) || aa') /\
    bb = (init n' (lambda (i : int), 0) || bb')){1} /\
    (n  = n' /\
    m  = m' /\
    q  = q' /\
    aa = aa' /\
    bb = bb'){2} /\
    (forall j b, 0 <= j < i{1} <=> in_dom (j, b) x{1}) /\
    (forall j, 0 <= j < i{1} => 
      x{1}.[(j, false)] = Some (setlsb (fst x{2}.[j]) (if j < n' + q' - m' then   getlsb (fst x{2}.[j]) else false)) /\
      x{1}.[(j,  true)] = Some (setlsb (snd x{2}.[j]) (if j < n' + q' - m' then ! getlsb (fst x{2}.[j]) else true ))) /\
    (length x = n + q){2}).

    wp.

    exists* (i{1}).
    elim*.
    intros i.

    pose bb := i < n' + q' - m'.

    call (_:b{1} = bb ==> 
    R1.trnd{1} =  (if bb then getlsb Rand.tok1{2} else false) /\
    R1.tok1{1} = setlsb Rand.tok1{2} (if bb then  (getlsb Rand.tok1{2}) else false) /\
    R1.tok2{1} = setlsb Rand.tok2{2} (if bb then !(getlsb Rand.tok1{2}) else  true)).
    by apply (eqR bb).

    skip.
    simplify bb.
    (do ! (do ? (intros h;elim h;clear h);intros ?)).
    subst.
    simplify.
    (do ! (do ? (intros h;elim h;clear h);intros ?)).
    subst.
    split;first smt.
    split;first smt.
    split=> //.
    intros j b;rewrite ! in_dom_spec ! get_set.
    case (i{2} = j)=> h.
      by rewrite h;case b;smt.
      by cut := H17 j b;smt.
    split=> //.
    intros j hj.
    simplify "_.[_]".
    rewrite ! get_set Array.get_set;first smt.
    case (i{2} = j)=> h.
      by rewrite -h //=;(cut -> : ((i{2}, true) = (i{2}, false)) = false by smt);rewrite /= fst_pair snd_pair //.
      by cut := H18 j _;smt.
  smt.
  wp.
  skip.
  generalize H H0.
  progress;first 7 smt.
  apply map_ext=> y.
  elim/tuple2_ind y=> i b hy.
  simplify randED randFormat.
  rewrite get_arrayToMap fst_pair snd_pair /=.
  cut -> : (length x_R < n' + q') = false by smt.
  rewrite /= length_mapi.
  case (0 <= i < length x_R)=> hh.
  rewrite Array.get_mapi //.

  cut := H8 i _;first by smt.
  simplify "_.[_]".
  progress.
  case b;[rewrite H11|rewrite H10];case (i < n' + q' - m');smt.

  cut := H7 i b.
  cut -> : 0 <= i < i_R = false by smt.
  simplify=> h.
  smt.
qed.

lemma Rand_islossless : islossless Rand.gen.
proof strict.
fun.
while (true) (n + q - i + 1).
intros z.
inline Rand.genTok.
wp;rnd;rnd;skip;smt.
wp;skip;smt.
qed.

equiv Rand_stateless : Rand.gen ~ Rand.gen : ={l} ==> res{1} = res{2}.
proof strict.
fun.
while (={i, x, n, m, q, aa, bb}).
inline Rand.genTok.
by wp;rnd;rnd.
by wp.
qed.

(* End Random equivalence *)

(* Begin security lemma *)

  equiv Sim_stateless: EncSecurity.SIM(Rand).sim ~ EncSecurity.SIM(Rand).sim: ={leakage} ==> ={glob EncSecurity.SIM, res}.
  proof strict.
  by fun; wp; call Rand_stateless; wp.
  qed.

module Red(A:EncSecurity.Adv_SIM_t) : DKCScheme.SchSecurity.EncSecurity.Adv_SIM_t = {
  fun gen_query() : DKCScheme.SchSecurity.EncSecurity.query_SIM = {
    var (f, x) : SchSecurity.EncSecurity.Encryption.plain;
    (f, x) = A.gen_query();
    return (funED f, inputED x);
  }
  fun get_challenge(cipher : DKCScheme.SchSecurity.EncSecurity.Encryption.cipher) : bool =
  {
    var (f, y, ko) : DKCScheme.SchSecurity.EncSecurity.Encryption.cipher;
    var b : bool;
    (f, y, ko) = cipher;
    b = A.get_challenge((funGDE f, inputGDE y, outputKDE ko));
    return b;
  }
}.

lemma sch_is_sim (A <: EncSecurity.Adv_SIM_t {Rand,DKCScheme.DKCSecurity.DKCm} ) &m:
 islossless A.gen_query =>
 islossless A.get_challenge =>
  `|2%r * Pr[EncSecurity.Game_SIM(Rand,EncSecurity.SIM(Rand), A).main()@ &m:res] - 1%r| <=
    2%r * (DKCScheme.bound+1)%r * `|2%r * Pr[DKCScheme.DKCSecurity.Game(DKCScheme.DKCSecurity.Dkc, DKCScheme.RedI(DKCScheme.SchSecurity.EncSecurity.RedSI(Red(A)))).main()@ &m:res] - 1%r|.
proof strict.
intros=> ll_ADVp1 ll_ADVp2.
cut := DKCScheme.sch_is_sim (Red(A)) &m _ _=> //.
by fun;call ll_ADVp1.
by fun;call ll_ADVp2;wp.

cut -> : Pr[DKCScheme.SchSecurity.EncSecurity.Game_SIM(DKCScheme.Rand, DKCScheme.SchSecurity.EncSecurity.SIM(DKCScheme.Rand), Red(A)).main()@ &m : res] = Pr[EncSecurity.Game_SIM(Rand, EncSecurity.SIM(Rand), A).main()@ &m : res];intros=> //.
equiv_deno (_: ={glob A} ==> ={res})=> //.
fun.
inline Red(A).gen_query Red(A).get_challenge EncSecurity.SIM(Rand).sim DKCScheme.SchSecurity.EncSecurity.SIM(DKCScheme.Rand).sim.


seq 3 2 : (={glob A} /\
(query{1} = (funED (fst query{2}), inputED (snd query{2}))) /\ real{1} = real{2} );
first by rnd;wp;call (_: ={glob A} ==> ={res, glob A});first fun true.

if;[smt|by rnd|].

(if;first smt);
exists* query{2};
elim*;
intros qu;
wp;(call (_: ={glob A, cipher} ==> ={res});first fun true=> //);
wp;call (RandEq (EncSecurity.Encryption.randfeed qu));
wp;

skip;
clear ll_ADVp1 ll_ADVp2;
intros &1 &2;
simplify DKCScheme.SchSecurity.EncSecurity.queryValid_SIM DKCScheme.SchSecurity.EncSecurity.Encryption.valid_plain DKCScheme.SchSecurity.EncSecurity.Encryption.randfeed EncSecurity.Encryption.randfeed Sch.Scheme.phi DKCScheme.SchSecurity.EncSecurity.Encryption.enc DKCScheme.SchSecurity.Sch.Scheme.phi ProjScheme.Sch.Scheme.phi DKCScheme.C2.phi EncSecurity.Encryption.enc Sch.Scheme.funG Sch.Scheme.Input.encode Sch.Scheme.outputK Sch.Scheme.inputK DKCScheme.SchSecurity.EncSecurity.Encryption.pi_sampler DKCScheme.SchSecurity.Sch.Scheme.phi EncSecurity.Encryption.pi_sampler Sch.Scheme.pi_sampler Sch.Scheme.phi Sch.Scheme.eval EncSecurity.Encryption.leak DKCScheme.SchSecurity.EncSecurity.Encryption.leak;
(do ! (do ? (intros h;elim h;clear h);intros ?));
subst;
generalize H3;
elim/tuple2_ind query{2}=> fn i hqu;
elim/tuple2_ind fn=> topo gg hfn;
elim/tuple5_ind topo=> n m q aa bb htopo;
rewrite ! fst_pair /= ! snd_pair /=;
rewrite -validInputs_DE;
intros valid;
(split;first by 
simplify fst snd funED topoED DKCScheme.SchSecurity.Sch.Scheme.pi_sampler DKCScheme.SchSecurity.EncSecurity.Encryption.leak DKCScheme.C2.pi_sampler DKCScheme.SchSecurity.Sch.Scheme.phi DKCScheme.C2.phi ProjScheme.Sch.Scheme.pi_sampler ProjScheme.Sch.Scheme.eval Mtopo;smt);
intros h' {h'} result_L result_R;
intros h;elim h;clear h;intros h1 len_res;
subst;
(split;last by subst;trivial);
(cut := valids ((n, m, q, aa, bb), gg) i result_R _ _=> //);
(do 12 ! intros [? ];intros ?).

  cut -> : randED (randFormat (n + q) m result_R) = randFormatD (fst (funED ((n, m, q, aa, bb), gg))) (randED result_R)
    by rewrite /randFormatD /funED /topoED /fst rand_DEED //.
  by rewrite - !(eval_ED, decode_ED, outputK_ED, inputK_ED, funG_ED, encode_ED, evalG_ED) ?(fun_DEED, rand_DEED, inputK_DEED, input_DEED, outputK_DEED, outputG_DEED, inputG_DEED, funG_DEED).

  cut -> : randED (randFormat (n + q) m result_R) = randFormatD (fst (fst
              ((DKCScheme.SchSecurity.Sch.Scheme.pi_sampler
                  (fst (funED ((n, m, q, aa, bb), gg)),
                   (DKCScheme.SchSecurity.Sch.Scheme.eval
                      (funED ((n, m, q, aa, bb), gg)) (inputED i))))))) (randED result_R)
    by rewrite /randFormatD /funED /topoED /fst rand_DEED //.
  cut -> : fst (funED ((n, m, q, aa, bb), gg)) = leakED (Sch.Scheme.phi ((n, m, q, aa, bb), gg)) by trivial.
  by rewrite - !(pi_sampler_ED, eval_ED, decode_ED, outputK_ED, inputK_ED, funG_ED, encode_ED, evalG_ED, phi_ED, leak_ED) /= ?(fst_pair, snd_pair, fun_DEED, rand_DEED, inputK_DEED, input_DEED, outputK_DEED, output_DEED, outputG_DEED, inputG_DEED, funG_DEED, leak_DEED) //.
qed.

(* End security lemma *)

end EfficientScheme.