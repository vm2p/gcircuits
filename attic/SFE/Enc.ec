theory Encryption.
    type rand.
    type plain.
    type cipher.
    type leakage.
    type randgenin.

    op enc:plain -> rand -> cipher.
    op valid_plain:plain -> bool.
    op leak:plain -> leakage.
    op randfeed:plain -> randgenin.
    op pi_sampler:leakage -> plain.
end Encryption.
